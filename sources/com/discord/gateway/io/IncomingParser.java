package com.discord.gateway.io;

import andhook.lib.HookHelper;
import com.discord.gateway.opcodes.Opcode;
import com.discord.models.domain.Model;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: Incoming.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b\u0000\u0018\u00002\u00020\u0001B\u001b\u0012\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00070\n¢\u0006\u0004\b\u0019\u0010\u001aJ\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\u0017\u0010\b\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\b\u0010\tR\"\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00070\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\f\u0010\rR\u0018\u0010\u000e\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0018\u0010\u0011\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0014\u001a\u00020\u00138\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u0018\u0010\u0017\u001a\u0004\u0018\u00010\u00168\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lcom/discord/gateway/io/IncomingParser;", "Lcom/discord/models/domain/Model;", "Lcom/discord/gateway/io/Incoming;", "build", "()Lcom/discord/gateway/io/Incoming;", "Lcom/discord/models/domain/Model$JsonReader;", "reader", "", "assignField", "(Lcom/discord/models/domain/Model$JsonReader;)V", "Lkotlin/Function1;", "", "log", "Lkotlin/jvm/functions/Function1;", "type", "Ljava/lang/String;", "", "data", "Ljava/lang/Object;", "Lcom/discord/gateway/opcodes/Opcode;", "opcode", "Lcom/discord/gateway/opcodes/Opcode;", "", "seq", "Ljava/lang/Integer;", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function1;)V", "gateway_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class IncomingParser implements Model {
    private Object data;
    private final Function1<String, Unit> log;
    private Opcode opcode;
    private Integer seq;
    private String type;

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            Opcode.values();
            int[] iArr = new int[20];
            $EnumSwitchMapping$0 = iArr;
            iArr[Opcode.HELLO.ordinal()] = 1;
            iArr[Opcode.INVALID_SESSION.ordinal()] = 2;
            iArr[Opcode.DISPATCH.ordinal()] = 3;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public IncomingParser(Function1<? super String, Unit> function1) {
        m.checkNotNullParameter(function1, "log");
        this.log = function1;
    }

    /* JADX WARN: Code restructure failed: missing block: B:101:0x021a, code lost:
        if (r3.equals("MESSAGE_DELETE") != false) goto L133;
     */
    /* JADX WARN: Code restructure failed: missing block: B:103:0x0224, code lost:
        if (r3.equals(com.discord.utilities.fcm.NotificationData.TYPE_MESSAGE_CREATE) != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:104:0x0226, code lost:
        r6.data = com.discord.models.deserialization.gson.InboundGatewayGsonParser.fromJson(r7, com.discord.api.message.Message.class);
     */
    /* JADX WARN: Code restructure failed: missing block: B:106:0x0236, code lost:
        if (r3.equals("INTERACTION_SUCCESS") != false) goto L178;
     */
    /* JADX WARN: Code restructure failed: missing block: B:111:0x0252, code lost:
        if (r3.equals("CALL_UPDATE") != false) goto L146;
     */
    /* JADX WARN: Code restructure failed: missing block: B:113:0x025c, code lost:
        if (r3.equals("GUILD_UPDATE") != false) goto L151;
     */
    /* JADX WARN: Code restructure failed: missing block: B:127:0x02e0, code lost:
        if (r3.equals("GUILD_MEMBER_UPDATE") != false) goto L194;
     */
    /* JADX WARN: Code restructure failed: missing block: B:132:0x02fc, code lost:
        if (r3.equals("MESSAGE_DELETE_BULK") != false) goto L133;
     */
    /* JADX WARN: Code restructure failed: missing block: B:133:0x02fe, code lost:
        r6.data = r7.parse(new com.discord.models.domain.ModelMessageDelete());
     */
    /* JADX WARN: Code restructure failed: missing block: B:138:0x0323, code lost:
        if (r3.equals("CHANNEL_UPDATE") != false) goto L162;
     */
    /* JADX WARN: Code restructure failed: missing block: B:143:0x033f, code lost:
        if (r3.equals("CALL_DELETE") != false) goto L146;
     */
    /* JADX WARN: Code restructure failed: missing block: B:145:0x0348, code lost:
        if (r3.equals("CALL_CREATE") != false) goto L146;
     */
    /* JADX WARN: Code restructure failed: missing block: B:146:0x034a, code lost:
        r6.data = r7.parse(new com.discord.models.domain.ModelCall());
     */
    /* JADX WARN: Code restructure failed: missing block: B:148:0x035d, code lost:
        if (r3.equals("GUILD_DELETE") != false) goto L151;
     */
    /* JADX WARN: Code restructure failed: missing block: B:150:0x0366, code lost:
        if (r3.equals("GUILD_CREATE") != false) goto L151;
     */
    /* JADX WARN: Code restructure failed: missing block: B:151:0x0368, code lost:
        r6.data = com.discord.models.deserialization.gson.InboundGatewayGsonParser.fromJson(r7, com.discord.api.guild.Guild.class);
     */
    /* JADX WARN: Code restructure failed: missing block: B:156:0x038a, code lost:
        if (r3.equals("CHANNEL_RECIPIENT_REMOVE") != false) goto L157;
     */
    /* JADX WARN: Code restructure failed: missing block: B:157:0x038c, code lost:
        r6.data = com.discord.models.deserialization.gson.InboundGatewayGsonParser.fromJson(r7, com.discord.api.channel.ChannelRecipient.class);
     */
    /* JADX WARN: Code restructure failed: missing block: B:159:0x039c, code lost:
        if (r3.equals("CHANNEL_DELETE") != false) goto L162;
     */
    /* JADX WARN: Code restructure failed: missing block: B:161:0x03a5, code lost:
        if (r3.equals("CHANNEL_CREATE") != false) goto L162;
     */
    /* JADX WARN: Code restructure failed: missing block: B:162:0x03a7, code lost:
        r6.data = com.discord.models.deserialization.gson.InboundGatewayGsonParser.fromJson(r7, com.discord.api.channel.Channel.class);
     */
    /* JADX WARN: Code restructure failed: missing block: B:164:0x03b5, code lost:
        if (r3.equals("RELATIONSHIP_REMOVE") != false) goto L203;
     */
    /* JADX WARN: Code restructure failed: missing block: B:166:0x03bf, code lost:
        if (r3.equals("STAGE_INSTANCE_UPDATE") != false) goto L200;
     */
    /* JADX WARN: Code restructure failed: missing block: B:177:0x0405, code lost:
        if (r3.equals("INTERACTION_CREATE") != false) goto L178;
     */
    /* JADX WARN: Code restructure failed: missing block: B:178:0x0407, code lost:
        r6.data = com.discord.models.deserialization.gson.InboundGatewayGsonParser.fromJson(r7, com.discord.api.interaction.InteractionStateUpdate.class);
     */
    /* JADX WARN: Code restructure failed: missing block: B:189:0x044d, code lost:
        if (r3.equals("MESSAGE_REACTION_ADD") != false) goto L255;
     */
    /* JADX WARN: Code restructure failed: missing block: B:191:0x0457, code lost:
        if (r3.equals("STAGE_INSTANCE_DELETE") != false) goto L200;
     */
    /* JADX WARN: Code restructure failed: missing block: B:193:0x0460, code lost:
        if (r3.equals("GUILD_MEMBER_ADD") != false) goto L194;
     */
    /* JADX WARN: Code restructure failed: missing block: B:194:0x0462, code lost:
        r6.data = com.discord.models.deserialization.gson.InboundGatewayGsonParser.fromJson(r7, com.discord.api.guildmember.GuildMember.class);
     */
    /* JADX WARN: Code restructure failed: missing block: B:196:0x0472, code lost:
        if (r3.equals("GUILD_BAN_ADD") != false) goto L197;
     */
    /* JADX WARN: Code restructure failed: missing block: B:197:0x0474, code lost:
        r6.data = r7.parse(new com.discord.models.domain.ModelBan());
     */
    /* JADX WARN: Code restructure failed: missing block: B:199:0x0487, code lost:
        if (r3.equals(com.discord.utilities.fcm.NotificationData.TYPE_STAGE_INSTANCE_CREATE) != false) goto L200;
     */
    /* JADX WARN: Code restructure failed: missing block: B:200:0x0489, code lost:
        r6.data = com.discord.models.deserialization.gson.InboundGatewayGsonParser.fromJson(r7, com.discord.api.stageinstance.StageInstance.class);
     */
    /* JADX WARN: Code restructure failed: missing block: B:202:0x0499, code lost:
        if (r3.equals(com.discord.utilities.fcm.NotificationData.TYPE_RELATIONSHIP_ADD) != false) goto L203;
     */
    /* JADX WARN: Code restructure failed: missing block: B:203:0x049b, code lost:
        r6.data = r7.parse(new com.discord.models.domain.ModelUserRelationship());
     */
    /* JADX WARN: Code restructure failed: missing block: B:217:0x04f2, code lost:
        if (r3.equals("MESSAGE_REACTION_REMOVE_ALL") != false) goto L255;
     */
    /* JADX WARN: Code restructure failed: missing block: B:219:0x04fc, code lost:
        if (r3.equals("GUILD_JOIN_REQUEST_UPDATE") != false) goto L252;
     */
    /* JADX WARN: Code restructure failed: missing block: B:230:0x053c, code lost:
        if (r3.equals("USER_CONNECTIONS_INTEGRATION_JOINING") != false) goto L231;
     */
    /* JADX WARN: Code restructure failed: missing block: B:231:0x053e, code lost:
        r7.skipValue();
        r6.data = kotlin.Unit.a;
     */
    /* JADX WARN: Code restructure failed: missing block: B:251:0x05b4, code lost:
        if (r3.equals("GUILD_JOIN_REQUEST_CREATE") != false) goto L252;
     */
    /* JADX WARN: Code restructure failed: missing block: B:252:0x05b6, code lost:
        r6.data = com.discord.models.deserialization.gson.InboundGatewayGsonParser.fromJson(r7, com.discord.api.guildjoinrequest.GuildJoinRequestCreateOrUpdate.class);
     */
    /* JADX WARN: Code restructure failed: missing block: B:254:0x05c5, code lost:
        if (r3.equals("MESSAGE_REACTION_REMOVE_EMOJI") != false) goto L255;
     */
    /* JADX WARN: Code restructure failed: missing block: B:255:0x05c7, code lost:
        r6.data = com.discord.models.deserialization.gson.InboundGatewayGsonParser.fromJson(r7, com.discord.api.message.reaction.MessageReactionUpdate.class);
     */
    /* JADX WARN: Code restructure failed: missing block: B:279:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:280:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:287:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:290:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:291:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:293:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:294:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:298:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:302:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:303:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:304:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:305:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:313:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:320:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:321:?, code lost:
        return;
     */
    /* JADX WARN: Code restructure failed: missing block: B:52:0x0103, code lost:
        if (r3.equals("THREAD_UPDATE") != false) goto L95;
     */
    /* JADX WARN: Code restructure failed: missing block: B:66:0x0156, code lost:
        if (r3.equals("GUILD_BAN_REMOVE") != false) goto L197;
     */
    /* JADX WARN: Code restructure failed: missing block: B:71:0x0172, code lost:
        if (r3.equals("INTERACTION_FAILURE") != false) goto L178;
     */
    /* JADX WARN: Code restructure failed: missing block: B:73:0x017c, code lost:
        if (r3.equals("MESSAGE_UPDATE") != false) goto L104;
     */
    /* JADX WARN: Code restructure failed: missing block: B:78:0x0198, code lost:
        if (r3.equals("USER_CONNECTIONS_UPDATE") != false) goto L231;
     */
    /* JADX WARN: Code restructure failed: missing block: B:86:0x01c9, code lost:
        if (r3.equals("THREAD_DELETE") != false) goto L95;
     */
    /* JADX WARN: Code restructure failed: missing block: B:94:0x01f6, code lost:
        if (r3.equals("THREAD_CREATE") != false) goto L95;
     */
    /* JADX WARN: Code restructure failed: missing block: B:95:0x01f8, code lost:
        r6.data = com.discord.models.deserialization.gson.InboundGatewayGsonParser.fromJson(r7, com.discord.api.channel.Channel.class);
     */
    /* JADX WARN: Code restructure failed: missing block: B:97:0x0206, code lost:
        if (r3.equals("MESSAGE_REACTION_REMOVE") != false) goto L255;
     */
    /* JADX WARN: Code restructure failed: missing block: B:99:0x0210, code lost:
        if (r3.equals("CHANNEL_RECIPIENT_ADD") != false) goto L157;
     */
    @Override // com.discord.models.domain.Model
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void assignField(final com.discord.models.domain.Model.JsonReader r7) throws java.io.IOException {
        /*
            Method dump skipped, instructions count: 1818
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.gateway.io.IncomingParser.assignField(com.discord.models.domain.Model$JsonReader):void");
    }

    public final Incoming build() {
        String str = this.type;
        Integer num = this.seq;
        Opcode opcode = this.opcode;
        if (opcode == null) {
            m.throwUninitializedPropertyAccessException("opcode");
        }
        return new Incoming(str, num, opcode, this.data);
    }
}
