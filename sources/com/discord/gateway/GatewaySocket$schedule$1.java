package com.discord.gateway;

import androidx.core.app.NotificationCompat;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import rx.functions.Action1;
/* compiled from: GatewaySocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\u0010\u0006\u001a\u00020\u00012\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u00020\u0001 \u0002*\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"Lkotlin/Function0;", "", "kotlin.jvm.PlatformType", "it", NotificationCompat.CATEGORY_CALL, "(Lkotlin/jvm/functions/Function0;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewaySocket$schedule$1<T> implements Action1<Function0<? extends Unit>> {
    public static final GatewaySocket$schedule$1 INSTANCE = new GatewaySocket$schedule$1();

    @Override // rx.functions.Action1
    public /* bridge */ /* synthetic */ void call(Function0<? extends Unit> function0) {
        call2((Function0<Unit>) function0);
    }

    /* renamed from: call  reason: avoid collision after fix types in other method */
    public final void call2(Function0<Unit> function0) {
        function0.invoke();
    }
}
