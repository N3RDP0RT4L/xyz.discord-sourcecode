package com.discord.gateway;

import d0.z.d.k;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GatewayDiscovery.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "p1", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class GatewayDiscovery$discoverGatewayUrl$3 extends k implements Function1<String, Unit> {
    public final /* synthetic */ GatewayDiscovery$discoverGatewayUrl$2 $handleSuccess$2;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GatewayDiscovery$discoverGatewayUrl$3(GatewayDiscovery$discoverGatewayUrl$2 gatewayDiscovery$discoverGatewayUrl$2) {
        super(1, null, "handleSuccess", "invoke(Ljava/lang/String;)V", 0);
        this.$handleSuccess$2 = gatewayDiscovery$discoverGatewayUrl$2;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(String str) {
        invoke2(str);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str) {
        this.$handleSuccess$2.invoke2(str);
    }
}
