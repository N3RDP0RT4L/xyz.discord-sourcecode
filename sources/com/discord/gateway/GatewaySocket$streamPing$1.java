package com.discord.gateway;

import b.d.b.a.a;
import com.discord.gateway.GatewaySocket;
import com.discord.gateway.io.Outgoing;
import com.discord.gateway.io.OutgoingPayload;
import com.discord.gateway.opcodes.Opcode;
import com.discord.utilities.logging.Logger;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: GatewaySocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewaySocket$streamPing$1 extends o implements Function0<Unit> {
    public final /* synthetic */ String $streamKey;
    public final /* synthetic */ GatewaySocket this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GatewaySocket$streamPing$1(GatewaySocket gatewaySocket, String str) {
        super(0);
        this.this$0 = gatewaySocket;
        this.$streamKey = str;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        GatewaySocket.Companion companion = GatewaySocket.Companion;
        Logger logger = this.this$0.logger;
        StringBuilder R = a.R("Sending OPCODE_STREAM_PING: ");
        R.append(this.$streamKey);
        GatewaySocket.Companion.log$default(companion, logger, R.toString(), false, 2, null);
        GatewaySocket.send$default(this.this$0, new Outgoing(Opcode.OPCODE_STREAM_PING, new OutgoingPayload.StreamPing(this.$streamKey)), false, null, 6, null);
    }
}
