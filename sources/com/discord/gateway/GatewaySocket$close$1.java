package com.discord.gateway;

import com.discord.utilities.rest.SendUtils;
import com.discord.utilities.websocket.WebSocket;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: GatewaySocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewaySocket$close$1 extends o implements Function0<Unit> {
    public final /* synthetic */ boolean $clean;
    public final /* synthetic */ GatewaySocket this$0;

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/websocket/WebSocket;", "ws", "", "invoke", "(Lcom/discord/utilities/websocket/WebSocket;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.gateway.GatewaySocket$close$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<WebSocket, Unit> {
        public final /* synthetic */ int $code;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public AnonymousClass1(int i) {
            super(1);
            this.$code = i;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(WebSocket webSocket) {
            invoke2(webSocket);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(WebSocket webSocket) {
            if (webSocket != null) {
                WebSocket.disconnect$default(webSocket, this.$code, null, 2, null);
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GatewaySocket$close$1(GatewaySocket gatewaySocket, boolean z2) {
        super(0);
        this.this$0 = gatewaySocket;
        this.$clean = z2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        int i;
        i = this.this$0.connectionState;
        if (i != 0) {
            int i2 = this.$clean ? 1000 : SendUtils.MAX_MESSAGE_CHARACTER_COUNT_PREMIUM;
            this.this$0.cleanup(new AnonymousClass1(i2));
            this.this$0.connectionState = 0;
            this.this$0.reset(this.$clean, i2, "Disconnect requested by client");
        }
    }
}
