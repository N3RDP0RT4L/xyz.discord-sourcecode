package com.discord.gateway;

import b.d.b.a.a;
import com.discord.gateway.GatewaySocket;
import com.discord.gateway.io.Outgoing;
import com.discord.gateway.io.OutgoingPayload;
import com.discord.gateway.opcodes.Opcode;
import com.discord.utilities.logging.Logger;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: GatewaySocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewaySocket$voiceStateUpdate$1 extends o implements Function0<Unit> {
    public final /* synthetic */ Long $channelId;
    public final /* synthetic */ Long $guildId;
    public final /* synthetic */ String $preferredRegion;
    public final /* synthetic */ boolean $selfDeaf;
    public final /* synthetic */ boolean $selfMute;
    public final /* synthetic */ boolean $selfVideo;
    public final /* synthetic */ boolean $shouldIncludePreferredRegion;
    public final /* synthetic */ GatewaySocket this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GatewaySocket$voiceStateUpdate$1(GatewaySocket gatewaySocket, Long l, Long l2, boolean z2, boolean z3, String str, boolean z4, boolean z5) {
        super(0);
        this.this$0 = gatewaySocket;
        this.$guildId = l;
        this.$channelId = l2;
        this.$selfMute = z2;
        this.$selfDeaf = z3;
        this.$preferredRegion = str;
        this.$shouldIncludePreferredRegion = z4;
        this.$selfVideo = z5;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Object obj;
        GatewaySocket.Companion companion = GatewaySocket.Companion;
        Logger logger = this.this$0.logger;
        StringBuilder R = a.R("Sending voice state update for guild [");
        R.append(this.$guildId);
        R.append("] and channel [");
        R.append(this.$channelId);
        R.append("]. ");
        R.append("Muted: ");
        R.append(this.$selfMute);
        R.append(", deafened: ");
        R.append(this.$selfDeaf);
        R.append(", preferredRegion: ");
        R.append(this.$preferredRegion);
        GatewaySocket.Companion.log$default(companion, logger, R.toString(), false, 2, null);
        GatewaySocket gatewaySocket = this.this$0;
        Opcode opcode = Opcode.VOICE_STATE_UPDATE;
        if (this.$shouldIncludePreferredRegion) {
            obj = new OutgoingPayload.VoiceStateUpdate(this.$guildId, this.$channelId, this.$selfMute, this.$selfDeaf, this.$selfVideo, this.$preferredRegion);
        } else {
            obj = new OutgoingPayload.VoiceStateUpdateNoPreferredRegion(this.$guildId, this.$channelId, this.$selfMute, this.$selfDeaf, this.$selfVideo);
        }
        GatewaySocket.send$default(gatewaySocket, new Outgoing(opcode, obj), false, null, 6, null);
    }
}
