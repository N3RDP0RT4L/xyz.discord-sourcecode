package com.discord.gateway;

import d0.z.d.k;
import d0.z.d.m;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
/* compiled from: GatewaySocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\n\u001a\u00020\u00072\u0006\u0010\u0001\u001a\u00020\u00002\n\u0010\u0004\u001a\u00060\u0002j\u0002`\u00032\u0014\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u0000\u0018\u00010\u0005¢\u0006\u0004\b\b\u0010\t"}, d2 = {"", "p1", "Ljava/lang/Exception;", "Lkotlin/Exception;", "p2", "", "p3", "", "invoke", "(Ljava/lang/String;Ljava/lang/Exception;Ljava/util/Map;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class GatewaySocket$connect$4 extends k implements Function3<String, Exception, Map<String, ? extends String>, Unit> {
    public GatewaySocket$connect$4(GatewaySocket gatewaySocket) {
        super(3, gatewaySocket, GatewaySocket.class, "logError", "logError(Ljava/lang/String;Ljava/lang/Exception;Ljava/util/Map;)V", 0);
    }

    @Override // kotlin.jvm.functions.Function3
    public /* bridge */ /* synthetic */ Unit invoke(String str, Exception exc, Map<String, ? extends String> map) {
        invoke2(str, exc, (Map<String, String>) map);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str, Exception exc, Map<String, String> map) {
        m.checkNotNullParameter(str, "p1");
        m.checkNotNullParameter(exc, "p2");
        ((GatewaySocket) this.receiver).logError(str, exc, map);
    }
}
