package com.discord.gateway;

import andhook.lib.HookHelper;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.preference.PreferenceManager;
import com.discord.utilities.networking.Backoff;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Observable;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action1;
/* compiled from: GatewayDiscovery.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0000\u0018\u00002\u00020\u0001:\u0001\u001eBA\u0012\u0006\u0010\u001b\u001a\u00020\u001a\u0012\u0006\u0010\u0016\u001a\u00020\u0015\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\n¢\u0006\u0004\b\u001c\u0010\u001dJ5\u0010\b\u001a\u00020\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00022\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00040\u0002¢\u0006\u0004\b\b\u0010\tR\u001c\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00030\n8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\"\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0018\u0010\u0013\u001a\u0004\u0018\u00010\u00128\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0016\u0010\u0016\u001a\u00020\u00158\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0018\u0010\u0018\u001a\u0004\u0018\u00010\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019¨\u0006\u001f"}, d2 = {"Lcom/discord/gateway/GatewayDiscovery;", "", "Lkotlin/Function1;", "", "", "onSuccess", "", "onFailure", "discoverGatewayUrl", "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "Lrx/Observable;", "gatewayUrlProvider", "Lrx/Observable;", "Lcom/discord/utilities/networking/Backoff;", "backoff", "Lcom/discord/utilities/networking/Backoff;", "log", "Lkotlin/jvm/functions/Function1;", "Lrx/Subscription;", "gatewayUrlDiscoverySubscription", "Lrx/Subscription;", "Lrx/Scheduler;", "scheduler", "Lrx/Scheduler;", "gatewayUrl", "Ljava/lang/String;", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Landroid/content/Context;Lrx/Scheduler;Lcom/discord/utilities/networking/Backoff;Lkotlin/jvm/functions/Function1;Lrx/Observable;)V", "Cache", "gateway_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewayDiscovery {
    private final Backoff backoff;
    private String gatewayUrl;
    private Subscription gatewayUrlDiscoverySubscription;
    private final Observable<String> gatewayUrlProvider;
    private final Function1<String, Unit> log;
    private final Scheduler scheduler;

    /* compiled from: GatewayDiscovery.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\bÂ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\b\u0010\tJ\u0017\u0010\u000b\u001a\u00020\u00042\b\u0010\n\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\r\u001a\u00020\u00078\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0016\u0010\u0010\u001a\u00020\u000f8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lcom/discord/gateway/GatewayDiscovery$Cache;", "", "Landroid/content/Context;", "context", "", "init", "(Landroid/content/Context;)V", "", "getGatewayUrl", "()Ljava/lang/String;", "gatewayUrl", "setGatewayUrl", "(Ljava/lang/String;)V", Cache.GATEWAY_URL_CACHE_KEY, "Ljava/lang/String;", "Landroid/content/SharedPreferences;", "sharedPreferences", "Landroid/content/SharedPreferences;", HookHelper.constructorName, "()V", "gateway_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Cache {
        private static final String GATEWAY_URL_CACHE_KEY = "GATEWAY_URL_CACHE_KEY";
        public static final Cache INSTANCE = new Cache();
        private static SharedPreferences sharedPreferences;

        private Cache() {
        }

        public final String getGatewayUrl() {
            SharedPreferences sharedPreferences2 = sharedPreferences;
            if (sharedPreferences2 == null) {
                m.throwUninitializedPropertyAccessException("sharedPreferences");
            }
            return sharedPreferences2.getString(GATEWAY_URL_CACHE_KEY, null);
        }

        public final void init(Context context) {
            m.checkNotNullParameter(context, "context");
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            m.checkNotNullExpressionValue(defaultSharedPreferences, "PreferenceManager.getDef…haredPreferences(context)");
            sharedPreferences = defaultSharedPreferences;
        }

        public final void setGatewayUrl(String str) {
            SharedPreferences sharedPreferences2 = sharedPreferences;
            if (sharedPreferences2 == null) {
                m.throwUninitializedPropertyAccessException("sharedPreferences");
            }
            SharedPreferences.Editor edit = sharedPreferences2.edit();
            m.checkNotNullExpressionValue(edit, "editor");
            edit.putString(GATEWAY_URL_CACHE_KEY, str);
            edit.apply();
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public GatewayDiscovery(Context context, Scheduler scheduler, Backoff backoff, Function1<? super String, Unit> function1, Observable<String> observable) {
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(scheduler, "scheduler");
        m.checkNotNullParameter(backoff, "backoff");
        m.checkNotNullParameter(function1, "log");
        m.checkNotNullParameter(observable, "gatewayUrlProvider");
        this.scheduler = scheduler;
        this.backoff = backoff;
        this.log = function1;
        this.gatewayUrlProvider = observable;
        Cache cache = Cache.INSTANCE;
        cache.init(context);
        this.gatewayUrl = cache.getGatewayUrl();
    }

    public final void discoverGatewayUrl(Function1<? super String, Unit> function1, Function1<? super Throwable, Unit> function12) {
        String str;
        m.checkNotNullParameter(function1, "onSuccess");
        m.checkNotNullParameter(function12, "onFailure");
        if (this.backoff.hasReachedFailureThreshold() || (str = this.gatewayUrl) == null) {
            this.log.invoke("Discovering gateway url.");
            GatewayDiscovery$discoverGatewayUrl$1 gatewayDiscovery$discoverGatewayUrl$1 = new GatewayDiscovery$discoverGatewayUrl$1(this, function12);
            GatewayDiscovery$discoverGatewayUrl$2 gatewayDiscovery$discoverGatewayUrl$2 = new GatewayDiscovery$discoverGatewayUrl$2(this, gatewayDiscovery$discoverGatewayUrl$1, function1);
            Subscription subscription = this.gatewayUrlDiscoverySubscription;
            if (subscription != null) {
                subscription.unsubscribe();
            }
            Observable<String> I = this.gatewayUrlProvider.I(this.scheduler);
            final GatewayDiscovery$discoverGatewayUrl$3 gatewayDiscovery$discoverGatewayUrl$3 = new GatewayDiscovery$discoverGatewayUrl$3(gatewayDiscovery$discoverGatewayUrl$2);
            Action1<? super String> gatewayDiscovery$sam$rx_functions_Action1$0 = new Action1() { // from class: com.discord.gateway.GatewayDiscovery$sam$rx_functions_Action1$0
                @Override // rx.functions.Action1
                public final /* synthetic */ void call(Object obj) {
                    m.checkNotNullExpressionValue(Function1.this.invoke(obj), "invoke(...)");
                }
            };
            final GatewayDiscovery$discoverGatewayUrl$4 gatewayDiscovery$discoverGatewayUrl$4 = new GatewayDiscovery$discoverGatewayUrl$4(gatewayDiscovery$discoverGatewayUrl$1);
            this.gatewayUrlDiscoverySubscription = I.W(gatewayDiscovery$sam$rx_functions_Action1$0, new Action1() { // from class: com.discord.gateway.GatewayDiscovery$sam$rx_functions_Action1$0
                @Override // rx.functions.Action1
                public final /* synthetic */ void call(Object obj) {
                    m.checkNotNullExpressionValue(Function1.this.invoke(obj), "invoke(...)");
                }
            });
            return;
        }
        Function1<String, Unit> function13 = this.log;
        function13.invoke("Using sticky gateway url: " + str);
        function1.invoke(str);
    }
}
