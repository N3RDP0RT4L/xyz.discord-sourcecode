package com.discord.gateway;

import andhook.lib.xposed.ClassUtils;
import b.d.b.a.a;
import com.discord.gateway.GatewaySocket;
import com.discord.gateway.io.Outgoing;
import com.discord.gateway.io.OutgoingPayload;
import com.discord.gateway.opcodes.Opcode;
import com.discord.utilities.logging.Logger;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: GatewaySocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewaySocket$callConnect$1 extends o implements Function0<Unit> {
    public final /* synthetic */ long $channelId;
    public final /* synthetic */ GatewaySocket this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GatewaySocket$callConnect$1(GatewaySocket gatewaySocket, long j) {
        super(0);
        this.this$0 = gatewaySocket;
        this.$channelId = j;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        GatewaySocket.Companion companion = GatewaySocket.Companion;
        Logger logger = this.this$0.logger;
        StringBuilder R = a.R("Sending call connect sync on channel id: ");
        R.append(this.$channelId);
        R.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        GatewaySocket.Companion.log$default(companion, logger, R.toString(), false, 2, null);
        GatewaySocket.send$default(this.this$0, new Outgoing(Opcode.CALL_CONNECT, new OutgoingPayload.CallConnect(this.$channelId)), false, null, 6, null);
    }
}
