package com.discord.gateway;

import com.discord.gateway.GatewaySocket;
import com.discord.gateway.io.Outgoing;
import com.discord.gateway.io.OutgoingPayload;
import com.discord.gateway.opcodes.Opcode;
import com.discord.utilities.logging.Logger;
import com.google.gson.Gson;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
/* compiled from: GatewaySocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewaySocket$requestGuildMembers$1 extends o implements Function0<Unit> {
    public final /* synthetic */ List $guildIds;
    public final /* synthetic */ Integer $limit;
    public final /* synthetic */ String $query;
    public final /* synthetic */ List $userIds;
    public final /* synthetic */ GatewaySocket this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GatewaySocket$requestGuildMembers$1(GatewaySocket gatewaySocket, List list, String str, List list2, Integer num) {
        super(0);
        this.this$0 = gatewaySocket;
        this.$guildIds = list;
        this.$query = str;
        this.$userIds = list2;
        this.$limit = num;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        Gson gson;
        OutgoingPayload.GuildMembersRequest guildMembersRequest = new OutgoingPayload.GuildMembersRequest(this.$guildIds, this.$query, this.$userIds, this.$limit, false, 16, null);
        GatewaySocket.Companion companion = GatewaySocket.Companion;
        Logger logger = this.this$0.logger;
        GatewaySocket.Companion.log$default(companion, logger, "Sending guild member request: " + guildMembersRequest, false, 2, null);
        GatewaySocket gatewaySocket = this.this$0;
        Outgoing outgoing = new Outgoing(Opcode.REQUEST_GUILD_MEMBERS, guildMembersRequest);
        gson = GatewaySocket.gsonOmitNulls;
        m.checkNotNullExpressionValue(gson, "gsonOmitNulls");
        GatewaySocket.send$default(gatewaySocket, outgoing, false, gson, 2, null);
    }
}
