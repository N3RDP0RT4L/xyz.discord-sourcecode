package com.discord.gateway.rest;

import andhook.lib.HookHelper;
import android.content.Context;
import com.discord.models.domain.ModelGateway;
import com.discord.restapi.RequiredHeadersInterceptor;
import com.discord.restapi.RestAPIBuilder;
import com.discord.restapi.utils.RetryWithDelay;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import d0.t.n;
import d0.z.d.c0;
import d0.z.d.m;
import j0.p.a;
import java.util.List;
import java.util.Objects;
import kotlin.Metadata;
import okhttp3.Interceptor;
import rx.Observable;
/* compiled from: RestClient.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\bÀ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u001d\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ\u0013\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\u0004\b\u000b\u0010\fR\u0016\u0010\u000e\u001a\u00020\r8\u0002@\u0002X\u0082.¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lcom/discord/gateway/rest/RestClient;", "", "Lcom/discord/gateway/rest/RestConfig;", "restConfig", "Landroid/content/Context;", "context", "", "init", "(Lcom/discord/gateway/rest/RestConfig;Landroid/content/Context;)V", "Lrx/Observable;", "", "getGateway", "()Lrx/Observable;", "Lcom/discord/gateway/rest/RestApi;", "restApi", "Lcom/discord/gateway/rest/RestApi;", HookHelper.constructorName, "()V", "gateway_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RestClient {
    public static final RestClient INSTANCE = new RestClient();
    private static RestApi restApi;

    private RestClient() {
    }

    public final Observable<String> getGateway() {
        RetryWithDelay retryWithDelay = RetryWithDelay.INSTANCE;
        RestApi restApi2 = restApi;
        if (restApi2 == null) {
            m.throwUninitializedPropertyAccessException("restApi");
        }
        Observable<ModelGateway> X = restApi2.getGateway().X(a.c());
        m.checkNotNullExpressionValue(X, "restApi\n          .getGa…scribeOn(Schedulers.io())");
        Observable<String> F = RetryWithDelay.restRetry$default(retryWithDelay, X, 0L, null, null, 7, null).F(RestClient$getGateway$1.INSTANCE);
        m.checkNotNullExpressionValue(F, "restApi\n          .getGa…          .map { it.url }");
        return F;
    }

    public final void init(RestConfig restConfig, Context context) {
        m.checkNotNullParameter(restConfig, "restConfig");
        m.checkNotNullParameter(context, "context");
        String component1 = restConfig.component1();
        RequiredHeadersInterceptor.HeadersProvider component2 = restConfig.component2();
        List<Interceptor> component3 = restConfig.component3();
        RequiredHeadersInterceptor requiredHeadersInterceptor = new RequiredHeadersInterceptor(component2);
        PersistentCookieJar persistentCookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        c0 c0Var = new c0(2);
        c0Var.add(requiredHeadersInterceptor);
        Object[] array = component3.toArray(new Interceptor[0]);
        Objects.requireNonNull(array, "null cannot be cast to non-null type kotlin.Array<T>");
        c0Var.addSpread(array);
        restApi = (RestApi) RestAPIBuilder.build$default(new RestAPIBuilder(component1, persistentCookieJar), RestApi.class, false, 0L, n.listOf((Object[]) ((Interceptor[]) c0Var.toArray(new Interceptor[c0Var.size()]))), null, false, null, 118, null);
    }
}
