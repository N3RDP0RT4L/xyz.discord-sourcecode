package com.discord.gateway;

import andhook.lib.HookHelper;
import andhook.lib.xposed.ClassUtils;
import android.content.Context;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import b.i.d.c;
import b.i.d.e;
import com.discord.api.activity.Activity;
import com.discord.api.activity.ActivityType;
import com.discord.api.activity.ActivityTypeTypeAdapter;
import com.discord.api.presence.ClientStatus;
import com.discord.gateway.GatewaySocket;
import com.discord.gateway.GatewaySocketLogger;
import com.discord.gateway.io.Incoming;
import com.discord.gateway.io.IncomingParser;
import com.discord.gateway.io.Outgoing;
import com.discord.gateway.io.OutgoingPayload;
import com.discord.gateway.opcodes.Opcode;
import com.discord.gateway.rest.RestClient;
import com.discord.gateway.rest.RestConfig;
import com.discord.models.domain.Model;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.models.domain.ModelPayload;
import com.discord.utilities.analytics.Traits;
import com.discord.utilities.collections.ListenerCollection;
import com.discord.utilities.collections.ListenerCollectionSubject;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.networking.Backoff;
import com.discord.utilities.networking.NetworkMonitor;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.websocket.RawMessageHandler;
import com.discord.utilities.websocket.WebSocket;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import d0.t.h0;
import d0.t.n0;
import d0.t.u;
import d0.z.d.e0;
import d0.z.d.m;
import d0.z.d.o;
import j0.k.b;
import j0.l.a.r;
import j0.l.a.v1;
import j0.l.a.w1;
import j0.l.e.k;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLSocketFactory;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Scheduler;
import rx.Subscription;
import rx.functions.Action1;
/* compiled from: GatewaySocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ê\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 Þ\u00012\u00020\u0001:\fÞ\u0001ß\u0001à\u0001á\u0001â\u0001ã\u0001B¿\u0001\u0012\u0010\u0010«\u0001\u001a\u000b\u0012\u0007\u0012\u0005\u0018\u00010ª\u00010\u0002\u0012\u001e\u00102\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00010\\\u0012\u0004\u0012\u00020\u00030E\u0012\b\u0010Ë\u0001\u001a\u00030Ê\u0001\u0012\b\u0010Ô\u0001\u001a\u00030Ó\u0001\u0012\b\u0010Á\u0001\u001a\u00030À\u0001\u0012\b\u0010×\u0001\u001a\u00030Ö\u0001\u0012\b\u0010Ù\u0001\u001a\u00030Ø\u0001\u0012\b\u0010Û\u0001\u001a\u00030Ú\u0001\u0012\u0017\b\u0002\u0010É\u0001\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e\u0018\u00010E\u0012\f\b\u0002\u0010£\u0001\u001a\u0005\u0018\u00010¢\u0001\u0012\u0015\b\u0002\u0010¨\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00010\\\u0012\b\u0010Å\u0001\u001a\u00030Ä\u0001¢\u0006\u0006\bÜ\u0001\u0010Ý\u0001J\u001d\u0010\u0005\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u000f\u0010\u000b\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u000b\u0010\fJ\u000f\u0010\r\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\r\u0010\fJ\u0017\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0010\u0010\u0011J\u0017\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u000eH\u0002¢\u0006\u0004\b\u0012\u0010\u0011J\u001f\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0007H\u0002¢\u0006\u0004\b\u0016\u0010\u0017J\u000f\u0010\u0018\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0018\u0010\fJ\u000f\u0010\u0019\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\u0019\u0010\fJ\u0017\u0010\u001c\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u001aH\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010 \u001a\u00020\u00032\u0006\u0010\u001f\u001a\u00020\u001eH\u0002¢\u0006\u0004\b \u0010!J#\u0010%\u001a\u00020\u00032\b\b\u0002\u0010\"\u001a\u00020\u000e2\b\b\u0002\u0010$\u001a\u00020#H\u0002¢\u0006\u0004\b%\u0010&J\u0017\u0010(\u001a\u00020\u00032\u0006\u0010'\u001a\u00020#H\u0002¢\u0006\u0004\b(\u0010)J;\u0010.\u001a\u00020\u00032\b\u0010\u001f\u001a\u0004\u0018\u00010\u00012\b\u0010*\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0015\u001a\u00020\u00072\u0006\u0010+\u001a\u00020\u00072\u0006\u0010-\u001a\u00020,H\u0002¢\u0006\u0004\b.\u0010/J/\u00102\u001a\u00020\u00032\u0006\u00101\u001a\u0002002\u0006\u0010\u0015\u001a\u00020\u00072\u0006\u0010+\u001a\u00020\u00072\u0006\u0010-\u001a\u00020,H\u0002¢\u0006\u0004\b2\u00103JI\u00109\u001a\u00020\u00032\b\u00104\u001a\u0004\u0018\u00010\u00012.\u00108\u001a*\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00070605j\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000706`7H\u0002¢\u0006\u0004\b9\u0010:J\u000f\u0010;\u001a\u00020\u0003H\u0002¢\u0006\u0004\b;\u0010\fJ\u000f\u0010<\u001a\u00020\u0003H\u0002¢\u0006\u0004\b<\u0010\fJ'\u0010?\u001a\u00020\u00032\u0006\u0010=\u001a\u00020#2\u0006\u0010>\u001a\u00020\u00072\u0006\u0010\"\u001a\u00020\u000eH\u0002¢\u0006\u0004\b?\u0010@J'\u0010A\u001a\u00020\u00032\u0006\u0010=\u001a\u00020#2\u0006\u0010>\u001a\u00020\u00072\u0006\u0010\"\u001a\u00020\u000eH\u0002¢\u0006\u0004\bA\u0010@J\u000f\u0010B\u001a\u00020\u0003H\u0002¢\u0006\u0004\bB\u0010\fJ\u000f\u0010C\u001a\u00020\u0003H\u0002¢\u0006\u0004\bC\u0010\fJ\u000f\u0010D\u001a\u00020\u0003H\u0002¢\u0006\u0004\bD\u0010\fJ'\u0010G\u001a\u00020\u00032\u0016\u0010\u0004\u001a\u0012\u0012\u0006\u0012\u0004\u0018\u00010F\u0012\u0004\u0012\u00020\u0003\u0018\u00010EH\u0002¢\u0006\u0004\bG\u0010HJ\u0013\u0010I\u001a\u00020\u000e*\u00020\u001eH\u0002¢\u0006\u0004\bI\u0010JJ\u000f\u0010K\u001a\u00020\u0003H\u0002¢\u0006\u0004\bK\u0010\fJ\u000f\u0010L\u001a\u00020\u0003H\u0002¢\u0006\u0004\bL\u0010\fJ\u000f\u0010M\u001a\u00020\u0003H\u0002¢\u0006\u0004\bM\u0010\fJ+\u0010R\u001a\u00020\u00032\u0006\u0010\u001f\u001a\u00020N2\b\b\u0002\u0010O\u001a\u00020#2\b\b\u0002\u0010Q\u001a\u00020PH\u0002¢\u0006\u0004\bR\u0010SJ'\u0010V\u001a\u00020\u00032\n\b\u0002\u0010T\u001a\u0004\u0018\u00010#2\n\b\u0002\u0010U\u001a\u0004\u0018\u00010#H\u0002¢\u0006\u0004\bV\u0010WJ9\u0010^\u001a\u00020\u00032\u0006\u0010X\u001a\u00020\u000e2\n\u0010[\u001a\u00060Yj\u0002`Z2\u0014\u0010]\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e\u0018\u00010\\H\u0002¢\u0006\u0004\b^\u0010_J\u0017\u0010a\u001a\u00020\u00032\u0006\u0010`\u001a\u00020#H\u0002¢\u0006\u0004\ba\u0010)J\r\u0010b\u001a\u00020#¢\u0006\u0004\bb\u0010cJ\r\u0010\u0010\u001a\u00020\u0003¢\u0006\u0004\b\u0010\u0010\fJ\u0017\u0010e\u001a\u00020\u00032\b\b\u0002\u0010d\u001a\u00020#¢\u0006\u0004\be\u0010)J\r\u0010f\u001a\u00020\u0003¢\u0006\u0004\bf\u0010\fJ+\u0010i\u001a\u00020\u00032\u0006\u0010g\u001a\u00020,2\n\b\u0002\u0010\"\u001a\u0004\u0018\u00010\u000e2\b\b\u0002\u0010h\u001a\u00020#¢\u0006\u0004\bi\u0010jJA\u0010r\u001a\u00020\u00032\b\u0010l\u001a\u0004\u0018\u00010k2\n\b\u0002\u0010m\u001a\u0004\u0018\u00010,2\u0010\b\u0002\u0010p\u001a\n\u0012\u0004\u0012\u00020o\u0018\u00010n2\n\b\u0002\u0010q\u001a\u0004\u0018\u00010#¢\u0006\u0004\br\u0010sJW\u0010}\u001a\u00020\u00032\u000e\u0010u\u001a\n\u0018\u00010,j\u0004\u0018\u0001`t2\u000e\u0010w\u001a\n\u0018\u00010,j\u0004\u0018\u0001`v2\u0006\u0010x\u001a\u00020#2\u0006\u0010y\u001a\u00020#2\u0006\u0010z\u001a\u00020#2\b\u0010{\u001a\u0004\u0018\u00010\u000e2\u0006\u0010|\u001a\u00020#¢\u0006\u0004\b}\u0010~J\r\u0010\u007f\u001a\u00020\u0003¢\u0006\u0004\b\u007f\u0010\fJ?\u0010\u0081\u0001\u001a\u00020\u00032\u0007\u0010\u0080\u0001\u001a\u00020\u000e2\n\u0010w\u001a\u00060,j\u0002`v2\u000e\u0010u\u001a\n\u0018\u00010,j\u0004\u0018\u0001`t2\b\u0010{\u001a\u0004\u0018\u00010\u000e¢\u0006\u0006\b\u0081\u0001\u0010\u0082\u0001J\u001d\u0010\u0085\u0001\u001a\u00020\u00032\f\u0010\u0084\u0001\u001a\u00070\u000ej\u0003`\u0083\u0001¢\u0006\u0005\b\u0085\u0001\u0010\u0011J\u001d\u0010\u0086\u0001\u001a\u00020\u00032\f\u0010\u0084\u0001\u001a\u00070\u000ej\u0003`\u0083\u0001¢\u0006\u0005\b\u0086\u0001\u0010\u0011J\u0018\u0010\u0087\u0001\u001a\u00020\u00032\u0007\u0010\u0084\u0001\u001a\u00020\u000e¢\u0006\u0005\b\u0087\u0001\u0010\u0011JS\u0010\u008d\u0001\u001a\u00020\u00032\r\u0010\u0088\u0001\u001a\b\u0012\u0004\u0012\u00020,0n2\u000b\b\u0002\u0010\u0089\u0001\u001a\u0004\u0018\u00010\u000e2\u0016\b\u0002\u0010\u008b\u0001\u001a\u000f\u0012\t\u0012\u00070,j\u0003`\u008a\u0001\u0018\u00010n2\u000b\b\u0002\u0010\u008c\u0001\u001a\u0004\u0018\u00010\u0007H\u0007¢\u0006\u0006\b\u008d\u0001\u0010\u008e\u0001J\u001c\u0010\u008f\u0001\u001a\u00020\u00032\n\u0010w\u001a\u00060,j\u0002`v¢\u0006\u0006\b\u008f\u0001\u0010\u0090\u0001J&\u0010\u0093\u0001\u001a\u00020\u00032\n\u0010u\u001a\u00060,j\u0002`t2\b\u0010\u0092\u0001\u001a\u00030\u0091\u0001¢\u0006\u0006\b\u0093\u0001\u0010\u0094\u0001J\u001a\u0010\u0097\u0001\u001a\u00020\u00032\b\u0010\u0096\u0001\u001a\u00030\u0095\u0001¢\u0006\u0006\b\u0097\u0001\u0010\u0098\u0001J\u000f\u0010\u0099\u0001\u001a\u00020\u0003¢\u0006\u0005\b\u0099\u0001\u0010\fR!\u0010\u009c\u0001\u001a\n\u0012\u0005\u0012\u00030\u009b\u00010\u009a\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009c\u0001\u0010\u009d\u0001R\u001a\u0010\u009f\u0001\u001a\u00030\u009e\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009f\u0001\u0010 \u0001R/\u00102\u001a\u001a\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00010\\\u0012\u0004\u0012\u00020\u00030E8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b2\u0010¡\u0001R\u001c\u0010£\u0001\u001a\u0005\u0018\u00010¢\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b£\u0001\u0010¤\u0001R\u001a\u0010¦\u0001\u001a\u00030¥\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b¦\u0001\u0010§\u0001R%\u0010¨\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00010\\8\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b¨\u0001\u0010©\u0001R\"\u0010«\u0001\u001a\u000b\u0012\u0007\u0012\u0005\u0018\u00010ª\u00010\u00028\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b«\u0001\u0010¬\u0001R\u001b\u0010\u00ad\u0001\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u00ad\u0001\u0010®\u0001R\u0019\u0010¯\u0001\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b¯\u0001\u0010°\u0001R\u0019\u0010±\u0001\u001a\u00020,8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b±\u0001\u0010²\u0001R\u001a\u0010³\u0001\u001a\u00030¥\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b³\u0001\u0010§\u0001R\u0019\u0010´\u0001\u001a\u00020,8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b´\u0001\u0010²\u0001R\"\u0010¶\u0001\u001a\u000b\u0018\u00010\u000ej\u0005\u0018\u0001`µ\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b¶\u0001\u0010®\u0001R\u0019\u0010·\u0001\u001a\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b·\u0001\u0010¸\u0001R\u0019\u0010¹\u0001\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b¹\u0001\u0010°\u0001R&\u0010»\u0001\u001a\n\u0012\u0005\u0012\u00030\u009b\u00010º\u00018\u0006@\u0006¢\u0006\u0010\n\u0006\b»\u0001\u0010¼\u0001\u001a\u0006\b½\u0001\u0010¾\u0001R\u0017\u0010\b\u001a\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\b\u0010¸\u0001R\u0019\u0010¿\u0001\u001a\u00020,8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b¿\u0001\u0010²\u0001R\u001a\u0010Á\u0001\u001a\u00030À\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÁ\u0001\u0010Â\u0001R\u0019\u0010Ã\u0001\u001a\u00020\u00078\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bÃ\u0001\u0010¸\u0001R\u001a\u0010Å\u0001\u001a\u00030Ä\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÅ\u0001\u0010Æ\u0001R\u001b\u0010Ç\u0001\u001a\u0004\u0018\u00010F8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bÇ\u0001\u0010È\u0001R'\u0010É\u0001\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e\u0018\u00010E8\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÉ\u0001\u0010¡\u0001R\u001a\u0010Ë\u0001\u001a\u00030Ê\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bË\u0001\u0010Ì\u0001R\u001a\u0010Í\u0001\u001a\u00030¥\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bÍ\u0001\u0010§\u0001R\u001a\u0010Ï\u0001\u001a\u00030Î\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÏ\u0001\u0010Ð\u0001R\u0017\u0010T\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\bT\u0010°\u0001R\u0019\u0010Ñ\u0001\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bÑ\u0001\u0010°\u0001R\u0019\u0010Ò\u0001\u001a\u00020,8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\bÒ\u0001\u0010²\u0001R\u0017\u0010U\u001a\u00020#8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\bU\u0010°\u0001R\u001a\u0010Ô\u0001\u001a\u00030Ó\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÔ\u0001\u0010Õ\u0001¨\u0006ä\u0001"}, d2 = {"Lcom/discord/gateway/GatewaySocket;", "", "Lkotlin/Function0;", "", "callback", "schedule", "(Lkotlin/jvm/functions/Function0;)V", "", "seq", "heartbeat", "(I)V", "discoveryFailed", "()V", "discover", "", "gatewayUrl", "connect", "(Ljava/lang/String;)V", "handleWebSocketOpened", "Ljava/io/InputStreamReader;", "reader", "compressedByteSize", "handleWebSocketMessage", "(Ljava/io/InputStreamReader;I)V", "handleHeartbeat", "handleWebSocketError", "Lcom/discord/utilities/websocket/WebSocket$Closed;", "closed", "handleWebSocketClose", "(Lcom/discord/utilities/websocket/WebSocket$Closed;)V", "Lcom/discord/models/domain/ModelPayload$Hello;", "data", "handleHello", "(Lcom/discord/models/domain/ModelPayload$Hello;)V", ModelAuditLogEntry.CHANGE_KEY_REASON, "", "resetSession", "handleReconnect", "(Ljava/lang/String;Z)V", "canResume", "handleInvalidSession", "(Z)V", "type", "uncompressedByteSize", "", "unpackDurationMs", "handleDispatch", "(Ljava/lang/Object;Ljava/lang/String;IIJ)V", "Lcom/discord/models/domain/ModelPayload;", "payload", "trackReadyPayload", "(Lcom/discord/models/domain/ModelPayload;IIJ)V", "trace", "Ljava/util/ArrayList;", "Lkotlin/Pair;", "Lkotlin/collections/ArrayList;", "result", "flattenTraces", "(Ljava/lang/Object;Ljava/util/ArrayList;)V", "handleHeartbeatAck", "handleHeartbeatTimeout", "wasClean", ModelAuditLogEntry.CHANGE_KEY_CODE, "handleClose", "(ZILjava/lang/String;)V", ModelAuditLogEntry.CHANGE_KEY_PERMISSIONS_RESET, "startHeartbeater", "stopHeartbeater", "clearHelloTimeout", "Lkotlin/Function1;", "Lcom/discord/utilities/websocket/WebSocket;", "cleanup", "(Lkotlin/jvm/functions/Function1;)V", "getConnectionPath", "(Lcom/discord/models/domain/ModelPayload$Hello;)Ljava/lang/String;", "doResume", "doIdentify", "doResumeOrIdentify", "Lcom/discord/gateway/io/Outgoing;", "checkSessionEstablished", "Lcom/google/gson/Gson;", "gson", "send", "(Lcom/discord/gateway/io/Outgoing;ZLcom/google/gson/Gson;)V", "connected", "connectionReady", "handleConnected", "(Ljava/lang/Boolean;Ljava/lang/Boolean;)V", "message", "Ljava/lang/Exception;", "Lkotlin/Exception;", "exception", "", "metadata", "logError", "(Ljava/lang/String;Ljava/lang/Exception;Ljava/util/Map;)V", "isConnected", "handleDeviceConnectivityChange", "isSessionEstablished", "()Z", "clean", "close", "resetOnError", "timeout", "shouldResetBackoff", "expeditedHeartbeat", "(JLjava/lang/String;Z)V", "Lcom/discord/api/presence/ClientStatus;", "status", "since", "", "Lcom/discord/api/activity/Activity;", "activities", "afk", "presenceUpdate", "(Lcom/discord/api/presence/ClientStatus;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Boolean;)V", "Lcom/discord/primitives/GuildId;", "guildId", "Lcom/discord/primitives/ChannelId;", "channelId", "selfMute", "selfDeaf", "selfVideo", "preferredRegion", "shouldIncludePreferredRegion", "voiceStateUpdate", "(Ljava/lang/Long;Ljava/lang/Long;ZZZLjava/lang/String;Z)V", "voiceServerPing", "streamType", "streamCreate", "(Ljava/lang/String;JLjava/lang/Long;Ljava/lang/String;)V", "Lcom/discord/primitives/StreamKey;", "streamKey", "streamWatch", "streamDelete", "streamPing", "guildIds", "query", "Lcom/discord/primitives/UserId;", "userIds", "limit", "requestGuildMembers", "(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;)V", "callConnect", "(J)V", "Lcom/discord/gateway/io/OutgoingPayload$GuildSubscriptions;", "guildSubscriptions", "updateGuildSubscriptions", "(JLcom/discord/gateway/io/OutgoingPayload$GuildSubscriptions;)V", "Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;", "request", "requestApplicationCommands", "(Lcom/discord/gateway/io/OutgoingPayload$ApplicationCommandRequest;)V", "simulateReconnectForTesting", "Lcom/discord/utilities/collections/ListenerCollectionSubject;", "Lcom/discord/gateway/GatewaySocket$Listener;", "listenerSubject", "Lcom/discord/utilities/collections/ListenerCollectionSubject;", "Lcom/discord/utilities/networking/Backoff;", "gatewayBackoff", "Lcom/discord/utilities/networking/Backoff;", "Lkotlin/jvm/functions/Function1;", "Ljavax/net/ssl/SSLSocketFactory;", "sslSocketFactory", "Ljavax/net/ssl/SSLSocketFactory;", "Lcom/discord/gateway/GatewaySocket$Timer;", "helloTimeout", "Lcom/discord/gateway/GatewaySocket$Timer;", "identifyProperties", "Ljava/util/Map;", "Lcom/discord/gateway/GatewaySocket$IdentifyData;", "identifyDataProvider", "Lkotlin/jvm/functions/Function0;", "token", "Ljava/lang/String;", "heartbeatAck", "Z", "connectionStartTime", "J", "heartbeatExpeditedTimeout", "identifyStartTime", "Lcom/discord/primitives/SessionId;", "sessionId", "connectionState", "I", "hasConnectedOnce", "Lcom/discord/utilities/collections/ListenerCollection;", "listeners", "Lcom/discord/utilities/collections/ListenerCollection;", "getListeners", "()Lcom/discord/utilities/collections/ListenerCollection;", "heartbeatAckTimeMostRecent", "Lcom/discord/utilities/logging/Logger;", "logger", "Lcom/discord/utilities/logging/Logger;", "replayedEvents", "Lcom/discord/gateway/GatewaySocketLogger;", "gatewaySocketLogger", "Lcom/discord/gateway/GatewaySocketLogger;", "webSocket", "Lcom/discord/utilities/websocket/WebSocket;", "gatewayUrlTransform", "Lcom/discord/gateway/GatewayEventHandler;", "eventHandler", "Lcom/discord/gateway/GatewayEventHandler;", "heartbeater", "Lcom/discord/gateway/GatewayDiscovery;", "gatewayDiscovery", "Lcom/discord/gateway/GatewayDiscovery;", "nextReconnectIsImmediate", "heartbeatInterval", "Lrx/Scheduler;", "scheduler", "Lrx/Scheduler;", "Lcom/discord/utilities/networking/NetworkMonitor;", "networkMonitor", "Lcom/discord/gateway/rest/RestConfig;", "restConfig", "Landroid/content/Context;", "context", HookHelper.constructorName, "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lcom/discord/gateway/GatewayEventHandler;Lrx/Scheduler;Lcom/discord/utilities/logging/Logger;Lcom/discord/utilities/networking/NetworkMonitor;Lcom/discord/gateway/rest/RestConfig;Landroid/content/Context;Lkotlin/jvm/functions/Function1;Ljavax/net/ssl/SSLSocketFactory;Ljava/util/Map;Lcom/discord/gateway/GatewaySocketLogger;)V", "Companion", "DefaultListener", "IdentifyData", "Listener", "SizeRecordingInputStreamReader", "Timer", "gateway_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewaySocket {
    private static final long CLIENT_CAPABILITIES = 95;
    private static final int CLOSE_CODE_CLEAN = 1000;
    private static final int CLOSE_CODE_DIRTY = 4000;
    private static final int CLOSE_CODE_UNAUTHORIZED = 4004;
    private static final boolean COMPRESS_DATA = true;
    private static final int CONNECTED = 5;
    private static final int CONNECTING = 2;
    private static final long DEDUPE_USER_OBJECTS = 16;
    private static final int DISCONNECTED = 0;
    private static final int DISCOVERING = 1;
    private static final String GATEWAY_COMPRESSION = "zlib-stream";
    private static final String GATEWAY_ENCODING = "json";
    private static final int GATEWAY_URL_RESET_THRESHOLD = 4;
    private static final int GATEWAY_VERSION = 9;
    private static final int HEARTBEAT_MAX_RESUME_THRESHOLD = 180000;
    private static final long HELLO_TIMEOUT = 20000;
    private static final int IDENTIFYING = 3;
    private static final int LARGE_GUILD_THRESHOLD = 100;
    private static final long LAZY_USER_NOTES = 1;
    private static final long MULTIPLE_GUILD_EXPERIMENT_POPULATIONS = 64;
    private static final long NO_AFFINE_USER_IDS = 2;
    private static final int RESUMING = 4;
    private static final long VERSIONED_READ_STATES = 4;
    private static final long VERSIONED_USER_GUILD_SETTINGS = 8;
    private static final Gson gsonIncludeNulls;
    private static final Gson gsonOmitNulls;
    private boolean connected;
    private boolean connectionReady;
    private long connectionStartTime;
    private int connectionState;
    private final GatewayEventHandler eventHandler;
    private final Backoff gatewayBackoff;
    private final GatewayDiscovery gatewayDiscovery;
    private final GatewaySocketLogger gatewaySocketLogger;
    private final Function1<String, String> gatewayUrlTransform;
    private boolean hasConnectedOnce;
    private boolean heartbeatAck;
    private long heartbeatAckTimeMostRecent;
    private Timer heartbeatExpeditedTimeout;
    private long heartbeatInterval;
    private Timer heartbeater;
    private Timer helloTimeout;
    private final Function0<IdentifyData> identifyDataProvider;
    private final Map<String, Object> identifyProperties;
    private long identifyStartTime;
    private final ListenerCollectionSubject<Listener> listenerSubject;
    private final ListenerCollection<Listener> listeners;
    private final Logger logger;
    private boolean nextReconnectIsImmediate;
    private int replayedEvents;
    private final Scheduler scheduler;
    private int seq;
    private String sessionId;
    private final SSLSocketFactory sslSocketFactory;
    private String token;
    private final Function1<Map<String, ? extends Object>, Unit> trackReadyPayload;
    private WebSocket webSocket;
    public static final Companion Companion = new Companion(null);
    private static final Set<String> EXPECTED_NULL_DATA_EVENTS = n0.setOf((Object[]) new String[]{"USER_SUBSCRIPTIONS_UPDATE", "USER_PAYMENT_SOURCES_UPDATE"});
    private static final Clock clock = ClockFactory.get();

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "message", "", "invoke", "(Ljava/lang/String;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.gateway.GatewaySocket$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<String, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(String str) {
            invoke2(str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(String str) {
            m.checkNotNullParameter(str, "message");
            Companion.log$default(GatewaySocket.Companion, GatewaySocket.this.logger, str, false, 2, null);
        }
    }

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u000b\n\u0002\b\u0005\u0010\u0005\u001a\n \u0001*\u0004\u0018\u00010\u00000\u00002\u000e\u0010\u0002\u001a\n \u0001*\u0004\u0018\u00010\u00000\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "kotlin.jvm.PlatformType", "isConnected", NotificationCompat.CATEGORY_CALL, "(Ljava/lang/Boolean;)Ljava/lang/Boolean;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.gateway.GatewaySocket$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2<T, R> implements b<Boolean, Boolean> {
        public static final AnonymousClass2 INSTANCE = new AnonymousClass2();

        public final Boolean call(Boolean bool) {
            return bool;
        }
    }

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\"\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b7\u00108J#\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006J%\u0010\r\u001a\u00020\f*\u00020\u00072\u0006\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\u000b\u001a\u00020\nH\u0002¢\u0006\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0012\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R\u0016\u0010\u0014\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0014\u0010\u0013R\u0016\u0010\u0015\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0015\u0010\u0013R\u0016\u0010\u0016\u001a\u00020\n8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u0016\u0010\u0018\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0018\u0010\u0013R\u0016\u0010\u0019\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u0019\u0010\u0013R\u0016\u0010\u001a\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001a\u0010\u0010R\u0016\u0010\u001b\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001b\u0010\u0013R\u0016\u0010\u001c\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\u001c\u0010\u0013R\u001c\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\b0\u001d8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u0016\u0010 \u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b \u0010!R\u0016\u0010\"\u001a\u00020\b8\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b\"\u0010!R\u0016\u0010#\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b#\u0010\u0013R\u0016\u0010$\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b$\u0010\u0013R\u0016\u0010%\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b%\u0010\u0013R\u0016\u0010&\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b&\u0010\u0010R\u0016\u0010'\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b'\u0010\u0013R\u0016\u0010(\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b(\u0010\u0013R\u0016\u0010)\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b)\u0010\u0010R\u0016\u0010*\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b*\u0010\u0010R\u0016\u0010+\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b+\u0010\u0010R\u0016\u0010,\u001a\u00020\u00118\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b,\u0010\u0013R\u0016\u0010-\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b-\u0010\u0010R\u0016\u0010.\u001a\u00020\u00028\u0002@\u0002X\u0082T¢\u0006\u0006\n\u0004\b.\u0010\u0010R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b0\u00101R\u001e\u00104\u001a\n 3*\u0004\u0018\u000102028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b4\u00105R\u001e\u00106\u001a\n 3*\u0004\u0018\u000102028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b6\u00105¨\u00069"}, d2 = {"Lcom/discord/gateway/GatewaySocket$Companion;", "", "", "startTime", "currentTime", "getDelay", "(JLjava/lang/Long;)J", "Lcom/discord/utilities/logging/Logger;", "", "message", "", "breadcrumb", "", "log", "(Lcom/discord/utilities/logging/Logger;Ljava/lang/String;Z)V", "CLIENT_CAPABILITIES", "J", "", "CLOSE_CODE_CLEAN", "I", "CLOSE_CODE_DIRTY", "CLOSE_CODE_UNAUTHORIZED", "COMPRESS_DATA", "Z", "CONNECTED", "CONNECTING", "DEDUPE_USER_OBJECTS", "DISCONNECTED", "DISCOVERING", "", "EXPECTED_NULL_DATA_EVENTS", "Ljava/util/Set;", "GATEWAY_COMPRESSION", "Ljava/lang/String;", "GATEWAY_ENCODING", "GATEWAY_URL_RESET_THRESHOLD", "GATEWAY_VERSION", "HEARTBEAT_MAX_RESUME_THRESHOLD", "HELLO_TIMEOUT", "IDENTIFYING", "LARGE_GUILD_THRESHOLD", "LAZY_USER_NOTES", "MULTIPLE_GUILD_EXPERIMENT_POPULATIONS", "NO_AFFINE_USER_IDS", "RESUMING", "VERSIONED_READ_STATES", "VERSIONED_USER_GUILD_SETTINGS", "Lcom/discord/utilities/time/Clock;", "clock", "Lcom/discord/utilities/time/Clock;", "Lcom/google/gson/Gson;", "kotlin.jvm.PlatformType", "gsonIncludeNulls", "Lcom/google/gson/Gson;", "gsonOmitNulls", HookHelper.constructorName, "()V", "gateway_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        private final long getDelay(long j, Long l) {
            return (l != null ? l.longValue() : ClockFactory.get().currentTimeMillis()) - j;
        }

        public static /* synthetic */ long getDelay$default(Companion companion, long j, Long l, int i, Object obj) {
            if ((i & 2) != 0) {
                l = null;
            }
            return companion.getDelay(j, l);
        }

        /* JADX INFO: Access modifiers changed from: private */
        public final void log(Logger logger, String str, boolean z2) {
            Logger.v$default(logger, a.v("[GatewaySocket] ", str), null, 2, null);
            if (z2) {
                logger.recordBreadcrumb("Gateway [" + str + ']', "log");
            }
        }

        public static /* synthetic */ void log$default(Companion companion, Logger logger, String str, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                z2 = true;
            }
            companion.log(logger, str, z2);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\b&\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\n\u0010\u000bJ\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\b\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\t\u0010\u0006¨\u0006\f"}, d2 = {"Lcom/discord/gateway/GatewaySocket$DefaultListener;", "Lcom/discord/gateway/GatewaySocket$Listener;", "Lcom/discord/gateway/GatewaySocket;", "gatewaySocket", "", "onConnecting", "(Lcom/discord/gateway/GatewaySocket;)V", "onConnected", "onHello", "onDisconnected", HookHelper.constructorName, "()V", "gateway_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class DefaultListener implements Listener {
        @Override // com.discord.gateway.GatewaySocket.Listener
        public void onConnected(GatewaySocket gatewaySocket) {
            m.checkNotNullParameter(gatewaySocket, "gatewaySocket");
        }

        @Override // com.discord.gateway.GatewaySocket.Listener
        public void onConnecting(GatewaySocket gatewaySocket) {
            m.checkNotNullParameter(gatewaySocket, "gatewaySocket");
        }

        @Override // com.discord.gateway.GatewaySocket.Listener
        public void onDisconnected(GatewaySocket gatewaySocket) {
            m.checkNotNullParameter(gatewaySocket, "gatewaySocket");
        }

        @Override // com.discord.gateway.GatewaySocket.Listener
        public void onHello(GatewaySocket gatewaySocket) {
            m.checkNotNullParameter(gatewaySocket, "gatewaySocket");
        }
    }

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\u0004\b\f\u0010\rR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u000e"}, d2 = {"Lcom/discord/gateway/GatewaySocket$IdentifyData;", "", "", "token", "Ljava/lang/String;", "getToken", "()Ljava/lang/String;", "Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;", "clientState", "Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;", "getClientState", "()Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/gateway/io/OutgoingPayload$IdentifyClientState;)V", "gateway_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class IdentifyData {
        private final OutgoingPayload.IdentifyClientState clientState;
        private final String token;

        public IdentifyData(String str, OutgoingPayload.IdentifyClientState identifyClientState) {
            m.checkNotNullParameter(str, "token");
            this.token = str;
            this.clientState = identifyClientState;
        }

        public final OutgoingPayload.IdentifyClientState getClientState() {
            return this.clientState;
        }

        public final String getToken() {
            return this.token;
        }
    }

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\bf\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\u0007\u0010\u0006J\u0017\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\b\u0010\u0006J\u0017\u0010\t\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H&¢\u0006\u0004\b\t\u0010\u0006¨\u0006\n"}, d2 = {"Lcom/discord/gateway/GatewaySocket$Listener;", "", "Lcom/discord/gateway/GatewaySocket;", "gatewaySocket", "", "onConnecting", "(Lcom/discord/gateway/GatewaySocket;)V", "onConnected", "onHello", "onDisconnected", "gateway_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public interface Listener {
        void onConnected(GatewaySocket gatewaySocket);

        void onConnecting(GatewaySocket gatewaySocket);

        void onDisconnected(GatewaySocket gatewaySocket);

        void onHello(GatewaySocket gatewaySocket);
    }

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0019\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\r\u001a\u00020\f\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0007¢\u0006\u0004\b\u0017\u0010\u0018J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J'\u0010\n\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\"\u0010\u0011\u001a\u00020\u00078\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016¨\u0006\u0019"}, d2 = {"Lcom/discord/gateway/GatewaySocket$SizeRecordingInputStreamReader;", "Ljava/io/Reader;", "", "close", "()V", "", "buffer", "", "offset", "len", "read", "([CII)I", "Ljava/io/InputStreamReader;", "source", "Ljava/io/InputStreamReader;", "getSource", "()Ljava/io/InputStreamReader;", "size", "I", "getSize", "()I", "setSize", "(I)V", HookHelper.constructorName, "(Ljava/io/InputStreamReader;I)V", "gateway_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class SizeRecordingInputStreamReader extends Reader {
        private int size;
        private final InputStreamReader source;

        public SizeRecordingInputStreamReader(InputStreamReader inputStreamReader, int i) {
            m.checkNotNullParameter(inputStreamReader, "source");
            this.source = inputStreamReader;
            this.size = i;
        }

        @Override // java.io.Reader, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.source.close();
        }

        public final int getSize() {
            return this.size;
        }

        public final InputStreamReader getSource() {
            return this.source;
        }

        @Override // java.io.Reader
        public int read(char[] cArr, int i, int i2) {
            m.checkNotNullParameter(cArr, "buffer");
            int read = this.source.read(cArr, i, i2);
            if (read != -1) {
                this.size += read;
            }
            return read;
        }

        public final void setSize(int i) {
            this.size = i;
        }

        public /* synthetic */ SizeRecordingInputStreamReader(InputStreamReader inputStreamReader, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(inputStreamReader, (i2 & 2) != 0 ? 0 : i);
        }
    }

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0013\u001a\u00020\u0012¢\u0006\u0004\b\u0019\u0010\u001aJ%\u0010\u0007\u001a\u00020\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0016¢\u0006\u0004\b\u0007\u0010\bJ#\u0010\u000b\u001a\u00020\u00032\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\n\u001a\u00020\u0005¢\u0006\u0004\b\u000b\u0010\bJ\u000f\u0010\f\u001a\u00020\u0003H\u0016¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\f\u001a\u00020\u00032\u0010\b\u0002\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u0002¢\u0006\u0004\b\f\u0010\u000eR\u0018\u0010\u0010\u001a\u0004\u0018\u00010\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0013\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u0013\u0010\u0018\u001a\u00020\u00158F@\u0006¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u001b"}, d2 = {"Lcom/discord/gateway/GatewaySocket$Timer;", "Lcom/discord/utilities/networking/Backoff$Scheduler;", "Lkotlin/Function0;", "", "action", "", "delayMs", "schedule", "(Lkotlin/jvm/functions/Function0;J)V", "callback", "delayMillis", "postInterval", "cancel", "()V", "(Lkotlin/jvm/functions/Function0;)V", "Lrx/Subscription;", Traits.Payment.Type.SUBSCRIPTION, "Lrx/Subscription;", "Lrx/Scheduler;", "scheduler", "Lrx/Scheduler;", "", "getPending", "()Z", "pending", HookHelper.constructorName, "(Lrx/Scheduler;)V", "gateway_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Timer implements Backoff.Scheduler {
        private final Scheduler scheduler;
        private Subscription subscription;

        public Timer(Scheduler scheduler) {
            m.checkNotNullParameter(scheduler, "scheduler");
            this.scheduler = scheduler;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ void cancel$default(Timer timer, Function0 function0, int i, Object obj) {
            if ((i & 1) != 0) {
                function0 = null;
            }
            timer.cancel(function0);
        }

        @Override // com.discord.utilities.networking.Backoff.Scheduler
        public void cancel() {
            cancel(null);
        }

        public final boolean getPending() {
            return this.subscription != null;
        }

        public final void postInterval(final Function0<Unit> function0, long j) {
            m.checkNotNullParameter(function0, "callback");
            cancel();
            this.subscription = Observable.E(j, j, TimeUnit.MILLISECONDS, j0.p.a.a()).I(this.scheduler).W(new Action1<Long>() { // from class: com.discord.gateway.GatewaySocket$Timer$postInterval$1
                public final void call(Long l) {
                    Function0.this.invoke();
                }
            }, GatewaySocket$Timer$postInterval$2.INSTANCE);
        }

        @Override // com.discord.utilities.networking.Backoff.Scheduler
        public void schedule(final Function0<Unit> function0, long j) {
            m.checkNotNullParameter(function0, "action");
            cancel();
            this.subscription = Observable.d0(j, TimeUnit.MILLISECONDS).I(this.scheduler).W(new Action1<Long>() { // from class: com.discord.gateway.GatewaySocket$Timer$schedule$1
                public final void call(Long l) {
                    try {
                        function0.invoke();
                    } finally {
                        GatewaySocket.Timer.this.subscription = null;
                    }
                }
            }, GatewaySocket$Timer$schedule$2.INSTANCE);
        }

        public final void cancel(Function0<Unit> function0) {
            Subscription subscription = this.subscription;
            if (subscription != null) {
                if (subscription != null) {
                    subscription.unsubscribe();
                }
                this.subscription = null;
                if (function0 != null) {
                    function0.invoke();
                }
            }
        }
    }

    @Metadata(bv = {1, 0, 3}, d1 = {}, d2 = {}, k = 3, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            GatewaySocketLogger.LogLevel.values();
            int[] iArr = new int[2];
            $EnumSwitchMapping$0 = iArr;
            iArr[GatewaySocketLogger.LogLevel.NONE.ordinal()] = 1;
            iArr[GatewaySocketLogger.LogLevel.VERBOSE.ordinal()] = 2;
            Opcode.values();
            int[] iArr2 = new int[20];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[Opcode.HELLO.ordinal()] = 1;
            iArr2[Opcode.RECONNECT.ordinal()] = 2;
            iArr2[Opcode.INVALID_SESSION.ordinal()] = 3;
            iArr2[Opcode.HEARTBEAT.ordinal()] = 4;
            iArr2[Opcode.HEARTBEAT_ACK.ordinal()] = 5;
            iArr2[Opcode.DISPATCH.ordinal()] = 6;
        }
    }

    static {
        e eVar = new e();
        eVar.g = true;
        c cVar = c.m;
        eVar.c = cVar;
        eVar.b(ActivityType.class, new ActivityTypeTypeAdapter());
        gsonIncludeNulls = eVar.a();
        e eVar2 = new e();
        eVar2.c = cVar;
        gsonOmitNulls = eVar2.a();
    }

    /* JADX WARN: Multi-variable type inference failed */
    public GatewaySocket(Function0<IdentifyData> function0, Function1<? super Map<String, ? extends Object>, Unit> function1, GatewayEventHandler gatewayEventHandler, Scheduler scheduler, Logger logger, NetworkMonitor networkMonitor, RestConfig restConfig, Context context, Function1<? super String, String> function12, SSLSocketFactory sSLSocketFactory, Map<String, ? extends Object> map, GatewaySocketLogger gatewaySocketLogger) {
        m.checkNotNullParameter(function0, "identifyDataProvider");
        m.checkNotNullParameter(function1, "trackReadyPayload");
        m.checkNotNullParameter(gatewayEventHandler, "eventHandler");
        m.checkNotNullParameter(scheduler, "scheduler");
        m.checkNotNullParameter(logger, "logger");
        m.checkNotNullParameter(networkMonitor, "networkMonitor");
        m.checkNotNullParameter(restConfig, "restConfig");
        m.checkNotNullParameter(context, "context");
        m.checkNotNullParameter(map, "identifyProperties");
        m.checkNotNullParameter(gatewaySocketLogger, "gatewaySocketLogger");
        this.identifyDataProvider = function0;
        this.trackReadyPayload = function1;
        this.eventHandler = gatewayEventHandler;
        this.scheduler = scheduler;
        this.logger = logger;
        this.gatewayUrlTransform = function12;
        this.sslSocketFactory = sSLSocketFactory;
        this.identifyProperties = map;
        this.gatewaySocketLogger = gatewaySocketLogger;
        Backoff backoff = new Backoff(1000L, 10000L, 4, true, new Timer(scheduler));
        this.gatewayBackoff = backoff;
        this.heartbeatExpeditedTimeout = new Timer(scheduler);
        this.heartbeatInterval = RecyclerView.FOREVER_NS;
        this.heartbeater = new Timer(scheduler);
        this.heartbeatAck = true;
        this.helloTimeout = new Timer(scheduler);
        ListenerCollectionSubject<Listener> listenerCollectionSubject = new ListenerCollectionSubject<>();
        this.listenerSubject = listenerCollectionSubject;
        this.listeners = listenerCollectionSubject;
        RestClient restClient = RestClient.INSTANCE;
        restClient.init(restConfig, context);
        this.gatewayDiscovery = new GatewayDiscovery(context, scheduler, backoff, new AnonymousClass1(), restClient.getGateway());
        Observable<Boolean> I = networkMonitor.observeIsConnected().I(scheduler);
        Observable.h0(new r(I.j, new w1(new v1(AnonymousClass2.INSTANCE)))).W(new Action1<Boolean>() { // from class: com.discord.gateway.GatewaySocket.3
            public final void call(Boolean bool) {
                GatewaySocket gatewaySocket = GatewaySocket.this;
                m.checkNotNullExpressionValue(bool, "isConnected");
                gatewaySocket.handleDeviceConnectivityChange(bool.booleanValue());
            }
        }, new Action1<Throwable>() { // from class: com.discord.gateway.GatewaySocket.4
            public final void call(Throwable th) {
                Logger logger2 = GatewaySocket.this.logger;
                StringBuilder R = a.R("failed to handle connectivity change in ");
                R.append(GatewaySocket.this.getClass().getSimpleName());
                Logger.e$default(logger2, R.toString(), th, null, 4, null);
            }
        });
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void cleanup(Function1<? super WebSocket, Unit> function1) {
        stopHeartbeater();
        clearHelloTimeout();
        if (function1 != null) {
            function1.invoke(this.webSocket);
        }
        WebSocket webSocket = this.webSocket;
        if (webSocket != null) {
            webSocket.resetListeners();
        }
        this.webSocket = null;
        this.gatewayBackoff.cancel();
    }

    private final void clearHelloTimeout() {
        this.helloTimeout.cancel();
    }

    public static /* synthetic */ void close$default(GatewaySocket gatewaySocket, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        gatewaySocket.close(z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void discover() {
        this.nextReconnectIsImmediate = false;
        if (this.connectionState == 1) {
            this.listenerSubject.notify(new GatewaySocket$discover$1(this));
            this.gatewayDiscovery.discoverGatewayUrl(new GatewaySocket$discover$2(this), new GatewaySocket$discover$3(this));
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void discoveryFailed() {
        long fail = this.gatewayBackoff.fail(new GatewaySocket$discoveryFailed$delay$1(this));
        Companion companion = Companion;
        Logger logger = this.logger;
        Companion.log$default(companion, logger, "Discovery failed, retrying in " + fail + "ms.", false, 2, null);
        if (this.gatewayBackoff.hasReachedFailureThreshold()) {
            reset(false, 0, "Gateway discovery failed.");
        }
    }

    private final void doIdentify() {
        this.seq = 0;
        this.sessionId = null;
        IdentifyData invoke = this.identifyDataProvider.invoke();
        if (invoke == null) {
            handleClose(true, CLOSE_CODE_UNAUTHORIZED, "No connection info provided.");
            return;
        }
        this.connectionState = 3;
        this.identifyStartTime = clock.currentTimeMillis();
        this.token = invoke.getToken();
        Companion.log$default(Companion, this.logger, "Sending identify.", false, 2, null);
        send$default(this, new Outgoing(Opcode.IDENTIFY, new OutgoingPayload.Identify(invoke.getToken(), 100, true, CLIENT_CAPABILITIES, this.identifyProperties, invoke.getClientState())), false, null, 4, null);
    }

    private final void doResume() {
        this.connectionState = 4;
        this.replayedEvents = 0;
        Companion companion = Companion;
        Logger logger = this.logger;
        StringBuilder R = a.R("Resuming session ");
        String str = this.sessionId;
        if (str == null) {
            str = "";
        }
        R.append(str);
        R.append(" at sequence: ");
        Companion.log$default(companion, logger, a.z(R, this.seq, ClassUtils.PACKAGE_SEPARATOR_CHAR), false, 2, null);
        send$default(this, new Outgoing(Opcode.RESUME, new OutgoingPayload.Resume(this.token, this.sessionId, this.seq)), false, null, 4, null);
    }

    private final void doResumeOrIdentify() {
        Companion companion = Companion;
        long delay$default = Companion.getDelay$default(companion, this.heartbeatAckTimeMostRecent, null, 2, null);
        float f = this.heartbeatAckTimeMostRecent == 0 ? 0.0f : (((float) delay$default) / 1000.0f) / 60.0f;
        if (this.sessionId != null && delay$default <= ((long) HEARTBEAT_MAX_RESUME_THRESHOLD)) {
            Logger logger = this.logger;
            StringBuilder R = a.R("Attempting to resume after elapsed duration of ");
            String format = String.format("%.2f", Arrays.copyOf(new Object[]{Float.valueOf(f)}, 1));
            m.checkNotNullExpressionValue(format, "java.lang.String.format(this, *args)");
            R.append(format);
            R.append(" minutes.");
            Companion.log$default(companion, logger, R.toString(), false, 2, null);
            doResume();
        } else {
            handleInvalidSession(false);
        }
        this.heartbeatAckTimeMostRecent = clock.currentTimeMillis();
    }

    public static /* synthetic */ void expeditedHeartbeat$default(GatewaySocket gatewaySocket, long j, String str, boolean z2, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            z2 = true;
        }
        gatewaySocket.expeditedHeartbeat(j, str, z2);
    }

    /* JADX WARN: Multi-variable type inference failed */
    private final void flattenTraces(Object obj, ArrayList<Pair<String, Integer>> arrayList) {
        if (obj != null) {
            try {
                if (e0.isMutableList(obj)) {
                    int i = 0;
                    while (true) {
                        int i2 = i + 1;
                        if (i2 < ((List) obj).size()) {
                            Object obj2 = ((List) obj).get(i);
                            Number number = null;
                            if (!(obj2 instanceof String)) {
                                obj2 = null;
                            }
                            String str = (String) obj2;
                            Object obj3 = ((List) obj).get(i2);
                            if (!e0.isMutableMap(obj3)) {
                                obj3 = null;
                            }
                            Map map = (Map) obj3;
                            i += 2;
                            if (!(str == null || map == null)) {
                                if (map.containsKey("micros")) {
                                    Object obj4 = map.get("micros");
                                    if (obj4 instanceof Number) {
                                        number = obj4;
                                    }
                                    number = number;
                                }
                                if (number != null) {
                                    arrayList.add(new Pair<>(str, Integer.valueOf(number.intValue() / 1000)));
                                }
                                flattenTraces(map.get("calls"), arrayList);
                            }
                        } else {
                            return;
                        }
                    }
                }
            } catch (Exception e) {
                Logger.e$default(this.logger, "Unable to parse ready payload traces", e, null, 4, null);
            }
        }
    }

    private final String getConnectionPath(ModelPayload.Hello hello) {
        String joinToString$default;
        List<String> trace = hello.getTrace();
        return (trace == null || (joinToString$default = u.joinToString$default(trace, " -> ", null, null, 0, null, null, 62, null)) == null) ? "???" : joinToString$default;
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleClose(boolean z2, int i, String str) {
        cleanup(GatewaySocket$handleClose$1.INSTANCE);
        handleConnected$default(this, null, Boolean.FALSE, 1, null);
        if (i == CLOSE_CODE_UNAUTHORIZED) {
            this.connectionState = 0;
            reset(z2, i, str);
            return;
        }
        this.connectionState = 1;
        StringBuilder sb = new StringBuilder();
        sb.append("Closed cleanly: ");
        sb.append(z2);
        sb.append(", with code: ");
        sb.append(i);
        sb.append(", for reason: '");
        String H = a.H(sb, str, "'.");
        if (this.nextReconnectIsImmediate) {
            Companion.log$default(Companion, this.logger, a.v(H, " Retrying immediately."), false, 2, null);
            discover();
            return;
        }
        long fail = this.gatewayBackoff.fail(new GatewaySocket$handleClose$delay$1(this));
        Companion companion = Companion;
        Logger logger = this.logger;
        Companion.log$default(companion, logger, H + " Retrying in: " + fail + "ms.", false, 2, null);
        if (this.gatewayBackoff.hasReachedFailureThreshold()) {
            reset(z2, i, str);
        }
    }

    private final void handleConnected(Boolean bool, Boolean bool2) {
        if (bool != null) {
            boolean booleanValue = bool.booleanValue();
            this.connected = booleanValue;
            this.hasConnectedOnce = this.hasConnectedOnce || booleanValue;
            this.eventHandler.handleConnected(booleanValue);
        }
        if (bool2 != null) {
            boolean booleanValue2 = bool2.booleanValue();
            this.connectionReady = booleanValue2;
            this.eventHandler.handleConnectionReady(booleanValue2);
        }
    }

    public static /* synthetic */ void handleConnected$default(GatewaySocket gatewaySocket, Boolean bool, Boolean bool2, int i, Object obj) {
        if ((i & 1) != 0) {
            bool = null;
        }
        if ((i & 2) != 0) {
            bool2 = null;
        }
        gatewaySocket.handleConnected(bool, bool2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleDeviceConnectivityChange(boolean z2) {
        if (z2) {
            expeditedHeartbeat$default(this, 4500L, "network detected online", false, 4, null);
        } else {
            expeditedHeartbeat$default(this, 9000L, "network detected offline", false, 4, null);
        }
    }

    private final void handleDispatch(Object obj, String str, int i, int i2, long j) {
        if (this.connectionState == 4) {
            this.replayedEvents++;
        }
        if (m.areEqual(str, "READY") || m.areEqual(str, "RESUMED")) {
            if (str != null) {
                int hashCode = str.hashCode();
                if (hashCode != 77848963) {
                    if (hashCode == 1815529911 && str.equals("RESUMED")) {
                        StringBuilder sb = new StringBuilder();
                        StringBuilder R = a.R("Resumed session, took ");
                        Companion companion = Companion;
                        R.append(Companion.getDelay$default(companion, this.connectionStartTime, null, 2, null));
                        R.append("ms, ");
                        sb.append(R.toString());
                        sb.append("replayed " + this.replayedEvents + " events, new seq: " + this.seq + ClassUtils.PACKAGE_SEPARATOR_CHAR);
                        String sb2 = sb.toString();
                        m.checkNotNullExpressionValue(sb2, "StringBuilder()\n        …              .toString()");
                        Companion.log$default(companion, this.logger, sb2, false, 2, null);
                        this.replayedEvents = 0;
                    }
                } else if (str.equals("READY")) {
                    ModelPayload modelPayload = (ModelPayload) obj;
                    if (modelPayload == null) {
                        handleReconnect$default(this, "Reconnect due to invalid ready payload received.", false, 2, null);
                        return;
                    }
                    this.sessionId = modelPayload.getSessionId();
                    trackReadyPayload(modelPayload, i, i2, j);
                    Companion companion2 = Companion;
                    Logger logger = this.logger;
                    StringBuilder R2 = a.R("Ready with session id: ");
                    R2.append(this.sessionId);
                    R2.append(", took ");
                    Companion.log$default(companion2, logger, a.B(R2, Companion.getDelay$default(companion2, this.connectionStartTime, null, 2, null), "ms"), false, 2, null);
                }
            }
            this.gatewayBackoff.succeed();
            this.connectionState = 5;
            Boolean bool = Boolean.TRUE;
            handleConnected(bool, bool);
        }
        if (obj != null) {
            this.eventHandler.handleDispatch(str, obj);
        } else if (u.contains(EXPECTED_NULL_DATA_EVENTS, str)) {
            this.eventHandler.handleDispatch(str, Unit.a);
        } else if ((!m.areEqual(str, "READY")) && (!m.areEqual(str, "RESUMED"))) {
            Logger.w$default(this.logger, a.w("handleDispatch() ", str, " is unhandled!"), null, 2, null);
        }
    }

    private final void handleHeartbeat() {
        heartbeat(this.seq);
    }

    private final void handleHeartbeatAck() {
        Companion.log(this.logger, "Received heartbeat ACK.", false);
        this.heartbeatAckTimeMostRecent = clock.currentTimeMillis();
        this.heartbeatAck = true;
        this.heartbeatExpeditedTimeout.cancel(new GatewaySocket$handleHeartbeatAck$1(this));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleHeartbeatTimeout() {
        cleanup(GatewaySocket$handleHeartbeatTimeout$1.INSTANCE);
        this.connectionState = 1;
        long fail = this.gatewayBackoff.fail(new GatewaySocket$handleHeartbeatTimeout$delay$1(this));
        Companion companion = Companion;
        Logger logger = this.logger;
        Companion.log$default(companion, logger, "Ack timeout, reconnecting om " + fail + "ms.", false, 2, null);
    }

    private final void handleHello(ModelPayload.Hello hello) {
        this.listenerSubject.notify(new GatewaySocket$handleHello$1(this));
        clearHelloTimeout();
        this.heartbeatInterval = hello.getHeartbeatInterval();
        Companion companion = Companion;
        Logger logger = this.logger;
        StringBuilder R = a.R("Hello via ");
        R.append(getConnectionPath(hello));
        R.append(", at interval ");
        R.append(this.heartbeatInterval);
        R.append(" took ");
        Companion.log$default(companion, logger, a.B(R, Companion.getDelay$default(companion, this.connectionStartTime, null, 2, null), "ms."), false, 2, null);
        startHeartbeater();
    }

    private final void handleInvalidSession(boolean z2) {
        Companion.log$default(Companion, this.logger, a.H(a.R("Invalid session, is "), z2 ? "" : "not", " resumable."), false, 2, null);
        if (z2) {
            doResumeOrIdentify();
            return;
        }
        Boolean bool = Boolean.FALSE;
        handleConnected(bool, bool);
        doIdentify();
    }

    private final void handleReconnect(String str, boolean z2) {
        cleanup(new GatewaySocket$handleReconnect$1(str));
        reset(!z2, z2 ? 1000 : 4000, str);
        handleClose(!z2, 0, str);
    }

    public static /* synthetic */ void handleReconnect$default(GatewaySocket gatewaySocket, String str, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = "Reconnect to gateway requested.";
        }
        if ((i & 2) != 0) {
            z2 = true;
        }
        gatewaySocket.handleReconnect(str, z2);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* JADX WARN: Code restructure failed: missing block: B:8:0x0010, code lost:
        if ((r0.length() > 0) != false) goto L10;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void handleWebSocketClose(com.discord.utilities.websocket.WebSocket.Closed r5) {
        /*
            r4 = this;
            java.lang.String r0 = r5.getReason()
            r1 = 1
            if (r0 == 0) goto L13
            int r2 = r0.length()
            if (r2 <= 0) goto Lf
            r2 = 1
            goto L10
        Lf:
            r2 = 0
        L10:
            if (r2 == 0) goto L13
            goto L16
        L13:
            java.lang.String r0 = "unspecified reason"
        L16:
            com.discord.utilities.collections.ListenerCollectionSubject<com.discord.gateway.GatewaySocket$Listener> r2 = r4.listenerSubject
            com.discord.gateway.GatewaySocket$handleWebSocketClose$1 r3 = new com.discord.gateway.GatewaySocket$handleWebSocketClose$1
            r3.<init>(r4)
            r2.notify(r3)
            int r5 = r5.getCode()
            r4.handleClose(r1, r5, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.gateway.GatewaySocket.handleWebSocketClose(com.discord.utilities.websocket.WebSocket$Closed):void");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleWebSocketError() {
        handleClose(false, 0, "An error with the web socket occurred.");
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleWebSocketMessage(InputStreamReader inputStreamReader, int i) {
        long currentTimeMillis = clock.currentTimeMillis();
        SizeRecordingInputStreamReader sizeRecordingInputStreamReader = new SizeRecordingInputStreamReader(inputStreamReader, 0, 2, null);
        Incoming build = ((IncomingParser) new Model.JsonReader(new JsonReader(sizeRecordingInputStreamReader)).parse(new IncomingParser(new GatewaySocket$handleWebSocketMessage$incomingParser$1(this)))).build();
        Integer seq = build.getSeq();
        Companion companion = Companion;
        long delay$default = Companion.getDelay$default(companion, currentTimeMillis, null, 2, null);
        if (seq != null) {
            this.seq = seq.intValue();
        }
        int ordinal = build.getOpcode().ordinal();
        if (ordinal == 1) {
            handleDispatch(build.getData(), build.getType(), i, sizeRecordingInputStreamReader.getSize(), delay$default);
        } else if (ordinal == 2) {
            handleHeartbeat();
        } else if (ordinal != 8) {
            switch (ordinal) {
                case 10:
                    Object data = build.getData();
                    Objects.requireNonNull(data, "null cannot be cast to non-null type kotlin.Boolean");
                    handleInvalidSession(((Boolean) data).booleanValue());
                    return;
                case 11:
                    Object data2 = build.getData();
                    Objects.requireNonNull(data2, "null cannot be cast to non-null type com.discord.models.domain.ModelPayload.Hello");
                    handleHello((ModelPayload.Hello) data2);
                    return;
                case 12:
                    handleHeartbeatAck();
                    return;
                default:
                    Logger logger = this.logger;
                    StringBuilder R = a.R("Unhandled op code ");
                    R.append(build.getOpcode());
                    R.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
                    Companion.log$default(companion, logger, R.toString(), false, 2, null);
                    return;
            }
        } else {
            handleReconnect$default(this, null, false, 1, null);
        }
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void handleWebSocketOpened(String str) {
        Companion companion = Companion;
        Companion.log$default(companion, this.logger, a.B(a.W("Connected to ", str, " in "), Companion.getDelay$default(companion, this.connectionStartTime, null, 2, null), "ms."), false, 2, null);
        this.listenerSubject.notify(new GatewaySocket$handleWebSocketOpened$1(this));
        doResumeOrIdentify();
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void heartbeat(int i) {
        Companion companion = Companion;
        Logger logger = this.logger;
        companion.log(logger, "Sending heartbeat at sequence: " + i + ClassUtils.PACKAGE_SEPARATOR_CHAR, false);
        send$default(this, new Outgoing(Opcode.HEARTBEAT, Integer.valueOf(i)), false, null, 4, null);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void logError(String str, Exception exc, Map<String, String> map) {
        Logger logger = this.logger;
        if (map == null) {
            map = h0.emptyMap();
        }
        logger.e(str, exc, map);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void presenceUpdate$default(GatewaySocket gatewaySocket, ClientStatus clientStatus, Long l, List list, Boolean bool, int i, Object obj) {
        if ((i & 2) != 0) {
            l = Long.valueOf(clock.currentTimeMillis());
        }
        if ((i & 4) != 0) {
            list = null;
        }
        if ((i & 8) != 0) {
            bool = Boolean.FALSE;
        }
        gatewaySocket.presenceUpdate(clientStatus, l, list, bool);
    }

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void requestGuildMembers$default(GatewaySocket gatewaySocket, List list, String str, List list2, Integer num, int i, Object obj) {
        if ((i & 2) != 0) {
            str = null;
        }
        if ((i & 4) != 0) {
            list2 = null;
        }
        if ((i & 8) != 0) {
            num = null;
        }
        gatewaySocket.requestGuildMembers(list, str, list2, num);
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void reset(boolean z2, int i, String str) {
        boolean z3 = false;
        if (i != 4000) {
            this.sessionId = null;
            this.seq = 0;
        }
        String str2 = z2 ? "cleanly" : "dirty";
        Companion companion = Companion;
        Logger logger = this.logger;
        Companion.log$default(companion, logger, "Reset " + str2 + ", with code " + i + ", at sequence " + this.seq + ". Reason: '" + str + "'.", false, 2, null);
        handleConnected(Boolean.valueOf(i == 4000 && this.connected), Boolean.FALSE);
        GatewayEventHandler gatewayEventHandler = this.eventHandler;
        if (i == CLOSE_CODE_UNAUTHORIZED) {
            z3 = true;
        }
        gatewayEventHandler.handleDisconnect(z3);
    }

    private final void schedule(Function0<Unit> function0) {
        new k(function0).X(this.scheduler).W(GatewaySocket$schedule$1.INSTANCE, GatewaySocket$schedule$2.INSTANCE);
    }

    private final void send(Outgoing outgoing, boolean z2, Gson gson) {
        if (!z2 || isSessionEstablished()) {
            WebSocket webSocket = this.webSocket;
            if (webSocket == null) {
                Companion companion = Companion;
                Logger logger = this.logger;
                StringBuilder R = a.R("Attempted to send without a web socket that exists, opcode: ");
                R.append(outgoing.getOp());
                R.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
                Companion.log$default(companion, logger, R.toString(), false, 2, null);
                return;
            }
            String m = gson.m(outgoing);
            GatewaySocketLogger gatewaySocketLogger = this.gatewaySocketLogger;
            m.checkNotNullExpressionValue(m, GATEWAY_ENCODING);
            gatewaySocketLogger.logOutboundMessage(m);
            webSocket.message(m);
            return;
        }
        Companion companion2 = Companion;
        Logger logger2 = this.logger;
        StringBuilder R2 = a.R("Attempted to send while not being in a connected state, opcode: ");
        R2.append(outgoing.getOp());
        R2.append(ClassUtils.PACKAGE_SEPARATOR_CHAR);
        Companion.log$default(companion2, logger2, R2.toString(), false, 2, null);
    }

    public static /* synthetic */ void send$default(GatewaySocket gatewaySocket, Outgoing outgoing, boolean z2, Gson gson, int i, Object obj) {
        if ((i & 2) != 0) {
            z2 = true;
        }
        if ((i & 4) != 0) {
            gson = gsonIncludeNulls;
            m.checkNotNullExpressionValue(gson, "gsonIncludeNulls");
        }
        gatewaySocket.send(outgoing, z2, gson);
    }

    private final void startHeartbeater() {
        this.heartbeater.cancel();
        this.heartbeatAck = true;
        this.heartbeater.postInterval(new GatewaySocket$startHeartbeater$1(this), this.heartbeatInterval);
    }

    private final void stopHeartbeater() {
        this.heartbeater.cancel();
        this.heartbeatExpeditedTimeout.cancel();
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x0073, code lost:
        if (d0.g0.t.startsWith$default(r6.component1(), "gateway-", false, 2, null) != false) goto L18;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    private final void trackReadyPayload(com.discord.models.domain.ModelPayload r20, int r21, int r22, long r23) {
        /*
            Method dump skipped, instructions count: 413
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.gateway.GatewaySocket.trackReadyPayload(com.discord.models.domain.ModelPayload, int, int, long):void");
    }

    public final void callConnect(long j) {
        schedule(new GatewaySocket$callConnect$1(this, j));
    }

    public final void close(boolean z2) {
        schedule(new GatewaySocket$close$1(this, z2));
    }

    public final void connect() {
        schedule(new GatewaySocket$connect$1(this));
    }

    public final void expeditedHeartbeat(long j, String str, boolean z2) {
        schedule(new GatewaySocket$expeditedHeartbeat$1(this, str, j, z2));
    }

    public final ListenerCollection<Listener> getListeners() {
        return this.listeners;
    }

    public final boolean isSessionEstablished() {
        return this.connectionState == 5;
    }

    public final void presenceUpdate(ClientStatus clientStatus, Long l, List<Activity> list, Boolean bool) {
        schedule(new GatewaySocket$presenceUpdate$1(this, clientStatus, list, l, bool));
    }

    public final void requestApplicationCommands(OutgoingPayload.ApplicationCommandRequest applicationCommandRequest) {
        m.checkNotNullParameter(applicationCommandRequest, "request");
        Outgoing outgoing = new Outgoing(Opcode.REQUEST_GUILD_APPLICATION_COMMANDS, applicationCommandRequest);
        Gson gson = gsonOmitNulls;
        m.checkNotNullExpressionValue(gson, "gsonOmitNulls");
        send$default(this, outgoing, false, gson, 2, null);
    }

    public final void requestGuildMembers(List<Long> list) {
        requestGuildMembers$default(this, list, null, null, null, 14, null);
    }

    public final void requestGuildMembers(List<Long> list, String str) {
        requestGuildMembers$default(this, list, str, null, null, 12, null);
    }

    public final void requestGuildMembers(List<Long> list, String str, List<Long> list2) {
        requestGuildMembers$default(this, list, str, list2, null, 8, null);
    }

    public final void requestGuildMembers(List<Long> list, String str, List<Long> list2, Integer num) {
        m.checkNotNullParameter(list, "guildIds");
        schedule(new GatewaySocket$requestGuildMembers$1(this, list, str, list2, num));
    }

    public final void resetOnError() {
        schedule(new GatewaySocket$resetOnError$1(this));
    }

    public final void simulateReconnectForTesting() {
        schedule(new GatewaySocket$simulateReconnectForTesting$1(this));
    }

    public final void streamCreate(String str, long j, Long l, String str2) {
        m.checkNotNullParameter(str, "streamType");
        Companion companion = Companion;
        Logger logger = this.logger;
        Companion.log$default(companion, logger, "Sending STREAM_CREATE: " + j + ' ' + l, false, 2, null);
        send$default(this, new Outgoing(Opcode.STREAM_CREATE, new OutgoingPayload.CreateStream(str, j, l, str2)), false, null, 6, null);
    }

    public final void streamDelete(String str) {
        m.checkNotNullParameter(str, "streamKey");
        schedule(new GatewaySocket$streamDelete$1(this, str));
    }

    public final void streamPing(String str) {
        m.checkNotNullParameter(str, "streamKey");
        schedule(new GatewaySocket$streamPing$1(this, str));
    }

    public final void streamWatch(String str) {
        m.checkNotNullParameter(str, "streamKey");
        schedule(new GatewaySocket$streamWatch$1(this, str));
    }

    public final void updateGuildSubscriptions(long j, OutgoingPayload.GuildSubscriptions guildSubscriptions) {
        m.checkNotNullParameter(guildSubscriptions, "guildSubscriptions");
        Companion companion = Companion;
        Logger logger = this.logger;
        Companion.log$default(companion, logger, "sending guild subscriptions: " + j + " -- " + guildSubscriptions, false, 2, null);
        Opcode opcode = Opcode.GUILD_SUBSCRIPTIONS;
        Boolean typing = guildSubscriptions.getTyping();
        Boolean activities = guildSubscriptions.getActivities();
        List<Long> members = guildSubscriptions.getMembers();
        Outgoing outgoing = new Outgoing(opcode, new OutgoingPayload.GuildSubscriptionsUpdate(j, typing, activities, members != null ? u.toList(members) : null, guildSubscriptions.getChannels(), guildSubscriptions.getThreads(), guildSubscriptions.getThreadMemberLists()));
        Gson gson = gsonOmitNulls;
        m.checkNotNullExpressionValue(gson, "gsonOmitNulls");
        send$default(this, outgoing, false, gson, 2, null);
    }

    public final void voiceServerPing() {
        schedule(new GatewaySocket$voiceServerPing$1(this));
    }

    public final void voiceStateUpdate(Long l, Long l2, boolean z2, boolean z3, boolean z4, String str, boolean z5) {
        schedule(new GatewaySocket$voiceStateUpdate$1(this, l, l2, z2, z3, str, z5, z4));
    }

    /* JADX INFO: Access modifiers changed from: private */
    public final void connect(final String str) {
        RawMessageHandler rawMessageHandler;
        if (this.connectionState == 1) {
            this.connectionState = 2;
            Companion.log$default(Companion, this.logger, a.w("Connect to: ", str, ", encoding: json, version 9."), false, 2, null);
            cleanup(GatewaySocket$connect$2.INSTANCE);
            this.connectionStartTime = clock.currentTimeMillis();
            this.helloTimeout.schedule(new GatewaySocket$connect$3(this), HELLO_TIMEOUT);
            WebSocket webSocket = new WebSocket(this.scheduler, new GatewaySocket$connect$4(this), this.sslSocketFactory);
            webSocket.setOnOpened(new GatewaySocket$connect$$inlined$apply$lambda$1(this, str));
            webSocket.setOnClosed(new GatewaySocket$connect$$inlined$apply$lambda$2(this, str));
            webSocket.setOnMessage(new GatewaySocket$connect$$inlined$apply$lambda$3(this, str));
            int ordinal = this.gatewaySocketLogger.getLogLevel().ordinal();
            if (ordinal == 0) {
                rawMessageHandler = null;
            } else if (ordinal == 1) {
                rawMessageHandler = new RawMessageHandler() { // from class: com.discord.gateway.GatewaySocket$connect$$inlined$apply$lambda$4
                    @Override // com.discord.utilities.websocket.RawMessageHandler
                    public void onRawMessage(String str2) {
                        GatewaySocketLogger gatewaySocketLogger;
                        m.checkNotNullParameter(str2, "rawMessage");
                        gatewaySocketLogger = GatewaySocket.this.gatewaySocketLogger;
                        gatewaySocketLogger.logInboundMessage(str2);
                    }

                    @Override // com.discord.utilities.websocket.RawMessageHandler
                    public void onRawMessageInflateFailed(Throwable th) {
                        GatewaySocketLogger gatewaySocketLogger;
                        m.checkNotNullParameter(th, "throwable");
                        gatewaySocketLogger = GatewaySocket.this.gatewaySocketLogger;
                        gatewaySocketLogger.logMessageInflateFailed(th);
                    }
                };
            } else {
                throw new NoWhenBranchMatchedException();
            }
            webSocket.setRawMessageHandler(rawMessageHandler);
            webSocket.setOnError(new GatewaySocket$connect$$inlined$apply$lambda$5(this, str));
            webSocket.connect(str + "/?encoding=json&v=9&compress=zlib-stream");
            this.webSocket = webSocket;
        }
    }

    public /* synthetic */ GatewaySocket(Function0 function0, Function1 function1, GatewayEventHandler gatewayEventHandler, Scheduler scheduler, Logger logger, NetworkMonitor networkMonitor, RestConfig restConfig, Context context, Function1 function12, SSLSocketFactory sSLSocketFactory, Map map, GatewaySocketLogger gatewaySocketLogger, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(function0, function1, gatewayEventHandler, scheduler, logger, networkMonitor, restConfig, context, (i & 256) != 0 ? null : function12, (i & 512) != 0 ? null : sSLSocketFactory, (i & 1024) != 0 ? h0.emptyMap() : map, gatewaySocketLogger);
    }
}
