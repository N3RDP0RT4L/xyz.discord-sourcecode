package com.discord.gateway;

import com.discord.gateway.GatewayDiscovery;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GatewayDiscovery.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "gatewayUrl", "", "invoke", "(Ljava/lang/String;)V", "handleSuccess"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewayDiscovery$discoverGatewayUrl$2 extends o implements Function1<String, Unit> {
    public final /* synthetic */ GatewayDiscovery$discoverGatewayUrl$1 $handleFailure$1;
    public final /* synthetic */ Function1 $onSuccess;
    public final /* synthetic */ GatewayDiscovery this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GatewayDiscovery$discoverGatewayUrl$2(GatewayDiscovery gatewayDiscovery, GatewayDiscovery$discoverGatewayUrl$1 gatewayDiscovery$discoverGatewayUrl$1, Function1 function1) {
        super(1);
        this.this$0 = gatewayDiscovery;
        this.$handleFailure$1 = gatewayDiscovery$discoverGatewayUrl$1;
        this.$onSuccess = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(String str) {
        invoke2(str);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(String str) {
        Function1 function1;
        if (str == null) {
            this.$handleFailure$1.invoke2(new Throwable("Malformed gateway url."));
            return;
        }
        this.this$0.gatewayUrl = str;
        GatewayDiscovery.Cache.INSTANCE.setGatewayUrl(str);
        function1 = this.this$0.log;
        function1.invoke("Discovered gateway url: " + str);
        this.$onSuccess.invoke(str);
    }
}
