package com.discord.gateway;

import d0.z.d.k;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GatewayDiscovery.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "p1", "", "invoke", "(Ljava/lang/Throwable;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final /* synthetic */ class GatewayDiscovery$discoverGatewayUrl$4 extends k implements Function1<Throwable, Unit> {
    public final /* synthetic */ GatewayDiscovery$discoverGatewayUrl$1 $handleFailure$1;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GatewayDiscovery$discoverGatewayUrl$4(GatewayDiscovery$discoverGatewayUrl$1 gatewayDiscovery$discoverGatewayUrl$1) {
        super(1, null, "handleFailure", "invoke(Ljava/lang/Throwable;)V", 0);
        this.$handleFailure$1 = gatewayDiscovery$discoverGatewayUrl$1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        invoke2(th);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Throwable th) {
        m.checkNotNullParameter(th, "p1");
        this.$handleFailure$1.invoke2(th);
    }
}
