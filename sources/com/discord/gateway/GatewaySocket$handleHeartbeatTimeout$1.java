package com.discord.gateway;

import com.discord.utilities.rest.SendUtils;
import com.discord.utilities.websocket.WebSocket;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GatewaySocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\b\u0010\u0001\u001a\u0004\u0018\u00010\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/utilities/websocket/WebSocket;", "ws", "", "invoke", "(Lcom/discord/utilities/websocket/WebSocket;)V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewaySocket$handleHeartbeatTimeout$1 extends o implements Function1<WebSocket, Unit> {
    public static final GatewaySocket$handleHeartbeatTimeout$1 INSTANCE = new GatewaySocket$handleHeartbeatTimeout$1();

    public GatewaySocket$handleHeartbeatTimeout$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(WebSocket webSocket) {
        invoke2(webSocket);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(WebSocket webSocket) {
        if (webSocket != null) {
            WebSocket.disconnect$default(webSocket, SendUtils.MAX_MESSAGE_CHARACTER_COUNT_PREMIUM, null, 2, null);
        }
    }
}
