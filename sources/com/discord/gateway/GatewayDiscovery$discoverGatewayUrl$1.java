package com.discord.gateway;

import com.discord.gateway.GatewayDiscovery;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: GatewayDiscovery.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", "throwable", "", "invoke", "(Ljava/lang/Throwable;)V", "handleFailure"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewayDiscovery$discoverGatewayUrl$1 extends o implements Function1<Throwable, Unit> {
    public final /* synthetic */ Function1 $onFailure;
    public final /* synthetic */ GatewayDiscovery this$0;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GatewayDiscovery$discoverGatewayUrl$1(GatewayDiscovery gatewayDiscovery, Function1 function1) {
        super(1);
        this.this$0 = gatewayDiscovery;
        this.$onFailure = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        invoke2(th);
        return Unit.a;
    }

    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2(Throwable th) {
        m.checkNotNullParameter(th, "throwable");
        this.this$0.gatewayUrl = null;
        GatewayDiscovery.Cache.INSTANCE.setGatewayUrl(null);
        this.$onFailure.invoke(th);
    }
}
