package com.discord.gateway;

import andhook.lib.xposed.ClassUtils;
import b.d.b.a.a;
import com.discord.gateway.GatewaySocket;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.networking.Backoff;
import com.discord.utilities.websocket.WebSocket;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: GatewaySocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewaySocket$expeditedHeartbeat$1 extends o implements Function0<Unit> {
    public final /* synthetic */ String $reason;
    public final /* synthetic */ boolean $shouldResetBackoff;
    public final /* synthetic */ long $timeout;
    public final /* synthetic */ GatewaySocket this$0;

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"", ModelAuditLogEntry.CHANGE_KEY_REASON, "", "invoke", "(Ljava/lang/String;)V", "resetBackoff"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.gateway.GatewaySocket$expeditedHeartbeat$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<String, Unit> {
        public AnonymousClass1() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(String str) {
            invoke2(str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(String str) {
            Backoff backoff;
            int i;
            m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_REASON);
            GatewaySocket.Companion companion = GatewaySocket.Companion;
            Logger logger = GatewaySocket$expeditedHeartbeat$1.this.this$0.logger;
            GatewaySocket.Companion.log$default(companion, logger, "Connection backoff reset " + str + ClassUtils.PACKAGE_SEPARATOR_CHAR, false, 2, null);
            backoff = GatewaySocket$expeditedHeartbeat$1.this.this$0.gatewayBackoff;
            backoff.succeed();
            GatewaySocket$expeditedHeartbeat$1.this.this$0.nextReconnectIsImmediate = true;
            i = GatewaySocket$expeditedHeartbeat$1.this.this$0.connectionState;
            if (i == 1) {
                GatewaySocket$expeditedHeartbeat$1.this.this$0.discover();
            }
        }
    }

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.gateway.GatewaySocket$expeditedHeartbeat$1$2  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass2 extends o implements Function0<Unit> {
        public AnonymousClass2() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            boolean z2;
            z2 = GatewaySocket$expeditedHeartbeat$1.this.this$0.heartbeatAck;
            if (!z2) {
                GatewaySocket$expeditedHeartbeat$1.this.this$0.handleHeartbeatTimeout();
            }
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GatewaySocket$expeditedHeartbeat$1(GatewaySocket gatewaySocket, String str, long j, boolean z2) {
        super(0);
        this.this$0 = gatewaySocket;
        this.$reason = str;
        this.$timeout = j;
        this.$shouldResetBackoff = z2;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        WebSocket webSocket;
        int i;
        GatewaySocket.Timer timer;
        String G = this.$reason != null ? a.G(a.R("with reason '"), this.$reason, '\'') : "";
        AnonymousClass1 r1 = new AnonymousClass1();
        webSocket = this.this$0.webSocket;
        if (webSocket != null) {
            GatewaySocket.Companion companion = GatewaySocket.Companion;
            Logger logger = this.this$0.logger;
            GatewaySocket.Companion.log$default(companion, logger, "Performing an expedited heartbeat " + G + ClassUtils.PACKAGE_SEPARATOR_CHAR, false, 2, null);
            GatewaySocket gatewaySocket = this.this$0;
            i = gatewaySocket.seq;
            gatewaySocket.heartbeat(i);
            this.this$0.heartbeatAck = false;
            timer = this.this$0.heartbeatExpeditedTimeout;
            timer.schedule(new AnonymousClass2(), this.$timeout);
        } else if (this.$shouldResetBackoff) {
            r1.invoke2(G);
        } else {
            GatewaySocket.Companion.log$default(GatewaySocket.Companion, this.this$0.logger, a.w("Expedited heartbeat requested ", G, ", but disconnected and no reset backoff."), false, 2, null);
        }
    }
}
