package com.discord.gateway;

import b.d.b.a.a;
import com.discord.api.activity.Activity;
import com.discord.api.presence.ClientStatus;
import com.discord.gateway.GatewaySocket;
import com.discord.gateway.io.Outgoing;
import com.discord.gateway.io.OutgoingPayload;
import com.discord.gateway.opcodes.Opcode;
import com.discord.utilities.logging.Logger;
import d0.t.n;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
/* compiled from: GatewaySocket.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\b\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0003\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0001\u0010\u0002"}, d2 = {"", "invoke", "()V", "<anonymous>"}, k = 3, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class GatewaySocket$presenceUpdate$1 extends o implements Function0<Unit> {
    public final /* synthetic */ List $activities;
    public final /* synthetic */ Boolean $afk;
    public final /* synthetic */ Long $since;
    public final /* synthetic */ ClientStatus $status;
    public final /* synthetic */ GatewaySocket this$0;

    /* compiled from: GatewaySocket.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\u0010\u0005\u001a\u00020\u00022\u0006\u0010\u0001\u001a\u00020\u0000H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"Lcom/discord/api/activity/Activity;", "it", "", "invoke", "(Lcom/discord/api/activity/Activity;)Ljava/lang/CharSequence;", "<anonymous>"}, k = 3, mv = {1, 4, 2})
    /* renamed from: com.discord.gateway.GatewaySocket$presenceUpdate$1$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static final class AnonymousClass1 extends o implements Function1<Activity, CharSequence> {
        public static final AnonymousClass1 INSTANCE = new AnonymousClass1();

        public AnonymousClass1() {
            super(1);
        }

        public final CharSequence invoke(Activity activity) {
            m.checkNotNullParameter(activity, "it");
            return activity.h();
        }
    }

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public GatewaySocket$presenceUpdate$1(GatewaySocket gatewaySocket, ClientStatus clientStatus, List list, Long l, Boolean bool) {
        super(0);
        this.this$0 = gatewaySocket;
        this.$status = clientStatus;
        this.$activities = list;
        this.$since = l;
        this.$afk = bool;
    }

    @Override // kotlin.jvm.functions.Function0
    /* renamed from: invoke  reason: avoid collision after fix types in other method */
    public final void invoke2() {
        String name;
        GatewaySocket.Companion companion = GatewaySocket.Companion;
        Logger logger = this.this$0.logger;
        StringBuilder R = a.R("Sending self presence update: ");
        R.append(this.$status);
        R.append(' ');
        List list = this.$activities;
        String str = null;
        R.append(list != null ? u.joinToString$default(list, null, null, null, 0, null, AnonymousClass1.INSTANCE, 31, null) : null);
        GatewaySocket.Companion.log$default(companion, logger, R.toString(), false, 2, null);
        GatewaySocket gatewaySocket = this.this$0;
        Opcode opcode = Opcode.PRESENCE_UPDATE;
        ClientStatus clientStatus = this.$status;
        if (!(clientStatus == null || (name = clientStatus.name()) == null)) {
            Locale locale = Locale.ROOT;
            m.checkNotNullExpressionValue(locale, "Locale.ROOT");
            str = name.toLowerCase(locale);
            m.checkNotNullExpressionValue(str, "(this as java.lang.String).toLowerCase(locale)");
        }
        Long l = this.$since;
        List list2 = this.$activities;
        if (list2 == null) {
            list2 = n.emptyList();
        }
        GatewaySocket.send$default(gatewaySocket, new Outgoing(opcode, new OutgoingPayload.PresenceUpdate(str, l, list2, this.$afk)), false, null, 6, null);
    }
}
