package com.discord;
/* loaded from: classes.dex */
public final class BuildConfig {
    public static final String APPLICATION_ID = "com.discord";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String EMBEDDED_ACTIVITY_APPLICATION_HOST = "discordsays.com";
    public static final String FLAVOR = "productionGoogle";
    public static final String FLAVOR_environment = "production";
    public static final String FLAVOR_vendor = "google";
    public static final String HOST = "https://discord.com";
    public static final String HOST_ALTERNATE = "https://discordapp.com";
    public static final String HOST_API = "https://discord.com/api/";
    public static final String HOST_CDN = "https://cdn.discordapp.com";
    public static final String HOST_DEVELOPER_PORTAL = "http://discord.com/developers";
    public static final String HOST_GIFT = "https://discord.gift";
    public static final String HOST_GUILD_TEMPLATE = "https://discord.new";
    public static final String HOST_INVITE = "https://discord.gg";
    public static final String HOST_MEDIA_PROXY = "https://media.discordapp.net";
    public static final String LOCAL_IP = "";
    public static final String LOCAL_VOICE = "";
    public static final String USER_AGENT = "Discord-Android/112014";
    public static final int VERSION_CODE = 112014;
    public static final String VERSION_NAME = "112.14 - Stable";
}
