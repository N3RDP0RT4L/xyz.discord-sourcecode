package com.discord.custom_tabs;

import androidx.browser.customtabs.CustomTabsClient;
import androidx.browser.customtabs.CustomTabsServiceConnection;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import b.a.h.b;
import d0.t.k;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import y.c.a;
/* compiled from: CustomTabs.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0006J\u0017\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0007\u0010\u0006R\u0018\u0010\u000b\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\t\u0010\n¨\u0006\f"}, d2 = {"com/discord/custom_tabs/CustomTabs$warmUp$1", "Landroidx/lifecycle/DefaultLifecycleObserver;", "Landroidx/lifecycle/LifecycleOwner;", "owner", "", "onResume", "(Landroidx/lifecycle/LifecycleOwner;)V", "onPause", "Landroidx/browser/customtabs/CustomTabsServiceConnection;", "j", "Landroidx/browser/customtabs/CustomTabsServiceConnection;", "connection", "custom_tabs_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class CustomTabs$warmUp$1 implements DefaultLifecycleObserver {
    public CustomTabsServiceConnection j;

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
        a.a(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
        a.b(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public void onPause(LifecycleOwner lifecycleOwner) {
        m.checkNotNullParameter(lifecycleOwner, "owner");
        a.c(this, lifecycleOwner);
        if (this.j == null) {
            this.j = null;
            return;
        }
        throw null;
    }

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public void onResume(LifecycleOwner lifecycleOwner) {
        m.checkNotNullParameter(lifecycleOwner, "owner");
        a.d(this, lifecycleOwner);
        b bVar = null;
        List list = k.toList((Object[]) null);
        m.checkNotNullParameter(null, "context");
        m.checkNotNullParameter(list, "uris");
        String a = b.a.h.a.a(null);
        if (a != null) {
            b bVar2 = new b(list);
            if (CustomTabsClient.bindCustomTabsService(null, a, bVar2)) {
                bVar = bVar2;
            }
        }
        this.j = bVar;
    }

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
        a.e(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.DefaultLifecycleObserver, androidx.lifecycle.FullLifecycleObserver
    public /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
        a.f(this, lifecycleOwner);
    }
}
