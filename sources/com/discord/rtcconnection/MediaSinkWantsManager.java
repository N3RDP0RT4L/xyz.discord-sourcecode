package com.discord.rtcconnection;

import andhook.lib.HookHelper;
import b.a.q.e;
import b.a.q.h;
import co.discord.media_engine.StreamParameters;
import com.discord.rtcconnection.KrispOveruseDetector;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.utilities.logging.Logger;
import d0.o;
import d0.t.g0;
import d0.t.h0;
import d0.t.n;
import d0.z.d.m;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.subjects.BehaviorSubject;
/* compiled from: MediaSinkWantsManager.kt */
/* loaded from: classes.dex */
public final class MediaSinkWantsManager implements MediaEngineConnection.d {
    public MediaEngineConnection d;
    public Long e;
    public final long h;
    public final b.a.q.c i;
    public final e j;
    public final Logger k;
    public final a l;
    public final Map<Long, Long> a = new LinkedHashMap();

    /* renamed from: b  reason: collision with root package name */
    public final Map<Long, List<b>> f2743b = new LinkedHashMap();
    public final Set<Long> c = new LinkedHashSet();
    public final BehaviorSubject<Map<String, EncodeQuality>> f = BehaviorSubject.l0(g0.mapOf(o.to("any", EncodeQuality.Hundred)));
    public Map<String, ? extends EncodeQuality> g = h0.emptyMap();

    /* compiled from: MediaSinkWantsManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\u000b\b\u0086\u0001\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\nB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u000bj\u0002\b\f¨\u0006\r"}, d2 = {"Lcom/discord/rtcconnection/MediaSinkWantsManager$VideoQualityMode;", "", "", "numeral", "I", "getNumeral", "()I", HookHelper.constructorName, "(Ljava/lang/String;II)V", "Companion", "a", "AUTO", "FULL", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum VideoQualityMode {
        AUTO(1),
        FULL(2);
        
        public static final a Companion = new a(null);
        private final int numeral;

        /* compiled from: MediaSinkWantsManager.kt */
        /* loaded from: classes.dex */
        public static final class a {
            public a(DefaultConstructorMarker defaultConstructorMarker) {
            }

            public final VideoQualityMode a(Integer num) {
                return (num != null && num.intValue() == 1) ? VideoQualityMode.AUTO : (num != null && num.intValue() == 2) ? VideoQualityMode.FULL : VideoQualityMode.AUTO;
            }
        }

        VideoQualityMode(int i) {
            this.numeral = i;
        }

        public final int getNumeral() {
            return this.numeral;
        }
    }

    /* compiled from: MediaSinkWantsManager.kt */
    /* loaded from: classes.dex */
    public interface a {
        void a(long j, long j2, long j3, VideoMetadata videoMetadata);
    }

    /* compiled from: MediaSinkWantsManager.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public final EncodeQuality a;

        /* renamed from: b  reason: collision with root package name */
        public final long f2744b;
        public final VideoMetadata c;

        public b(EncodeQuality encodeQuality, long j, VideoMetadata videoMetadata) {
            m.checkNotNullParameter(encodeQuality, "encodeQuality");
            this.a = encodeQuality;
            this.f2744b = j;
            this.c = videoMetadata;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return m.areEqual(this.a, bVar.a) && this.f2744b == bVar.f2744b && m.areEqual(this.c, bVar.c);
        }

        public int hashCode() {
            EncodeQuality encodeQuality = this.a;
            int i = 0;
            int a = (a0.a.a.b.a(this.f2744b) + ((encodeQuality != null ? encodeQuality.hashCode() : 0) * 31)) * 31;
            VideoMetadata videoMetadata = this.c;
            if (videoMetadata != null) {
                i = videoMetadata.hashCode();
            }
            return a + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("VideoStreamDescriptor(encodeQuality=");
            R.append(this.a);
            R.append(", ssrc=");
            R.append(this.f2744b);
            R.append(", metadata=");
            R.append(this.c);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: MediaSinkWantsManager.kt */
    /* loaded from: classes.dex */
    public static final class c extends d0.z.d.o implements Function0<Unit> {
        public final /* synthetic */ Long $ssrc;
        public final /* synthetic */ long $userId;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(Long l, long j) {
            super(0);
            this.$ssrc = l;
            this.$userId = j;
        }

        @Override // kotlin.jvm.functions.Function0
        public Unit invoke() {
            if (this.$ssrc != null) {
                MediaSinkWantsManager.this.a.put(Long.valueOf(this.$userId), this.$ssrc);
            } else {
                MediaSinkWantsManager.this.a.remove(Long.valueOf(this.$userId));
            }
            MediaSinkWantsManager.e(MediaSinkWantsManager.this, null, 1);
            return Unit.a;
        }
    }

    public MediaSinkWantsManager(long j, b.a.q.c cVar, e eVar, Logger logger, a aVar) {
        m.checkNotNullParameter(cVar, "mediaEngineThreadExecutor");
        m.checkNotNullParameter(eVar, "ladder");
        m.checkNotNullParameter(logger, "logger");
        m.checkNotNullParameter(aVar, "listener");
        this.h = j;
        this.i = cVar;
        this.j = eVar;
        this.k = logger;
        this.l = aVar;
        new LinkedHashMap();
    }

    public static /* synthetic */ void e(MediaSinkWantsManager mediaSinkWantsManager, List list, int i) {
        mediaSinkWantsManager.d((i & 1) != 0 ? n.emptyList() : null);
    }

    public final void a(String str) {
        Logger.w$default(this.k, "MediaSinkWantsManager", str, null, 4, null);
    }

    public final Future<?> b(Function0<Unit> function0) {
        b.a.q.c cVar = this.i;
        return cVar.l.submit(new b.a.q.m(function0));
    }

    public final Future<?> c(long j, Long l) {
        return b(new c(l, j));
    }

    /* JADX WARN: Removed duplicated region for block: B:115:0x0246  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x0043  */
    /* JADX WARN: Removed duplicated region for block: B:22:0x0046  */
    /* JADX WARN: Removed duplicated region for block: B:38:0x009a  */
    /* JADX WARN: Removed duplicated region for block: B:90:0x018b A[LOOP:6: B:88:0x0185->B:90:0x018b, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:93:0x01a9  */
    /* JADX WARN: Removed duplicated region for block: B:97:0x01ec  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void d(java.util.List<java.lang.String> r21) {
        /*
            Method dump skipped, instructions count: 632
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.rtcconnection.MediaSinkWantsManager.d(java.util.List):void");
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onConnected(MediaEngineConnection mediaEngineConnection, MediaEngineConnection.TransportInfo transportInfo, List<b.a.q.m0.a> list) {
        m.checkNotNullParameter(mediaEngineConnection, "connection");
        m.checkNotNullParameter(transportInfo, "transportInfo");
        m.checkNotNullParameter(list, "supportedVideoCodecs");
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onConnectionStateChange(MediaEngineConnection mediaEngineConnection, MediaEngineConnection.ConnectionState connectionState) {
        m.checkNotNullParameter(mediaEngineConnection, "connection");
        m.checkNotNullParameter(connectionState, "connectionState");
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onDestroy(MediaEngineConnection mediaEngineConnection) {
        m.checkNotNullParameter(mediaEngineConnection, "connection");
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onError(MediaEngineConnection mediaEngineConnection, MediaEngineConnection.FailedConnectionException failedConnectionException) {
        m.checkNotNullParameter(mediaEngineConnection, "connection");
        m.checkNotNullParameter(failedConnectionException, "exception");
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onKrispStatus(MediaEngineConnection mediaEngineConnection, KrispOveruseDetector.Status status) {
        m.checkNotNullParameter(mediaEngineConnection, "connection");
        m.checkNotNullParameter(status, "status");
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onLocalMute(long j, boolean z2) {
        b(new h(this));
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onLocalVideoOffScreen(long j, boolean z2) {
        b(new h(this));
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onSpeaking(long j, int i, boolean z2) {
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onTargetBitrate(int i) {
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onTargetFrameRate(int i) {
    }

    @Override // com.discord.rtcconnection.mediaengine.MediaEngineConnection.d
    public void onVideo(long j, Integer num, int i, int i2, int i3, StreamParameters[] streamParametersArr) {
        m.checkNotNullParameter(streamParametersArr, "streams");
    }
}
