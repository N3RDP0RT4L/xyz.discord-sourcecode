package com.discord.rtcconnection.socket.io;

import andhook.lib.HookHelper;
import androidx.core.app.FrameMetricsAggregator;
import b.d.b.a.a;
import b.i.d.p.b;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.gson.JsonElement;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.webrtc.MediaStreamTrack;
/* compiled from: Payloads.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0011\bÆ\u0002\u0018\u00002\u00020\u0001:\u000e\u0004\u0005\u0006\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0012"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads;", "", HookHelper.constructorName, "()V", "ClientDisconnect", "Description", "Hello", "Identify", "Incoming", "Outgoing", "Protocol", "Ready", "ResolutionType", "Resume", "SessionUpdate", "Speaking", "Stream", "Video", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class Payloads {
    public static final Payloads INSTANCE = new Payloads();

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0080\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0002¢\u0006\u0004\b\u0014\u0010\u0015J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001a\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\t\u001a\u00020\bHÖ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u001a\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0005\u001a\u00020\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0005\u0010\u0012\u001a\u0004\b\u0013\u0010\u0004¨\u0006\u0016"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$ClientDisconnect;", "", "", "component1", "()J", "userId", "copy", "(J)Lcom/discord/rtcconnection/socket/io/Payloads$ClientDisconnect;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "J", "getUserId", HookHelper.constructorName, "(J)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class ClientDisconnect {
        @b("user_id")
        private final long userId;

        public ClientDisconnect(long j) {
            this.userId = j;
        }

        public static /* synthetic */ ClientDisconnect copy$default(ClientDisconnect clientDisconnect, long j, int i, Object obj) {
            if ((i & 1) != 0) {
                j = clientDisconnect.userId;
            }
            return clientDisconnect.copy(j);
        }

        public final long component1() {
            return this.userId;
        }

        public final ClientDisconnect copy(long j) {
            return new ClientDisconnect(j);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof ClientDisconnect) && this.userId == ((ClientDisconnect) obj).userId;
            }
            return true;
        }

        public final long getUserId() {
            return this.userId;
        }

        public int hashCode() {
            return a0.a.a.b.a(this.userId);
        }

        public String toString() {
            return a.B(a.R("ClientDisconnect(userId="), this.userId, ")");
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0002\b\f\b\u0080\b\u0018\u00002\u00020\u0001B7\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u000e\u001a\u00020\u0006\u0012\u0006\u0010\u000f\u001a\u00020\u0006\u0012\u0006\u0010\u0010\u001a\u00020\u0006¢\u0006\u0004\b!\u0010\"J\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003¢\u0006\u0004\b\u0004\u0010\u0005J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0010\u0010\t\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0010\u0010\n\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0010\u0010\u000b\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u000b\u0010\bJJ\u0010\u0011\u001a\u00020\u00002\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00062\b\b\u0002\u0010\u000e\u001a\u00020\u00062\b\b\u0002\u0010\u000f\u001a\u00020\u00062\b\b\u0002\u0010\u0010\u001a\u00020\u0006HÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0013\u0010\bJ\u0010\u0010\u0014\u001a\u00020\u0003HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0015J\u001a\u0010\u0018\u001a\u00020\u00172\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0018\u0010\u0019R\"\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\f\u0010\u001a\u001a\u0004\b\u001b\u0010\u0005R\u001c\u0010\u000e\u001a\u00020\u00068\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u001c\u001a\u0004\b\u001d\u0010\bR\u001c\u0010\u000f\u001a\u00020\u00068\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u001e\u0010\bR\u0019\u0010\u0010\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001c\u001a\u0004\b\u001f\u0010\bR\u001e\u0010\r\u001a\u0004\u0018\u00010\u00068\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\r\u0010\u001c\u001a\u0004\b \u0010\b¨\u0006#"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Description;", "", "", "", "component1", "()Ljava/util/List;", "", "component2", "()Ljava/lang/String;", "component3", "component4", "component5", "secretKey", "mediaSessionId", "audioCodec", "videoCodec", "mode", "copy", "(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/rtcconnection/socket/io/Payloads$Description;", "toString", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/util/List;", "getSecretKey", "Ljava/lang/String;", "getAudioCodec", "getVideoCodec", "getMode", "getMediaSessionId", HookHelper.constructorName, "(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Description {
        @b("audio_codec")
        private final String audioCodec;
        @b("media_session_id")
        private final String mediaSessionId;
        private final String mode;
        @b("secret_key")
        private final List<Integer> secretKey;
        @b("video_codec")
        private final String videoCodec;

        public Description(List<Integer> list, String str, String str2, String str3, String str4) {
            m.checkNotNullParameter(list, "secretKey");
            m.checkNotNullParameter(str2, "audioCodec");
            m.checkNotNullParameter(str3, "videoCodec");
            m.checkNotNullParameter(str4, "mode");
            this.secretKey = list;
            this.mediaSessionId = str;
            this.audioCodec = str2;
            this.videoCodec = str3;
            this.mode = str4;
        }

        public static /* synthetic */ Description copy$default(Description description, List list, String str, String str2, String str3, String str4, int i, Object obj) {
            List<Integer> list2 = list;
            if ((i & 1) != 0) {
                list2 = description.secretKey;
            }
            if ((i & 2) != 0) {
                str = description.mediaSessionId;
            }
            String str5 = str;
            if ((i & 4) != 0) {
                str2 = description.audioCodec;
            }
            String str6 = str2;
            if ((i & 8) != 0) {
                str3 = description.videoCodec;
            }
            String str7 = str3;
            if ((i & 16) != 0) {
                str4 = description.mode;
            }
            return description.copy(list2, str5, str6, str7, str4);
        }

        public final List<Integer> component1() {
            return this.secretKey;
        }

        public final String component2() {
            return this.mediaSessionId;
        }

        public final String component3() {
            return this.audioCodec;
        }

        public final String component4() {
            return this.videoCodec;
        }

        public final String component5() {
            return this.mode;
        }

        public final Description copy(List<Integer> list, String str, String str2, String str3, String str4) {
            m.checkNotNullParameter(list, "secretKey");
            m.checkNotNullParameter(str2, "audioCodec");
            m.checkNotNullParameter(str3, "videoCodec");
            m.checkNotNullParameter(str4, "mode");
            return new Description(list, str, str2, str3, str4);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Description)) {
                return false;
            }
            Description description = (Description) obj;
            return m.areEqual(this.secretKey, description.secretKey) && m.areEqual(this.mediaSessionId, description.mediaSessionId) && m.areEqual(this.audioCodec, description.audioCodec) && m.areEqual(this.videoCodec, description.videoCodec) && m.areEqual(this.mode, description.mode);
        }

        public final String getAudioCodec() {
            return this.audioCodec;
        }

        public final String getMediaSessionId() {
            return this.mediaSessionId;
        }

        public final String getMode() {
            return this.mode;
        }

        public final List<Integer> getSecretKey() {
            return this.secretKey;
        }

        public final String getVideoCodec() {
            return this.videoCodec;
        }

        public int hashCode() {
            List<Integer> list = this.secretKey;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            String str = this.mediaSessionId;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.audioCodec;
            int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.videoCodec;
            int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.mode;
            if (str4 != null) {
                i = str4.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Description(secretKey=");
            R.append(this.secretKey);
            R.append(", mediaSessionId=");
            R.append(this.mediaSessionId);
            R.append(", audioCodec=");
            R.append(this.audioCodec);
            R.append(", videoCodec=");
            R.append(this.videoCodec);
            R.append(", mode=");
            return a.H(R, this.mode, ")");
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\t\b\u0080\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0007J\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001c\u0010\t\u001a\u00020\u00058\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\t\u0010\u0014\u001a\u0004\b\u0015\u0010\u0007R\u001c\u0010\b\u001a\u00020\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\b\u0010\u0016\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Hello;", "", "", "component1", "()J", "", "component2", "()I", "heartbeatIntervalMs", "serverVersion", "copy", "(JI)Lcom/discord/rtcconnection/socket/io/Payloads$Hello;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getServerVersion", "J", "getHeartbeatIntervalMs", HookHelper.constructorName, "(JI)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Hello {
        @b("heartbeat_interval")
        private final long heartbeatIntervalMs;
        @b("v")
        private final int serverVersion;

        public Hello(long j, int i) {
            this.heartbeatIntervalMs = j;
            this.serverVersion = i;
        }

        public static /* synthetic */ Hello copy$default(Hello hello, long j, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                j = hello.heartbeatIntervalMs;
            }
            if ((i2 & 2) != 0) {
                i = hello.serverVersion;
            }
            return hello.copy(j, i);
        }

        public final long component1() {
            return this.heartbeatIntervalMs;
        }

        public final int component2() {
            return this.serverVersion;
        }

        public final Hello copy(long j, int i) {
            return new Hello(j, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Hello)) {
                return false;
            }
            Hello hello = (Hello) obj;
            return this.heartbeatIntervalMs == hello.heartbeatIntervalMs && this.serverVersion == hello.serverVersion;
        }

        public final long getHeartbeatIntervalMs() {
            return this.heartbeatIntervalMs;
        }

        public final int getServerVersion() {
            return this.serverVersion;
        }

        public int hashCode() {
            return (a0.a.a.b.a(this.heartbeatIntervalMs) * 31) + this.serverVersion;
        }

        public String toString() {
            StringBuilder R = a.R("Hello(heartbeatIntervalMs=");
            R.append(this.heartbeatIntervalMs);
            R.append(", serverVersion=");
            return a.A(R, this.serverVersion, ")");
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0012\b\u0080\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0011\u001a\u00020\u0002\u0012\u0006\u0010\u0012\u001a\u00020\u0005\u0012\u0006\u0010\u0013\u001a\u00020\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0015\u001a\u00020\n\u0012\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\u0004\b*\u0010+J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\t\u0010\u0004J\u0010\u0010\u000b\u001a\u00020\nHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rHÆ\u0003¢\u0006\u0004\b\u000f\u0010\u0010JR\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0011\u001a\u00020\u00022\b\b\u0002\u0010\u0012\u001a\u00020\u00052\b\b\u0002\u0010\u0013\u001a\u00020\u00022\b\b\u0002\u0010\u0014\u001a\u00020\u00022\b\b\u0002\u0010\u0015\u001a\u00020\n2\u000e\b\u0002\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000e0\rHÆ\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0019\u0010\u0004J\u0010\u0010\u001b\u001a\u00020\u001aHÖ\u0001¢\u0006\u0004\b\u001b\u0010\u001cJ\u001a\u0010\u001e\u001a\u00020\n2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001e\u0010\u001fR\u001c\u0010\u0011\u001a\u00020\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0011\u0010 \u001a\u0004\b!\u0010\u0004R\u001c\u0010\u0012\u001a\u00020\u00058\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b#\u0010\u0007R\u001c\u0010\u0013\u001a\u00020\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0013\u0010 \u001a\u0004\b$\u0010\u0004R\u0019\u0010\u0015\u001a\u00020\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010%\u001a\u0004\b&\u0010\fR\u0019\u0010\u0014\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010 \u001a\u0004\b'\u0010\u0004R\u001f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010(\u001a\u0004\b)\u0010\u0010¨\u0006,"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Identify;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()J", "component3", "component4", "", "component5", "()Z", "", "Lcom/discord/rtcconnection/socket/io/Payloads$Stream;", "component6", "()Ljava/util/List;", "serverId", "userId", "sessionId", "token", MediaStreamTrack.VIDEO_TRACK_KIND, "streams", "copy", "(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZLjava/util/List;)Lcom/discord/rtcconnection/socket/io/Payloads$Identify;", "toString", "", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getServerId", "J", "getUserId", "getSessionId", "Z", "getVideo", "getToken", "Ljava/util/List;", "getStreams", HookHelper.constructorName, "(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZLjava/util/List;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Identify {
        @b("server_id")
        private final String serverId;
        @b("session_id")
        private final String sessionId;
        private final List<Stream> streams;
        private final String token;
        @b("user_id")
        private final long userId;
        private final boolean video;

        public Identify(String str, long j, String str2, String str3, boolean z2, List<Stream> list) {
            m.checkNotNullParameter(str, "serverId");
            m.checkNotNullParameter(str2, "sessionId");
            m.checkNotNullParameter(str3, "token");
            m.checkNotNullParameter(list, "streams");
            this.serverId = str;
            this.userId = j;
            this.sessionId = str2;
            this.token = str3;
            this.video = z2;
            this.streams = list;
        }

        public static /* synthetic */ Identify copy$default(Identify identify, String str, long j, String str2, String str3, boolean z2, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                str = identify.serverId;
            }
            if ((i & 2) != 0) {
                j = identify.userId;
            }
            long j2 = j;
            if ((i & 4) != 0) {
                str2 = identify.sessionId;
            }
            String str4 = str2;
            if ((i & 8) != 0) {
                str3 = identify.token;
            }
            String str5 = str3;
            if ((i & 16) != 0) {
                z2 = identify.video;
            }
            boolean z3 = z2;
            List<Stream> list2 = list;
            if ((i & 32) != 0) {
                list2 = identify.streams;
            }
            return identify.copy(str, j2, str4, str5, z3, list2);
        }

        public final String component1() {
            return this.serverId;
        }

        public final long component2() {
            return this.userId;
        }

        public final String component3() {
            return this.sessionId;
        }

        public final String component4() {
            return this.token;
        }

        public final boolean component5() {
            return this.video;
        }

        public final List<Stream> component6() {
            return this.streams;
        }

        public final Identify copy(String str, long j, String str2, String str3, boolean z2, List<Stream> list) {
            m.checkNotNullParameter(str, "serverId");
            m.checkNotNullParameter(str2, "sessionId");
            m.checkNotNullParameter(str3, "token");
            m.checkNotNullParameter(list, "streams");
            return new Identify(str, j, str2, str3, z2, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Identify)) {
                return false;
            }
            Identify identify = (Identify) obj;
            return m.areEqual(this.serverId, identify.serverId) && this.userId == identify.userId && m.areEqual(this.sessionId, identify.sessionId) && m.areEqual(this.token, identify.token) && this.video == identify.video && m.areEqual(this.streams, identify.streams);
        }

        public final String getServerId() {
            return this.serverId;
        }

        public final String getSessionId() {
            return this.sessionId;
        }

        public final List<Stream> getStreams() {
            return this.streams;
        }

        public final String getToken() {
            return this.token;
        }

        public final long getUserId() {
            return this.userId;
        }

        public final boolean getVideo() {
            return this.video;
        }

        public int hashCode() {
            String str = this.serverId;
            int i = 0;
            int a = (a0.a.a.b.a(this.userId) + ((str != null ? str.hashCode() : 0) * 31)) * 31;
            String str2 = this.sessionId;
            int hashCode = (a + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.token;
            int hashCode2 = (hashCode + (str3 != null ? str3.hashCode() : 0)) * 31;
            boolean z2 = this.video;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode2 + i2) * 31;
            List<Stream> list = this.streams;
            if (list != null) {
                i = list.hashCode();
            }
            return i4 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Identify(serverId=");
            R.append(this.serverId);
            R.append(", userId=");
            R.append(this.userId);
            R.append(", sessionId=");
            R.append(this.sessionId);
            R.append(", token=");
            R.append(this.token);
            R.append(", video=");
            R.append(this.video);
            R.append(", streams=");
            return a.K(R, this.streams, ")");
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\t\b\u0080\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0005¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J$\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\r\u001a\u00020\fHÖ\u0001¢\u0006\u0004\b\r\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0004J\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001c\u0010\b\u001a\u00020\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\b\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u001c\u0010\t\u001a\u00020\u00058\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\t\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007¨\u0006\u001a"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;", "", "", "component1", "()I", "Lcom/google/gson/JsonElement;", "component2", "()Lcom/google/gson/JsonElement;", "opcode", "data", "copy", "(ILcom/google/gson/JsonElement;)Lcom/discord/rtcconnection/socket/io/Payloads$Incoming;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getOpcode", "Lcom/google/gson/JsonElement;", "getData", HookHelper.constructorName, "(ILcom/google/gson/JsonElement;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Incoming {
        @b("d")
        private final JsonElement data;
        @b("op")
        private final int opcode;

        public Incoming(int i, JsonElement jsonElement) {
            m.checkNotNullParameter(jsonElement, "data");
            this.opcode = i;
            this.data = jsonElement;
        }

        public static /* synthetic */ Incoming copy$default(Incoming incoming, int i, JsonElement jsonElement, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = incoming.opcode;
            }
            if ((i2 & 2) != 0) {
                jsonElement = incoming.data;
            }
            return incoming.copy(i, jsonElement);
        }

        public final int component1() {
            return this.opcode;
        }

        public final JsonElement component2() {
            return this.data;
        }

        public final Incoming copy(int i, JsonElement jsonElement) {
            m.checkNotNullParameter(jsonElement, "data");
            return new Incoming(i, jsonElement);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Incoming)) {
                return false;
            }
            Incoming incoming = (Incoming) obj;
            return this.opcode == incoming.opcode && m.areEqual(this.data, incoming.data);
        }

        public final JsonElement getData() {
            return this.data;
        }

        public final int getOpcode() {
            return this.opcode;
        }

        public int hashCode() {
            int i = this.opcode * 31;
            JsonElement jsonElement = this.data;
            return i + (jsonElement != null ? jsonElement.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = a.R("Incoming(opcode=");
            R.append(this.opcode);
            R.append(", data=");
            R.append(this.data);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\t\b\u0080\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0001¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0001HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J$\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0007\u001a\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\u0001HÆ\u0001¢\u0006\u0004\b\t\u0010\nJ\u0010\u0010\f\u001a\u00020\u000bHÖ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0004J\u001a\u0010\u0011\u001a\u00020\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0007\u001a\u00020\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0007\u0010\u0013\u001a\u0004\b\u0014\u0010\u0004R\u001c\u0010\b\u001a\u00020\u00018\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\b\u0010\u0015\u001a\u0004\b\u0016\u0010\u0006¨\u0006\u0019"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Outgoing;", "", "", "component1", "()I", "component2", "()Ljava/lang/Object;", "opcode", "data", "copy", "(ILjava/lang/Object;)Lcom/discord/rtcconnection/socket/io/Payloads$Outgoing;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getOpcode", "Ljava/lang/Object;", "getData", HookHelper.constructorName, "(ILjava/lang/Object;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Outgoing {
        @b("d")
        private final Object data;
        @b("op")
        private final int opcode;

        public Outgoing(int i, Object obj) {
            m.checkNotNullParameter(obj, "data");
            this.opcode = i;
            this.data = obj;
        }

        public static /* synthetic */ Outgoing copy$default(Outgoing outgoing, int i, Object obj, int i2, Object obj2) {
            if ((i2 & 1) != 0) {
                i = outgoing.opcode;
            }
            if ((i2 & 2) != 0) {
                obj = outgoing.data;
            }
            return outgoing.copy(i, obj);
        }

        public final int component1() {
            return this.opcode;
        }

        public final Object component2() {
            return this.data;
        }

        public final Outgoing copy(int i, Object obj) {
            m.checkNotNullParameter(obj, "data");
            return new Outgoing(i, obj);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Outgoing)) {
                return false;
            }
            Outgoing outgoing = (Outgoing) obj;
            return this.opcode == outgoing.opcode && m.areEqual(this.data, outgoing.data);
        }

        public final Object getData() {
            return this.data;
        }

        public final int getOpcode() {
            return this.opcode;
        }

        public int hashCode() {
            int i = this.opcode * 31;
            Object obj = this.data;
            return i + (obj != null ? obj.hashCode() : 0);
        }

        public String toString() {
            StringBuilder R = a.R("Outgoing(opcode=");
            R.append(this.opcode);
            R.append(", data=");
            R.append(this.data);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\r\b\u0080\b\u0018\u00002\u00020\u0001:\u0002!\"B'\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b¢\u0006\u0004\b\u001f\u0010 J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0018\u0010\n\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJ6\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\u0010\b\u0002\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0004J\u0010\u0010\u0013\u001a\u00020\u0012HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001b\u001a\u0004\b\u001c\u0010\u0007R!\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001d\u001a\u0004\b\u001e\u0010\u000b¨\u0006#"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Protocol;", "", "", "component1", "()Ljava/lang/String;", "Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$ProtocolInfo;", "component2", "()Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$ProtocolInfo;", "", "Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$CodecInfo;", "component3", "()Ljava/util/List;", "protocol", "data", "codecs", "copy", "(Ljava/lang/String;Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$ProtocolInfo;Ljava/util/List;)Lcom/discord/rtcconnection/socket/io/Payloads$Protocol;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getProtocol", "Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$ProtocolInfo;", "getData", "Ljava/util/List;", "getCodecs", HookHelper.constructorName, "(Ljava/lang/String;Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$ProtocolInfo;Ljava/util/List;)V", "CodecInfo", "ProtocolInfo", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Protocol {
        private final List<CodecInfo> codecs;
        private final ProtocolInfo data;
        private final String protocol;

        /* compiled from: Payloads.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0002\b\r\b\u0086\b\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\f\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0005\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b!\u0010\"J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\t\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\t\u0010\u0007J\u0012\u0010\n\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0004\b\n\u0010\u000bJD\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\f\u001a\u00020\u00022\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00052\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0013\u0010\u0004J\u0010\u0010\u0014\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0007J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001c\u0010\u000f\u001a\u00020\u00058\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u000f\u0010\u0019\u001a\u0004\b\u001a\u0010\u0007R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u001e\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0010\u0010\u001d\u001a\u0004\b\u001e\u0010\u000bR\u0019\u0010\f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001b\u001a\u0004\b\u001f\u0010\u0004R\u0019\u0010\r\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b \u0010\u0007¨\u0006#"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$CodecInfo;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()I", "component3", "component4", "component5", "()Ljava/lang/Integer;", ModelAuditLogEntry.CHANGE_KEY_NAME, "priority", "type", "payloadType", "rtxPayloadType", "copy", "(Ljava/lang/String;ILjava/lang/String;ILjava/lang/Integer;)Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$CodecInfo;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getPayloadType", "Ljava/lang/String;", "getType", "Ljava/lang/Integer;", "getRtxPayloadType", "getName", "getPriority", HookHelper.constructorName, "(Ljava/lang/String;ILjava/lang/String;ILjava/lang/Integer;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class CodecInfo {
            private final String name;
            @b("payload_type")
            private final int payloadType;
            private final int priority;
            @b("rtx_payload_type")
            private final Integer rtxPayloadType;
            private final String type;

            public CodecInfo(String str, int i, String str2, int i2, Integer num) {
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkNotNullParameter(str2, "type");
                this.name = str;
                this.priority = i;
                this.type = str2;
                this.payloadType = i2;
                this.rtxPayloadType = num;
            }

            public static /* synthetic */ CodecInfo copy$default(CodecInfo codecInfo, String str, int i, String str2, int i2, Integer num, int i3, Object obj) {
                if ((i3 & 1) != 0) {
                    str = codecInfo.name;
                }
                if ((i3 & 2) != 0) {
                    i = codecInfo.priority;
                }
                int i4 = i;
                if ((i3 & 4) != 0) {
                    str2 = codecInfo.type;
                }
                String str3 = str2;
                if ((i3 & 8) != 0) {
                    i2 = codecInfo.payloadType;
                }
                int i5 = i2;
                if ((i3 & 16) != 0) {
                    num = codecInfo.rtxPayloadType;
                }
                return codecInfo.copy(str, i4, str3, i5, num);
            }

            public final String component1() {
                return this.name;
            }

            public final int component2() {
                return this.priority;
            }

            public final String component3() {
                return this.type;
            }

            public final int component4() {
                return this.payloadType;
            }

            public final Integer component5() {
                return this.rtxPayloadType;
            }

            public final CodecInfo copy(String str, int i, String str2, int i2, Integer num) {
                m.checkNotNullParameter(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
                m.checkNotNullParameter(str2, "type");
                return new CodecInfo(str, i, str2, i2, num);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof CodecInfo)) {
                    return false;
                }
                CodecInfo codecInfo = (CodecInfo) obj;
                return m.areEqual(this.name, codecInfo.name) && this.priority == codecInfo.priority && m.areEqual(this.type, codecInfo.type) && this.payloadType == codecInfo.payloadType && m.areEqual(this.rtxPayloadType, codecInfo.rtxPayloadType);
            }

            public final String getName() {
                return this.name;
            }

            public final int getPayloadType() {
                return this.payloadType;
            }

            public final int getPriority() {
                return this.priority;
            }

            public final Integer getRtxPayloadType() {
                return this.rtxPayloadType;
            }

            public final String getType() {
                return this.type;
            }

            public int hashCode() {
                String str = this.name;
                int i = 0;
                int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.priority) * 31;
                String str2 = this.type;
                int hashCode2 = (((hashCode + (str2 != null ? str2.hashCode() : 0)) * 31) + this.payloadType) * 31;
                Integer num = this.rtxPayloadType;
                if (num != null) {
                    i = num.hashCode();
                }
                return hashCode2 + i;
            }

            public String toString() {
                StringBuilder R = a.R("CodecInfo(name=");
                R.append(this.name);
                R.append(", priority=");
                R.append(this.priority);
                R.append(", type=");
                R.append(this.type);
                R.append(", payloadType=");
                R.append(this.payloadType);
                R.append(", rtxPayloadType=");
                return a.E(R, this.rtxPayloadType, ")");
            }
        }

        /* compiled from: Payloads.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\b\u0010\u0004J.\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u000e\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0007J\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u0019\u0010\n\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0014\u001a\u0004\b\u0018\u0010\u0004¨\u0006\u001b"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$ProtocolInfo;", "", "", "component1", "()Ljava/lang/String;", "", "component2", "()I", "component3", "address", "port", "mode", "copy", "(Ljava/lang/String;ILjava/lang/String;)Lcom/discord/rtcconnection/socket/io/Payloads$Protocol$ProtocolInfo;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getAddress", "I", "getPort", "getMode", HookHelper.constructorName, "(Ljava/lang/String;ILjava/lang/String;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class ProtocolInfo {
            private final String address;
            private final String mode;
            private final int port;

            public ProtocolInfo(String str, int i, String str2) {
                m.checkNotNullParameter(str, "address");
                m.checkNotNullParameter(str2, "mode");
                this.address = str;
                this.port = i;
                this.mode = str2;
            }

            public static /* synthetic */ ProtocolInfo copy$default(ProtocolInfo protocolInfo, String str, int i, String str2, int i2, Object obj) {
                if ((i2 & 1) != 0) {
                    str = protocolInfo.address;
                }
                if ((i2 & 2) != 0) {
                    i = protocolInfo.port;
                }
                if ((i2 & 4) != 0) {
                    str2 = protocolInfo.mode;
                }
                return protocolInfo.copy(str, i, str2);
            }

            public final String component1() {
                return this.address;
            }

            public final int component2() {
                return this.port;
            }

            public final String component3() {
                return this.mode;
            }

            public final ProtocolInfo copy(String str, int i, String str2) {
                m.checkNotNullParameter(str, "address");
                m.checkNotNullParameter(str2, "mode");
                return new ProtocolInfo(str, i, str2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof ProtocolInfo)) {
                    return false;
                }
                ProtocolInfo protocolInfo = (ProtocolInfo) obj;
                return m.areEqual(this.address, protocolInfo.address) && this.port == protocolInfo.port && m.areEqual(this.mode, protocolInfo.mode);
            }

            public final String getAddress() {
                return this.address;
            }

            public final String getMode() {
                return this.mode;
            }

            public final int getPort() {
                return this.port;
            }

            public int hashCode() {
                String str = this.address;
                int i = 0;
                int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.port) * 31;
                String str2 = this.mode;
                if (str2 != null) {
                    i = str2.hashCode();
                }
                return hashCode + i;
            }

            public String toString() {
                StringBuilder R = a.R("ProtocolInfo(address=");
                R.append(this.address);
                R.append(", port=");
                R.append(this.port);
                R.append(", mode=");
                return a.H(R, this.mode, ")");
            }
        }

        public Protocol(String str, ProtocolInfo protocolInfo, List<CodecInfo> list) {
            m.checkNotNullParameter(str, "protocol");
            m.checkNotNullParameter(protocolInfo, "data");
            this.protocol = str;
            this.data = protocolInfo;
            this.codecs = list;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Protocol copy$default(Protocol protocol, String str, ProtocolInfo protocolInfo, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                str = protocol.protocol;
            }
            if ((i & 2) != 0) {
                protocolInfo = protocol.data;
            }
            if ((i & 4) != 0) {
                list = protocol.codecs;
            }
            return protocol.copy(str, protocolInfo, list);
        }

        public final String component1() {
            return this.protocol;
        }

        public final ProtocolInfo component2() {
            return this.data;
        }

        public final List<CodecInfo> component3() {
            return this.codecs;
        }

        public final Protocol copy(String str, ProtocolInfo protocolInfo, List<CodecInfo> list) {
            m.checkNotNullParameter(str, "protocol");
            m.checkNotNullParameter(protocolInfo, "data");
            return new Protocol(str, protocolInfo, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Protocol)) {
                return false;
            }
            Protocol protocol = (Protocol) obj;
            return m.areEqual(this.protocol, protocol.protocol) && m.areEqual(this.data, protocol.data) && m.areEqual(this.codecs, protocol.codecs);
        }

        public final List<CodecInfo> getCodecs() {
            return this.codecs;
        }

        public final ProtocolInfo getData() {
            return this.data;
        }

        public final String getProtocol() {
            return this.protocol;
        }

        public int hashCode() {
            String str = this.protocol;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            ProtocolInfo protocolInfo = this.data;
            int hashCode2 = (hashCode + (protocolInfo != null ? protocolInfo.hashCode() : 0)) * 31;
            List<CodecInfo> list = this.codecs;
            if (list != null) {
                i = list.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Protocol(protocol=");
            R.append(this.protocol);
            R.append(", data=");
            R.append(this.data);
            R.append(", codecs=");
            return a.K(R, this.codecs, ")");
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\f\b\u0080\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0006\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0016\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\tHÆ\u0003¢\u0006\u0004\b\u000b\u0010\fJ>\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\r\u001a\u00020\u00022\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00062\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\n0\tHÆ\u0001¢\u0006\u0004\b\u0011\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u0013\u0010\bJ\u0010\u0010\u0014\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0004J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u0019\u0010\r\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004R\u0019\u0010\u000e\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u0019\u001a\u0004\b\u001b\u0010\u0004R\u0019\u0010\u000f\u001a\u00020\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u001c\u001a\u0004\b\u001d\u0010\bR\u001f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\n0\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u001e\u001a\u0004\b\u001f\u0010\f¨\u0006\""}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Ready;", "", "", "component1", "()I", "component2", "", "component3", "()Ljava/lang/String;", "", "Lcom/discord/rtcconnection/socket/io/Payloads$Stream;", "component4", "()Ljava/util/List;", "ssrc", "port", "ip", "streams", "copy", "(IILjava/lang/String;Ljava/util/List;)Lcom/discord/rtcconnection/socket/io/Payloads$Ready;", "toString", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getSsrc", "getPort", "Ljava/lang/String;", "getIp", "Ljava/util/List;", "getStreams", HookHelper.constructorName, "(IILjava/lang/String;Ljava/util/List;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Ready {
        private final String ip;
        private final int port;
        private final int ssrc;
        private final List<Stream> streams;

        public Ready(int i, int i2, String str, List<Stream> list) {
            m.checkNotNullParameter(str, "ip");
            m.checkNotNullParameter(list, "streams");
            this.ssrc = i;
            this.port = i2;
            this.ip = str;
            this.streams = list;
        }

        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Ready copy$default(Ready ready, int i, int i2, String str, List list, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                i = ready.ssrc;
            }
            if ((i3 & 2) != 0) {
                i2 = ready.port;
            }
            if ((i3 & 4) != 0) {
                str = ready.ip;
            }
            if ((i3 & 8) != 0) {
                list = ready.streams;
            }
            return ready.copy(i, i2, str, list);
        }

        public final int component1() {
            return this.ssrc;
        }

        public final int component2() {
            return this.port;
        }

        public final String component3() {
            return this.ip;
        }

        public final List<Stream> component4() {
            return this.streams;
        }

        public final Ready copy(int i, int i2, String str, List<Stream> list) {
            m.checkNotNullParameter(str, "ip");
            m.checkNotNullParameter(list, "streams");
            return new Ready(i, i2, str, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Ready)) {
                return false;
            }
            Ready ready = (Ready) obj;
            return this.ssrc == ready.ssrc && this.port == ready.port && m.areEqual(this.ip, ready.ip) && m.areEqual(this.streams, ready.streams);
        }

        public final String getIp() {
            return this.ip;
        }

        public final int getPort() {
            return this.port;
        }

        public final int getSsrc() {
            return this.ssrc;
        }

        public final List<Stream> getStreams() {
            return this.streams;
        }

        public int hashCode() {
            int i = ((this.ssrc * 31) + this.port) * 31;
            String str = this.ip;
            int i2 = 0;
            int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
            List<Stream> list = this.streams;
            if (list != null) {
                i2 = list.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder R = a.R("Ready(ssrc=");
            R.append(this.ssrc);
            R.append(", port=");
            R.append(this.port);
            R.append(", ip=");
            R.append(this.ip);
            R.append(", streams=");
            return a.K(R, this.streams, ")");
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$ResolutionType;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Source", "Fixed", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum ResolutionType {
        Source,
        Fixed
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\t\b\u0080\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\u0006\u0010\b\u001a\u00020\u0002\u0012\u0006\u0010\t\u001a\u00020\u0002¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J.\u0010\n\u001a\u00020\u00002\b\b\u0002\u0010\u0007\u001a\u00020\u00022\b\b\u0002\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\t\u001a\u00020\u0002HÆ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0010\u0010\f\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\f\u0010\u0004J\u0010\u0010\u000e\u001a\u00020\rHÖ\u0001¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0012\u001a\u00020\u00112\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0012\u0010\u0013R\u001c\u0010\b\u001a\u00020\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\b\u0010\u0014\u001a\u0004\b\u0015\u0010\u0004R\u0019\u0010\u0007\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0014\u001a\u0004\b\u0016\u0010\u0004R\u001c\u0010\t\u001a\u00020\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\t\u0010\u0014\u001a\u0004\b\u0017\u0010\u0004¨\u0006\u001a"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Resume;", "", "", "component1", "()Ljava/lang/String;", "component2", "component3", "token", "sessionId", "serverId", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/discord/rtcconnection/socket/io/Payloads$Resume;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getSessionId", "getToken", "getServerId", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Resume {
        @b("server_id")
        private final String serverId;
        @b("session_id")
        private final String sessionId;
        private final String token;

        public Resume(String str, String str2, String str3) {
            a.n0(str, "token", str2, "sessionId", str3, "serverId");
            this.token = str;
            this.sessionId = str2;
            this.serverId = str3;
        }

        public static /* synthetic */ Resume copy$default(Resume resume, String str, String str2, String str3, int i, Object obj) {
            if ((i & 1) != 0) {
                str = resume.token;
            }
            if ((i & 2) != 0) {
                str2 = resume.sessionId;
            }
            if ((i & 4) != 0) {
                str3 = resume.serverId;
            }
            return resume.copy(str, str2, str3);
        }

        public final String component1() {
            return this.token;
        }

        public final String component2() {
            return this.sessionId;
        }

        public final String component3() {
            return this.serverId;
        }

        public final Resume copy(String str, String str2, String str3) {
            m.checkNotNullParameter(str, "token");
            m.checkNotNullParameter(str2, "sessionId");
            m.checkNotNullParameter(str3, "serverId");
            return new Resume(str, str2, str3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Resume)) {
                return false;
            }
            Resume resume = (Resume) obj;
            return m.areEqual(this.token, resume.token) && m.areEqual(this.sessionId, resume.sessionId) && m.areEqual(this.serverId, resume.serverId);
        }

        public final String getServerId() {
            return this.serverId;
        }

        public final String getSessionId() {
            return this.sessionId;
        }

        public final String getToken() {
            return this.token;
        }

        public int hashCode() {
            String str = this.token;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.sessionId;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.serverId;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Resume(token=");
            R.append(this.token);
            R.append(", sessionId=");
            R.append(this.sessionId);
            R.append(", serverId=");
            return a.H(R, this.serverId, ")");
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0080\b\u0018\u00002\u00020\u0001B\u0011\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u001c\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\b\u0010\u0004J\u0010\u0010\n\u001a\u00020\tHÖ\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u001a\u0010\u000e\u001a\u00020\r2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u000e\u0010\u000fR\u001e\u0010\u0005\u001a\u0004\u0018\u00010\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0005\u0010\u0010\u001a\u0004\b\u0011\u0010\u0004¨\u0006\u0014"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$SessionUpdate;", "", "", "component1", "()Ljava/lang/String;", "mediaSessionId", "copy", "(Ljava/lang/String;)Lcom/discord/rtcconnection/socket/io/Payloads$SessionUpdate;", "toString", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/String;", "getMediaSessionId", HookHelper.constructorName, "(Ljava/lang/String;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class SessionUpdate {
        @b("media_session_id")
        private final String mediaSessionId;

        public SessionUpdate(String str) {
            this.mediaSessionId = str;
        }

        public static /* synthetic */ SessionUpdate copy$default(SessionUpdate sessionUpdate, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = sessionUpdate.mediaSessionId;
            }
            return sessionUpdate.copy(str);
        }

        public final String component1() {
            return this.mediaSessionId;
        }

        public final SessionUpdate copy(String str) {
            return new SessionUpdate(str);
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof SessionUpdate) && m.areEqual(this.mediaSessionId, ((SessionUpdate) obj).mediaSessionId);
            }
            return true;
        }

        public final String getMediaSessionId() {
            return this.mediaSessionId;
        }

        public int hashCode() {
            String str = this.mediaSessionId;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return a.H(a.R("SessionUpdate(mediaSessionId="), this.mediaSessionId, ")");
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\r\b\u0080\b\u0018\u0000 \"2\u00020\u0001:\u0001\"B3\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\b¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0006J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0007\u0010\u0006J\u0012\u0010\t\u001a\u0004\u0018\u00010\bHÆ\u0003¢\u0006\u0004\b\t\u0010\nJ>\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u000b\u001a\u00020\u00022\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\bHÆ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u0011HÖ\u0001¢\u0006\u0004\b\u0012\u0010\u0013J\u0010\u0010\u0014\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0014\u0010\u0004J\u001a\u0010\u0017\u001a\u00020\u00162\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0017\u0010\u0018R\u001e\u0010\u000e\u001a\u0004\u0018\u00010\b8\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u0019\u001a\u0004\b\u001a\u0010\nR\u0019\u0010\u000b\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001b\u001a\u0004\b\u001c\u0010\u0004R\u001b\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u001d\u001a\u0004\b\u001e\u0010\u0006R\u001b\u0010\f\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\u001d\u001a\u0004\b\u001f\u0010\u0006¨\u0006#"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;", "", "", "component1", "()I", "component2", "()Ljava/lang/Integer;", "component3", "", "component4", "()Ljava/lang/Long;", "ssrc", "speaking", "delay", "userId", "copy", "(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)Lcom/discord/rtcconnection/socket/io/Payloads$Speaking;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getUserId", "I", "getSsrc", "Ljava/lang/Integer;", "getDelay", "getSpeaking", HookHelper.constructorName, "(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V", "Companion", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Speaking {
        public static final Companion Companion = new Companion(null);
        public static final int NOT_SPEAKING = 0;
        public static final int SPEAKING = 1;
        private final Integer delay;
        private final Integer speaking;
        private final int ssrc;
        @b("user_id")
        private final Long userId;

        /* compiled from: Payloads.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u0016\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0003\u0010\u0004R\u0016\u0010\u0005\u001a\u00020\u00028\u0006@\u0006X\u0086T¢\u0006\u0006\n\u0004\b\u0005\u0010\u0004¨\u0006\b"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Speaking$Companion;", "", "", "NOT_SPEAKING", "I", "SPEAKING", HookHelper.constructorName, "()V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }
        }

        public Speaking(int i, Integer num, Integer num2, Long l) {
            this.ssrc = i;
            this.speaking = num;
            this.delay = num2;
            this.userId = l;
        }

        public static /* synthetic */ Speaking copy$default(Speaking speaking, int i, Integer num, Integer num2, Long l, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = speaking.ssrc;
            }
            if ((i2 & 2) != 0) {
                num = speaking.speaking;
            }
            if ((i2 & 4) != 0) {
                num2 = speaking.delay;
            }
            if ((i2 & 8) != 0) {
                l = speaking.userId;
            }
            return speaking.copy(i, num, num2, l);
        }

        public final int component1() {
            return this.ssrc;
        }

        public final Integer component2() {
            return this.speaking;
        }

        public final Integer component3() {
            return this.delay;
        }

        public final Long component4() {
            return this.userId;
        }

        public final Speaking copy(int i, Integer num, Integer num2, Long l) {
            return new Speaking(i, num, num2, l);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Speaking)) {
                return false;
            }
            Speaking speaking = (Speaking) obj;
            return this.ssrc == speaking.ssrc && m.areEqual(this.speaking, speaking.speaking) && m.areEqual(this.delay, speaking.delay) && m.areEqual(this.userId, speaking.userId);
        }

        public final Integer getDelay() {
            return this.delay;
        }

        public final Integer getSpeaking() {
            return this.speaking;
        }

        public final int getSsrc() {
            return this.ssrc;
        }

        public final Long getUserId() {
            return this.userId;
        }

        public int hashCode() {
            int i = this.ssrc * 31;
            Integer num = this.speaking;
            int i2 = 0;
            int hashCode = (i + (num != null ? num.hashCode() : 0)) * 31;
            Integer num2 = this.delay;
            int hashCode2 = (hashCode + (num2 != null ? num2.hashCode() : 0)) * 31;
            Long l = this.userId;
            if (l != null) {
                i2 = l.hashCode();
            }
            return hashCode2 + i2;
        }

        public String toString() {
            StringBuilder R = a.R("Speaking(ssrc=");
            R.append(this.ssrc);
            R.append(", speaking=");
            R.append(this.speaking);
            R.append(", delay=");
            R.append(this.delay);
            R.append(", userId=");
            return a.F(R, this.userId, ")");
        }

        public /* synthetic */ Speaking(int i, Integer num, Integer num2, Long l, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(i, (i2 & 2) != 0 ? null : num, (i2 & 4) != 0 ? null : num2, (i2 & 8) != 0 ? null : l);
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b%\b\u0080\b\u0018\u00002\u00020\u0001:\u00013Bs\u0012\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0006¢\u0006\u0004\b1\u00102J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0007\u0010\bJ\u0012\u0010\t\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\t\u0010\bJ\u0012\u0010\n\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\n\u0010\bJ\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u000b\u0010\bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\fHÆ\u0003¢\u0006\u0004\b\r\u0010\u000eJ\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u000fHÆ\u0003¢\u0006\u0004\b\u0010\u0010\u0011J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0004\b\u0012\u0010\bJ|\u0010\u001c\u001a\u00020\u00002\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\f2\n\b\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u0006HÆ\u0001¢\u0006\u0004\b\u001c\u0010\u001dJ\u0010\u0010\u001e\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u001e\u0010\u0004J\u0010\u0010\u001f\u001a\u00020\u0006HÖ\u0001¢\u0006\u0004\b\u001f\u0010 J\u001a\u0010\"\u001a\u00020\u000f2\b\u0010!\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\"\u0010#R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010$\u001a\u0004\b%\u0010\bR\u001b\u0010\u0014\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010&\u001a\u0004\b'\u0010\u0004R\u001b\u0010\u001a\u001a\u0004\u0018\u00010\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010(\u001a\u0004\b)\u0010\u0011R\u001b\u0010\u0017\u001a\u0004\u0018\u00010\u00068\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010$\u001a\u0004\b*\u0010\bR\u001e\u0010\u001b\u001a\u0004\u0018\u00010\u00068\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u001b\u0010$\u001a\u0004\b+\u0010\bR\u001e\u0010\u0015\u001a\u0004\u0018\u00010\u00068\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0015\u0010$\u001a\u0004\b,\u0010\bR\u001e\u0010\u0019\u001a\u0004\u0018\u00010\f8\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0019\u0010-\u001a\u0004\b.\u0010\u000eR\u001e\u0010\u0018\u001a\u0004\u0018\u00010\u00068\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0018\u0010$\u001a\u0004\b/\u0010\bR\u001b\u0010\u0013\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010&\u001a\u0004\b0\u0010\u0004¨\u00064"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Stream;", "", "", "component1", "()Ljava/lang/String;", "component2", "", "component3", "()Ljava/lang/Integer;", "component4", "component5", "component6", "Lcom/discord/rtcconnection/socket/io/Payloads$Stream$MaxResolution;", "component7", "()Lcom/discord/rtcconnection/socket/io/Payloads$Stream$MaxResolution;", "", "component8", "()Ljava/lang/Boolean;", "component9", "type", "rid", "maxFrameRate", "quality", "ssrc", "rtxSsrc", "maxResolution", "active", "maxBitrate", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/rtcconnection/socket/io/Payloads$Stream$MaxResolution;Ljava/lang/Boolean;Ljava/lang/Integer;)Lcom/discord/rtcconnection/socket/io/Payloads$Stream;", "toString", "hashCode", "()I", "other", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Integer;", "getQuality", "Ljava/lang/String;", "getRid", "Ljava/lang/Boolean;", "getActive", "getSsrc", "getMaxBitrate", "getMaxFrameRate", "Lcom/discord/rtcconnection/socket/io/Payloads$Stream$MaxResolution;", "getMaxResolution", "getRtxSsrc", "getType", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/discord/rtcconnection/socket/io/Payloads$Stream$MaxResolution;Ljava/lang/Boolean;Ljava/lang/Integer;)V", "MaxResolution", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Stream {
        private final Boolean active;
        @b("max_bitrate")
        private final Integer maxBitrate;
        @b("max_framerate")
        private final Integer maxFrameRate;
        @b("max_resolution")
        private final MaxResolution maxResolution;
        private final Integer quality;
        private final String rid;
        @b("rtx_ssrc")
        private final Integer rtxSsrc;
        private final Integer ssrc;
        private final String type;

        /* compiled from: Payloads.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\b\u0086\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005¢\u0006\u0004\b\u001b\u0010\u001cJ\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\b\u001a\u00020\u0005HÆ\u0003¢\u0006\u0004\b\b\u0010\u0007J.\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\t\u001a\u00020\u00022\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0005HÆ\u0001¢\u0006\u0004\b\f\u0010\rJ\u0010\u0010\u000f\u001a\u00020\u000eHÖ\u0001¢\u0006\u0004\b\u000f\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0011\u0010\u0007J\u001a\u0010\u0014\u001a\u00020\u00132\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u0014\u0010\u0015R\u0019\u0010\n\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u0016\u001a\u0004\b\u0017\u0010\u0007R\u0019\u0010\u000b\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u0016\u001a\u0004\b\u0018\u0010\u0007R\u0019\u0010\t\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\t\u0010\u0019\u001a\u0004\b\u001a\u0010\u0004¨\u0006\u001d"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Stream$MaxResolution;", "", "Lcom/discord/rtcconnection/socket/io/Payloads$ResolutionType;", "component1", "()Lcom/discord/rtcconnection/socket/io/Payloads$ResolutionType;", "", "component2", "()I", "component3", "type", "width", "height", "copy", "(Lcom/discord/rtcconnection/socket/io/Payloads$ResolutionType;II)Lcom/discord/rtcconnection/socket/io/Payloads$Stream$MaxResolution;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "I", "getWidth", "getHeight", "Lcom/discord/rtcconnection/socket/io/Payloads$ResolutionType;", "getType", HookHelper.constructorName, "(Lcom/discord/rtcconnection/socket/io/Payloads$ResolutionType;II)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
        /* loaded from: classes.dex */
        public static final class MaxResolution {
            private final int height;
            private final ResolutionType type;
            private final int width;

            public MaxResolution(ResolutionType resolutionType, int i, int i2) {
                m.checkNotNullParameter(resolutionType, "type");
                this.type = resolutionType;
                this.width = i;
                this.height = i2;
            }

            public static /* synthetic */ MaxResolution copy$default(MaxResolution maxResolution, ResolutionType resolutionType, int i, int i2, int i3, Object obj) {
                if ((i3 & 1) != 0) {
                    resolutionType = maxResolution.type;
                }
                if ((i3 & 2) != 0) {
                    i = maxResolution.width;
                }
                if ((i3 & 4) != 0) {
                    i2 = maxResolution.height;
                }
                return maxResolution.copy(resolutionType, i, i2);
            }

            public final ResolutionType component1() {
                return this.type;
            }

            public final int component2() {
                return this.width;
            }

            public final int component3() {
                return this.height;
            }

            public final MaxResolution copy(ResolutionType resolutionType, int i, int i2) {
                m.checkNotNullParameter(resolutionType, "type");
                return new MaxResolution(resolutionType, i, i2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof MaxResolution)) {
                    return false;
                }
                MaxResolution maxResolution = (MaxResolution) obj;
                return m.areEqual(this.type, maxResolution.type) && this.width == maxResolution.width && this.height == maxResolution.height;
            }

            public final int getHeight() {
                return this.height;
            }

            public final ResolutionType getType() {
                return this.type;
            }

            public final int getWidth() {
                return this.width;
            }

            public int hashCode() {
                ResolutionType resolutionType = this.type;
                return ((((resolutionType != null ? resolutionType.hashCode() : 0) * 31) + this.width) * 31) + this.height;
            }

            public String toString() {
                StringBuilder R = a.R("MaxResolution(type=");
                R.append(this.type);
                R.append(", width=");
                R.append(this.width);
                R.append(", height=");
                return a.A(R, this.height, ")");
            }
        }

        public Stream() {
            this(null, null, null, null, null, null, null, null, null, FrameMetricsAggregator.EVERY_DURATION, null);
        }

        public Stream(String str, String str2, Integer num, Integer num2, Integer num3, Integer num4, MaxResolution maxResolution, Boolean bool, Integer num5) {
            this.type = str;
            this.rid = str2;
            this.maxFrameRate = num;
            this.quality = num2;
            this.ssrc = num3;
            this.rtxSsrc = num4;
            this.maxResolution = maxResolution;
            this.active = bool;
            this.maxBitrate = num5;
        }

        public final String component1() {
            return this.type;
        }

        public final String component2() {
            return this.rid;
        }

        public final Integer component3() {
            return this.maxFrameRate;
        }

        public final Integer component4() {
            return this.quality;
        }

        public final Integer component5() {
            return this.ssrc;
        }

        public final Integer component6() {
            return this.rtxSsrc;
        }

        public final MaxResolution component7() {
            return this.maxResolution;
        }

        public final Boolean component8() {
            return this.active;
        }

        public final Integer component9() {
            return this.maxBitrate;
        }

        public final Stream copy(String str, String str2, Integer num, Integer num2, Integer num3, Integer num4, MaxResolution maxResolution, Boolean bool, Integer num5) {
            return new Stream(str, str2, num, num2, num3, num4, maxResolution, bool, num5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Stream)) {
                return false;
            }
            Stream stream = (Stream) obj;
            return m.areEqual(this.type, stream.type) && m.areEqual(this.rid, stream.rid) && m.areEqual(this.maxFrameRate, stream.maxFrameRate) && m.areEqual(this.quality, stream.quality) && m.areEqual(this.ssrc, stream.ssrc) && m.areEqual(this.rtxSsrc, stream.rtxSsrc) && m.areEqual(this.maxResolution, stream.maxResolution) && m.areEqual(this.active, stream.active) && m.areEqual(this.maxBitrate, stream.maxBitrate);
        }

        public final Boolean getActive() {
            return this.active;
        }

        public final Integer getMaxBitrate() {
            return this.maxBitrate;
        }

        public final Integer getMaxFrameRate() {
            return this.maxFrameRate;
        }

        public final MaxResolution getMaxResolution() {
            return this.maxResolution;
        }

        public final Integer getQuality() {
            return this.quality;
        }

        public final String getRid() {
            return this.rid;
        }

        public final Integer getRtxSsrc() {
            return this.rtxSsrc;
        }

        public final Integer getSsrc() {
            return this.ssrc;
        }

        public final String getType() {
            return this.type;
        }

        public int hashCode() {
            String str = this.type;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.rid;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Integer num = this.maxFrameRate;
            int hashCode3 = (hashCode2 + (num != null ? num.hashCode() : 0)) * 31;
            Integer num2 = this.quality;
            int hashCode4 = (hashCode3 + (num2 != null ? num2.hashCode() : 0)) * 31;
            Integer num3 = this.ssrc;
            int hashCode5 = (hashCode4 + (num3 != null ? num3.hashCode() : 0)) * 31;
            Integer num4 = this.rtxSsrc;
            int hashCode6 = (hashCode5 + (num4 != null ? num4.hashCode() : 0)) * 31;
            MaxResolution maxResolution = this.maxResolution;
            int hashCode7 = (hashCode6 + (maxResolution != null ? maxResolution.hashCode() : 0)) * 31;
            Boolean bool = this.active;
            int hashCode8 = (hashCode7 + (bool != null ? bool.hashCode() : 0)) * 31;
            Integer num5 = this.maxBitrate;
            if (num5 != null) {
                i = num5.hashCode();
            }
            return hashCode8 + i;
        }

        public String toString() {
            StringBuilder R = a.R("Stream(type=");
            R.append(this.type);
            R.append(", rid=");
            R.append(this.rid);
            R.append(", maxFrameRate=");
            R.append(this.maxFrameRate);
            R.append(", quality=");
            R.append(this.quality);
            R.append(", ssrc=");
            R.append(this.ssrc);
            R.append(", rtxSsrc=");
            R.append(this.rtxSsrc);
            R.append(", maxResolution=");
            R.append(this.maxResolution);
            R.append(", active=");
            R.append(this.active);
            R.append(", maxBitrate=");
            return a.E(R, this.maxBitrate, ")");
        }

        public /* synthetic */ Stream(String str, String str2, Integer num, Integer num2, Integer num3, Integer num4, MaxResolution maxResolution, Boolean bool, Integer num5, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : num, (i & 8) != 0 ? null : num2, (i & 16) != 0 ? null : num3, (i & 32) != 0 ? null : num4, (i & 64) != 0 ? null : maxResolution, (i & 128) != 0 ? null : bool, (i & 256) == 0 ? num5 : null);
        }
    }

    /* compiled from: Payloads.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\r\b\u0080\b\u0018\u00002\u00020\u0001B;\u0012\u0006\u0010\u000e\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u0002\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0007\u0012\u000e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n¢\u0006\u0004\b%\u0010&J\u0010\u0010\u0003\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0005\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0002HÆ\u0003¢\u0006\u0004\b\u0006\u0010\u0004J\u0012\u0010\b\u001a\u0004\u0018\u00010\u0007HÆ\u0003¢\u0006\u0004\b\b\u0010\tJ\u0018\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nHÆ\u0003¢\u0006\u0004\b\f\u0010\rJL\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u000e\u001a\u00020\u00022\b\b\u0002\u0010\u000f\u001a\u00020\u00022\b\b\u0002\u0010\u0010\u001a\u00020\u00022\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00072\u0010\b\u0002\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\nHÆ\u0001¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0016\u001a\u00020\u0015HÖ\u0001¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0018\u0010\u0004J\u001a\u0010\u001b\u001a\u00020\u001a2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\u001b\u0010\u001cR\u001e\u0010\u0011\u001a\u0004\u0018\u00010\u00078\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0011\u0010\u001d\u001a\u0004\b\u001e\u0010\tR\u001c\u0010\u000f\u001a\u00020\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u000f\u0010\u001f\u001a\u0004\b \u0010\u0004R\u001c\u0010\u000e\u001a\u00020\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u000e\u0010\u001f\u001a\u0004\b!\u0010\u0004R!\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u000b\u0018\u00010\n8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\"\u001a\u0004\b#\u0010\rR\u001c\u0010\u0010\u001a\u00020\u00028\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b\u0010\u0010\u001f\u001a\u0004\b$\u0010\u0004¨\u0006'"}, d2 = {"Lcom/discord/rtcconnection/socket/io/Payloads$Video;", "", "", "component1", "()I", "component2", "component3", "", "component4", "()Ljava/lang/Long;", "", "Lcom/discord/rtcconnection/socket/io/Payloads$Stream;", "component5", "()Ljava/util/List;", "audioSsrc", "videoSsrc", "rtxSsrc", "userId", "streams", "copy", "(IIILjava/lang/Long;Ljava/util/List;)Lcom/discord/rtcconnection/socket/io/Payloads$Video;", "", "toString", "()Ljava/lang/String;", "hashCode", "other", "", "equals", "(Ljava/lang/Object;)Z", "Ljava/lang/Long;", "getUserId", "I", "getVideoSsrc", "getAudioSsrc", "Ljava/util/List;", "getStreams", "getRtxSsrc", HookHelper.constructorName, "(IIILjava/lang/Long;Ljava/util/List;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Video {
        @b("audio_ssrc")
        private final int audioSsrc;
        @b("rtx_ssrc")
        private final int rtxSsrc;
        private final List<Stream> streams;
        @b("user_id")
        private final Long userId;
        @b("video_ssrc")
        private final int videoSsrc;

        public Video(int i, int i2, int i3, Long l, List<Stream> list) {
            this.audioSsrc = i;
            this.videoSsrc = i2;
            this.rtxSsrc = i3;
            this.userId = l;
            this.streams = list;
        }

        public static /* synthetic */ Video copy$default(Video video, int i, int i2, int i3, Long l, List list, int i4, Object obj) {
            if ((i4 & 1) != 0) {
                i = video.audioSsrc;
            }
            if ((i4 & 2) != 0) {
                i2 = video.videoSsrc;
            }
            int i5 = i2;
            if ((i4 & 4) != 0) {
                i3 = video.rtxSsrc;
            }
            int i6 = i3;
            if ((i4 & 8) != 0) {
                l = video.userId;
            }
            Long l2 = l;
            List<Stream> list2 = list;
            if ((i4 & 16) != 0) {
                list2 = video.streams;
            }
            return video.copy(i, i5, i6, l2, list2);
        }

        public final int component1() {
            return this.audioSsrc;
        }

        public final int component2() {
            return this.videoSsrc;
        }

        public final int component3() {
            return this.rtxSsrc;
        }

        public final Long component4() {
            return this.userId;
        }

        public final List<Stream> component5() {
            return this.streams;
        }

        public final Video copy(int i, int i2, int i3, Long l, List<Stream> list) {
            return new Video(i, i2, i3, l, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Video)) {
                return false;
            }
            Video video = (Video) obj;
            return this.audioSsrc == video.audioSsrc && this.videoSsrc == video.videoSsrc && this.rtxSsrc == video.rtxSsrc && m.areEqual(this.userId, video.userId) && m.areEqual(this.streams, video.streams);
        }

        public final int getAudioSsrc() {
            return this.audioSsrc;
        }

        public final int getRtxSsrc() {
            return this.rtxSsrc;
        }

        public final List<Stream> getStreams() {
            return this.streams;
        }

        public final Long getUserId() {
            return this.userId;
        }

        public final int getVideoSsrc() {
            return this.videoSsrc;
        }

        public int hashCode() {
            int i = ((((this.audioSsrc * 31) + this.videoSsrc) * 31) + this.rtxSsrc) * 31;
            Long l = this.userId;
            int i2 = 0;
            int hashCode = (i + (l != null ? l.hashCode() : 0)) * 31;
            List<Stream> list = this.streams;
            if (list != null) {
                i2 = list.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder R = a.R("Video(audioSsrc=");
            R.append(this.audioSsrc);
            R.append(", videoSsrc=");
            R.append(this.videoSsrc);
            R.append(", rtxSsrc=");
            R.append(this.rtxSsrc);
            R.append(", userId=");
            R.append(this.userId);
            R.append(", streams=");
            return a.K(R, this.streams, ")");
        }

        public /* synthetic */ Video(int i, int i2, int i3, Long l, List list, int i4, DefaultConstructorMarker defaultConstructorMarker) {
            this(i, i2, i3, (i4 & 8) != 0 ? null : l, list);
        }
    }

    private Payloads() {
    }
}
