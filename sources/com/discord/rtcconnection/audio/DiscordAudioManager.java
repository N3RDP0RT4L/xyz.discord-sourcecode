package com.discord.rtcconnection.audio;

import andhook.lib.HookHelper;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.MainThread;
import b.a.q.k0.h;
import b.a.q.k0.i;
import b.a.q.l0.a;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.rtcconnection.enums.AudioManagerBroadcastAction;
import com.discord.rtcconnection.enums.BluetoothBroadcastAction;
import com.discord.rtcconnection.enums.BluetoothHeadsetAudioState;
import com.discord.rtcconnection.enums.BluetoothProfileConnectionState;
import com.discord.rtcconnection.enums.ScoAudioState;
import com.discord.utilities.lifecycle.ApplicationProvider;
import d0.g;
import d0.t.n;
import d0.t.o;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function0;
import org.webrtc.MediaStreamTrack;
import org.webrtc.ThreadUtils;
import rx.subjects.BehaviorSubject;
import rx.subjects.SerializedSubject;
/* compiled from: DiscordAudioManager.kt */
/* loaded from: classes.dex */
public final class DiscordAudioManager {
    public static final Lazy a = g.lazy(d.j);

    /* renamed from: b  reason: collision with root package name */
    public static final AudioDevice f2756b = new AudioDevice(null, false, null, null, 15);
    public static final List<DeviceTypes> c = n.listOf((Object[]) new DeviceTypes[]{DeviceTypes.EARPIECE, DeviceTypes.SPEAKERPHONE, DeviceTypes.BLUETOOTH_HEADSET, DeviceTypes.WIRED_HEADSET});
    public static final DiscordAudioManager d = null;
    public boolean A;
    public boolean B;
    public boolean C;
    public boolean D;
    public final AudioManager e;
    public final BluetoothManager f;
    public final Lazy g;
    public final boolean h;
    public final long j;
    public final ContentResolver k;
    public final b.a.q.k0.c l;
    public final b.a.q.k0.a m;
    public BluetoothHeadset n;
    public AudioManager.OnAudioFocusChangeListener p;
    public AudioFocusRequest q;
    public List<AudioDevice> r;

    /* renamed from: s  reason: collision with root package name */
    public final SerializedSubject<List<AudioDevice>, List<AudioDevice>> f2757s;
    public DeviceTypes t;
    public final SerializedSubject<DeviceTypes, DeviceTypes> u;
    public ContentObserver v;
    public int w;

    /* renamed from: x  reason: collision with root package name */
    public final int f2758x;

    /* renamed from: y  reason: collision with root package name */
    public final SerializedSubject<Integer, Integer> f2759y;

    /* renamed from: z  reason: collision with root package name */
    public DeviceTypes f2760z;
    public final Object i = this;
    public BluetoothScoState o = BluetoothScoState.INVALID;

    /* compiled from: DiscordAudioManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000f\b\u0086\b\u0018\u00002\u00020\u0001B3\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u0010\u0012\b\b\u0002\u0010\u000e\u001a\u00020\t\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u001d\u0010\u001eJ\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\u000e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u0019\u0010\u0015\u001a\u00020\u00108\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u001b\u0010\u0019\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0004R\u001b\u0010\u001c\u001a\u0004\u0018\u00010\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u0017\u001a\u0004\b\u001b\u0010\u0004¨\u0006\u001f"}, d2 = {"Lcom/discord/rtcconnection/audio/DiscordAudioManager$AudioDevice;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "b", "Z", "isAvailable", "()Z", "Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;", "a", "Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;", "getType", "()Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;", "type", "c", "Ljava/lang/String;", "getId", ModelAuditLogEntry.CHANGE_KEY_ID, "d", "getName", ModelAuditLogEntry.CHANGE_KEY_NAME, HookHelper.constructorName, "(Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;ZLjava/lang/String;Ljava/lang/String;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AudioDevice {
        public final DeviceTypes a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f2761b;
        public final String c;
        public final String d;

        public AudioDevice() {
            this(null, false, null, null, 15);
        }

        public AudioDevice(DeviceTypes deviceTypes, boolean z2, String str, String str2) {
            m.checkNotNullParameter(deviceTypes, "type");
            this.a = deviceTypes;
            this.f2761b = z2;
            this.c = str;
            this.d = str2;
        }

        public static AudioDevice a(AudioDevice audioDevice, DeviceTypes deviceTypes, boolean z2, String str, String str2, int i) {
            DeviceTypes deviceTypes2 = (i & 1) != 0 ? audioDevice.a : null;
            if ((i & 2) != 0) {
                z2 = audioDevice.f2761b;
            }
            if ((i & 4) != 0) {
                str = audioDevice.c;
            }
            if ((i & 8) != 0) {
                str2 = audioDevice.d;
            }
            Objects.requireNonNull(audioDevice);
            m.checkNotNullParameter(deviceTypes2, "type");
            return new AudioDevice(deviceTypes2, z2, str, str2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AudioDevice)) {
                return false;
            }
            AudioDevice audioDevice = (AudioDevice) obj;
            return m.areEqual(this.a, audioDevice.a) && this.f2761b == audioDevice.f2761b && m.areEqual(this.c, audioDevice.c) && m.areEqual(this.d, audioDevice.d);
        }

        public int hashCode() {
            DeviceTypes deviceTypes = this.a;
            int i = 0;
            int hashCode = (deviceTypes != null ? deviceTypes.hashCode() : 0) * 31;
            boolean z2 = this.f2761b;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (hashCode + i2) * 31;
            String str = this.c;
            int hashCode2 = (i4 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.d;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("AudioDevice(type=");
            R.append(this.a);
            R.append(", isAvailable=");
            R.append(this.f2761b);
            R.append(", id=");
            R.append(this.c);
            R.append(", name=");
            return b.d.b.a.a.H(R, this.d, ")");
        }

        public AudioDevice(DeviceTypes deviceTypes, boolean z2, String str, String str2, int i) {
            deviceTypes = (i & 1) != 0 ? DeviceTypes.INVALID : deviceTypes;
            z2 = (i & 2) != 0 ? false : z2;
            int i2 = i & 4;
            int i3 = i & 8;
            m.checkNotNullParameter(deviceTypes, "type");
            this.a = deviceTypes;
            this.f2761b = z2;
            this.c = null;
            this.d = null;
        }
    }

    /* compiled from: DiscordAudioManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\f\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000e"}, d2 = {"Lcom/discord/rtcconnection/audio/DiscordAudioManager$BluetoothScoState;", "", "", "value", "I", "getValue", "()I", HookHelper.constructorName, "(Ljava/lang/String;II)V", "INVALID", "OFF", "ON", "TURNING_ON", "TURNING_OFF", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum BluetoothScoState {
        INVALID(-1),
        OFF(0),
        ON(1),
        TURNING_ON(2),
        TURNING_OFF(3);
        
        private final int value;

        BluetoothScoState(int i) {
            this.value = i;
        }

        public final int getValue() {
            return this.value;
        }
    }

    /* compiled from: DiscordAudioManager.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\r\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/rtcconnection/audio/DiscordAudioManager$DeviceTypes;", "", "", "value", "I", "getValue", "()I", HookHelper.constructorName, "(Ljava/lang/String;II)V", "DEFAULT", "INVALID", "SPEAKERPHONE", "WIRED_HEADSET", "EARPIECE", "BLUETOOTH_HEADSET", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum DeviceTypes {
        DEFAULT(-2),
        INVALID(-1),
        SPEAKERPHONE(0),
        WIRED_HEADSET(1),
        EARPIECE(2),
        BLUETOOTH_HEADSET(3);
        
        private final int value;

        DeviceTypes(int i) {
            this.value = i;
        }

        public final int getValue() {
            return this.value;
        }
    }

    /* compiled from: DiscordAudioManager.kt */
    /* loaded from: classes.dex */
    public static final class a implements Runnable {
        public a() {
        }

        @Override // java.lang.Runnable
        public final void run() {
            DiscordAudioManager discordAudioManager = DiscordAudioManager.this;
            b.a.q.k0.a aVar = discordAudioManager.m;
            Objects.requireNonNull(aVar);
            ThreadUtils.checkIsOnMainThread();
            boolean z2 = false;
            List<AudioManagerBroadcastAction> listOf = n.listOf((Object[]) new AudioManagerBroadcastAction[]{AudioManagerBroadcastAction.HeadsetPlug, AudioManagerBroadcastAction.ScoAudioStateUpdated});
            b.c.a.a0.d.b1("AudioManagerBroadcastReceiver", "registering for broadcasts with actions: " + listOf);
            IntentFilter intentFilter = new IntentFilter();
            for (AudioManagerBroadcastAction audioManagerBroadcastAction : listOf) {
                intentFilter.addAction(audioManagerBroadcastAction.getAction());
            }
            aVar.f257b.registerReceiver(aVar, intentFilter);
            b.a.q.k0.c cVar = discordAudioManager.l;
            Objects.requireNonNull(cVar);
            ThreadUtils.checkIsOnMainThread();
            if (!cVar.l) {
                StringBuilder R = b.d.b.a.a.R("registering for broadcasts with actions: ");
                Set<BluetoothBroadcastAction> set = b.a.q.k0.c.j;
                R.append(set);
                b.c.a.a0.d.b1("BluetoothBroadcastReceiver", R.toString());
                IntentFilter intentFilter2 = new IntentFilter();
                for (BluetoothBroadcastAction bluetoothBroadcastAction : set) {
                    intentFilter2.addAction(bluetoothBroadcastAction.getAction());
                }
                cVar.n.registerReceiver(cVar, intentFilter2);
                b.a.q.k0.d dVar = b.a.q.k0.d.c;
                Lazy lazy = b.a.q.k0.d.a;
                if (((String) lazy.getValue()) != null) {
                    Context context = cVar.n;
                    m.checkNotNullParameter(context, "context");
                    m.checkNotNullParameter(cVar, "receiver");
                    try {
                        String str = (String) lazy.getValue();
                        if (str != null) {
                            context.registerReceiver(cVar, new IntentFilter(str));
                        }
                    } catch (Throwable unused) {
                    }
                }
                try {
                    BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (defaultAdapter != null) {
                        z2 = defaultAdapter.getProfileProxy(cVar.n, cVar, 1);
                    }
                } catch (SecurityException e) {
                    b.c.a.a0.d.f1("BluetoothBroadcastReceiver", "failed to get BluetoothHeadset profile: " + e);
                }
                if (z2) {
                    b.c.a.a0.d.b1("BluetoothBroadcastReceiver", "listening for HeadsetProfile proxy");
                } else {
                    b.c.a.a0.d.c1("BluetoothBroadcastReceiver", "listening for HeadsetProfile proxy failed", null);
                }
                cVar.l = true;
            }
            b.a.q.k0.e eVar = new b.a.q.k0.e(discordAudioManager);
            synchronized (discordAudioManager.i) {
                discordAudioManager.p = eVar;
            }
        }
    }

    /* compiled from: DiscordAudioManager.kt */
    /* loaded from: classes.dex */
    public final class b implements h {
        public b() {
        }

        @Override // b.a.q.k0.h
        public void a(Context context, boolean z2) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(context, "context");
        }

        @Override // b.a.q.k0.h
        @MainThread
        public void b(Context context, ScoAudioState.b bVar) {
            boolean z2;
            AudioDevice audioDevice;
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(bVar, "scoAudioStateUpdate");
            int ordinal = bVar.a.ordinal();
            if (ordinal == 0) {
                b.c.a.a0.d.b1("DiscordAudioManager", "[onScoAudioStateUpdate] scoAudioStateUpdate = " + bVar);
                DiscordAudioManager discordAudioManager = DiscordAudioManager.this;
                BluetoothScoState bluetoothScoState = discordAudioManager.o;
                discordAudioManager.k();
                int ordinal2 = bluetoothScoState.ordinal();
                if (ordinal2 == 2) {
                    synchronized (DiscordAudioManager.this.i) {
                        z2 = DiscordAudioManager.this.D;
                    }
                    if (z2) {
                        b.c.a.a0.d.b1("DiscordAudioManager", "SCO off detected directly from ON. Refreshing Bluetooth device");
                        DiscordAudioManager.this.j();
                        DiscordAudioManager.this.l();
                    }
                } else if (ordinal2 == 3) {
                    StringBuilder R = b.d.b.a.a.R("Unable to turn on SCO. Clearing Bluetooth device. mode: ");
                    R.append(DiscordAudioManager.this.e.getMode());
                    b.c.a.a0.d.b1("DiscordAudioManager", R.toString());
                    synchronized (DiscordAudioManager.this.i) {
                        DiscordAudioManager discordAudioManager2 = DiscordAudioManager.this;
                        List<AudioDevice> list = discordAudioManager2.r;
                        ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
                        for (AudioDevice audioDevice2 : list) {
                            if (audioDevice2.a.ordinal() != 5) {
                                audioDevice = AudioDevice.a(audioDevice2, null, false, null, null, 15);
                            } else {
                                audioDevice = AudioDevice.a(audioDevice2, null, false, null, null, 1);
                            }
                            arrayList.add(audioDevice);
                        }
                        discordAudioManager2.r = arrayList;
                        discordAudioManager2.f2757s.k.onNext(arrayList);
                    }
                    DiscordAudioManager discordAudioManager3 = DiscordAudioManager.this;
                    discordAudioManager3.a(discordAudioManager3.r);
                }
            } else if (ordinal == 1) {
                b.c.a.a0.d.b1("DiscordAudioManager", "[onScoAudioStateUpdate] scoAudioStateUpdate = " + bVar);
                DiscordAudioManager.this.j();
            }
        }

        @Override // b.a.q.k0.h
        public void c(Context context) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(context, "context");
        }

        @Override // b.a.q.k0.h
        public void d(Context context, boolean z2) {
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(context, "context");
        }

        @Override // b.a.q.k0.h
        @MainThread
        public void e(Context context, b.a.q.l0.a aVar) {
            AudioDevice audioDevice;
            AudioDevice audioDevice2;
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(aVar, "wiredHeadsetState");
            b.c.a.a0.d.b1("DiscordAudioManager", "[onWiredHeadsetPlug] wiredHeadsetState = " + aVar);
            if (m.areEqual(aVar, a.b.a)) {
                synchronized (DiscordAudioManager.this.i) {
                    DiscordAudioManager discordAudioManager = DiscordAudioManager.this;
                    List<AudioDevice> list = discordAudioManager.r;
                    ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
                    for (AudioDevice audioDevice3 : list) {
                        int ordinal = audioDevice3.a.ordinal();
                        if (ordinal == 3) {
                            audioDevice2 = AudioDevice.a(audioDevice3, null, false, null, null, 13);
                        } else if (ordinal != 4) {
                            audioDevice2 = AudioDevice.a(audioDevice3, null, false, null, null, 15);
                        } else {
                            audioDevice2 = AudioDevice.a(audioDevice3, null, DiscordAudioManager.this.h, null, null, 13);
                        }
                        arrayList.add(audioDevice2);
                    }
                    discordAudioManager.r = arrayList;
                    discordAudioManager.f2757s.k.onNext(arrayList);
                }
            } else if (aVar instanceof a.C0040a) {
                synchronized (DiscordAudioManager.this.i) {
                    DiscordAudioManager discordAudioManager2 = DiscordAudioManager.this;
                    List<AudioDevice> list2 = discordAudioManager2.r;
                    ArrayList arrayList2 = new ArrayList(o.collectionSizeOrDefault(list2, 10));
                    for (AudioDevice audioDevice4 : list2) {
                        int ordinal2 = audioDevice4.a.ordinal();
                        if (ordinal2 == 3) {
                            audioDevice = AudioDevice.a(audioDevice4, null, true, null, null, 13);
                        } else if (ordinal2 != 4) {
                            audioDevice = AudioDevice.a(audioDevice4, null, false, null, null, 15);
                        } else {
                            audioDevice = AudioDevice.a(audioDevice4, null, false, null, null, 13);
                        }
                        arrayList2.add(audioDevice);
                    }
                    discordAudioManager2.r = arrayList2;
                    discordAudioManager2.f2757s.k.onNext(arrayList2);
                }
            }
            DiscordAudioManager.this.l();
        }
    }

    /* compiled from: DiscordAudioManager.kt */
    /* loaded from: classes.dex */
    public final class c implements i {
        public c() {
        }

        @Override // b.a.q.k0.i
        @MainThread
        public void a(Context context, BluetoothHeadsetAudioState.b bVar) {
            AudioDevice audioDevice;
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(bVar, "audioState");
            BluetoothHeadsetAudioState bluetoothHeadsetAudioState = bVar.a;
            if (bluetoothHeadsetAudioState == null) {
                bluetoothHeadsetAudioState = BluetoothHeadsetAudioState.Disconnected;
            }
            BluetoothDevice bluetoothDevice = bVar.c;
            StringBuilder sb = new StringBuilder();
            sb.append("[onHeadsetAudioStateChanged] state: ");
            sb.append(bluetoothHeadsetAudioState);
            sb.append(", device: ");
            sb.append(bluetoothDevice != null ? bluetoothDevice.getName() : null);
            b.c.a.a0.d.b1("DiscordAudioManager", sb.toString());
            int ordinal = bluetoothHeadsetAudioState.ordinal();
            if (ordinal == 0) {
                DiscordAudioManager.this.l();
            } else if (ordinal == 2) {
                synchronized (DiscordAudioManager.this.i) {
                    DiscordAudioManager discordAudioManager = DiscordAudioManager.this;
                    List<AudioDevice> list = discordAudioManager.r;
                    ArrayList arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
                    for (AudioDevice audioDevice2 : list) {
                        if (audioDevice2.a.ordinal() != 5) {
                            audioDevice = AudioDevice.a(audioDevice2, null, false, null, null, 15);
                        } else {
                            audioDevice = AudioDevice.a(audioDevice2, null, true, bluetoothDevice != null ? bluetoothDevice.getAddress() : null, bluetoothDevice != null ? bluetoothDevice.getName() : null, 1);
                        }
                        arrayList.add(audioDevice);
                    }
                    discordAudioManager.r = arrayList;
                    discordAudioManager.f2757s.k.onNext(arrayList);
                }
            }
        }

        @Override // b.a.q.k0.i
        @MainThread
        public void b(BluetoothDevice bluetoothDevice) {
            b.a.q.k0.d dVar = b.a.q.k0.d.c;
            if (!(((String) b.a.q.k0.d.a.getValue()) != null)) {
                throw new IllegalStateException("Check failed.".toString());
            } else if (bluetoothDevice != null) {
                DiscordAudioManager.this.l();
            }
        }

        @Override // b.a.q.k0.i
        @MainThread
        public void c(BluetoothHeadset bluetoothHeadset) {
            DiscordAudioManager.this.n = bluetoothHeadset;
        }

        @Override // b.a.q.k0.i
        @MainThread
        public void d(Context context, BluetoothProfileConnectionState.b bVar) {
            ArrayList arrayList;
            DiscordAudioManager discordAudioManager;
            DeviceTypes deviceTypes;
            m.checkNotNullParameter(context, "context");
            m.checkNotNullParameter(bVar, "connectionState");
            BluetoothProfileConnectionState bluetoothProfileConnectionState = bVar.f2763b;
            if (bluetoothProfileConnectionState == null) {
                bluetoothProfileConnectionState = BluetoothProfileConnectionState.Disconnected;
            }
            BluetoothDevice bluetoothDevice = bVar.c;
            StringBuilder sb = new StringBuilder();
            sb.append("[onHeadsetConnectionStateChanged] state: ");
            sb.append(bluetoothProfileConnectionState);
            sb.append(", device: ");
            String str = null;
            sb.append(bluetoothDevice != null ? bluetoothDevice.getName() : null);
            b.c.a.a0.d.b1("DiscordAudioManager", sb.toString());
            int ordinal = bluetoothProfileConnectionState.ordinal();
            if (ordinal != 0) {
                if (ordinal != 1) {
                    if (ordinal == 2) {
                        synchronized (DiscordAudioManager.this.i) {
                            discordAudioManager = DiscordAudioManager.this;
                            deviceTypes = discordAudioManager.t;
                        }
                        if (deviceTypes == DeviceTypes.BLUETOOTH_HEADSET) {
                            discordAudioManager.g();
                            return;
                        } else {
                            discordAudioManager.l();
                            return;
                        }
                    } else if (ordinal != 3) {
                        return;
                    }
                }
                b.c.a.a0.d.b1("DiscordAudioManager", "[onHeadsetConnectionStateChanged] " + bluetoothProfileConnectionState + "...");
                return;
            }
            synchronized (DiscordAudioManager.this.i) {
                List<AudioDevice> list = DiscordAudioManager.this.r;
                arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
                for (AudioDevice audioDevice : list) {
                    arrayList.add(AudioDevice.a(audioDevice, null, false, null, null, 15));
                }
            }
            DeviceTypes deviceTypes2 = DeviceTypes.BLUETOOTH_HEADSET;
            if (((AudioDevice) arrayList.get(deviceTypes2.getValue())).c != null) {
                if (bluetoothDevice != null) {
                    str = bluetoothDevice.getAddress();
                }
                if (m.areEqual(str, ((AudioDevice) arrayList.get(deviceTypes2.getValue())).c)) {
                    DiscordAudioManager.this.l();
                }
            }
        }
    }

    /* compiled from: DiscordAudioManager.kt */
    /* loaded from: classes.dex */
    public static final class d extends d0.z.d.o implements Function0<DiscordAudioManager> {
        public static final d j = new d();

        public d() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public DiscordAudioManager invoke() {
            return new DiscordAudioManager(ApplicationProvider.INSTANCE.get());
        }
    }

    /* compiled from: DiscordAudioManager.kt */
    /* loaded from: classes.dex */
    public static final class e extends d0.z.d.o implements Function0<b.a.q.k0.b> {
        public final /* synthetic */ Context $context;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public e(Context context) {
            super(0);
            this.$context = context;
        }

        @Override // kotlin.jvm.functions.Function0
        public b.a.q.k0.b invoke() {
            return new b.a.q.k0.b(this.$context);
        }
    }

    public DiscordAudioManager(Context context) {
        AudioDevice audioDevice;
        m.checkNotNullParameter(context, "context");
        Object systemService = context.getSystemService(MediaStreamTrack.AUDIO_TRACK_KIND);
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.media.AudioManager");
        this.e = (AudioManager) systemService;
        Object systemService2 = context.getSystemService("bluetooth");
        Objects.requireNonNull(systemService2, "null cannot be cast to non-null type android.bluetooth.BluetoothManager");
        this.f = (BluetoothManager) systemService2;
        this.g = g.lazy(new e(context));
        this.h = context.getPackageManager().hasSystemFeature("android.hardware.telephony");
        Thread currentThread = Thread.currentThread();
        m.checkNotNullExpressionValue(currentThread, "Thread.currentThread()");
        this.j = currentThread.getId();
        ContentResolver contentResolver = context.getContentResolver();
        m.checkNotNullExpressionValue(contentResolver, "context.contentResolver");
        this.k = contentResolver;
        this.l = new b.a.q.k0.c(context, new c());
        this.m = new b.a.q.k0.a(context, new b());
        new Handler(Looper.getMainLooper()).post(new a());
        List<AudioDevice> listOf = n.listOf((Object[]) new AudioDevice[]{new AudioDevice(DeviceTypes.SPEAKERPHONE, false, null, null, 14), new AudioDevice(DeviceTypes.WIRED_HEADSET, false, null, null, 14), new AudioDevice(DeviceTypes.EARPIECE, false, null, null, 14), new AudioDevice(DeviceTypes.BLUETOOTH_HEADSET, false, null, null, 14)});
        ArrayList arrayList = new ArrayList();
        for (AudioDevice audioDevice2 : listOf) {
            int ordinal = audioDevice2.a.ordinal();
            if (ordinal == 0 || ordinal == 1) {
                audioDevice = null;
            } else if (ordinal == 2) {
                audioDevice = AudioDevice.a(audioDevice2, null, true, null, null, 13);
            } else if (ordinal == 3) {
                audioDevice = AudioDevice.a(audioDevice2, null, this.e.isWiredHeadsetOn(), null, null, 13);
            } else if (ordinal == 4) {
                audioDevice = AudioDevice.a(audioDevice2, null, this.h, null, null, 13);
            } else if (ordinal == 5) {
                audioDevice = AudioDevice.a(audioDevice2, null, false, null, null, 15);
            } else {
                throw new NoWhenBranchMatchedException();
            }
            if (audioDevice != null) {
                arrayList.add(audioDevice);
            }
        }
        this.r = arrayList;
        this.f2757s = new SerializedSubject<>(BehaviorSubject.l0(arrayList));
        DeviceTypes deviceTypes = DeviceTypes.INVALID;
        this.t = deviceTypes;
        this.u = new SerializedSubject<>(BehaviorSubject.l0(deviceTypes));
        this.f2758x = this.e.getStreamMaxVolume(3);
        this.f2759y = new SerializedSubject<>(BehaviorSubject.l0(Integer.valueOf(this.w)));
        this.f2760z = DeviceTypes.DEFAULT;
    }

    public static final DiscordAudioManager d() {
        return (DiscordAudioManager) a.getValue();
    }

    public static final List<AudioDevice> f() {
        return n.listOf((Object[]) new AudioDevice[]{new AudioDevice(DeviceTypes.SPEAKERPHONE, false, null, null, 14), new AudioDevice(DeviceTypes.WIRED_HEADSET, false, null, null, 14), new AudioDevice(DeviceTypes.EARPIECE, false, null, null, 14), new AudioDevice(DeviceTypes.BLUETOOTH_HEADSET, false, null, null, 14)});
    }

    public final void a(List<AudioDevice> list) {
        DeviceTypes deviceTypes;
        DeviceTypes deviceTypes2;
        synchronized (this.i) {
            List<DeviceTypes> list2 = c;
            ListIterator<DeviceTypes> listIterator = list2.listIterator(list2.size());
            while (true) {
                if (!listIterator.hasPrevious()) {
                    deviceTypes = null;
                    break;
                }
                deviceTypes = listIterator.previous();
                if (list.get(deviceTypes.getValue()).f2761b) {
                    break;
                }
            }
            deviceTypes2 = deviceTypes;
            if (deviceTypes2 == null) {
                deviceTypes2 = DeviceTypes.SPEAKERPHONE;
            }
        }
        b.c.a.a0.d.b1("DiscordAudioManager", "Default device to activate: " + deviceTypes2);
        b(deviceTypes2);
    }

    public final void b(DeviceTypes deviceTypes) {
        boolean z2;
        boolean z3;
        synchronized (this.i) {
            z2 = true;
            z3 = !this.D;
        }
        if (z3) {
            b.c.a.a0.d.f1("DiscordAudioManager", "Unable to activate audio output outside Discord-requested communication mode");
            return;
        }
        if (deviceTypes == DeviceTypes.BLUETOOTH_HEADSET) {
            j();
        } else {
            k();
        }
        if (deviceTypes != DeviceTypes.SPEAKERPHONE) {
            z2 = false;
        }
        if (this.e.isSpeakerphoneOn() != z2) {
            this.e.setSpeakerphoneOn(z2);
        }
        synchronized (this.i) {
            this.t = deviceTypes;
            this.u.k.onNext(deviceTypes);
        }
        b.c.a.a0.d.b1("DiscordAudioManager", "Activated device: " + deviceTypes);
    }

    public final void c() {
        Thread currentThread = Thread.currentThread();
        m.checkNotNullExpressionValue(currentThread, "Thread.currentThread()");
        if (currentThread.getId() != this.j) {
            throw new IllegalStateException("Method was not called from a valid thread");
        }
    }

    public final b.a.q.k0.b e() {
        return (b.a.q.k0.b) this.g.getValue();
    }

    /* JADX WARN: Removed duplicated region for block: B:33:0x006c  */
    /* JADX WARN: Removed duplicated region for block: B:36:0x0076  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x007e  */
    /* JADX WARN: Removed duplicated region for block: B:75:0x0099 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void g() {
        /*
            Method dump skipped, instructions count: 264
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.rtcconnection.audio.DiscordAudioManager.g():void");
    }

    /* JADX WARN: Code restructure failed: missing block: B:19:0x0090, code lost:
        if (r6.e.requestAudioFocus(r7) == 1) goto L22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x0099, code lost:
        if (r6.e.requestAudioFocus(r1, 0, 1) == 1) goto L22;
     */
    /* JADX WARN: Code restructure failed: missing block: B:22:0x009b, code lost:
        r2 = true;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x009c, code lost:
        if (r2 == false) goto L25;
     */
    /* JADX WARN: Code restructure failed: missing block: B:24:0x009e, code lost:
        b.c.a.a0.d.b1("DiscordAudioManager", "Successful requestAudioFocus()");
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x00a6, code lost:
        b.c.a.a0.d.f1("DiscordAudioManager", "Unable to requestAudioFocus()");
     */
    /* JADX WARN: Code restructure failed: missing block: B:45:0x00e4, code lost:
        if (r6.e.abandonAudioFocusRequest(r0) == 1) goto L55;
     */
    /* JADX WARN: Code restructure failed: missing block: B:54:0x00f8, code lost:
        if (r6.e.abandonAudioFocus(r0) == 1) goto L55;
     */
    /* JADX WARN: Code restructure failed: missing block: B:55:0x00fa, code lost:
        r2 = true;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void h(boolean r7) {
        /*
            Method dump skipped, instructions count: 316
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.rtcconnection.audio.DiscordAudioManager.h(boolean):void");
    }

    public final void i(boolean z2) {
        try {
            this.e.setMode(z2 ? 3 : 0);
        } catch (SecurityException unused) {
        }
        synchronized (this.i) {
            this.D = z2;
        }
    }

    public final synchronized void j() {
        BluetoothScoState bluetoothScoState;
        BluetoothScoState bluetoothScoState2 = this.o;
        BluetoothScoState bluetoothScoState3 = BluetoothScoState.ON;
        if (!(bluetoothScoState2 == bluetoothScoState3 || bluetoothScoState2 == (bluetoothScoState = BluetoothScoState.TURNING_ON))) {
            if (this.e.isBluetoothScoOn()) {
                this.o = bluetoothScoState3;
                return;
            }
            this.o = bluetoothScoState;
            this.e.startBluetoothSco();
        }
    }

    public final synchronized void k() {
        BluetoothScoState bluetoothScoState = this.o;
        if (bluetoothScoState != BluetoothScoState.ON && bluetoothScoState != BluetoothScoState.TURNING_ON) {
            return;
        }
        if (!this.e.isBluetoothScoOn()) {
            this.o = BluetoothScoState.OFF;
            return;
        }
        this.o = BluetoothScoState.TURNING_OFF;
        this.e.stopBluetoothSco();
    }

    public final void l() {
        DeviceTypes deviceTypes;
        ArrayList arrayList;
        synchronized (this.i) {
            deviceTypes = this.f2760z;
        }
        g();
        synchronized (this.i) {
            List<AudioDevice> list = this.r;
            arrayList = new ArrayList(o.collectionSizeOrDefault(list, 10));
            for (AudioDevice audioDevice : list) {
                arrayList.add(AudioDevice.a(audioDevice, null, false, null, null, 15));
            }
        }
        if (deviceTypes == DeviceTypes.DEFAULT || !arrayList.get(deviceTypes.getValue()).f2761b) {
            a(arrayList);
        } else {
            b(deviceTypes);
        }
    }
}
