package com.discord.rtcconnection.mediaengine;

import andhook.lib.HookHelper;
import androidx.annotation.AnyThread;
import androidx.core.app.FrameMetricsAggregator;
import co.discord.media_engine.RtcRegion;
import co.discord.media_engine.StreamParameters;
import co.discord.media_engine.VideoInputDeviceDescription;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.hammerandchisel.libdiscord.Discord;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.Observable;
/* compiled from: MediaEngine.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\bf\u0018\u00002\u00020\u0001:\t=\b/>:?@ABJ\u0015\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002H&¢\u0006\u0004\b\u0004\u0010\u0005J\u0015\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H&¢\u0006\u0004\b\b\u0010\tJA\u0010\u0014\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e2\u0016\u0010\u0013\u001a\u0012\u0012\b\u0012\u00060\u0011j\u0002`\u0012\u0012\u0004\u0012\u00020\u00070\u0010H&¢\u0006\u0004\b\u0014\u0010\u0015J\u0017\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u0016H&¢\u0006\u0004\b\u0018\u0010\u0019J\u0017\u0010\u001c\u001a\u00020\u00072\u0006\u0010\u001b\u001a\u00020\u001aH'¢\u0006\u0004\b\u001c\u0010\u001dJ\u0017\u0010 \u001a\u00020\u00072\u0006\u0010\u001f\u001a\u00020\u001eH&¢\u0006\u0004\b \u0010!J\u0017\u0010$\u001a\u00020\u00072\u0006\u0010#\u001a\u00020\"H&¢\u0006\u0004\b$\u0010%J)\u0010)\u001a\u00020\u00072\u0018\u0010(\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020'0&\u0012\u0004\u0012\u00020\u00070\u0010H&¢\u0006\u0004\b)\u0010*J7\u0010/\u001a\u00020\u00072\f\u0010,\u001a\b\u0012\u0004\u0012\u00020+0&2\u0018\u0010.\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020-0&\u0012\u0004\u0012\u00020\u00070\u0010H&¢\u0006\u0004\b/\u00100J%\u00103\u001a\u00020\u00072\u0014\u00102\u001a\u0010\u0012\u0004\u0012\u000201\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0010H&¢\u0006\u0004\b3\u0010*J\u0011\u00105\u001a\u0004\u0018\u000104H&¢\u0006\u0004\b5\u00106J\u0015\u00108\u001a\b\u0012\u0004\u0012\u0002070\u0006H&¢\u0006\u0004\b8\u0010\tR\u0016\u0010<\u001a\u0002098&@&X¦\u0004¢\u0006\u0006\u001a\u0004\b:\u0010;¨\u0006C"}, d2 = {"Lcom/discord/rtcconnection/mediaengine/MediaEngine;", "", "", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;", "getConnections", "()Ljava/util/List;", "Lrx/Observable;", "", "a", "()Lrx/Observable;", "", "userId", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$a;", "options", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;", "type", "Lkotlin/Function1;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "onFailure", "g", "(JLcom/discord/rtcconnection/mediaengine/MediaEngine$a;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$Type;Lkotlin/jvm/functions/Function1;)Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;", "", "deviceIndex", "f", "(I)V", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$VoiceConfig;", "voiceConfig", "d", "(Lcom/discord/rtcconnection/mediaengine/MediaEngine$VoiceConfig;)V", "", "enabled", "k", "(Z)V", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLUsageMode;", "openSLUsageMode", "h", "(Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLUsageMode;)V", "", "Lco/discord/media_engine/VideoInputDeviceDescription;", "devicesCallback", "j", "(Lkotlin/jvm/functions/Function1;)V", "Lco/discord/media_engine/RtcRegion;", "regionsWithIps", "", "callback", "b", "([Lco/discord/media_engine/RtcRegion;Lkotlin/jvm/functions/Function1;)V", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", "voiceStatusListener", "l", "Lcom/hammerandchisel/libdiscord/Discord;", "i", "()Lcom/hammerandchisel/libdiscord/Discord;", "Lcom/discord/rtcconnection/mediaengine/MediaEngine$AudioInfo;", "e", "Lb/a/q/c;", "c", "()Lb/a/q/c;", "mediaEngineThreadExecutor", "AudioInfo", "EchoCancellationInfo", "LocalVoiceStatus", "OpenSLESConfig", "OpenSLUsageMode", "VoiceConfig", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public interface MediaEngine {

    /* compiled from: MediaEngine.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0012\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\u000f\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u0004R\u0019\u0010\u0012\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0010\u0010\r\u001a\u0004\b\u0011\u0010\u0004¨\u0006\u0015"}, d2 = {"Lcom/discord/rtcconnection/mediaengine/MediaEngine$AudioInfo;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "b", "Ljava/lang/String;", "getAudioLayer", "audioLayer", "a", "getAudioSubsystem", "audioSubsystem", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AudioInfo {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final String f2765b;

        public AudioInfo(String str, String str2) {
            m.checkNotNullParameter(str, "audioSubsystem");
            m.checkNotNullParameter(str2, "audioLayer");
            this.a = str;
            this.f2765b = str2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AudioInfo)) {
                return false;
            }
            AudioInfo audioInfo = (AudioInfo) obj;
            return m.areEqual(this.a, audioInfo.a) && m.areEqual(this.f2765b, audioInfo.f2765b);
        }

        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.f2765b;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("AudioInfo(audioSubsystem=");
            R.append(this.a);
            R.append(", audioLayer=");
            return b.d.b.a.a.H(R, this.f2765b, ")");
        }
    }

    /* compiled from: MediaEngine.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\"\b\u0086\b\u0018\u00002\u00020\u0001Ba\u0012\b\b\u0002\u0010\u0010\u001a\u00020\t\u0012\b\b\u0002\u0010\u0019\u001a\u00020\t\u0012\b\b\u0002\u0010\u001c\u001a\u00020\t\u0012\b\b\u0002\u0010\u0013\u001a\u00020\t\u0012\b\b\u0002\u0010(\u001a\u00020\t\u0012\b\b\u0002\u0010\u001f\u001a\u00020\t\u0012\b\b\u0002\u0010\"\u001a\u00020\t\u0012\b\b\u0002\u0010\u0016\u001a\u00020\t\u0012\b\b\u0002\u0010%\u001a\u00020\t¢\u0006\u0004\b)\u0010*J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\u0010\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u0019\u0010\u0013\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0011\u0010\r\u001a\u0004\b\u0012\u0010\u000fR\u0019\u0010\u0016\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\r\u001a\u0004\b\u0015\u0010\u000fR\u0019\u0010\u0019\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0017\u0010\r\u001a\u0004\b\u0018\u0010\u000fR\u0019\u0010\u001c\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\r\u001a\u0004\b\u001b\u0010\u000fR\u0019\u0010\u001f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\r\u001a\u0004\b\u001e\u0010\u000fR\u0019\u0010\"\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b \u0010\r\u001a\u0004\b!\u0010\u000fR\u0019\u0010%\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010\r\u001a\u0004\b$\u0010\u000fR\u0019\u0010(\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b&\u0010\r\u001a\u0004\b'\u0010\u000f¨\u0006+"}, d2 = {"Lcom/discord/rtcconnection/mediaengine/MediaEngine$EchoCancellationInfo;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "a", "Z", "getBuiltinAecRequested", "()Z", "builtinAecRequested", "d", "getBuiltinAecEnabled", "builtinAecEnabled", "h", "getAecEnabledByDefault", "aecEnabledByDefault", "b", "getBuiltinAecSupportedNative", "builtinAecSupportedNative", "c", "getBuiltinAecSupportedJava", "builtinAecSupportedJava", "f", "getAecEnabledInNativeConfig", "aecEnabledInNativeConfig", "g", "getAecMobileMode", "aecMobileMode", "i", "getAecMobileModeByDefault", "aecMobileModeByDefault", "e", "getAecEnabledInSettings", "aecEnabledInSettings", HookHelper.constructorName, "(ZZZZZZZZZ)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class EchoCancellationInfo {
        public final boolean a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f2766b;
        public final boolean c;
        public final boolean d;
        public final boolean e;
        public final boolean f;
        public final boolean g;
        public final boolean h;
        public final boolean i;

        public EchoCancellationInfo() {
            this(false, false, false, false, false, false, false, false, false, FrameMetricsAggregator.EVERY_DURATION);
        }

        public EchoCancellationInfo(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10) {
            this.a = z2;
            this.f2766b = z3;
            this.c = z4;
            this.d = z5;
            this.e = z6;
            this.f = z7;
            this.g = z8;
            this.h = z9;
            this.i = z10;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof EchoCancellationInfo)) {
                return false;
            }
            EchoCancellationInfo echoCancellationInfo = (EchoCancellationInfo) obj;
            return this.a == echoCancellationInfo.a && this.f2766b == echoCancellationInfo.f2766b && this.c == echoCancellationInfo.c && this.d == echoCancellationInfo.d && this.e == echoCancellationInfo.e && this.f == echoCancellationInfo.f && this.g == echoCancellationInfo.g && this.h == echoCancellationInfo.h && this.i == echoCancellationInfo.i;
        }

        public int hashCode() {
            boolean z2 = this.a;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = i2 * 31;
            boolean z3 = this.f2766b;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.c;
            if (z4) {
                z4 = true;
            }
            int i8 = z4 ? 1 : 0;
            int i9 = z4 ? 1 : 0;
            int i10 = (i7 + i8) * 31;
            boolean z5 = this.d;
            if (z5) {
                z5 = true;
            }
            int i11 = z5 ? 1 : 0;
            int i12 = z5 ? 1 : 0;
            int i13 = (i10 + i11) * 31;
            boolean z6 = this.e;
            if (z6) {
                z6 = true;
            }
            int i14 = z6 ? 1 : 0;
            int i15 = z6 ? 1 : 0;
            int i16 = (i13 + i14) * 31;
            boolean z7 = this.f;
            if (z7) {
                z7 = true;
            }
            int i17 = z7 ? 1 : 0;
            int i18 = z7 ? 1 : 0;
            int i19 = (i16 + i17) * 31;
            boolean z8 = this.g;
            if (z8) {
                z8 = true;
            }
            int i20 = z8 ? 1 : 0;
            int i21 = z8 ? 1 : 0;
            int i22 = (i19 + i20) * 31;
            boolean z9 = this.h;
            if (z9) {
                z9 = true;
            }
            int i23 = z9 ? 1 : 0;
            int i24 = z9 ? 1 : 0;
            int i25 = (i22 + i23) * 31;
            boolean z10 = this.i;
            if (!z10) {
                i = z10 ? 1 : 0;
            }
            return i25 + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("EchoCancellationInfo(builtinAecRequested=");
            R.append(this.a);
            R.append(", builtinAecSupportedNative=");
            R.append(this.f2766b);
            R.append(", builtinAecSupportedJava=");
            R.append(this.c);
            R.append(", builtinAecEnabled=");
            R.append(this.d);
            R.append(", aecEnabledInSettings=");
            R.append(this.e);
            R.append(", aecEnabledInNativeConfig=");
            R.append(this.f);
            R.append(", aecMobileMode=");
            R.append(this.g);
            R.append(", aecEnabledByDefault=");
            R.append(this.h);
            R.append(", aecMobileModeByDefault=");
            return b.d.b.a.a.M(R, this.i, ")");
        }

        public /* synthetic */ EchoCancellationInfo(boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10, int i) {
            this((i & 1) != 0 ? false : z2, (i & 2) != 0 ? false : z3, (i & 4) != 0 ? false : z4, (i & 8) != 0 ? false : z5, (i & 16) != 0 ? false : z6, (i & 32) != 0 ? false : z7, (i & 64) != 0 ? false : z8, (i & 128) != 0 ? false : z9, (i & 256) == 0 ? z10 : false);
        }
    }

    /* compiled from: MediaEngine.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\f\b\u0086\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0011\u001a\u00020\f\u0012\u0006\u0010\u0014\u001a\u00020\t¢\u0006\u0004\b\u0016\u0010\u0017J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\u0011\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0014\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015¨\u0006\u0018"}, d2 = {"Lcom/discord/rtcconnection/mediaengine/MediaEngine$LocalVoiceStatus;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "a", "F", "getAmplitude", "()F", "amplitude", "b", "Z", "isSpeaking", "()Z", HookHelper.constructorName, "(FZ)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class LocalVoiceStatus {
        public final float a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f2767b;

        public LocalVoiceStatus(float f, boolean z2) {
            this.a = f;
            this.f2767b = z2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof LocalVoiceStatus)) {
                return false;
            }
            LocalVoiceStatus localVoiceStatus = (LocalVoiceStatus) obj;
            return Float.compare(this.a, localVoiceStatus.a) == 0 && this.f2767b == localVoiceStatus.f2767b;
        }

        public int hashCode() {
            int floatToIntBits = Float.floatToIntBits(this.a) * 31;
            boolean z2 = this.f2767b;
            if (z2) {
                z2 = true;
            }
            int i = z2 ? 1 : 0;
            int i2 = z2 ? 1 : 0;
            return floatToIntBits + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("LocalVoiceStatus(amplitude=");
            R.append(this.a);
            R.append(", isSpeaking=");
            return b.d.b.a.a.M(R, this.f2767b, ")");
        }
    }

    /* compiled from: MediaEngine.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLESConfig;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "DEFAULT", "FORCE_ENABLED", "FORCE_DISABLED", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum OpenSLESConfig {
        DEFAULT,
        FORCE_ENABLED,
        FORCE_DISABLED
    }

    /* compiled from: MediaEngine.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lcom/discord/rtcconnection/mediaengine/MediaEngine$OpenSLUsageMode;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "ALLOW_LIST", "EXCLUDE_LIST", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum OpenSLUsageMode {
        ALLOW_LIST,
        EXCLUDE_LIST
    }

    /* compiled from: MediaEngine.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0002\b\u0010\b\u0086\b\u0018\u00002\u00020\u0001BO\u0012\u0006\u0010'\u001a\u00020\"\u0012\u0006\u0010\u0018\u001a\u00020\t\u0012\u0006\u0010\u001b\u001a\u00020\t\u0012\u0006\u0010,\u001a\u00020\t\u0012\u0006\u0010/\u001a\u00020\t\u0012\u0006\u0010!\u001a\u00020\u001c\u0012\u0006\u0010\u0011\u001a\u00020\f\u0012\u0006\u0010)\u001a\u00020\t\u0012\u0006\u0010\u0014\u001a\u00020\t¢\u0006\u0004\b0\u00101J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u0019\u0010\u0011\u001a\u00020\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0014\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u0019\u0010\u0018\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0013\u001a\u0004\b\u0017\u0010\u0015R\u0019\u0010\u001b\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0013\u001a\u0004\b\u001a\u0010\u0015R\u0019\u0010!\u001a\u00020\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0019\u0010'\u001a\u00020\"8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010$\u001a\u0004\b%\u0010&R\u0019\u0010)\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b(\u0010\u0013\u001a\u0004\b)\u0010\u0015R\u0019\u0010,\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b*\u0010\u0013\u001a\u0004\b+\u0010\u0015R\u0019\u0010/\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u0013\u001a\u0004\b.\u0010\u0015¨\u00062"}, d2 = {"Lcom/discord/rtcconnection/mediaengine/MediaEngine$VoiceConfig;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$c;", "g", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$c;", "getInputModeOptions", "()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$c;", "inputModeOptions", "i", "Z", "isSelfMuted", "()Z", "b", "getEchoCancellation", "echoCancellation", "c", "getNoiseSuppression", "noiseSuppression", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "f", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "getInputMode", "()Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;", "inputMode", "", "a", "F", "getOutputVolume", "()F", "outputVolume", "h", "isSelfDeafened", "d", "getNoiseCancellation", "noiseCancellation", "e", "getAutomaticGainControl", "automaticGainControl", HookHelper.constructorName, "(FZZZZLcom/discord/rtcconnection/mediaengine/MediaEngineConnection$InputMode;Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$c;ZZ)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class VoiceConfig {
        public final float a;

        /* renamed from: b  reason: collision with root package name */
        public final boolean f2768b;
        public final boolean c;
        public final boolean d;
        public final boolean e;
        public final MediaEngineConnection.InputMode f;
        public final MediaEngineConnection.c g;
        public final boolean h;
        public final boolean i;

        public VoiceConfig(float f, boolean z2, boolean z3, boolean z4, boolean z5, MediaEngineConnection.InputMode inputMode, MediaEngineConnection.c cVar, boolean z6, boolean z7) {
            m.checkNotNullParameter(inputMode, "inputMode");
            m.checkNotNullParameter(cVar, "inputModeOptions");
            this.a = f;
            this.f2768b = z2;
            this.c = z3;
            this.d = z4;
            this.e = z5;
            this.f = inputMode;
            this.g = cVar;
            this.h = z6;
            this.i = z7;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof VoiceConfig)) {
                return false;
            }
            VoiceConfig voiceConfig = (VoiceConfig) obj;
            return Float.compare(this.a, voiceConfig.a) == 0 && this.f2768b == voiceConfig.f2768b && this.c == voiceConfig.c && this.d == voiceConfig.d && this.e == voiceConfig.e && m.areEqual(this.f, voiceConfig.f) && m.areEqual(this.g, voiceConfig.g) && this.h == voiceConfig.h && this.i == voiceConfig.i;
        }

        public int hashCode() {
            int floatToIntBits = Float.floatToIntBits(this.a) * 31;
            boolean z2 = this.f2768b;
            int i = 1;
            if (z2) {
                z2 = true;
            }
            int i2 = z2 ? 1 : 0;
            int i3 = z2 ? 1 : 0;
            int i4 = (floatToIntBits + i2) * 31;
            boolean z3 = this.c;
            if (z3) {
                z3 = true;
            }
            int i5 = z3 ? 1 : 0;
            int i6 = z3 ? 1 : 0;
            int i7 = (i4 + i5) * 31;
            boolean z4 = this.d;
            if (z4) {
                z4 = true;
            }
            int i8 = z4 ? 1 : 0;
            int i9 = z4 ? 1 : 0;
            int i10 = (i7 + i8) * 31;
            boolean z5 = this.e;
            if (z5) {
                z5 = true;
            }
            int i11 = z5 ? 1 : 0;
            int i12 = z5 ? 1 : 0;
            int i13 = (i10 + i11) * 31;
            MediaEngineConnection.InputMode inputMode = this.f;
            int i14 = 0;
            int hashCode = (i13 + (inputMode != null ? inputMode.hashCode() : 0)) * 31;
            MediaEngineConnection.c cVar = this.g;
            if (cVar != null) {
                i14 = cVar.hashCode();
            }
            int i15 = (hashCode + i14) * 31;
            boolean z6 = this.h;
            if (z6) {
                z6 = true;
            }
            int i16 = z6 ? 1 : 0;
            int i17 = z6 ? 1 : 0;
            int i18 = (i15 + i16) * 31;
            boolean z7 = this.i;
            if (!z7) {
                i = z7 ? 1 : 0;
            }
            return i18 + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("VoiceConfig(outputVolume=");
            R.append(this.a);
            R.append(", echoCancellation=");
            R.append(this.f2768b);
            R.append(", noiseSuppression=");
            R.append(this.c);
            R.append(", noiseCancellation=");
            R.append(this.d);
            R.append(", automaticGainControl=");
            R.append(this.e);
            R.append(", inputMode=");
            R.append(this.f);
            R.append(", inputModeOptions=");
            R.append(this.g);
            R.append(", isSelfDeafened=");
            R.append(this.h);
            R.append(", isSelfMuted=");
            return b.d.b.a.a.M(R, this.i, ")");
        }
    }

    /* compiled from: MediaEngine.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public final int a;

        /* renamed from: b  reason: collision with root package name */
        public final String f2769b;
        public final int c;
        public final List<StreamParameters> d;

        public a(int i, String str, int i2, List<StreamParameters> list) {
            m.checkNotNullParameter(str, "ip");
            m.checkNotNullParameter(list, "streams");
            this.a = i;
            this.f2769b = str;
            this.c = i2;
            this.d = list;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && m.areEqual(this.f2769b, aVar.f2769b) && this.c == aVar.c && m.areEqual(this.d, aVar.d);
        }

        public int hashCode() {
            int i = this.a * 31;
            String str = this.f2769b;
            int i2 = 0;
            int hashCode = (((i + (str != null ? str.hashCode() : 0)) * 31) + this.c) * 31;
            List<StreamParameters> list = this.d;
            if (list != null) {
                i2 = list.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("ConnectionOptions(ssrc=");
            R.append(this.a);
            R.append(", ip=");
            R.append(this.f2769b);
            R.append(", port=");
            R.append(this.c);
            R.append(", streams=");
            return b.d.b.a.a.K(R, this.d, ")");
        }
    }

    /* compiled from: MediaEngine.kt */
    /* loaded from: classes.dex */
    public interface b {
        void onEchoCancellationUpdated(EchoCancellationInfo echoCancellationInfo);
    }

    /* compiled from: MediaEngine.kt */
    /* loaded from: classes.dex */
    public interface c {
        void onConnected();

        void onConnecting();

        void onNativeEngineInitialized();

        void onNewConnection(MediaEngineConnection mediaEngineConnection);
    }

    Observable<Unit> a();

    void b(RtcRegion[] rtcRegionArr, Function1<? super String[], Unit> function1);

    b.a.q.c c();

    @AnyThread
    void d(VoiceConfig voiceConfig);

    Observable<AudioInfo> e();

    void f(int i);

    MediaEngineConnection g(long j, a aVar, MediaEngineConnection.Type type, Function1<? super Exception, Unit> function1);

    List<MediaEngineConnection> getConnections();

    void h(OpenSLUsageMode openSLUsageMode);

    Discord i();

    void j(Function1<? super VideoInputDeviceDescription[], Unit> function1);

    void k(boolean z2);

    void l(Function1<? super LocalVoiceStatus, Unit> function1);
}
