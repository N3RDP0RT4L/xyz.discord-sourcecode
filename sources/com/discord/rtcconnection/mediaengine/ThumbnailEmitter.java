package com.discord.rtcconnection.mediaengine;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.opengl.GLES20;
import com.discord.utilities.time.Clock;
import com.discord.utilities.time.ClockFactory;
import d0.a0.a;
import d0.z.d.m;
import java.nio.ByteBuffer;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.webrtc.GlRectDrawer;
import org.webrtc.GlTextureFrameBuffer;
import org.webrtc.GlUtil;
import org.webrtc.JniCommon;
import org.webrtc.VideoFrame;
import org.webrtc.VideoFrameDrawer;
/* compiled from: ThumbnailEmitter.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002¢\u0006\u0004\b\u0005\u0010\u0006R\u0016\u0010\n\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0016\u0010\r\u001a\u00020\u000b8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0005\u0010\fR\u0016\u0010\u0011\u001a\u00020\u000e8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0015\u001a\u00020\u00128\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\"\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00170\u00168\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0016\u0010\u001c\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\fR\u0016\u0010\u001e\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001d\u0010\tR\u0016\u0010 \u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001f\u0010\fR\u001e\u0010%\u001a\n \"*\u0004\u0018\u00010!0!8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b#\u0010$R\u0016\u0010)\u001a\u00020&8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u0016\u0010+\u001a\u00020\u000b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b*\u0010\fR\u0016\u0010/\u001a\u00020,8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b-\u0010.¨\u00060"}, d2 = {"Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;", "", "Lorg/webrtc/VideoFrame;", "frame", "Landroid/graphics/Bitmap;", "a", "(Lorg/webrtc/VideoFrame;)Landroid/graphics/Bitmap;", "", "h", "I", "height", "", "J", "lastTimestampNs", "Lorg/webrtc/GlRectDrawer;", "c", "Lorg/webrtc/GlRectDrawer;", "rectDrawer", "Lcom/discord/utilities/time/Clock;", "k", "Lcom/discord/utilities/time/Clock;", "clock", "Lkotlin/Function1;", "", "l", "Lkotlin/jvm/functions/Function1;", "onNextThumbnail", "j", "initialDelayMs", "g", "width", "e", "initializationTimeMs", "Ljava/nio/ByteBuffer;", "kotlin.jvm.PlatformType", "b", "Ljava/nio/ByteBuffer;", "outputByteBuffer", "Landroid/graphics/Matrix;", "f", "Landroid/graphics/Matrix;", "renderMatrix", "i", "periodMs", "Lorg/webrtc/VideoFrameDrawer;", "d", "Lorg/webrtc/VideoFrameDrawer;", "frameDrawer", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class ThumbnailEmitter {
    public long a;

    /* renamed from: b  reason: collision with root package name */
    public final ByteBuffer f2773b;
    public final GlRectDrawer c;
    public final VideoFrameDrawer d;
    public final long e;
    public final Matrix f;
    public final int g;
    public final int h;
    public final long i;
    public final long j;
    public final Clock k;
    public final Function1<Bitmap, Unit> l;

    public ThumbnailEmitter(int i, int i2, long j, long j2, Clock clock, Function1 function1, int i3) {
        j2 = (i3 & 8) != 0 ? 0L : j2;
        Clock clock2 = (i3 & 16) != 0 ? ClockFactory.get() : null;
        m.checkNotNullParameter(clock2, "clock");
        m.checkNotNullParameter(function1, "onNextThumbnail");
        this.g = i;
        this.h = i2;
        this.i = j;
        this.j = j2;
        this.k = clock2;
        this.l = function1;
        long j3 = 1000;
        this.a = -(j * j3 * j3);
        this.f2773b = JniCommon.nativeAllocateByteBuffer(i * i2 * 4);
        this.c = new GlRectDrawer();
        this.d = new VideoFrameDrawer();
        this.e = clock2.currentTimeMillis();
        Matrix matrix = new Matrix();
        matrix.preTranslate(0.5f, 0.5f);
        matrix.preScale(1.0f, -1.0f);
        matrix.preTranslate(-0.5f, -0.5f);
        this.f = matrix;
    }

    public final Bitmap a(VideoFrame videoFrame) {
        GlTextureFrameBuffer glTextureFrameBuffer = new GlTextureFrameBuffer(6408);
        glTextureFrameBuffer.setSize(this.g, this.h);
        GLES20.glBindFramebuffer(36160, glTextureFrameBuffer.getFrameBufferId());
        GlUtil.checkNoGLES2Error("glBindFramebuffer");
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glClear(16384);
        if (videoFrame.getRotatedWidth() / videoFrame.getRotatedHeight() < this.g / this.h) {
            float rotatedHeight = (this.h / videoFrame.getRotatedHeight()) * videoFrame.getRotatedWidth();
            this.d.drawFrame(videoFrame, this.c, this.f, a.roundToInt((this.g - rotatedHeight) / 2.0f), 0, a.roundToInt(rotatedHeight), this.h);
        } else {
            float rotatedWidth = (this.g / videoFrame.getRotatedWidth()) * videoFrame.getRotatedHeight();
            this.d.drawFrame(videoFrame, this.c, this.f, 0, a.roundToInt((this.h - rotatedWidth) / 2.0f), this.g, a.roundToInt(rotatedWidth));
        }
        this.f2773b.rewind();
        GLES20.glReadPixels(0, 0, glTextureFrameBuffer.getWidth(), glTextureFrameBuffer.getHeight(), 6408, 5121, this.f2773b);
        GlUtil.checkNoGLES2Error("ThumbnailEmitter.createThumbnail");
        glTextureFrameBuffer.release();
        this.f2773b.rewind();
        Bitmap createBitmap = Bitmap.createBitmap(this.g, this.h, Bitmap.Config.ARGB_8888);
        createBitmap.copyPixelsFromBuffer(this.f2773b);
        m.checkNotNullExpressionValue(createBitmap, "bitmap");
        return createBitmap;
    }
}
