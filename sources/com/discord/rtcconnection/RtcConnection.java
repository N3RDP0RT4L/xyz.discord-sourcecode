package com.discord.rtcconnection;

import andhook.lib.HookHelper;
import android.content.Intent;
import android.util.Log;
import androidx.annotation.AnyThread;
import androidx.core.app.NotificationCompat;
import androidx.exifinterface.media.ExifInterface;
import b.a.q.a0;
import b.a.q.b0;
import b.a.q.f0;
import b.a.q.g;
import b.a.q.g0;
import b.a.q.h0;
import b.a.q.j;
import b.a.q.p;
import b.a.q.q;
import b.a.q.r;
import b.a.q.x;
import b.a.q.z;
import co.discord.media_engine.InboundRtpAudio;
import co.discord.media_engine.OutboundRtpAudio;
import co.discord.media_engine.Stats;
import co.discord.media_engine.VoiceQuality;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.rtcconnection.MediaSinkWantsManager;
import com.discord.rtcconnection.mediaengine.MediaEngine;
import com.discord.rtcconnection.mediaengine.MediaEngineConnection;
import com.discord.rtcconnection.mediaengine.ThumbnailEmitter;
import com.discord.utilities.debug.DebugPrintBuilder;
import com.discord.utilities.debug.DebugPrintable;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.networking.Backoff;
import com.discord.utilities.networking.NetworkMonitor;
import com.discord.utilities.time.Clock;
import d0.g0.s;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.Subscription;
import rx.subjects.BehaviorSubject;
/* compiled from: RtcConnection.kt */
@kotlin.Metadata(bv = {1, 0, 3}, d1 = {"\u0000Ò\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010$\n\u0002\b\u0007\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010!\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r*\u0004\u0099\u0001¶\u0001\u0018\u0000  2\u00020\u00012\u00020\u00022\u00020\u0003:\u000eÚ\u0001N.?Û\u0001Ü\u0001\rÝ\u0001Þ\u0001J=\u0010\r\u001a\u00020\f2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\b\u001a\u00060\u0004j\u0002`\u00072\n\u0010\t\u001a\u00060\u0004j\u0002`\u00072\b\u0010\u000b\u001a\u0004\u0018\u00010\nH\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u000f\u0010\u0013\u001a\u00020\fH\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0017\u001a\u00020\f2\u0006\u0010\u0016\u001a\u00020\u0015H\u0002¢\u0006\u0004\b\u0017\u0010\u0018J!\u0010\u001b\u001a\u00020\f2\u0006\u0010\u001a\u001a\u00020\u00192\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fH\u0002¢\u0006\u0004\b\u001b\u0010\u001cJ/\u0010 \u001a\u00020\f2\n\u0010\u001d\u001a\u00060\u0004j\u0002`\u00052\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00030\u001eH\u0002¢\u0006\u0004\b \u0010!J1\u0010\"\u001a\u00020\f2\n\u0010\u001d\u001a\u00060\u0004j\u0002`\u00052\u0014\u0010\u001f\u001a\u0010\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u001eH\u0002¢\u0006\u0004\b\"\u0010!J1\u0010#\u001a\u00020\f2\n\u0010\u001d\u001a\u00060\u0004j\u0002`\u00052\u0014\u0010\u001f\u001a\u0010\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u001eH\u0002¢\u0006\u0004\b#\u0010!J\u001b\u0010$\u001a\u00020\u00192\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0002¢\u0006\u0004\b$\u0010%J\u001b\u0010'\u001a\u00020&2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u0005H\u0002¢\u0006\u0004\b'\u0010(J+\u0010,\u001a\u00020\f2\u0006\u0010*\u001a\u00020)2\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00030+H\u0002¢\u0006\u0004\b,\u0010-J+\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00030+*\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00030+H\u0002¢\u0006\u0004\b.\u0010/J\u001d\u00102\u001a\u00020\f2\f\u00101\u001a\b\u0012\u0004\u0012\u00020\f00H\u0003¢\u0006\u0004\b2\u00103J\u0017\u00105\u001a\u00020\f2\u0006\u00104\u001a\u00020\u000fH\u0002¢\u0006\u0004\b5\u0010\u0012J\u0017\u00108\u001a\u00020\f2\u0006\u00107\u001a\u000206H\u0016¢\u0006\u0004\b8\u00109J\r\u0010;\u001a\u00020:¢\u0006\u0004\b;\u0010<J\u0015\u0010?\u001a\u00020\f2\u0006\u0010>\u001a\u00020=¢\u0006\u0004\b?\u0010@J\r\u0010A\u001a\u00020\f¢\u0006\u0004\bA\u0010\u0014J!\u0010C\u001a\u00020\f2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\u0006\u0010B\u001a\u00020&¢\u0006\u0004\bC\u0010DJ#\u0010I\u001a\u00020\f2\b\u0010F\u001a\u0004\u0018\u00010E2\n\b\u0002\u0010H\u001a\u0004\u0018\u00010G¢\u0006\u0004\bI\u0010JJ\u0017\u0010K\u001a\u00020\f2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\bK\u0010LJ=\u0010N\u001a\u00020\f2\n\u0010\u0006\u001a\u00060\u0004j\u0002`\u00052\n\u0010\b\u001a\u00060\u0004j\u0002`\u00072\n\u0010M\u001a\u00060\u0004j\u0002`\u00072\b\u0010\u000b\u001a\u0004\u0018\u00010\nH\u0016¢\u0006\u0004\bN\u0010\u000eR\u0016\u0010R\u001a\u00020O8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bP\u0010QR\u001e\u0010V\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`S8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bT\u0010UR\u0019\u0010Z\u001a\u00020\u000f8\u0006@\u0006¢\u0006\f\n\u0004\b#\u0010W\u001a\u0004\bX\u0010YR\"\u0010a\u001a\u00020\u00198\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b[\u0010\\\u001a\u0004\b]\u0010^\"\u0004\b_\u0010`R\u001a\u0010\t\u001a\u00060\u0004j\u0002`\u00078\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bb\u0010[R\"\u0010f\u001a\u00020\u000f8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bc\u0010W\u001a\u0004\bd\u0010Y\"\u0004\be\u0010\u0012R\u0016\u0010g\u001a\u00020\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b,\u0010\\R\u001c\u0010j\u001a\b\u0012\u0004\u0012\u00020\u00040h8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bC\u0010iR\u001e\u0010l\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`S8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bk\u0010UR\u0016\u0010n\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bm\u0010\\R\u0018\u0010r\u001a\u0004\u0018\u00010o8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bp\u0010qR\u0016\u0010u\u001a\u00020s8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0013\u0010tR\u0016\u0010y\u001a\u00020v8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bw\u0010xR\u0016\u0010}\u001a\u00020z8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b{\u0010|R\u0019\u0010\u0080\u0001\u001a\u0004\u0018\u00010~8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bK\u0010\u007fR\u001a\u0010\u0084\u0001\u001a\u00030\u0081\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u0082\u0001\u0010\u0083\u0001R\u001c\u0010\u0087\u0001\u001a\u00020\u000f8\u0006@\u0006¢\u0006\u000e\n\u0005\b\u0085\u0001\u0010W\u001a\u0005\b\u0086\u0001\u0010YR\u001b\u0010\u008a\u0001\u001a\u0005\u0018\u00010\u0088\u00018\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b5\u0010\u0089\u0001R\u001c\u0010\u008e\u0001\u001a\u0005\u0018\u00010\u008b\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b\u008c\u0001\u0010\u008d\u0001R\u001a\u0010\u0090\u0001\u001a\u0004\u0018\u00010\u000f8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u008f\u0001\u0010WR\u001f\u0010\u0093\u0001\u001a\t\u0012\u0004\u0012\u00020=0\u0091\u00018\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\u001b\u0010\u0092\u0001R\"\u0010\u0098\u0001\u001a\u00070\u0004j\u0003`\u0094\u00018\u0006@\u0006¢\u0006\u000f\n\u0005\b\u0095\u0001\u0010[\u001a\u0006\b\u0096\u0001\u0010\u0097\u0001R\u001a\u0010\u009c\u0001\u001a\u00030\u0099\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b\u009a\u0001\u0010\u009b\u0001R\u0017\u0010\u009d\u0001\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\"\u0010WR\u0019\u0010 \u0001\u001a\u00030\u009e\u00018\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b2\u0010\u009f\u0001R\u0018\u0010¢\u0001\u001a\u00020v8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b¡\u0001\u0010xR\u001b\u0010£\u0001\u001a\u0005\u0018\u00010\u008b\u00018\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\bx\u0010\u008d\u0001R&\u0010¨\u0001\u001a\u000b\u0018\u00010\u0004j\u0005\u0018\u0001`¤\u00018\u0006@\u0006¢\u0006\u000f\n\u0005\b¥\u0001\u0010U\u001a\u0006\b¦\u0001\u0010§\u0001R\u001a\u0010¬\u0001\u001a\u00030©\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bª\u0001\u0010«\u0001R \u0010®\u0001\u001a\n\u0018\u00010\u0004j\u0004\u0018\u0001`S8\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\b\u00ad\u0001\u0010UR\u001a\u0010°\u0001\u001a\u0004\u0018\u00010\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b¯\u0001\u0010WR\u0017\u0010\u0006\u001a\u00020\u00048\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b±\u0001\u0010[R\u001c\u0010µ\u0001\u001a\u0005\u0018\u00010²\u00018\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b³\u0001\u0010´\u0001R\u001a\u0010¹\u0001\u001a\u00030¶\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\b·\u0001\u0010¸\u0001R\u0018\u0010»\u0001\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\bº\u0001\u0010\\R\u0018\u0010½\u0001\u001a\u00020\u000f8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b¼\u0001\u0010WRE\u0010Á\u0001\u001a/\u0012\u000f\u0012\r ¿\u0001*\u0005\u0018\u00010\u009e\u00010\u009e\u0001 ¿\u0001*\u0016\u0012\u000f\u0012\r ¿\u0001*\u0005\u0018\u00010\u009e\u00010\u009e\u0001\u0018\u00010¾\u00010¾\u00018\u0002@\u0002X\u0082\u000e¢\u0006\u0007\n\u0005\bI\u0010À\u0001R:\u0010Æ\u0001\u001a\u000b\u0018\u00010\u000fj\u0005\u0018\u0001`Â\u00012\u0010\u0010Ã\u0001\u001a\u000b\u0018\u00010\u000fj\u0005\u0018\u0001`Â\u00018\u0006@BX\u0086\u000e¢\u0006\u000e\n\u0005\bÄ\u0001\u0010W\u001a\u0005\bÅ\u0001\u0010YR)\u0010É\u0001\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020\u00190+8\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÇ\u0001\u0010È\u0001R!\u0010Ì\u0001\u001a\u000b\u0018\u00010\u000fj\u0005\u0018\u0001`Ê\u00018\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\bË\u0001\u0010WR(\u0010Í\u0001\u001a\u0012\u0012\b\u0012\u00060\u0004j\u0002`\u0005\u0012\u0004\u0012\u00020&0+8\u0002@\u0002X\u0082\u0004¢\u0006\u0007\n\u0005\b\\\u0010È\u0001R\u001c\u0010Ñ\u0001\u001a\u0005\u0018\u00010Î\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÏ\u0001\u0010Ð\u0001R\u001a\u0010Õ\u0001\u001a\u00030Ò\u00018\u0002@\u0002X\u0082\u0004¢\u0006\b\n\u0006\bÓ\u0001\u0010Ô\u0001R\u0017\u0010Ö\u0001\u001a\u00020\u00198\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0017\u0010\\R\u001b\u0010Ù\u0001\u001a\u0004\u0018\u00010v8\u0002@\u0002X\u0082\u000e¢\u0006\b\n\u0006\b×\u0001\u0010Ø\u0001¨\u0006ß\u0001"}, d2 = {"Lcom/discord/rtcconnection/RtcConnection;", "Lcom/discord/utilities/debug/DebugPrintable;", "Lcom/discord/rtcconnection/MediaSinkWantsManager$a;", "", "", "Lcom/discord/primitives/UserId;", "userId", "Lcom/discord/primitives/SSRC;", "audioSsrc", "videoSsrc", "Lcom/discord/rtcconnection/VideoMetadata;", "metadata", "", "d", "(JJJLcom/discord/rtcconnection/VideoMetadata;)V", "", ModelAuditLogEntry.CHANGE_KEY_REASON, "f", "(Ljava/lang/String;)V", "q", "()V", "Lcom/discord/rtcconnection/RtcConnection$State;", "state", "u", "(Lcom/discord/rtcconnection/RtcConnection$State;)V", "", "willReconnect", "n", "(ZLjava/lang/String;)V", "senderId", "", "properties", "k", "(JLjava/util/Map;)V", "l", "m", "g", "(J)Z", "", "h", "(J)F", "Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;", "event", "", "p", "(Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;Ljava/util/Map;)V", "b", "(Ljava/util/Map;)Ljava/util/Map;", "Lkotlin/Function0;", "action", "s", "(Lkotlin/jvm/functions/Function0;)V", "message", "r", "Lcom/discord/utilities/debug/DebugPrintBuilder;", "dp", "debugPrint", "(Lcom/discord/utilities/debug/DebugPrintBuilder;)V", "Lcom/discord/rtcconnection/RtcConnection$Metadata;", "i", "()Lcom/discord/rtcconnection/RtcConnection$Metadata;", "Lcom/discord/rtcconnection/RtcConnection$c;", "listener", "c", "(Lcom/discord/rtcconnection/RtcConnection$c;)V", "e", "volume", "v", "(JF)V", "Landroid/content/Intent;", "intent", "Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;", "thumbnailEmitter", "t", "(Landroid/content/Intent;Lcom/discord/rtcconnection/mediaengine/ThumbnailEmitter;)V", "w", "(Ljava/lang/Long;)V", "videoSSRC", "a", "Lcom/discord/utilities/logging/Logger;", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "Lcom/discord/utilities/logging/Logger;", "logger", "Lcom/discord/primitives/Timestamp;", "G", "Ljava/lang/Long;", "networkLossTime", "Ljava/lang/String;", "getId", "()Ljava/lang/String;", ModelAuditLogEntry.CHANGE_KEY_ID, "J", "Z", "getConnected", "()Z", "setConnected", "(Z)V", "connected", "L", "Q", "getSessionId", "setSessionId", "sessionId", "sentVideo", "", "Ljava/util/List;", "pings", "C", "connectStartTime", "b0", "enableMediaSinkWants", "Lb/a/q/o0/d;", "o", "Lb/a/q/o0/d;", "rtcStatsCollector", "Lcom/discord/utilities/networking/Backoff;", "Lcom/discord/utilities/networking/Backoff;", "reconnectBackoff", "", ExifInterface.LONGITUDE_EAST, "I", "connectCount", "Lcom/discord/rtcconnection/RtcConnection$d;", "X", "Lcom/discord/rtcconnection/RtcConnection$d;", "rtcConnectionType", "Lb/a/q/n0/a;", "Lb/a/q/n0/a;", "socket", "Lb/a/q/o0/e;", "B", "Lb/a/q/o0/e;", "videoQuality", "c0", "getLoggingTagPrefix", "loggingTagPrefix", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection$TransportInfo;", "transportInfo", "Lrx/Subscription;", "F", "Lrx/Subscription;", "mediaEnginePrepareSubscription", "y", "hostname", "Ljava/util/concurrent/CopyOnWriteArrayList;", "Ljava/util/concurrent/CopyOnWriteArrayList;", "listeners", "Lcom/discord/primitives/ChannelId;", "P", "getChannelId", "()J", "channelId", "b/a/q/b0", "N", "Lb/a/q/b0;", "mediaEngineConnectionListener", "loggingTag", "Lcom/discord/rtcconnection/RtcConnection$StateChange;", "Lcom/discord/rtcconnection/RtcConnection$StateChange;", "connectionStateChange", ExifInterface.GPS_MEASUREMENT_IN_PROGRESS, "pingBadCount", "localMediaSinkWantsSubscription", "Lcom/discord/primitives/GuildId;", "O", "getGuildId", "()Ljava/lang/Long;", "guildId", "Lcom/discord/rtcconnection/mediaengine/MediaEngine;", "U", "Lcom/discord/rtcconnection/mediaengine/MediaEngine;", "mediaEngine", "D", "connectCompletedTime", "a0", "parentMediaSessionId", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;", "x", "Lcom/discord/rtcconnection/mediaengine/MediaEngineConnection;", "mediaEngineConnection", "b/a/q/h0", "M", "Lb/a/q/h0;", "socketListener", "R", "isVideoEnabled", ExifInterface.LATITUDE_SOUTH, "rtcServerId", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "Lrx/subjects/BehaviorSubject;", "connectionStateSubject", "Lcom/discord/primitives/MediaSessionId;", "<set-?>", "K", "getMediaSessionId", "mediaSessionId", "Y", "Ljava/util/Map;", "mutedUsers", "Lcom/discord/primitives/StreamKey;", "d0", "streamKey", "userVolumes", "Lcom/discord/rtcconnection/MediaSinkWantsManager;", "H", "Lcom/discord/rtcconnection/MediaSinkWantsManager;", "localMediaSinkWantsManager", "Lcom/discord/utilities/time/Clock;", ExifInterface.LONGITUDE_WEST, "Lcom/discord/utilities/time/Clock;", "clock", "destroyed", "z", "Ljava/lang/Integer;", "port", "AnalyticsEvent", "Metadata", "Quality", "State", "StateChange", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class RtcConnection implements DebugPrintable, MediaSinkWantsManager.a {
    public static int j;
    public static final a k = new a(null);
    public int A;
    public final b.a.q.o0.e B;
    public Long C;
    public Long D;
    public int E;
    public Subscription F;
    public Long G;
    public final MediaSinkWantsManager H;
    public final Subscription I;
    public boolean J;
    public String K;
    public long L;
    public final h0 M;
    public final b0 N;
    public final Long O;
    public final long P;
    public String Q;
    public final boolean R;
    public final String S;
    public final long T;
    public final MediaEngine U;
    public final Logger V;
    public final Clock W;
    public final d X;
    public final Map<Long, Boolean> Y;
    public final Map<Long, Float> Z;

    /* renamed from: a0  reason: collision with root package name */
    public final String f2745a0;

    /* renamed from: b0  reason: collision with root package name */
    public final boolean f2746b0;

    /* renamed from: c0  reason: collision with root package name */
    public final String f2747c0;

    /* renamed from: d0  reason: collision with root package name */
    public final String f2748d0;
    public final String l;
    public final String m;
    public final CopyOnWriteArrayList<c> n;
    public b.a.q.o0.d o;
    public boolean p;
    public final Backoff q;
    public MediaEngineConnection.TransportInfo r;

    /* renamed from: s  reason: collision with root package name */
    public StateChange f2749s;
    public BehaviorSubject<StateChange> t;
    public boolean u;
    public List<Long> v;
    public b.a.q.n0.a w;

    /* renamed from: x  reason: collision with root package name */
    public MediaEngineConnection f2750x;

    /* renamed from: y  reason: collision with root package name */
    public String f2751y;

    /* renamed from: z  reason: collision with root package name */
    public Integer f2752z;

    /* compiled from: RtcConnection.kt */
    @kotlin.Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lcom/discord/rtcconnection/RtcConnection$AnalyticsEvent;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "VOICE_CONNECTION_SUCCESS", "VOICE_CONNECTION_FAILURE", "VOICE_DISCONNECT", "VIDEO_STREAM_ENDED", "MEDIA_SESSION_JOINED", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum AnalyticsEvent {
        VOICE_CONNECTION_SUCCESS,
        VOICE_CONNECTION_FAILURE,
        VOICE_DISCONNECT,
        VIDEO_STREAM_ENDED,
        MEDIA_SESSION_JOINED
    }

    /* compiled from: RtcConnection.kt */
    @kotlin.Metadata(bv = {1, 0, 3}, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0086\b\u0018\u00002\u00020\u0001BS\u0012\n\u0010#\u001a\u00060\u0002j\u0002` \u0012\u000e\u0010\u001b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0018\u0012\u000e\u0010\u001f\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\u001c\u0012\u000e\u0010\u0012\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\r\u0012\u000e\u0010\u0017\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u0013¢\u0006\u0004\b$\u0010%J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR!\u0010\u0012\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\r8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R!\u0010\u0017\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0004R!\u0010\u001b\u001a\n\u0018\u00010\u0002j\u0004\u0018\u0001`\u00188\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u0015\u001a\u0004\b\u001a\u0010\u0004R!\u0010\u001f\u001a\n\u0018\u00010\fj\u0004\u0018\u0001`\u001c8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u000f\u001a\u0004\b\u001e\u0010\u0011R\u001d\u0010#\u001a\u00060\u0002j\u0002` 8\u0006@\u0006¢\u0006\f\n\u0004\b!\u0010\u0015\u001a\u0004\b\"\u0010\u0004¨\u0006&"}, d2 = {"Lcom/discord/rtcconnection/RtcConnection$Metadata;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "Lcom/discord/primitives/GuildId;", "d", "Ljava/lang/Long;", "getGuildId", "()Ljava/lang/Long;", "guildId", "Lcom/discord/primitives/StreamKey;", "e", "Ljava/lang/String;", "getStreamKey", "streamKey", "Lcom/discord/primitives/MediaSessionId;", "b", "getMediaSessionId", "mediaSessionId", "Lcom/discord/primitives/ChannelId;", "c", "getChannelId", "channelId", "Lcom/discord/primitives/RtcConnectionId;", "a", "getRtcConnectionId", "rtcConnectionId", HookHelper.constructorName, "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Metadata {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final String f2753b;
        public final Long c;
        public final Long d;
        public final String e;

        public Metadata(String str, String str2, Long l, Long l2, String str3) {
            m.checkNotNullParameter(str, "rtcConnectionId");
            this.a = str;
            this.f2753b = str2;
            this.c = l;
            this.d = l2;
            this.e = str3;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Metadata)) {
                return false;
            }
            Metadata metadata = (Metadata) obj;
            return m.areEqual(this.a, metadata.a) && m.areEqual(this.f2753b, metadata.f2753b) && m.areEqual(this.c, metadata.c) && m.areEqual(this.d, metadata.d) && m.areEqual(this.e, metadata.e);
        }

        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.f2753b;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            Long l = this.c;
            int hashCode3 = (hashCode2 + (l != null ? l.hashCode() : 0)) * 31;
            Long l2 = this.d;
            int hashCode4 = (hashCode3 + (l2 != null ? l2.hashCode() : 0)) * 31;
            String str3 = this.e;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Metadata(rtcConnectionId=");
            R.append(this.a);
            R.append(", mediaSessionId=");
            R.append(this.f2753b);
            R.append(", channelId=");
            R.append(this.c);
            R.append(", guildId=");
            R.append(this.d);
            R.append(", streamKey=");
            return b.d.b.a.a.H(R, this.e, ")");
        }
    }

    /* compiled from: RtcConnection.kt */
    @kotlin.Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\t\b\u0086\u0001\u0018\u0000 \u00042\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0005B\t\b\u0002¢\u0006\u0004\b\u0002\u0010\u0003j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\t¨\u0006\n"}, d2 = {"Lcom/discord/rtcconnection/RtcConnection$Quality;", "", HookHelper.constructorName, "(Ljava/lang/String;I)V", "Companion", "a", "UNKNOWN", "BAD", "AVERAGE", "FINE", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public enum Quality {
        UNKNOWN,
        BAD,
        AVERAGE,
        FINE;
        
        public static final a Companion = new a(null);

        /* compiled from: RtcConnection.kt */
        /* loaded from: classes.dex */
        public static final class a {
            public a(DefaultConstructorMarker defaultConstructorMarker) {
            }
        }
    }

    /* compiled from: RtcConnection.kt */
    @kotlin.Metadata(bv = {1, 0, 3}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\b\u0007\b\t\n\u000b\f\r\u000eB\t\b\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004\u0082\u0001\b\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016¨\u0006\u0017"}, d2 = {"Lcom/discord/rtcconnection/RtcConnection$State;", "", "", "toString", "()Ljava/lang/String;", HookHelper.constructorName, "()V", "a", "b", "c", "d", "e", "f", "g", "h", "Lcom/discord/rtcconnection/RtcConnection$State$d;", "Lcom/discord/rtcconnection/RtcConnection$State$b;", "Lcom/discord/rtcconnection/RtcConnection$State$a;", "Lcom/discord/rtcconnection/RtcConnection$State$c;", "Lcom/discord/rtcconnection/RtcConnection$State$h;", "Lcom/discord/rtcconnection/RtcConnection$State$g;", "Lcom/discord/rtcconnection/RtcConnection$State$f;", "Lcom/discord/rtcconnection/RtcConnection$State$e;", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static abstract class State {

        /* compiled from: RtcConnection.kt */
        /* loaded from: classes.dex */
        public static final class a extends State {
            public static final a a = new a();

            public a() {
                super(null);
            }
        }

        /* compiled from: RtcConnection.kt */
        /* loaded from: classes.dex */
        public static final class b extends State {
            public static final b a = new b();

            public b() {
                super(null);
            }
        }

        /* compiled from: RtcConnection.kt */
        /* loaded from: classes.dex */
        public static final class c extends State {
            public static final c a = new c();

            public c() {
                super(null);
            }
        }

        /* compiled from: RtcConnection.kt */
        /* loaded from: classes.dex */
        public static final class d extends State {
            public final boolean a;

            public d(boolean z2) {
                super(null);
                this.a = z2;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof d) && this.a == ((d) obj).a;
                }
                return true;
            }

            public int hashCode() {
                boolean z2 = this.a;
                if (z2) {
                    return 1;
                }
                return z2 ? 1 : 0;
            }

            @Override // com.discord.rtcconnection.RtcConnection.State
            public String toString() {
                return b.d.b.a.a.M(b.d.b.a.a.R("Disconnected(willReconnect="), this.a, ")");
            }
        }

        /* compiled from: RtcConnection.kt */
        /* loaded from: classes.dex */
        public static final class e extends State {
            public static final e a = new e();

            public e() {
                super(null);
            }
        }

        /* compiled from: RtcConnection.kt */
        /* loaded from: classes.dex */
        public static final class f extends State {
            public static final f a = new f();

            public f() {
                super(null);
            }
        }

        /* compiled from: RtcConnection.kt */
        /* loaded from: classes.dex */
        public static final class g extends State {
            public static final g a = new g();

            public g() {
                super(null);
            }
        }

        /* compiled from: RtcConnection.kt */
        /* loaded from: classes.dex */
        public static final class h extends State {
            public static final h a = new h();

            public h() {
                super(null);
            }
        }

        public State() {
        }

        public String toString() {
            if (this instanceof d) {
                return super.toString();
            }
            String simpleName = getClass().getSimpleName();
            m.checkNotNullExpressionValue(simpleName, "javaClass.simpleName");
            return simpleName;
        }

        public State(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: RtcConnection.kt */
    @kotlin.Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\b\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0017\u001a\u00020\u0012\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b\u0018\u0010\u0019J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR\u001b\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0017\u001a\u00020\u00128\u0006@\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016¨\u0006\u001a"}, d2 = {"Lcom/discord/rtcconnection/RtcConnection$StateChange;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lcom/discord/rtcconnection/RtcConnection$Metadata;", "b", "Lcom/discord/rtcconnection/RtcConnection$Metadata;", "getMetadata", "()Lcom/discord/rtcconnection/RtcConnection$Metadata;", "metadata", "Lcom/discord/rtcconnection/RtcConnection$State;", "a", "Lcom/discord/rtcconnection/RtcConnection$State;", "getState", "()Lcom/discord/rtcconnection/RtcConnection$State;", "state", HookHelper.constructorName, "(Lcom/discord/rtcconnection/RtcConnection$State;Lcom/discord/rtcconnection/RtcConnection$Metadata;)V", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class StateChange {
        public final State a;

        /* renamed from: b  reason: collision with root package name */
        public final Metadata f2754b;

        public StateChange(State state, Metadata metadata) {
            m.checkNotNullParameter(state, "state");
            this.a = state;
            this.f2754b = metadata;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StateChange)) {
                return false;
            }
            StateChange stateChange = (StateChange) obj;
            return m.areEqual(this.a, stateChange.a) && m.areEqual(this.f2754b, stateChange.f2754b);
        }

        public int hashCode() {
            State state = this.a;
            int i = 0;
            int hashCode = (state != null ? state.hashCode() : 0) * 31;
            Metadata metadata = this.f2754b;
            if (metadata != null) {
                i = metadata.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("StateChange(state=");
            R.append(this.a);
            R.append(", metadata=");
            R.append(this.f2754b);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: RtcConnection.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: RtcConnection.kt */
    /* loaded from: classes.dex */
    public static abstract class b implements c {
        @Override // com.discord.rtcconnection.RtcConnection.c
        public void onAnalyticsEvent(AnalyticsEvent analyticsEvent, Map<String, Object> map) {
            m.checkNotNullParameter(analyticsEvent, "event");
            m.checkNotNullParameter(map, "properties");
        }

        @Override // com.discord.rtcconnection.RtcConnection.c
        public void onFatalClose() {
        }

        public void onFirstFrameReceived(long j) {
        }

        public void onFirstFrameSent() {
        }

        @Override // com.discord.rtcconnection.RtcConnection.c
        public void onMediaEngineConnectionConnected(RtcConnection rtcConnection) {
            m.checkNotNullParameter(rtcConnection, "connection");
        }

        @Override // com.discord.rtcconnection.RtcConnection.c
        public void onMediaSessionIdReceived() {
        }

        @Override // com.discord.rtcconnection.RtcConnection.c
        public void onQualityUpdate(Quality quality) {
            m.checkNotNullParameter(quality, "quality");
        }

        @Override // com.discord.rtcconnection.RtcConnection.c
        public void onSpeaking(long j, boolean z2) {
        }

        @Override // com.discord.rtcconnection.RtcConnection.c
        public abstract void onStateChange(StateChange stateChange);

        @Override // com.discord.rtcconnection.RtcConnection.c
        public void onUserCreated(RtcConnection rtcConnection, long j) {
            m.checkNotNullParameter(rtcConnection, "connection");
        }

        @Override // com.discord.rtcconnection.RtcConnection.c
        public void onVideoMetadata(VideoMetadata videoMetadata) {
            m.checkNotNullParameter(videoMetadata, "metadata");
        }

        @Override // com.discord.rtcconnection.RtcConnection.c
        public void onVideoStream(long j, Integer num, int i, int i2, int i3) {
        }
    }

    /* compiled from: RtcConnection.kt */
    /* loaded from: classes.dex */
    public interface c {
        void onAnalyticsEvent(AnalyticsEvent analyticsEvent, Map<String, Object> map);

        void onFatalClose();

        void onMediaEngineConnectionConnected(RtcConnection rtcConnection);

        void onMediaSessionIdReceived();

        void onQualityUpdate(Quality quality);

        void onSpeaking(long j, boolean z2);

        void onStateChange(StateChange stateChange);

        void onUserCreated(RtcConnection rtcConnection, long j);

        void onVideoMetadata(VideoMetadata videoMetadata);

        void onVideoStream(long j, Integer num, int i, int i2, int i3);
    }

    /* compiled from: RtcConnection.kt */
    /* loaded from: classes.dex */
    public static abstract class d {

        /* compiled from: RtcConnection.kt */
        /* loaded from: classes.dex */
        public static final class a extends d {
            public static final a a = new a();

            public a() {
                super(null);
            }
        }

        /* compiled from: RtcConnection.kt */
        /* loaded from: classes.dex */
        public static final class b extends d {
            public final long a;

            public b(long j) {
                super(null);
                this.a = j;
            }

            public boolean equals(Object obj) {
                if (this != obj) {
                    return (obj instanceof b) && this.a == ((b) obj).a;
                }
                return true;
            }

            public int hashCode() {
                return a0.a.a.b.a(this.a);
            }

            public String toString() {
                return b.d.b.a.a.B(b.d.b.a.a.R("Stream(senderId="), this.a, ")");
            }
        }

        public d(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: RtcConnection.kt */
    /* loaded from: classes.dex */
    public static final class e extends o implements Function0<Unit> {
        public e() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public Unit invoke() {
            RtcConnection.this.f("Force Close");
            return Unit.a;
        }
    }

    public RtcConnection(Long l, long j2, String str, boolean z2, String str2, long j3, MediaEngine mediaEngine, Logger logger, Clock clock, d dVar, NetworkMonitor networkMonitor, Map map, Map map2, String str3, boolean z3, String str4, String str5, int i) {
        Subscription subscription;
        MediaSinkWantsManager mediaSinkWantsManager;
        d dVar2 = (i & 512) != 0 ? d.a.a : dVar;
        Map linkedHashMap = (i & 2048) != 0 ? new LinkedHashMap() : map;
        LinkedHashMap linkedHashMap2 = (i & 4096) != 0 ? new LinkedHashMap() : null;
        String str6 = (i & 8192) != 0 ? null : str3;
        boolean z4 = (i & 16384) != 0 ? false : z3;
        String str7 = (i & 65536) != 0 ? null : str5;
        m.checkNotNullParameter(str, "sessionId");
        m.checkNotNullParameter(str2, "rtcServerId");
        m.checkNotNullParameter(mediaEngine, "mediaEngine");
        m.checkNotNullParameter(logger, "logger");
        m.checkNotNullParameter(clock, "clock");
        m.checkNotNullParameter(dVar2, "rtcConnectionType");
        boolean z5 = z4;
        m.checkNotNullParameter(networkMonitor, "networkMonitor");
        m.checkNotNullParameter(linkedHashMap, "mutedUsers");
        m.checkNotNullParameter(linkedHashMap2, "userVolumes");
        m.checkNotNullParameter(str4, "loggingTagPrefix");
        this.O = l;
        this.P = j2;
        this.Q = str;
        this.R = z2;
        this.S = str2;
        this.T = j3;
        this.U = mediaEngine;
        this.V = logger;
        this.W = clock;
        this.X = dVar2;
        this.Y = linkedHashMap;
        this.Z = linkedHashMap2;
        this.f2745a0 = str6;
        this.f2746b0 = z5;
        this.f2747c0 = str4;
        this.f2748d0 = str7;
        StringBuilder V = b.d.b.a.a.V(str4, "->RtcConnection ");
        int i2 = j + 1;
        j = i2;
        V.append(i2);
        this.l = V.toString();
        String uuid = UUID.randomUUID().toString();
        m.checkNotNullExpressionValue(uuid, "UUID.randomUUID().toString()");
        this.m = uuid;
        CopyOnWriteArrayList<c> copyOnWriteArrayList = new CopyOnWriteArrayList<>();
        this.n = copyOnWriteArrayList;
        this.q = new Backoff(1000L, 10000L, 0, false, null, 28, null);
        StateChange stateChange = new StateChange(new State.d(false), null);
        this.f2749s = stateChange;
        this.t = BehaviorSubject.l0(stateChange);
        this.v = new ArrayList();
        b.a.q.o0.e eVar = new b.a.q.o0.e(logger, clock);
        eVar.f.add(new x(this));
        this.B = eVar;
        if (z5) {
            subscription = null;
            mediaSinkWantsManager = new MediaSinkWantsManager(j3, mediaEngine.c(), new b.a.q.e(null, 1), logger, this);
        } else {
            mediaSinkWantsManager = null;
            subscription = null;
        }
        this.H = mediaSinkWantsManager;
        if (z5) {
            m.checkNotNull(mediaSinkWantsManager);
            Observable<Map<String, EncodeQuality>> q = mediaSinkWantsManager.f.q();
            m.checkNotNullExpressionValue(q, "mediaSinkWantsSubject.distinctUntilChanged()");
            subscription = Observable.j(q, this.t, z.j).V(new g0(new a0(this)));
        }
        this.I = subscription;
        r("Created RtcConnection. GuildID: " + l + " ChannelID: " + j2);
        networkMonitor.observeIsConnected().S(1).W(new p(this), new q(this));
        copyOnWriteArrayList.add(new r(this));
        this.M = new h0(this);
        this.N = new b0(this);
    }

    public static void j(RtcConnection rtcConnection, boolean z2, String str, Throwable th, boolean z3, int i) {
        if ((i & 4) != 0) {
            th = null;
        }
        if ((i & 8) != 0) {
            z3 = true;
        }
        if (z3) {
            o(rtcConnection, str, th, null, 4);
        } else {
            rtcConnection.V.i(rtcConnection.l, str, th);
        }
        b.a.q.o0.d dVar = rtcConnection.o;
        if (dVar != null) {
            dVar.a();
        }
        rtcConnection.o = null;
        MediaEngineConnection mediaEngineConnection = rtcConnection.f2750x;
        if (mediaEngineConnection != null) {
            mediaEngineConnection.destroy();
        }
        rtcConnection.f2750x = null;
        rtcConnection.p = false;
        rtcConnection.q.cancel();
        rtcConnection.u(new State.d(z2));
        if (z2) {
            rtcConnection.q();
            return;
        }
        for (c cVar : rtcConnection.n) {
            cVar.onFatalClose();
        }
        rtcConnection.f(str);
    }

    public static void o(RtcConnection rtcConnection, String str, Throwable th, Map map, int i) {
        if ((i & 2) != 0) {
            th = null;
        }
        int i2 = i & 4;
        rtcConnection.V.e(rtcConnection.l, str, th, null);
    }

    @Override // com.discord.rtcconnection.MediaSinkWantsManager.a
    public void a(long j2, long j3, long j4, VideoMetadata videoMetadata) {
        d(j2, j3, j4, videoMetadata);
    }

    public final Map<String, Object> b(Map<String, Object> map) {
        String str = this.f2751y;
        if (str != null) {
            map.put("hostname", str);
        }
        Integer num = this.f2752z;
        if (num != null) {
            map.put("port", Integer.valueOf(num.intValue()));
        }
        return map;
    }

    public final void c(c cVar) {
        m.checkNotNullParameter(cVar, "listener");
        this.n.add(cVar);
    }

    public final void d(long j2, long j3, long j4, VideoMetadata videoMetadata) {
        MediaEngineConnection mediaEngineConnection = this.f2750x;
        if (mediaEngineConnection != null) {
            mediaEngineConnection.s(j2, (int) j3, Integer.valueOf((int) j4), g(j2), h(j2));
        }
        for (c cVar : this.n) {
            cVar.onUserCreated(this, j2);
        }
        if (videoMetadata != null) {
            for (c cVar2 : this.n) {
                cVar2.onVideoMetadata(videoMetadata);
            }
        }
    }

    @Override // com.discord.utilities.debug.DebugPrintable
    public void debugPrint(DebugPrintBuilder debugPrintBuilder) {
        m.checkNotNullParameter(debugPrintBuilder, "dp");
        debugPrintBuilder.appendKeyValue(ModelAuditLogEntry.CHANGE_KEY_ID, this.m);
        debugPrintBuilder.appendKeyValue("mediaSessionId", this.K);
        debugPrintBuilder.appendKeyValue("parentMediaSessionId", this.f2745a0);
        debugPrintBuilder.appendKeyValue("hostname", this.f2751y);
        debugPrintBuilder.appendKeyValue("channelId", Long.valueOf(this.P));
        debugPrintBuilder.appendKeyValue("guildId", this.O);
        debugPrintBuilder.appendKeyValue("streamKey", this.f2748d0);
        debugPrintBuilder.appendKeyValue("isVideoEnabled", Boolean.valueOf(this.R));
        debugPrintBuilder.appendKeyValue("rtcServerId", this.S);
        debugPrintBuilder.appendKeyValue("userId", Long.valueOf(this.T));
        debugPrintBuilder.appendKeyValue("rtcConnectionType", this.X);
        debugPrintBuilder.appendKeyValue("enableMediaSinkWants", Boolean.valueOf(this.f2746b0));
        debugPrintBuilder.appendKeyValue("socket", (DebugPrintable) this.w);
        debugPrintBuilder.appendKeyValue("sentVideo", Boolean.valueOf(this.p));
    }

    public final void e() {
        s(new e());
    }

    public final void f(String str) {
        this.q.cancel();
        b.a.q.n0.a aVar = this.w;
        if (aVar != null) {
            aVar.q.clear();
            aVar.c();
        }
        this.w = null;
        b.a.q.o0.e eVar = this.B;
        synchronized (eVar) {
            eVar.o.stop();
            eVar.h = Long.valueOf(eVar.q.currentTimeMillis());
        }
        if (!(this.f2749s.a instanceof State.d)) {
            n(false, str);
            d dVar = this.X;
            if (dVar instanceof d.b) {
                Map<String, Object> c2 = this.B.c(String.valueOf(((d.b) dVar).a));
                if (c2 != null) {
                    l(((d.b) this.X).a, c2);
                }
                if (this.p) {
                    m(this.T, this.B.d());
                }
            }
        }
        MediaSinkWantsManager mediaSinkWantsManager = this.H;
        if (mediaSinkWantsManager != null) {
            mediaSinkWantsManager.b(new g(mediaSinkWantsManager));
        }
        Subscription subscription = this.I;
        if (subscription != null) {
            subscription.unsubscribe();
        }
        Subscription subscription2 = this.F;
        if (subscription2 != null) {
            subscription2.unsubscribe();
        }
        this.F = null;
        MediaEngineConnection mediaEngineConnection = this.f2750x;
        if (mediaEngineConnection != null) {
            mediaEngineConnection.destroy();
        }
        this.f2750x = null;
        u(new State.d(false));
        r("Destroy internal RTC connection: " + str);
        this.n.clear();
        this.p = false;
        this.u = true;
    }

    public final boolean g(long j2) {
        Boolean bool = this.Y.get(Long.valueOf(j2));
        if (bool != null) {
            return bool.booleanValue();
        }
        return false;
    }

    public final float h(long j2) {
        Float f = this.Z.get(Long.valueOf(j2));
        if (f != null) {
            return f.floatValue();
        }
        return 1.0f;
    }

    public final Metadata i() {
        return new Metadata(this.m, this.K, Long.valueOf(this.P), this.O, this.f2748d0);
    }

    public final void k(long j2, Map<String, ? extends Object> map) {
        HashMap hashMap = new HashMap();
        Long l = this.O;
        if (l != null) {
            l.longValue();
            hashMap.put(ModelAuditLogEntry.CHANGE_KEY_GUILD_ID, this.O);
        }
        hashMap.put(ModelAuditLogEntry.CHANGE_KEY_CHANNEL_ID, Long.valueOf(this.P));
        hashMap.put("sender_user_id", Long.valueOf(j2));
        hashMap.putAll(map);
        p(AnalyticsEvent.VIDEO_STREAM_ENDED, hashMap);
    }

    public final void l(long j2, Map<String, ? extends Object> map) {
        if (map != null) {
            k(j2, d0.t.h0.plus(map, d0.t.g0.mapOf(d0.o.to("participant_type", "receiver"))));
        }
    }

    public final void m(long j2, Map<String, ? extends Object> map) {
        if (map != null) {
            k(j2, d0.t.h0.plus(map, d0.t.g0.mapOf(d0.o.to("participant_type", this.X instanceof d.b ? "streamer" : NotificationCompat.MessagingStyle.Message.KEY_SENDER))));
        }
    }

    public final void n(boolean z2, String str) {
        List<String> list;
        String str2;
        Stats stats;
        VoiceQuality voiceQuality;
        Map<String, Object> mutableMapOf = d0.t.h0.mutableMapOf(d0.o.to("ping_bad_count", Integer.valueOf(this.A)), d0.o.to("connect_count", Integer.valueOf(this.E)), d0.o.to("channel_count", 1));
        b(mutableMapOf);
        mutableMapOf.put("reconnect", Boolean.valueOf(z2));
        if (str != null) {
            mutableMapOf.put(ModelAuditLogEntry.CHANGE_KEY_REASON, str);
        }
        double averageOfLong = u.averageOfLong(this.v);
        if (!Double.isNaN(averageOfLong)) {
            mutableMapOf.put("ping_average", Integer.valueOf(d0.a0.a.roundToInt(averageOfLong)));
        }
        String str3 = this.K;
        if (str3 != null) {
            mutableMapOf.put("media_session_id", str3);
        }
        b.a.q.o0.d dVar = this.o;
        if (!(dVar == null || (stats = (Stats) u.lastOrNull(dVar.a)) == null)) {
            OutboundRtpAudio outboundRtpAudio = stats.getOutboundRtpAudio();
            if (outboundRtpAudio != null) {
                mutableMapOf.put("packets_sent", Long.valueOf(outboundRtpAudio.getPacketsSent()));
                mutableMapOf.put("packets_sent_lost", Integer.valueOf(outboundRtpAudio.getPacketsLost()));
            }
            long j2 = 0;
            long j3 = 0;
            for (InboundRtpAudio inboundRtpAudio : stats.getInboundRtpAudio().values()) {
                j3 += inboundRtpAudio.getPacketsLost();
                j2 += inboundRtpAudio.getPacketsReceived();
            }
            mutableMapOf.put("packets_received", Long.valueOf(j2));
            mutableMapOf.put("packets_received_lost", Long.valueOf(j3));
            b.a.q.o0.d dVar2 = this.o;
            if (!(dVar2 == null || (voiceQuality = dVar2.f) == null)) {
                voiceQuality.getDurationStats(mutableMapOf);
                voiceQuality.getMosStats(mutableMapOf);
                voiceQuality.getPacketStats(mutableMapOf);
                voiceQuality.getBufferStats(mutableMapOf);
                voiceQuality.getFrameOpStats(mutableMapOf);
            }
        }
        Long l = this.D;
        MediaEngineConnection.TransportInfo.Protocol protocol = null;
        Long valueOf = l != null ? Long.valueOf(this.W.currentTimeMillis() - l.longValue()) : null;
        if (valueOf != null) {
            mutableMapOf.put("duration", Long.valueOf(valueOf.longValue()));
        }
        MediaEngineConnection.TransportInfo transportInfo = this.r;
        if (transportInfo != null) {
            protocol = transportInfo.c;
        }
        if (protocol != null) {
            int ordinal = protocol.ordinal();
            if (ordinal == 0) {
                str2 = "udp";
            } else if (ordinal == 1) {
                str2 = "tcp";
            } else {
                throw new NoWhenBranchMatchedException();
            }
            mutableMapOf.put("protocol", str2);
        }
        p(AnalyticsEvent.VOICE_DISCONNECT, mutableMapOf);
        if (this.X instanceof d.a) {
            b.a.q.o0.e eVar = this.B;
            synchronized (eVar) {
                list = u.toList(eVar.m.keySet());
            }
            for (String str4 : list) {
                Long longOrNull = s.toLongOrNull(str4);
                if (longOrNull != null) {
                    l(longOrNull.longValue(), this.B.c(str4));
                }
            }
            if (this.p) {
                m(this.T, this.B.d());
            }
        }
    }

    public final void p(AnalyticsEvent analyticsEvent, Map<String, Object> map) {
        String str;
        map.put("rtc_connection_id", this.m);
        d dVar = this.X;
        if (m.areEqual(dVar, d.a.a)) {
            str = "default";
        } else if (dVar instanceof d.b) {
            str = "stream";
        } else {
            throw new NoWhenBranchMatchedException();
        }
        map.put("context", str);
        String str2 = this.K;
        if (str2 != null) {
            map.put("media_session_id", str2);
        }
        String str3 = this.f2745a0;
        if (str3 != null) {
            map.put("parent_media_session_id", str3);
        }
        for (c cVar : this.n) {
            cVar.onAnalyticsEvent(analyticsEvent, map);
        }
    }

    public final void q() {
        this.V.recordBreadcrumb("reconnect", this.l);
        if (this.J) {
            this.C = Long.valueOf(this.W.currentTimeMillis());
        }
        this.E++;
        b.a.q.n0.a aVar = this.w;
        if (aVar != null) {
            aVar.c();
            aVar.d();
        }
    }

    public final void r(String str) {
        this.V.recordBreadcrumb(str, this.l);
    }

    @AnyThread
    public final void s(Function0<Unit> function0) {
        b.a.q.c c2 = this.U.c();
        Object obj = function0;
        if (function0 != null) {
            obj = new f0(function0);
        }
        c2.l.execute((Runnable) obj);
    }

    public final void t(Intent intent, ThumbnailEmitter thumbnailEmitter) {
        if (this.X instanceof d.b) {
            r("Setting screenshare " + intent + ' ' + this.f2750x);
            MediaEngineConnection mediaEngineConnection = this.f2750x;
            if (mediaEngineConnection == null) {
                Log.e("RtcConnection", "MediaEngine not connected for setScreenshare.");
            } else if (intent != null) {
                mediaEngineConnection.a(intent, thumbnailEmitter);
            } else {
                mediaEngineConnection.h();
            }
        }
    }

    public final void u(State state) {
        if (!m.areEqual(this.f2749s.a, state)) {
            StateChange stateChange = new StateChange(state, i());
            this.f2749s = stateChange;
            for (c cVar : this.n) {
                cVar.onStateChange(stateChange);
            }
        }
    }

    public final void v(long j2, float f) {
        this.Z.put(Long.valueOf(j2), Float.valueOf(f));
        MediaEngineConnection mediaEngineConnection = this.f2750x;
        if (mediaEngineConnection != null) {
            mediaEngineConnection.e(j2, f);
        }
    }

    public final void w(Long l) {
        MediaSinkWantsManager mediaSinkWantsManager = this.H;
        if (mediaSinkWantsManager != null) {
            mediaSinkWantsManager.b(new j(mediaSinkWantsManager, l));
        }
    }
}
