package com.discord.rtcconnection.enums;

import andhook.lib.HookHelper;
import android.content.Intent;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: ScoAudioState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\u0001\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0002\n\u000bB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000f¨\u0006\u0010"}, d2 = {"Lcom/discord/rtcconnection/enums/ScoAudioState;", "", "", "value", "I", "getValue", "()I", HookHelper.constructorName, "(Ljava/lang/String;II)V", "Companion", "a", "b", "Disconnected", "Connected", "Connecting", "Error", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public enum ScoAudioState {
    Disconnected(0),
    Connected(1),
    Connecting(2),
    Error(-1);
    
    public static final a Companion = new a(null);
    private final int value;

    /* compiled from: ScoAudioState.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final ScoAudioState a(int i) {
            ScoAudioState[] values = ScoAudioState.values();
            for (int i2 = 0; i2 < 4; i2++) {
                ScoAudioState scoAudioState = values[i2];
                if (scoAudioState.getValue() == i) {
                    return scoAudioState;
                }
            }
            return null;
        }

        public final b b(Intent intent) {
            m.checkNotNullParameter(intent, "intent");
            if (m.areEqual(intent.getAction(), "android.media.ACTION_SCO_AUDIO_STATE_UPDATED")) {
                ScoAudioState a = a(intent.getIntExtra("android.media.extra.SCO_AUDIO_STATE", 0));
                m.checkNotNull(a);
                ScoAudioState a2 = a(intent.getIntExtra("android.media.extra.SCO_AUDIO_PREVIOUS_STATE", 0));
                m.checkNotNull(a2);
                return new b(a, a2);
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }

    /* compiled from: ScoAudioState.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public final ScoAudioState a;

        /* renamed from: b  reason: collision with root package name */
        public final ScoAudioState f2764b;

        public b(ScoAudioState scoAudioState, ScoAudioState scoAudioState2) {
            m.checkNotNullParameter(scoAudioState, "current");
            m.checkNotNullParameter(scoAudioState2, "previous");
            this.a = scoAudioState;
            this.f2764b = scoAudioState2;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return m.areEqual(this.a, bVar.a) && m.areEqual(this.f2764b, bVar.f2764b);
        }

        public int hashCode() {
            ScoAudioState scoAudioState = this.a;
            int i = 0;
            int hashCode = (scoAudioState != null ? scoAudioState.hashCode() : 0) * 31;
            ScoAudioState scoAudioState2 = this.f2764b;
            if (scoAudioState2 != null) {
                i = scoAudioState2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            if (this.f2764b == ScoAudioState.Error) {
                return String.valueOf(this.a);
            }
            StringBuilder R = b.d.b.a.a.R("Update(");
            R.append(this.f2764b);
            R.append(" -> ");
            R.append(this.a);
            R.append(')');
            return R.toString();
        }
    }

    ScoAudioState(int i) {
        this.value = i;
    }

    public final int getValue() {
        return this.value;
    }
}
