package com.discord.rtcconnection.enums;

import andhook.lib.HookHelper;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import b.c.a.a0.d;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: BluetoothHeadsetAudioState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\r\b\u0086\u0001\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0002\n\u000bB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\fj\u0002\b\rj\u0002\b\u000e¨\u0006\u000f"}, d2 = {"Lcom/discord/rtcconnection/enums/BluetoothHeadsetAudioState;", "", "", "value", "I", "getValue", "()I", HookHelper.constructorName, "(Ljava/lang/String;II)V", "Companion", "a", "b", "Disconnected", "Connecting", "Connected", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public enum BluetoothHeadsetAudioState {
    Disconnected(10),
    Connecting(11),
    Connected(12);
    
    public static final a Companion = new a(null);
    private final int value;

    /* compiled from: BluetoothHeadsetAudioState.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final BluetoothHeadsetAudioState a(int i) {
            BluetoothHeadsetAudioState[] values = BluetoothHeadsetAudioState.values();
            for (int i2 = 0; i2 < 3; i2++) {
                BluetoothHeadsetAudioState bluetoothHeadsetAudioState = values[i2];
                if (bluetoothHeadsetAudioState.getValue() == i) {
                    return bluetoothHeadsetAudioState;
                }
            }
            return null;
        }

        public final b b(Intent intent) {
            m.checkNotNullParameter(intent, "intent");
            if (m.areEqual(intent.getAction(), "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED")) {
                m.checkNotNullParameter(intent, "$this$getBluetoothDeviceExtra");
                BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                Integer x0 = d.x0(intent, "android.bluetooth.profile.extra.PREVIOUS_STATE");
                BluetoothHeadsetAudioState bluetoothHeadsetAudioState = null;
                BluetoothHeadsetAudioState a = x0 != null ? BluetoothHeadsetAudioState.Companion.a(x0.intValue()) : null;
                Integer x02 = d.x0(intent, "android.bluetooth.profile.extra.STATE");
                if (x02 != null) {
                    bluetoothHeadsetAudioState = BluetoothHeadsetAudioState.Companion.a(x02.intValue());
                }
                return new b(bluetoothHeadsetAudioState, a, bluetoothDevice);
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }

    /* compiled from: BluetoothHeadsetAudioState.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public final BluetoothHeadsetAudioState a;

        /* renamed from: b  reason: collision with root package name */
        public final BluetoothHeadsetAudioState f2762b;
        public final BluetoothDevice c;

        public b(BluetoothHeadsetAudioState bluetoothHeadsetAudioState, BluetoothHeadsetAudioState bluetoothHeadsetAudioState2, BluetoothDevice bluetoothDevice) {
            this.a = bluetoothHeadsetAudioState;
            this.f2762b = bluetoothHeadsetAudioState2;
            this.c = bluetoothDevice;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return m.areEqual(this.a, bVar.a) && m.areEqual(this.f2762b, bVar.f2762b) && m.areEqual(this.c, bVar.c);
        }

        public int hashCode() {
            BluetoothHeadsetAudioState bluetoothHeadsetAudioState = this.a;
            int i = 0;
            int hashCode = (bluetoothHeadsetAudioState != null ? bluetoothHeadsetAudioState.hashCode() : 0) * 31;
            BluetoothHeadsetAudioState bluetoothHeadsetAudioState2 = this.f2762b;
            int hashCode2 = (hashCode + (bluetoothHeadsetAudioState2 != null ? bluetoothHeadsetAudioState2.hashCode() : 0)) * 31;
            BluetoothDevice bluetoothDevice = this.c;
            if (bluetoothDevice != null) {
                i = bluetoothDevice.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Update(current=");
            R.append(this.a);
            R.append(", previous=");
            R.append(this.f2762b);
            R.append(", device=");
            R.append(this.c);
            R.append(")");
            return R.toString();
        }
    }

    BluetoothHeadsetAudioState(int i) {
        this.value = i;
    }

    public final int getValue() {
        return this.value;
    }
}
