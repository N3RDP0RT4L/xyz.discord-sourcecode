package com.discord.rtcconnection.enums;

import andhook.lib.HookHelper;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import b.c.a.a0.d;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: BluetoothProfileConnectionState.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0010\b\n\u0002\b\u000e\b\u0086\u0001\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0002\n\u000bB\u0011\b\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\u0003\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000f¨\u0006\u0010"}, d2 = {"Lcom/discord/rtcconnection/enums/BluetoothProfileConnectionState;", "", "", "value", "I", "getValue", "()I", HookHelper.constructorName, "(Ljava/lang/String;II)V", "Companion", "a", "b", "Disconnected", "Connecting", "Connected", "Disconnecting", "rtcconnection_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public enum BluetoothProfileConnectionState {
    Disconnected(0),
    Connecting(1),
    Connected(2),
    Disconnecting(3);
    
    public static final a Companion = new a(null);
    private final int value;

    /* compiled from: BluetoothProfileConnectionState.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final BluetoothProfileConnectionState a(int i) {
            BluetoothProfileConnectionState[] values = BluetoothProfileConnectionState.values();
            for (int i2 = 0; i2 < 4; i2++) {
                BluetoothProfileConnectionState bluetoothProfileConnectionState = values[i2];
                if (bluetoothProfileConnectionState.getValue() == i) {
                    return bluetoothProfileConnectionState;
                }
            }
            return null;
        }

        public final b b(Intent intent) {
            m.checkNotNullParameter(intent, "intent");
            if (m.areEqual(intent.getAction(), "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED")) {
                Integer x0 = d.x0(intent, "android.bluetooth.profile.extra.PREVIOUS_STATE");
                BluetoothProfileConnectionState bluetoothProfileConnectionState = null;
                BluetoothProfileConnectionState a = x0 != null ? BluetoothProfileConnectionState.Companion.a(x0.intValue()) : null;
                Integer x02 = d.x0(intent, "android.bluetooth.profile.extra.STATE");
                if (x02 != null) {
                    bluetoothProfileConnectionState = BluetoothProfileConnectionState.Companion.a(x02.intValue());
                }
                return new b(a, bluetoothProfileConnectionState, (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE"));
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }

    /* compiled from: BluetoothProfileConnectionState.kt */
    /* loaded from: classes.dex */
    public static final class b {
        public final BluetoothProfileConnectionState a;

        /* renamed from: b  reason: collision with root package name */
        public final BluetoothProfileConnectionState f2763b;
        public final BluetoothDevice c;

        public b(BluetoothProfileConnectionState bluetoothProfileConnectionState, BluetoothProfileConnectionState bluetoothProfileConnectionState2, BluetoothDevice bluetoothDevice) {
            this.a = bluetoothProfileConnectionState;
            this.f2763b = bluetoothProfileConnectionState2;
            this.c = bluetoothDevice;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return m.areEqual(this.a, bVar.a) && m.areEqual(this.f2763b, bVar.f2763b) && m.areEqual(this.c, bVar.c);
        }

        public int hashCode() {
            BluetoothProfileConnectionState bluetoothProfileConnectionState = this.a;
            int i = 0;
            int hashCode = (bluetoothProfileConnectionState != null ? bluetoothProfileConnectionState.hashCode() : 0) * 31;
            BluetoothProfileConnectionState bluetoothProfileConnectionState2 = this.f2763b;
            int hashCode2 = (hashCode + (bluetoothProfileConnectionState2 != null ? bluetoothProfileConnectionState2.hashCode() : 0)) * 31;
            BluetoothDevice bluetoothDevice = this.c;
            if (bluetoothDevice != null) {
                i = bluetoothDevice.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Update(previous=");
            R.append(this.a);
            R.append(", current=");
            R.append(this.f2763b);
            R.append(", device=");
            R.append(this.c);
            R.append(")");
            return R.toString();
        }
    }

    BluetoothProfileConnectionState(int i) {
        this.value = i;
    }

    public final int getValue() {
        return this.value;
    }
}
