package com.discord.app;

import andhook.lib.HookHelper;
import android.app.Application;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import b.a.d.i0;
import b.a.d.k0;
import b.a.d.l;
import com.discord.stores.StoreConnectionOpen;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AnalyticsUtils;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.error.Error;
import com.discord.utilities.fcm.NotificationClient;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.intent.RouteHandlers;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.voice.DiscordOverlayService;
import d0.g0.s;
import d0.g0.t;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.l.e.k;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.text.MatchResult;
import kotlin.text.Regex;
import rx.Observable;
import xyz.discord.R;
/* compiled from: DiscordConnectService.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u00112\u00020\u0001:\u0001\u0012B\u0007¢\u0006\u0004\b\u0010\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J)\u0010\n\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\n\u0010\u000bJ\u001b\u0010\r\u001a\u0004\u0018\u00010\f2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u000f\u0010\u000f\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u000f\u0010\u0004¨\u0006\u0013"}, d2 = {"Lcom/discord/app/DiscordConnectService;", "Landroid/app/Service;", "", "onCreate", "()V", "Landroid/content/Intent;", "intent", "", "flags", "startId", "onStartCommand", "(Landroid/content/Intent;II)I", "Landroid/os/IBinder;", "onBind", "(Landroid/content/Intent;)Landroid/os/IBinder;", "onDestroy", HookHelper.constructorName, "j", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class DiscordConnectService extends Service {
    public static final a j = new a(null);

    /* compiled from: DiscordConnectService.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public static final void a(a aVar, String str) {
            AppLog appLog = AppLog.g;
            String simpleName = DiscordConnectService.class.getSimpleName();
            m.checkNotNullExpressionValue(simpleName, "DiscordConnectService::class.java.simpleName");
            Logger.i$default(appLog, simpleName, str, null, 4, null);
        }

        public final void b(Context context, long j) {
            m.checkNotNullParameter(context, "context");
            Intent intent = IntentUtils.RouteBuilders.INSTANCE.connectVoice(j).setPackage(context.getPackageName());
            m.checkNotNullExpressionValue(intent, "IntentUtils.RouteBuilder…kage(context.packageName)");
            if (Build.VERSION.SDK_INT >= 26) {
                context.startForegroundService(intent);
            } else {
                context.startService(intent);
            }
        }
    }

    /* compiled from: DiscordConnectService.kt */
    /* loaded from: classes.dex */
    public static final class b extends o implements Function1<Object, Unit> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Object obj) {
            return Unit.a;
        }
    }

    /* compiled from: DiscordConnectService.kt */
    /* loaded from: classes.dex */
    public static final class c extends o implements Function0<Unit> {
        public final /* synthetic */ int $startId;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(int i) {
            super(0);
            this.$startId = i;
        }

        @Override // kotlin.jvm.functions.Function0
        public Unit invoke() {
            a aVar = DiscordConnectService.j;
            StringBuilder R = b.d.b.a.a.R("Success[");
            R.append(this.$startId);
            R.append(']');
            a.a(aVar, R.toString());
            DiscordConnectService discordConnectService = DiscordConnectService.this;
            int i = this.$startId;
            discordConnectService.stopForeground(true);
            discordConnectService.stopSelf(i);
            return Unit.a;
        }
    }

    /* compiled from: DiscordConnectService.kt */
    /* loaded from: classes.dex */
    public static final class d extends o implements Function1<Error, Unit> {
        public final /* synthetic */ int $startId;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public d(int i) {
            super(1);
            this.$startId = i;
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Error error) {
            Error error2 = error;
            m.checkNotNullParameter(error2, "it");
            error2.setShouldLog(false);
            a aVar = DiscordConnectService.j;
            StringBuilder R = b.d.b.a.a.R("Request timeout[");
            R.append(this.$startId);
            R.append("]: ");
            R.append(error2);
            a.a(aVar, R.toString());
            DiscordConnectService discordConnectService = DiscordConnectService.this;
            int i = this.$startId;
            discordConnectService.stopForeground(true);
            discordConnectService.stopSelf(i);
            return Unit.a;
        }
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        throw new IllegalStateException("All my bases are belong to me!");
    }

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        AppLog appLog = AppLog.g;
        String simpleName = DiscordConnectService.class.getSimpleName();
        m.checkNotNullExpressionValue(simpleName, "DiscordConnectService::class.java.simpleName");
        Logger.i$default(appLog, simpleName, "onCreate", null, 4, null);
        l.c.a(this);
        Notification build = new NotificationCompat.Builder(this, NotificationClient.NOTIF_CHANNEL_SOCIAL).setAutoCancel(true).setOnlyAlertOnce(true).setLocalOnly(true).setSmallIcon(R.drawable.ic_notification_24dp).setColor(ColorCompat.getThemedColor(this, (int) R.attr.color_brand_500)).setContentTitle(getString(R.string.connecting)).setContentText(getString(R.string.connection_status_awaiting_endpoint)).build();
        m.checkNotNullExpressionValue(build, "NotificationCompat.Build…dpoint))\n        .build()");
        startForeground(100, build);
        StoreStream.Companion companion = StoreStream.Companion;
        Application application = getApplication();
        m.checkNotNullExpressionValue(application, "application");
        companion.initialize(application);
        AnalyticsUtils analyticsUtils = AnalyticsUtils.INSTANCE;
        Application application2 = getApplication();
        m.checkNotNullExpressionValue(application2, "application");
        analyticsUtils.initAppOpen(application2);
    }

    @Override // android.app.Service
    public void onDestroy() {
        AppLog appLog = AppLog.g;
        String simpleName = DiscordConnectService.class.getSimpleName();
        m.checkNotNullExpressionValue(simpleName, "DiscordConnectService::class.java.simpleName");
        Logger.i$default(appLog, simpleName, "onDestroy", null, 4, null);
        l.c.b(this);
        super.onDestroy();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        Observable observable;
        Observable observable2;
        List<String> groupValues;
        String str;
        String p = b.d.b.a.a.p("onStartCommand: ", i2);
        AppLog appLog = AppLog.g;
        String simpleName = DiscordConnectService.class.getSimpleName();
        m.checkNotNullExpressionValue(simpleName, "DiscordConnectService::class.java.simpleName");
        Logger.i$default(appLog, simpleName, p, null, 4, null);
        Uri data = intent != null ? intent.getData() : null;
        if (data == null || !IntentUtils.INSTANCE.isDiscordAppUri(data)) {
            String simpleName2 = DiscordConnectService.class.getSimpleName();
            m.checkNotNullExpressionValue(simpleName2, "DiscordConnectService::class.java.simpleName");
            Logger.i$default(appLog, simpleName2, "Invalid request " + data, null, 4, null);
            stopForeground(true);
            stopSelf(i2);
            return 2;
        }
        StoreStream.Companion companion = StoreStream.Companion;
        String authToken$app_productionGoogleRelease = companion.getAuthentication().getAuthToken$app_productionGoogleRelease();
        if (authToken$app_productionGoogleRelease == null || t.isBlank(authToken$app_productionGoogleRelease)) {
            b.a.d.m.g(this, R.string.overlay_mobile_unauthed, 0, null, 12);
            observable2 = Observable.w(new IllegalStateException("UNAUTHED"));
            m.checkNotNullExpressionValue(observable2, "Observable.error(Illegal…ateException(\"UNAUTHED\"))");
        } else {
            b.a.d.l0.a aVar = b.a.d.l0.a.F;
            Regex regex = b.a.d.l0.a.f61s;
            String path = data.getPath();
            if (path == null) {
                path = "";
            }
            MatchResult matchEntire = regex.matchEntire(path);
            Long longOrNull = (matchEntire == null || (groupValues = matchEntire.getGroupValues()) == null || (str = (String) u.getOrNull(groupValues, 1)) == null) ? null : s.toLongOrNull(str);
            if (matchEntire != null) {
                companion.getAnalytics().deepLinkReceived(intent != null ? intent : new Intent(), new RouteHandlers.AnalyticsMetadata("connect", null, longOrNull, 2, null));
            }
            if (longOrNull != null) {
                if (ContextCompat.checkSelfPermission(this, "android.permission.RECORD_AUDIO") != 0) {
                    b.a.d.m.g(this, R.string.permission_microphone_denied, 0, null, 12);
                    observable2 = Observable.w(new IllegalStateException("Do not have microphone permissions, go to main app"));
                    m.checkNotNullExpressionValue(observable2, "Observable.error(\n      …to main app\")\n          )");
                } else {
                    long longValue = longOrNull.longValue();
                    String simpleName3 = DiscordConnectService.class.getSimpleName();
                    m.checkNotNullExpressionValue(simpleName3, "DiscordConnectService::class.java.simpleName");
                    Logger.i$default(appLog, simpleName3, "Try joining voice channel", null, 4, null);
                    companion.getVoiceChannelSelected().selectVoiceChannel(longValue);
                    Observable x2 = StoreConnectionOpen.observeConnectionOpen$default(companion.getConnectionOpen(), false, 1, null).x(i0.j);
                    m.checkNotNullExpressionValue(x2, "StoreStream\n        .get…()\n        .filter { it }");
                    observable2 = ObservableExtensionsKt.takeSingleUntilTimeout$default(x2, 10000L, false, 2, null).Y(k0.j);
                    m.checkNotNullExpressionValue(observable2, "isConnectedObs.switchMap…nnected\n          }\n    }");
                }
            } else if (matchEntire != null) {
                DiscordOverlayService.Companion.launchForConnect(this);
                observable = new k(Unit.a);
                m.checkNotNullExpressionValue(observable, "Observable.just(Unit)");
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout$default(observable, 10000L, false, 2, null), DiscordConnectService.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new d(i2), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : new c(i2), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, b.j);
                return 2;
            } else {
                observable2 = Observable.w(new IllegalArgumentException("Invalid Request: " + data));
                m.checkNotNullExpressionValue(observable2, "Observable.error(Illegal…\"Invalid Request: $uri\"))");
            }
        }
        observable = observable2;
        ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.takeSingleUntilTimeout$default(observable, 10000L, false, 2, null), DiscordConnectService.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new d(i2), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : new c(i2), (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, b.j);
        return 2;
    }
}
