package com.discord.app;

import andhook.lib.HookHelper;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.display.DisplayUtils;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.view.text.TextWatcher;
import com.discord.widgets.notice.WidgetNoticeDialog;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
import xyz.discord.R;
/* compiled from: AppDialog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\b&\u0018\u00002\u00020\u00012\u00020\u0002B\t\b\u0016¢\u0006\u0004\b9\u0010\nB\u0013\b\u0016\u0012\b\b\u0001\u0010:\u001a\u00020\u001a¢\u0006\u0004\b9\u0010;J\u0019\u0010\u0006\u001a\u00020\u00052\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0016¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\t\u001a\u00020\bH\u0016¢\u0006\u0004\b\t\u0010\nJ!\u0010\r\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b2\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003H\u0016¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u000f\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000bH\u0017¢\u0006\u0004\b\u000f\u0010\u0010J\u000f\u0010\u0011\u001a\u00020\bH\u0017¢\u0006\u0004\b\u0011\u0010\nJ!\u0010\u0016\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u00122\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J!\u0010\u0016\u001a\u00020\u001a2\u0006\u0010\u0019\u001a\u00020\u00182\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014H\u0016¢\u0006\u0004\b\u0016\u0010\u001bJ\u000f\u0010\u001c\u001a\u00020\bH\u0016¢\u0006\u0004\b\u001c\u0010\nJ\u000f\u0010\u001d\u001a\u00020\bH\u0016¢\u0006\u0004\b\u001d\u0010\nJ\u000f\u0010\u001e\u001a\u00020\bH\u0016¢\u0006\u0004\b\u001e\u0010\nJ\u000f\u0010\u001f\u001a\u00020\bH\u0016¢\u0006\u0004\b\u001f\u0010\nJ\u0015\u0010 \u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u000b¢\u0006\u0004\b \u0010\u0010J\u001b\u0010!\u001a\u00020\b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u000bH\u0007¢\u0006\u0004\b!\u0010\u0010J'\u0010$\u001a\u00020\b*\u00020\u000b2\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\b0\"H\u0004¢\u0006\u0004\b$\u0010%R\u0016\u0010(\u001a\u00020\u00038D@\u0004X\u0084\u0004¢\u0006\u0006\u001a\u0004\b&\u0010'R(\u0010+\u001a\u000e\u0012\u0004\u0012\u00020*\u0012\u0004\u0012\u00020*0)8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b+\u0010,\u001a\u0004\b-\u0010.R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b0\u00101R\u0015\u00105\u001a\u0004\u0018\u0001028F@\u0006¢\u0006\u0006\u001a\u0004\b3\u00104R$\u00107\u001a\u00020/2\u0006\u00106\u001a\u00020/8\u0004@BX\u0084\u000e¢\u0006\f\n\u0004\b7\u00101\u001a\u0004\b7\u00108¨\u0006<"}, d2 = {"Lcom/discord/app/AppDialog;", "Landroidx/fragment/app/DialogFragment;", "Lcom/discord/app/AppComponent;", "Landroid/os/Bundle;", "savedInstanceState", "Landroid/app/Dialog;", "onCreateDialog", "(Landroid/os/Bundle;)Landroid/app/Dialog;", "", "onStart", "()V", "Landroid/view/View;", "view", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onViewBound", "(Landroid/view/View;)V", "onViewBoundOrOnResume", "Landroidx/fragment/app/FragmentManager;", "manager", "", "tag", "show", "(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V", "Landroidx/fragment/app/FragmentTransaction;", "transaction", "", "(Landroidx/fragment/app/FragmentTransaction;Ljava/lang/String;)I", "dismiss", "onPause", "onResume", "onDestroyView", "showKeyboard", "hideKeyboard", "Lkotlin/Function1;", "onClickListener", "setOnClickAndDismissListener", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "getArgumentsOrDefault", "()Landroid/os/Bundle;", "argumentsOrDefault", "Lrx/subjects/Subject;", "Ljava/lang/Void;", "unsubscribeSignal", "Lrx/subjects/Subject;", "getUnsubscribeSignal", "()Lrx/subjects/Subject;", "", "onViewBoundOrOnResumeInvoked", "Z", "Lcom/discord/app/AppActivity;", "getAppActivity", "()Lcom/discord/app/AppActivity;", "appActivity", "<set-?>", "isRecreated", "()Z", HookHelper.constructorName, "contentResId", "(I)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public abstract class AppDialog extends DialogFragment implements AppComponent {
    private boolean isRecreated;
    private boolean onViewBoundOrOnResumeInvoked;
    private final Subject<Void, Void> unsubscribeSignal;

    /* compiled from: AppDialog.kt */
    /* loaded from: classes.dex */
    public static final class a implements View.OnClickListener {
        public final /* synthetic */ View k;
        public final /* synthetic */ Function1 l;

        public a(View view, Function1 function1) {
            this.k = view;
            this.l = function1;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            Unit unit = (Unit) this.l.invoke(this.k);
            AppDialog.this.dismiss();
        }
    }

    public AppDialog() {
        PublishSubject k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.unsubscribeSignal = k0;
    }

    public static /* synthetic */ void hideKeyboard$default(AppDialog appDialog, View view, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                view = null;
            }
            appDialog.hideKeyboard(view);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: hideKeyboard");
    }

    @Override // androidx.fragment.app.DialogFragment
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception unused) {
        }
    }

    public final AppActivity getAppActivity() {
        return (AppActivity) e();
    }

    public final Bundle getArgumentsOrDefault() {
        Bundle arguments = getArguments();
        if (arguments == null) {
            arguments = new Bundle();
        }
        m.checkNotNullExpressionValue(arguments, "arguments ?: Bundle()");
        return arguments;
    }

    @Override // com.discord.app.AppComponent
    public Subject<Void, Void> getUnsubscribeSignal() {
        return this.unsubscribeSignal;
    }

    public final void hideKeyboard() {
        hideKeyboard$default(this, null, 1, null);
    }

    public final void hideKeyboard(View view) {
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            appActivity.hideKeyboard(view);
        }
    }

    public final boolean isRecreated() {
        return this.isRecreated;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        Window window;
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        m.checkNotNullExpressionValue(onCreateDialog, "super.onCreateDialog(savedInstanceState)");
        onCreateDialog.requestWindowFeature(1);
        if (AccessibilityUtils.INSTANCE.isReducedMotionEnabled() && (window = onCreateDialog.getWindow()) != null) {
            window.setWindowAnimations(R.style.FadeInOut);
        }
        return onCreateDialog;
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        TextWatcher.Companion.reset(this);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        getUnsubscribeSignal().onNext(null);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (this.onViewBoundOrOnResumeInvoked) {
            this.onViewBoundOrOnResumeInvoked = false;
        } else {
            onViewBoundOrOnResume();
        }
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onStart() {
        Window window;
        try {
            super.onStart();
        } catch (Exception e) {
            if (this instanceof WidgetNoticeDialog) {
                ((WidgetNoticeDialog) this).logOnStartError(e);
            } else {
                AppLog appLog = AppLog.g;
                StringBuilder R = b.d.b.a.a.R("Failed to start AppDialog: ");
                R.append(getClass().getName());
                Logger.e$default(appLog, R.toString(), e, null, 4, null);
            }
        }
        Dialog dialog = getDialog();
        if (dialog != null && (window = dialog.getWindow()) != null) {
            window.setBackgroundDrawableResource(R.color.transparent);
        }
    }

    @CallSuper
    public void onViewBound(View view) {
        m.checkNotNullParameter(view, "view");
    }

    @CallSuper
    public void onViewBoundOrOnResume() {
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        this.isRecreated = bundle != null;
        DisplayUtils.drawUnderSystemBars(view);
        onViewBound(view);
        onViewBoundOrOnResume();
        this.onViewBoundOrOnResumeInvoked = true;
    }

    public final void setOnClickAndDismissListener(View view, Function1<? super View, Unit> function1) {
        m.checkNotNullParameter(view, "$this$setOnClickAndDismissListener");
        m.checkNotNullParameter(function1, "onClickListener");
        view.setOnClickListener(new a(view, function1));
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        m.checkNotNullParameter(fragmentManager, "manager");
        if (!isDetached()) {
            try {
                super.show(fragmentManager, str);
            } catch (Exception unused) {
            }
        } else {
            AppLog appLog = AppLog.g;
            StringBuilder R = b.d.b.a.a.R("failed to show ");
            R.append(fragmentManager.getClass().getName());
            Logger.e$default(appLog, "Could not show dialog because of detached FragmentManager", new IllegalStateException(R.toString()), null, 4, null);
        }
    }

    public final void showKeyboard(View view) {
        m.checkNotNullParameter(view, "view");
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            appActivity.showKeyboard(view);
        }
    }

    public AppDialog(@LayoutRes int i) {
        super(i);
        PublishSubject k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.unsubscribeSignal = k0;
    }

    @Override // androidx.fragment.app.DialogFragment
    public int show(FragmentTransaction fragmentTransaction, String str) {
        m.checkNotNullParameter(fragmentTransaction, "transaction");
        try {
            return super.show(fragmentTransaction, str);
        } catch (Exception unused) {
            return -1;
        }
    }
}
