package com.discord.app;

import andhook.lib.HookHelper;
import android.content.SharedPreferences;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.discord.BuildConfig;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.mg_recycler.MGRecyclerDataPayload;
import com.discord.utilities.systemlog.SystemLogReport;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import d0.g0.y;
import d0.t.k;
import d0.t.u;
import d0.z.d.m;
import d0.z.d.o;
import j0.q.a;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import rx.subjects.SerializedSubject;
/* compiled from: AppLog.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010$\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001AB\t\b\u0002¢\u0006\u0004\b?\u0010@J\u0017\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¢\u0006\u0004\b\u0005\u0010\u0006J9\u0010\f\u001a\u00020\u00042\u0010\b\u0002\u0010\t\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u00022\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0002H\u0007¢\u0006\u0004\b\f\u0010\rJ?\u0010\u0014\u001a\u00020\u0004*\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u0018\b\u0002\u0010\u0013\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0018\u00010\u0012H\u0002¢\u0006\u0004\b\u0014\u0010\u0015J!\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0016¢\u0006\u0004\b\u0016\u0010\u0017J)\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0016¢\u0006\u0004\b\u0016\u0010\u0019J!\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0016¢\u0006\u0004\b\u0005\u0010\u0017J)\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0016¢\u0006\u0004\b\u0005\u0010\u0019J!\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0016¢\u0006\u0004\b\u001a\u0010\u0017J)\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0016¢\u0006\u0004\b\u001a\u0010\u0019J!\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u0010H\u0016¢\u0006\u0004\b\u001b\u0010\u0017J7\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u0014\u0010\u0013\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u0012H\u0016¢\u0006\u0004\b\u001c\u0010\u001dJ?\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u0014\u0010\u0013\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u0012H\u0016¢\u0006\u0004\b\u001c\u0010\u001eJG\u0010\"\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020\u00022\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u001c\b\u0002\u0010!\u001a\u0016\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\u0004\u0012\u00020\u00040 ¢\u0006\u0004\b\"\u0010#J\u001d\u0010&\u001a\u00020\u00042\u0006\u0010$\u001a\u00020\u00022\u0006\u0010%\u001a\u00020\u0002¢\u0006\u0004\b&\u0010'J\u001f\u0010(\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020\u0002H\u0016¢\u0006\u0004\b(\u0010'R\"\u0010/\u001a\u00020\u000e8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b)\u0010*\u001a\u0004\b+\u0010,\"\u0004\b-\u0010.R\u0016\u00102\u001a\u0002008\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0014\u00101R2\u00107\u001a\u001e\u0012\f\u0012\n 5*\u0004\u0018\u00010404\u0012\f\u0012\n 5*\u0004\u0018\u00010404038\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u00106R\u001c\u0010:\u001a\b\u0012\u0004\u0012\u00020\u0002088\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b&\u00109R\u0018\u0010=\u001a\u0004\u0018\u00010;8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\"\u0010<R\u0016\u0010>\u001a\u0002008\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u001c\u00101¨\u0006B"}, d2 = {"Lcom/discord/app/AppLog;", "Lcom/discord/utilities/logging/Logger;", "", "message", "", "i", "(Ljava/lang/String;)V", "", "Lcom/discord/primitives/UserId;", "userId", "userLogin", "username", "g", "(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;)V", "", "priority", "", "throwable", "", "metadata", "b", "(Ljava/lang/String;ILjava/lang/Throwable;Ljava/util/Map;)V", "w", "(Ljava/lang/String;Ljava/lang/Throwable;)V", "tag", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V", "d", "v", "e", "(Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V", "category", "Lkotlin/Function2;", "loggingFn", "c", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Lkotlin/jvm/functions/Function2;)V", "from", "to", "f", "(Ljava/lang/String;Ljava/lang/String;)V", "recordBreadcrumb", "a", "I", "getMinLoggingPriority", "()I", "setMinLoggingPriority", "(I)V", "minLoggingPriority", "", "Z", "initCalled", "Lrx/subjects/SerializedSubject;", "Lcom/discord/app/AppLog$LoggedItem;", "kotlin.jvm.PlatformType", "Lrx/subjects/SerializedSubject;", "logsSubject", "", "[Ljava/lang/String;", "PACKAGE_MARKERS", "Landroid/content/SharedPreferences;", "Landroid/content/SharedPreferences;", "cache", "hasReportedTombstone", HookHelper.constructorName, "()V", "LoggedItem", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AppLog extends Logger {

    /* renamed from: b  reason: collision with root package name */
    public static boolean f2069b;
    public static SharedPreferences c;
    public static boolean e;
    public static final AppLog g = new AppLog();
    public static int a = 99;
    public static final SerializedSubject<LoggedItem, LoggedItem> d = new SerializedSubject<>(new j0.q.a(new a.d(new a.c(5000))));
    public static final String[] f = {BuildConfig.APPLICATION_ID, "co.discord"};

    /* compiled from: AppLog.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0003\n\u0002\b\u0011\b\u0086\b\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\u0006\u0010\u001c\u001a\u00020\u0002\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u0011¢\u0006\u0004\b \u0010!J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\u000b\u001a\u00020\n2\b\u0010\t\u001a\u0004\u0018\u00010\bHÖ\u0003¢\u0006\u0004\b\u000b\u0010\fR\u0019\u0010\u0010\u001a\u00020\u00058\u0006@\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0007R\u001b\u0010\u0016\u001a\u0004\u0018\u00010\u00118\u0006@\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0014\u0010\u0015R\u001c\u0010\u0017\u001a\u00020\u00058\u0016@\u0016X\u0096D¢\u0006\f\n\u0004\b\u0017\u0010\u000e\u001a\u0004\b\u0018\u0010\u0007R\u0019\u0010\u001c\u001a\u00020\u00028\u0006@\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u0004R\u001c\u0010\u001f\u001a\u00020\u00028\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001d\u0010\u001a\u001a\u0004\b\u001e\u0010\u0004¨\u0006\""}, d2 = {"Lcom/discord/app/AppLog$LoggedItem;", "Lcom/discord/utilities/mg_recycler/MGRecyclerDataPayload;", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "", "other", "", "equals", "(Ljava/lang/Object;)Z", "k", "I", "getPriority", "priority", "", "m", "Ljava/lang/Throwable;", "getThrowable", "()Ljava/lang/Throwable;", "throwable", "type", "getType", "l", "Ljava/lang/String;", "getMessage", "message", "j", "getKey", "key", HookHelper.constructorName, "(ILjava/lang/String;Ljava/lang/Throwable;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class LoggedItem implements MGRecyclerDataPayload {
        public final String j;
        public final int k;
        public final String l;
        public final Throwable m;

        public LoggedItem(int i, String str, Throwable th) {
            m.checkNotNullParameter(str, "message");
            this.k = i;
            this.l = str;
            this.m = th;
            String uuid = UUID.randomUUID().toString();
            m.checkNotNullExpressionValue(uuid, "UUID.randomUUID().toString()");
            this.j = uuid;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof LoggedItem)) {
                return false;
            }
            LoggedItem loggedItem = (LoggedItem) obj;
            return this.k == loggedItem.k && m.areEqual(this.l, loggedItem.l) && m.areEqual(this.m, loggedItem.m);
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload, com.discord.utilities.recycler.DiffKeyProvider
        public String getKey() {
            return this.j;
        }

        @Override // com.discord.utilities.mg_recycler.MGRecyclerDataPayload
        public int getType() {
            return 0;
        }

        public int hashCode() {
            int i = this.k * 31;
            String str = this.l;
            int i2 = 0;
            int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
            Throwable th = this.m;
            if (th != null) {
                i2 = th.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("LoggedItem(priority=");
            R.append(this.k);
            R.append(", message=");
            R.append(this.l);
            R.append(", throwable=");
            R.append(this.m);
            R.append(")");
            return R.toString();
        }
    }

    /* compiled from: AppLog.kt */
    /* loaded from: classes.dex */
    public static final class a extends o implements Function1<String, Unit> {
        public final /* synthetic */ Map $metadata;
        public final /* synthetic */ int $priority;
        public final /* synthetic */ Throwable $throwable;

        /* compiled from: AppLog.kt */
        /* renamed from: com.discord.app.AppLog$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static final class C0190a extends o implements Function2<String, Integer, Unit> {
            public C0190a() {
                super(2);
            }

            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(String str, Integer num) {
                invoke(str, num.intValue());
                return Unit.a;
            }

            public final void invoke(String str, int i) {
                m.checkNotNullParameter(str, "message");
                for (String str2 : y.chunked(str, i)) {
                    Log.println(a.this.$priority, AppLog.g.getDefaultTag(), str2);
                }
            }
        }

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(int i, Map map, Throwable th) {
            super(1);
            this.$priority = i;
            this.$metadata = map;
            this.$throwable = th;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(String str) {
            invoke2(str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(String str) {
            Set entrySet;
            String joinToString$default;
            m.checkNotNullParameter(str, "message");
            C0190a aVar = new C0190a();
            aVar.invoke(str, 1000);
            Map map = this.$metadata;
            if (!(map == null || (entrySet = map.entrySet()) == null || (joinToString$default = u.joinToString$default(entrySet, "\n\t", null, null, 0, null, null, 62, null)) == null)) {
                aVar.invoke("Metadata: " + joinToString$default, Integer.MAX_VALUE);
            }
            String stackTraceString = Log.getStackTraceString(this.$throwable);
            m.checkNotNullExpressionValue(stackTraceString, "Log.getStackTraceString(throwable)");
            aVar.invoke(stackTraceString, 1000);
        }
    }

    /* compiled from: AppLog.kt */
    /* loaded from: classes.dex */
    public static final class b extends o implements Function1<String, Unit> {
        public final /* synthetic */ Map $metadata;
        public final /* synthetic */ Throwable $throwable;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(Throwable th, Map map) {
            super(1);
            this.$throwable = th;
            this.$metadata = map;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(String str) {
            invoke2(str);
            return Unit.a;
        }

        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2(String str) {
            Set<Map.Entry> entrySet;
            m.checkNotNullParameter(str, "message");
            try {
                if (this.$throwable != null) {
                    AppLog appLog = AppLog.g;
                    appLog.recordBreadcrumb("Message " + str, "Error");
                }
                Map map = this.$metadata;
                if (!(map == null || (entrySet = map.entrySet()) == null)) {
                    for (Map.Entry entry : entrySet) {
                        AppLog appLog2 = AppLog.g;
                        appLog2.recordBreadcrumb("Metadata: " + ((String) entry.getKey()) + ", " + ((String) entry.getValue()), "Error");
                    }
                }
                Exception exc = new Exception(str, this.$throwable);
                AppLog appLog3 = AppLog.g;
                StackTraceElement[] stackTrace = exc.getStackTrace();
                m.checkNotNullExpressionValue(stackTrace, "e.stackTrace");
                exc.setStackTrace(AppLog.a(appLog3, stackTrace, str));
                Throwable cause = exc.getCause();
                if (cause != null) {
                    StackTraceElement[] stackTrace2 = cause.getStackTrace();
                    m.checkNotNullExpressionValue(stackTrace2, "cause.stackTrace");
                    cause.setStackTrace(AppLog.a(appLog3, stackTrace2, str));
                }
                FirebaseCrashlytics.getInstance().recordException(exc);
            } catch (Exception e) {
                AppLog.g.w("Unable to notify error logging.", e);
            }
            if (this.$throwable != null) {
                try {
                    AnalyticsTracker analyticsTracker = AnalyticsTracker.INSTANCE;
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.$throwable.getClass().toString());
                    sb.append(":\n");
                    StackTraceElement[] stackTrace3 = this.$throwable.getStackTrace();
                    m.checkNotNullExpressionValue(stackTrace3, "throwable.stackTrace");
                    sb.append(k.joinToString$default(stackTrace3, "\n", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 62, (Object) null));
                    analyticsTracker.appExceptionThrown(sb.toString());
                } catch (Exception e2) {
                    AppLog.g.w("Unable to report to analytics.", e2);
                }
            }
        }
    }

    /* compiled from: AppLog.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class c extends d0.z.d.k implements Function2<String, Throwable, Unit> {
        public c(AppLog appLog) {
            super(2, appLog, AppLog.class, "v", "v(Ljava/lang/String;Ljava/lang/Throwable;)V", 0);
        }

        @Override // kotlin.jvm.functions.Function2
        public Unit invoke(String str, Throwable th) {
            String str2 = str;
            m.checkNotNullParameter(str2, "p1");
            ((AppLog) this.receiver).v(str2, th);
            return Unit.a;
        }
    }

    public AppLog() {
        super("Discord");
    }

    /* JADX WARN: Removed duplicated region for block: B:24:0x0085 A[LOOP:0: B:3:0x0006->B:24:0x0085, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:52:0x008a A[EDGE_INSN: B:52:0x008a->B:26:0x008a ?: BREAK  , SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final java.lang.StackTraceElement[] a(com.discord.app.AppLog r12, java.lang.StackTraceElement[] r13, java.lang.String r14) {
        /*
            Method dump skipped, instructions count: 227
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.app.AppLog.a(com.discord.app.AppLog, java.lang.StackTraceElement[], java.lang.String):java.lang.StackTraceElement[]");
    }

    public static final void g(Long l, String str, String str2) {
        String str3;
        if (f2069b) {
            SharedPreferences sharedPreferences = c;
            if (sharedPreferences != null) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                m.checkNotNullExpressionValue(edit, "editor");
                if (l != null) {
                    l.longValue();
                    edit.putString("LOG_CACHE_KEY_USER_ID", String.valueOf(l));
                }
                if (str != null) {
                    edit.putString("LOG_CACHE_KEY_USER_LOGIN", str);
                }
                if (str2 != null) {
                    edit.putString("LOG_CACHE_KEY_USER_NAME", str2);
                }
                edit.apply();
            }
            FirebaseCrashlytics firebaseCrashlytics = FirebaseCrashlytics.getInstance();
            if (l == null || (str3 = String.valueOf(l.longValue())) == null) {
                str3 = "";
            }
            firebaseCrashlytics.setUserId(str3);
            if (str != null) {
                FirebaseCrashlytics.getInstance().setCustomKey("userLogin", str);
            }
            if (str2 != null) {
                FirebaseCrashlytics.getInstance().setCustomKey("userName", str2);
            }
            if (l != null && !e) {
                e = true;
                SystemLogReport.INSTANCE.reportLastCrash();
            }
        }
    }

    public final void b(String str, int i, Throwable th, Map<String, String> map) {
        if (i >= a) {
            a aVar = new a(i, map, th);
            d.k.onNext(new LoggedItem(i, str, th));
            b bVar = new b(th, map);
            if (i == 6) {
                bVar.invoke2(str);
            }
            aVar.invoke2(str);
        }
    }

    public final void c(String str, String str2, Throwable th, Function2<? super String, ? super Throwable, Unit> function2) {
        m.checkNotNullParameter(str, "message");
        m.checkNotNullParameter(str2, "category");
        m.checkNotNullParameter(function2, "loggingFn");
        if (f2069b) {
            String str3 = '[' + str2 + "]: " + str;
            function2.invoke("Breadcrumb, " + str3, th);
            FirebaseCrashlytics.getInstance().log(str3);
        }
    }

    @Override // com.discord.utilities.logging.Logger
    public void d(String str, String str2, Throwable th) {
        m.checkNotNullParameter(str, "tag");
        m.checkNotNullParameter(str2, "message");
        d(str + " -> " + str2, th);
    }

    @Override // com.discord.utilities.logging.Logger
    public void e(String str, Throwable th, Map<String, String> map) {
        m.checkNotNullParameter(str, "message");
        b(str, 6, th, map);
    }

    public final void f(String str, String str2) {
        m.checkNotNullParameter(str, "from");
        m.checkNotNullParameter(str2, "to");
        recordBreadcrumb("Navigation [" + str + "] > [" + str2 + ']', NotificationCompat.CATEGORY_NAVIGATION);
    }

    @Override // com.discord.utilities.logging.Logger
    public void i(String str, String str2, Throwable th) {
        m.checkNotNullParameter(str, "tag");
        m.checkNotNullParameter(str2, "message");
        i(str + " -> " + str2, th);
    }

    @Override // com.discord.utilities.logging.Logger
    public void recordBreadcrumb(String str, String str2) {
        m.checkNotNullParameter(str, "message");
        m.checkNotNullParameter(str2, "category");
        c(str, str2, null, new c(this));
    }

    @Override // com.discord.utilities.logging.Logger
    public void v(String str, Throwable th) {
        m.checkNotNullParameter(str, "message");
        b(str, 2, th, null);
    }

    @Override // com.discord.utilities.logging.Logger
    public void w(String str, String str2, Throwable th) {
        m.checkNotNullParameter(str, "tag");
        m.checkNotNullParameter(str2, "message");
        w(str + " -> " + str2, th);
    }

    public static final void i(String str) {
        m.checkNotNullParameter(str, "message");
        g.i(str, null);
    }

    @Override // com.discord.utilities.logging.Logger
    public void d(String str, Throwable th) {
        m.checkNotNullParameter(str, "message");
        b(str, 3, th, null);
    }

    @Override // com.discord.utilities.logging.Logger
    public void e(String str, String str2, Throwable th, Map<String, String> map) {
        m.checkNotNullParameter(str, "tag");
        m.checkNotNullParameter(str2, "message");
        e(str + " -> " + str2, th, map);
    }

    @Override // com.discord.utilities.logging.Logger
    public void w(String str, Throwable th) {
        m.checkNotNullParameter(str, "message");
        b(str, 5, th, null);
    }

    @Override // com.discord.utilities.logging.Logger
    public void i(String str, Throwable th) {
        m.checkNotNullParameter(str, "message");
        b(str, 4, th, null);
    }
}
