package com.discord.app;

import andhook.lib.HookHelper;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.PreferenceManager;
import com.discord.BuildConfig;
import com.discord.models.domain.emoji.ModelEmojiCustom;
import com.discord.models.experiments.domain.Experiment;
import com.discord.stores.StoreStream;
import com.discord.utilities.analytics.AdjustConfig;
import com.discord.utilities.analytics.AnalyticsDeviceResourceUsageMonitor;
import com.discord.utilities.analytics.AnalyticsTracker;
import com.discord.utilities.buildutils.BuildUtils;
import com.discord.utilities.cache.SharedPreferencesProvider;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.debug.DebugPrintableCollection;
import com.discord.utilities.error.Error;
import com.discord.utilities.fcm.NotificationClient;
import com.discord.utilities.images.MGImagesConfig;
import com.discord.utilities.lifecycle.ActivityProvider;
import com.discord.utilities.lifecycle.ApplicationProvider;
import com.discord.utilities.logging.Logger;
import com.discord.utilities.logging.LoggingProvider;
import com.discord.utilities.persister.PersisterConfig;
import com.discord.utilities.rest.RestAPI;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$1;
import com.discord.utilities.rx.ObservableExtensionsKt$filterNull$2;
import com.discord.utilities.surveys.SurveyUtils;
import com.discord.utilities.systemlog.SystemLogUtils;
import com.discord.utilities.time.ClockFactory;
import com.discord.utilities.uri.UriHandler;
import com.discord.utilities.view.text.LinkifiedTextView;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import d0.g0.w;
import d0.z.d.k;
import d0.z.d.m;
import d0.z.d.o;
import java.util.Map;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import xyz.discord.R;
/* compiled from: App.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\b\u0016\u0018\u0000 \u00102\u00020\u0001:\u0001\u0011B\u0007¢\u0006\u0004\b\u000f\u0010\u0004J\u000f\u0010\u0003\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0003\u0010\u0004J\u000f\u0010\u0005\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0005\u0010\u0004J\u000f\u0010\u0006\u001a\u00020\u0002H\u0016¢\u0006\u0004\b\u0006\u0010\u0004J\u0017\u0010\t\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u0007H\u0016¢\u0006\u0004\b\t\u0010\nR\u001c\u0010\f\u001a\u00020\u000b8\u0014@\u0014X\u0094D¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\f\u0010\u000e¨\u0006\u0012"}, d2 = {"Lcom/discord/app/App;", "Landroid/app/Application;", "", "onCreate", "()V", "initializeFlipper", "initializeRLottie", "", "level", "onTrimMemory", "(I)V", "", "isUnderTest", "Z", "()Z", HookHelper.constructorName, "Companion", "a", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public class App extends Application {
    public static final a Companion = new a(null);
    private static final boolean IS_LOCAL = w.contains$default((CharSequence) BuildConfig.FLAVOR, (CharSequence) "local", false, 2, (Object) null);
    private final boolean isUnderTest;

    /* compiled from: App.kt */
    /* loaded from: classes.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: App.kt */
    /* loaded from: classes.dex */
    public static final class b extends o implements Function1<Throwable, Unit> {
        public static final b j = new b();

        public b() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Throwable th) {
            Throwable th2 = th;
            m.checkNotNullParameter(th2, "throwable");
            AppLog appLog = AppLog.g;
            Logger.e$default(appLog, "Subscription error in backgrounded delay, " + th2, null, null, 6, null);
            return Unit.a;
        }
    }

    /* compiled from: App.kt */
    /* loaded from: classes.dex */
    public static final class c extends o implements Function0<Integer> {
        public static final c j = new c();

        public c() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public Integer invoke() {
            return Integer.valueOf(ColorCompat.getThemedColor(ActivityProvider.Companion.getActivity(), (int) R.attr.colorTextLink));
        }
    }

    /* compiled from: App.kt */
    /* loaded from: classes.dex */
    public static final class d extends o implements Function2<String, View, Unit> {
        public static final d j = new d();

        public d() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        public Unit invoke(String str, View view) {
            String str2 = str;
            View view2 = view;
            m.checkNotNullParameter(str2, "url");
            m.checkNotNullParameter(view2, "view");
            UriHandler uriHandler = UriHandler.INSTANCE;
            Context context = view2.getContext();
            m.checkNotNullExpressionValue(context, "view.context");
            UriHandler.handle$default(uriHandler, context, str2, null, 4, null);
            return Unit.a;
        }
    }

    /* compiled from: App.kt */
    /* loaded from: classes.dex */
    public static final /* synthetic */ class e extends k implements Function3<String, Throwable, Map<String, ? extends String>, Unit> {
        public e(AppLog appLog) {
            super(3, appLog, AppLog.class, "e", "e(Ljava/lang/String;Ljava/lang/Throwable;Ljava/util/Map;)V", 0);
        }

        /* JADX WARN: Multi-variable type inference failed */
        @Override // kotlin.jvm.functions.Function3
        public Unit invoke(String str, Throwable th, Map<String, ? extends String> map) {
            String str2 = str;
            m.checkNotNullParameter(str2, "p1");
            ((AppLog) this.receiver).e(str2, th, map);
            return Unit.a;
        }
    }

    /* compiled from: App.kt */
    /* loaded from: classes.dex */
    public static final class f extends o implements Function2<View, String, Unit> {
        public static final f j = new f();

        public f() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        public Unit invoke(View view, String str) {
            View view2 = view;
            String str2 = str;
            m.checkNotNullParameter(view2, "textView");
            m.checkNotNullParameter(str2, "url");
            UriHandler uriHandler = UriHandler.INSTANCE;
            Context context = view2.getContext();
            m.checkNotNullExpressionValue(context, "textView.context");
            UriHandler.handle$default(uriHandler, context, str2, null, 4, null);
            return Unit.a;
        }
    }

    /* compiled from: App.kt */
    /* loaded from: classes.dex */
    public static final class g extends o implements Function1<Experiment, Unit> {
        public static final g j = new g();

        public g() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Experiment experiment) {
            return Unit.a;
        }
    }

    public void initializeFlipper() {
        m.checkNotNullParameter(this, "context");
    }

    public void initializeRLottie() {
        System.loadLibrary("dsti");
    }

    public boolean isUnderTest() {
        return this.isUnderTest;
    }

    @Override // android.app.Application
    public void onCreate() {
        super.onCreate();
        SharedPreferencesProvider.INSTANCE.init(this);
        ApplicationProvider.INSTANCE.init(this);
        ActivityProvider.Companion.init(this);
        ClockFactory.INSTANCE.init(this);
        int i = AppLog.a;
        m.checkNotNullParameter(this, "application");
        AppLog.f2069b = true;
        AppLog.a = 0;
        AppLog.c = PreferenceManager.getDefaultSharedPreferences(this);
        LoggingProvider loggingProvider = LoggingProvider.INSTANCE;
        AppLog appLog = AppLog.g;
        loggingProvider.init(appLog);
        b.i.c.c.e(this);
        String str = null;
        if (BuildUtils.INSTANCE.isValidBuildVersionName(BuildConfig.VERSION_NAME)) {
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        } else {
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(false);
            Logger.w$default(appLog, "Disable crashlytics logging, likely modified client detected.", null, 2, null);
        }
        SystemLogUtils.INSTANCE.initSystemLogCapture();
        Objects.requireNonNull(appLog);
        Bundle bundle = getPackageManager().getApplicationInfo(getPackageName(), 128).metaData;
        if (bundle != null) {
            str = bundle.getString("libdiscord_version");
        }
        if (str == null) {
            str = "Unknown";
        }
        appLog.recordBreadcrumb(str, "libdiscord_version");
        DebugPrintableCollection.Companion.initialize(str);
        AdjustConfig.INSTANCE.init(this, isUnderTest());
        b.a.e.d dVar = b.a.e.d.d;
        b bVar = b.j;
        m.checkNotNullParameter(this, "application");
        m.checkNotNullParameter(bVar, "onError");
        registerActivityLifecycleCallbacks(new b.a.e.b(bVar));
        PersisterConfig.INSTANCE.init(this, ClockFactory.get());
        b.a.k.g.d dVar2 = b.a.k.g.d.f243b;
        b.a.k.g.d dVar3 = (b.a.k.g.d) b.a.k.g.d.a.getValue();
        c cVar = c.j;
        d dVar4 = d.j;
        m.checkNotNullParameter(dVar3, "formattingParserProvider");
        m.checkNotNullParameter(cVar, "defaultClickableTextColorProvider");
        m.checkNotNullParameter(dVar4, "defaultUrlOnClick");
        b.a.k.a aVar = b.a.k.a.d;
        m.checkNotNullParameter(dVar3, "formattingParserProvider");
        m.checkNotNullParameter(cVar, "defaultClickableTextColorProvider");
        m.checkNotNullParameter(dVar4, "defaultUrlOnClick");
        b.a.k.a.a = dVar3;
        b.a.k.a.f241b = cVar;
        b.a.k.a.c = dVar4;
        RestAPI.Companion.init(this);
        NotificationClient.INSTANCE.init(this);
        MGImagesConfig.INSTANCE.init(this);
        Error.init(new b.a.d.a(new e(appLog)));
        LinkifiedTextView.Companion.init(f.j);
        ModelEmojiCustom.setCdnUri(BuildConfig.HOST_CDN);
        SurveyUtils.INSTANCE.init(this);
        AppCompatDelegate.setDefaultNightMode(1);
        initializeFlipper();
        initializeRLottie();
        Objects.requireNonNull(appLog);
        if (FirebaseCrashlytics.getInstance().didCrashOnPreviousExecution()) {
            AnalyticsTracker.INSTANCE.appCrashed();
        }
        AnalyticsDeviceResourceUsageMonitor.INSTANCE.start();
        Observable<R> F = StoreStream.Companion.getExperiments().observeUserExperiment("2022-01_rna_rollout_experiment_validation", true).x(ObservableExtensionsKt$filterNull$1.INSTANCE).F(ObservableExtensionsKt$filterNull$2.INSTANCE);
        m.checkNotNullExpressionValue(F, "filter { it != null }.map { it!! }");
        Observable Z = F.Z(1);
        m.checkNotNullExpressionValue(Z, "StoreStream.getExperimen…erNull()\n        .take(1)");
        ObservableExtensionsKt.appSubscribe(Z, getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, g.j);
        AppLog.i("Application initialized.");
    }

    @Override // android.app.Application, android.content.ComponentCallbacks2
    public void onTrimMemory(int i) {
        super.onTrimMemory(i);
        MGImagesConfig.INSTANCE.onTrimMemory(i);
    }
}
