package com.discord.app;

import b.d.b.a.a;
import com.discord.api.science.AnalyticsSchema;
import d0.e0.g;
import d0.t.n;
import d0.z.d.m;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: AppLogger.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR#\u0010\u0012\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\r\u0018\u00010\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R'\u0010\u0019\u001a\u0010\u0012\f\u0012\n\u0012\u0002\b\u0003\u0012\u0002\b\u00030\u00140\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u0019\u0010\u001e\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u001b\u001a\u0004\b\u001c\u0010\u001d¨\u0006\u001f"}, d2 = {"Lcom/discord/app/LoggingConfig;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "Lkotlin/Function0;", "Lcom/discord/api/science/AnalyticsSchema;", "c", "Lkotlin/jvm/functions/Function0;", "getImpressionSchemaProvider", "()Lkotlin/jvm/functions/Function0;", "impressionSchemaProvider", "", "Ld0/e0/g;", "b", "Ljava/util/List;", "getAutoLogImpressionProperties", "()Ljava/util/List;", "autoLogImpressionProperties", "a", "Z", "getAutoLogImpressionOnChanged", "()Z", "autoLogImpressionOnChanged", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class LoggingConfig {
    public final boolean a;

    /* renamed from: b  reason: collision with root package name */
    public final List<g<?, ?>> f2072b;
    public final Function0<AnalyticsSchema> c;

    public LoggingConfig() {
        this(false, null, null, 7);
    }

    public LoggingConfig(boolean z2, List list, Function0 function0, int i) {
        z2 = (i & 1) != 0 ? true : z2;
        List<g<?, ?>> emptyList = (i & 2) != 0 ? n.emptyList() : null;
        function0 = (i & 4) != 0 ? null : function0;
        m.checkNotNullParameter(emptyList, "autoLogImpressionProperties");
        this.a = z2;
        this.f2072b = emptyList;
        this.c = function0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LoggingConfig)) {
            return false;
        }
        LoggingConfig loggingConfig = (LoggingConfig) obj;
        return this.a == loggingConfig.a && m.areEqual(this.f2072b, loggingConfig.f2072b) && m.areEqual(this.c, loggingConfig.c);
    }

    public int hashCode() {
        boolean z2 = this.a;
        if (z2) {
            z2 = true;
        }
        int i = z2 ? 1 : 0;
        int i2 = z2 ? 1 : 0;
        int i3 = i * 31;
        List<g<?, ?>> list = this.f2072b;
        int i4 = 0;
        int hashCode = (i3 + (list != null ? list.hashCode() : 0)) * 31;
        Function0<AnalyticsSchema> function0 = this.c;
        if (function0 != null) {
            i4 = function0.hashCode();
        }
        return hashCode + i4;
    }

    public String toString() {
        StringBuilder R = a.R("LoggingConfig(autoLogImpressionOnChanged=");
        R.append(this.a);
        R.append(", autoLogImpressionProperties=");
        R.append(this.f2072b);
        R.append(", impressionSchemaProvider=");
        R.append(this.c);
        R.append(")");
        return R.toString();
    }
}
