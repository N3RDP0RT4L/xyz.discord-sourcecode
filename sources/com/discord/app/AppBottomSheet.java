package com.discord.app;

import andhook.lib.HookHelper;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.annotation.CallSuper;
import androidx.annotation.LayoutRes;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.graphics.Insets;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.discord.app.AppLogger;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.display.DisplayUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.utilities.view.text.TextWatcher;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import d0.z.d.m;
import d0.z.d.o;
import j0.l.e.k;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
import rx.subscriptions.CompositeSubscription;
import xyz.discord.R;
/* compiled from: AppBottomSheet.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000²\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\r\b&\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\u0011\u0012\b\b\u0002\u0010p\u001a\u00020\u0017¢\u0006\u0004\br\u0010sJ\u000f\u0010\u0006\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u000f\u0010\b\u001a\u00020\u0005H\u0002¢\u0006\u0004\b\b\u0010\u0007J\r\u0010\n\u001a\u00020\t¢\u0006\u0004\b\n\u0010\u000bJ\u001d\u0010\u000e\u001a\u00020\u00052\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\fH\u0016¢\u0006\u0004\b\u000e\u0010\u000fJ\u001d\u0010\u0010\u001a\u00020\u00052\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\fH\u0016¢\u0006\u0004\b\u0010\u0010\u000fJ-\u0010\u0012\u001a\u00020\u00052\u000e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\fH\u0016¢\u0006\u0004\b\u0012\u0010\u0013J\u001d\u0010\u0014\u001a\u00020\u00052\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\fH\u0016¢\u0006\u0004\b\u0014\u0010\u000fJ-\u0010\u0015\u001a\u00020\u00052\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\f2\u000e\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\fH\u0016¢\u0006\u0004\b\u0015\u0010\u0013J\u001d\u0010\u0015\u001a\u00020\u00052\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\fH\u0016¢\u0006\u0004\b\u0015\u0010\u000fJ+\u0010\u0016\u001a\u00020\u00052\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\f2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00050\fH\u0016¢\u0006\u0004\b\u0016\u0010\u0013J\u000f\u0010\u0018\u001a\u00020\u0017H\u0016¢\u0006\u0004\b\u0018\u0010\u0019J\u000f\u0010\u001b\u001a\u00020\u001aH'¢\u0006\u0004\b\u001b\u0010\u001cJ\u0019\u0010\u001f\u001a\u00020\u00052\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dH\u0016¢\u0006\u0004\b\u001f\u0010 J\u0019\u0010\"\u001a\u00020!2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dH\u0016¢\u0006\u0004\b\"\u0010#J-\u0010)\u001a\u0004\u0018\u00010(2\u0006\u0010%\u001a\u00020$2\b\u0010'\u001a\u0004\u0018\u00010&2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dH\u0016¢\u0006\u0004\b)\u0010*J!\u0010,\u001a\u00020\u00052\u0006\u0010+\u001a\u00020(2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001dH\u0017¢\u0006\u0004\b,\u0010-J\u000f\u0010.\u001a\u00020\u0005H\u0016¢\u0006\u0004\b.\u0010\u0007J!\u00103\u001a\u00020\u00052\u0006\u00100\u001a\u00020/2\b\u00102\u001a\u0004\u0018\u000101H\u0016¢\u0006\u0004\b3\u00104J!\u00103\u001a\u00020\u001a2\u0006\u00106\u001a\u0002052\b\u00102\u001a\u0004\u0018\u000101H\u0016¢\u0006\u0004\b3\u00107J\u000f\u00108\u001a\u00020\u0005H\u0016¢\u0006\u0004\b8\u0010\u0007J\u000f\u00109\u001a\u00020\u0005H\u0016¢\u0006\u0004\b9\u0010\u0007J\u000f\u0010:\u001a\u00020\u0005H\u0016¢\u0006\u0004\b:\u0010\u0007J\u0017\u0010<\u001a\u00020\u00052\b\u0010;\u001a\u0004\u0018\u00010(¢\u0006\u0004\b<\u0010=J\u0015\u0010?\u001a\u00020\u00052\u0006\u0010>\u001a\u00020\u001a¢\u0006\u0004\b?\u0010@J\u000f\u0010A\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\bA\u0010BJ\u0015\u0010D\u001a\u00020\u00052\u0006\u0010C\u001a\u00020\u001a¢\u0006\u0004\bD\u0010@J\u0017\u0010G\u001a\u00020\u00052\u0006\u0010F\u001a\u00020EH\u0016¢\u0006\u0004\bG\u0010HJ'\u0010K\u001a\u00020\u0005*\u00020(2\u0012\u0010J\u001a\u000e\u0012\u0004\u0012\u00020(\u0012\u0004\u0012\u00020\u00050IH\u0004¢\u0006\u0004\bK\u0010LJ\u0015\u0010M\u001a\u00020\u00052\u0006\u0010+\u001a\u00020(¢\u0006\u0004\bM\u0010=J\u0019\u0010N\u001a\u00020\u00052\n\b\u0002\u0010+\u001a\u0004\u0018\u00010(¢\u0006\u0004\bN\u0010=J\u0017\u0010O\u001a\u00020\u00052\u0006\u0010>\u001a\u00020\u001aH\u0016¢\u0006\u0004\bO\u0010@R\u001e\u0010Q\u001a\u0004\u0018\u00010P8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bQ\u0010R\u001a\u0004\bS\u0010TR(\u0010W\u001a\u000e\u0012\u0004\u0012\u00020V\u0012\u0004\u0012\u00020V0U8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bW\u0010X\u001a\u0004\bY\u0010ZR\u0016\u0010\\\u001a\u00020[8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\\\u0010]R\u0015\u0010_\u001a\u0004\u0018\u00010\t8F@\u0006¢\u0006\u0006\u001a\u0004\b^\u0010\u000bR\u0018\u0010F\u001a\u0004\u0018\u00010E8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bF\u0010`R\u001d\u0010f\u001a\u00020a8B@\u0002X\u0082\u0084\u0002¢\u0006\f\n\u0004\bb\u0010c\u001a\u0004\bd\u0010eR\u001e\u0010j\u001a\n\u0012\u0004\u0012\u00020(\u0018\u00010g8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\bh\u0010iR\u0016\u0010m\u001a\u00020\u001d8D@\u0004X\u0084\u0004¢\u0006\u0006\u001a\u0004\bk\u0010lR\u0018\u0010n\u001a\u0004\u0018\u00010(8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bn\u0010oR\u0016\u0010p\u001a\u00020\u00178\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bp\u0010q¨\u0006t"}, d2 = {"Lcom/discord/app/AppBottomSheet;", "Lcom/discord/app/AppPermissionsRequests;", "Lcom/discord/app/AppComponent;", "Lcom/discord/app/AppLogger$a;", "Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;", "", "fixWindowInsetHandling", "()V", "resizeContentForSoftInput", "Lcom/discord/app/AppActivity;", "requireAppActivity", "()Lcom/discord/app/AppActivity;", "Lkotlin/Function0;", "onSuccess", "requestVideoCallPermissions", "(Lkotlin/jvm/functions/Function0;)V", "requestMedia", "onFailure", "requestMicrophone", "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "requestMediaDownload", "requestCameraQRScanner", "requestContacts", "", "hasMedia", "()Z", "", "getContentViewResId", "()I", "Landroid/os/Bundle;", "savedInstanceState", "onCreate", "(Landroid/os/Bundle;)V", "Landroid/app/Dialog;", "onCreateDialog", "(Landroid/os/Bundle;)Landroid/app/Dialog;", "Landroid/view/LayoutInflater;", "inflater", "Landroid/view/ViewGroup;", "container", "Landroid/view/View;", "onCreateView", "(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;", "view", "onViewCreated", "(Landroid/view/View;Landroid/os/Bundle;)V", "onDestroyView", "Landroidx/fragment/app/FragmentManager;", "manager", "", "tag", "show", "(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V", "Landroidx/fragment/app/FragmentTransaction;", "transaction", "(Landroidx/fragment/app/FragmentTransaction;Ljava/lang/String;)I", "dismiss", "onResume", "onPause", "targetView", "setPeekHeightBottomView", "(Landroid/view/View;)V", "state", "setBottomSheetState", "(I)V", "setBottomSheetCollapsedStateDisabled", "()Lkotlin/Unit;", "peekHeightPx", "updatePeekHeightPx", "Lrx/subscriptions/CompositeSubscription;", "compositeSubscription", "bindSubscriptions", "(Lrx/subscriptions/CompositeSubscription;)V", "Lkotlin/Function1;", "onClickListener", "setOnClickAndDismissListener", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "showKeyboard", "hideKeyboard", "onStateChanged", "Lcom/discord/app/LoggingConfig;", "loggingConfig", "Lcom/discord/app/LoggingConfig;", "getLoggingConfig", "()Lcom/discord/app/LoggingConfig;", "Lrx/subjects/Subject;", "Ljava/lang/Void;", "unsubscribeSignal", "Lrx/subjects/Subject;", "getUnsubscribeSignal", "()Lrx/subjects/Subject;", "Landroid/view/View$OnLayoutChangeListener;", "peekLayoutListener", "Landroid/view/View$OnLayoutChangeListener;", "getAppActivity", "appActivity", "Lrx/subscriptions/CompositeSubscription;", "Lcom/discord/app/AppLogger;", "appLogger$delegate", "Lkotlin/Lazy;", "getAppLogger", "()Lcom/discord/app/AppLogger;", "appLogger", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "getBottomSheetBehavior", "()Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "bottomSheetBehavior", "getArgumentsOrDefault", "()Landroid/os/Bundle;", "argumentsOrDefault", "peekBottomView", "Landroid/view/View;", "shouldAvoidKeyboard", "Z", HookHelper.constructorName, "(Z)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public abstract class AppBottomSheet extends BottomSheetDialogFragment implements AppPermissionsRequests, AppComponent, AppLogger.a {
    private final Lazy appLogger$delegate;
    private CompositeSubscription compositeSubscription;
    private final LoggingConfig loggingConfig;
    private View peekBottomView;
    private final View.OnLayoutChangeListener peekLayoutListener;
    private final boolean shouldAvoidKeyboard;
    private final Subject<Void, Void> unsubscribeSignal;

    /* compiled from: java-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a implements OnApplyWindowInsetsListener {
        public static final a a = new a(0);

        /* renamed from: b  reason: collision with root package name */
        public static final a f2067b = new a(1);
        public final /* synthetic */ int c;

        public a(int i) {
            this.c = i;
        }

        @Override // androidx.core.view.OnApplyWindowInsetsListener
        public final WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
            int i = this.c;
            if (i == 0) {
                m.checkNotNullParameter(view, "v");
                m.checkNotNullParameter(windowInsetsCompat, "insets");
                view.setPadding(view.getPaddingLeft(), windowInsetsCompat.getSystemWindowInsetTop(), view.getPaddingRight(), view.getPaddingBottom());
                return new WindowInsetsCompat.Builder(windowInsetsCompat).setSystemWindowInsets(Insets.of(windowInsetsCompat.getSystemWindowInsetLeft(), 0, windowInsetsCompat.getSystemWindowInsetRight(), windowInsetsCompat.getSystemWindowInsetBottom())).build();
            } else if (i == 1) {
                m.checkNotNullExpressionValue(view, "v");
                m.checkNotNullExpressionValue(windowInsetsCompat, "insets");
                view.setPadding(windowInsetsCompat.getSystemWindowInsetLeft(), windowInsetsCompat.getSystemWindowInsetTop(), windowInsetsCompat.getSystemWindowInsetRight(), windowInsetsCompat.getSystemWindowInsetBottom());
                return windowInsetsCompat.consumeSystemWindowInsets();
            } else {
                throw null;
            }
        }
    }

    /* compiled from: AppBottomSheet.kt */
    /* loaded from: classes.dex */
    public static final class b extends o implements Function0<AppLogger> {
        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public AppLogger invoke() {
            return new AppLogger(AppBottomSheet.this, null, false, 6);
        }
    }

    /* compiled from: AppBottomSheet.kt */
    /* loaded from: classes.dex */
    public static final class c implements DialogInterface.OnShowListener {
        public final /* synthetic */ Dialog a;

        /* renamed from: b  reason: collision with root package name */
        public final /* synthetic */ AppBottomSheet f2068b;

        /* compiled from: AppBottomSheet.kt */
        /* loaded from: classes.dex */
        public static final class a extends o implements Function1<Unit, Unit> {
            public a() {
                super(1);
            }

            @Override // kotlin.jvm.functions.Function1
            public Unit invoke(Unit unit) {
                BottomSheetBehavior<FrameLayout> behavior;
                Dialog dialog = c.this.a;
                if (!(dialog instanceof BottomSheetDialog)) {
                    dialog = null;
                }
                BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialog;
                if (!(bottomSheetDialog == null || (behavior = bottomSheetDialog.getBehavior()) == null)) {
                    behavior.setState(3);
                }
                return Unit.a;
            }
        }

        public c(Dialog dialog, AppBottomSheet appBottomSheet) {
            this.a = dialog;
            this.f2068b = appBottomSheet;
        }

        @Override // android.content.DialogInterface.OnShowListener
        public final void onShow(DialogInterface dialogInterface) {
            k kVar = new k(Unit.a);
            m.checkNotNullExpressionValue(kVar, "Observable.just(Unit)");
            ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(kVar, this.f2068b, null, 2, null), this.f2068b.getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new a());
        }
    }

    /* compiled from: AppBottomSheet.kt */
    /* loaded from: classes.dex */
    public static final class d extends BottomSheetBehavior.BottomSheetCallback {
        public d() {
        }

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
        public void onSlide(View view, float f) {
            m.checkNotNullParameter(view, "bottomSheet");
        }

        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
        public void onStateChanged(View view, int i) {
            m.checkNotNullParameter(view, "bottomSheet");
            AppBottomSheet.this.onStateChanged(i);
            if (i == 5) {
                AppBottomSheet.this.dismiss();
            }
        }
    }

    /* compiled from: AppBottomSheet.kt */
    /* loaded from: classes.dex */
    public static final class e implements View.OnLayoutChangeListener {
        public e() {
        }

        @Override // android.view.View.OnLayoutChangeListener
        public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            AppBottomSheet.this.updatePeekHeightPx(i4);
        }
    }

    /* compiled from: AppBottomSheet.kt */
    /* loaded from: classes.dex */
    public static final class f implements DialogInterface.OnShowListener {
        public final /* synthetic */ Dialog a;

        public f(Dialog dialog) {
            this.a = dialog;
        }

        @Override // android.content.DialogInterface.OnShowListener
        public final void onShow(DialogInterface dialogInterface) {
            BottomSheetBehavior from = BottomSheetBehavior.from(this.a.findViewById(R.id.design_bottom_sheet));
            m.checkNotNullExpressionValue(from, "BottomSheetBehavior.from(bottomSheet)");
            from.setSkipCollapsed(true);
            from.setState(3);
        }
    }

    /* compiled from: AppBottomSheet.kt */
    /* loaded from: classes.dex */
    public static final class g implements View.OnClickListener {
        public final /* synthetic */ View k;
        public final /* synthetic */ Function1 l;

        public g(View view, Function1 function1) {
            this.k = view;
            this.l = function1;
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            Unit unit = (Unit) this.l.invoke(this.k);
            AppBottomSheet.this.dismiss();
        }
    }

    public AppBottomSheet() {
        this(false, 1, null);
    }

    public /* synthetic */ AppBottomSheet(boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? false : z2);
    }

    private final void fixWindowInsetHandling() {
        View findViewById;
        View view = getView();
        if (view != null && (findViewById = view.getRootView().findViewById(R.id.container)) != null) {
            ViewCompat.setOnApplyWindowInsetsListener(findViewById, a.a);
            View findViewById2 = view.getRootView().findViewById(R.id.design_bottom_sheet);
            m.checkNotNullExpressionValue(findViewById2, "designBottomSheet");
            findViewById2.setFitsSystemWindows(true);
            ViewCompat.setOnApplyWindowInsetsListener(findViewById2, DisplayUtils.getNO_OP_WINDOW_INSETS_LISTENER());
            ViewCompat.setOnApplyWindowInsetsListener(view, a.f2067b);
        }
    }

    private final AppLogger getAppLogger() {
        return (AppLogger) this.appLogger$delegate.getValue();
    }

    private final BottomSheetBehavior<View> getBottomSheetBehavior() {
        View view = getView();
        BottomSheetBehavior<View> bottomSheetBehavior = null;
        ViewParent parent = view != null ? view.getParent() : null;
        if (!(parent instanceof View)) {
            parent = null;
        }
        View view2 = (View) parent;
        ViewGroup.LayoutParams layoutParams = view2 != null ? view2.getLayoutParams() : null;
        if (!(layoutParams instanceof CoordinatorLayout.LayoutParams)) {
            layoutParams = null;
        }
        CoordinatorLayout.LayoutParams layoutParams2 = (CoordinatorLayout.LayoutParams) layoutParams;
        CoordinatorLayout.Behavior behavior = layoutParams2 != null ? layoutParams2.getBehavior() : null;
        if (behavior instanceof BottomSheetBehavior) {
            bottomSheetBehavior = behavior;
        }
        return bottomSheetBehavior;
    }

    public static /* synthetic */ void hideKeyboard$default(AppBottomSheet appBottomSheet, View view, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                view = null;
            }
            appBottomSheet.hideKeyboard(view);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: hideKeyboard");
    }

    private final void resizeContentForSoftInput() {
        Window window;
        Dialog dialog = getDialog();
        if (dialog != null && (window = dialog.getWindow()) != null) {
            window.setSoftInputMode(16);
        }
    }

    public void bindSubscriptions(CompositeSubscription compositeSubscription) {
        m.checkNotNullParameter(compositeSubscription, "compositeSubscription");
    }

    @Override // com.google.android.material.bottomsheet.BottomSheetDialogFragment, androidx.fragment.app.DialogFragment
    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception unused) {
        }
    }

    public final AppActivity getAppActivity() {
        return (AppActivity) e();
    }

    public final Bundle getArgumentsOrDefault() {
        Bundle arguments = getArguments();
        if (arguments == null) {
            arguments = new Bundle();
        }
        m.checkNotNullExpressionValue(arguments, "arguments ?: Bundle()");
        return arguments;
    }

    @LayoutRes
    public abstract int getContentViewResId();

    @Override // com.discord.app.AppLogger.a
    public LoggingConfig getLoggingConfig() {
        return this.loggingConfig;
    }

    @Override // com.discord.app.AppComponent
    public Subject<Void, Void> getUnsubscribeSignal() {
        return this.unsubscribeSignal;
    }

    @Override // com.discord.app.AppPermissionsRequests
    public boolean hasMedia() {
        return requireAppActivity().f2063x.hasMedia();
    }

    public final void hideKeyboard(View view) {
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            appActivity.hideKeyboard(view);
        }
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (this.shouldAvoidKeyboard) {
            setStyle(0, R.style.UiKit_Sheet_KeyboardAvoiding);
        }
    }

    @Override // com.google.android.material.bottomsheet.BottomSheetDialogFragment, androidx.appcompat.app.AppCompatDialogFragment, androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        onCreateDialog.setCanceledOnTouchOutside(true);
        if (AccessibilityUtils.INSTANCE.isReducedMotionEnabled()) {
            m.checkNotNullExpressionValue(onCreateDialog, "dialog");
            Window window = onCreateDialog.getWindow();
            if (window != null) {
                window.setWindowAnimations(R.style.FadeInOut);
            }
        }
        if (this.shouldAvoidKeyboard) {
            onCreateDialog.setOnShowListener(new c(onCreateDialog, this));
        }
        m.checkNotNullExpressionValue(onCreateDialog, "super.onCreateDialog(sav…      }\n      }\n    }\n  }");
        return onCreateDialog;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        m.checkNotNullParameter(layoutInflater, "inflater");
        return layoutInflater.inflate(getContentViewResId(), (ViewGroup) null);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        TextWatcher.Companion.reset(this);
        super.onDestroyView();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        CompositeSubscription compositeSubscription = this.compositeSubscription;
        if (compositeSubscription != null) {
            compositeSubscription.b();
        }
        getUnsubscribeSignal().onNext(null);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        fixWindowInsetHandling();
        CompositeSubscription compositeSubscription = new CompositeSubscription();
        bindSubscriptions(compositeSubscription);
        this.compositeSubscription = compositeSubscription;
    }

    public void onStateChanged(int i) {
    }

    @Override // androidx.fragment.app.Fragment
    @CallSuper
    public void onViewCreated(View view, Bundle bundle) {
        m.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        DisplayUtils.drawUnderSystemBars(view);
        BottomSheetBehavior<View> bottomSheetBehavior = getBottomSheetBehavior();
        if (bottomSheetBehavior != null) {
            bottomSheetBehavior.setBottomSheetCallback(new d());
        }
        getAppLogger().b();
    }

    @Override // com.discord.app.AppPermissionsRequests
    public void requestCameraQRScanner(Function0<Unit> function0, Function0<Unit> function02) {
        m.checkNotNullParameter(function0, "onSuccess");
        requireAppActivity().f2063x.requestCameraQRScanner(function0, function02);
    }

    @Override // com.discord.app.AppPermissionsRequests
    public void requestContacts(Function0<Unit> function0, Function0<Unit> function02) {
        m.checkNotNullParameter(function0, "onSuccess");
        m.checkNotNullParameter(function02, "onFailure");
        requireAppActivity().f2063x.requestContacts(function0, function02);
    }

    @Override // com.discord.app.AppPermissionsRequests
    public void requestMedia(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onSuccess");
        requireAppActivity().f2063x.requestMedia(function0);
    }

    @Override // com.discord.app.AppPermissionsRequests
    public void requestMediaDownload(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onSuccess");
        requireAppActivity().f2063x.requestMediaDownload(function0);
    }

    @Override // com.discord.app.AppPermissionsRequests
    public void requestMicrophone(Function0<Unit> function0, Function0<Unit> function02) {
        m.checkNotNullParameter(function02, "onSuccess");
        requireAppActivity().f2063x.requestMicrophone(function0, function02);
    }

    @Override // com.discord.app.AppPermissionsRequests
    public void requestVideoCallPermissions(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onSuccess");
        requireAppActivity().f2063x.requestVideoCallPermissions(function0);
    }

    public final AppActivity requireAppActivity() {
        AppActivity appActivity = getAppActivity();
        m.checkNotNull(appActivity);
        return appActivity;
    }

    public final Unit setBottomSheetCollapsedStateDisabled() {
        Dialog dialog = getDialog();
        if (dialog == null) {
            return null;
        }
        resizeContentForSoftInput();
        dialog.setOnShowListener(new f(dialog));
        return Unit.a;
    }

    public final void setBottomSheetState(int i) {
        BottomSheetBehavior<View> bottomSheetBehavior = getBottomSheetBehavior();
        if (bottomSheetBehavior != null) {
            bottomSheetBehavior.setState(i);
        }
    }

    public final void setOnClickAndDismissListener(View view, Function1<? super View, Unit> function1) {
        m.checkNotNullParameter(view, "$this$setOnClickAndDismissListener");
        m.checkNotNullParameter(function1, "onClickListener");
        view.setOnClickListener(new g(view, function1));
    }

    public final void setPeekHeightBottomView(View view) {
        if (!m.areEqual(this.peekBottomView, view)) {
            View view2 = this.peekBottomView;
            if (view2 != null) {
                view2.removeOnLayoutChangeListener(this.peekLayoutListener);
            }
            this.peekBottomView = view;
            if (view != null) {
                view.addOnLayoutChangeListener(this.peekLayoutListener);
                view.requestLayout();
                return;
            }
            setPeekHeightBottomView(getView());
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        m.checkNotNullParameter(fragmentManager, "manager");
        if (fragmentManager.findFragmentByTag(str) == null) {
            try {
                super.show(fragmentManager, str);
            } catch (Exception unused) {
            }
        }
    }

    public final void showKeyboard(View view) {
        m.checkNotNullParameter(view, "view");
        AppActivity appActivity = getAppActivity();
        if (appActivity != null) {
            appActivity.showKeyboard(view);
        }
    }

    public final void updatePeekHeightPx(int i) {
        BottomSheetBehavior<View> bottomSheetBehavior = getBottomSheetBehavior();
        if (bottomSheetBehavior != null) {
            bottomSheetBehavior.setPeekHeight(i);
        }
    }

    public AppBottomSheet(boolean z2) {
        this.shouldAvoidKeyboard = z2;
        PublishSubject k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.unsubscribeSignal = k0;
        this.peekLayoutListener = new e();
        this.appLogger$delegate = d0.g.lazy(new b());
    }

    @Override // androidx.fragment.app.DialogFragment
    public int show(FragmentTransaction fragmentTransaction, String str) {
        m.checkNotNullParameter(fragmentTransaction, "transaction");
        try {
            return super.show(fragmentTransaction, str);
        } catch (Exception unused) {
            return -1;
        }
    }

    @Override // com.discord.app.AppPermissionsRequests
    public void requestCameraQRScanner(Function0<Unit> function0) {
        m.checkNotNullParameter(function0, "onSuccess");
        requireAppActivity().f2063x.requestCameraQRScanner(function0);
    }
}
