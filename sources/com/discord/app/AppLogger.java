package com.discord.app;

import com.discord.api.science.AnalyticsSchema;
import com.discord.utilities.analytics.AnalyticsUtils;
import com.discord.utilities.features.GrowthTeamFeatures;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
/* compiled from: AppLogger.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0001\u0007J\u000f\u0010\u0003\u001a\u00020\u0002H\u0000¢\u0006\u0004\b\u0003\u0010\u0004J\u0019\u0010\u0007\u001a\u00020\u00022\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b\u0007\u0010\bR\u0019\u0010\f\u001a\u00020\t8\u0006@\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u0019\u0010\u0013\u001a\u00020\u000e8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0019\u001a\u00020\u00148\u0006@\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0017\u0010\u0018R\u0018\u0010\u001b\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0003\u0010\u001aR\u0016\u0010\u001c\u001a\u00020\t8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\b\u0007\u0010\u000b¨\u0006\u001d"}, d2 = {"Lcom/discord/app/AppLogger;", "", "", "b", "()V", "Lcom/discord/api/science/AnalyticsSchema;", "analyticsSchema", "a", "(Lcom/discord/api/science/AnalyticsSchema;)V", "", "e", "Z", "isImpressionLoggingEnabled", "()Z", "Lcom/discord/app/AppLogger$a;", "c", "Lcom/discord/app/AppLogger$a;", "getProvider", "()Lcom/discord/app/AppLogger$a;", "provider", "Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;", "d", "Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;", "getTracker", "()Lcom/discord/utilities/analytics/AnalyticsUtils$Tracker;", "tracker", "Lcom/discord/api/science/AnalyticsSchema;", "previousAnalyticsSchema", "hasLoggedImpression", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class AppLogger {
    public boolean a;

    /* renamed from: b  reason: collision with root package name */
    public AnalyticsSchema f2070b;
    public final a c;
    public final AnalyticsUtils.Tracker d;
    public final boolean e;

    /* compiled from: AppLogger.kt */
    /* loaded from: classes.dex */
    public interface a {
        LoggingConfig getLoggingConfig();
    }

    public AppLogger(a aVar, AnalyticsUtils.Tracker tracker, boolean z2, int i) {
        AnalyticsUtils.Tracker companion = (i & 2) != 0 ? AnalyticsUtils.Tracker.Companion.getInstance() : null;
        z2 = (i & 4) != 0 ? GrowthTeamFeatures.INSTANCE.isImpressionLoggingEnabled() : z2;
        m.checkNotNullParameter(aVar, "provider");
        m.checkNotNullParameter(companion, "tracker");
        this.c = aVar;
        this.d = companion;
        this.e = z2;
    }

    public final void a(AnalyticsSchema analyticsSchema) {
        Function0<AnalyticsSchema> function0;
        AnalyticsSchema invoke;
        if (analyticsSchema != null) {
            this.d.track(analyticsSchema);
            return;
        }
        LoggingConfig loggingConfig = this.c.getLoggingConfig();
        if (loggingConfig != null && (function0 = loggingConfig.c) != null && (invoke = function0.invoke()) != null) {
            this.d.track(invoke);
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:39:0x006e, code lost:
        if (r0 != false) goto L40;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final synchronized void b() {
        /*
            r7 = this;
            monitor-enter(r7)
            boolean r0 = r7.e     // Catch: java.lang.Throwable -> L86
            if (r0 != 0) goto L7
            monitor-exit(r7)
            return
        L7:
            com.discord.app.AppLogger$a r0 = r7.c     // Catch: java.lang.Throwable -> L86
            com.discord.app.LoggingConfig r0 = r0.getLoggingConfig()     // Catch: java.lang.Throwable -> L86
            if (r0 == 0) goto L84
            kotlin.jvm.functions.Function0<com.discord.api.science.AnalyticsSchema> r1 = r0.c     // Catch: java.lang.Throwable -> L86
            if (r1 == 0) goto L82
            java.lang.Object r1 = r1.invoke()     // Catch: java.lang.Throwable -> L86
            com.discord.api.science.AnalyticsSchema r1 = (com.discord.api.science.AnalyticsSchema) r1     // Catch: java.lang.Throwable -> L86
            if (r1 == 0) goto L82
            com.discord.api.science.AnalyticsSchema r2 = r7.f2070b     // Catch: java.lang.Throwable -> L86
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L70
            boolean r5 = r0.a     // Catch: java.lang.Throwable -> L86
            if (r5 == 0) goto L71
            java.util.List<d0.e0.g<?, ?>> r5 = r0.f2072b     // Catch: java.lang.Throwable -> L86
            boolean r5 = r5.isEmpty()     // Catch: java.lang.Throwable -> L86
            if (r5 == 0) goto L34
            boolean r5 = d0.z.d.m.areEqual(r2, r1)     // Catch: java.lang.Throwable -> L86
            r5 = r5 ^ r3
            if (r5 != 0) goto L70
        L34:
            java.util.List<d0.e0.g<?, ?>> r0 = r0.f2072b     // Catch: java.lang.Throwable -> L86
            boolean r5 = r0 instanceof java.util.Collection     // Catch: java.lang.Throwable -> L86
            if (r5 == 0) goto L41
            boolean r5 = r0.isEmpty()     // Catch: java.lang.Throwable -> L86
            if (r5 == 0) goto L41
            goto L6d
        L41:
            java.util.Iterator r0 = r0.iterator()     // Catch: java.lang.Throwable -> L86
        L45:
            boolean r5 = r0.hasNext()     // Catch: java.lang.Throwable -> L86
            if (r5 == 0) goto L6d
            java.lang.Object r5 = r0.next()     // Catch: java.lang.Throwable -> L86
            d0.e0.g r5 = (d0.e0.g) r5     // Catch: java.lang.Throwable -> L86
            boolean r6 = r5 instanceof d0.e0.g     // Catch: java.lang.Throwable -> L86
            if (r6 != 0) goto L57
            r6 = 0
            goto L58
        L57:
            r6 = r5
        L58:
            if (r6 == 0) goto L68
            java.lang.Object r6 = r5.get(r1)     // Catch: java.lang.Throwable -> L86
            java.lang.Object r5 = r5.get(r2)     // Catch: java.lang.Throwable -> L86
            boolean r5 = d0.z.d.m.areEqual(r6, r5)     // Catch: java.lang.Throwable -> L86
            r5 = r5 ^ r3
            goto L69
        L68:
            r5 = 0
        L69:
            if (r5 == 0) goto L45
            r0 = 1
            goto L6e
        L6d:
            r0 = 0
        L6e:
            if (r0 == 0) goto L71
        L70:
            r4 = 1
        L71:
            boolean r0 = r7.a     // Catch: java.lang.Throwable -> L86
            if (r0 == 0) goto L79
            if (r4 != 0) goto L79
            monitor-exit(r7)
            return
        L79:
            r7.f2070b = r1     // Catch: java.lang.Throwable -> L86
            r7.a = r3     // Catch: java.lang.Throwable -> L86
            r7.a(r1)     // Catch: java.lang.Throwable -> L86
            monitor-exit(r7)
            return
        L82:
            monitor-exit(r7)
            return
        L84:
            monitor-exit(r7)
            return
        L86:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.discord.app.AppLogger.b():void");
    }
}
