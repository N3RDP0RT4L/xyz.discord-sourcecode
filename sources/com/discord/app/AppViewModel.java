package com.discord.app;

import andhook.lib.HookHelper;
import androidx.annotation.MainThread;
import androidx.exifinterface.media.ExifInterface;
import androidx.lifecycle.ViewModel;
import d0.z.d.m;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
/* compiled from: AppViewModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\b&\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u00022\u00020\u0003B\u0013\u0012\n\b\u0002\u0010#\u001a\u0004\u0018\u00018\u0000¢\u0006\u0004\b$\u0010\fJ\u000f\u0010\u0004\u001a\u00028\u0000H\u0004¢\u0006\u0004\b\u0004\u0010\u0005J\u0013\u0010\u0007\u001a\b\u0012\u0004\u0012\u00028\u00000\u0006¢\u0006\u0004\b\u0007\u0010\bJ\u0017\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00028\u0000H\u0015¢\u0006\u0004\b\u000b\u0010\fJ!\u0010\u000f\u001a\u00028\u00002\b\u0010\r\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u000e\u001a\u00028\u0000H\u0014¢\u0006\u0004\b\u000f\u0010\u0010J'\u0010\u0014\u001a\u00028\u0001\"\u0004\b\u0001\u0010\u00112\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0012¢\u0006\u0004\b\u0014\u0010\u0015J\u000f\u0010\u0016\u001a\u00020\nH\u0014¢\u0006\u0004\b\u0016\u0010\u0017R\u0018\u0010\t\u001a\u0004\u0018\u00018\u00008D@\u0004X\u0084\u0004¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0005R(\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001a0\u00198\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR:\u0010!\u001a&\u0012\f\u0012\n  *\u0004\u0018\u00018\u00008\u0000  *\u0012\u0012\f\u0012\n  *\u0004\u0018\u00018\u00008\u0000\u0018\u00010\u001f0\u001f8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b!\u0010\"¨\u0006%"}, d2 = {"Lcom/discord/app/AppViewModel;", ExifInterface.GPS_MEASUREMENT_INTERRUPTED, "Landroidx/lifecycle/ViewModel;", "Lcom/discord/app/AppComponent;", "requireViewState", "()Ljava/lang/Object;", "Lrx/Observable;", "observeViewState", "()Lrx/Observable;", "viewState", "", "updateViewState", "(Ljava/lang/Object;)V", "previousViewState", "pendingViewState", "modifyPendingViewState", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;", ExifInterface.GPS_DIRECTION_TRUE, "Lkotlin/Function1;", "block", "withViewState", "(Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "onCleared", "()V", "getViewState", "Lrx/subjects/Subject;", "Ljava/lang/Void;", "unsubscribeSignal", "Lrx/subjects/Subject;", "getUnsubscribeSignal", "()Lrx/subjects/Subject;", "Lrx/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "viewStateSubject", "Lrx/subjects/BehaviorSubject;", "initialViewState", HookHelper.constructorName, "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public abstract class AppViewModel<V> extends ViewModel implements AppComponent {
    private final Subject<Void, Void> unsubscribeSignal;
    private final BehaviorSubject<V> viewStateSubject;

    public AppViewModel() {
        this(null, 1, null);
    }

    public AppViewModel(V v) {
        BehaviorSubject<V> k0 = BehaviorSubject.k0();
        this.viewStateSubject = k0;
        PublishSubject k02 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k02, "PublishSubject.create()");
        this.unsubscribeSignal = k02;
        if (v != null) {
            k0.onNext(v);
        }
    }

    @Override // com.discord.app.AppComponent
    public Subject<Void, Void> getUnsubscribeSignal() {
        return this.unsubscribeSignal;
    }

    public final V getViewState() {
        BehaviorSubject<V> behaviorSubject = this.viewStateSubject;
        m.checkNotNullExpressionValue(behaviorSubject, "viewStateSubject");
        return behaviorSubject.n0();
    }

    public V modifyPendingViewState(V v, V v2) {
        return v2;
    }

    public final Observable<V> observeViewState() {
        BehaviorSubject<V> behaviorSubject = this.viewStateSubject;
        m.checkNotNullExpressionValue(behaviorSubject, "viewStateSubject");
        return behaviorSubject;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        super.onCleared();
        getUnsubscribeSignal().onNext(null);
    }

    public final V requireViewState() {
        V viewState = getViewState();
        m.checkNotNull(viewState);
        return viewState;
    }

    @MainThread
    public void updateViewState(V v) {
        this.viewStateSubject.onNext(modifyPendingViewState(getViewState(), v));
    }

    public final <T> T withViewState(Function1<? super V, ? extends T> function1) {
        m.checkNotNullParameter(function1, "block");
        return function1.invoke(requireViewState());
    }

    public /* synthetic */ AppViewModel(Object obj, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : obj);
    }
}
