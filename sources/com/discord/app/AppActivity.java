package com.discord.app;

import andhook.lib.HookHelper;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.LocaleList;
import android.os.Looper;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.ActivityChooserModel;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.preference.PreferenceManager;
import b.a.d.h;
import b.a.d.i;
import b.a.d.j;
import com.discord.app.AppTransitionActivity;
import com.discord.models.domain.ModelUserSettings;
import com.discord.screenshot_detection.ScreenshotDetector;
import com.discord.stores.StoreStream;
import com.discord.stores.StoreUserSettingsSystem;
import com.discord.utilities.ShareUtils;
import com.discord.utilities.accessibility.AccessibilityMonitor;
import com.discord.utilities.accessibility.AccessibilityUtils;
import com.discord.utilities.analytics.AnalyticsUtils;
import com.discord.utilities.billing.GooglePlayBillingManager;
import com.discord.utilities.bugreports.BugReportManager;
import com.discord.utilities.color.ColorCompat;
import com.discord.utilities.drawable.DrawableCompat;
import com.discord.utilities.font.FontUtils;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.lifecycle.ApplicationProvider;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.views.ToolbarTitleLayout;
import com.discord.widgets.debugging.WidgetFatalCrash;
import com.discord.widgets.share.WidgetIncomingShare;
import com.discord.widgets.tabs.NavigationTab;
import com.discord.widgets.tabs.WidgetTabsHost;
import com.discord.widgets.voice.call.WidgetVoiceCallIncoming;
import com.discord.widgets.voice.fullscreen.WidgetCallFullscreen;
import com.google.android.material.textfield.TextInputLayout;
import d0.z.d.a0;
import d0.z.d.m;
import d0.z.d.o;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;
import xyz.discord.R;
/* compiled from: AppActivity.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000¦\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\f\b\u0016\u0018\u00002\u00020\u00012\u00020\u0002:\u0004vwxyB\u0007¢\u0006\u0004\bu\u0010 J#\u0010\b\u001a\u00020\u00072\u0006\u0010\u0004\u001a\u00020\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005H\u0002¢\u0006\u0004\b\b\u0010\tJ\u001f\u0010\r\u001a\u00020\u00072\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\u0003H\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0017\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u000fH\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\u0019\u0010\u0015\u001a\u00020\u00072\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013H\u0014¢\u0006\u0004\b\u0015\u0010\u0016J\u0015\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u0018\u001a\u00020\u0017¢\u0006\u0004\b\u0019\u0010\u001aJ\u0019\u0010\u001d\u001a\u00020\u00072\b\u0010\u001c\u001a\u0004\u0018\u00010\u001bH\u0014¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u001f\u001a\u00020\u0007H\u0014¢\u0006\u0004\b\u001f\u0010 J\u000f\u0010!\u001a\u00020\u0007H\u0014¢\u0006\u0004\b!\u0010 J\u000f\u0010\"\u001a\u00020\u0007H\u0014¢\u0006\u0004\b\"\u0010 J\u0019\u0010$\u001a\u00020\u00072\b\u0010#\u001a\u0004\u0018\u00010\u0017H\u0014¢\u0006\u0004\b$\u0010\u001aJ\u0019\u0010'\u001a\u00020\u00072\b\u0010&\u001a\u0004\u0018\u00010%H\u0016¢\u0006\u0004\b'\u0010(J#\u0010,\u001a\u00020\u00032\u0014\u0010+\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00020*0)¢\u0006\u0004\b,\u0010-J\u001d\u0010/\u001a\u00020\u00032\u000e\u0010.\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00020*¢\u0006\u0004\b/\u00100J%\u00105\u001a\u0004\u0018\u00010\u00072\b\u00102\u001a\u0004\u0018\u0001012\n\b\u0001\u00104\u001a\u0004\u0018\u000103¢\u0006\u0004\b5\u00106J3\u0010;\u001a\u0004\u0018\u00010:2\b\b\u0002\u00107\u001a\u00020\u00032\n\b\u0003\u00108\u001a\u0004\u0018\u0001032\n\b\u0003\u00109\u001a\u0004\u0018\u000103H\u0007¢\u0006\u0004\b;\u0010<J\u0015\u0010=\u001a\u00020\u00072\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0004\b=\u0010>J\u0019\u0010?\u001a\u00020\u00072\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0005¢\u0006\u0004\b?\u0010>R+\u0010.\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00020@8T@\u0014X\u0094\u0084\u0002¢\u0006\u0012\n\u0004\bA\u0010B\u0012\u0004\bE\u0010 \u001a\u0004\bC\u0010DRJ\u0010K\u001a6\u0012\u0004\u0012\u000203\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00070G0Fj\u001a\u0012\u0004\u0012\u000203\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00070G`H8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\bI\u0010JR.\u0010S\u001a\u0004\u0018\u00010:2\b\u0010L\u001a\u0004\u0018\u00010:8\u0006@FX\u0086\u000e¢\u0006\u0012\n\u0004\bM\u0010N\u001a\u0004\bO\u0010P\"\u0004\bQ\u0010RR\"\u0010Y\u001a\u00020\u00138F@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\bT\u0010U\u001a\u0004\bV\u0010W\"\u0004\bX\u0010\u0016R\u0016\u0010\\\u001a\u00020\n8\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bZ\u0010[R\u0018\u0010`\u001a\u0004\u0018\u00010]8B@\u0002X\u0082\u0004¢\u0006\u0006\u001a\u0004\b^\u0010_R\u0016\u0010c\u001a\u0002038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\ba\u0010bR(\u0010j\u001a\u000e\u0012\u0004\u0012\u00020e\u0012\u0004\u0012\u00020e0d8\u0016@\u0016X\u0096\u0004¢\u0006\f\n\u0004\bf\u0010g\u001a\u0004\bh\u0010iR\u0016\u0010m\u001a\u00020\u00038\u0002@\u0002X\u0082\u000e¢\u0006\u0006\n\u0004\bk\u0010lR\u001f\u0010t\u001a\u00020n8\u0006@\u0006¢\u0006\u0012\n\u0004\bo\u0010p\u0012\u0004\bs\u0010 \u001a\u0004\bq\u0010r¨\u0006z"}, d2 = {"Lcom/discord/app/AppActivity;", "Lb/a/d/d;", "Lcom/discord/app/AppComponent;", "", "keyboardOpen", "Landroid/view/View;", "view", "", "m", "(ZLandroid/view/View;)V", "", "localeString", "refreshIfChanged", "b", "(Ljava/lang/String;Z)V", "Ljava/util/Locale;", "locale", "f", "(Ljava/util/Locale;)Z", "Landroid/content/Intent;", "intent", "onNewIntent", "(Landroid/content/Intent;)V", "Landroid/content/Context;", "context", "j", "(Landroid/content/Context;)V", "Landroid/os/Bundle;", "savedInstanceState", "onCreate", "(Landroid/os/Bundle;)V", "onResume", "()V", "onPause", "onDestroy", "newBase", "attachBaseContext", "Landroid/content/res/Configuration;", "overrideConfiguration", "applyOverrideConfiguration", "(Landroid/content/res/Configuration;)V", "", "Ld0/e0/c;", "screens", "g", "(Ljava/util/List;)Z", "screen", "h", "(Ld0/e0/c;)Z", "", "title", "", "leftDrawable", "l", "(Ljava/lang/CharSequence;Ljava/lang/Integer;)Lkotlin/Unit;", "showHomeAsUp", "iconRes", "iconAccessibilityLabel", "Landroidx/appcompat/widget/Toolbar;", "k", "(ZLjava/lang/Integer;Ljava/lang/Integer;)Landroidx/appcompat/widget/Toolbar;", "showKeyboard", "(Landroid/view/View;)V", "hideKeyboard", "Ljava/lang/Class;", "v", "Lkotlin/Lazy;", "d", "()Ljava/lang/Class;", "getScreen$annotations", "Ljava/util/LinkedHashMap;", "Lkotlin/Function1;", "Lkotlin/collections/LinkedHashMap;", "p", "Ljava/util/LinkedHashMap;", "newIntentListeners", "value", "u", "Landroidx/appcompat/widget/Toolbar;", "getToolbar", "()Landroidx/appcompat/widget/Toolbar;", "n", "(Landroidx/appcompat/widget/Toolbar;)V", "toolbar", "w", "Landroid/content/Intent;", "c", "()Landroid/content/Intent;", "setMostRecentIntent", "mostRecentIntent", "r", "Ljava/lang/String;", "originalLocale", "Lcom/discord/views/ToolbarTitleLayout;", "e", "()Lcom/discord/views/ToolbarTitleLayout;", "toolbarTitleLayout", "q", "I", "originalFontScale", "Lrx/subjects/Subject;", "Ljava/lang/Void;", "t", "Lrx/subjects/Subject;", "getUnsubscribeSignal", "()Lrx/subjects/Subject;", "unsubscribeSignal", "s", "Z", "refreshEnabled", "Lcom/discord/app/AppPermissionsRequests;", "x", "Lcom/discord/app/AppPermissionsRequests;", "getAppPermissions", "()Lcom/discord/app/AppPermissionsRequests;", "getAppPermissions$annotations", "appPermissions", HookHelper.constructorName, "AppAction", "Call", "IncomingCall", "Main", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public class AppActivity extends b.a.d.d implements AppComponent {
    public static boolean m;
    public static final Intent n = new Intent();
    public static boolean o = true;
    public final Subject<Void, Void> t;
    public Toolbar u;
    public final LinkedHashMap<Integer, Function1<Intent, Unit>> p = new LinkedHashMap<>();
    public int q = -1;
    public String r = "";

    /* renamed from: s  reason: collision with root package name */
    public boolean f2062s = true;
    public final Lazy v = d0.g.lazy(new e());
    public Intent w = n;

    /* renamed from: x  reason: collision with root package name */
    public final AppPermissionsRequests f2063x = new h(this);

    /* compiled from: AppActivity.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u000e\u0010\u000fJ\u0019\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0014¢\u0006\u0004\b\u0005\u0010\u0006R%\u0010\r\u001a\n\u0012\u0006\b\u0001\u0012\u00020\b0\u00078T@\u0014X\u0094\u0084\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lcom/discord/app/AppActivity$AppAction;", "Lcom/discord/app/AppActivity;", "Landroid/os/Bundle;", "savedInstanceState", "", "onCreate", "(Landroid/os/Bundle;)V", "Ljava/lang/Class;", "Lcom/discord/app/AppComponent;", "y", "Lkotlin/Lazy;", "d", "()Ljava/lang/Class;", "screen", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class AppAction extends AppActivity {

        /* renamed from: y  reason: collision with root package name */
        public final Lazy f2064y = d0.g.lazy(new a());

        /* compiled from: AppActivity.kt */
        /* loaded from: classes.dex */
        public static final class a extends o implements Function0<Class<? extends AppFragment>> {
            public a() {
                super(0);
            }

            @Override // kotlin.jvm.functions.Function0
            public Class<? extends AppFragment> invoke() {
                int hashCode;
                String action = AppAction.this.c().getAction();
                if (action != null && ((hashCode = action.hashCode()) == -1173264947 ? action.equals("android.intent.action.SEND") : !(hashCode == -1103390587 ? !action.equals("com.discord.intent.action.SDK") : hashCode != -58484670 || !action.equals("android.intent.action.SEND_MULTIPLE")))) {
                    return WidgetIncomingShare.class;
                }
                return WidgetTabsHost.class;
            }
        }

        @Override // com.discord.app.AppActivity
        public Class<? extends AppComponent> d() {
            return (Class) this.f2064y.getValue();
        }

        @Override // com.discord.app.AppActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
        public void onCreate(Bundle bundle) {
            AppActivity.super.onCreate(bundle);
            if (AppActivity.m) {
                finish();
            }
        }
    }

    /* compiled from: AppActivity.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\t\u0010\nR$\u0010\b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u00028\u0014@\u0014X\u0094\u0004¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u000b"}, d2 = {"Lcom/discord/app/AppActivity$Call;", "Lcom/discord/app/AppActivity;", "Ljava/lang/Class;", "Lcom/discord/app/AppComponent;", "y", "Ljava/lang/Class;", "d", "()Ljava/lang/Class;", "screen", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Call extends AppActivity {

        /* renamed from: y  reason: collision with root package name */
        public final Class<? extends AppComponent> f2065y = WidgetCallFullscreen.class;

        @Override // com.discord.app.AppActivity
        public Class<? extends AppComponent> d() {
            return this.f2065y;
        }
    }

    /* compiled from: AppActivity.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\t\u0010\nR$\u0010\b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00030\u00028\u0014@\u0014X\u0094\u0004¢\u0006\f\n\u0004\b\u0004\u0010\u0005\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u000b"}, d2 = {"Lcom/discord/app/AppActivity$IncomingCall;", "Lcom/discord/app/AppActivity;", "Ljava/lang/Class;", "Lcom/discord/app/AppComponent;", "y", "Ljava/lang/Class;", "d", "()Ljava/lang/Class;", "screen", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class IncomingCall extends AppActivity {

        /* renamed from: y  reason: collision with root package name */
        public final Class<? extends AppComponent> f2066y = WidgetVoiceCallIncoming.SystemCallIncoming.class;

        @Override // com.discord.app.AppActivity
        public Class<? extends AppComponent> d() {
            return this.f2066y;
        }
    }

    /* compiled from: AppActivity.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u0002\u0010\u0003¨\u0006\u0004"}, d2 = {"Lcom/discord/app/AppActivity$Main;", "Lcom/discord/app/AppActivity;", HookHelper.constructorName, "()V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes.dex */
    public static final class Main extends AppActivity {
    }

    /* compiled from: AppActivity.kt */
    /* loaded from: classes.dex */
    public static final class b extends o implements Function0<Unit> {
        public static final b j = new b();

        public b() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            if (AppActivity.o) {
                AppActivity.o = false;
                AppLog.i("Application activity initialized.");
                StoreStream.Companion companion = StoreStream.Companion;
                ApplicationProvider applicationProvider = ApplicationProvider.INSTANCE;
                companion.initialize(applicationProvider.get());
                AnalyticsUtils.INSTANCE.initAppOpen(applicationProvider.get());
                AccessibilityMonitor.Companion.initialize(applicationProvider.get());
                ShareUtils.INSTANCE.updateDirectShareTargets(applicationProvider.get());
                GooglePlayBillingManager.INSTANCE.init(applicationProvider.get());
                AppLog appLog = AppLog.g;
                m.checkNotNullParameter(appLog, "logger");
                Application application = applicationProvider.get();
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationProvider.get());
                m.checkNotNullExpressionValue(defaultSharedPreferences, "PreferenceManager.getDef…pplicationProvider.get())");
                ScreenshotDetector.a = new ScreenshotDetector(application, appLog, defaultSharedPreferences);
                BugReportManager.Companion.init();
            }
        }
    }

    /* compiled from: AppActivity.kt */
    /* loaded from: classes.dex */
    public static final class c extends o implements Function1<StoreUserSettingsSystem.Settings, Unit> {
        public c() {
            super(1);
        }

        /* JADX WARN: Removed duplicated region for block: B:20:0x0079  */
        @Override // kotlin.jvm.functions.Function1
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public kotlin.Unit invoke(com.discord.stores.StoreUserSettingsSystem.Settings r8) {
            /*
                r7 = this;
                com.discord.stores.StoreUserSettingsSystem$Settings r8 = (com.discord.stores.StoreUserSettingsSystem.Settings) r8
                java.lang.String r0 = "it"
                d0.z.d.m.checkNotNullParameter(r8, r0)
                com.discord.app.AppActivity r0 = com.discord.app.AppActivity.this
                boolean r1 = com.discord.app.AppActivity.m
                java.util.Objects.requireNonNull(r0)
                java.lang.String r1 = r8.getLocale()
                java.util.Locale r1 = com.discord.models.domain.ModelUserSettings.getLocaleObject(r1)
                java.lang.String r2 = "ModelUserSettings.getLocaleObject(model.locale)"
                d0.z.d.m.checkNotNullExpressionValue(r1, r2)
                boolean r1 = r0.f(r1)
                r2 = 1
                r3 = 0
                if (r1 == 0) goto L2b
                java.lang.String r8 = r8.getLocale()
                r0.b(r8, r2)
                goto L76
            L2b:
                java.lang.String r1 = r8.getTheme()
                b.a.d.c r4 = new b.a.d.c
                r4.<init>(r0)
                android.util.TypedValue r5 = new android.util.TypedValue
                r5.<init>()
                com.discord.app.AppActivity r4 = r4.this$0
                android.content.res.Resources$Theme r4 = r4.getTheme()
                r6 = 2130970240(0x7f040680, float:1.7549185E38)
                r4.resolveAttribute(r6, r5, r2)
                java.lang.CharSequence r4 = r5.string
                boolean r1 = d0.z.d.m.areEqual(r4, r1)
                r1 = r1 ^ r2
                if (r1 != 0) goto L76
                int r8 = r8.getFontScale()
                com.discord.utilities.font.FontUtils r1 = com.discord.utilities.font.FontUtils.INSTANCE
                android.content.ContentResolver r4 = r0.getContentResolver()
                java.lang.String r5 = "contentResolver"
                d0.z.d.m.checkNotNullExpressionValue(r4, r5)
                int r1 = r1.getSystemFontScaleInt(r4)
                r4 = -1
                if (r8 != r4) goto L68
                int r5 = r0.q
                if (r5 != r1) goto L6e
            L68:
                if (r8 == r4) goto L70
                int r0 = r0.q
                if (r0 == r8) goto L70
            L6e:
                r8 = 1
                goto L71
            L70:
                r8 = 0
            L71:
                if (r8 == 0) goto L74
                goto L76
            L74:
                r8 = 0
                goto L77
            L76:
                r8 = 1
            L77:
                if (r8 == 0) goto L7f
                com.discord.app.AppActivity r8 = com.discord.app.AppActivity.this
                r0 = 0
                com.discord.app.AppActivity.i(r8, r3, r2, r0)
            L7f:
                kotlin.Unit r8 = kotlin.Unit.a
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.discord.app.AppActivity.c.invoke(java.lang.Object):java.lang.Object");
        }
    }

    /* compiled from: AppActivity.kt */
    /* loaded from: classes.dex */
    public static final class d extends o implements Function1<ScreenshotDetector.Screenshot, Unit> {
        public d() {
            super(1);
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(ScreenshotDetector.Screenshot screenshot) {
            ScreenshotDetector.Screenshot screenshot2 = screenshot;
            m.checkNotNullParameter(screenshot2, "screenshot");
            BugReportManager.Companion.get().onScreenshot(AppActivity.this, screenshot2);
            return Unit.a;
        }
    }

    /* compiled from: AppActivity.kt */
    /* loaded from: classes.dex */
    public static final class e extends o implements Function0<Class<? extends AppComponent>> {
        public e() {
            super(0);
        }

        @Override // kotlin.jvm.functions.Function0
        public Class<? extends AppComponent> invoke() {
            Intent intent = AppActivity.this.getIntent();
            Class<? extends AppComponent> cls = (Class) (intent != null ? intent.getSerializableExtra("com.discord.intent.extra.EXTRA_SCREEN") : null);
            return cls != null ? cls : WidgetTabsHost.class;
        }
    }

    /* compiled from: AppActivity.kt */
    /* loaded from: classes.dex */
    public static final class f implements Runnable {
        public final /* synthetic */ View j;
        public final /* synthetic */ InputMethodManager k;

        public f(View view, InputMethodManager inputMethodManager) {
            this.j = view;
            this.k = inputMethodManager;
        }

        @Override // java.lang.Runnable
        public final void run() {
            try {
                View view = this.j;
                if ((view instanceof TextInputLayout) && (view = ((TextInputLayout) view).getEditText()) == null) {
                    view = this.j;
                }
                view.requestFocus();
                this.k.showSoftInput(view, 2);
            } catch (Exception e) {
                AppLog.g.d("Error Opening/Closing the Keyboard", e);
            }
        }
    }

    /* compiled from: AppActivity.kt */
    /* loaded from: classes.dex */
    public static final class g implements View.OnClickListener {
        public g() {
        }

        @Override // android.view.View.OnClickListener
        public final void onClick(View view) {
            AppActivity appActivity = AppActivity.this;
            boolean z2 = AppActivity.m;
            appActivity.hideKeyboard(null);
            AppActivity.this.onBackPressed();
        }
    }

    public AppActivity() {
        PublishSubject k0 = PublishSubject.k0();
        m.checkNotNullExpressionValue(k0, "PublishSubject.create()");
        this.t = k0;
        int i = AppPermissionsRequests.a;
        m.checkNotNullParameter(this, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
    }

    public static void i(AppActivity appActivity, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z2 = true;
        }
        if (appActivity.f2062s) {
            appActivity.finish();
            if (z2) {
                AppTransitionActivity.Transition transition = AppTransitionActivity.Transition.TYPE_FADE;
                appActivity.overridePendingTransition(transition.getAnimations().c, transition.getAnimations().d);
                appActivity.getIntent().putExtra("transition", transition);
            }
            j.d(appActivity, appActivity.d(), appActivity.getIntent());
        }
    }

    @Override // android.view.ContextThemeWrapper
    public void applyOverrideConfiguration(Configuration configuration) {
        if (configuration != null) {
            int i = configuration.uiMode;
            Context baseContext = getBaseContext();
            m.checkNotNullExpressionValue(baseContext, "baseContext");
            Resources resources = baseContext.getResources();
            m.checkNotNullExpressionValue(resources, "baseContext.resources");
            configuration.setTo(resources.getConfiguration());
            configuration.uiMode = i;
        }
        super.applyOverrideConfiguration(configuration);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        b.j.invoke2();
        if (context == null) {
            context = this;
        }
        if (!o) {
            Resources resources = context.getResources();
            m.checkNotNullExpressionValue(resources, "oldContext.resources");
            Configuration configuration = resources.getConfiguration();
            float targetFontScaleFloat = FontUtils.INSTANCE.getTargetFontScaleFloat(context);
            configuration.fontScale = targetFontScaleFloat;
            this.q = d0.a0.a.roundToInt(targetFontScaleFloat * 100.0f);
            context = context.createConfigurationContext(configuration);
            m.checkNotNullExpressionValue(context, "oldContext.createConfigurationContext(config)");
        }
        super.attachBaseContext(context);
    }

    public final void b(String str, boolean z2) {
        Locale localeObject = ModelUserSettings.getLocaleObject(str);
        m.checkNotNullExpressionValue(localeObject, "locale");
        if (f(localeObject)) {
            Locale.setDefault(localeObject);
            if (Build.VERSION.SDK_INT >= 24) {
                Resources resources = getResources();
                m.checkNotNullExpressionValue(resources, "resources");
                resources.getConfiguration().setLocale(localeObject);
            } else {
                Resources resources2 = getResources();
                m.checkNotNullExpressionValue(resources2, "resources");
                resources2.getConfiguration().locale = localeObject;
            }
            Resources resources3 = getResources();
            Resources resources4 = getResources();
            m.checkNotNullExpressionValue(resources4, "resources");
            Configuration configuration = resources4.getConfiguration();
            Resources resources5 = getResources();
            m.checkNotNullExpressionValue(resources5, "resources");
            resources3.updateConfiguration(configuration, resources5.getDisplayMetrics());
            if (z2) {
                i(this, false, 1, null);
            }
        }
    }

    public final Intent c() {
        Intent intent = this.w;
        Intent intent2 = n;
        if (intent != intent2) {
            return intent;
        }
        Intent intent3 = getIntent();
        return intent3 != null ? intent3 : new Intent(intent2);
    }

    public Class<? extends AppComponent> d() {
        return (Class) this.v.getValue();
    }

    public final ToolbarTitleLayout e() {
        Toolbar toolbar = this.u;
        ToolbarTitleLayout toolbarTitleLayout = null;
        View childAt = toolbar != null ? toolbar.getChildAt(0) : null;
        if (childAt instanceof ToolbarTitleLayout) {
            toolbarTitleLayout = childAt;
        }
        return toolbarTitleLayout;
    }

    public final boolean f(Locale locale) {
        if (Build.VERSION.SDK_INT >= 24) {
            Resources resources = getResources();
            m.checkNotNullExpressionValue(resources, "resources");
            Configuration configuration = resources.getConfiguration();
            m.checkNotNullExpressionValue(configuration, "resources.configuration");
            LocaleList locales = configuration.getLocales();
            m.checkNotNullExpressionValue(locales, "resources.configuration.locales");
            if (!locales.isEmpty()) {
                Resources resources2 = getResources();
                m.checkNotNullExpressionValue(resources2, "resources");
                Configuration configuration2 = resources2.getConfiguration();
                m.checkNotNullExpressionValue(configuration2, "resources.configuration");
                if (!(!m.areEqual(configuration2.getLocales().get(0), locale))) {
                    return false;
                }
            }
        } else {
            Resources resources3 = getResources();
            m.checkNotNullExpressionValue(resources3, "resources");
            if (resources3.getConfiguration().locale != null) {
                Resources resources4 = getResources();
                m.checkNotNullExpressionValue(resources4, "resources");
                if (!(!m.areEqual(resources4.getConfiguration().locale, locale))) {
                    return false;
                }
            }
        }
        return true;
    }

    public final boolean g(List<? extends d0.e0.c<? extends AppComponent>> list) {
        m.checkNotNullParameter(list, "screens");
        if ((list instanceof Collection) && list.isEmpty()) {
            return false;
        }
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            if (m.areEqual(d0.z.a.getJavaClass((d0.e0.c) it.next()), d())) {
                return true;
            }
        }
        return false;
    }

    @Override // com.discord.app.AppComponent
    public Subject<Void, Void> getUnsubscribeSignal() {
        return this.t;
    }

    public final boolean h(d0.e0.c<? extends AppComponent> cVar) {
        m.checkNotNullParameter(cVar, "screen");
        return m.areEqual(d0.z.a.getJavaClass(cVar), d());
    }

    public final void hideKeyboard(View view) {
        m(false, view);
    }

    public final void j(Context context) {
        m.checkNotNullParameter(context, "context");
        if (!h(a0.getOrCreateKotlinClass(WidgetTabsHost.class))) {
            List<d0.e0.c<? extends AppFragment>> list = j.a;
            m.checkNotNullParameter(context, "context");
            Intent intent = new Intent();
            intent.addFlags(67108864);
            j.c(context, false, intent, 2);
        }
    }

    public final Toolbar k(boolean z2, @DrawableRes Integer num, @StringRes Integer num2) {
        Toolbar toolbar = this.u;
        Drawable drawable = null;
        if (toolbar == null) {
            return null;
        }
        if (z2) {
            int themedDrawableRes$default = DrawableCompat.getThemedDrawableRes$default(toolbar, (int) R.attr.ic_action_bar_back, 0, 2, (Object) null);
            Context context = toolbar.getContext();
            if (num != null) {
                themedDrawableRes$default = num.intValue();
            }
            drawable = ContextCompat.getDrawable(context, themedDrawableRes$default);
            if (drawable != null) {
                androidx.core.graphics.drawable.DrawableCompat.setTint(drawable, ColorCompat.getThemedColor(toolbar, (int) R.attr.colorInteractiveActive));
            }
            toolbar.setNavigationContentDescription(getString(R.string.back));
        }
        toolbar.setNavigationIcon(drawable);
        if (num2 == null) {
            return toolbar;
        }
        toolbar.setNavigationContentDescription(getString(num2.intValue()));
        return toolbar;
    }

    public final Unit l(CharSequence charSequence, @DrawableRes Integer num) {
        ToolbarTitleLayout e2 = e();
        if (e2 == null) {
            return null;
        }
        int i = ToolbarTitleLayout.j;
        e2.a(charSequence, num, null);
        return Unit.a;
    }

    public final void m(boolean z2, View view) {
        IBinder iBinder;
        View childAt;
        Object systemService = getSystemService("input_method");
        Objects.requireNonNull(systemService, "null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
        InputMethodManager inputMethodManager = (InputMethodManager) systemService;
        if (view == null || (iBinder = view.getWindowToken()) == null) {
            FrameLayout frameLayout = (FrameLayout) findViewById(16908290);
            iBinder = (frameLayout == null || (childAt = frameLayout.getChildAt(0)) == null) ? null : childAt.getApplicationWindowToken();
        }
        if (!z2) {
            try {
                inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
            } catch (Exception e2) {
                AppLog.g.d("Error Opening/Closing the Keyboard", e2);
            }
        } else if (view != null) {
            view.postDelayed(new f(view, inputMethodManager), 250L);
        }
    }

    public final void n(Toolbar toolbar) {
        this.u = toolbar;
        if (e() == null) {
            Toolbar toolbar2 = this.u;
            if (toolbar2 != null) {
                toolbar2.addView(new ToolbarTitleLayout(this), 0);
            }
            ToolbarTitleLayout e2 = e();
            if (e2 != null) {
                e2.setBackground(ContextCompat.getDrawable(this, DrawableCompat.getThemedDrawableRes$default(this, (int) R.attr.selectableItemBackground, 0, 2, (Object) null)));
            }
            Toolbar toolbar3 = this.u;
            if (toolbar3 != null) {
                toolbar3.setNavigationOnClickListener(new g());
            }
        }
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a aVar = new a(0, this);
        a aVar2 = new a(1, this);
        try {
            StoreStream.Companion companion = StoreStream.Companion;
            String theme = companion.getUserSettingsSystem().getTheme();
            boolean areEqual = m.areEqual(theme, ModelUserSettings.THEME_LIGHT);
            int i = R.style.AppTheme_Dark;
            if (areEqual) {
                i = R.style.AppTheme_Light;
            } else if (!m.areEqual(theme, ModelUserSettings.THEME_DARK) && m.areEqual(theme, ModelUserSettings.THEME_PURE_EVIL)) {
                i = R.style.AppTheme_Dark_Evil;
            }
            setTheme(i);
            b(companion.getUserSettingsSystem().getLocale(), false);
            aVar.invoke2();
            aVar2.invoke2();
            if (!h(a0.getOrCreateKotlinClass(WidgetTabsHost.class)) || companion.getTabsNavigation().getSelectedTab() != NavigationTab.HOME) {
                companion.getAnalytics().appUiViewed(d());
            }
        } catch (Exception e2) {
            if (!h(a0.getOrCreateKotlinClass(WidgetFatalCrash.class))) {
                WidgetFatalCrash.Companion companion2 = WidgetFatalCrash.Companion;
                String name = d().getName();
                m.checkNotNullExpressionValue(name, "screen.name");
                companion2.launch(this, e2, name);
            }
            finish();
        }
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        this.p.clear();
        super.onDestroy();
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent == null) {
            intent = n;
        }
        this.w = intent;
        m = IntentUtils.INSTANCE.consumeExternalRoutingIntent(c(), this);
        Intent c2 = c();
        for (Map.Entry<Integer, Function1<Intent, Unit>> entry : this.p.entrySet()) {
            entry.getValue().invoke(c2);
        }
    }

    @Override // com.discord.app.AppTransitionActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
        this.t.onNext(null);
    }

    @Override // com.discord.app.AppTransitionActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        j jVar = j.g;
        m.checkNotNullParameter(this, ActivityChooserModel.ATTRIBUTE_ACTIVITY);
        boolean booleanExtra = c().getBooleanExtra("INTENT_RECREATE", false);
        if (booleanExtra) {
            c().removeExtra("INTENT_RECREATE");
            new Handler(Looper.getMainLooper()).post(new i(this));
        }
        if (!booleanExtra) {
            StoreStream.Companion companion = StoreStream.Companion;
            if (!(!m.areEqual(companion.getUserSettingsSystem().getLocale(), this.r)) || !(!m.areEqual(this.r, ""))) {
                this.r = companion.getUserSettingsSystem().getLocale();
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(companion.getUserSettingsSystem().observeSettings(true), this, null, 2, null), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new c());
                ScreenshotDetector screenshotDetector = ScreenshotDetector.a;
                if (screenshotDetector == null) {
                    m.throwUninitializedPropertyAccessException("screenshotDetector");
                }
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(screenshotDetector.f2776b, this, null, 2, null), getClass(), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new d());
                return;
            }
            i(this, false, 1, null);
        }
    }

    public final void showKeyboard(View view) {
        m.checkNotNullParameter(view, "view");
        m(true, view);
    }

    /* compiled from: kotlin-style lambda group */
    /* loaded from: classes2.dex */
    public static final class a extends o implements Function0<Unit> {
        public final /* synthetic */ int j;
        public final /* synthetic */ Object k;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public a(int i, Object obj) {
            super(0);
            this.j = i;
            this.k = obj;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Unit invoke() {
            int i = this.j;
            if (i == 0) {
                invoke2();
                return Unit.a;
            } else if (i == 1) {
                invoke2();
                return Unit.a;
            } else {
                throw null;
            }
        }

        @Override // kotlin.jvm.functions.Function0
        /* renamed from: invoke  reason: avoid collision after fix types in other method */
        public final void invoke2() {
            int i = this.j;
            AppTransitionActivity.a aVar = null;
            boolean z2 = true;
            if (i == 0) {
                AppActivity appActivity = (AppActivity) this.k;
                AppActivity.m = IntentUtils.INSTANCE.consumeExternalRoutingIntent(appActivity.c(), appActivity);
                Intent c = ((AppActivity) this.k).c();
                AppActivity appActivity2 = (AppActivity) this.k;
                Objects.requireNonNull(appActivity2);
                Serializable serializableExtra = c != null ? c.getSerializableExtra("transition") : null;
                if (!(serializableExtra instanceof AppTransitionActivity.Transition)) {
                    serializableExtra = null;
                }
                AppTransitionActivity.Transition transition = (AppTransitionActivity.Transition) serializableExtra;
                if (AccessibilityUtils.INSTANCE.isReducedMotionEnabled()) {
                    transition = AppTransitionActivity.Transition.TYPE_FADE_FAST;
                } else if (transition == null) {
                    j jVar = j.g;
                    transition = appActivity2.g(j.d) ? AppTransitionActivity.Transition.TYPE_SLIDE_HORIZONTAL : null;
                }
                if (transition != null) {
                    aVar = transition.getAnimations();
                }
                appActivity2.k = aVar;
                if (((AppActivity) this.k).getSupportFragmentManager().findFragmentByTag(((AppActivity) this.k).d().getName()) == null) {
                    z2 = false;
                }
                if (!z2) {
                    j jVar2 = j.g;
                    FragmentManager supportFragmentManager = ((AppActivity) this.k).getSupportFragmentManager();
                    AppActivity appActivity3 = (AppActivity) this.k;
                    j.g(jVar2, supportFragmentManager, appActivity3, appActivity3.d(), 0, false, null, null, 120);
                }
            } else if (i == 1) {
                Objects.requireNonNull((AppActivity) this.k);
                AppActivity appActivity4 = (AppActivity) this.k;
                if (appActivity4.u == null) {
                    appActivity4.n((Toolbar) appActivity4.findViewById(R.id.action_bar_toolbar));
                }
            } else {
                throw null;
            }
        }
    }
}
