package com.discord.nullserializable;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import com.discord.nullserializable.NullSerializable;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: NullSerializable.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0000\u0018\u0000*\u0004\b\u0000\u0010\u00012\u0010\u0012\f\u0012\n\u0012\u0006\b\u0000\u0012\u00028\u00000\u00030\u0002B\u001d\u0012\u0006\u0010\n\u001a\u00020\u0007\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002¢\u0006\u0004\b\u000b\u0010\fR\u001c\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u00028\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005R\u0016\u0010\n\u001a\u00020\u00078\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\r"}, d2 = {"Lcom/discord/nullserializable/NullSerializableTypeAdapter;", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/google/gson/TypeAdapter;", "Lcom/discord/nullserializable/NullSerializable;", "b", "Lcom/google/gson/TypeAdapter;", "delegateTypeAdapter", "Lcom/google/gson/Gson;", "a", "Lcom/google/gson/Gson;", "gsonInstance", HookHelper.constructorName, "(Lcom/google/gson/Gson;Lcom/google/gson/TypeAdapter;)V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class NullSerializableTypeAdapter<T> extends TypeAdapter<NullSerializable<? super T>> {
    public final Gson a;

    /* renamed from: b  reason: collision with root package name */
    public final TypeAdapter<T> f2727b;

    public NullSerializableTypeAdapter(Gson gson, TypeAdapter<T> typeAdapter) {
        m.checkNotNullParameter(gson, "gsonInstance");
        m.checkNotNullParameter(typeAdapter, "delegateTypeAdapter");
        this.a = gson;
        this.f2727b = typeAdapter;
    }

    @Override // com.google.gson.TypeAdapter
    public Object read(JsonReader jsonReader) {
        m.checkNotNullParameter(jsonReader, "in");
        T read = this.f2727b.read(jsonReader);
        if (read == null) {
            return new NullSerializable.a(null, 1);
        }
        return new NullSerializable.b(read);
    }

    @Override // com.google.gson.TypeAdapter
    public void write(JsonWriter jsonWriter, Object obj) {
        NullSerializable nullSerializable = (NullSerializable) obj;
        m.checkNotNullParameter(jsonWriter, "out");
        if (nullSerializable instanceof NullSerializable.b) {
            Object a = nullSerializable.a();
            Gson gson = this.a;
            m.checkNotNull(a);
            TypeAdapter<T> i = gson.i(a.getClass());
            Objects.requireNonNull(i, "null cannot be cast to non-null type com.google.gson.TypeAdapter<T>");
            i.write(jsonWriter, a);
        } else if (nullSerializable instanceof NullSerializable.a) {
            boolean z2 = jsonWriter.t;
            jsonWriter.t = true;
            jsonWriter.s();
            jsonWriter.t = z2;
        } else if (nullSerializable == null) {
            boolean z3 = jsonWriter.t;
            jsonWriter.t = false;
            jsonWriter.s();
            jsonWriter.t = z3;
        }
    }
}
