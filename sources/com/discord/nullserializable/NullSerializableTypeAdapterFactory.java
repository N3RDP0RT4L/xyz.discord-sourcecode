package com.discord.nullserializable;

import andhook.lib.HookHelper;
import androidx.exifinterface.media.ExifInterface;
import b.i.d.o;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import d0.z.d.m;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Objects;
import kotlin.Metadata;
/* compiled from: NullSerializable.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\n\u0010\u000bJ3\u0010\b\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0007\"\u0004\b\u0000\u0010\u00022\u0006\u0010\u0004\u001a\u00020\u00032\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0005H\u0016¢\u0006\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lcom/discord/nullserializable/NullSerializableTypeAdapterFactory;", "Lb/i/d/o;", ExifInterface.GPS_DIRECTION_TRUE, "Lcom/google/gson/Gson;", "gson", "Lcom/google/gson/reflect/TypeToken;", "type", "Lcom/google/gson/TypeAdapter;", "create", "(Lcom/google/gson/Gson;Lcom/google/gson/reflect/TypeToken;)Lcom/google/gson/TypeAdapter;", HookHelper.constructorName, "()V", "discord_api"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes.dex */
public final class NullSerializableTypeAdapterFactory implements o {
    @Override // b.i.d.o
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
        m.checkNotNullParameter(gson, "gson");
        m.checkNotNullParameter(typeToken, "type");
        if (!NullSerializable.class.isAssignableFrom(typeToken.getRawType())) {
            return null;
        }
        Type type = typeToken.getType();
        Objects.requireNonNull(type, "null cannot be cast to non-null type java.lang.reflect.ParameterizedType");
        TypeAdapter<T> h = gson.h(TypeToken.get(((ParameterizedType) type).getActualTypeArguments()[0]));
        m.checkNotNullExpressionValue(h, "delegateTypeAdapter");
        return new NullSerializableTypeAdapter(gson, h);
    }
}
