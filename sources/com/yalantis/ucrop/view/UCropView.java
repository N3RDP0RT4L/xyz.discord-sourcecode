package com.yalantis.ucrop.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import b.q.a.l.d;
import b.q.a.l.e;
import com.yalantis.ucrop.R;
import java.util.Objects;
/* loaded from: classes3.dex */
public class UCropView extends FrameLayout {
    public GestureCropImageView j = (GestureCropImageView) findViewById(R.d.image_view_crop);
    public final OverlayView k;

    public UCropView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        LayoutInflater.from(context).inflate(R.e.ucrop_view, (ViewGroup) this, true);
        OverlayView overlayView = (OverlayView) findViewById(R.d.view_overlay);
        this.k = overlayView;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.h.ucrop_UCropView);
        Objects.requireNonNull(overlayView);
        overlayView.u = obtainStyledAttributes.getBoolean(R.h.ucrop_UCropView_ucrop_circle_dimmed_layer, false);
        int color = obtainStyledAttributes.getColor(R.h.ucrop_UCropView_ucrop_dimmed_color, overlayView.getResources().getColor(R.a.ucrop_color_default_dimmed));
        overlayView.v = color;
        overlayView.f3155x.setColor(color);
        overlayView.f3155x.setStyle(Paint.Style.STROKE);
        overlayView.f3155x.setStrokeWidth(1.0f);
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(R.h.ucrop_UCropView_ucrop_frame_stroke_size, overlayView.getResources().getDimensionPixelSize(R.b.ucrop_default_crop_frame_stoke_width));
        int color2 = obtainStyledAttributes.getColor(R.h.ucrop_UCropView_ucrop_frame_color, overlayView.getResources().getColor(R.a.ucrop_color_default_crop_frame));
        overlayView.f3157z.setStrokeWidth(dimensionPixelSize);
        overlayView.f3157z.setColor(color2);
        overlayView.f3157z.setStyle(Paint.Style.STROKE);
        overlayView.A.setStrokeWidth(dimensionPixelSize * 3);
        overlayView.A.setColor(color2);
        overlayView.A.setStyle(Paint.Style.STROKE);
        overlayView.f3154s = obtainStyledAttributes.getBoolean(R.h.ucrop_UCropView_ucrop_show_frame, true);
        int dimensionPixelSize2 = obtainStyledAttributes.getDimensionPixelSize(R.h.ucrop_UCropView_ucrop_grid_stroke_size, overlayView.getResources().getDimensionPixelSize(R.b.ucrop_default_crop_grid_stoke_width));
        int color3 = obtainStyledAttributes.getColor(R.h.ucrop_UCropView_ucrop_grid_color, overlayView.getResources().getColor(R.a.ucrop_color_default_crop_grid));
        overlayView.f3156y.setStrokeWidth(dimensionPixelSize2);
        overlayView.f3156y.setColor(color3);
        overlayView.o = obtainStyledAttributes.getInt(R.h.ucrop_UCropView_ucrop_grid_row_count, 2);
        overlayView.p = obtainStyledAttributes.getInt(R.h.ucrop_UCropView_ucrop_grid_column_count, 2);
        overlayView.t = obtainStyledAttributes.getBoolean(R.h.ucrop_UCropView_ucrop_show_grid, true);
        GestureCropImageView gestureCropImageView = this.j;
        Objects.requireNonNull(gestureCropImageView);
        float abs = Math.abs(obtainStyledAttributes.getFloat(R.h.ucrop_UCropView_ucrop_aspect_ratio_x, 0.0f));
        float abs2 = Math.abs(obtainStyledAttributes.getFloat(R.h.ucrop_UCropView_ucrop_aspect_ratio_y, 0.0f));
        if (abs == 0.0f || abs2 == 0.0f) {
            gestureCropImageView.A = 0.0f;
        } else {
            gestureCropImageView.A = abs / abs2;
        }
        obtainStyledAttributes.recycle();
        this.j.setCropBoundsChangeListener(new d(this));
        overlayView.setOverlayViewChangeListener(new e(this));
    }

    @NonNull
    public GestureCropImageView getCropImageView() {
        return this.j;
    }

    @NonNull
    public OverlayView getOverlayView() {
        return this.k;
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return false;
    }
}
