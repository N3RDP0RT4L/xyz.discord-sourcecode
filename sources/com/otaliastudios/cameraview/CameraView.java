package com.otaliastudios.cameraview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.display.DisplayManager;
import android.location.Location;
import android.media.MediaActionSound;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.vectordrawable.graphics.drawable.PathInterpolatorCompat;
import b.o.a.l;
import b.o.a.m.h;
import b.o.a.m.j;
import b.o.a.m.k;
import b.o.a.m.l;
import b.o.a.m.m;
import b.o.a.n.i;
import b.o.a.q.c;
import b.o.a.q.e;
import b.o.a.q.f;
import b.o.a.q.g;
import b.o.a.r.d;
import b.o.a.r.f;
import b.o.a.s.c;
import b.o.a.u.c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
/* loaded from: classes3.dex */
public class CameraView extends FrameLayout implements LifecycleObserver {
    public static final String j;
    public static final b.o.a.b k;
    public b.o.a.x.b A;
    public MediaActionSound B;
    public b.o.a.s.a C;
    public Lifecycle F;
    @VisibleForTesting
    public e G;
    @VisibleForTesting
    public g H;
    @VisibleForTesting
    public f I;
    @VisibleForTesting
    public d J;
    @VisibleForTesting
    public c K;
    public boolean L;
    public boolean M;
    @VisibleForTesting
    public b.o.a.u.c N;
    public boolean l;
    public boolean m;
    public boolean n;
    public k p;
    public b.o.a.m.d q;
    public b.o.a.o.b r;

    /* renamed from: s  reason: collision with root package name */
    public int f3145s;
    public int t;
    public Handler u;
    public Executor v;
    @VisibleForTesting
    public b w;

    /* renamed from: x  reason: collision with root package name */
    public b.o.a.w.a f3146x;

    /* renamed from: y  reason: collision with root package name */
    public b.o.a.r.f f3147y;

    /* renamed from: z  reason: collision with root package name */
    public i f3148z;
    public HashMap<b.o.a.q.a, b.o.a.q.b> o = new HashMap<>(4);
    @VisibleForTesting
    public List<b.o.a.a> D = new CopyOnWriteArrayList();
    @VisibleForTesting
    public List<b.o.a.p.d> E = new CopyOnWriteArrayList();

    /* loaded from: classes3.dex */
    public class a implements ThreadFactory {
        public final AtomicInteger j = new AtomicInteger(1);

        public a(CameraView cameraView) {
        }

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(@NonNull Runnable runnable) {
            StringBuilder R = b.d.b.a.a.R("FrameExecutor #");
            R.append(this.j.getAndIncrement());
            return new Thread(runnable, R.toString());
        }
    }

    @VisibleForTesting
    /* loaded from: classes3.dex */
    public class b implements i.g, f.c, c.a {
        public final String a;

        /* renamed from: b  reason: collision with root package name */
        public final b.o.a.b f3149b;

        /* loaded from: classes3.dex */
        public class a implements Runnable {
            public final /* synthetic */ float j;
            public final /* synthetic */ PointF[] k;

            public a(float f, PointF[] pointFArr) {
                this.j = f;
                this.k = pointFArr;
            }

            @Override // java.lang.Runnable
            public void run() {
                for (b.o.a.a aVar : CameraView.this.D) {
                    Objects.requireNonNull(aVar);
                }
            }
        }

        /* renamed from: com.otaliastudios.cameraview.CameraView$b$b  reason: collision with other inner class name */
        /* loaded from: classes3.dex */
        public class RunnableC0270b implements Runnable {
            public final /* synthetic */ float j;
            public final /* synthetic */ float[] k;
            public final /* synthetic */ PointF[] l;

            public RunnableC0270b(float f, float[] fArr, PointF[] pointFArr) {
                this.j = f;
                this.k = fArr;
                this.l = pointFArr;
            }

            @Override // java.lang.Runnable
            public void run() {
                for (b.o.a.a aVar : CameraView.this.D) {
                    Objects.requireNonNull(aVar);
                }
            }
        }

        /* loaded from: classes3.dex */
        public class c implements Runnable {
            public final /* synthetic */ b.o.a.p.b j;

            public c(b.o.a.p.b bVar) {
                this.j = bVar;
            }

            @Override // java.lang.Runnable
            public void run() {
                b.this.f3149b.a(0, "dispatchFrame: executing. Passing", Long.valueOf(this.j.a()), "to processors.");
                for (b.o.a.p.d dVar : CameraView.this.E) {
                    try {
                        dVar.a(this.j);
                    } catch (Exception e) {
                        b.this.f3149b.a(2, "Frame processor crashed:", e);
                    }
                }
                this.j.b();
            }
        }

        /* loaded from: classes3.dex */
        public class d implements Runnable {
            public final /* synthetic */ CameraException j;

            public d(CameraException cameraException) {
                this.j = cameraException;
            }

            @Override // java.lang.Runnable
            public void run() {
                for (b.o.a.a aVar : CameraView.this.D) {
                    aVar.b(this.j);
                }
            }
        }

        /* loaded from: classes3.dex */
        public class e implements Runnable {
            public e() {
            }

            @Override // java.lang.Runnable
            public void run() {
                CameraView.this.requestLayout();
            }
        }

        /* loaded from: classes3.dex */
        public class f implements Runnable {
            public final /* synthetic */ PointF j;
            public final /* synthetic */ b.o.a.q.a k;

            public f(PointF pointF, b.o.a.q.a aVar) {
                this.j = pointF;
                this.k = aVar;
            }

            @Override // java.lang.Runnable
            public void run() {
                b.o.a.s.c cVar = CameraView.this.K;
                PointF[] pointFArr = {this.j};
                View view = cVar.j.get(1);
                if (view != null) {
                    view.clearAnimation();
                    PointF pointF = pointFArr[0];
                    view.setTranslationX((int) (pointF.x - (view.getWidth() / 2)));
                    view.setTranslationY((int) (pointF.y - (view.getHeight() / 2)));
                }
                b.o.a.s.a aVar = CameraView.this.C;
                if (aVar != null) {
                    aVar.a(this.k != null ? b.o.a.s.b.GESTURE : b.o.a.s.b.METHOD, this.j);
                }
                for (b.o.a.a aVar2 : CameraView.this.D) {
                    Objects.requireNonNull(aVar2);
                }
            }
        }

        /* loaded from: classes3.dex */
        public class g implements Runnable {
            public final /* synthetic */ boolean j;
            public final /* synthetic */ b.o.a.q.a k;
            public final /* synthetic */ PointF l;

            public g(boolean z2, b.o.a.q.a aVar, PointF pointF) {
                this.j = z2;
                this.k = aVar;
                this.l = pointF;
            }

            @Override // java.lang.Runnable
            public void run() {
                CameraView cameraView;
                boolean z2;
                if (this.j && (z2 = (cameraView = CameraView.this).l) && z2) {
                    if (cameraView.B == null) {
                        cameraView.B = new MediaActionSound();
                    }
                    cameraView.B.play(1);
                }
                b.o.a.s.a aVar = CameraView.this.C;
                if (aVar != null) {
                    aVar.c(this.k != null ? b.o.a.s.b.GESTURE : b.o.a.s.b.METHOD, this.j, this.l);
                }
                for (b.o.a.a aVar2 : CameraView.this.D) {
                    Objects.requireNonNull(aVar2);
                }
            }
        }

        public b() {
            String simpleName = b.class.getSimpleName();
            this.a = simpleName;
            this.f3149b = new b.o.a.b(simpleName);
        }

        public void a(CameraException cameraException) {
            this.f3149b.a(1, "dispatchError", cameraException);
            CameraView.this.u.post(new d(cameraException));
        }

        public void b(@NonNull b.o.a.p.b bVar) {
            this.f3149b.a(0, "dispatchFrame:", Long.valueOf(bVar.a()), "processors:", Integer.valueOf(CameraView.this.E.size()));
            if (CameraView.this.E.isEmpty()) {
                bVar.b();
            } else {
                CameraView.this.v.execute(new c(bVar));
            }
        }

        public void c(float f2, @NonNull float[] fArr, @Nullable PointF[] pointFArr) {
            this.f3149b.a(1, "dispatchOnExposureCorrectionChanged", Float.valueOf(f2));
            CameraView.this.u.post(new RunnableC0270b(f2, fArr, pointFArr));
        }

        public void d(@Nullable b.o.a.q.a aVar, boolean z2, @NonNull PointF pointF) {
            this.f3149b.a(1, "dispatchOnFocusEnd", aVar, Boolean.valueOf(z2), pointF);
            CameraView.this.u.post(new g(z2, aVar, pointF));
        }

        public void e(@Nullable b.o.a.q.a aVar, @NonNull PointF pointF) {
            this.f3149b.a(1, "dispatchOnFocusStart", aVar, pointF);
            CameraView.this.u.post(new f(pointF, aVar));
        }

        public void f(float f2, @Nullable PointF[] pointFArr) {
            this.f3149b.a(1, "dispatchOnZoomChanged", Float.valueOf(f2));
            CameraView.this.u.post(new a(f2, pointFArr));
        }

        @NonNull
        public Context g() {
            return CameraView.this.getContext();
        }

        public void h() {
            b.o.a.x.b C = CameraView.this.f3148z.C(b.o.a.n.t.b.VIEW);
            if (C == null) {
                throw new RuntimeException("Preview stream size should not be null here.");
            } else if (C.equals(CameraView.this.A)) {
                this.f3149b.a(1, "onCameraPreviewStreamSizeChanged:", "swallowing because the preview size has not changed.", C);
            } else {
                this.f3149b.a(1, "onCameraPreviewStreamSizeChanged: posting a requestLayout call.", "Preview stream size:", C);
                CameraView.this.u.post(new e());
            }
        }
    }

    static {
        String simpleName = CameraView.class.getSimpleName();
        j = simpleName;
        k = new b.o.a.b(simpleName);
    }

    public CameraView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
        int i;
        int i2;
        int i3;
        b.o.a.x.c cVar;
        int i4;
        b.o.a.x.c cVar2;
        b.o.a.x.c cVar3;
        b.o.a.o.b bVar;
        boolean isInEditMode = isInEditMode();
        this.M = isInEditMode;
        if (!isInEditMode) {
            setWillNotDraw(false);
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.c.CameraView, 0, 0);
            int integer = obtainStyledAttributes.getInteger(R.c.CameraView_cameraPreview, k.GL_SURFACE.g());
            int i5 = R.c.CameraView_cameraFacing;
            b.o.a.m.e eVar = b.o.a.m.e.BACK;
            if (!b.o.a.e.a(eVar)) {
                b.o.a.m.e eVar2 = b.o.a.m.e.FRONT;
                if (b.o.a.e.a(eVar2)) {
                    eVar = eVar2;
                }
            }
            int integer2 = obtainStyledAttributes.getInteger(i5, eVar.g());
            int integer3 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraFlash, b.o.a.m.f.OFF.g());
            int integer4 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraGrid, b.o.a.m.g.OFF.g());
            int integer5 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraWhiteBalance, m.AUTO.g());
            int integer6 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraMode, b.o.a.m.i.PICTURE.g());
            int integer7 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraHdr, h.OFF.g());
            int integer8 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraAudio, b.o.a.m.a.ON.g());
            int integer9 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraVideoCodec, l.DEVICE_DEFAULT.g());
            int integer10 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraAudioCodec, b.o.a.m.b.DEVICE_DEFAULT.g());
            int integer11 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraEngine, b.o.a.m.d.CAMERA1.g());
            int integer12 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraPictureFormat, j.JPEG.g());
            boolean z2 = obtainStyledAttributes.getBoolean(R.c.CameraView_cameraPlaySounds, true);
            boolean z3 = obtainStyledAttributes.getBoolean(R.c.CameraView_cameraUseDeviceOrientation, true);
            this.L = obtainStyledAttributes.getBoolean(R.c.CameraView_cameraExperimental, false);
            this.n = obtainStyledAttributes.getBoolean(R.c.CameraView_cameraRequestPermissions, true);
            this.p = k.f(integer);
            this.q = b.o.a.m.d.f(integer11);
            int color = obtainStyledAttributes.getColor(R.c.CameraView_cameraGridColor, d.j);
            long j2 = obtainStyledAttributes.getFloat(R.c.CameraView_cameraVideoMaxSize, 0.0f);
            int integer13 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraVideoMaxDuration, 0);
            int integer14 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraVideoBitRate, 0);
            int integer15 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraAudioBitRate, 0);
            float f = obtainStyledAttributes.getFloat(R.c.CameraView_cameraPreviewFrameRate, 0.0f);
            boolean z4 = obtainStyledAttributes.getBoolean(R.c.CameraView_cameraPreviewFrameRateExact, false);
            long integer16 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraAutoFocusResetDelay, PathInterpolatorCompat.MAX_NUM_POINTS);
            boolean z5 = obtainStyledAttributes.getBoolean(R.c.CameraView_cameraPictureMetering, true);
            boolean z6 = obtainStyledAttributes.getBoolean(R.c.CameraView_cameraPictureSnapshotMetering, false);
            int integer17 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraSnapshotMaxWidth, 0);
            int integer18 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraSnapshotMaxHeight, 0);
            int integer19 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraFrameProcessingMaxWidth, 0);
            int integer20 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraFrameProcessingMaxHeight, 0);
            int integer21 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraFrameProcessingFormat, 0);
            int integer22 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraFrameProcessingPoolSize, 2);
            int integer23 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraFrameProcessingExecutors, 1);
            boolean z7 = obtainStyledAttributes.getBoolean(R.c.CameraView_cameraDrawHardwareOverlays, false);
            ArrayList arrayList = new ArrayList(3);
            int i6 = R.c.CameraView_cameraPictureSizeMinWidth;
            if (obtainStyledAttributes.hasValue(i6)) {
                i = integer8;
                i2 = 0;
                arrayList.add(b.i.a.f.e.o.f.N0(obtainStyledAttributes.getInteger(i6, 0)));
            } else {
                i = integer8;
                i2 = 0;
            }
            int i7 = R.c.CameraView_cameraPictureSizeMaxWidth;
            if (obtainStyledAttributes.hasValue(i7)) {
                arrayList.add(b.i.a.f.e.o.f.L0(obtainStyledAttributes.getInteger(i7, i2)));
            }
            int i8 = R.c.CameraView_cameraPictureSizeMinHeight;
            if (obtainStyledAttributes.hasValue(i8)) {
                arrayList.add(b.i.a.f.e.o.f.M0(obtainStyledAttributes.getInteger(i8, i2)));
            }
            int i9 = R.c.CameraView_cameraPictureSizeMaxHeight;
            if (obtainStyledAttributes.hasValue(i9)) {
                arrayList.add(b.i.a.f.e.o.f.K0(obtainStyledAttributes.getInteger(i9, i2)));
            }
            int i10 = R.c.CameraView_cameraPictureSizeMinArea;
            if (obtainStyledAttributes.hasValue(i10)) {
                arrayList.add(b.i.a.f.e.o.f.D1(new b.o.a.x.h(obtainStyledAttributes.getInteger(i10, i2))));
            }
            int i11 = R.c.CameraView_cameraPictureSizeMaxArea;
            if (obtainStyledAttributes.hasValue(i11)) {
                arrayList.add(b.i.a.f.e.o.f.D1(new b.o.a.x.g(obtainStyledAttributes.getInteger(i11, 0))));
            }
            int i12 = R.c.CameraView_cameraPictureSizeAspectRatio;
            if (obtainStyledAttributes.hasValue(i12)) {
                i3 = integer7;
                arrayList.add(b.i.a.f.e.o.f.D1(new b.o.a.x.d(b.o.a.x.a.h(obtainStyledAttributes.getString(i12)).i(), 0.0f)));
            } else {
                i3 = integer7;
            }
            if (obtainStyledAttributes.getBoolean(R.c.CameraView_cameraPictureSizeSmallest, false)) {
                arrayList.add(new b.o.a.x.f());
            }
            if (obtainStyledAttributes.getBoolean(R.c.CameraView_cameraPictureSizeBiggest, false)) {
                arrayList.add(new b.o.a.x.e());
            }
            if (!arrayList.isEmpty()) {
                cVar = b.i.a.f.e.o.f.g((b.o.a.x.c[]) arrayList.toArray(new b.o.a.x.c[0]));
            } else {
                cVar = new b.o.a.x.e();
            }
            ArrayList arrayList2 = new ArrayList(3);
            int i13 = R.c.CameraView_cameraVideoSizeMinWidth;
            if (obtainStyledAttributes.hasValue(i13)) {
                i4 = 0;
                arrayList2.add(b.i.a.f.e.o.f.N0(obtainStyledAttributes.getInteger(i13, 0)));
            } else {
                i4 = 0;
            }
            int i14 = R.c.CameraView_cameraVideoSizeMaxWidth;
            if (obtainStyledAttributes.hasValue(i14)) {
                arrayList2.add(b.i.a.f.e.o.f.L0(obtainStyledAttributes.getInteger(i14, i4)));
            }
            int i15 = R.c.CameraView_cameraVideoSizeMinHeight;
            if (obtainStyledAttributes.hasValue(i15)) {
                arrayList2.add(b.i.a.f.e.o.f.M0(obtainStyledAttributes.getInteger(i15, i4)));
            }
            int i16 = R.c.CameraView_cameraVideoSizeMaxHeight;
            if (obtainStyledAttributes.hasValue(i16)) {
                arrayList2.add(b.i.a.f.e.o.f.K0(obtainStyledAttributes.getInteger(i16, i4)));
            }
            int i17 = R.c.CameraView_cameraVideoSizeMinArea;
            if (obtainStyledAttributes.hasValue(i17)) {
                arrayList2.add(b.i.a.f.e.o.f.D1(new b.o.a.x.h(obtainStyledAttributes.getInteger(i17, i4))));
            }
            int i18 = R.c.CameraView_cameraVideoSizeMaxArea;
            if (obtainStyledAttributes.hasValue(i18)) {
                arrayList2.add(b.i.a.f.e.o.f.D1(new b.o.a.x.g(obtainStyledAttributes.getInteger(i18, 0))));
            }
            int i19 = R.c.CameraView_cameraVideoSizeAspectRatio;
            if (obtainStyledAttributes.hasValue(i19)) {
                cVar2 = cVar;
                arrayList2.add(b.i.a.f.e.o.f.D1(new b.o.a.x.d(b.o.a.x.a.h(obtainStyledAttributes.getString(i19)).i(), 0.0f)));
            } else {
                cVar2 = cVar;
            }
            if (obtainStyledAttributes.getBoolean(R.c.CameraView_cameraVideoSizeSmallest, false)) {
                arrayList2.add(new b.o.a.x.f());
            }
            if (obtainStyledAttributes.getBoolean(R.c.CameraView_cameraVideoSizeBiggest, false)) {
                arrayList2.add(new b.o.a.x.e());
            }
            if (!arrayList2.isEmpty()) {
                cVar3 = b.i.a.f.e.o.f.g((b.o.a.x.c[]) arrayList2.toArray(new b.o.a.x.c[0]));
            } else {
                cVar3 = new b.o.a.x.e();
            }
            int i20 = R.c.CameraView_cameraGestureTap;
            b.o.a.q.b bVar2 = b.o.a.q.b.NONE;
            int integer24 = obtainStyledAttributes.getInteger(i20, bVar2.h());
            int integer25 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraGestureLongTap, bVar2.h());
            int integer26 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraGesturePinch, bVar2.h());
            int integer27 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraGestureScrollHorizontal, bVar2.h());
            int integer28 = obtainStyledAttributes.getInteger(R.c.CameraView_cameraGestureScrollVertical, bVar2.h());
            String string = obtainStyledAttributes.getString(R.c.CameraView_cameraAutoFocusMarker);
            b.o.a.s.a aVar = null;
            if (string != null) {
                try {
                    aVar = (b.o.a.s.a) Class.forName(string).newInstance();
                } catch (Exception unused) {
                }
            }
            try {
                bVar = (b.o.a.o.b) Class.forName(obtainStyledAttributes.getString(R.c.CameraView_cameraFilter)).newInstance();
            } catch (Exception unused2) {
                bVar = new b.o.a.o.c();
            }
            obtainStyledAttributes.recycle();
            this.w = new b();
            this.u = new Handler(Looper.getMainLooper());
            this.G = new e(this.w);
            this.H = new g(this.w);
            this.I = new b.o.a.q.f(this.w);
            this.J = new d(context);
            this.N = new b.o.a.u.c(context);
            this.K = new b.o.a.s.c(context);
            addView(this.J);
            addView(this.K);
            addView(this.N);
            b();
            setPlaySounds(z2);
            setUseDeviceOrientation(z3);
            setGrid(b.o.a.m.g.f(integer4));
            setGridColor(color);
            setDrawHardwareOverlays(z7);
            setFacing(b.o.a.m.e.f(integer2));
            setFlash(b.o.a.m.f.f(integer3));
            setMode(b.o.a.m.i.f(integer6));
            setWhiteBalance(m.f(integer5));
            setHdr(h.f(i3));
            setAudio(b.o.a.m.a.f(i));
            setAudioBitRate(integer15);
            setAudioCodec(b.o.a.m.b.f(integer10));
            setPictureSize(cVar2);
            setPictureMetering(z5);
            setPictureSnapshotMetering(z6);
            setPictureFormat(j.f(integer12));
            setVideoSize(cVar3);
            setVideoCodec(l.f(integer9));
            setVideoMaxSize(j2);
            setVideoMaxDuration(integer13);
            setVideoBitRate(integer14);
            setAutoFocusResetDelay(integer16);
            setPreviewFrameRateExact(z4);
            setPreviewFrameRate(f);
            setSnapshotMaxWidth(integer17);
            setSnapshotMaxHeight(integer18);
            setFrameProcessingMaxWidth(integer19);
            setFrameProcessingMaxHeight(integer20);
            setFrameProcessingFormat(integer21);
            setFrameProcessingPoolSize(integer22);
            setFrameProcessingExecutors(integer23);
            e(b.o.a.q.a.TAP, b.o.a.q.b.f(integer24));
            e(b.o.a.q.a.LONG_TAP, b.o.a.q.b.f(integer25));
            e(b.o.a.q.a.PINCH, b.o.a.q.b.f(integer26));
            e(b.o.a.q.a.SCROLL_HORIZONTAL, b.o.a.q.b.f(integer27));
            e(b.o.a.q.a.SCROLL_VERTICAL, b.o.a.q.b.f(integer28));
            setAutoFocusMarker(aVar);
            setFilter(bVar);
            this.f3147y = new b.o.a.r.f(context, this.w);
        }
    }

    @SuppressLint({"NewApi"})
    public boolean a(@NonNull b.o.a.m.a aVar) {
        b.o.a.m.a aVar2 = b.o.a.m.a.STEREO;
        b.o.a.m.a aVar3 = b.o.a.m.a.MONO;
        b.o.a.m.a aVar4 = b.o.a.m.a.ON;
        if (aVar == aVar4 || aVar == aVar3 || aVar == aVar2) {
            try {
                for (String str : getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 4096).requestedPermissions) {
                    if (!str.equals("android.permission.RECORD_AUDIO")) {
                    }
                }
                throw new IllegalStateException(k.a(3, "Permission error: when audio is enabled (Audio.ON) the RECORD_AUDIO permission should be added to the app manifest file."));
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        if (Build.VERSION.SDK_INT < 23) {
            return true;
        }
        Context context = getContext();
        boolean z2 = aVar == aVar4 || aVar == aVar3 || aVar == aVar2;
        boolean z3 = context.checkSelfPermission("android.permission.CAMERA") != 0;
        boolean z4 = z2 && context.checkSelfPermission("android.permission.RECORD_AUDIO") != 0;
        if (!(z3 || z4)) {
            return true;
        }
        if (this.n) {
            Activity activity = null;
            for (Context context2 = getContext(); context2 instanceof ContextWrapper; context2 = ((ContextWrapper) context2).getBaseContext()) {
                if (context2 instanceof Activity) {
                    activity = (Activity) context2;
                }
            }
            ArrayList arrayList = new ArrayList();
            if (z3) {
                arrayList.add("android.permission.CAMERA");
            }
            if (z4) {
                arrayList.add("android.permission.RECORD_AUDIO");
            }
            if (activity != null) {
                activity.requestPermissions((String[]) arrayList.toArray(new String[0]), 16);
            }
        }
        return false;
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        if (!this.M) {
            Objects.requireNonNull(this.N);
            if (layoutParams instanceof c.a) {
                this.N.addView(view, layoutParams);
                return;
            }
        }
        super.addView(view, i, layoutParams);
    }

    public final void b() {
        i iVar;
        b.o.a.b bVar = k;
        bVar.a(2, "doInstantiateEngine:", "instantiating. engine:", this.q);
        b.o.a.m.d dVar = this.q;
        b bVar2 = this.w;
        if (!this.L || dVar != b.o.a.m.d.CAMERA2) {
            this.q = b.o.a.m.d.CAMERA1;
            iVar = new b.o.a.n.b(bVar2);
        } else {
            iVar = new b.o.a.n.d(bVar2);
        }
        this.f3148z = iVar;
        bVar.a(2, "doInstantiateEngine:", "instantiated. engine:", iVar.getClass().getSimpleName());
        this.f3148z.o0(this.N);
    }

    public final boolean c() {
        i iVar = this.f3148z;
        return iVar.n.f == b.o.a.n.v.e.OFF && !iVar.O();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void close() {
        if (!this.M) {
            b.o.a.r.f fVar = this.f3147y;
            if (fVar.h) {
                fVar.h = false;
                fVar.d.disable();
                ((DisplayManager) fVar.f1947b.getSystemService("display")).unregisterDisplayListener(fVar.f);
                fVar.g = -1;
                fVar.e = -1;
            }
            this.f3148z.L0(false);
            b.o.a.w.a aVar = this.f3146x;
            if (aVar != null) {
                aVar.p();
            }
        }
    }

    public boolean d() {
        b.o.a.n.v.e eVar = this.f3148z.n.f;
        b.o.a.n.v.e eVar2 = b.o.a.n.v.e.ENGINE;
        return eVar.f(eVar2) && this.f3148z.n.g.f(eVar2);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void destroy() {
        if (!this.M) {
            this.D.clear();
            boolean z2 = this.E.size() > 0;
            this.E.clear();
            if (z2) {
                this.f3148z.k0(false);
            }
            this.f3148z.d(true, 0);
            b.o.a.w.a aVar = this.f3146x;
            if (aVar != null) {
                aVar.o();
            }
        }
    }

    public boolean e(@NonNull b.o.a.q.a aVar, @NonNull b.o.a.q.b bVar) {
        b.o.a.q.b bVar2 = b.o.a.q.b.NONE;
        if (aVar.f(bVar)) {
            this.o.put(aVar, bVar);
            int ordinal = aVar.ordinal();
            if (ordinal == 0) {
                this.G.a = this.o.get(b.o.a.q.a.PINCH) != bVar2;
            } else if (ordinal == 1 || ordinal == 2) {
                this.H.a = (this.o.get(b.o.a.q.a.TAP) == bVar2 && this.o.get(b.o.a.q.a.LONG_TAP) == bVar2) ? false : true;
            } else if (ordinal == 3 || ordinal == 4) {
                this.I.a = (this.o.get(b.o.a.q.a.SCROLL_HORIZONTAL) == bVar2 && this.o.get(b.o.a.q.a.SCROLL_VERTICAL) == bVar2) ? false : true;
            }
            this.t = 0;
            Iterator<b.o.a.q.b> it = this.o.values().iterator();
            while (it.hasNext()) {
                this.t += it.next() == bVar2 ? 0 : 1;
            }
            return true;
        }
        e(aVar, bVar2);
        return false;
    }

    public final String f(int i) {
        if (i == Integer.MIN_VALUE) {
            return "AT_MOST";
        }
        if (i == 0) {
            return "UNSPECIFIED";
        }
        if (i != 1073741824) {
            return null;
        }
        return "EXACTLY";
    }

    public final void g(@NonNull b.o.a.q.c cVar, @NonNull b.o.a.c cVar2) {
        b.o.a.q.a aVar = cVar.f1943b;
        PointF[] pointFArr = cVar.c;
        switch (this.o.get(aVar).ordinal()) {
            case 1:
                float width = getWidth();
                float height = getHeight();
                RectF a2 = b.o.a.t.b.a(pointFArr[0], width * 0.05f, 0.05f * height);
                ArrayList arrayList = new ArrayList();
                PointF pointF = new PointF(a2.centerX(), a2.centerY());
                float width2 = a2.width();
                float height2 = a2.height();
                arrayList.add(new b.o.a.t.a(a2, 1000));
                arrayList.add(new b.o.a.t.a(b.o.a.t.b.a(pointF, width2 * 1.5f, height2 * 1.5f), Math.round(1000 * 0.1f)));
                ArrayList arrayList2 = new ArrayList();
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    b.o.a.t.a aVar2 = (b.o.a.t.a) it.next();
                    Objects.requireNonNull(aVar2);
                    RectF rectF = new RectF(0.0f, 0.0f, width, height);
                    RectF rectF2 = new RectF();
                    rectF2.set(Math.max(rectF.left, aVar2.j.left), Math.max(rectF.top, aVar2.j.top), Math.min(rectF.right, aVar2.j.right), Math.min(rectF.bottom, aVar2.j.bottom));
                    arrayList2.add(new b.o.a.t.a(rectF2, aVar2.k));
                }
                this.f3148z.I0(aVar, new b.o.a.t.b(arrayList2), pointFArr[0]);
                return;
            case 2:
                this.f3148z.O0(new l.a());
                return;
            case 3:
                this.f3148z.P0(new l.a());
                return;
            case 4:
                float N = this.f3148z.N();
                float a3 = cVar.a(N, 0.0f, 1.0f);
                if (a3 != N) {
                    this.f3148z.G0(a3, pointFArr, true);
                    return;
                }
                return;
            case 5:
                float k2 = this.f3148z.k();
                float f = cVar2.m;
                float f2 = cVar2.n;
                float a4 = cVar.a(k2, f, f2);
                if (a4 != k2) {
                    this.f3148z.d0(a4, new float[]{f, f2}, pointFArr, true);
                    return;
                }
                return;
            case 6:
                if (getFilter() instanceof b.o.a.o.d) {
                    b.o.a.o.d dVar = (b.o.a.o.d) getFilter();
                    float i = dVar.i();
                    float a5 = cVar.a(i, 0.0f, 1.0f);
                    if (a5 != i) {
                        dVar.d(a5);
                        return;
                    }
                    return;
                }
                return;
            case 7:
                if (getFilter() instanceof b.o.a.o.e) {
                    b.o.a.o.e eVar = (b.o.a.o.e) getFilter();
                    float g = eVar.g();
                    float a6 = cVar.a(g, 0.0f, 1.0f);
                    if (a6 != g) {
                        eVar.b(a6);
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }

    @NonNull
    public b.o.a.m.a getAudio() {
        return this.f3148z.f();
    }

    public int getAudioBitRate() {
        return this.f3148z.g();
    }

    @NonNull
    public b.o.a.m.b getAudioCodec() {
        return this.f3148z.h();
    }

    public long getAutoFocusResetDelay() {
        return this.f3148z.i();
    }

    @Nullable
    public b.o.a.c getCameraOptions() {
        return this.f3148z.j();
    }

    public boolean getDrawHardwareOverlays() {
        return this.N.getHardwareCanvasEnabled();
    }

    @NonNull
    public b.o.a.m.d getEngine() {
        return this.q;
    }

    public float getExposureCorrection() {
        return this.f3148z.k();
    }

    @NonNull
    public b.o.a.m.e getFacing() {
        return this.f3148z.l();
    }

    @NonNull
    public b.o.a.o.b getFilter() {
        b.o.a.w.a aVar = this.f3146x;
        if (aVar == null) {
            return this.r;
        }
        if (aVar instanceof b.o.a.w.b) {
            return ((b.o.a.w.b) aVar).c();
        }
        StringBuilder R = b.d.b.a.a.R("Filters are only supported by the GL_SURFACE preview. Current:");
        R.append(this.p);
        throw new RuntimeException(R.toString());
    }

    @NonNull
    public b.o.a.m.f getFlash() {
        return this.f3148z.m();
    }

    public int getFrameProcessingExecutors() {
        return this.f3145s;
    }

    public int getFrameProcessingFormat() {
        return this.f3148z.n();
    }

    public int getFrameProcessingMaxHeight() {
        return this.f3148z.o();
    }

    public int getFrameProcessingMaxWidth() {
        return this.f3148z.p();
    }

    public int getFrameProcessingPoolSize() {
        return this.f3148z.q();
    }

    @NonNull
    public b.o.a.m.g getGrid() {
        return this.J.getGridMode();
    }

    public int getGridColor() {
        return this.J.getGridColor();
    }

    @NonNull
    public h getHdr() {
        return this.f3148z.r();
    }

    @Nullable
    public Location getLocation() {
        return this.f3148z.s();
    }

    @NonNull
    public b.o.a.m.i getMode() {
        return this.f3148z.t();
    }

    @NonNull
    public j getPictureFormat() {
        return this.f3148z.u();
    }

    public boolean getPictureMetering() {
        return this.f3148z.v();
    }

    @Nullable
    public b.o.a.x.b getPictureSize() {
        return this.f3148z.w(b.o.a.n.t.b.OUTPUT);
    }

    public boolean getPictureSnapshotMetering() {
        return this.f3148z.y();
    }

    public boolean getPlaySounds() {
        return this.l;
    }

    @NonNull
    public k getPreview() {
        return this.p;
    }

    public float getPreviewFrameRate() {
        return this.f3148z.A();
    }

    public boolean getPreviewFrameRateExact() {
        return this.f3148z.B();
    }

    public int getSnapshotMaxHeight() {
        return this.f3148z.D();
    }

    public int getSnapshotMaxWidth() {
        return this.f3148z.E();
    }

    @Nullable
    public b.o.a.x.b getSnapshotSize() {
        b.o.a.x.b bVar = null;
        if (!(getWidth() == 0 || getHeight() == 0)) {
            i iVar = this.f3148z;
            b.o.a.n.t.b bVar2 = b.o.a.n.t.b.VIEW;
            b.o.a.x.b F = iVar.F(bVar2);
            if (F == null) {
                return null;
            }
            Rect L = b.i.a.f.e.o.f.L(F, b.o.a.x.a.f(getWidth(), getHeight()));
            bVar = new b.o.a.x.b(L.width(), L.height());
            if (this.f3148z.e().b(bVar2, b.o.a.n.t.b.OUTPUT)) {
                return bVar.f();
            }
        }
        return bVar;
    }

    public boolean getUseDeviceOrientation() {
        return this.m;
    }

    public int getVideoBitRate() {
        return this.f3148z.G();
    }

    @NonNull
    public b.o.a.m.l getVideoCodec() {
        return this.f3148z.H();
    }

    public int getVideoMaxDuration() {
        return this.f3148z.I();
    }

    public long getVideoMaxSize() {
        return this.f3148z.J();
    }

    @Nullable
    public b.o.a.x.b getVideoSize() {
        return this.f3148z.K(b.o.a.n.t.b.OUTPUT);
    }

    @NonNull
    public m getWhiteBalance() {
        return this.f3148z.M();
    }

    public float getZoom() {
        return this.f3148z.N();
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onAttachedToWindow() {
        b.o.a.w.a aVar;
        super.onAttachedToWindow();
        if (!this.M && this.f3146x == null) {
            b.o.a.b bVar = k;
            bVar.a(2, "doInstantiateEngine:", "instantiating. preview:", this.p);
            k kVar = this.p;
            Context context = getContext();
            int ordinal = kVar.ordinal();
            if (ordinal == 0) {
                aVar = new b.o.a.w.h(context, this);
            } else if (ordinal == 1 && isHardwareAccelerated()) {
                aVar = new b.o.a.w.j(context, this);
            } else {
                this.p = k.GL_SURFACE;
                aVar = new b.o.a.w.d(context, this);
            }
            this.f3146x = aVar;
            bVar.a(2, "doInstantiateEngine:", "instantiated. preview:", aVar.getClass().getSimpleName());
            this.f3148z.u0(this.f3146x);
            b.o.a.o.b bVar2 = this.r;
            if (bVar2 != null) {
                setFilter(bVar2);
                this.r = null;
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    public void onDetachedFromWindow() {
        this.A = null;
        super.onDetachedFromWindow();
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.t > 0;
    }

    @Override // android.widget.FrameLayout, android.view.View
    public void onMeasure(int i, int i2) {
        if (this.M) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i), BasicMeasure.EXACTLY), View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i2), BasicMeasure.EXACTLY));
            return;
        }
        b.o.a.x.b C = this.f3148z.C(b.o.a.n.t.b.VIEW);
        this.A = C;
        if (C == null) {
            k.a(2, "onMeasure:", "surface is not ready. Calling default behavior.");
            super.onMeasure(i, i2);
            return;
        }
        int mode = View.MeasureSpec.getMode(i);
        int mode2 = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        b.o.a.x.b bVar = this.A;
        float f = bVar.j;
        float f2 = bVar.k;
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (!this.f3146x.u()) {
            if (mode == 1073741824) {
                mode = Integer.MIN_VALUE;
            }
            if (mode2 == 1073741824) {
                mode2 = Integer.MIN_VALUE;
            }
        } else {
            if (mode == Integer.MIN_VALUE && layoutParams.width == -1) {
                mode = BasicMeasure.EXACTLY;
            }
            if (mode2 == Integer.MIN_VALUE && layoutParams.height == -1) {
                mode2 = BasicMeasure.EXACTLY;
            }
        }
        b.o.a.b bVar2 = k;
        StringBuilder S = b.d.b.a.a.S("requested dimensions are (", size, "[");
        S.append(f(mode));
        S.append("]x");
        S.append(size2);
        S.append("[");
        S.append(f(mode2));
        S.append("])");
        bVar2.a(1, "onMeasure:", S.toString());
        bVar2.a(1, "onMeasure:", "previewSize is", "(" + f + "x" + f2 + ")");
        if (mode == 1073741824 && mode2 == 1073741824) {
            bVar2.a(1, "onMeasure:", "both are MATCH_PARENT or fixed value. We adapt.", "This means CROP_CENTER.", "(" + size + "x" + size2 + ")");
            super.onMeasure(i, i2);
        } else if (mode == 0 && mode2 == 0) {
            bVar2.a(1, "onMeasure:", "both are completely free.", "We respect that and extend to the whole preview size.", "(" + f + "x" + f2 + ")");
            super.onMeasure(View.MeasureSpec.makeMeasureSpec((int) f, BasicMeasure.EXACTLY), View.MeasureSpec.makeMeasureSpec((int) f2, BasicMeasure.EXACTLY));
        } else {
            float f3 = f2 / f;
            if (mode == 0 || mode2 == 0) {
                if (mode == 0) {
                    size = Math.round(size2 / f3);
                } else {
                    size2 = Math.round(size * f3);
                }
                bVar2.a(1, "onMeasure:", "one dimension was free, we adapted it to fit the ratio.", "(" + size + "x" + size2 + ")");
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, BasicMeasure.EXACTLY), View.MeasureSpec.makeMeasureSpec(size2, BasicMeasure.EXACTLY));
            } else if (mode == 1073741824 || mode2 == 1073741824) {
                if (mode == Integer.MIN_VALUE) {
                    size = Math.min(Math.round(size2 / f3), size);
                } else {
                    size2 = Math.min(Math.round(size * f3), size2);
                }
                bVar2.a(1, "onMeasure:", "one dimension was EXACTLY, another AT_MOST.", "We have TRIED to fit the aspect ratio, but it's not guaranteed.", "(" + size + "x" + size2 + ")");
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, BasicMeasure.EXACTLY), View.MeasureSpec.makeMeasureSpec(size2, BasicMeasure.EXACTLY));
            } else {
                float f4 = size2;
                float f5 = size;
                if (f4 / f5 >= f3) {
                    size2 = Math.round(f5 * f3);
                } else {
                    size = Math.round(f4 / f3);
                }
                bVar2.a(1, "onMeasure:", "both dimension were AT_MOST.", "We fit the preview aspect ratio.", "(" + size + "x" + size2 + ")");
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, BasicMeasure.EXACTLY), View.MeasureSpec.makeMeasureSpec(size2, BasicMeasure.EXACTLY));
            }
        }
    }

    @Override // android.view.View
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!d()) {
            return true;
        }
        b.o.a.c j2 = this.f3148z.j();
        if (j2 != null) {
            e eVar = this.G;
            if (!eVar.a ? false : eVar.c(motionEvent)) {
                k.a(1, "onTouchEvent", "pinch!");
                g(this.G, j2);
            } else {
                b.o.a.q.f fVar = this.I;
                if (!fVar.a ? false : fVar.c(motionEvent)) {
                    k.a(1, "onTouchEvent", "scroll!");
                    g(this.I, j2);
                } else {
                    g gVar = this.H;
                    if (!gVar.a ? false : gVar.c(motionEvent)) {
                        k.a(1, "onTouchEvent", "tap!");
                        g(this.H, j2);
                    }
                }
            }
            return true;
        }
        throw new IllegalStateException("Options should not be null here.");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void open() {
        if (!this.M) {
            b.o.a.w.a aVar = this.f3146x;
            if (aVar != null) {
                aVar.q();
            }
            if (a(getAudio())) {
                b.o.a.r.f fVar = this.f3147y;
                if (!fVar.h) {
                    fVar.h = true;
                    fVar.g = fVar.a();
                    ((DisplayManager) fVar.f1947b.getSystemService("display")).registerDisplayListener(fVar.f, fVar.a);
                    fVar.d.enable();
                }
                b.o.a.n.t.a e = this.f3148z.e();
                int i = this.f3147y.g;
                e.e(i);
                e.d = i;
                e.d();
                this.f3148z.H0();
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.ViewManager
    public void removeView(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!this.M && layoutParams != null) {
            Objects.requireNonNull(this.N);
            if (layoutParams instanceof c.a) {
                this.N.removeView(view);
                return;
            }
        }
        super.removeView(view);
    }

    public void set(@NonNull b.o.a.m.c cVar) {
        if (cVar instanceof b.o.a.m.a) {
            setAudio((b.o.a.m.a) cVar);
        } else if (cVar instanceof b.o.a.m.e) {
            setFacing((b.o.a.m.e) cVar);
        } else if (cVar instanceof b.o.a.m.f) {
            setFlash((b.o.a.m.f) cVar);
        } else if (cVar instanceof b.o.a.m.g) {
            setGrid((b.o.a.m.g) cVar);
        } else if (cVar instanceof h) {
            setHdr((h) cVar);
        } else if (cVar instanceof b.o.a.m.i) {
            setMode((b.o.a.m.i) cVar);
        } else if (cVar instanceof m) {
            setWhiteBalance((m) cVar);
        } else if (cVar instanceof b.o.a.m.l) {
            setVideoCodec((b.o.a.m.l) cVar);
        } else if (cVar instanceof b.o.a.m.b) {
            setAudioCodec((b.o.a.m.b) cVar);
        } else if (cVar instanceof k) {
            setPreview((k) cVar);
        } else if (cVar instanceof b.o.a.m.d) {
            setEngine((b.o.a.m.d) cVar);
        } else if (cVar instanceof j) {
            setPictureFormat((j) cVar);
        }
    }

    public void setAudio(@NonNull b.o.a.m.a aVar) {
        if (aVar == getAudio() || c()) {
            this.f3148z.Z(aVar);
        } else if (a(aVar)) {
            this.f3148z.Z(aVar);
        } else {
            close();
        }
    }

    public void setAudioBitRate(int i) {
        this.f3148z.a0(i);
    }

    public void setAudioCodec(@NonNull b.o.a.m.b bVar) {
        this.f3148z.b0(bVar);
    }

    public void setAutoFocusMarker(@Nullable b.o.a.s.a aVar) {
        View b2;
        this.C = aVar;
        b.o.a.s.c cVar = this.K;
        View view = cVar.j.get(1);
        if (view != null) {
            cVar.removeView(view);
        }
        if (aVar != null && (b2 = aVar.b(cVar.getContext(), cVar)) != null) {
            cVar.j.put(1, b2);
            cVar.addView(b2);
        }
    }

    public void setAutoFocusResetDelay(long j2) {
        this.f3148z.c0(j2);
    }

    public void setDrawHardwareOverlays(boolean z2) {
        this.N.setHardwareCanvasEnabled(z2);
    }

    public void setEngine(@NonNull b.o.a.m.d dVar) {
        if (c()) {
            this.q = dVar;
            i iVar = this.f3148z;
            b();
            b.o.a.w.a aVar = this.f3146x;
            if (aVar != null) {
                this.f3148z.u0(aVar);
            }
            setFacing(iVar.l());
            setFlash(iVar.m());
            setMode(iVar.t());
            setWhiteBalance(iVar.M());
            setHdr(iVar.r());
            setAudio(iVar.f());
            setAudioBitRate(iVar.g());
            setAudioCodec(iVar.h());
            setPictureSize(iVar.x());
            setPictureFormat(iVar.u());
            setVideoSize(iVar.L());
            setVideoCodec(iVar.H());
            setVideoMaxSize(iVar.J());
            setVideoMaxDuration(iVar.I());
            setVideoBitRate(iVar.G());
            setAutoFocusResetDelay(iVar.i());
            setPreviewFrameRate(iVar.A());
            setPreviewFrameRateExact(iVar.B());
            setSnapshotMaxWidth(iVar.E());
            setSnapshotMaxHeight(iVar.D());
            setFrameProcessingMaxWidth(iVar.p());
            setFrameProcessingMaxHeight(iVar.o());
            setFrameProcessingFormat(0);
            setFrameProcessingPoolSize(iVar.q());
            this.f3148z.k0(!this.E.isEmpty());
        }
    }

    public void setExperimental(boolean z2) {
        this.L = z2;
    }

    public void setExposureCorrection(float f) {
        b.o.a.c cameraOptions = getCameraOptions();
        if (cameraOptions != null) {
            float f2 = cameraOptions.m;
            float f3 = cameraOptions.n;
            if (f < f2) {
                f = f2;
            }
            if (f > f3) {
                f = f3;
            }
            this.f3148z.d0(f, new float[]{f2, f3}, null, false);
        }
    }

    public void setFacing(@NonNull b.o.a.m.e eVar) {
        this.f3148z.e0(eVar);
    }

    public void setFilter(@NonNull b.o.a.o.b bVar) {
        b.o.a.w.a aVar = this.f3146x;
        if (aVar == null) {
            this.r = bVar;
            return;
        }
        boolean z2 = aVar instanceof b.o.a.w.b;
        if (!(bVar instanceof b.o.a.o.c) && !z2) {
            StringBuilder R = b.d.b.a.a.R("Filters are only supported by the GL_SURFACE preview. Current preview:");
            R.append(this.p);
            throw new RuntimeException(R.toString());
        } else if (z2) {
            ((b.o.a.w.b) aVar).a(bVar);
        }
    }

    public void setFlash(@NonNull b.o.a.m.f fVar) {
        this.f3148z.f0(fVar);
    }

    public void setFrameProcessingExecutors(int i) {
        if (i >= 1) {
            this.f3145s = i;
            ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(i, i, 4L, TimeUnit.SECONDS, new LinkedBlockingQueue(), new a(this));
            threadPoolExecutor.allowCoreThreadTimeOut(true);
            this.v = threadPoolExecutor;
            return;
        }
        throw new IllegalArgumentException(b.d.b.a.a.p("Need at least 1 executor, got ", i));
    }

    public void setFrameProcessingFormat(int i) {
        this.f3148z.g0(i);
    }

    public void setFrameProcessingMaxHeight(int i) {
        this.f3148z.h0(i);
    }

    public void setFrameProcessingMaxWidth(int i) {
        this.f3148z.i0(i);
    }

    public void setFrameProcessingPoolSize(int i) {
        this.f3148z.j0(i);
    }

    public void setGrid(@NonNull b.o.a.m.g gVar) {
        this.J.setGridMode(gVar);
    }

    public void setGridColor(@ColorInt int i) {
        this.J.setGridColor(i);
    }

    public void setHdr(@NonNull h hVar) {
        this.f3148z.l0(hVar);
    }

    public void setLifecycleOwner(@Nullable LifecycleOwner lifecycleOwner) {
        if (lifecycleOwner == null) {
            Lifecycle lifecycle = this.F;
            if (lifecycle != null) {
                lifecycle.removeObserver(this);
                this.F = null;
                return;
            }
            return;
        }
        Lifecycle lifecycle2 = this.F;
        if (lifecycle2 != null) {
            lifecycle2.removeObserver(this);
            this.F = null;
        }
        Lifecycle lifecycle3 = lifecycleOwner.getLifecycle();
        this.F = lifecycle3;
        lifecycle3.addObserver(this);
    }

    public void setLocation(@Nullable Location location) {
        this.f3148z.m0(location);
    }

    public void setMode(@NonNull b.o.a.m.i iVar) {
        this.f3148z.n0(iVar);
    }

    public void setPictureFormat(@NonNull j jVar) {
        this.f3148z.p0(jVar);
    }

    public void setPictureMetering(boolean z2) {
        this.f3148z.q0(z2);
    }

    public void setPictureSize(@NonNull b.o.a.x.c cVar) {
        this.f3148z.r0(cVar);
    }

    public void setPictureSnapshotMetering(boolean z2) {
        this.f3148z.s0(z2);
    }

    public void setPlaySounds(boolean z2) {
        this.l = z2;
        this.f3148z.t0(z2);
    }

    public void setPreview(@NonNull k kVar) {
        b.o.a.w.a aVar;
        boolean z2 = true;
        if (kVar != this.p) {
            this.p = kVar;
            if (getWindowToken() == null) {
                z2 = false;
            }
            if (!z2 && (aVar = this.f3146x) != null) {
                aVar.o();
                this.f3146x = null;
            }
        }
    }

    public void setPreviewFrameRate(float f) {
        this.f3148z.v0(f);
    }

    public void setPreviewFrameRateExact(boolean z2) {
        this.f3148z.w0(z2);
    }

    public void setPreviewStreamSize(@NonNull b.o.a.x.c cVar) {
        this.f3148z.x0(cVar);
    }

    public void setRequestPermissions(boolean z2) {
        this.n = z2;
    }

    public void setSnapshotMaxHeight(int i) {
        this.f3148z.y0(i);
    }

    public void setSnapshotMaxWidth(int i) {
        this.f3148z.z0(i);
    }

    public void setUseDeviceOrientation(boolean z2) {
        this.m = z2;
    }

    public void setVideoBitRate(int i) {
        this.f3148z.A0(i);
    }

    public void setVideoCodec(@NonNull b.o.a.m.l lVar) {
        this.f3148z.B0(lVar);
    }

    public void setVideoMaxDuration(int i) {
        this.f3148z.C0(i);
    }

    public void setVideoMaxSize(long j2) {
        this.f3148z.D0(j2);
    }

    public void setVideoSize(@NonNull b.o.a.x.c cVar) {
        this.f3148z.E0(cVar);
    }

    public void setWhiteBalance(@NonNull m mVar) {
        this.f3148z.F0(mVar);
    }

    public void setZoom(float f) {
        if (f < 0.0f) {
            f = 0.0f;
        }
        if (f > 1.0f) {
            f = 1.0f;
        }
        this.f3148z.G0(f, null, false);
    }

    @Override // android.widget.FrameLayout, android.view.ViewGroup
    public FrameLayout.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        if (!this.M) {
            b.o.a.u.c cVar = this.N;
            Objects.requireNonNull(cVar);
            boolean z2 = false;
            if (attributeSet != null) {
                TypedArray obtainStyledAttributes = cVar.getContext().obtainStyledAttributes(attributeSet, R.c.CameraView_Layout);
                if (obtainStyledAttributes.hasValue(R.c.CameraView_Layout_layout_drawOnPreview) || obtainStyledAttributes.hasValue(R.c.CameraView_Layout_layout_drawOnPictureSnapshot) || obtainStyledAttributes.hasValue(R.c.CameraView_Layout_layout_drawOnVideoSnapshot)) {
                    z2 = true;
                }
                obtainStyledAttributes.recycle();
            }
            if (z2) {
                return this.N.generateLayoutParams(attributeSet);
            }
        }
        return super.generateLayoutParams(attributeSet);
    }
}
