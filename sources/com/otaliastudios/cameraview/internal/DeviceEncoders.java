package com.otaliastudios.cameraview.internal;
/* loaded from: classes3.dex */
public class DeviceEncoders {

    /* loaded from: classes3.dex */
    public class AudioException extends RuntimeException {
        public final /* synthetic */ DeviceEncoders this$0;
    }

    /* loaded from: classes3.dex */
    public class VideoException extends RuntimeException {
        public final /* synthetic */ DeviceEncoders this$0;
    }
}
