package com.otaliastudios.cameraview;
/* loaded from: classes3.dex */
public final class R {

    /* loaded from: classes3.dex */
    public static final class a {
        public static final int aac = 0x7f0a000e;
        public static final int aacEld = 0x7f0a000f;
        public static final int accessibility_action_clickable_span = 0x7f0a0023;
        public static final int accessibility_custom_action_0 = 0x7f0a0024;
        public static final int accessibility_custom_action_1 = 0x7f0a0025;
        public static final int accessibility_custom_action_10 = 0x7f0a0026;
        public static final int accessibility_custom_action_11 = 0x7f0a0027;
        public static final int accessibility_custom_action_12 = 0x7f0a0028;
        public static final int accessibility_custom_action_13 = 0x7f0a0029;
        public static final int accessibility_custom_action_14 = 0x7f0a002a;
        public static final int accessibility_custom_action_15 = 0x7f0a002b;
        public static final int accessibility_custom_action_16 = 0x7f0a002c;
        public static final int accessibility_custom_action_17 = 0x7f0a002d;
        public static final int accessibility_custom_action_18 = 0x7f0a002e;
        public static final int accessibility_custom_action_19 = 0x7f0a002f;
        public static final int accessibility_custom_action_2 = 0x7f0a0030;
        public static final int accessibility_custom_action_20 = 0x7f0a0031;
        public static final int accessibility_custom_action_21 = 0x7f0a0032;
        public static final int accessibility_custom_action_22 = 0x7f0a0033;
        public static final int accessibility_custom_action_23 = 0x7f0a0034;
        public static final int accessibility_custom_action_24 = 0x7f0a0035;
        public static final int accessibility_custom_action_25 = 0x7f0a0036;
        public static final int accessibility_custom_action_26 = 0x7f0a0037;
        public static final int accessibility_custom_action_27 = 0x7f0a0038;
        public static final int accessibility_custom_action_28 = 0x7f0a0039;
        public static final int accessibility_custom_action_29 = 0x7f0a003a;
        public static final int accessibility_custom_action_3 = 0x7f0a003b;
        public static final int accessibility_custom_action_30 = 0x7f0a003c;
        public static final int accessibility_custom_action_31 = 0x7f0a003d;
        public static final int accessibility_custom_action_4 = 0x7f0a003e;
        public static final int accessibility_custom_action_5 = 0x7f0a003f;
        public static final int accessibility_custom_action_6 = 0x7f0a0040;
        public static final int accessibility_custom_action_7 = 0x7f0a0041;
        public static final int accessibility_custom_action_8 = 0x7f0a0042;
        public static final int accessibility_custom_action_9 = 0x7f0a0043;
        public static final int action_container = 0x7f0a0059;
        public static final int action_divider = 0x7f0a005c;
        public static final int action_image = 0x7f0a005d;
        public static final int action_text = 0x7f0a0066;
        public static final int actions = 0x7f0a0067;
        public static final int async = 0x7f0a00d5;
        public static final int auto = 0x7f0a0131;
        public static final int autoFocus = 0x7f0a0135;
        public static final int back = 0x7f0a0140;
        public static final int blocking = 0x7f0a0172;
        public static final int bottom = 0x7f0a019b;
        public static final int camera1 = 0x7f0a01e0;
        public static final int camera2 = 0x7f0a01e1;
        public static final int chronometer = 0x7f0a03a3;
        public static final int cloudy = 0x7f0a03ba;
        public static final int daylight = 0x7f0a0494;
        public static final int deviceDefault = 0x7f0a04b4;
        public static final int dialog_button = 0x7f0a04b5;
        public static final int dng = 0x7f0a0513;
        public static final int draw3x3 = 0x7f0a051a;
        public static final int draw4x4 = 0x7f0a051b;
        public static final int drawPhi = 0x7f0a051c;
        public static final int end = 0x7f0a0593;
        public static final int exposureCorrection = 0x7f0a05e7;
        public static final int filterControl1 = 0x7f0a062f;
        public static final int filterControl2 = 0x7f0a0630;
        public static final int fluorescent = 0x7f0a0645;
        public static final int focusMarkerContainer = 0x7f0a0647;
        public static final int focusMarkerFill = 0x7f0a0648;
        public static final int forever = 0x7f0a0650;
        public static final int front = 0x7f0a067a;
        public static final int glSurface = 0x7f0a06b2;
        public static final int gl_surface_view = 0x7f0a06b3;
        public static final int h263 = 0x7f0a0852;
        public static final int h264 = 0x7f0a0853;
        public static final int heAac = 0x7f0a0856;
        public static final int icon = 0x7f0a0872;
        public static final int icon_group = 0x7f0a0875;
        public static final int incandescent = 0x7f0a0893;
        public static final int info = 0x7f0a08a9;
        public static final int italic = 0x7f0a08e3;
        public static final int jpeg = 0x7f0a0937;
        public static final int left = 0x7f0a0954;
        public static final int line1 = 0x7f0a0958;
        public static final int line3 = 0x7f0a0959;
        public static final int mono = 0x7f0a0a29;
        public static final int none = 0x7f0a0a7e;
        public static final int normal = 0x7f0a0a7f;
        public static final int notification_background = 0x7f0a0a8a;
        public static final int notification_main_column = 0x7f0a0a8b;
        public static final int notification_main_column_container = 0x7f0a0a8c;
        public static final int off = 0x7f0a0ac4;
        public static final int on = 0x7f0a0ac8;
        public static final int picture = 0x7f0a0b48;
        public static final int right = 0x7f0a0c15;
        public static final int right_icon = 0x7f0a0c17;
        public static final int right_side = 0x7f0a0c18;
        public static final int start = 0x7f0a0e82;
        public static final int stereo = 0x7f0a0e9c;
        public static final int surface = 0x7f0a0ef2;
        public static final int surface_view = 0x7f0a0ef3;
        public static final int surface_view_root = 0x7f0a0ef4;
        public static final int tag_accessibility_actions = 0x7f0a0f18;
        public static final int tag_accessibility_clickable_spans = 0x7f0a0f19;
        public static final int tag_accessibility_heading = 0x7f0a0f1a;
        public static final int tag_accessibility_pane_title = 0x7f0a0f1b;
        public static final int tag_screen_reader_focusable = 0x7f0a0f1f;
        public static final int tag_transition_group = 0x7f0a0f21;
        public static final int tag_unhandled_key_event_manager = 0x7f0a0f22;
        public static final int tag_unhandled_key_listeners = 0x7f0a0f23;
        public static final int takePicture = 0x7f0a0f25;
        public static final int takePictureSnapshot = 0x7f0a0f26;
        public static final int text = 0x7f0a0f30;
        public static final int text2 = 0x7f0a0f32;
        public static final int texture = 0x7f0a0f4d;
        public static final int texture_view = 0x7f0a0f4e;
        public static final int time = 0x7f0a0f90;
        public static final int title = 0x7f0a0f97;
        public static final int top = 0x7f0a0fa9;
        public static final int torch = 0x7f0a0fb0;
        public static final int video = 0x7f0a1079;
        public static final int zoom = 0x7f0a1133;

        private a() {
        }
    }

    /* loaded from: classes3.dex */
    public static final class b {
        public static final int cameraview_gl_view = 0x7f0d0027;
        public static final int cameraview_layout_focus_marker = 0x7f0d0028;
        public static final int cameraview_surface_view = 0x7f0d0029;
        public static final int cameraview_texture_view = 0x7f0d002a;
        public static final int custom_dialog = 0x7f0d003b;
        public static final int notification_action = 0x7f0d00da;
        public static final int notification_action_tombstone = 0x7f0d00db;
        public static final int notification_template_custom_big = 0x7f0d00e3;
        public static final int notification_template_icon_group = 0x7f0d00e4;
        public static final int notification_template_part_chronometer = 0x7f0d00e8;
        public static final int notification_template_part_time = 0x7f0d00e9;

        private b() {
        }
    }

    /* loaded from: classes3.dex */
    public static final class c {
        public static final int CameraView_Layout_layout_drawOnPictureSnapshot = 0x00000000;
        public static final int CameraView_Layout_layout_drawOnPreview = 0x00000001;
        public static final int CameraView_Layout_layout_drawOnVideoSnapshot = 0x00000002;
        public static final int CameraView_cameraAudio = 0x00000000;
        public static final int CameraView_cameraAudioBitRate = 0x00000001;
        public static final int CameraView_cameraAudioCodec = 0x00000002;
        public static final int CameraView_cameraAutoFocusMarker = 0x00000003;
        public static final int CameraView_cameraAutoFocusResetDelay = 0x00000004;
        public static final int CameraView_cameraDrawHardwareOverlays = 0x00000005;
        public static final int CameraView_cameraEngine = 0x00000006;
        public static final int CameraView_cameraExperimental = 0x00000007;
        public static final int CameraView_cameraFacing = 0x00000008;
        public static final int CameraView_cameraFilter = 0x00000009;
        public static final int CameraView_cameraFlash = 0x0000000a;
        public static final int CameraView_cameraFrameProcessingExecutors = 0x0000000b;
        public static final int CameraView_cameraFrameProcessingFormat = 0x0000000c;
        public static final int CameraView_cameraFrameProcessingMaxHeight = 0x0000000d;
        public static final int CameraView_cameraFrameProcessingMaxWidth = 0x0000000e;
        public static final int CameraView_cameraFrameProcessingPoolSize = 0x0000000f;
        public static final int CameraView_cameraGestureLongTap = 0x00000010;
        public static final int CameraView_cameraGesturePinch = 0x00000011;
        public static final int CameraView_cameraGestureScrollHorizontal = 0x00000012;
        public static final int CameraView_cameraGestureScrollVertical = 0x00000013;
        public static final int CameraView_cameraGestureTap = 0x00000014;
        public static final int CameraView_cameraGrid = 0x00000015;
        public static final int CameraView_cameraGridColor = 0x00000016;
        public static final int CameraView_cameraHdr = 0x00000017;
        public static final int CameraView_cameraMode = 0x00000018;
        public static final int CameraView_cameraPictureFormat = 0x00000019;
        public static final int CameraView_cameraPictureMetering = 0x0000001a;
        public static final int CameraView_cameraPictureSizeAspectRatio = 0x0000001b;
        public static final int CameraView_cameraPictureSizeBiggest = 0x0000001c;
        public static final int CameraView_cameraPictureSizeMaxArea = 0x0000001d;
        public static final int CameraView_cameraPictureSizeMaxHeight = 0x0000001e;
        public static final int CameraView_cameraPictureSizeMaxWidth = 0x0000001f;
        public static final int CameraView_cameraPictureSizeMinArea = 0x00000020;
        public static final int CameraView_cameraPictureSizeMinHeight = 0x00000021;
        public static final int CameraView_cameraPictureSizeMinWidth = 0x00000022;
        public static final int CameraView_cameraPictureSizeSmallest = 0x00000023;
        public static final int CameraView_cameraPictureSnapshotMetering = 0x00000024;
        public static final int CameraView_cameraPlaySounds = 0x00000025;
        public static final int CameraView_cameraPreview = 0x00000026;
        public static final int CameraView_cameraPreviewFrameRate = 0x00000027;
        public static final int CameraView_cameraPreviewFrameRateExact = 0x00000028;
        public static final int CameraView_cameraRequestPermissions = 0x00000029;
        public static final int CameraView_cameraSnapshotMaxHeight = 0x0000002a;
        public static final int CameraView_cameraSnapshotMaxWidth = 0x0000002b;
        public static final int CameraView_cameraUseDeviceOrientation = 0x0000002c;
        public static final int CameraView_cameraVideoBitRate = 0x0000002d;
        public static final int CameraView_cameraVideoCodec = 0x0000002e;
        public static final int CameraView_cameraVideoMaxDuration = 0x0000002f;
        public static final int CameraView_cameraVideoMaxSize = 0x00000030;
        public static final int CameraView_cameraVideoSizeAspectRatio = 0x00000031;
        public static final int CameraView_cameraVideoSizeBiggest = 0x00000032;
        public static final int CameraView_cameraVideoSizeMaxArea = 0x00000033;
        public static final int CameraView_cameraVideoSizeMaxHeight = 0x00000034;
        public static final int CameraView_cameraVideoSizeMaxWidth = 0x00000035;
        public static final int CameraView_cameraVideoSizeMinArea = 0x00000036;
        public static final int CameraView_cameraVideoSizeMinHeight = 0x00000037;
        public static final int CameraView_cameraVideoSizeMinWidth = 0x00000038;
        public static final int CameraView_cameraVideoSizeSmallest = 0x00000039;
        public static final int CameraView_cameraWhiteBalance = 0x0000003a;
        public static final int ColorStateListItem_alpha = 0x00000002;
        public static final int ColorStateListItem_android_alpha = 0x00000001;
        public static final int ColorStateListItem_android_color = 0x00000000;
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0x00000000;
        public static final int CoordinatorLayout_Layout_layout_anchor = 0x00000001;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 0x00000002;
        public static final int CoordinatorLayout_Layout_layout_behavior = 0x00000003;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 0x00000004;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 0x00000005;
        public static final int CoordinatorLayout_Layout_layout_keyline = 0x00000006;
        public static final int CoordinatorLayout_keylines = 0x00000000;
        public static final int CoordinatorLayout_statusBarBackground = 0x00000001;
        public static final int FontFamilyFont_android_font = 0x00000000;
        public static final int FontFamilyFont_android_fontStyle = 0x00000002;
        public static final int FontFamilyFont_android_fontVariationSettings = 0x00000004;
        public static final int FontFamilyFont_android_fontWeight = 0x00000001;
        public static final int FontFamilyFont_android_ttcIndex = 0x00000003;
        public static final int FontFamilyFont_font = 0x00000005;
        public static final int FontFamilyFont_fontStyle = 0x00000006;
        public static final int FontFamilyFont_fontVariationSettings = 0x00000007;
        public static final int FontFamilyFont_fontWeight = 0x00000008;
        public static final int FontFamilyFont_ttcIndex = 0x00000009;
        public static final int FontFamily_fontProviderAuthority = 0x00000000;
        public static final int FontFamily_fontProviderCerts = 0x00000001;
        public static final int FontFamily_fontProviderFetchStrategy = 0x00000002;
        public static final int FontFamily_fontProviderFetchTimeout = 0x00000003;
        public static final int FontFamily_fontProviderPackage = 0x00000004;
        public static final int FontFamily_fontProviderQuery = 0x00000005;
        public static final int FontFamily_fontProviderSystemFontFamily = 0x00000006;
        public static final int GradientColorItem_android_color = 0x00000000;
        public static final int GradientColorItem_android_offset = 0x00000001;
        public static final int GradientColor_android_centerColor = 0x00000007;
        public static final int GradientColor_android_centerX = 0x00000003;
        public static final int GradientColor_android_centerY = 0x00000004;
        public static final int GradientColor_android_endColor = 0x00000001;
        public static final int GradientColor_android_endX = 0x0000000a;
        public static final int GradientColor_android_endY = 0x0000000b;
        public static final int GradientColor_android_gradientRadius = 0x00000005;
        public static final int GradientColor_android_startColor = 0x00000000;
        public static final int GradientColor_android_startX = 0x00000008;
        public static final int GradientColor_android_startY = 0x00000009;
        public static final int GradientColor_android_tileMode = 0x00000006;
        public static final int GradientColor_android_type = 0x00000002;
        public static final int[] CameraView = {xyz.discord.R.attr.cameraAudio, xyz.discord.R.attr.cameraAudioBitRate, xyz.discord.R.attr.cameraAudioCodec, xyz.discord.R.attr.cameraAutoFocusMarker, xyz.discord.R.attr.cameraAutoFocusResetDelay, xyz.discord.R.attr.cameraDrawHardwareOverlays, xyz.discord.R.attr.cameraEngine, xyz.discord.R.attr.cameraExperimental, xyz.discord.R.attr.cameraFacing, xyz.discord.R.attr.cameraFilter, xyz.discord.R.attr.cameraFlash, xyz.discord.R.attr.cameraFrameProcessingExecutors, xyz.discord.R.attr.cameraFrameProcessingFormat, xyz.discord.R.attr.cameraFrameProcessingMaxHeight, xyz.discord.R.attr.cameraFrameProcessingMaxWidth, xyz.discord.R.attr.cameraFrameProcessingPoolSize, xyz.discord.R.attr.cameraGestureLongTap, xyz.discord.R.attr.cameraGesturePinch, xyz.discord.R.attr.cameraGestureScrollHorizontal, xyz.discord.R.attr.cameraGestureScrollVertical, xyz.discord.R.attr.cameraGestureTap, xyz.discord.R.attr.cameraGrid, xyz.discord.R.attr.cameraGridColor, xyz.discord.R.attr.cameraHdr, xyz.discord.R.attr.cameraMode, xyz.discord.R.attr.cameraPictureFormat, xyz.discord.R.attr.cameraPictureMetering, xyz.discord.R.attr.cameraPictureSizeAspectRatio, xyz.discord.R.attr.cameraPictureSizeBiggest, xyz.discord.R.attr.cameraPictureSizeMaxArea, xyz.discord.R.attr.cameraPictureSizeMaxHeight, xyz.discord.R.attr.cameraPictureSizeMaxWidth, xyz.discord.R.attr.cameraPictureSizeMinArea, xyz.discord.R.attr.cameraPictureSizeMinHeight, xyz.discord.R.attr.cameraPictureSizeMinWidth, xyz.discord.R.attr.cameraPictureSizeSmallest, xyz.discord.R.attr.cameraPictureSnapshotMetering, xyz.discord.R.attr.cameraPlaySounds, xyz.discord.R.attr.cameraPreview, xyz.discord.R.attr.cameraPreviewFrameRate, xyz.discord.R.attr.cameraPreviewFrameRateExact, xyz.discord.R.attr.cameraRequestPermissions, xyz.discord.R.attr.cameraSnapshotMaxHeight, xyz.discord.R.attr.cameraSnapshotMaxWidth, xyz.discord.R.attr.cameraUseDeviceOrientation, xyz.discord.R.attr.cameraVideoBitRate, xyz.discord.R.attr.cameraVideoCodec, xyz.discord.R.attr.cameraVideoMaxDuration, xyz.discord.R.attr.cameraVideoMaxSize, xyz.discord.R.attr.cameraVideoSizeAspectRatio, xyz.discord.R.attr.cameraVideoSizeBiggest, xyz.discord.R.attr.cameraVideoSizeMaxArea, xyz.discord.R.attr.cameraVideoSizeMaxHeight, xyz.discord.R.attr.cameraVideoSizeMaxWidth, xyz.discord.R.attr.cameraVideoSizeMinArea, xyz.discord.R.attr.cameraVideoSizeMinHeight, xyz.discord.R.attr.cameraVideoSizeMinWidth, xyz.discord.R.attr.cameraVideoSizeSmallest, xyz.discord.R.attr.cameraWhiteBalance};
        public static final int[] CameraView_Layout = {xyz.discord.R.attr.layout_drawOnPictureSnapshot, xyz.discord.R.attr.layout_drawOnPreview, xyz.discord.R.attr.layout_drawOnVideoSnapshot};
        public static final int[] ColorStateListItem = {16843173, 16843551, xyz.discord.R.attr.alpha};
        public static final int[] CoordinatorLayout = {xyz.discord.R.attr.keylines, xyz.discord.R.attr.statusBarBackground};
        public static final int[] CoordinatorLayout_Layout = {16842931, xyz.discord.R.attr.layout_anchor, xyz.discord.R.attr.layout_anchorGravity, xyz.discord.R.attr.layout_behavior, xyz.discord.R.attr.layout_dodgeInsetEdges, xyz.discord.R.attr.layout_insetEdge, xyz.discord.R.attr.layout_keyline};
        public static final int[] FontFamily = {xyz.discord.R.attr.fontProviderAuthority, xyz.discord.R.attr.fontProviderCerts, xyz.discord.R.attr.fontProviderFetchStrategy, xyz.discord.R.attr.fontProviderFetchTimeout, xyz.discord.R.attr.fontProviderPackage, xyz.discord.R.attr.fontProviderQuery, xyz.discord.R.attr.fontProviderSystemFontFamily};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, xyz.discord.R.attr.font, xyz.discord.R.attr.fontStyle, xyz.discord.R.attr.fontVariationSettings, xyz.discord.R.attr.fontWeight, xyz.discord.R.attr.ttcIndex};
        public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
        public static final int[] GradientColorItem = {16843173, 16844052};

        private c() {
        }
    }

    private R() {
    }
}
