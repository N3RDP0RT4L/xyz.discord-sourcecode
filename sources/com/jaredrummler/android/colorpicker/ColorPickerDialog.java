package com.jaredrummler.android.colorpicker;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.FontRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.motion.widget.Key;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.graphics.ColorUtils;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import b.k.a.a.b;
import com.discord.models.domain.ModelAuditLogEntry;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import com.jaredrummler.android.colorpicker.ColorPickerView;
import java.util.Arrays;
import java.util.Locale;
/* loaded from: classes3.dex */
public class ColorPickerDialog extends DialogFragment implements ColorPickerView.c, TextWatcher {
    public static final int[] j = {-769226, -1499549, -54125, -6543440, -10011977, -12627531, -14575885, -16537100, -16728876, -16738680, -11751600, -7617718, -3285959, -5317, -16121, -26624, -8825528, -10453621, -6381922};
    public EditText A;
    public View B;
    public TextView C;
    public Button D;
    public Button E;
    public TextView F;
    public boolean G;
    public int H;
    public boolean I;
    public int J;
    public final View.OnTouchListener K = new b();
    public b.k.a.a.f k;
    public View l;
    public FrameLayout m;
    public int[] n;
    @ColorInt
    public int o;
    public int p;
    public int q;
    public boolean r;

    /* renamed from: s  reason: collision with root package name */
    public int f3115s;
    public b.k.a.a.b t;
    public LinearLayout u;
    public SeekBar v;
    public TextView w;

    /* renamed from: x  reason: collision with root package name */
    public ColorPickerView f3116x;

    /* renamed from: y  reason: collision with root package name */
    public ColorPanelView f3117y;

    /* renamed from: z  reason: collision with root package name */
    public EditText f3118z;

    /* loaded from: classes3.dex */
    public class a implements View.OnLongClickListener {
        public final /* synthetic */ ColorPanelView j;

        public a(ColorPickerDialog colorPickerDialog, ColorPanelView colorPanelView) {
            this.j = colorPanelView;
        }

        @Override // android.view.View.OnLongClickListener
        public boolean onLongClick(View view) {
            this.j.a();
            return true;
        }
    }

    /* loaded from: classes3.dex */
    public class b implements View.OnTouchListener {
        public b() {
        }

        @Override // android.view.View.OnTouchListener
        public boolean onTouch(View view, MotionEvent motionEvent) {
            EditText editText = ColorPickerDialog.this.A;
            if (view == editText || !editText.hasFocus()) {
                return false;
            }
            ColorPickerDialog.this.A.clearFocus();
            ((InputMethodManager) ColorPickerDialog.this.e().getSystemService("input_method")).hideSoftInputFromWindow(ColorPickerDialog.this.A.getWindowToken(), 0);
            ColorPickerDialog.this.A.clearFocus();
            return true;
        }
    }

    /* loaded from: classes3.dex */
    public class c implements View.OnClickListener {
        public c() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            ColorPickerDialog colorPickerDialog = ColorPickerDialog.this;
            ColorPickerDialog.g(colorPickerDialog, colorPickerDialog.o);
            ColorPickerDialog.this.dismiss();
        }
    }

    /* loaded from: classes3.dex */
    public class d implements View.OnClickListener {
        public d() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            ColorPickerDialog colorPickerDialog = ColorPickerDialog.this;
            if (colorPickerDialog.k != null) {
                Log.w("ColorPickerDialog", "Using deprecated listener which may be remove in future releases");
                colorPickerDialog.k.onColorReset(colorPickerDialog.q);
            } else {
                FragmentActivity activity = colorPickerDialog.e();
                if (activity instanceof b.k.a.a.f) {
                    ((b.k.a.a.f) activity).onColorReset(colorPickerDialog.q);
                } else {
                    throw new IllegalStateException("The activity must implement ColorPickerDialogListener");
                }
            }
            ColorPickerDialog.this.dismiss();
        }
    }

    /* loaded from: classes3.dex */
    public class e implements View.OnClickListener {
        public e() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            ColorPickerDialog.this.m.removeAllViews();
            ColorPickerDialog colorPickerDialog = ColorPickerDialog.this;
            int i = colorPickerDialog.p;
            if (i == 0) {
                colorPickerDialog.p = 1;
                Button button = (Button) view;
                int i2 = colorPickerDialog.J;
                if (i2 == 0) {
                    i2 = R.e.cpv_custom;
                }
                button.setText(i2);
                ColorPickerDialog colorPickerDialog2 = ColorPickerDialog.this;
                colorPickerDialog2.m.addView(colorPickerDialog2.j());
            } else if (i == 1) {
                colorPickerDialog.p = 0;
                Button button2 = (Button) view;
                int i3 = colorPickerDialog.H;
                if (i3 == 0) {
                    i3 = R.e.cpv_presets;
                }
                button2.setText(i3);
                ColorPickerDialog colorPickerDialog3 = ColorPickerDialog.this;
                colorPickerDialog3.m.addView(colorPickerDialog3.i());
            }
        }
    }

    /* loaded from: classes3.dex */
    public class f implements View.OnClickListener {
        public f() {
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            int color = ColorPickerDialog.this.f3117y.getColor();
            ColorPickerDialog colorPickerDialog = ColorPickerDialog.this;
            int i = colorPickerDialog.o;
            if (color == i) {
                ColorPickerDialog.g(colorPickerDialog, i);
                ColorPickerDialog.this.dismiss();
            }
        }
    }

    /* loaded from: classes3.dex */
    public class g implements View.OnFocusChangeListener {
        public g() {
        }

        @Override // android.view.View.OnFocusChangeListener
        public void onFocusChange(View view, boolean z2) {
            if (z2) {
                ((InputMethodManager) ColorPickerDialog.this.e().getSystemService("input_method")).showSoftInput(ColorPickerDialog.this.A, 1);
            }
        }
    }

    /* loaded from: classes3.dex */
    public class h implements b.a {
        public h() {
        }
    }

    /* loaded from: classes3.dex */
    public class i implements Runnable {
        public final /* synthetic */ ColorPanelView j;
        public final /* synthetic */ int k;

        public i(ColorPickerDialog colorPickerDialog, ColorPanelView colorPanelView, int i) {
            this.j = colorPanelView;
            this.k = i;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.j.setColor(this.k);
        }
    }

    /* loaded from: classes3.dex */
    public class j implements View.OnClickListener {
        public final /* synthetic */ ColorPanelView j;

        public j(ColorPanelView colorPanelView) {
            this.j = colorPanelView;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (!(view.getTag() instanceof Boolean) || !((Boolean) view.getTag()).booleanValue()) {
                ColorPickerDialog.this.o = this.j.getColor();
                b.k.a.a.b bVar = ColorPickerDialog.this.t;
                bVar.l = -1;
                bVar.notifyDataSetChanged();
                for (int i = 0; i < ColorPickerDialog.this.u.getChildCount(); i++) {
                    FrameLayout frameLayout = (FrameLayout) ColorPickerDialog.this.u.getChildAt(i);
                    ColorPanelView colorPanelView = (ColorPanelView) frameLayout.findViewById(R.c.cpv_color_panel_view);
                    ImageView imageView = (ImageView) frameLayout.findViewById(R.c.cpv_color_image_view);
                    imageView.setImageResource(colorPanelView == view ? R.b.cpv_preset_checked : 0);
                    if ((colorPanelView != view || ColorUtils.calculateLuminance(colorPanelView.getColor()) < 0.65d) && Color.alpha(colorPanelView.getColor()) > 165) {
                        imageView.setColorFilter((ColorFilter) null);
                    } else {
                        imageView.setColorFilter(ViewCompat.MEASURED_STATE_MASK, PorterDuff.Mode.SRC_IN);
                    }
                    colorPanelView.setTag(Boolean.valueOf(colorPanelView == view));
                }
                return;
            }
            ColorPickerDialog colorPickerDialog = ColorPickerDialog.this;
            ColorPickerDialog.g(colorPickerDialog, colorPickerDialog.o);
            ColorPickerDialog.this.dismiss();
        }
    }

    /* loaded from: classes3.dex */
    public static final class k {
        @StringRes
        public int a = R.e.cpv_default_title;
        @StringRes

        /* renamed from: b  reason: collision with root package name */
        public int f3119b = R.e.cpv_presets;
        @StringRes
        public int c = R.e.cpv_custom;
        @StringRes
        public int d = R.e.cpv_select;
        @StringRes
        public int e = 0;
        public int f = 1;
        public int[] g = ColorPickerDialog.j;
        @ColorInt
        public int h = ViewCompat.MEASURED_STATE_MASK;
        public boolean i = false;
        public boolean j = true;
        public boolean k = true;
        public boolean l = true;
        public boolean m = true;
        public int n = 1;
        @ColorInt
        public int o = 0;
        @ColorInt
        public int p = 0;
        @ColorInt
        public int q = 0;
        @ColorInt
        public int r = 0;
        @ColorInt

        /* renamed from: s  reason: collision with root package name */
        public int f3120s = 0;
        @ColorInt
        public int t = 0;
        @DrawableRes
        public int u = 0;
        @ColorInt
        public int v = 0;
        @ColorInt
        public int w = 0;
        @FontRes

        /* renamed from: x  reason: collision with root package name */
        public int f3121x = 0;
        @FontRes

        /* renamed from: y  reason: collision with root package name */
        public int f3122y = 0;
        @FontRes

        /* renamed from: z  reason: collision with root package name */
        public int f3123z = 0;

        public ColorPickerDialog a() {
            ColorPickerDialog colorPickerDialog = new ColorPickerDialog();
            Bundle bundle = new Bundle();
            bundle.putInt(ModelAuditLogEntry.CHANGE_KEY_ID, 0);
            bundle.putInt("dialogType", this.f);
            bundle.putInt(ModelAuditLogEntry.CHANGE_KEY_COLOR, this.h);
            bundle.putIntArray("presets", this.g);
            bundle.putBoolean(Key.ALPHA, this.i);
            bundle.putBoolean("allowCustom", this.k);
            bundle.putBoolean("allowPresets", this.j);
            bundle.putBoolean("allowReset", this.l);
            bundle.putInt("dialogTitle", this.a);
            bundle.putBoolean("showColorShades", this.m);
            bundle.putInt("colorShape", this.n);
            bundle.putInt("presetsButtonText", this.f3119b);
            bundle.putInt("customButtonText", this.c);
            bundle.putInt("customButtonColor", this.o);
            bundle.putInt("customButtonTextColor", this.v);
            bundle.putInt("selectedButtonText", this.d);
            bundle.putInt("selectedButtonColor", this.p);
            bundle.putInt("selectedButtonTextColor", this.w);
            bundle.putInt("resetButtonText", this.e);
            bundle.putInt("buttonFont", this.f3122y);
            bundle.putInt("titleTextColor", this.r);
            bundle.putInt("titleFont", this.f3121x);
            bundle.putInt("dividerColor", this.q);
            bundle.putInt("backgroundColor", this.f3120s);
            bundle.putInt("inputTextColor", this.t);
            bundle.putInt("inputBackground", this.u);
            bundle.putInt("inputFont", this.f3123z);
            colorPickerDialog.setArguments(bundle);
            return colorPickerDialog;
        }
    }

    public static void g(ColorPickerDialog colorPickerDialog, int i2) {
        if (colorPickerDialog.k != null) {
            Log.w("ColorPickerDialog", "Using deprecated listener which may be remove in future releases");
            colorPickerDialog.k.onColorSelected(colorPickerDialog.q, i2);
            return;
        }
        FragmentActivity activity = colorPickerDialog.e();
        if (activity instanceof b.k.a.a.f) {
            ((b.k.a.a.f) activity).onColorSelected(colorPickerDialog.q, i2);
            return;
        }
        throw new IllegalStateException("The activity must implement ColorPickerDialogListener");
    }

    /* JADX WARN: Removed duplicated region for block: B:39:0x011b  */
    /* JADX WARN: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    @Override // android.text.TextWatcher
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void afterTextChanged(android.text.Editable r13) {
        /*
            Method dump skipped, instructions count: 291
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jaredrummler.android.colorpicker.ColorPickerDialog.afterTextChanged(android.text.Editable):void");
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    public void h(@ColorInt int i2) {
        int i3;
        int i4 = 0;
        int[] iArr = {m(i2, 0.9d), m(i2, 0.7d), m(i2, 0.5d), m(i2, 0.333d), m(i2, 0.166d), m(i2, -0.125d), m(i2, -0.25d), m(i2, -0.375d), m(i2, -0.5d), m(i2, -0.675d), m(i2, -0.7d), m(i2, -0.775d)};
        if (this.u.getChildCount() != 0) {
            while (i4 < this.u.getChildCount()) {
                FrameLayout frameLayout = (FrameLayout) this.u.getChildAt(i4);
                ColorPanelView colorPanelView = (ColorPanelView) frameLayout.findViewById(R.c.cpv_color_panel_view);
                colorPanelView.setColor(iArr[i4]);
                colorPanelView.setTag(Boolean.FALSE);
                ((ImageView) frameLayout.findViewById(R.c.cpv_color_image_view)).setImageDrawable(null);
                i4++;
            }
            return;
        }
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.a.cpv_item_horizontal_padding);
        while (i4 < 12) {
            int i5 = iArr[i4];
            if (this.f3115s == 0) {
                i3 = R.d.cpv_color_item_square;
            } else {
                i3 = R.d.cpv_color_item_circle;
            }
            View inflate = View.inflate(e(), i3, null);
            ColorPanelView colorPanelView2 = (ColorPanelView) inflate.findViewById(R.c.cpv_color_panel_view);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) colorPanelView2.getLayoutParams();
            marginLayoutParams.rightMargin = dimensionPixelSize;
            marginLayoutParams.leftMargin = dimensionPixelSize;
            colorPanelView2.setLayoutParams(marginLayoutParams);
            colorPanelView2.setColor(i5);
            this.u.addView(inflate);
            colorPanelView2.post(new i(this, colorPanelView2, i5));
            colorPanelView2.setOnClickListener(new j(colorPanelView2));
            colorPanelView2.setOnLongClickListener(new a(this, colorPanelView2));
            i4++;
        }
    }

    public View i() {
        View inflate = View.inflate(e(), R.d.cpv_dialog_color_picker, null);
        this.f3116x = (ColorPickerView) inflate.findViewById(R.c.cpv_color_picker_view);
        this.f3117y = (ColorPanelView) inflate.findViewById(R.c.cpv_color_panel_new);
        this.A = (EditText) inflate.findViewById(R.c.cpv_hex);
        this.f3118z = (EditText) inflate.findViewById(R.c.cpv_hex_prefix);
        this.B = inflate.findViewById(R.c.cpv_hex_container);
        this.f3116x.setAlphaSliderVisible(this.G);
        this.f3116x.b(this.o, true);
        this.f3117y.setColor(this.o);
        l(this.o);
        if (!this.G) {
            this.A.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        }
        this.f3117y.setOnClickListener(new f());
        inflate.setOnTouchListener(this.K);
        this.f3116x.setOnColorChangedListener(this);
        this.A.addTextChangedListener(this);
        this.A.setOnFocusChangeListener(new g());
        int i2 = getArguments().getInt("inputTextColor", 0);
        if (i2 != 0) {
            this.A.setTextColor(i2);
            this.f3118z.setTextColor(i2);
        }
        int i3 = getArguments().getInt("inputBackground", 0);
        if (i3 != 0) {
            this.B.setBackgroundResource(i3);
        }
        int i4 = getArguments().getInt("inputFont", 0);
        if (i4 != 0) {
            this.A.setTypeface(ResourcesCompat.getFont(requireContext(), i4));
            this.f3118z.setTypeface(ResourcesCompat.getFont(requireContext(), i4));
        }
        return inflate;
    }

    public View j() {
        boolean z2;
        View inflate = View.inflate(e(), R.d.cpv_dialog_presets, null);
        this.u = (LinearLayout) inflate.findViewById(R.c.shades_layout);
        this.v = (SeekBar) inflate.findViewById(R.c.transparency_seekbar);
        this.w = (TextView) inflate.findViewById(R.c.transparency_text);
        GridView gridView = (GridView) inflate.findViewById(R.c.gridView);
        View findViewById = inflate.findViewById(R.c.shades_divider);
        int alpha = Color.alpha(this.o);
        int[] intArray = getArguments().getIntArray("presets");
        this.n = intArray;
        if (intArray == null) {
            this.n = j;
        }
        int[] iArr = this.n;
        boolean z3 = iArr == j;
        this.n = Arrays.copyOf(iArr, iArr.length);
        if (alpha != 255) {
            int i2 = 0;
            while (true) {
                int[] iArr2 = this.n;
                if (i2 >= iArr2.length) {
                    break;
                }
                int i3 = iArr2[i2];
                this.n[i2] = Color.argb(alpha, Color.red(i3), Color.green(i3), Color.blue(i3));
                i2++;
            }
        }
        this.n = n(this.n, this.o);
        int i4 = getArguments().getInt(ModelAuditLogEntry.CHANGE_KEY_COLOR);
        if (i4 != this.o) {
            this.n = n(this.n, i4);
        }
        if (z3) {
            int[] iArr3 = this.n;
            if (iArr3.length == 19) {
                int argb = Color.argb(alpha, 0, 0, 0);
                int length = iArr3.length;
                int i5 = 0;
                while (true) {
                    if (i5 >= length) {
                        z2 = false;
                        break;
                    } else if (iArr3[i5] == argb) {
                        z2 = true;
                        break;
                    } else {
                        i5++;
                    }
                }
                if (!z2) {
                    int length2 = iArr3.length + 1;
                    int[] iArr4 = new int[length2];
                    int i6 = length2 - 1;
                    iArr4[i6] = argb;
                    System.arraycopy(iArr3, 0, iArr4, 0, i6);
                    iArr3 = iArr4;
                }
                this.n = iArr3;
            }
        }
        if (this.r) {
            h(this.o);
            findViewById.setVisibility(0);
            int i7 = getArguments().getInt("dividerColor", 0);
            if (i7 != 0) {
                findViewById.setBackgroundTintList(ColorStateList.valueOf(i7));
            }
        } else {
            this.u.setVisibility(8);
            findViewById.setVisibility(8);
        }
        h hVar = new h();
        int[] iArr5 = this.n;
        int i8 = 0;
        while (true) {
            int[] iArr6 = this.n;
            if (i8 >= iArr6.length) {
                i8 = -1;
                break;
            } else if (iArr6[i8] == this.o) {
                break;
            } else {
                i8++;
            }
        }
        b.k.a.a.b bVar = new b.k.a.a.b(hVar, iArr5, i8, this.f3115s);
        this.t = bVar;
        gridView.setAdapter((ListAdapter) bVar);
        if (this.G) {
            int alpha2 = 255 - Color.alpha(this.o);
            this.v.setMax(255);
            this.v.setProgress(alpha2);
            this.w.setText(String.format(Locale.ENGLISH, "%d%%", Integer.valueOf((int) ((alpha2 * 100.0d) / 255.0d))));
            this.v.setOnSeekBarChangeListener(new b.k.a.a.e(this));
        } else {
            inflate.findViewById(R.c.transparency_layout).setVisibility(8);
            inflate.findViewById(R.c.transparency_title).setVisibility(8);
        }
        return inflate;
    }

    public void k(int i2) {
        this.o = i2;
        ColorPanelView colorPanelView = this.f3117y;
        if (colorPanelView != null) {
            colorPanelView.setColor(i2);
        }
        if (!this.I && this.A != null) {
            l(i2);
            if (this.A.hasFocus()) {
                ((InputMethodManager) e().getSystemService("input_method")).hideSoftInputFromWindow(this.A.getWindowToken(), 0);
                this.A.clearFocus();
            }
        }
        this.I = false;
    }

    public final void l(int i2) {
        if (this.G) {
            this.A.setText(String.format("%08X", Integer.valueOf(i2)));
        } else {
            this.A.setText(String.format("%06X", Integer.valueOf(i2 & ViewCompat.MEASURED_SIZE_MASK)));
        }
    }

    public final int m(@ColorInt int i2, double d2) {
        long parseLong = Long.parseLong(String.format("#%06X", Integer.valueOf(16777215 & i2)).substring(1), 16);
        double d3 = ShadowDrawableWrapper.COS_45;
        int i3 = (d2 > ShadowDrawableWrapper.COS_45 ? 1 : (d2 == ShadowDrawableWrapper.COS_45 ? 0 : -1));
        if (i3 >= 0) {
            d3 = 255.0d;
        }
        if (i3 < 0) {
            d2 *= -1.0d;
        }
        long j2 = parseLong >> 16;
        long j3 = (parseLong >> 8) & 255;
        long j4 = parseLong & 255;
        return Color.argb(Color.alpha(i2), (int) (Math.round((d3 - j2) * d2) + j2), (int) (Math.round((d3 - j3) * d2) + j3), (int) (Math.round((d3 - j4) * d2) + j4));
    }

    public final int[] n(int[] iArr, int i2) {
        boolean z2;
        int length = iArr.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                z2 = false;
                break;
            } else if (iArr[i3] == i2) {
                z2 = true;
                break;
            } else {
                i3++;
            }
        }
        if (z2) {
            return iArr;
        }
        int length2 = iArr.length + 1;
        int[] iArr2 = new int[length2];
        iArr2[0] = i2;
        System.arraycopy(iArr, 0, iArr2, 1, length2 - 1);
        return iArr2;
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        this.q = getArguments().getInt(ModelAuditLogEntry.CHANGE_KEY_ID);
        this.G = getArguments().getBoolean(Key.ALPHA);
        this.r = getArguments().getBoolean("showColorShades");
        this.f3115s = getArguments().getInt("colorShape");
        if (bundle == null) {
            this.o = getArguments().getInt(ModelAuditLogEntry.CHANGE_KEY_COLOR);
            this.p = getArguments().getInt("dialogType");
        } else {
            this.o = bundle.getInt(ModelAuditLogEntry.CHANGE_KEY_COLOR);
            this.p = bundle.getInt("dialogType");
        }
        View inflate = LayoutInflater.from(requireContext()).inflate(R.d.cpv_dialog, (ViewGroup) null);
        this.l = inflate;
        FrameLayout frameLayout = (FrameLayout) inflate.findViewById(R.c.cpv_color_picker_content);
        this.m = frameLayout;
        int i2 = this.p;
        if (i2 == 0) {
            frameLayout.addView(i());
        } else if (i2 == 1) {
            frameLayout.addView(j());
        }
        return new AlertDialog.Builder(requireActivity()).setView(this.l).create();
    }

    @Override // androidx.fragment.app.Fragment
    @Nullable
    public View onCreateView(@NonNull LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        return this.l;
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (this.k != null) {
            Log.w("ColorPickerDialog", "Using deprecated listener which may be remove in future releases");
            this.k.onDialogDismissed(this.q);
            return;
        }
        FragmentActivity activity = e();
        if (activity instanceof b.k.a.a.f) {
            ((b.k.a.a.f) activity).onDialogDismissed(this.q);
        }
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt(ModelAuditLogEntry.CHANGE_KEY_COLOR, this.o);
        bundle.putInt("dialogType", this.p);
        super.onSaveInstanceState(bundle);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        AlertDialog alertDialog = (AlertDialog) getDialog();
        alertDialog.getWindow().clearFlags(131080);
        alertDialog.getWindow().setSoftInputMode(4);
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(@NonNull View view, @Nullable Bundle bundle) {
        int i2;
        super.onViewCreated(view, bundle);
        this.C = (TextView) view.findViewById(R.c.cpv_color_picker_title);
        this.D = (Button) view.findViewById(R.c.cpv_color_picker_custom_button);
        this.E = (Button) view.findViewById(R.c.cpv_color_picker_select_button);
        this.F = (TextView) view.findViewById(R.c.cpv_color_picker_custom_reset);
        int i3 = getArguments().getInt("backgroundColor", 0);
        if (i3 != 0) {
            view.setBackgroundColor(i3);
        }
        int i4 = getArguments().getInt("dialogTitle");
        if (i4 != 0) {
            this.C.setText(i4);
        }
        int i5 = getArguments().getInt("titleTextColor", 0);
        if (i5 != 0) {
            this.C.setTextColor(i5);
        }
        int i6 = getArguments().getInt("selectedButtonText");
        if (i6 == 0) {
            i6 = R.e.cpv_select;
        }
        this.E.setText(i6);
        this.E.setOnClickListener(new c());
        int i7 = getArguments().getInt("resetButtonText");
        if (i7 != 0) {
            this.F.setText(i7);
        }
        this.F.setOnClickListener(new d());
        this.H = getArguments().getInt("presetsButtonText");
        this.J = getArguments().getInt("customButtonText");
        if (this.p == 0 && getArguments().getBoolean("allowPresets")) {
            i2 = this.H;
            if (i2 == 0) {
                i2 = R.e.cpv_presets;
            }
        } else if (this.p != 1 || !getArguments().getBoolean("allowCustom")) {
            i2 = 0;
        } else {
            i2 = this.J;
            if (i2 == 0) {
                i2 = R.e.cpv_custom;
            }
        }
        if (i2 != 0) {
            this.D.setText(i2);
            this.D.setVisibility(0);
        } else {
            this.D.setVisibility(8);
        }
        if (getArguments().getBoolean("allowReset")) {
            this.F.setVisibility(0);
        } else {
            this.F.setVisibility(8);
        }
        int i8 = getArguments().getInt("customButtonColor", 0);
        if (i8 != 0) {
            this.D.setBackgroundTintList(ColorStateList.valueOf(i8));
        }
        int i9 = getArguments().getInt("selectedButtonColor", 0);
        if (i9 != 0) {
            this.E.setBackgroundTintList(ColorStateList.valueOf(i9));
            this.F.setTextColor(i9);
        }
        int i10 = getArguments().getInt("customButtonTextColor", 0);
        if (i10 != 0) {
            this.D.setTextColor(i10);
        }
        int i11 = getArguments().getInt("selectedButtonTextColor", 0);
        if (i11 != 0) {
            this.E.setTextColor(i11);
        }
        int i12 = getArguments().getInt("titleFont", 0);
        if (i12 != 0) {
            this.C.setTypeface(ResourcesCompat.getFont(requireContext(), i12));
        }
        int i13 = getArguments().getInt("buttonFont", 0);
        if (i13 != 0) {
            this.E.setTypeface(ResourcesCompat.getFont(requireContext(), i13));
            this.D.setTypeface(ResourcesCompat.getFont(requireContext(), i13));
            this.F.setTypeface(ResourcesCompat.getFont(requireContext(), i13));
        }
        this.D.setOnClickListener(new e());
    }
}
