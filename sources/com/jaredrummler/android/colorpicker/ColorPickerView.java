package com.jaredrummler.android.colorpicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import androidx.constraintlayout.motion.widget.Key;
import androidx.core.view.ViewCompat;
import b.i.a.f.e.o.f;
/* loaded from: classes3.dex */
public class ColorPickerView extends View {
    public boolean E;
    public String F;
    public int G;
    public int H;
    public Rect I;
    public Rect J;
    public Rect K;
    public Rect L;
    public b.k.a.a.a N;
    public c O;
    public int j;
    public int k;
    public int l;
    public int m;
    public int n;
    public int o;
    public Paint p;
    public Paint q;
    public Paint r;

    /* renamed from: s  reason: collision with root package name */
    public Paint f3124s;
    public Paint t;
    public Paint u;
    public Shader v;
    public Shader w;

    /* renamed from: x  reason: collision with root package name */
    public Shader f3125x;

    /* renamed from: y  reason: collision with root package name */
    public b f3126y;

    /* renamed from: z  reason: collision with root package name */
    public b f3127z;
    public int A = 255;
    public float B = 360.0f;
    public float C = 0.0f;
    public float D = 0.0f;
    public Point M = null;

    /* loaded from: classes3.dex */
    public class b {
        public Canvas a;

        /* renamed from: b  reason: collision with root package name */
        public Bitmap f3128b;
        public float c;

        public b(ColorPickerView colorPickerView, a aVar) {
        }
    }

    /* loaded from: classes3.dex */
    public interface c {
    }

    public ColorPickerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        this.E = false;
        this.F = null;
        this.G = -4342339;
        this.H = -9539986;
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.f.ColorPickerView);
        this.E = obtainStyledAttributes.getBoolean(R.f.ColorPickerView_cpv_alphaChannelVisible, false);
        this.F = obtainStyledAttributes.getString(R.f.ColorPickerView_cpv_alphaChannelText);
        this.G = obtainStyledAttributes.getColor(R.f.ColorPickerView_cpv_sliderColor, -4342339);
        this.H = obtainStyledAttributes.getColor(R.f.ColorPickerView_cpv_borderColor, -9539986);
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(new TypedValue().data, new int[]{16842808});
        if (this.H == -9539986) {
            this.H = obtainStyledAttributes2.getColor(0, -9539986);
        }
        if (this.G == -4342339) {
            this.G = obtainStyledAttributes2.getColor(0, -4342339);
        }
        obtainStyledAttributes2.recycle();
        this.j = f.S(getContext(), 30.0f);
        this.k = f.S(getContext(), 20.0f);
        this.l = f.S(getContext(), 10.0f);
        this.m = f.S(getContext(), 5.0f);
        this.o = f.S(getContext(), 4.0f);
        this.n = f.S(getContext(), 2.0f);
        this.p = new Paint();
        this.q = new Paint();
        this.t = new Paint();
        this.r = new Paint();
        this.f3124s = new Paint();
        this.u = new Paint();
        this.q.setStyle(Paint.Style.STROKE);
        this.q.setStrokeWidth(f.S(getContext(), 2.0f));
        this.q.setAntiAlias(true);
        this.t.setColor(this.G);
        this.t.setStyle(Paint.Style.STROKE);
        this.t.setStrokeWidth(f.S(getContext(), 2.0f));
        this.t.setAntiAlias(true);
        this.f3124s.setColor(-14935012);
        this.f3124s.setTextSize(f.S(getContext(), 14.0f));
        this.f3124s.setAntiAlias(true);
        this.f3124s.setTextAlign(Paint.Align.CENTER);
        this.f3124s.setFakeBoldText(true);
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    private int getPreferredHeight() {
        int S = f.S(getContext(), 200.0f);
        return this.E ? S + this.l + this.k : S;
    }

    private int getPreferredWidth() {
        return f.S(getContext(), 200.0f) + this.j + this.l;
    }

    public final boolean a(MotionEvent motionEvent) {
        float f;
        Point point = this.M;
        int i = 0;
        if (point == null) {
            return false;
        }
        int i2 = point.x;
        int i3 = point.y;
        float f2 = 0.0f;
        if (this.K.contains(i2, i3)) {
            float y2 = motionEvent.getY();
            Rect rect = this.K;
            float height = rect.height();
            float f3 = rect.top;
            if (y2 >= f3) {
                f2 = y2 > ((float) rect.bottom) ? height : y2 - f3;
            }
            this.B = 360.0f - ((f2 * 360.0f) / height);
        } else if (this.J.contains(i2, i3)) {
            float x2 = motionEvent.getX();
            float y3 = motionEvent.getY();
            Rect rect2 = this.J;
            float[] fArr = new float[2];
            float width = rect2.width();
            float height2 = rect2.height();
            float f4 = rect2.left;
            if (x2 < f4) {
                f = 0.0f;
            } else {
                f = x2 > ((float) rect2.right) ? width : x2 - f4;
            }
            float f5 = rect2.top;
            if (y3 >= f5) {
                f2 = y3 > ((float) rect2.bottom) ? height2 : y3 - f5;
            }
            fArr[0] = (1.0f / width) * f;
            fArr[1] = 1.0f - ((1.0f / height2) * f2);
            this.C = fArr[0];
            this.D = fArr[1];
        } else {
            Rect rect3 = this.L;
            if (rect3 == null || !rect3.contains(i2, i3)) {
                return false;
            }
            int x3 = (int) motionEvent.getX();
            Rect rect4 = this.L;
            int width2 = rect4.width();
            int i4 = rect4.left;
            if (x3 >= i4) {
                i = x3 > rect4.right ? width2 : x3 - i4;
            }
            this.A = 255 - ((i * 255) / width2);
        }
        return true;
    }

    public void b(int i, boolean z2) {
        c cVar;
        int alpha = Color.alpha(i);
        float[] fArr = new float[3];
        Color.RGBToHSV(Color.red(i), Color.green(i), Color.blue(i), fArr);
        this.A = alpha;
        float f = fArr[0];
        this.B = f;
        float f2 = fArr[1];
        this.C = f2;
        float f3 = fArr[2];
        this.D = f3;
        if (z2 && (cVar = this.O) != null) {
            ((ColorPickerDialog) cVar).k(Color.HSVToColor(alpha, new float[]{f, f2, f3}));
        }
        invalidate();
    }

    public String getAlphaSliderText() {
        return this.F;
    }

    public int getBorderColor() {
        return this.H;
    }

    public int getColor() {
        return Color.HSVToColor(this.A, new float[]{this.B, this.C, this.D});
    }

    @Override // android.view.View
    public int getPaddingBottom() {
        return Math.max(super.getPaddingBottom(), 0);
    }

    @Override // android.view.View
    public int getPaddingLeft() {
        return Math.max(super.getPaddingLeft(), 0);
    }

    @Override // android.view.View
    public int getPaddingRight() {
        return Math.max(super.getPaddingRight(), 0);
    }

    @Override // android.view.View
    public int getPaddingTop() {
        return Math.max(super.getPaddingTop(), 0);
    }

    public int getSliderTrackerColor() {
        return this.G;
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        Rect rect;
        b bVar;
        b bVar2;
        if (this.I.width() > 0 && this.I.height() > 0) {
            Rect rect2 = this.J;
            this.u.setColor(this.H);
            Rect rect3 = this.I;
            canvas.drawRect(rect3.left, rect3.top, rect2.right + 1, rect2.bottom + 1, this.u);
            if (this.v == null) {
                float f = rect2.left;
                this.v = new LinearGradient(f, rect2.top, f, rect2.bottom, -1, (int) ViewCompat.MEASURED_STATE_MASK, Shader.TileMode.CLAMP);
            }
            b bVar3 = this.f3126y;
            if (bVar3 == null || bVar3.c != this.B) {
                if (bVar3 == null) {
                    this.f3126y = new b(this, null);
                }
                b bVar4 = this.f3126y;
                if (bVar4.f3128b == null) {
                    bVar4.f3128b = Bitmap.createBitmap(rect2.width(), rect2.height(), Bitmap.Config.ARGB_8888);
                }
                b bVar5 = this.f3126y;
                if (bVar5.a == null) {
                    bVar5.a = new Canvas(this.f3126y.f3128b);
                }
                int HSVToColor = Color.HSVToColor(new float[]{this.B, 1.0f, 1.0f});
                float f2 = rect2.top;
                this.w = new LinearGradient(rect2.left, f2, rect2.right, f2, -1, HSVToColor, Shader.TileMode.CLAMP);
                this.p.setShader(new ComposeShader(this.v, this.w, PorterDuff.Mode.MULTIPLY));
                this.f3126y.a.drawRect(0.0f, 0.0f, bVar2.f3128b.getWidth(), this.f3126y.f3128b.getHeight(), this.p);
                this.f3126y.c = this.B;
            }
            canvas.drawBitmap(this.f3126y.f3128b, (Rect) null, rect2, (Paint) null);
            float f3 = this.C;
            float f4 = this.D;
            Rect rect4 = this.J;
            Point point = new Point();
            point.x = (int) ((f3 * rect4.width()) + rect4.left);
            point.y = (int) (((1.0f - f4) * rect4.height()) + rect4.top);
            this.q.setColor(ViewCompat.MEASURED_STATE_MASK);
            canvas.drawCircle(point.x, point.y, this.m - f.S(getContext(), 1.0f), this.q);
            this.q.setColor(-2236963);
            canvas.drawCircle(point.x, point.y, this.m, this.q);
            Rect rect5 = this.K;
            this.u.setColor(this.H);
            canvas.drawRect(rect5.left - 1, rect5.top - 1, rect5.right + 1, rect5.bottom + 1, this.u);
            if (this.f3127z == null) {
                b bVar6 = new b(this, null);
                this.f3127z = bVar6;
                bVar6.f3128b = Bitmap.createBitmap(rect5.width(), rect5.height(), Bitmap.Config.ARGB_8888);
                this.f3127z.a = new Canvas(this.f3127z.f3128b);
                int height = (int) (rect5.height() + 0.5f);
                int[] iArr = new int[height];
                float f5 = 360.0f;
                for (int i = 0; i < height; i++) {
                    iArr[i] = Color.HSVToColor(new float[]{f5, 1.0f, 1.0f});
                    f5 -= 360.0f / height;
                }
                Paint paint = new Paint();
                paint.setStrokeWidth(0.0f);
                for (int i2 = 0; i2 < height; i2++) {
                    paint.setColor(iArr[i2]);
                    float f6 = i2;
                    this.f3127z.a.drawLine(0.0f, f6, bVar.f3128b.getWidth(), f6, paint);
                }
            }
            canvas.drawBitmap(this.f3127z.f3128b, (Rect) null, rect5, (Paint) null);
            float f7 = this.B;
            Rect rect6 = this.K;
            float height2 = rect6.height();
            Point point2 = new Point();
            point2.y = (int) ((height2 - ((f7 * height2) / 360.0f)) + rect6.top);
            point2.x = rect6.left;
            RectF rectF = new RectF();
            int i3 = rect5.left;
            int i4 = this.n;
            rectF.left = i3 - i4;
            rectF.right = rect5.right + i4;
            int i5 = point2.y;
            int i6 = this.o / 2;
            rectF.top = i5 - i6;
            rectF.bottom = i6 + i5;
            canvas.drawRoundRect(rectF, 2.0f, 2.0f, this.t);
            if (!(!this.E || (rect = this.L) == null || this.N == null)) {
                this.u.setColor(this.H);
                canvas.drawRect(rect.left - 1, rect.top - 1, rect.right + 1, rect.bottom + 1, this.u);
                this.N.draw(canvas);
                float[] fArr = {this.B, this.C, this.D};
                int HSVToColor2 = Color.HSVToColor(fArr);
                int HSVToColor3 = Color.HSVToColor(0, fArr);
                float f8 = rect.top;
                LinearGradient linearGradient = new LinearGradient(rect.left, f8, rect.right, f8, HSVToColor2, HSVToColor3, Shader.TileMode.CLAMP);
                this.f3125x = linearGradient;
                this.r.setShader(linearGradient);
                canvas.drawRect(rect, this.r);
                String str = this.F;
                if (str != null && !str.equals("")) {
                    canvas.drawText(this.F, rect.centerX(), f.S(getContext(), 4.0f) + rect.centerY(), this.f3124s);
                }
                int i7 = this.A;
                Rect rect7 = this.L;
                float width = rect7.width();
                Point point3 = new Point();
                point3.x = (int) ((width - ((i7 * width) / 255.0f)) + rect7.left);
                point3.y = rect7.top;
                RectF rectF2 = new RectF();
                int i8 = point3.x;
                int i9 = this.o / 2;
                rectF2.left = i8 - i9;
                rectF2.right = i9 + i8;
                int i10 = rect.top;
                int i11 = this.n;
                rectF2.top = i10 - i11;
                rectF2.bottom = rect.bottom + i11;
                canvas.drawRoundRect(rectF2, 2.0f, 2.0f, this.t);
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:20:0x0058, code lost:
        if (r0 != false) goto L21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:34:0x0087, code lost:
        if (r1 > r6) goto L35;
     */
    @Override // android.view.View
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void onMeasure(int r6, int r7) {
        /*
            r5 = this;
            int r0 = android.view.View.MeasureSpec.getMode(r6)
            int r1 = android.view.View.MeasureSpec.getMode(r7)
            int r6 = android.view.View.MeasureSpec.getSize(r6)
            int r2 = r5.getPaddingLeft()
            int r6 = r6 - r2
            int r2 = r5.getPaddingRight()
            int r6 = r6 - r2
            int r7 = android.view.View.MeasureSpec.getSize(r7)
            int r2 = r5.getPaddingBottom()
            int r7 = r7 - r2
            int r2 = r5.getPaddingTop()
            int r7 = r7 - r2
            r2 = 1073741824(0x40000000, float:2.0)
            if (r0 == r2) goto L5c
            if (r1 != r2) goto L2b
            goto L5c
        L2b:
            int r0 = r5.l
            int r1 = r7 + r0
            int r2 = r5.j
            int r1 = r1 + r2
            int r3 = r6 - r0
            int r3 = r3 - r2
            boolean r2 = r5.E
            if (r2 == 0) goto L40
            int r2 = r5.k
            int r4 = r0 + r2
            int r1 = r1 - r4
            int r0 = r0 + r2
            int r3 = r3 + r0
        L40:
            r0 = 1
            r2 = 0
            if (r1 > r6) goto L46
            r4 = 1
            goto L47
        L46:
            r4 = 0
        L47:
            if (r3 > r7) goto L4a
            goto L4b
        L4a:
            r0 = 0
        L4b:
            if (r4 == 0) goto L50
            if (r0 == 0) goto L50
            goto L5a
        L50:
            if (r0 != 0) goto L56
            if (r4 == 0) goto L56
        L54:
            r6 = r1
            goto L89
        L56:
            if (r4 != 0) goto L89
            if (r0 == 0) goto L89
        L5a:
            r7 = r3
            goto L89
        L5c:
            if (r0 != r2) goto L74
            if (r1 == r2) goto L74
            int r0 = r5.l
            int r1 = r6 - r0
            int r2 = r5.j
            int r1 = r1 - r2
            boolean r2 = r5.E
            if (r2 == 0) goto L6f
            int r2 = r5.k
            int r0 = r0 + r2
            int r1 = r1 + r0
        L6f:
            if (r1 <= r7) goto L72
            goto L89
        L72:
            r7 = r1
            goto L89
        L74:
            if (r1 != r2) goto L89
            if (r0 == r2) goto L89
            int r0 = r5.l
            int r1 = r7 + r0
            int r2 = r5.j
            int r1 = r1 + r2
            boolean r2 = r5.E
            if (r2 == 0) goto L87
            int r2 = r5.k
            int r0 = r0 + r2
            int r1 = r1 - r0
        L87:
            if (r1 <= r6) goto L54
        L89:
            int r0 = r5.getPaddingLeft()
            int r0 = r0 + r6
            int r6 = r5.getPaddingRight()
            int r6 = r6 + r0
            int r0 = r5.getPaddingTop()
            int r0 = r0 + r7
            int r7 = r5.getPaddingBottom()
            int r7 = r7 + r0
            r5.setMeasuredDimension(r6, r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jaredrummler.android.colorpicker.ColorPickerView.onMeasure(int, int):void");
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.A = bundle.getInt(Key.ALPHA);
            this.B = bundle.getFloat("hue");
            this.C = bundle.getFloat("sat");
            this.D = bundle.getFloat("val");
            this.E = bundle.getBoolean("show_alpha");
            this.F = bundle.getString("alpha_text");
            parcelable = bundle.getParcelable("instanceState");
        }
        super.onRestoreInstanceState(parcelable);
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putInt(Key.ALPHA, this.A);
        bundle.putFloat("hue", this.B);
        bundle.putFloat("sat", this.C);
        bundle.putFloat("val", this.D);
        bundle.putBoolean("show_alpha", this.E);
        bundle.putString("alpha_text", this.F);
        return bundle;
    }

    @Override // android.view.View
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        Rect rect = new Rect();
        this.I = rect;
        rect.left = getPaddingLeft();
        this.I.right = i - getPaddingRight();
        this.I.top = getPaddingTop();
        this.I.bottom = i2 - getPaddingBottom();
        this.v = null;
        this.w = null;
        this.f3125x = null;
        this.f3126y = null;
        this.f3127z = null;
        Rect rect2 = this.I;
        int i5 = rect2.left + 1;
        int i6 = rect2.top + 1;
        int i7 = rect2.bottom - 1;
        int i8 = this.l;
        int i9 = ((rect2.right - 1) - i8) - this.j;
        if (this.E) {
            i7 -= this.k + i8;
        }
        this.J = new Rect(i5, i6, i9, i7);
        Rect rect3 = this.I;
        int i10 = rect3.right;
        this.K = new Rect((i10 - this.j) + 1, rect3.top + 1, i10 - 1, (rect3.bottom - 1) - (this.E ? this.l + this.k : 0));
        if (this.E) {
            Rect rect4 = this.I;
            int i11 = rect4.bottom;
            this.L = new Rect(rect4.left + 1, (i11 - this.k) + 1, rect4.right - 1, i11 - 1);
            b.k.a.a.a aVar = new b.k.a.a.a(f.S(getContext(), 4.0f));
            this.N = aVar;
            aVar.setBounds(Math.round(this.L.left), Math.round(this.L.top), Math.round(this.L.right), Math.round(this.L.bottom));
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        int action = motionEvent.getAction();
        if (action == 0) {
            this.M = new Point((int) motionEvent.getX(), (int) motionEvent.getY());
            z2 = a(motionEvent);
        } else if (action != 1) {
            z2 = action != 2 ? false : a(motionEvent);
        } else {
            this.M = null;
            z2 = a(motionEvent);
        }
        if (!z2) {
            return super.onTouchEvent(motionEvent);
        }
        c cVar = this.O;
        if (cVar != null) {
            ((ColorPickerDialog) cVar).k(Color.HSVToColor(this.A, new float[]{this.B, this.C, this.D}));
        }
        invalidate();
        return true;
    }

    public void setAlphaSliderText(int i) {
        setAlphaSliderText(getContext().getString(i));
    }

    public void setAlphaSliderVisible(boolean z2) {
        if (this.E != z2) {
            this.E = z2;
            this.v = null;
            this.w = null;
            this.f3125x = null;
            this.f3127z = null;
            this.f3126y = null;
            requestLayout();
        }
    }

    public void setBorderColor(int i) {
        this.H = i;
        invalidate();
    }

    public void setColor(int i) {
        b(i, false);
    }

    public void setOnColorChangedListener(c cVar) {
        this.O = cVar;
    }

    public void setSliderTrackerColor(int i) {
        this.G = i;
        this.t.setColor(i);
        invalidate();
    }

    public void setAlphaSliderText(String str) {
        this.F = str;
        invalidate();
    }
}
