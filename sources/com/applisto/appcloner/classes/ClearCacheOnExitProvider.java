package com.applisto.appcloner.classes;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;
import com.applisto.appcloner.classes.util.Log;
import com.applisto.appcloner.classes.util.activity.OnAppExitListener;
/* loaded from: classes4.dex */
public class ClearCacheOnExitProvider extends OnAppExitListener {
    private static final String TAG = ClearCacheOnExitProvider.class.getSimpleName();

    @Override // com.applisto.appcloner.classes.util.activity.ActivityLifecycleListener
    protected boolean onInit(Application application) {
        String str = TAG;
        Log.i(str, "onInit; application: " + application);
        try {
            application.startService(new Intent(application, ClearCacheOnExitService.class));
            return true;
        } catch (Exception e) {
            Log.w(TAG, e);
            return true;
        }
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // com.applisto.appcloner.classes.util.activity.OnAppExitListener
    public void onAppExit(Context context) {
        clearCache(context, true);
    }

    public static synchronized void clearCache(Context context, boolean z2) {
        synchronized (ClearCacheOnExitProvider.class) {
            try {
                DefaultProvider.invokeSecondaryStatic("util.Utils", "clearCache", context);
                if (z2) {
                    Toast.makeText(context, Utils.getStringsProperties().getProperty("cache_cleared_message"), 0).show();
                }
            } catch (Throwable th) {
                Log.w(TAG, th);
            }
        }
    }
}
