package com.applisto.appcloner.classes;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import androidx.core.app.NotificationCompat;
import com.applisto.appcloner.classes.NotificationOptions;
import com.applisto.appcloner.classes.util.Log;
import com.applisto.appcloner.classes.util.activity.OnAppExitListener;
import com.discord.models.domain.ModelAuditLogEntry;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
/* loaded from: classes4.dex */
public class NotificationOptions extends OnAppExitListener {
    private static final String ACTION_SNOOZE_NOTIFICATION = "com.applisto.appcloner.action.SNOOZE_NOTIFICATION";
    private static final String EXTRA_SNOOZE_ACTION = "snooze_action";
    private static final String TAG = NotificationOptions.class.getSimpleName();
    private static boolean mAllowNotificationsWhenRunning;
    private static boolean mBlockAllNotifications;
    private static Integer mNotificationColor;
    private boolean mHeadsUpNotifications;
    private Icon mIcon;
    private boolean mLocalOnlyNotifications;
    private boolean mNotificationDots;
    private String mNotificationLightsColor;
    private String mNotificationLightsPattern;
    private String mNotificationPriority;
    private boolean mNotificationQuietTime;
    private int mNotificationQuietTimeEndHour;
    private int mNotificationQuietTimeEndMinute;
    private int mNotificationQuietTimeStartHour;
    private int mNotificationQuietTimeStartMinute;
    private int mNotificationSnoozeTimeout;
    private String mNotificationSound;
    private int mNotificationTimeout;
    private boolean mNotificationTintStatusBarIcon;
    private String mNotificationVibration;
    private String mNotificationVisibility;
    private String mOngoingNotifications;
    private boolean mRemoveNotificationActions;
    private boolean mRemoveNotificationIcon;
    private boolean mRemoveNotificationPeople;
    private boolean mReplaceNotificationIcon;
    private boolean mRunning;
    private boolean mShowNotificationTime;
    private boolean mSimpleNotifications;
    private String mSingleNotificationCategory;
    private boolean mSingleNotificationGroup;
    private List<Map<String, Object>> mNotificationCategories = new ArrayList();
    private List<Map<String, Object>> mNotificationTextReplacements = new ArrayList();
    private Handler mTimeoutHandler = new Handler();
    private Map<Integer, Runnable> mTimeoutRunnables = new HashMap();
    private Map<Integer, Notification> mSnoozeNotifications = new HashMap();
    private final Set<String> mNotificationFilterSet = new HashSet();

    public NotificationOptions(CloneSettings cloneSettings) {
        this.mNotificationLightsPattern = "NO_CHANGE";
        this.mNotificationLightsColor = "NO_CHANGE";
        mBlockAllNotifications = cloneSettings.getBoolean("blockAllNotifications", false).booleanValue();
        mAllowNotificationsWhenRunning = cloneSettings.getBoolean("allowNotificationsWhenRunning", false).booleanValue();
        String string = cloneSettings.getString("notificationFilter", null);
        if (!TextUtils.isEmpty(string)) {
            for (String str : string.trim().split(",")) {
                String trim = str.trim();
                if (!TextUtils.isEmpty(trim)) {
                    this.mNotificationFilterSet.add(trim.toLowerCase(Locale.ENGLISH));
                }
            }
        }
        this.mNotificationQuietTime = cloneSettings.getBoolean("notificationQuietTime", false).booleanValue();
        try {
            String[] split = cloneSettings.getString("notificationQuietTimeStart", "21:00").split(":");
            this.mNotificationQuietTimeStartHour = Integer.parseInt(split[0]);
            this.mNotificationQuietTimeStartMinute = Integer.parseInt(split[1]);
            String[] split2 = cloneSettings.getString("notificationQuietTimeEnd", "07:00").split(":");
            this.mNotificationQuietTimeEndHour = Integer.parseInt(split2[0]);
            this.mNotificationQuietTimeEndMinute = Integer.parseInt(split2[1]);
        } catch (Exception e) {
            Log.w(TAG, e);
        }
        if (cloneSettings.getBoolean("notificationColorUseStatusBarColor", false).booleanValue()) {
            mNotificationColor = cloneSettings.getInteger("statusBarColor", null);
        } else {
            mNotificationColor = cloneSettings.getInteger("notificationColor", null);
        }
        this.mNotificationTintStatusBarIcon = cloneSettings.getBoolean("notificationTintStatusBarIcon", false).booleanValue();
        this.mNotificationSound = cloneSettings.getString("notificationSound", "NO_CHANGE");
        this.mNotificationVibration = cloneSettings.getString("notificationVibration", "NO_CHANGE");
        this.mNotificationTimeout = cloneSettings.getInteger("notificationTimeout", 0).intValue();
        this.mNotificationSnoozeTimeout = cloneSettings.getInteger("notificationSnoozeTimeout", 0).intValue();
        this.mHeadsUpNotifications = cloneSettings.getBoolean("headsUpNotifications", false).booleanValue();
        this.mLocalOnlyNotifications = cloneSettings.getBoolean("localOnlyNotifications", false).booleanValue();
        this.mOngoingNotifications = cloneSettings.getString("ongoingNotifications", "NO_CHANGE");
        this.mShowNotificationTime = cloneSettings.getBoolean("showNotificationTime", false).booleanValue();
        CloneSettings forObject = cloneSettings.forObject("defaultNotificationLights");
        if (forObject != null) {
            this.mNotificationLightsPattern = forObject.getString("notificationLightsPattern", "NO_CHANGE");
            this.mNotificationLightsColor = forObject.getString("notificationLightsColor", "NO_CHANGE");
        }
        this.mNotificationVisibility = cloneSettings.getString("notificationVisibility", "NO_CHANGE");
        this.mNotificationPriority = cloneSettings.getString("notificationPriority", "NO_CHANGE");
        this.mReplaceNotificationIcon = cloneSettings.getBoolean("replaceNotificationIcon", false).booleanValue();
        this.mRemoveNotificationIcon = cloneSettings.getBoolean("removeNotificationIcon", false).booleanValue();
        this.mRemoveNotificationActions = cloneSettings.getBoolean("removeNotificationActions", false).booleanValue();
        this.mRemoveNotificationPeople = cloneSettings.getBoolean("removeNotificationPeople", false).booleanValue();
        this.mSimpleNotifications = cloneSettings.getBoolean("simpleNotifications", false).booleanValue();
        this.mSingleNotificationGroup = cloneSettings.getBoolean("singleNotificationGroup", false).booleanValue();
        List<CloneSettings> forObjectArray = cloneSettings.forObjectArray("notificationCategories");
        if (forObjectArray != null) {
            for (CloneSettings cloneSettings2 : forObjectArray) {
                HashMap hashMap = new HashMap();
                hashMap.put(ModelAuditLogEntry.CHANGE_KEY_NAME, cloneSettings2.getString(ModelAuditLogEntry.CHANGE_KEY_NAME, ""));
                hashMap.put("keywords", cloneSettings2.getString("keywords", ""));
                hashMap.put("ignoreCase", cloneSettings2.getBoolean("ignoreCase", true));
                this.mNotificationCategories.add(hashMap);
            }
        }
        this.mSingleNotificationCategory = cloneSettings.getString("singleNotificationCategory", null);
        List<CloneSettings> forObjectArray2 = cloneSettings.forObjectArray("notificationTextReplacements");
        if (forObjectArray2 != null) {
            for (CloneSettings cloneSettings3 : forObjectArray2) {
                HashMap hashMap2 = new HashMap();
                hashMap2.put("replace", cloneSettings3.getString("replace", ""));
                hashMap2.put("with", cloneSettings3.getString("with", ""));
                hashMap2.put("ignoreCase", cloneSettings3.getBoolean("ignoreCase", true));
                hashMap2.put("replaceInTitle", cloneSettings3.getBoolean("replaceInTitle", true));
                hashMap2.put("replaceInContent", cloneSettings3.getBoolean("replaceInContent", true));
                hashMap2.put("replaceInMessages", cloneSettings3.getBoolean("replaceInMessages", true));
                hashMap2.put("replaceInActions", cloneSettings3.getBoolean("replaceInActions", true));
                this.mNotificationTextReplacements.add(hashMap2);
            }
        }
        this.mNotificationDots = cloneSettings.getBoolean("notificationDots", false).booleanValue();
        Log.i(TAG, "NotificationOptions; mBlockAllNotifications: " + mBlockAllNotifications);
        Log.i(TAG, "NotificationOptions; mAllowNotificationsWhenRunning: " + mAllowNotificationsWhenRunning);
        Log.i(TAG, "NotificationOptions; mNotificationFilterSet: " + this.mNotificationFilterSet);
        Log.i(TAG, "NotificationOptions; mNotificationQuietTime: " + this.mNotificationQuietTime);
        Log.i(TAG, "NotificationOptions; mNotificationQuietTimeStartHour: " + this.mNotificationQuietTimeStartHour);
        Log.i(TAG, "NotificationOptions; mNotificationQuietTimeStartMinute: " + this.mNotificationQuietTimeStartMinute);
        Log.i(TAG, "NotificationOptions; mNotificationQuietTimeEndHour: " + this.mNotificationQuietTimeEndHour);
        Log.i(TAG, "NotificationOptions; mNotificationQuietTimeEndMinute: " + this.mNotificationQuietTimeEndMinute);
        Log.i(TAG, "NotificationOptions; mNotificationColor: " + mNotificationColor);
        Log.i(TAG, "NotificationOptions; mNotificationTintStatusBarIcon: " + this.mNotificationTintStatusBarIcon);
        Log.i(TAG, "NotificationOptions; mNotificationSound: " + this.mNotificationSound);
        Log.i(TAG, "NotificationOptions; mNotificationVibration: " + this.mNotificationVibration);
        Log.i(TAG, "NotificationOptions; mNotificationTimeout: " + this.mNotificationTimeout);
        Log.i(TAG, "NotificationOptions; mNotificationSnoozeTimeout: " + this.mNotificationSnoozeTimeout);
        Log.i(TAG, "NotificationOptions; mHeadsUpNotifications: " + this.mHeadsUpNotifications);
        Log.i(TAG, "NotificationOptions; mLocalOnlyNotifications: " + this.mLocalOnlyNotifications);
        Log.i(TAG, "NotificationOptions; mOngoingNotifications: " + this.mOngoingNotifications);
        Log.i(TAG, "NotificationOptions; mShowNotificationTime: " + this.mShowNotificationTime);
        Log.i(TAG, "NotificationOptions; mNotificationLightsPattern: " + this.mNotificationLightsPattern);
        Log.i(TAG, "NotificationOptions; mNotificationLightsColor: " + this.mNotificationLightsColor);
        Log.i(TAG, "NotificationOptions; mNotificationVisibility: " + this.mNotificationVisibility);
        Log.i(TAG, "NotificationOptions; mNotificationPriority: " + this.mNotificationPriority);
        Log.i(TAG, "NotificationOptions; mReplaceNotificationIcon: " + this.mReplaceNotificationIcon);
        Log.i(TAG, "NotificationOptions; mRemoveNotificationIcon: " + this.mRemoveNotificationIcon);
        Log.i(TAG, "NotificationOptions; mRemoveNotificationActions: " + this.mRemoveNotificationActions);
        Log.i(TAG, "NotificationOptions; mRemoveNotificationPeople: " + this.mRemoveNotificationPeople);
        Log.i(TAG, "NotificationOptions; mSimpleNotifications: " + this.mSimpleNotifications);
        Log.i(TAG, "NotificationOptions; mSingleNotificationGroup: " + this.mSingleNotificationGroup);
        Log.i(TAG, "NotificationOptions; mNotificationCategories: " + this.mNotificationCategories);
        Log.i(TAG, "NotificationOptions; mSingleNotificationCategory: " + this.mSingleNotificationCategory);
        Log.i(TAG, "NotificationOptions; mNotificationTextReplacements: " + this.mNotificationTextReplacements);
    }

    public void install(final Context context) {
        Log.i(TAG, "install; ");
        if (mNotificationColor != null || mBlockAllNotifications || !this.mNotificationFilterSet.isEmpty() || this.mNotificationQuietTime || !"NO_CHANGE".equals(this.mNotificationSound) || !"NO_CHANGE".equals(this.mNotificationVibration) || this.mNotificationTimeout != 0 || this.mNotificationSnoozeTimeout != 0 || this.mHeadsUpNotifications || this.mLocalOnlyNotifications || !"NO_CHANGE".equals(this.mOngoingNotifications) || this.mShowNotificationTime || !"NO_CHANGE".equals(this.mNotificationLightsPattern) || !"NO_CHANGE".equals(this.mNotificationLightsColor) || !"NO_CHANGE".equals(this.mNotificationVisibility) || !"NO_CHANGE".equals(this.mNotificationPriority) || this.mReplaceNotificationIcon || this.mRemoveNotificationIcon || this.mRemoveNotificationActions || this.mRemoveNotificationPeople || this.mSimpleNotifications || this.mSingleNotificationGroup || !this.mNotificationCategories.isEmpty() || !TextUtils.isEmpty(this.mSingleNotificationCategory) || !this.mNotificationTextReplacements.isEmpty()) {
            onCreate();
            try {
                Field declaredField = NotificationManager.class.getDeclaredField("sService");
                declaredField.setAccessible(true);
                if (declaredField.get(null) != null) {
                    Log.i(TAG, "onCreate; sService already initialized!!!");
                } else {
                    Log.i(TAG, "onCreate; sService == null");
                }
                NotificationManager.class.getMethod("getService", new Class[0]).invoke(null, new Object[0]);
                final Object obj = declaredField.get(null);
                declaredField.set(null, Proxy.newProxyInstance(NotificationOptions.class.getClassLoader(), new Class[]{Class.forName("android.app.INotificationManager")}, new InvocationHandler() { // from class: com.applisto.appcloner.classes.-$$Lambda$NotificationOptions$PMv6Pe-FXiJ1ET7L7mZ6zyF_y2c
                    @Override // java.lang.reflect.InvocationHandler
                    public final Object invoke(Object obj2, Method method, Object[] objArr) {
                        return NotificationOptions.this.lambda$install$1$NotificationOptions(context, obj, obj2, method, objArr);
                    }
                }));
                if (this.mNotificationSnoozeTimeout > 0) {
                    context.registerReceiver(new AnonymousClass1(), new IntentFilter(ACTION_SNOOZE_NOTIFICATION));
                }
                if (this.mReplaceNotificationIcon && Build.VERSION.SDK_INT >= 23) {
                    try {
                        byte[] readFully = Utils.readFully(context.getAssets().open(".notificationIconFile"), true);
                        this.mIcon = Icon.createWithData(readFully, 0, readFully.length);
                        String str = TAG;
                        Log.i(str, "install; mIcon: " + this.mIcon);
                    } catch (Exception e) {
                        Log.w(TAG, e);
                    }
                }
            } catch (Exception e2) {
                Log.w(TAG, e2);
            }
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:258:0x0866  */
    /* JADX WARN: Removed duplicated region for block: B:271:0x08ab  */
    /* JADX WARN: Removed duplicated region for block: B:307:0x0941 A[Catch: Exception -> 0x029e, TryCatch #11 {Exception -> 0x029e, blocks: (B:62:0x01cd, B:67:0x0214, B:71:0x022c, B:77:0x023b, B:83:0x02a9, B:86:0x02bd, B:88:0x02c5, B:89:0x02d6, B:91:0x02e0, B:92:0x02f0, B:94:0x02fa, B:95:0x0335, B:97:0x033d, B:98:0x034f, B:100:0x0359, B:101:0x036b, B:103:0x0377, B:104:0x0392, B:106:0x039c, B:107:0x03b7, B:109:0x03c1, B:110:0x03dc, B:112:0x03e6, B:113:0x0400, B:115:0x0406, B:117:0x0414, B:119:0x041e, B:122:0x045d, B:124:0x0461, B:126:0x0467, B:127:0x0486, B:129:0x048a, B:130:0x04a6, B:132:0x04b0, B:133:0x04d3, B:135:0x04dd, B:136:0x0500, B:141:0x0529, B:142:0x052e, B:144:0x0535, B:146:0x053f, B:147:0x0559, B:149:0x0563, B:150:0x057e, B:152:0x0588, B:153:0x05a3, B:155:0x05ad, B:156:0x05c7, B:158:0x05cf, B:159:0x05e9, B:161:0x05f3, B:162:0x060c, B:164:0x0616, B:165:0x062e, B:167:0x0638, B:169:0x063c, B:170:0x063e, B:172:0x0657, B:173:0x0660, B:175:0x066a, B:176:0x0671, B:178:0x067b, B:179:0x0682, B:181:0x068c, B:182:0x0691, B:184:0x069b, B:185:0x06a0, B:187:0x06a8, B:189:0x06ae, B:191:0x06b6, B:192:0x06ba, B:194:0x06d3, B:195:0x06d6, B:197:0x06e0, B:198:0x06e5, B:200:0x06ef, B:201:0x06f4, B:203:0x06fe, B:204:0x0704, B:206:0x070e, B:207:0x0714, B:209:0x071e, B:210:0x0724, B:212:0x072e, B:213:0x0733, B:215:0x0737, B:217:0x073d, B:219:0x0768, B:220:0x0771, B:221:0x0775, B:223:0x077b, B:225:0x0787, B:227:0x0793, B:228:0x0797, B:229:0x07ac, B:231:0x07b3, B:232:0x07b8, B:233:0x07c3, B:235:0x07c7, B:237:0x07ef, B:238:0x07fd, B:240:0x0801, B:242:0x0805, B:254:0x085b, B:256:0x0862, B:268:0x08a2, B:269:0x08a7, B:278:0x08c9, B:279:0x08ce, B:304:0x0938, B:305:0x093d, B:307:0x0941, B:309:0x0945, B:310:0x095a, B:312:0x095e, B:325:0x09d6, B:327:0x09dd, B:329:0x09e1, B:281:0x08d2, B:283:0x08e7, B:285:0x08ef, B:287:0x08f3, B:289:0x08f9, B:290:0x0905, B:291:0x0908, B:293:0x090e, B:295:0x0916, B:297:0x0919, B:299:0x091f, B:300:0x092b, B:301:0x092e, B:259:0x0867, B:261:0x0876, B:262:0x087b, B:264:0x0883, B:265:0x0888, B:272:0x08ac, B:274:0x08ba, B:275:0x08bf, B:138:0x0504), top: B:390:0x01cd, inners: #3, #9, #15, #16 }] */
    /* JADX WARN: Removed duplicated region for block: B:312:0x095e A[Catch: Exception -> 0x029e, TRY_LEAVE, TryCatch #11 {Exception -> 0x029e, blocks: (B:62:0x01cd, B:67:0x0214, B:71:0x022c, B:77:0x023b, B:83:0x02a9, B:86:0x02bd, B:88:0x02c5, B:89:0x02d6, B:91:0x02e0, B:92:0x02f0, B:94:0x02fa, B:95:0x0335, B:97:0x033d, B:98:0x034f, B:100:0x0359, B:101:0x036b, B:103:0x0377, B:104:0x0392, B:106:0x039c, B:107:0x03b7, B:109:0x03c1, B:110:0x03dc, B:112:0x03e6, B:113:0x0400, B:115:0x0406, B:117:0x0414, B:119:0x041e, B:122:0x045d, B:124:0x0461, B:126:0x0467, B:127:0x0486, B:129:0x048a, B:130:0x04a6, B:132:0x04b0, B:133:0x04d3, B:135:0x04dd, B:136:0x0500, B:141:0x0529, B:142:0x052e, B:144:0x0535, B:146:0x053f, B:147:0x0559, B:149:0x0563, B:150:0x057e, B:152:0x0588, B:153:0x05a3, B:155:0x05ad, B:156:0x05c7, B:158:0x05cf, B:159:0x05e9, B:161:0x05f3, B:162:0x060c, B:164:0x0616, B:165:0x062e, B:167:0x0638, B:169:0x063c, B:170:0x063e, B:172:0x0657, B:173:0x0660, B:175:0x066a, B:176:0x0671, B:178:0x067b, B:179:0x0682, B:181:0x068c, B:182:0x0691, B:184:0x069b, B:185:0x06a0, B:187:0x06a8, B:189:0x06ae, B:191:0x06b6, B:192:0x06ba, B:194:0x06d3, B:195:0x06d6, B:197:0x06e0, B:198:0x06e5, B:200:0x06ef, B:201:0x06f4, B:203:0x06fe, B:204:0x0704, B:206:0x070e, B:207:0x0714, B:209:0x071e, B:210:0x0724, B:212:0x072e, B:213:0x0733, B:215:0x0737, B:217:0x073d, B:219:0x0768, B:220:0x0771, B:221:0x0775, B:223:0x077b, B:225:0x0787, B:227:0x0793, B:228:0x0797, B:229:0x07ac, B:231:0x07b3, B:232:0x07b8, B:233:0x07c3, B:235:0x07c7, B:237:0x07ef, B:238:0x07fd, B:240:0x0801, B:242:0x0805, B:254:0x085b, B:256:0x0862, B:268:0x08a2, B:269:0x08a7, B:278:0x08c9, B:279:0x08ce, B:304:0x0938, B:305:0x093d, B:307:0x0941, B:309:0x0945, B:310:0x095a, B:312:0x095e, B:325:0x09d6, B:327:0x09dd, B:329:0x09e1, B:281:0x08d2, B:283:0x08e7, B:285:0x08ef, B:287:0x08f3, B:289:0x08f9, B:290:0x0905, B:291:0x0908, B:293:0x090e, B:295:0x0916, B:297:0x0919, B:299:0x091f, B:300:0x092b, B:301:0x092e, B:259:0x0867, B:261:0x0876, B:262:0x087b, B:264:0x0883, B:265:0x0888, B:272:0x08ac, B:274:0x08ba, B:275:0x08bf, B:138:0x0504), top: B:390:0x01cd, inners: #3, #9, #15, #16 }] */
    /* JADX WARN: Removed duplicated region for block: B:317:0x0972 A[Catch: Exception -> 0x09d3, TRY_LEAVE, TryCatch #6 {Exception -> 0x09d3, blocks: (B:314:0x0966, B:315:0x096c, B:317:0x0972), top: B:380:0x0966 }] */
    /* JADX WARN: Removed duplicated region for block: B:329:0x09e1 A[Catch: Exception -> 0x029e, TRY_LEAVE, TryCatch #11 {Exception -> 0x029e, blocks: (B:62:0x01cd, B:67:0x0214, B:71:0x022c, B:77:0x023b, B:83:0x02a9, B:86:0x02bd, B:88:0x02c5, B:89:0x02d6, B:91:0x02e0, B:92:0x02f0, B:94:0x02fa, B:95:0x0335, B:97:0x033d, B:98:0x034f, B:100:0x0359, B:101:0x036b, B:103:0x0377, B:104:0x0392, B:106:0x039c, B:107:0x03b7, B:109:0x03c1, B:110:0x03dc, B:112:0x03e6, B:113:0x0400, B:115:0x0406, B:117:0x0414, B:119:0x041e, B:122:0x045d, B:124:0x0461, B:126:0x0467, B:127:0x0486, B:129:0x048a, B:130:0x04a6, B:132:0x04b0, B:133:0x04d3, B:135:0x04dd, B:136:0x0500, B:141:0x0529, B:142:0x052e, B:144:0x0535, B:146:0x053f, B:147:0x0559, B:149:0x0563, B:150:0x057e, B:152:0x0588, B:153:0x05a3, B:155:0x05ad, B:156:0x05c7, B:158:0x05cf, B:159:0x05e9, B:161:0x05f3, B:162:0x060c, B:164:0x0616, B:165:0x062e, B:167:0x0638, B:169:0x063c, B:170:0x063e, B:172:0x0657, B:173:0x0660, B:175:0x066a, B:176:0x0671, B:178:0x067b, B:179:0x0682, B:181:0x068c, B:182:0x0691, B:184:0x069b, B:185:0x06a0, B:187:0x06a8, B:189:0x06ae, B:191:0x06b6, B:192:0x06ba, B:194:0x06d3, B:195:0x06d6, B:197:0x06e0, B:198:0x06e5, B:200:0x06ef, B:201:0x06f4, B:203:0x06fe, B:204:0x0704, B:206:0x070e, B:207:0x0714, B:209:0x071e, B:210:0x0724, B:212:0x072e, B:213:0x0733, B:215:0x0737, B:217:0x073d, B:219:0x0768, B:220:0x0771, B:221:0x0775, B:223:0x077b, B:225:0x0787, B:227:0x0793, B:228:0x0797, B:229:0x07ac, B:231:0x07b3, B:232:0x07b8, B:233:0x07c3, B:235:0x07c7, B:237:0x07ef, B:238:0x07fd, B:240:0x0801, B:242:0x0805, B:254:0x085b, B:256:0x0862, B:268:0x08a2, B:269:0x08a7, B:278:0x08c9, B:279:0x08ce, B:304:0x0938, B:305:0x093d, B:307:0x0941, B:309:0x0945, B:310:0x095a, B:312:0x095e, B:325:0x09d6, B:327:0x09dd, B:329:0x09e1, B:281:0x08d2, B:283:0x08e7, B:285:0x08ef, B:287:0x08f3, B:289:0x08f9, B:290:0x0905, B:291:0x0908, B:293:0x090e, B:295:0x0916, B:297:0x0919, B:299:0x091f, B:300:0x092b, B:301:0x092e, B:259:0x0867, B:261:0x0876, B:262:0x087b, B:264:0x0883, B:265:0x0888, B:272:0x08ac, B:274:0x08ba, B:275:0x08bf, B:138:0x0504), top: B:390:0x01cd, inners: #3, #9, #15, #16 }] */
    /* JADX WARN: Removed duplicated region for block: B:374:0x08d2 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public /* synthetic */ java.lang.Object lambda$install$1$NotificationOptions(final android.content.Context r18, java.lang.Object r19, java.lang.Object r20, java.lang.reflect.Method r21, java.lang.Object[] r22) throws java.lang.Throwable {
        /*
            Method dump skipped, instructions count: 2728
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applisto.appcloner.classes.NotificationOptions.lambda$install$1$NotificationOptions(android.content.Context, java.lang.Object, java.lang.Object, java.lang.reflect.Method, java.lang.Object[]):java.lang.Object");
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    public static /* synthetic */ void lambda$null$0(int i, Context context) {
        try {
            String str = TAG;
            Log.i(str, "run; cancelling notification; id: " + i);
            ((NotificationManager) context.getSystemService("notification")).cancel(i);
        } catch (Throwable th) {
            Log.w(TAG, th);
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* renamed from: com.applisto.appcloner.classes.NotificationOptions$1  reason: invalid class name */
    /* loaded from: classes4.dex */
    public class AnonymousClass1 extends BroadcastReceiver {
        AnonymousClass1() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            try {
                final int intExtra = intent.getIntExtra(ModelAuditLogEntry.CHANGE_KEY_ID, 0);
                final Notification notification = (Notification) NotificationOptions.this.mSnoozeNotifications.remove(Integer.valueOf(intExtra));
                if (notification != null) {
                    final NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
                    notificationManager.cancel(intExtra);
                    Runnable runnable = (Runnable) NotificationOptions.this.mTimeoutRunnables.get(Integer.valueOf(intExtra));
                    if (runnable != null) {
                        NotificationOptions.this.mTimeoutHandler.removeCallbacks(runnable);
                    }
                    Runnable runnable2 = new Runnable() { // from class: com.applisto.appcloner.classes.-$$Lambda$NotificationOptions$1$3W4-9Dj5LfZQOeUnizMcb6bp1D0
                        @Override // java.lang.Runnable
                        public final void run() {
                            NotificationOptions.AnonymousClass1.lambda$onReceive$0(notificationManager, intExtra, notification);
                        }
                    };
                    NotificationOptions.this.mTimeoutHandler.postDelayed(runnable2, NotificationOptions.this.mNotificationSnoozeTimeout * 1000);
                    NotificationOptions.this.mTimeoutRunnables.put(Integer.valueOf(intExtra), runnable2);
                }
            } catch (Throwable th) {
                Log.w(NotificationOptions.TAG, th);
            }
        }

        /* JADX INFO: Access modifiers changed from: package-private */
        public static /* synthetic */ void lambda$onReceive$0(NotificationManager notificationManager, int i, Notification notification) {
            try {
                notificationManager.notify(i, notification);
            } catch (Throwable th) {
                Log.w(NotificationOptions.TAG, th);
            }
        }
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // com.applisto.appcloner.classes.util.activity.OnAppExitListener, com.applisto.appcloner.classes.util.activity.ActivityLifecycleListener
    public void onActivityCreated(Activity activity) {
        super.onActivityCreated(activity);
        this.mRunning = true;
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // com.applisto.appcloner.classes.util.activity.OnAppExitListener
    public void onAppExit(Context context) {
        Log.i(TAG, "onAppExit; ");
        this.mRunning = false;
    }

    private boolean isAppClonerClassesNotification() {
        boolean z2 = false;
        for (StackTraceElement stackTraceElement : new Exception().getStackTrace()) {
            String className = stackTraceElement.getClassName();
            if ("android.app.NotificationManager".equals(className)) {
                z2 = true;
            } else if (z2) {
                return className.startsWith(DefaultProvider.PREF_KEY_PREFIX);
            }
        }
        return false;
    }

    private static Bundle getExtras(Notification notification) {
        Bundle bundle;
        try {
            try {
                bundle = notification.extras;
            } catch (Throwable unused) {
                Field declaredField = Notification.class.getDeclaredField(NotificationCompat.MessagingStyle.Message.KEY_EXTRAS_BUNDLE);
                declaredField.setAccessible(true);
                bundle = (Bundle) declaredField.get(notification);
            }
        } catch (Exception e) {
            Log.w(TAG, e);
            bundle = null;
        }
        return bundle == null ? new Bundle() : bundle;
    }

    private String getNotificationChannelId(Context context, String str) {
        String notificationChannelName = getNotificationChannelName(str);
        if (notificationChannelName == null) {
            return null;
        }
        String str2 = "app_cloner_" + Math.abs(notificationChannelName.hashCode());
        Log.i(TAG, "getNotificationChannelId; channelId: " + str2 + ", channelName: " + notificationChannelName);
        ((NotificationManager) context.getSystemService("notification")).createNotificationChannel(new NotificationChannel(str2, notificationChannelName, 3));
        return str2;
    }

    private String getNotificationChannelName(String str) {
        if (!TextUtils.isEmpty(this.mSingleNotificationCategory)) {
            Log.i(TAG, "getNotificationChannelName; returning mSingleNotificationCategory: " + this.mSingleNotificationCategory);
            return this.mSingleNotificationCategory;
        }
        for (Map<String, Object> map : this.mNotificationCategories) {
            String str2 = (String) map.get(ModelAuditLogEntry.CHANGE_KEY_NAME);
            if (!TextUtils.isEmpty(str2)) {
                String str3 = (String) map.get("keywords");
                if (TextUtils.isEmpty(str3)) {
                    continue;
                } else {
                    boolean booleanValue = ((Boolean) map.get("ignoreCase")).booleanValue();
                    String lowerCase = booleanValue ? str.toLowerCase(Locale.ENGLISH) : str;
                    Log.i(TAG, "getNotificationChannelName; name: " + str2 + ", keywords: " + str3 + ", ignoreCase: " + booleanValue + ", matchText: " + lowerCase);
                    for (String str4 : str3.split(",")) {
                        String trim = str4.trim();
                        if (booleanValue) {
                            trim = trim.toLowerCase(Locale.ENGLISH);
                        }
                        if (lowerCase.contains(trim)) {
                            Log.i(TAG, "getNotificationChannelName; found keyword; keyword: " + trim);
                            return str2;
                        }
                    }
                    continue;
                }
            }
        }
        return null;
    }

    private void replaceNotificationText(Notification notification, String str, String str2, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        Bundle bundle;
        String str3;
        Bundle bundle2;
        Bundle bundle3;
        Parcelable[] parcelableArray;
        Exception e;
        Parcelable[] parcelableArr;
        int i;
        int i2;
        Exception e2;
        Notification notification2;
        ArrayList parcelableArrayList;
        Notification.Action[] actionArr;
        Parcelable[] parcelableArray2;
        Log.i(TAG, "replaceNotificationText; replace: " + str + ", with: " + str2 + ", ignoreCase: " + z2 + ", replaceInTitle: " + z3 + ", replaceInContent: " + z4 + ", replaceInMessages: " + z5 + ", replaceInActions: " + z6);
        notification.tickerText = replaceText(notification.tickerText, str, str2, z2);
        if (Build.VERSION.SDK_INT >= 19) {
            Bundle extras = getExtras(notification);
            if (z3) {
                CharSequence charSequence = extras.getCharSequence(NotificationCompat.EXTRA_TITLE);
                if (!TextUtils.isEmpty(charSequence)) {
                    CharSequence replaceText = replaceText(charSequence, str, str2, z2);
                    if (!TextUtils.isEmpty(replaceText)) {
                        extras.putCharSequence(NotificationCompat.EXTRA_TITLE, replaceText);
                    } else {
                        extras.remove(NotificationCompat.EXTRA_TITLE_BIG);
                    }
                }
                CharSequence charSequence2 = extras.getCharSequence(NotificationCompat.EXTRA_TITLE_BIG);
                if (!TextUtils.isEmpty(charSequence2)) {
                    CharSequence replaceText2 = replaceText(charSequence2, str, str2, z2);
                    if (!TextUtils.isEmpty(replaceText2)) {
                        extras.putCharSequence(NotificationCompat.EXTRA_TITLE_BIG, replaceText2);
                    } else {
                        extras.remove(NotificationCompat.EXTRA_TITLE_BIG);
                    }
                }
                if (Build.VERSION.SDK_INT >= 24) {
                    CharSequence charSequence3 = extras.getCharSequence(NotificationCompat.EXTRA_CONVERSATION_TITLE);
                    if (!TextUtils.isEmpty(charSequence3)) {
                        CharSequence replaceText3 = replaceText(charSequence3, str, str2, z2);
                        if (!TextUtils.isEmpty(replaceText3)) {
                            extras.putCharSequence(NotificationCompat.EXTRA_CONVERSATION_TITLE, replaceText3);
                        } else {
                            extras.remove(NotificationCompat.EXTRA_CONVERSATION_TITLE);
                        }
                    }
                    CharSequence charSequence4 = extras.getCharSequence(NotificationCompat.EXTRA_HIDDEN_CONVERSATION_TITLE);
                    if (!TextUtils.isEmpty(charSequence4)) {
                        CharSequence replaceText4 = replaceText(charSequence4, str, str2, z2);
                        if (!TextUtils.isEmpty(replaceText4)) {
                            extras.putCharSequence(NotificationCompat.EXTRA_HIDDEN_CONVERSATION_TITLE, replaceText4);
                        } else {
                            extras.remove(NotificationCompat.EXTRA_HIDDEN_CONVERSATION_TITLE);
                        }
                    }
                }
            }
            if (z4) {
                CharSequence charSequence5 = extras.getCharSequence(NotificationCompat.EXTRA_TEXT);
                if (!TextUtils.isEmpty(charSequence5)) {
                    CharSequence replaceText5 = replaceText(charSequence5, str, str2, z2);
                    if (!TextUtils.isEmpty(replaceText5)) {
                        extras.putCharSequence(NotificationCompat.EXTRA_TEXT, replaceText5);
                    } else {
                        extras.remove(NotificationCompat.EXTRA_TEXT);
                    }
                }
                CharSequence charSequence6 = extras.getCharSequence(NotificationCompat.EXTRA_SUB_TEXT);
                if (!TextUtils.isEmpty(charSequence6)) {
                    CharSequence replaceText6 = replaceText(charSequence6, str, str2, z2);
                    if (!TextUtils.isEmpty(replaceText6)) {
                        extras.putCharSequence(NotificationCompat.EXTRA_SUB_TEXT, replaceText6);
                    } else {
                        extras.remove(NotificationCompat.EXTRA_SUB_TEXT);
                    }
                }
                CharSequence charSequence7 = extras.getCharSequence(NotificationCompat.EXTRA_INFO_TEXT);
                if (!TextUtils.isEmpty(charSequence7)) {
                    CharSequence replaceText7 = replaceText(charSequence7, str, str2, z2);
                    if (!TextUtils.isEmpty(replaceText7)) {
                        extras.putCharSequence(NotificationCompat.EXTRA_INFO_TEXT, replaceText7);
                    } else {
                        extras.remove(NotificationCompat.EXTRA_INFO_TEXT);
                    }
                }
                CharSequence charSequence8 = extras.getCharSequence(NotificationCompat.EXTRA_SUMMARY_TEXT);
                if (!TextUtils.isEmpty(charSequence8)) {
                    CharSequence replaceText8 = replaceText(charSequence8, str, str2, z2);
                    if (!TextUtils.isEmpty(replaceText8)) {
                        extras.putCharSequence(NotificationCompat.EXTRA_SUMMARY_TEXT, replaceText8);
                    } else {
                        extras.remove(NotificationCompat.EXTRA_SUMMARY_TEXT);
                    }
                }
                if (Build.VERSION.SDK_INT >= 21) {
                    CharSequence charSequence9 = extras.getCharSequence(NotificationCompat.EXTRA_BIG_TEXT);
                    if (!TextUtils.isEmpty(charSequence9)) {
                        CharSequence replaceText9 = replaceText(charSequence9, str, str2, z2);
                        if (!TextUtils.isEmpty(replaceText9)) {
                            extras.putCharSequence(NotificationCompat.EXTRA_BIG_TEXT, replaceText9);
                        } else {
                            extras.remove(NotificationCompat.EXTRA_BIG_TEXT);
                        }
                    }
                }
                if (Build.VERSION.SDK_INT >= 24) {
                    CharSequence charSequence10 = extras.getCharSequence(NotificationCompat.EXTRA_SELF_DISPLAY_NAME);
                    if (!TextUtils.isEmpty(charSequence10)) {
                        CharSequence replaceText10 = replaceText(charSequence10, str, str2, z2);
                        if (!TextUtils.isEmpty(replaceText10)) {
                            extras.putCharSequence(NotificationCompat.EXTRA_SELF_DISPLAY_NAME, replaceText10);
                        } else {
                            extras.remove(NotificationCompat.EXTRA_SELF_DISPLAY_NAME);
                        }
                    }
                }
            }
            String str4 = NotificationCompat.MessagingStyle.Message.KEY_TEXT;
            if (z5) {
                CharSequence[] charSequenceArray = extras.getCharSequenceArray(NotificationCompat.EXTRA_TEXT_LINES);
                if (charSequenceArray != null) {
                    ArrayList arrayList = new ArrayList();
                    int length = charSequenceArray.length;
                    for (int i3 = 0; i3 < length; i3++) {
                        length = length;
                        CharSequence charSequence11 = charSequenceArray[i3];
                        CharSequence replaceText11 = replaceText(charSequence11, str, str2, z2);
                        if (!TextUtils.isEmpty(replaceText11)) {
                            if (charSequence11 instanceof String) {
                                replaceText11 = replaceText11.toString();
                            }
                            arrayList.add(replaceText11);
                        }
                    }
                    if (!arrayList.isEmpty()) {
                        extras.putCharSequenceArray(NotificationCompat.EXTRA_TEXT_LINES, (CharSequence[]) arrayList.toArray(new CharSequence[0]));
                    } else {
                        extras.remove(NotificationCompat.EXTRA_TEXT_LINES);
                    }
                }
                if (Build.VERSION.SDK_INT >= 24 && (parcelableArray2 = extras.getParcelableArray(NotificationCompat.EXTRA_MESSAGES)) != null) {
                    ArrayList arrayList2 = new ArrayList();
                    int length2 = parcelableArray2.length;
                    for (int i4 = 0; i4 < length2; i4++) {
                        Parcelable parcelable = parcelableArray2[i4];
                        parcelableArray2 = parcelableArray2;
                        Bundle bundle4 = (Bundle) parcelable;
                        length2 = length2;
                        String string = bundle4.getString(NotificationCompat.MessagingStyle.Message.KEY_SENDER);
                        if (!TextUtils.isEmpty(string)) {
                            string = "" + ((Object) replaceText(string, str, str2, z2));
                            bundle4.putString(NotificationCompat.MessagingStyle.Message.KEY_SENDER, string);
                        }
                        String string2 = bundle4.getString(str4);
                        if (!TextUtils.isEmpty(string2)) {
                            string2 = "" + ((Object) replaceText(string2, str, str2, z2));
                            bundle4.putString(str4, string2);
                        }
                        if (!TextUtils.isEmpty(string) || !TextUtils.isEmpty(string2)) {
                            arrayList2.add(bundle4);
                        }
                    }
                    if (!arrayList2.isEmpty()) {
                        extras.putParcelableArray(NotificationCompat.EXTRA_MESSAGES, (Parcelable[]) arrayList2.toArray(new Parcelable[0]));
                    } else {
                        extras.remove(NotificationCompat.EXTRA_MESSAGES);
                    }
                }
            }
            if (z6 && notification.actions != null) {
                ArrayList arrayList3 = new ArrayList();
                for (Notification.Action action : notification.actions) {
                    action.title = replaceText(action.title, str, str2, z2);
                    if (!TextUtils.isEmpty(action.title)) {
                        arrayList3.add(action);
                    }
                }
                if (!arrayList3.isEmpty()) {
                    notification.actions = (Notification.Action[]) arrayList3.toArray(new Notification.Action[0]);
                } else {
                    notification.actions = null;
                }
            }
            try {
                Bundle bundle5 = extras.getBundle("android.wearable.EXTENSIONS");
                if (bundle5 != null) {
                    if (z6 && (parcelableArrayList = bundle5.getParcelableArrayList("actions")) != null) {
                        Iterator it = parcelableArrayList.iterator();
                        while (it.hasNext()) {
                            Notification.Action action2 = (Notification.Action) it.next();
                            action2.title = replaceText(action2.title, str, str2, z2);
                            if (TextUtils.isEmpty(action2.title)) {
                                it.remove();
                            }
                        }
                    }
                    Parcelable[] parcelableArray3 = bundle5.getParcelableArray("pages");
                    if (parcelableArray3 != null) {
                        int length3 = parcelableArray3.length;
                        int i5 = 0;
                        while (i5 < length3) {
                            try {
                                notification2 = (Notification) parcelableArray3[i5];
                                parcelableArr = parcelableArray3;
                                str3 = str4;
                                i2 = length3;
                                bundle = extras;
                                i = i5;
                            } catch (Exception e3) {
                                e2 = e3;
                                i = i5;
                                parcelableArr = parcelableArray3;
                                i2 = length3;
                                str3 = str4;
                                bundle = extras;
                            }
                            try {
                                replaceNotificationText(notification2, str, str2, z2, z3, z4, z5, z6);
                            } catch (Exception e4) {
                                e2 = e4;
                                try {
                                    Log.w(TAG, e2);
                                    i5 = i + 1;
                                    str4 = str3;
                                    extras = bundle;
                                    length3 = i2;
                                    parcelableArray3 = parcelableArr;
                                } catch (Exception e5) {
                                    e = e5;
                                    Log.w(TAG, e);
                                    bundle2 = bundle.getBundle(NotificationCompat.CarExtender.EXTRA_CAR_EXTENDER);
                                    if (bundle2 == null) {
                                        return;
                                    }
                                    return;
                                }
                            }
                            i5 = i + 1;
                            str4 = str3;
                            extras = bundle;
                            length3 = i2;
                            parcelableArray3 = parcelableArr;
                        }
                    }
                }
                str3 = str4;
                bundle = extras;
            } catch (Exception e6) {
                e = e6;
                str3 = str4;
                bundle = extras;
            }
            try {
                bundle2 = bundle.getBundle(NotificationCompat.CarExtender.EXTRA_CAR_EXTENDER);
                if (!(bundle2 == null || !z5 || (bundle3 = bundle2.getBundle("car_conversation")) == null || (parcelableArray = bundle3.getParcelableArray("messages")) == null)) {
                    for (Parcelable parcelable2 : parcelableArray) {
                        Bundle bundle6 = (Bundle) parcelable2;
                        CharSequence charSequence12 = bundle6.getCharSequence("author");
                        if (!TextUtils.isEmpty(charSequence12)) {
                            bundle6.putCharSequence("author", replaceText(charSequence12, str, str2, z2));
                        }
                        CharSequence charSequence13 = bundle6.getCharSequence(str3);
                        if (!TextUtils.isEmpty(charSequence13)) {
                            bundle6.putCharSequence(str3, replaceText(charSequence13, str, str2, z2));
                        }
                    }
                }
            } catch (Exception e7) {
                Log.w(TAG, e7);
            }
        }
    }

    private CharSequence replaceText(CharSequence charSequence, String str, CharSequence charSequence2, boolean z2) {
        if (TextUtils.isEmpty(charSequence)) {
            return charSequence;
        }
        if (TextUtils.isEmpty(str)) {
            return charSequence2;
        }
        while (true) {
            try {
                CharSequence replace = replace(charSequence, str, charSequence2, z2);
                if (charSequence.toString().equals(replace.toString())) {
                    break;
                }
                charSequence = replace;
            } catch (Exception e) {
                Log.w(TAG, e);
            }
        }
        return charSequence;
    }

    private static CharSequence replace(CharSequence charSequence, String str, CharSequence charSequence2, boolean z2) {
        int i;
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        if (z2) {
            i = charSequence.toString().toLowerCase(Locale.ENGLISH).indexOf(str.toLowerCase(Locale.ENGLISH));
        } else {
            i = charSequence.toString().indexOf(str);
        }
        if (i == -1) {
            return charSequence;
        }
        spannableStringBuilder.setSpan(str, i, str.length() + i, 33);
        int spanStart = spannableStringBuilder.getSpanStart(str);
        int spanEnd = spannableStringBuilder.getSpanEnd(str);
        if (spanStart != -1) {
            spannableStringBuilder.replace(spanStart, spanEnd, charSequence2);
        }
        return spannableStringBuilder;
    }
}
