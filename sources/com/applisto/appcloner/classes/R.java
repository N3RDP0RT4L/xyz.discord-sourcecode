package com.applisto.appcloner.classes;
/* loaded from: classes4.dex */
public final class R {

    /* loaded from: classes4.dex */
    public static final class drawable {
        public static final int circle_notification_dot = 0x7f010000;
        public static final int ic_launcher_notification_dot = 0x7f010001;
    }

    /* loaded from: classes4.dex */
    public static final class id {
        public static final int picture = 0x7f020000;
        public static final int root = 0x7f020001;
        public static final int shoot = 0x7f020002;
        public static final int text = 0x7f020003;
        public static final int texture = 0x7f020004;
    }

    /* loaded from: classes4.dex */
    public static final class layout {
        public static final int camera2_activity = 0x7f030000;
        public static final int camera_activity = 0x7f030001;
        public static final int main = 0x7f030002;
    }

    /* loaded from: classes4.dex */
    public static final class mipmap {
        public static final int ic_launcher = 0x7f040000;
    }

    /* loaded from: classes4.dex */
    public static final class string {
        public static final int app_name = 0x7f050000;
    }

    /* loaded from: classes4.dex */
    public static final class xml {
        public static final int device_admin = 0x7f060000;
    }
}
