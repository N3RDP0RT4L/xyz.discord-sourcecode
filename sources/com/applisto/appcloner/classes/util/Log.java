package com.applisto.appcloner.classes.util;

import com.applisto.appcloner.classes.Utils;
/* loaded from: classes4.dex */
public class Log {
    public static boolean sEnabled = Utils.isDevDevice();

    private Log() {
    }

    public static int v(String str, String str2) {
        if (!sEnabled) {
            return 0;
        }
        return android.util.Log.v(str, str2);
    }

    public static int v(String str, String str2, Throwable th) {
        if (!sEnabled) {
            return 0;
        }
        return android.util.Log.v(str, str2, th);
    }

    public static int d(String str, String str2) {
        if (!sEnabled) {
            return 0;
        }
        return android.util.Log.d(str, str2);
    }

    public static int d(String str, String str2, Throwable th) {
        if (!sEnabled) {
            return 0;
        }
        return android.util.Log.d(str, str2, th);
    }

    public static int i(String str, String str2) {
        if (!sEnabled) {
            return 0;
        }
        return android.util.Log.i(str, str2);
    }

    public static int i(String str, String str2, Throwable th) {
        if (!sEnabled) {
            return 0;
        }
        return android.util.Log.i(str, str2, th);
    }

    public static int w(String str, String str2) {
        if (!sEnabled) {
            return 0;
        }
        return android.util.Log.w(str, str2);
    }

    public static int w(String str, String str2, Throwable th) {
        if (!sEnabled) {
            return 0;
        }
        return android.util.Log.w(str, str2, th);
    }

    public static int w(String str, Throwable th) {
        if (!sEnabled) {
            return 0;
        }
        return android.util.Log.w(str, th);
    }

    public static int e(String str, String str2) {
        if (!sEnabled) {
            return 0;
        }
        return android.util.Log.e(str, str2);
    }

    public static int e(String str, String str2, Throwable th) {
        if (!sEnabled) {
            return 0;
        }
        return android.util.Log.e(str, str2, th);
    }

    public static String getStackTraceString(Throwable th) {
        return android.util.Log.getStackTraceString(th);
    }
}
