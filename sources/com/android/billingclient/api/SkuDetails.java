package com.android.billingclient.api;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.discord.models.domain.ModelAuditLogEntry;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: com.android.billingclient:billing@@4.0.0 */
/* loaded from: classes.dex */
public class SkuDetails {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final JSONObject f2002b;

    public SkuDetails(@NonNull String str) throws JSONException {
        this.a = str;
        JSONObject jSONObject = new JSONObject(str);
        this.f2002b = jSONObject;
        if (TextUtils.isEmpty(jSONObject.optString("productId"))) {
            throw new IllegalArgumentException("SKU cannot be empty.");
        } else if (TextUtils.isEmpty(jSONObject.optString("type"))) {
            throw new IllegalArgumentException("SkuType cannot be empty.");
        }
    }

    @NonNull
    public String a() {
        return this.f2002b.optString(ModelAuditLogEntry.CHANGE_KEY_DESCRIPTION);
    }

    @NonNull
    public String b() {
        return this.f2002b.optString("price");
    }

    public long c() {
        return this.f2002b.optLong("price_amount_micros");
    }

    @NonNull
    public String d() {
        return this.f2002b.optString("productId");
    }

    @NonNull
    public String e() {
        return this.f2002b.optString("type");
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SkuDetails)) {
            return false;
        }
        return TextUtils.equals(this.a, ((SkuDetails) obj).a);
    }

    @NonNull
    public final String f() {
        return this.f2002b.optString("packageName");
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @NonNull
    public String toString() {
        String valueOf = String.valueOf(this.a);
        return valueOf.length() != 0 ? "SkuDetails: ".concat(valueOf) : new String("SkuDetails: ");
    }
}
