package com.android.billingclient.api;

import androidx.annotation.NonNull;
import b.d.a.a.o;
import java.util.ArrayList;
/* compiled from: com.android.billingclient:billing@@4.0.0 */
/* loaded from: classes.dex */
public class BillingFlowParams {
    public boolean a;

    /* renamed from: b  reason: collision with root package name */
    public String f1997b;
    public String c;
    public String d;
    public int e = 0;
    public ArrayList<SkuDetails> f;
    public boolean g;

    /* compiled from: com.android.billingclient:billing@@4.0.0 */
    /* loaded from: classes.dex */
    public static class a {
        public String a;

        /* renamed from: b  reason: collision with root package name */
        public String f1998b;
        public int c = 0;
        public ArrayList<SkuDetails> d;

        @NonNull
        public BillingFlowParams a() {
            ArrayList<SkuDetails> arrayList = this.d;
            if (arrayList == null || arrayList.isEmpty()) {
                throw new IllegalArgumentException("SkuDetails must be provided.");
            }
            ArrayList<SkuDetails> arrayList2 = this.d;
            int size = arrayList2.size();
            int i = 0;
            while (i < size) {
                i++;
                if (arrayList2.get(i) == null) {
                    throw new IllegalArgumentException("SKU cannot be null.");
                }
            }
            if (this.d.size() > 1) {
                SkuDetails skuDetails = this.d.get(0);
                String e = skuDetails.e();
                ArrayList<SkuDetails> arrayList3 = this.d;
                int size2 = arrayList3.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    SkuDetails skuDetails2 = arrayList3.get(i2);
                    if (!e.equals("play_pass_subs") && !skuDetails2.e().equals("play_pass_subs") && !e.equals(skuDetails2.e())) {
                        throw new IllegalArgumentException("SKUs should have the same type.");
                    }
                }
                String f = skuDetails.f();
                ArrayList<SkuDetails> arrayList4 = this.d;
                int size3 = arrayList4.size();
                for (int i3 = 0; i3 < size3; i3++) {
                    SkuDetails skuDetails3 = arrayList4.get(i3);
                    if (!e.equals("play_pass_subs") && !skuDetails3.e().equals("play_pass_subs") && !f.equals(skuDetails3.f())) {
                        throw new IllegalArgumentException("All SKUs must have the same package name.");
                    }
                }
            }
            BillingFlowParams billingFlowParams = new BillingFlowParams(null);
            billingFlowParams.a = true ^ this.d.get(0).f().isEmpty();
            billingFlowParams.f1997b = this.a;
            billingFlowParams.d = null;
            billingFlowParams.c = this.f1998b;
            billingFlowParams.e = this.c;
            billingFlowParams.f = this.d;
            billingFlowParams.g = false;
            return billingFlowParams;
        }
    }

    public BillingFlowParams() {
    }

    public /* synthetic */ BillingFlowParams(o oVar) {
    }
}
