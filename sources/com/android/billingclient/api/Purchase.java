package com.android.billingclient.api;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/* compiled from: com.android.billingclient:billing@@4.0.0 */
/* loaded from: classes.dex */
public class Purchase {
    public final String a;

    /* renamed from: b  reason: collision with root package name */
    public final String f2000b;
    public final JSONObject c;

    /* compiled from: com.android.billingclient:billing@@4.0.0 */
    /* loaded from: classes.dex */
    public static class a {
        @Nullable
        public final List<Purchase> a;

        /* renamed from: b  reason: collision with root package name */
        public final BillingResult f2001b;

        public a(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {
            this.a = list;
            this.f2001b = billingResult;
        }
    }

    public Purchase(@NonNull String str, @NonNull String str2) throws JSONException {
        this.a = str;
        this.f2000b = str2;
        this.c = new JSONObject(str);
    }

    @NonNull
    public String a() {
        JSONObject jSONObject = this.c;
        return jSONObject.optString("token", jSONObject.optString("purchaseToken"));
    }

    @NonNull
    public ArrayList<String> b() {
        ArrayList<String> arrayList = new ArrayList<>();
        if (this.c.has("productIds")) {
            JSONArray optJSONArray = this.c.optJSONArray("productIds");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    arrayList.add(optJSONArray.optString(i));
                }
            }
        } else if (this.c.has("productId")) {
            arrayList.add(this.c.optString("productId"));
        }
        return arrayList;
    }

    public boolean c() {
        return this.c.optBoolean("acknowledged", true);
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Purchase)) {
            return false;
        }
        Purchase purchase = (Purchase) obj;
        return TextUtils.equals(this.a, purchase.a) && TextUtils.equals(this.f2000b, purchase.f2000b);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    @NonNull
    public String toString() {
        String valueOf = String.valueOf(this.a);
        return valueOf.length() != 0 ? "Purchase. Json: ".concat(valueOf) : new String("Purchase. Json: ");
    }
}
