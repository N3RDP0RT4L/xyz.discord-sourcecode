package com.zwitserloot.cmdreader;
/* loaded from: xyz.discord_v112014.apk:com/zwitserloot/cmdreader/InvalidCommandLineException.SCL.lombok */
public class InvalidCommandLineException extends Exception {
    private static final long serialVersionUID = 20080509;

    public InvalidCommandLineException(String message) {
        super(message);
    }
}
