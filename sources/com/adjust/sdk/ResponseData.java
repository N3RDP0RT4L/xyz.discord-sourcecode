package com.adjust.sdk;

import org.json.JSONObject;
/* loaded from: classes.dex */
public class ResponseData {
    public ActivityKind activityKind;
    public String adid;
    public AdjustAttribution attribution;
    public JSONObject jsonResponse;
    public String message;
    public boolean success;
    public String timestamp;
    public TrackingState trackingState;
    public boolean willRetry;

    /* renamed from: com.adjust.sdk.ResponseData$1  reason: invalid class name */
    /* loaded from: classes.dex */
    public static /* synthetic */ class AnonymousClass1 {
        public static final /* synthetic */ int[] $SwitchMap$com$adjust$sdk$ActivityKind;

        static {
            ActivityKind.values();
            int[] iArr = new int[12];
            $SwitchMap$com$adjust$sdk$ActivityKind = iArr;
            try {
                iArr[ActivityKind.SESSION.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$adjust$sdk$ActivityKind[ActivityKind.CLICK.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$adjust$sdk$ActivityKind[ActivityKind.ATTRIBUTION.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$adjust$sdk$ActivityKind[ActivityKind.EVENT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public static ResponseData buildResponseData(ActivityPackage activityPackage) {
        ResponseData responseData;
        ResponseData responseData2;
        ActivityKind activityKind = activityPackage.getActivityKind();
        int ordinal = activityKind.ordinal();
        if (ordinal == 1) {
            responseData2 = new SessionResponseData(activityPackage);
        } else if (ordinal != 2) {
            if (ordinal == 3) {
                responseData = new SdkClickResponseData();
            } else if (ordinal != 4) {
                responseData = new ResponseData();
            } else {
                responseData = new AttributionResponseData();
            }
            responseData.activityKind = activityKind;
            return responseData;
        } else {
            responseData2 = new EventResponseData(activityPackage);
        }
        responseData = responseData2;
        responseData.activityKind = activityKind;
        return responseData;
    }

    public String toString() {
        return Util.formatString("message:%s timestamp:%s json:%s", this.message, this.timestamp, this.jsonResponse);
    }
}
