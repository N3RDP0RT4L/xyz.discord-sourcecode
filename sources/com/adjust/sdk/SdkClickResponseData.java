package com.adjust.sdk;
/* loaded from: classes.dex */
public class SdkClickResponseData extends ResponseData {
    public long clickTime;
    public long installBegin;
    public String installReferrer;
    public boolean isInstallReferrer;
    public String referrerApi;
}
