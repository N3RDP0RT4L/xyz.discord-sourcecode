package com.airbnb.lottie;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.PathMeasure;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import androidx.annotation.DrawableRes;
import androidx.annotation.FloatRange;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RawRes;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.view.ViewCompat;
import b.c.a.b0.g;
import b.c.a.d;
import b.c.a.e;
import b.c.a.h;
import b.c.a.i;
import b.c.a.j;
import b.c.a.l;
import b.c.a.n;
import b.c.a.o;
import b.c.a.r;
import b.c.a.s;
import b.c.a.t;
import b.c.a.u;
import b.c.a.v;
import b.c.a.y.f;
import com.google.android.material.shadow.ShadowDrawableWrapper;
import java.io.ByteArrayInputStream;
import java.io.InterruptedIOException;
import java.lang.ref.WeakReference;
import java.net.ProtocolException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.nio.channels.ClosedChannelException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.net.ssl.SSLException;
/* loaded from: classes.dex */
public class LottieAnimationView extends AppCompatImageView {
    public static final String j = LottieAnimationView.class.getSimpleName();
    public static final l<Throwable> k = new a();
    @Nullable
    public r<d> B;
    @Nullable
    public d C;
    @Nullable
    public l<Throwable> n;
    public final j p;
    public boolean q;
    public String r;
    @RawRes

    /* renamed from: s  reason: collision with root package name */
    public int f1993s;
    public boolean v;
    public boolean w;

    /* renamed from: x  reason: collision with root package name */
    public boolean f1994x;
    public final l<d> l = new b();
    public final l<Throwable> m = new c();
    @DrawableRes
    public int o = 0;
    public boolean t = false;
    public boolean u = false;

    /* renamed from: y  reason: collision with root package name */
    public t f1995y = t.AUTOMATIC;

    /* renamed from: z  reason: collision with root package name */
    public Set<n> f1996z = new HashSet();
    public int A = 0;

    /* loaded from: classes.dex */
    public static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new a();
        public String j;
        public int k;
        public float l;
        public boolean m;
        public String n;
        public int o;
        public int p;

        /* loaded from: classes.dex */
        public class a implements Parcelable.Creator<SavedState> {
            @Override // android.os.Parcelable.Creator
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel, null);
            }

            @Override // android.os.Parcelable.Creator
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        @Override // android.view.View.BaseSavedState, android.view.AbsSavedState, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.j);
            parcel.writeFloat(this.l);
            parcel.writeInt(this.m ? 1 : 0);
            parcel.writeString(this.n);
            parcel.writeInt(this.o);
            parcel.writeInt(this.p);
        }

        public SavedState(Parcel parcel, a aVar) {
            super(parcel);
            this.j = parcel.readString();
            this.l = parcel.readFloat();
            this.m = parcel.readInt() != 1 ? false : true;
            this.n = parcel.readString();
            this.o = parcel.readInt();
            this.p = parcel.readInt();
        }
    }

    /* loaded from: classes.dex */
    public class a implements l<Throwable> {
        @Override // b.c.a.l
        public void a(Throwable th) {
            Throwable th2 = th;
            PathMeasure pathMeasure = g.a;
            if ((th2 instanceof SocketException) || (th2 instanceof ClosedChannelException) || (th2 instanceof InterruptedIOException) || (th2 instanceof ProtocolException) || (th2 instanceof SSLException) || (th2 instanceof UnknownHostException) || (th2 instanceof UnknownServiceException)) {
                b.c.a.b0.c.c("Unable to load composition.", th2);
                return;
            }
            throw new IllegalStateException("Unable to parse composition", th2);
        }
    }

    /* loaded from: classes.dex */
    public class b implements l<d> {
        public b() {
        }

        @Override // b.c.a.l
        public void a(d dVar) {
            LottieAnimationView.this.setComposition(dVar);
        }
    }

    /* loaded from: classes.dex */
    public class c implements l<Throwable> {
        public c() {
        }

        @Override // b.c.a.l
        public void a(Throwable th) {
            Throwable th2 = th;
            LottieAnimationView lottieAnimationView = LottieAnimationView.this;
            int i = lottieAnimationView.o;
            if (i != 0) {
                lottieAnimationView.setImageResource(i);
            }
            l<Throwable> lVar = LottieAnimationView.this.n;
            if (lVar == null) {
                String str = LottieAnimationView.j;
                lVar = LottieAnimationView.k;
            }
            lVar.a(th2);
        }
    }

    public LottieAnimationView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        String string;
        boolean z2 = false;
        j jVar = new j();
        this.p = jVar;
        this.v = false;
        this.w = false;
        this.f1994x = true;
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R.b.LottieAnimationView, R.a.lottieAnimationViewStyle, 0);
        if (!isInEditMode()) {
            this.f1994x = obtainStyledAttributes.getBoolean(R.b.LottieAnimationView_lottie_cacheComposition, true);
            int i = R.b.LottieAnimationView_lottie_rawRes;
            boolean hasValue = obtainStyledAttributes.hasValue(i);
            int i2 = R.b.LottieAnimationView_lottie_fileName;
            boolean hasValue2 = obtainStyledAttributes.hasValue(i2);
            int i3 = R.b.LottieAnimationView_lottie_url;
            boolean hasValue3 = obtainStyledAttributes.hasValue(i3);
            if (!hasValue || !hasValue2) {
                if (hasValue) {
                    int resourceId = obtainStyledAttributes.getResourceId(i, 0);
                    if (resourceId != 0) {
                        setAnimation(resourceId);
                    }
                } else if (hasValue2) {
                    String string2 = obtainStyledAttributes.getString(i2);
                    if (string2 != null) {
                        setAnimation(string2);
                    }
                } else if (hasValue3 && (string = obtainStyledAttributes.getString(i3)) != null) {
                    setAnimationFromUrl(string);
                }
                setFallbackResource(obtainStyledAttributes.getResourceId(R.b.LottieAnimationView_lottie_fallbackRes, 0));
            } else {
                throw new IllegalArgumentException("lottie_rawRes and lottie_fileName cannot be used at the same time. Please use only one at once.");
            }
        }
        if (obtainStyledAttributes.getBoolean(R.b.LottieAnimationView_lottie_autoPlay, false)) {
            this.v = true;
            this.w = true;
        }
        if (obtainStyledAttributes.getBoolean(R.b.LottieAnimationView_lottie_loop, false)) {
            jVar.l.setRepeatCount(-1);
        }
        int i4 = R.b.LottieAnimationView_lottie_repeatMode;
        if (obtainStyledAttributes.hasValue(i4)) {
            setRepeatMode(obtainStyledAttributes.getInt(i4, 1));
        }
        int i5 = R.b.LottieAnimationView_lottie_repeatCount;
        if (obtainStyledAttributes.hasValue(i5)) {
            setRepeatCount(obtainStyledAttributes.getInt(i5, -1));
        }
        int i6 = R.b.LottieAnimationView_lottie_speed;
        if (obtainStyledAttributes.hasValue(i6)) {
            setSpeed(obtainStyledAttributes.getFloat(i6, 1.0f));
        }
        setImageAssetsFolder(obtainStyledAttributes.getString(R.b.LottieAnimationView_lottie_imageAssetsFolder));
        setProgress(obtainStyledAttributes.getFloat(R.b.LottieAnimationView_lottie_progress, 0.0f));
        boolean z3 = obtainStyledAttributes.getBoolean(R.b.LottieAnimationView_lottie_enableMergePathsForKitKatAndAbove, false);
        if (jVar.w != z3) {
            jVar.w = z3;
            if (jVar.k != null) {
                jVar.b();
            }
        }
        int i7 = R.b.LottieAnimationView_lottie_colorFilter;
        if (obtainStyledAttributes.hasValue(i7)) {
            jVar.a(new f("**"), o.C, new b.c.a.c0.c(new u(obtainStyledAttributes.getColor(i7, 0))));
        }
        int i8 = R.b.LottieAnimationView_lottie_scale;
        if (obtainStyledAttributes.hasValue(i8)) {
            jVar.m = obtainStyledAttributes.getFloat(i8, 1.0f);
            jVar.v();
        }
        int i9 = R.b.LottieAnimationView_lottie_renderMode;
        if (obtainStyledAttributes.hasValue(i9)) {
            int i10 = obtainStyledAttributes.getInt(i9, 0);
            t.values();
            setRenderMode(t.values()[i10 >= 3 ? 0 : i10]);
        }
        if (getScaleType() != null) {
            jVar.r = getScaleType();
        }
        obtainStyledAttributes.recycle();
        Context context2 = getContext();
        PathMeasure pathMeasure = g.a;
        Boolean valueOf = Boolean.valueOf(Settings.Global.getFloat(context2.getContentResolver(), "animator_duration_scale", 1.0f) != 0.0f ? true : z2);
        Objects.requireNonNull(jVar);
        jVar.n = valueOf.booleanValue();
        b();
        this.q = true;
    }

    private void setCompositionTask(r<d> rVar) {
        this.C = null;
        this.p.c();
        a();
        rVar.b(this.l);
        rVar.a(this.m);
        this.B = rVar;
    }

    public final void a() {
        r<d> rVar = this.B;
        if (rVar != null) {
            l<d> lVar = this.l;
            synchronized (rVar) {
                rVar.f370b.remove(lVar);
            }
            r<d> rVar2 = this.B;
            l<Throwable> lVar2 = this.m;
            synchronized (rVar2) {
                rVar2.c.remove(lVar2);
            }
        }
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x0027, code lost:
        if (r3 != false) goto L17;
     */
    /* JADX WARN: Code restructure failed: missing block: B:4:0x000a, code lost:
        if (r0 != 1) goto L5;
     */
    /* JADX WARN: Code restructure failed: missing block: B:5:0x000c, code lost:
        r1 = 1;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void b() {
        /*
            r6 = this;
            b.c.a.t r0 = r6.f1995y
            int r0 = r0.ordinal()
            r1 = 2
            r2 = 1
            if (r0 == 0) goto Le
            if (r0 == r2) goto L29
        Lc:
            r1 = 1
            goto L29
        Le:
            b.c.a.d r0 = r6.C
            r3 = 0
            if (r0 == 0) goto L1e
            boolean r4 = r0.n
            if (r4 == 0) goto L1e
            int r4 = android.os.Build.VERSION.SDK_INT
            r5 = 28
            if (r4 >= r5) goto L1e
            goto L27
        L1e:
            if (r0 == 0) goto L26
            int r0 = r0.o
            r4 = 4
            if (r0 <= r4) goto L26
            goto L27
        L26:
            r3 = 1
        L27:
            if (r3 == 0) goto Lc
        L29:
            int r0 = r6.getLayerType()
            if (r1 == r0) goto L33
            r0 = 0
            r6.setLayerType(r1, r0)
        L33:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.airbnb.lottie.LottieAnimationView.b():void");
    }

    @Override // android.view.View
    public void buildDrawingCache(boolean z2) {
        this.A++;
        super.buildDrawingCache(z2);
        if (this.A == 1 && getWidth() > 0 && getHeight() > 0 && getLayerType() == 1 && getDrawingCache(z2) == null) {
            setRenderMode(t.HARDWARE);
        }
        this.A--;
        b.c.a.c.a("buildDrawingCache");
    }

    @MainThread
    public void c() {
        this.w = false;
        this.v = false;
        this.u = false;
        this.t = false;
        j jVar = this.p;
        jVar.p.clear();
        jVar.l.m();
        b();
    }

    @MainThread
    public void d() {
        if (isShown()) {
            this.p.j();
            b();
            return;
        }
        this.t = true;
    }

    @Nullable
    public d getComposition() {
        return this.C;
    }

    public long getDuration() {
        d dVar = this.C;
        if (dVar != null) {
            return dVar.b();
        }
        return 0L;
    }

    public int getFrame() {
        return (int) this.p.l.o;
    }

    @Nullable
    public String getImageAssetsFolder() {
        return this.p.t;
    }

    public float getMaxFrame() {
        return this.p.e();
    }

    public float getMinFrame() {
        return this.p.f();
    }

    @Nullable
    public s getPerformanceTracker() {
        d dVar = this.p.k;
        if (dVar != null) {
            return dVar.a;
        }
        return null;
    }

    @FloatRange(from = ShadowDrawableWrapper.COS_45, to = 1.0d)
    public float getProgress() {
        return this.p.g();
    }

    public int getRepeatCount() {
        return this.p.h();
    }

    public int getRepeatMode() {
        return this.p.l.getRepeatMode();
    }

    public float getScale() {
        return this.p.m;
    }

    public float getSpeed() {
        return this.p.l.l;
    }

    @Override // android.widget.ImageView, android.view.View, android.graphics.drawable.Drawable.Callback
    public void invalidateDrawable(@NonNull Drawable drawable) {
        Drawable drawable2 = getDrawable();
        j jVar = this.p;
        if (drawable2 == jVar) {
            super.invalidateDrawable(jVar);
        } else {
            super.invalidateDrawable(drawable);
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.w || this.v) {
            d();
            this.w = false;
            this.v = false;
        }
        if (Build.VERSION.SDK_INT < 23) {
            onVisibilityChanged(this, getVisibility());
        }
    }

    @Override // android.widget.ImageView, android.view.View
    public void onDetachedFromWindow() {
        if (this.p.i()) {
            this.v = false;
            this.u = false;
            this.t = false;
            j jVar = this.p;
            jVar.p.clear();
            jVar.l.cancel();
            b();
            this.v = true;
        }
        super.onDetachedFromWindow();
    }

    @Override // android.view.View
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        String str = savedState.j;
        this.r = str;
        if (!TextUtils.isEmpty(str)) {
            setAnimation(this.r);
        }
        int i = savedState.k;
        this.f1993s = i;
        if (i != 0) {
            setAnimation(i);
        }
        setProgress(savedState.l);
        if (savedState.m) {
            d();
        }
        this.p.t = savedState.n;
        setRepeatMode(savedState.o);
        setRepeatCount(savedState.p);
    }

    @Override // android.view.View
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.j = this.r;
        savedState.k = this.f1993s;
        savedState.l = this.p.g();
        savedState.m = this.p.i() || (!ViewCompat.isAttachedToWindow(this) && this.v);
        j jVar = this.p;
        savedState.n = jVar.t;
        savedState.o = jVar.l.getRepeatMode();
        savedState.p = this.p.h();
        return savedState;
    }

    @Override // android.view.View
    public void onVisibilityChanged(@NonNull View view, int i) {
        if (this.q) {
            if (isShown()) {
                if (this.u) {
                    if (isShown()) {
                        this.p.k();
                        b();
                    } else {
                        this.t = false;
                        this.u = true;
                    }
                } else if (this.t) {
                    d();
                }
                this.u = false;
                this.t = false;
            } else if (this.p.i()) {
                c();
                this.u = true;
            }
        }
    }

    public void setAnimation(@RawRes int i) {
        r<d> rVar;
        this.f1993s = i;
        this.r = null;
        if (this.f1994x) {
            Context context = getContext();
            rVar = e.a(e.f(context, i), new h(new WeakReference(context), context.getApplicationContext(), i));
        } else {
            Context context2 = getContext();
            Map<String, r<d>> map = e.a;
            rVar = e.a(null, new h(new WeakReference(context2), context2.getApplicationContext(), i));
        }
        setCompositionTask(rVar);
    }

    @Deprecated
    public void setAnimationFromJson(String str) {
        setCompositionTask(e.a(null, new i(new ByteArrayInputStream(str.getBytes()), null)));
    }

    public void setAnimationFromUrl(String str) {
        r<d> rVar;
        if (this.f1994x) {
            Context context = getContext();
            Map<String, r<d>> map = e.a;
            String v = b.d.b.a.a.v("url_", str);
            rVar = e.a(v, new b.c.a.f(context, str, v));
        } else {
            rVar = e.a(null, new b.c.a.f(getContext(), str, null));
        }
        setCompositionTask(rVar);
    }

    public void setApplyingOpacityToLayersEnabled(boolean z2) {
        this.p.A = z2;
    }

    public void setCacheComposition(boolean z2) {
        this.f1994x = z2;
    }

    public void setComposition(@NonNull d dVar) {
        this.p.setCallback(this);
        this.C = dVar;
        j jVar = this.p;
        boolean z2 = false;
        if (jVar.k != dVar) {
            jVar.C = false;
            jVar.c();
            jVar.k = dVar;
            jVar.b();
            b.c.a.b0.d dVar2 = jVar.l;
            if (dVar2.f340s == null) {
                z2 = true;
            }
            dVar2.f340s = dVar;
            if (z2) {
                dVar2.o((int) Math.max(dVar2.q, dVar.k), (int) Math.min(dVar2.r, dVar.l));
            } else {
                dVar2.o((int) dVar.k, (int) dVar.l);
            }
            float f = dVar2.o;
            dVar2.o = 0.0f;
            dVar2.n((int) f);
            dVar2.f();
            jVar.u(jVar.l.getAnimatedFraction());
            jVar.m = jVar.m;
            jVar.v();
            jVar.v();
            Iterator it = new ArrayList(jVar.p).iterator();
            while (it.hasNext()) {
                ((j.o) it.next()).a(dVar);
                it.remove();
            }
            jVar.p.clear();
            dVar.a.a = jVar.f351z;
            Drawable.Callback callback = jVar.getCallback();
            if (callback instanceof ImageView) {
                ImageView imageView = (ImageView) callback;
                imageView.setImageDrawable(null);
                imageView.setImageDrawable(jVar);
            }
            z2 = true;
        }
        b();
        if (getDrawable() != this.p || z2) {
            onVisibilityChanged(this, getVisibility());
            requestLayout();
            for (n nVar : this.f1996z) {
                nVar.a(dVar);
            }
        }
    }

    public void setFailureListener(@Nullable l<Throwable> lVar) {
        this.n = lVar;
    }

    public void setFallbackResource(@DrawableRes int i) {
        this.o = i;
    }

    public void setFontAssetDelegate(b.c.a.a aVar) {
        b.c.a.x.a aVar2 = this.p.v;
    }

    public void setFrame(int i) {
        this.p.l(i);
    }

    public void setImageAssetDelegate(b.c.a.b bVar) {
        j jVar = this.p;
        jVar.u = bVar;
        b.c.a.x.b bVar2 = jVar.f348s;
        if (bVar2 != null) {
            bVar2.d = bVar;
        }
    }

    public void setImageAssetsFolder(String str) {
        this.p.t = str;
    }

    @Override // androidx.appcompat.widget.AppCompatImageView, android.widget.ImageView
    public void setImageBitmap(Bitmap bitmap) {
        a();
        super.setImageBitmap(bitmap);
    }

    @Override // androidx.appcompat.widget.AppCompatImageView, android.widget.ImageView
    public void setImageDrawable(Drawable drawable) {
        a();
        super.setImageDrawable(drawable);
    }

    @Override // androidx.appcompat.widget.AppCompatImageView, android.widget.ImageView
    public void setImageResource(int i) {
        a();
        super.setImageResource(i);
    }

    public void setMaxFrame(int i) {
        this.p.m(i);
    }

    public void setMaxProgress(@FloatRange(from = 0.0d, to = 1.0d) float f) {
        this.p.o(f);
    }

    public void setMinAndMaxFrame(String str) {
        this.p.q(str);
    }

    public void setMinFrame(int i) {
        this.p.r(i);
    }

    public void setMinProgress(float f) {
        this.p.t(f);
    }

    public void setPerformanceTrackingEnabled(boolean z2) {
        j jVar = this.p;
        jVar.f351z = z2;
        d dVar = jVar.k;
        if (dVar != null) {
            dVar.a.a = z2;
        }
    }

    public void setProgress(@FloatRange(from = 0.0d, to = 1.0d) float f) {
        this.p.u(f);
    }

    public void setRenderMode(t tVar) {
        this.f1995y = tVar;
        b();
    }

    public void setRepeatCount(int i) {
        this.p.l.setRepeatCount(i);
    }

    public void setRepeatMode(int i) {
        this.p.l.setRepeatMode(i);
    }

    public void setSafeMode(boolean z2) {
        this.p.o = z2;
    }

    public void setScale(float f) {
        j jVar = this.p;
        jVar.m = f;
        jVar.v();
        if (getDrawable() == this.p) {
            setImageDrawable(null);
            setImageDrawable(this.p);
        }
    }

    @Override // android.widget.ImageView
    public void setScaleType(ImageView.ScaleType scaleType) {
        super.setScaleType(scaleType);
        j jVar = this.p;
        if (jVar != null) {
            jVar.r = scaleType;
        }
    }

    public void setSpeed(float f) {
        this.p.l.l = f;
    }

    public void setTextDelegate(v vVar) {
        Objects.requireNonNull(this.p);
    }

    public void setMaxFrame(String str) {
        this.p.n(str);
    }

    public void setMinFrame(String str) {
        this.p.s(str);
    }

    public void setAnimation(String str) {
        r<d> rVar;
        this.r = str;
        this.f1993s = 0;
        if (this.f1994x) {
            Context context = getContext();
            Map<String, r<d>> map = e.a;
            String v = b.d.b.a.a.v("asset_", str);
            rVar = e.a(v, new b.c.a.g(context.getApplicationContext(), str, v));
        } else {
            Context context2 = getContext();
            Map<String, r<d>> map2 = e.a;
            rVar = e.a(null, new b.c.a.g(context2.getApplicationContext(), str, null));
        }
        setCompositionTask(rVar);
    }
}
