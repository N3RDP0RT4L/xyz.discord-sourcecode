package com.fasterxml.jackson.databind;

import b.g.a.a.m;
import b.g.a.b.f;
import b.g.a.c.i0.d;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.Closeable;
import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Objects;
/* loaded from: classes2.dex */
public class JsonMappingException extends JsonProcessingException {
    public static final /* synthetic */ int j = 0;
    private static final long serialVersionUID = 1;
    public LinkedList<a> _path;
    public transient Closeable k;

    public JsonMappingException(Closeable closeable, String str) {
        super(str);
        this.k = closeable;
        if (closeable instanceof f) {
            this._location = ((f) closeable).a();
        }
    }

    public static JsonMappingException f(Throwable th, a aVar) {
        JsonMappingException jsonMappingException;
        if (th instanceof JsonMappingException) {
            jsonMappingException = (JsonMappingException) th;
        } else {
            String h = d.h(th);
            if (h == null || h.isEmpty()) {
                StringBuilder R = b.d.b.a.a.R("(was ");
                R.append(th.getClass().getName());
                R.append(")");
                h = R.toString();
            }
            Closeable closeable = null;
            if (th instanceof JsonProcessingException) {
                Object c = ((JsonProcessingException) th).c();
                if (c instanceof Closeable) {
                    closeable = (Closeable) c;
                }
            }
            jsonMappingException = new JsonMappingException(closeable, h, th);
        }
        jsonMappingException.e(aVar);
        return jsonMappingException;
    }

    @Override // com.fasterxml.jackson.core.JsonProcessingException
    @m
    public Object c() {
        return this.k;
    }

    public String d() {
        String message = super.getMessage();
        if (this._path == null) {
            return message;
        }
        StringBuilder sb = message == null ? new StringBuilder() : new StringBuilder(message);
        sb.append(" (through reference chain: ");
        LinkedList<a> linkedList = this._path;
        if (linkedList != null) {
            Iterator<a> it = linkedList.iterator();
            while (it.hasNext()) {
                sb.append(it.next().a());
                if (it.hasNext()) {
                    sb.append("->");
                }
            }
        }
        sb.append(')');
        return sb.toString();
    }

    public void e(a aVar) {
        if (this._path == null) {
            this._path = new LinkedList<>();
        }
        if (this._path.size() < 1000) {
            this._path.addFirst(aVar);
        }
    }

    @Override // java.lang.Throwable
    public String getLocalizedMessage() {
        return d();
    }

    @Override // com.fasterxml.jackson.core.JsonProcessingException, java.lang.Throwable
    public String getMessage() {
        return d();
    }

    @Override // com.fasterxml.jackson.core.JsonProcessingException, java.lang.Throwable
    public String toString() {
        return getClass().getName() + ": " + getMessage();
    }

    /* loaded from: classes2.dex */
    public static class a implements Serializable {
        private static final long serialVersionUID = 2;
        public String _desc;
        public String _fieldName;
        public int _index;
        public transient Object j;

        public a() {
            this._index = -1;
        }

        public String a() {
            if (this._desc == null) {
                StringBuilder sb = new StringBuilder();
                Object obj = this.j;
                if (obj != null) {
                    Class<?> cls = obj instanceof Class ? (Class) obj : obj.getClass();
                    int i = 0;
                    while (cls.isArray()) {
                        cls = cls.getComponentType();
                        i++;
                    }
                    sb.append(cls.getName());
                    while (true) {
                        i--;
                        if (i < 0) {
                            break;
                        }
                        sb.append("[]");
                    }
                } else {
                    sb.append("UNKNOWN");
                }
                sb.append('[');
                if (this._fieldName != null) {
                    sb.append('\"');
                    sb.append(this._fieldName);
                    sb.append('\"');
                } else {
                    int i2 = this._index;
                    if (i2 >= 0) {
                        sb.append(i2);
                    } else {
                        sb.append('?');
                    }
                }
                sb.append(']');
                this._desc = sb.toString();
            }
            return this._desc;
        }

        public String toString() {
            return a();
        }

        public Object writeReplace() {
            a();
            return this;
        }

        public a(Object obj, String str) {
            this._index = -1;
            this.j = obj;
            Objects.requireNonNull(str, "Cannot pass null fieldName");
            this._fieldName = str;
        }

        public a(Object obj, int i) {
            this._index = -1;
            this.j = obj;
            this._index = i;
        }
    }

    public JsonMappingException(Closeable closeable, String str, Throwable th) {
        super(str, th);
        this.k = closeable;
        if (th instanceof JsonProcessingException) {
            this._location = ((JsonProcessingException) th)._location;
        } else if (closeable instanceof f) {
            this._location = ((f) closeable).a();
        }
    }
}
