package com.fasterxml.jackson.databind.exc;
/* loaded from: classes2.dex */
public class InvalidFormatException extends MismatchedInputException {
    private static final long serialVersionUID = 1;
    public final Object _value;
}
