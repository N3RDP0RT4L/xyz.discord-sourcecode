package com.fasterxml.jackson.databind.exc;

import b.g.a.c.j;
/* loaded from: classes2.dex */
public class InvalidTypeIdException extends MismatchedInputException {
    private static final long serialVersionUID = 1;
    public final j _baseType;
    public final String _typeId;
}
