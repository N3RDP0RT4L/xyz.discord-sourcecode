package com.fasterxml.jackson.databind.exc;

import java.util.Collection;
import java.util.Iterator;
/* loaded from: classes2.dex */
public abstract class PropertyBindingException extends MismatchedInputException {
    public final Collection<Object> _propertyIds;
    public final String _propertyName;
    public final Class<?> _referringClass;
    public transient String l;

    @Override // com.fasterxml.jackson.core.JsonProcessingException
    public String a() {
        String str = this.l;
        if (str != null || this._propertyIds == null) {
            return str;
        }
        StringBuilder sb = new StringBuilder(100);
        int size = this._propertyIds.size();
        if (size != 1) {
            sb.append(" (");
            sb.append(size);
            sb.append(" known properties: ");
            Iterator<Object> it = this._propertyIds.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                sb.append('\"');
                sb.append(String.valueOf(it.next()));
                sb.append('\"');
                if (sb.length() > 1000) {
                    sb.append(" [truncated]");
                    break;
                } else if (it.hasNext()) {
                    sb.append(", ");
                }
            }
        } else {
            sb.append(" (one known property: \"");
            sb.append(String.valueOf(this._propertyIds.iterator().next()));
            sb.append('\"');
        }
        sb.append("])");
        String sb2 = sb.toString();
        this.l = sb2;
        return sb2;
    }
}
