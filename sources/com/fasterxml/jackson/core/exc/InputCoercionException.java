package com.fasterxml.jackson.core.exc;

import b.g.a.b.h;
/* loaded from: classes2.dex */
public class InputCoercionException extends StreamReadException {
    private static final long serialVersionUID = 1;
    public final h _inputType;
    public final Class<?> _targetType;
}
