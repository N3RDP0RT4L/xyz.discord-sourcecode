package com.fasterxml.jackson.core.exc;

import b.d.b.a.a;
import b.g.a.b.f;
import b.g.a.b.t.i;
import com.fasterxml.jackson.core.JsonProcessingException;
/* loaded from: classes2.dex */
public abstract class StreamReadException extends JsonProcessingException {
    public static final long serialVersionUID = 1;
    public i _requestPayload;

    /* renamed from: d */
    public f c() {
        return null;
    }

    @Override // com.fasterxml.jackson.core.JsonProcessingException, java.lang.Throwable
    public String getMessage() {
        String message = super.getMessage();
        if (this._requestPayload == null) {
            return message;
        }
        StringBuilder V = a.V(message, "\nRequest payload : ");
        V.append(this._requestPayload.toString());
        return V.toString();
    }
}
