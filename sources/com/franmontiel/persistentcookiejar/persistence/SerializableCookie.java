package com.franmontiel.persistentcookiejar.persistence;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.models.domain.ModelAuditLogEntry;
import com.discord.widgets.chat.input.autocomplete.AutocompleteViewModel;
import d0.g0.t;
import d0.g0.w;
import d0.z.d.m;
import f0.n;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Objects;
/* loaded from: classes2.dex */
public class SerializableCookie implements Serializable {
    public static final String j = SerializableCookie.class.getSimpleName();
    private static final long serialVersionUID = -8594045714036645534L;
    public transient n k;

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        boolean z2;
        long j2;
        boolean z3;
        String str = (String) objectInputStream.readObject();
        m.checkParameterIsNotNull(str, ModelAuditLogEntry.CHANGE_KEY_NAME);
        if (m.areEqual(w.trim(str).toString(), str)) {
            String str2 = (String) objectInputStream.readObject();
            m.checkParameterIsNotNull(str2, "value");
            if (m.areEqual(w.trim(str2).toString(), str2)) {
                long readLong = objectInputStream.readLong();
                long j3 = 253402300799999L;
                if (readLong != -1) {
                    if (readLong <= 0) {
                        readLong = Long.MIN_VALUE;
                    }
                    if (readLong <= 253402300799999L) {
                        j3 = readLong;
                    }
                    j2 = j3;
                    z2 = true;
                } else {
                    j2 = 253402300799999L;
                    z2 = false;
                }
                String str3 = (String) objectInputStream.readObject();
                m.checkParameterIsNotNull(str3, "domain");
                String r1 = f.r1(str3);
                if (r1 != null) {
                    String str4 = (String) objectInputStream.readObject();
                    m.checkParameterIsNotNull(str4, "path");
                    if (t.startsWith$default(str4, AutocompleteViewModel.COMMAND_DISCOVER_TOKEN, false, 2, null)) {
                        boolean z4 = objectInputStream.readBoolean();
                        boolean z5 = objectInputStream.readBoolean();
                        if (objectInputStream.readBoolean()) {
                            m.checkParameterIsNotNull(str3, "domain");
                            String r12 = f.r1(str3);
                            if (r12 != null) {
                                r1 = r12;
                                z3 = true;
                            } else {
                                throw new IllegalArgumentException(a.v("unexpected domain: ", str3));
                            }
                        } else {
                            z3 = false;
                        }
                        Objects.requireNonNull(str, "builder.name == null");
                        Objects.requireNonNull(str2, "builder.value == null");
                        Objects.requireNonNull(r1, "builder.domain == null");
                        this.k = new n(str, str2, j2, r1, str4, z4, z5, z2, z3, null);
                        return;
                    }
                    throw new IllegalArgumentException("path must start with '/'".toString());
                }
                throw new IllegalArgumentException(a.v("unexpected domain: ", str3));
            }
            throw new IllegalArgumentException("value is not trimmed".toString());
        }
        throw new IllegalArgumentException("name is not trimmed".toString());
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(this.k.f);
        objectOutputStream.writeObject(this.k.g);
        n nVar = this.k;
        objectOutputStream.writeLong(nVar.m ? nVar.h : -1L);
        objectOutputStream.writeObject(this.k.i);
        objectOutputStream.writeObject(this.k.j);
        objectOutputStream.writeBoolean(this.k.k);
        objectOutputStream.writeBoolean(this.k.l);
        objectOutputStream.writeBoolean(this.k.n);
    }
}
