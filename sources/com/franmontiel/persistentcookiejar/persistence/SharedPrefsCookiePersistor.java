package com.franmontiel.persistentcookiejar.persistence;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.adjust.sdk.Constants;
import f0.n;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Collection;
@SuppressLint({"CommitPrefEdits"})
/* loaded from: classes2.dex */
public class SharedPrefsCookiePersistor implements CookiePersistor {
    public final SharedPreferences a;

    public SharedPrefsCookiePersistor(Context context) {
        this.a = context.getSharedPreferences("CookiePersistence", 0);
    }

    public static String b(n nVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(nVar.k ? Constants.SCHEME : "http");
        sb.append("://");
        sb.append(nVar.i);
        sb.append(nVar.j);
        sb.append("|");
        sb.append(nVar.f);
        return sb.toString();
    }

    @Override // com.franmontiel.persistentcookiejar.persistence.CookiePersistor
    public void a(Collection<n> collection) {
        Throwable th;
        ObjectOutputStream objectOutputStream;
        IOException e;
        SharedPreferences.Editor edit = this.a.edit();
        for (n nVar : collection) {
            String b2 = b(nVar);
            SerializableCookie serializableCookie = new SerializableCookie();
            serializableCookie.k = nVar;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            String str = null;
            ObjectOutputStream objectOutputStream2 = null;
            str = null;
            str = null;
            try {
                objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                try {
                    try {
                        objectOutputStream.writeObject(serializableCookie);
                        try {
                            objectOutputStream.close();
                        } catch (IOException e2) {
                            Log.d(SerializableCookie.j, "Stream not closed in encodeCookie", e2);
                        }
                        byte[] byteArray = byteArrayOutputStream.toByteArray();
                        StringBuilder sb = new StringBuilder(byteArray.length * 2);
                        for (byte b3 : byteArray) {
                            int i = b3 & 255;
                            if (i < 16) {
                                sb.append('0');
                            }
                            sb.append(Integer.toHexString(i));
                        }
                        str = sb.toString();
                    } catch (IOException e3) {
                        e = e3;
                        Log.d(SerializableCookie.j, "IOException in encodeCookie", e);
                        if (objectOutputStream != null) {
                            try {
                                objectOutputStream.close();
                            } catch (IOException e4) {
                                Log.d(SerializableCookie.j, "Stream not closed in encodeCookie", e4);
                            }
                        }
                        edit.putString(b2, str);
                    }
                } catch (Throwable th2) {
                    th = th2;
                    objectOutputStream2 = objectOutputStream;
                    if (objectOutputStream2 != null) {
                        try {
                            objectOutputStream2.close();
                        } catch (IOException e5) {
                            Log.d(SerializableCookie.j, "Stream not closed in encodeCookie", e5);
                        }
                    }
                    throw th;
                }
            } catch (IOException e6) {
                e = e6;
                objectOutputStream = null;
            } catch (Throwable th3) {
                th = th3;
            }
            edit.putString(b2, str);
        }
        edit.commit();
    }

    /* JADX WARN: Code restructure failed: missing block: B:26:0x0097, code lost:
        if (r5 == null) goto L28;
     */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00a1 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:47:0x001d A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.util.List<f0.n> c() {
        /*
            r11 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            android.content.SharedPreferences r1 = r11.a
            java.util.Map r1 = r1.getAll()
            int r1 = r1.size()
            r0.<init>(r1)
            android.content.SharedPreferences r1 = r11.a
            java.util.Map r1 = r1.getAll()
            java.util.Set r1 = r1.entrySet()
            java.util.Iterator r1 = r1.iterator()
        L1d:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto Lb3
            java.lang.Object r2 = r1.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r2 = r2.getValue()
            java.lang.String r2 = (java.lang.String) r2
            com.franmontiel.persistentcookiejar.persistence.SerializableCookie r3 = new com.franmontiel.persistentcookiejar.persistence.SerializableCookie
            r3.<init>()
            java.lang.String r3 = "Stream not closed in decodeCookie"
            int r4 = r2.length()
            int r5 = r4 / 2
            byte[] r5 = new byte[r5]
            r6 = 0
        L3f:
            if (r6 >= r4) goto L60
            int r7 = r6 / 2
            char r8 = r2.charAt(r6)
            r9 = 16
            int r8 = java.lang.Character.digit(r8, r9)
            int r8 = r8 << 4
            int r10 = r6 + 1
            char r10 = r2.charAt(r10)
            int r9 = java.lang.Character.digit(r10, r9)
            int r9 = r9 + r8
            byte r8 = (byte) r9
            r5[r7] = r8
            int r6 = r6 + 2
            goto L3f
        L60:
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream
            r2.<init>(r5)
            r4 = 0
            java.io.ObjectInputStream r5 = new java.io.ObjectInputStream     // Catch: java.lang.Throwable -> L80 java.lang.ClassNotFoundException -> L82 java.io.IOException -> L8e
            r5.<init>(r2)     // Catch: java.lang.Throwable -> L80 java.lang.ClassNotFoundException -> L82 java.io.IOException -> L8e
            java.lang.Object r2 = r5.readObject()     // Catch: java.lang.Throwable -> L79 java.lang.ClassNotFoundException -> L7c java.io.IOException -> L7e
            com.franmontiel.persistentcookiejar.persistence.SerializableCookie r2 = (com.franmontiel.persistentcookiejar.persistence.SerializableCookie) r2     // Catch: java.lang.Throwable -> L79 java.lang.ClassNotFoundException -> L7c java.io.IOException -> L7e
            f0.n r4 = r2.k     // Catch: java.lang.Throwable -> L79 java.lang.ClassNotFoundException -> L7c java.io.IOException -> L7e
        L73:
            r5.close()     // Catch: java.io.IOException -> L77
            goto L9f
        L77:
            r2 = move-exception
            goto L9a
        L79:
            r0 = move-exception
            r4 = r5
            goto La6
        L7c:
            r2 = move-exception
            goto L84
        L7e:
            r2 = move-exception
            goto L90
        L80:
            r0 = move-exception
            goto La6
        L82:
            r2 = move-exception
            r5 = r4
        L84:
            java.lang.String r6 = com.franmontiel.persistentcookiejar.persistence.SerializableCookie.j     // Catch: java.lang.Throwable -> L79
            java.lang.String r7 = "ClassNotFoundException in decodeCookie"
            android.util.Log.d(r6, r7, r2)     // Catch: java.lang.Throwable -> L79
            if (r5 == 0) goto L9f
            goto L73
        L8e:
            r2 = move-exception
            r5 = r4
        L90:
            java.lang.String r6 = com.franmontiel.persistentcookiejar.persistence.SerializableCookie.j     // Catch: java.lang.Throwable -> L79
            java.lang.String r7 = "IOException in decodeCookie"
            android.util.Log.d(r6, r7, r2)     // Catch: java.lang.Throwable -> L79
            if (r5 == 0) goto L9f
            goto L73
        L9a:
            java.lang.String r5 = com.franmontiel.persistentcookiejar.persistence.SerializableCookie.j
            android.util.Log.d(r5, r3, r2)
        L9f:
            if (r4 == 0) goto L1d
            r0.add(r4)
            goto L1d
        La6:
            if (r4 == 0) goto Lb2
            r4.close()     // Catch: java.io.IOException -> Lac
            goto Lb2
        Lac:
            r1 = move-exception
            java.lang.String r2 = com.franmontiel.persistentcookiejar.persistence.SerializableCookie.j
            android.util.Log.d(r2, r3, r1)
        Lb2:
            throw r0
        Lb3:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor.c():java.util.List");
    }

    @Override // com.franmontiel.persistentcookiejar.persistence.CookiePersistor
    public void removeAll(Collection<n> collection) {
        SharedPreferences.Editor edit = this.a.edit();
        for (n nVar : collection) {
            edit.remove(b(nVar));
        }
        edit.commit();
    }
}
