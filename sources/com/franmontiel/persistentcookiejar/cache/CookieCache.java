package com.franmontiel.persistentcookiejar.cache;

import f0.n;
import java.util.Collection;
/* loaded from: classes2.dex */
public interface CookieCache extends Iterable<n> {
    void addAll(Collection<n> collection);
}
