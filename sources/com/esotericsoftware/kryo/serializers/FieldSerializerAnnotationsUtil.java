package com.esotericsoftware.kryo.serializers;

import b.d.b.a.a;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.factories.ReflectionSerializerFactory;
import com.esotericsoftware.kryo.serializers.CollectionSerializer;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import com.esotericsoftware.kryo.serializers.MapSerializer;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;
/* loaded from: classes2.dex */
public final class FieldSerializerAnnotationsUtil {
    public FieldSerializerAnnotationsUtil(FieldSerializer fieldSerializer) {
    }

    public void processAnnotatedFields(FieldSerializer fieldSerializer) {
        FieldSerializer.CachedField[] fields = fieldSerializer.getFields();
        int length = fields.length;
        for (int i = 0; i < length; i++) {
            Field field = fields[i].getField();
            if (field.isAnnotationPresent(FieldSerializer.Bind.class)) {
                fields[i].setSerializer(ReflectionSerializerFactory.makeSerializer(fieldSerializer.getKryo(), ((FieldSerializer.Bind) field.getAnnotation(FieldSerializer.Bind.class)).value(), field.getClass()));
            }
            if (field.isAnnotationPresent(CollectionSerializer.BindCollection.class)) {
                field.isAnnotationPresent(MapSerializer.BindMap.class);
            }
            Class<?> cls = null;
            if (field.isAnnotationPresent(CollectionSerializer.BindCollection.class)) {
                if (fields[i].serializer == null) {
                    CollectionSerializer.BindCollection bindCollection = (CollectionSerializer.BindCollection) field.getAnnotation(CollectionSerializer.BindCollection.class);
                    if (Collection.class.isAssignableFrom(fields[i].field.getType())) {
                        Class<? extends Serializer> elementSerializer = bindCollection.elementSerializer();
                        if (elementSerializer == Serializer.class) {
                            elementSerializer = null;
                        }
                        Serializer makeSerializer = elementSerializer == null ? null : ReflectionSerializerFactory.makeSerializer(fieldSerializer.getKryo(), elementSerializer, field.getClass());
                        boolean elementsCanBeNull = bindCollection.elementsCanBeNull();
                        Class<?> elementClass = bindCollection.elementClass();
                        if (elementClass == Object.class) {
                            elementClass = null;
                        }
                        CollectionSerializer collectionSerializer = new CollectionSerializer();
                        collectionSerializer.setElementsCanBeNull(elementsCanBeNull);
                        collectionSerializer.setElementClass(elementClass, makeSerializer);
                        fields[i].setSerializer(collectionSerializer);
                    } else {
                        StringBuilder R = a.R("CollectionSerialier.Bind should be used only with fields implementing java.util.Collection, but field ");
                        R.append(fields[i].getField().getDeclaringClass().getName());
                        R.append(".");
                        R.append(fields[i].getField().getName());
                        R.append(" does not implement it.");
                        throw new RuntimeException(R.toString());
                    }
                } else {
                    StringBuilder R2 = a.R("CollectionSerialier.Bind cannot be used with field ");
                    R2.append(fields[i].getField().getDeclaringClass().getName());
                    R2.append(".");
                    R2.append(fields[i].getField().getName());
                    R2.append(", because it has a serializer already.");
                    throw new RuntimeException(R2.toString());
                }
            }
            if (field.isAnnotationPresent(MapSerializer.BindMap.class)) {
                if (fields[i].serializer == null) {
                    MapSerializer.BindMap bindMap = (MapSerializer.BindMap) field.getAnnotation(MapSerializer.BindMap.class);
                    if (Map.class.isAssignableFrom(fields[i].field.getType())) {
                        Class<? extends Serializer> valueSerializer = bindMap.valueSerializer();
                        Class<? extends Serializer> keySerializer = bindMap.keySerializer();
                        if (valueSerializer == Serializer.class) {
                            valueSerializer = null;
                        }
                        if (keySerializer == Serializer.class) {
                            keySerializer = null;
                        }
                        Serializer makeSerializer2 = valueSerializer == null ? null : ReflectionSerializerFactory.makeSerializer(fieldSerializer.getKryo(), valueSerializer, field.getClass());
                        Serializer makeSerializer3 = keySerializer == null ? null : ReflectionSerializerFactory.makeSerializer(fieldSerializer.getKryo(), keySerializer, field.getClass());
                        boolean valuesCanBeNull = bindMap.valuesCanBeNull();
                        boolean keysCanBeNull = bindMap.keysCanBeNull();
                        Class<?> keyClass = bindMap.keyClass();
                        Class<?> valueClass = bindMap.valueClass();
                        if (keyClass == Object.class) {
                            keyClass = null;
                        }
                        if (valueClass != Object.class) {
                            cls = valueClass;
                        }
                        MapSerializer mapSerializer = new MapSerializer();
                        mapSerializer.setKeysCanBeNull(keysCanBeNull);
                        mapSerializer.setValuesCanBeNull(valuesCanBeNull);
                        mapSerializer.setKeyClass(keyClass, makeSerializer3);
                        mapSerializer.setValueClass(cls, makeSerializer2);
                        fields[i].setSerializer(mapSerializer);
                    } else {
                        StringBuilder R3 = a.R("MapSerialier.Bind should be used only with fields implementing java.util.Map, but field ");
                        R3.append(fields[i].getField().getDeclaringClass().getName());
                        R3.append(".");
                        R3.append(fields[i].getField().getName());
                        R3.append(" does not implement it.");
                        throw new RuntimeException(R3.toString());
                    }
                } else {
                    StringBuilder R4 = a.R("MapSerialier.Bind cannot be used with field ");
                    R4.append(fields[i].getField().getDeclaringClass().getName());
                    R4.append(".");
                    R4.append(fields[i].getField().getName());
                    R4.append(", because it has a serializer already.");
                    throw new RuntimeException(R4.toString());
                }
            }
        }
    }
}
