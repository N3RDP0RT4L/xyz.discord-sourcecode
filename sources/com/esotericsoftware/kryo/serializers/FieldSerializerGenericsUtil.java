package com.esotericsoftware.kryo.serializers;

import b.e.a.a;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
/* loaded from: classes2.dex */
public final class FieldSerializerGenericsUtil {
    private Kryo kryo;
    private FieldSerializer serializer;

    public FieldSerializerGenericsUtil(FieldSerializer fieldSerializer) {
        this.serializer = fieldSerializer;
        this.kryo = fieldSerializer.getKryo();
    }

    public static Class[] getGenerics(Type type, Kryo kryo) {
        Class concreteClass;
        Class concreteClass2;
        if (type instanceof GenericArrayType) {
            Type genericComponentType = ((GenericArrayType) type).getGenericComponentType();
            return genericComponentType instanceof Class ? new Class[]{(Class) genericComponentType} : getGenerics(genericComponentType, kryo);
        } else if (!(type instanceof ParameterizedType)) {
            return null;
        } else {
            a.C0063a aVar = a.a;
            Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
            Class[] clsArr = new Class[actualTypeArguments.length];
            int length = actualTypeArguments.length;
            int i = 0;
            for (int i2 = 0; i2 < length; i2++) {
                Type type2 = actualTypeArguments[i2];
                a.C0063a aVar2 = a.a;
                clsArr[i2] = Object.class;
                if (type2 instanceof Class) {
                    clsArr[i2] = (Class) type2;
                } else if (type2 instanceof ParameterizedType) {
                    clsArr[i2] = (Class) ((ParameterizedType) type2).getRawType();
                } else if (type2 instanceof TypeVariable) {
                    GenericsResolver genericsResolver = kryo.getGenericsResolver();
                    if (genericsResolver.isSet() && (concreteClass2 = genericsResolver.getConcreteClass(((TypeVariable) type2).getName())) != null) {
                        clsArr[i2] = concreteClass2;
                    }
                } else if (type2 instanceof GenericArrayType) {
                    Type genericComponentType2 = ((GenericArrayType) type2).getGenericComponentType();
                    if (genericComponentType2 instanceof Class) {
                        clsArr[i2] = Array.newInstance((Class) genericComponentType2, 0).getClass();
                    } else if (genericComponentType2 instanceof TypeVariable) {
                        GenericsResolver genericsResolver2 = kryo.getGenericsResolver();
                        if (genericsResolver2.isSet() && (concreteClass = genericsResolver2.getConcreteClass(((TypeVariable) genericComponentType2).getName())) != null) {
                            clsArr[i2] = Array.newInstance(concreteClass, 0).getClass();
                        }
                    } else {
                        Class[] generics = getGenerics(genericComponentType2, kryo);
                        if (generics != null) {
                            clsArr[i2] = generics[0];
                        }
                    }
                }
                i++;
            }
            if (i == 0) {
                return null;
            }
            return clsArr;
        }
    }

    private Class<?> getTypeVarConcreteClass(Class[] clsArr, int i, String str) {
        if (clsArr != null && clsArr.length > i) {
            return clsArr[i];
        }
        a.C0063a aVar = a.a;
        GenericsResolver genericsResolver = this.kryo.getGenericsResolver();
        if (genericsResolver.isSet()) {
            return genericsResolver.getConcreteClass(str);
        }
        return null;
    }

    /* JADX WARN: Code restructure failed: missing block: B:16:0x0022, code lost:
        r7 = r4.getGenericSuperclass();
        r4 = r4.getSuperclass();
     */
    /* JADX WARN: Code restructure failed: missing block: B:17:0x002a, code lost:
        if (r7 == null) goto L50;
     */
    /* JADX WARN: Code restructure failed: missing block: B:19:0x002e, code lost:
        if ((r7 instanceof java.lang.reflect.ParameterizedType) == false) goto L16;
     */
    /* JADX WARN: Code restructure failed: missing block: B:20:0x0030, code lost:
        if (r7 != null) goto L21;
     */
    /* JADX WARN: Code restructure failed: missing block: B:21:0x0033, code lost:
        r7 = ((java.lang.reflect.ParameterizedType) r7).getActualTypeArguments();
        r1 = r4.getTypeParameters();
        r8 = new java.lang.Class[r7.length];
        r3 = 0;
     */
    /* JADX WARN: Code restructure failed: missing block: B:23:0x0042, code lost:
        if (r3 >= r7.length) goto L52;
     */
    /* JADX WARN: Code restructure failed: missing block: B:25:0x0048, code lost:
        if ((r7[r3] instanceof java.lang.Class) == false) goto L27;
     */
    /* JADX WARN: Code restructure failed: missing block: B:26:0x004a, code lost:
        r4 = (java.lang.Class) r7[r3];
     */
    /* JADX WARN: Code restructure failed: missing block: B:27:0x004f, code lost:
        r4 = java.lang.Object.class;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0051, code lost:
        r8[r3] = r4;
        r3 = r3 + 1;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public com.esotericsoftware.kryo.serializers.Generics buildGenericsScope(java.lang.Class r7, java.lang.Class[] r8) {
        /*
            r6 = this;
            r0 = 0
            r1 = r0
        L2:
            r2 = 0
            if (r7 == 0) goto L5b
            com.esotericsoftware.kryo.serializers.FieldSerializer r1 = r6.serializer
            java.lang.Class r3 = r1.type
            if (r7 != r3) goto Le
            java.lang.reflect.TypeVariable[] r1 = r1.typeParameters
            goto L12
        Le:
            java.lang.reflect.TypeVariable[] r1 = r7.getTypeParameters()
        L12:
            if (r1 == 0) goto L17
            int r3 = r1.length
            if (r3 != 0) goto L5b
        L17:
            com.esotericsoftware.kryo.serializers.FieldSerializer r3 = r6.serializer
            java.lang.Class r4 = r3.type
            if (r7 != r4) goto L56
            java.lang.Class r7 = r3.componentType
            if (r7 == 0) goto L22
            goto L2
        L22:
            java.lang.reflect.Type r7 = r4.getGenericSuperclass()
            java.lang.Class r4 = r4.getSuperclass()
            if (r7 == 0) goto L30
            boolean r3 = r7 instanceof java.lang.reflect.ParameterizedType
            if (r3 == 0) goto L22
        L30:
            if (r7 != 0) goto L33
            goto L5b
        L33:
            java.lang.reflect.ParameterizedType r7 = (java.lang.reflect.ParameterizedType) r7
            java.lang.reflect.Type[] r7 = r7.getActualTypeArguments()
            java.lang.reflect.TypeVariable[] r1 = r4.getTypeParameters()
            int r8 = r7.length
            java.lang.Class[] r8 = new java.lang.Class[r8]
            r3 = 0
        L41:
            int r4 = r7.length
            if (r3 >= r4) goto L5b
            r4 = r7[r3]
            boolean r4 = r4 instanceof java.lang.Class
            if (r4 == 0) goto L4f
            r4 = r7[r3]
            java.lang.Class r4 = (java.lang.Class) r4
            goto L51
        L4f:
            java.lang.Class<java.lang.Object> r4 = java.lang.Object.class
        L51:
            r8[r3] = r4
            int r3 = r3 + 1
            goto L41
        L56:
            java.lang.Class r7 = r7.getComponentType()
            goto L2
        L5b:
            if (r1 == 0) goto L87
            int r7 = r1.length
            if (r7 <= 0) goto L87
            b.e.a.a$a r7 = b.e.a.a.a
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            int r0 = r1.length
            r3 = 0
        L69:
            if (r2 >= r0) goto L81
            r4 = r1[r2]
            java.lang.String r4 = r4.getName()
            b.e.a.a$a r5 = b.e.a.a.a
            java.lang.Class r5 = r6.getTypeVarConcreteClass(r8, r3, r4)
            if (r5 == 0) goto L7c
            r7.put(r4, r5)
        L7c:
            int r3 = r3 + 1
            int r2 = r2 + 1
            goto L69
        L81:
            com.esotericsoftware.kryo.serializers.Generics r8 = new com.esotericsoftware.kryo.serializers.Generics
            r8.<init>(r7)
            return r8
        L87:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.esotericsoftware.kryo.serializers.FieldSerializerGenericsUtil.buildGenericsScope(java.lang.Class, java.lang.Class[]):com.esotericsoftware.kryo.serializers.Generics");
    }

    public Class[] computeFieldGenerics(Type type, Field field, Class[] clsArr) {
        Generics genericsScope;
        Class concreteClass;
        if (type == null) {
            return null;
        }
        if ((type instanceof TypeVariable) && this.serializer.getGenericsScope() != null) {
            Class concreteClass2 = this.serializer.getGenericsScope().getConcreteClass(((TypeVariable) type).getName());
            if (concreteClass2 == null) {
                return null;
            }
            clsArr[0] = concreteClass2;
            Class[] clsArr2 = {clsArr[0]};
            a.C0063a aVar = a.a;
            return clsArr2;
        } else if (type instanceof ParameterizedType) {
            Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
            if (actualTypeArguments == null) {
                return null;
            }
            Class[] clsArr3 = new Class[actualTypeArguments.length];
            for (int i = 0; i < actualTypeArguments.length; i++) {
                Type type2 = actualTypeArguments[i];
                if (type2 instanceof Class) {
                    clsArr3[i] = (Class) type2;
                } else if (type2 instanceof ParameterizedType) {
                    clsArr3[i] = (Class) ((ParameterizedType) type2).getRawType();
                } else if ((type2 instanceof TypeVariable) && this.serializer.getGenericsScope() != null) {
                    clsArr3[i] = this.serializer.getGenericsScope().getConcreteClass(((TypeVariable) type2).getName());
                    if (clsArr3[i] == null) {
                        clsArr3[i] = Object.class;
                    }
                } else if (type2 instanceof WildcardType) {
                    clsArr3[i] = Object.class;
                } else if (type2 instanceof GenericArrayType) {
                    Type genericComponentType = ((GenericArrayType) type2).getGenericComponentType();
                    if (genericComponentType instanceof Class) {
                        clsArr3[i] = Array.newInstance((Class) genericComponentType, 0).getClass();
                    } else if (!(!(genericComponentType instanceof TypeVariable) || (genericsScope = this.serializer.getGenericsScope()) == null || (concreteClass = genericsScope.getConcreteClass(((TypeVariable) genericComponentType).getName())) == null)) {
                        clsArr3[i] = Array.newInstance(concreteClass, 0).getClass();
                    }
                } else {
                    clsArr3[i] = null;
                }
            }
            a.C0063a aVar2 = a.a;
            return clsArr3;
        } else if (!(type instanceof GenericArrayType)) {
            return null;
        } else {
            Class[] computeFieldGenerics = computeFieldGenerics(((GenericArrayType) type).getGenericComponentType(), field, new Class[]{clsArr[0]});
            a.C0063a aVar3 = a.a;
            return computeFieldGenerics;
        }
    }

    public FieldSerializer.CachedField newCachedFieldOfGenericType(Field field, int i, Class[] clsArr, Type type) {
        a.C0063a aVar = a.a;
        buildGenericsScope(clsArr[0], getGenerics(type, this.kryo));
        if (clsArr[0] == Object.class && (type instanceof TypeVariable) && this.serializer.getGenericsScope() != null) {
            TypeVariable typeVariable = (TypeVariable) type;
            Class concreteClass = this.serializer.getGenericsScope().getConcreteClass(typeVariable.getName());
            if (concreteClass != null) {
                new Generics().add(typeVariable.getName(), concreteClass);
            }
        }
        Class[] computeFieldGenerics = computeFieldGenerics(type, field, clsArr);
        FieldSerializer.CachedField newMatchingCachedField = this.serializer.newMatchingCachedField(field, i, clsArr[0], type, computeFieldGenerics);
        if (computeFieldGenerics != null && (newMatchingCachedField instanceof ObjectField) && computeFieldGenerics.length > 0 && computeFieldGenerics[0] != null) {
            ((ObjectField) newMatchingCachedField).generics = computeFieldGenerics;
        }
        return newMatchingCachedField;
    }
}
