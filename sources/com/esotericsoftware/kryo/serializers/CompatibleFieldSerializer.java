package com.esotericsoftware.kryo.serializers;

import b.e.a.a;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.InputChunked;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.io.OutputChunked;
import com.esotericsoftware.kryo.serializers.FieldSerializer;
import com.esotericsoftware.kryo.util.ObjectMap;
/* loaded from: classes2.dex */
public class CompatibleFieldSerializer<T> extends FieldSerializer<T> {
    private static final int THRESHOLD_BINARY_SEARCH = 32;

    public CompatibleFieldSerializer(Kryo kryo, Class cls) {
        super(kryo, cls);
    }

    @Override // com.esotericsoftware.kryo.serializers.FieldSerializer, com.esotericsoftware.kryo.Serializer
    public T read(Kryo kryo, Input input, Class<T> cls) {
        T create = create(kryo, input, cls);
        kryo.reference(create);
        ObjectMap graphContext = kryo.getGraphContext();
        FieldSerializer.CachedField[] cachedFieldArr = (FieldSerializer.CachedField[]) graphContext.get(this);
        boolean z2 = true;
        if (cachedFieldArr == null) {
            int readVarInt = input.readVarInt(true);
            a.C0063a aVar = a.a;
            String[] strArr = new String[readVarInt];
            for (int i = 0; i < readVarInt; i++) {
                strArr[i] = input.readString();
            }
            FieldSerializer.CachedField[] cachedFieldArr2 = new FieldSerializer.CachedField[readVarInt];
            FieldSerializer.CachedField[] fields = getFields();
            if (readVarInt < 32) {
                for (int i2 = 0; i2 < readVarInt; i2++) {
                    String str = strArr[i2];
                    int length = fields.length;
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length) {
                            a.C0063a aVar2 = a.a;
                            break;
                        } else if (getCachedFieldName(fields[i3]).equals(str)) {
                            cachedFieldArr2[i2] = fields[i3];
                            break;
                        } else {
                            i3++;
                        }
                    }
                }
            } else {
                int length2 = fields.length;
                for (int i4 = 0; i4 < readVarInt; i4++) {
                    String str2 = strArr[i4];
                    int i5 = length2 - 1;
                    int i6 = 0;
                    while (true) {
                        if (i6 > i5) {
                            a.C0063a aVar3 = a.a;
                            break;
                        }
                        int i7 = (i6 + i5) >>> 1;
                        int compareTo = str2.compareTo(getCachedFieldName(fields[i7]));
                        if (compareTo >= 0) {
                            if (compareTo <= 0) {
                                cachedFieldArr2[i4] = fields[i7];
                                break;
                            }
                            i6 = i7 + 1;
                        } else {
                            i5 = i7 - 1;
                        }
                    }
                }
            }
            graphContext.put(this, cachedFieldArr2);
            cachedFieldArr = cachedFieldArr2;
        }
        InputChunked inputChunked = new InputChunked(input, 1024);
        if (getGenerics() == null) {
            z2 = false;
        }
        for (FieldSerializer.CachedField cachedField : cachedFieldArr) {
            if (cachedField != null && z2) {
                cachedField = getField(getCachedFieldName(cachedField));
            }
            if (cachedField == null) {
                a.C0063a aVar4 = a.a;
                inputChunked.nextChunks();
            } else {
                cachedField.read(inputChunked, create);
                inputChunked.nextChunks();
            }
        }
        return create;
    }

    @Override // com.esotericsoftware.kryo.serializers.FieldSerializer, com.esotericsoftware.kryo.Serializer
    public void write(Kryo kryo, Output output, T t) {
        FieldSerializer.CachedField[] fields = getFields();
        ObjectMap graphContext = kryo.getGraphContext();
        if (!graphContext.containsKey(this)) {
            graphContext.put(this, null);
            a.C0063a aVar = a.a;
            output.writeVarInt(fields.length, true);
            for (FieldSerializer.CachedField cachedField : fields) {
                output.writeString(getCachedFieldName(cachedField));
            }
        }
        OutputChunked outputChunked = new OutputChunked(output, 1024);
        for (FieldSerializer.CachedField cachedField2 : fields) {
            cachedField2.write(outputChunked, t);
            outputChunked.endChunks();
        }
    }
}
