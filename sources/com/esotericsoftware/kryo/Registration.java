package com.esotericsoftware.kryo;

import b.e.a.a;
import com.esotericsoftware.kryo.util.Util;
import h0.b.a.a;
/* loaded from: classes2.dex */
public class Registration {

    /* renamed from: id  reason: collision with root package name */
    private final int f2852id;
    private a instantiator;
    private Serializer serializer;
    private final Class type;

    public Registration(Class cls, Serializer serializer, int i) {
        if (cls == null) {
            throw new IllegalArgumentException("type cannot be null.");
        } else if (serializer != null) {
            this.type = cls;
            this.serializer = serializer;
            this.f2852id = i;
        } else {
            throw new IllegalArgumentException("serializer cannot be null.");
        }
    }

    public int getId() {
        return this.f2852id;
    }

    public a getInstantiator() {
        return this.instantiator;
    }

    public Serializer getSerializer() {
        return this.serializer;
    }

    public Class getType() {
        return this.type;
    }

    public void setInstantiator(a aVar) {
        if (aVar != null) {
            this.instantiator = aVar;
            return;
        }
        throw new IllegalArgumentException("instantiator cannot be null.");
    }

    public void setSerializer(Serializer serializer) {
        if (serializer != null) {
            this.serializer = serializer;
            a.C0063a aVar = b.e.a.a.a;
            return;
        }
        throw new IllegalArgumentException("serializer cannot be null.");
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("[");
        R.append(this.f2852id);
        R.append(", ");
        R.append(Util.className(this.type));
        R.append("]");
        return R.toString();
    }
}
