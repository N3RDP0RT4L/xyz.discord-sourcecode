package com.esotericsoftware.kryo.util;

import b.e.a.a;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import sun.misc.Cleaner;
import sun.misc.Unsafe;
import sun.nio.ch.DirectBuffer;
/* loaded from: classes2.dex */
public class UnsafeUtil {
    private static final Unsafe _unsafe;
    public static final long byteArrayBaseOffset;
    public static final long charArrayBaseOffset;
    public static Constructor<? extends ByteBuffer> directByteBufferConstr;
    public static final long doubleArrayBaseOffset;
    public static final long floatArrayBaseOffset;
    public static final long intArrayBaseOffset;
    public static final long longArrayBaseOffset;
    public static final long shortArrayBaseOffset;

    static {
        long j;
        long j2;
        long j3;
        long j4;
        long j5;
        Unsafe unsafe;
        long j6;
        long j7;
        long j8;
        Unsafe unsafe2;
        long j9;
        long j10;
        long j11;
        try {
            if (!Util.IS_ANDROID) {
                Field declaredField = Unsafe.class.getDeclaredField("theUnsafe");
                declaredField.setAccessible(true);
                unsafe2 = (Unsafe) declaredField.get(null);
                try {
                    j8 = unsafe2.arrayBaseOffset(byte[].class);
                } catch (Exception unused) {
                    j9 = 0;
                    j8 = 0;
                    j5 = 0;
                    j4 = 0;
                    j3 = 0;
                    j2 = 0;
                    a.C0063a aVar = a.a;
                    j = 0;
                    j7 = j8;
                    unsafe = unsafe2;
                    j6 = j9;
                    byteArrayBaseOffset = j7;
                    charArrayBaseOffset = j5;
                    shortArrayBaseOffset = j4;
                    intArrayBaseOffset = j3;
                    floatArrayBaseOffset = j2;
                    longArrayBaseOffset = j6;
                    doubleArrayBaseOffset = j;
                    _unsafe = unsafe;
                    Constructor declaredConstructor = ByteBuffer.allocateDirect(1).getClass().getDeclaredConstructor(Long.TYPE, Integer.TYPE, Object.class);
                    directByteBufferConstr = declaredConstructor;
                    declaredConstructor.setAccessible(true);
                }
                try {
                    j5 = unsafe2.arrayBaseOffset(char[].class);
                } catch (Exception unused2) {
                    j9 = 0;
                    j5 = 0;
                    j4 = 0;
                    j3 = 0;
                    j2 = 0;
                    a.C0063a aVar2 = a.a;
                    j = 0;
                    j7 = j8;
                    unsafe = unsafe2;
                    j6 = j9;
                    byteArrayBaseOffset = j7;
                    charArrayBaseOffset = j5;
                    shortArrayBaseOffset = j4;
                    intArrayBaseOffset = j3;
                    floatArrayBaseOffset = j2;
                    longArrayBaseOffset = j6;
                    doubleArrayBaseOffset = j;
                    _unsafe = unsafe;
                    Constructor declaredConstructor2 = ByteBuffer.allocateDirect(1).getClass().getDeclaredConstructor(Long.TYPE, Integer.TYPE, Object.class);
                    directByteBufferConstr = declaredConstructor2;
                    declaredConstructor2.setAccessible(true);
                }
                try {
                    j4 = unsafe2.arrayBaseOffset(short[].class);
                } catch (Exception unused3) {
                    j9 = 0;
                    j4 = 0;
                    j3 = 0;
                    j2 = 0;
                    a.C0063a aVar22 = a.a;
                    j = 0;
                    j7 = j8;
                    unsafe = unsafe2;
                    j6 = j9;
                    byteArrayBaseOffset = j7;
                    charArrayBaseOffset = j5;
                    shortArrayBaseOffset = j4;
                    intArrayBaseOffset = j3;
                    floatArrayBaseOffset = j2;
                    longArrayBaseOffset = j6;
                    doubleArrayBaseOffset = j;
                    _unsafe = unsafe;
                    Constructor declaredConstructor22 = ByteBuffer.allocateDirect(1).getClass().getDeclaredConstructor(Long.TYPE, Integer.TYPE, Object.class);
                    directByteBufferConstr = declaredConstructor22;
                    declaredConstructor22.setAccessible(true);
                }
                try {
                    j3 = unsafe2.arrayBaseOffset(int[].class);
                } catch (Exception unused4) {
                    j9 = 0;
                    j3 = 0;
                    j2 = 0;
                    a.C0063a aVar222 = a.a;
                    j = 0;
                    j7 = j8;
                    unsafe = unsafe2;
                    j6 = j9;
                    byteArrayBaseOffset = j7;
                    charArrayBaseOffset = j5;
                    shortArrayBaseOffset = j4;
                    intArrayBaseOffset = j3;
                    floatArrayBaseOffset = j2;
                    longArrayBaseOffset = j6;
                    doubleArrayBaseOffset = j;
                    _unsafe = unsafe;
                    Constructor declaredConstructor222 = ByteBuffer.allocateDirect(1).getClass().getDeclaredConstructor(Long.TYPE, Integer.TYPE, Object.class);
                    directByteBufferConstr = declaredConstructor222;
                    declaredConstructor222.setAccessible(true);
                }
                try {
                    j2 = unsafe2.arrayBaseOffset(float[].class);
                    try {
                        j9 = unsafe2.arrayBaseOffset(long[].class);
                        try {
                            j11 = unsafe2.arrayBaseOffset(double[].class);
                            j10 = j2;
                            unsafe = unsafe2;
                            j6 = j9;
                            j7 = j8;
                        } catch (Exception unused5) {
                            a.C0063a aVar2222 = a.a;
                            j = 0;
                            j7 = j8;
                            unsafe = unsafe2;
                            j6 = j9;
                            byteArrayBaseOffset = j7;
                            charArrayBaseOffset = j5;
                            shortArrayBaseOffset = j4;
                            intArrayBaseOffset = j3;
                            floatArrayBaseOffset = j2;
                            longArrayBaseOffset = j6;
                            doubleArrayBaseOffset = j;
                            _unsafe = unsafe;
                            Constructor declaredConstructor2222 = ByteBuffer.allocateDirect(1).getClass().getDeclaredConstructor(Long.TYPE, Integer.TYPE, Object.class);
                            directByteBufferConstr = declaredConstructor2222;
                            declaredConstructor2222.setAccessible(true);
                        }
                    } catch (Exception unused6) {
                        j9 = 0;
                    }
                } catch (Exception unused7) {
                    j9 = 0;
                    j2 = 0;
                    a.C0063a aVar22222 = a.a;
                    j = 0;
                    j7 = j8;
                    unsafe = unsafe2;
                    j6 = j9;
                    byteArrayBaseOffset = j7;
                    charArrayBaseOffset = j5;
                    shortArrayBaseOffset = j4;
                    intArrayBaseOffset = j3;
                    floatArrayBaseOffset = j2;
                    longArrayBaseOffset = j6;
                    doubleArrayBaseOffset = j;
                    _unsafe = unsafe;
                    Constructor declaredConstructor22222 = ByteBuffer.allocateDirect(1).getClass().getDeclaredConstructor(Long.TYPE, Integer.TYPE, Object.class);
                    directByteBufferConstr = declaredConstructor22222;
                    declaredConstructor22222.setAccessible(true);
                }
            } else {
                a.C0063a aVar3 = a.a;
                j11 = 0;
                j7 = 0;
                j6 = 0;
                unsafe = null;
                j5 = 0;
                j4 = 0;
                j3 = 0;
                j10 = 0;
            }
            j2 = j10;
            j = j11;
        } catch (Exception unused8) {
            j9 = 0;
            unsafe2 = null;
        }
        byteArrayBaseOffset = j7;
        charArrayBaseOffset = j5;
        shortArrayBaseOffset = j4;
        intArrayBaseOffset = j3;
        floatArrayBaseOffset = j2;
        longArrayBaseOffset = j6;
        doubleArrayBaseOffset = j;
        _unsafe = unsafe;
        try {
            Constructor declaredConstructor222222 = ByteBuffer.allocateDirect(1).getClass().getDeclaredConstructor(Long.TYPE, Integer.TYPE, Object.class);
            directByteBufferConstr = declaredConstructor222222;
            declaredConstructor222222.setAccessible(true);
        } catch (Exception unused9) {
            directByteBufferConstr = null;
        }
    }

    public static final ByteBuffer getDirectBufferAt(long j, int i) {
        Constructor<? extends ByteBuffer> constructor = directByteBufferConstr;
        if (constructor == null) {
            return null;
        }
        try {
            return constructor.newInstance(Long.valueOf(j), Integer.valueOf(i), null);
        } catch (Exception e) {
            throw new RuntimeException(b.d.b.a.a.s("Cannot allocate ByteBuffer at a given address: ", j), e);
        }
    }

    public static void releaseBuffer(ByteBuffer byteBuffer) {
        Cleaner cleaner;
        if (byteBuffer != null && byteBuffer.isDirect() && (cleaner = ((DirectBuffer) byteBuffer).cleaner()) != null) {
            cleaner.clean();
        }
    }

    public static Field[] sortFieldsByOffset(List<Field> list) {
        Field[] fieldArr = (Field[]) list.toArray(new Field[0]);
        Arrays.sort(fieldArr, new Comparator<Field>() { // from class: com.esotericsoftware.kryo.util.UnsafeUtil.1
            public int compare(Field field, Field field2) {
                int i = (UnsafeUtil.unsafe().objectFieldOffset(field) > UnsafeUtil.unsafe().objectFieldOffset(field2) ? 1 : (UnsafeUtil.unsafe().objectFieldOffset(field) == UnsafeUtil.unsafe().objectFieldOffset(field2) ? 0 : -1));
                if (i < 0) {
                    return -1;
                }
                return i == 0 ? 0 : 1;
            }
        });
        for (Field field : list) {
            a.C0063a aVar = a.a;
        }
        return fieldArr;
    }

    public static final Unsafe unsafe() {
        return _unsafe;
    }
}
