package com.esotericsoftware.kryo.util;

import b.e.a.a;
import com.esotericsoftware.kryo.ClassResolver;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.Registration;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
/* loaded from: classes2.dex */
public class DefaultClassResolver implements ClassResolver {
    public static final byte NAME = -1;
    public IdentityObjectIntMap<Class> classToNameId;
    public Kryo kryo;
    private Class memoizedClass;
    private Registration memoizedClassIdValue;
    private Registration memoizedClassValue;
    public IntMap<Class> nameIdToClass;
    public ObjectMap<String, Class> nameToClass;
    public int nextNameId;
    public final IntMap<Registration> idToRegistration = new IntMap<>();
    public final ObjectMap<Class, Registration> classToRegistration = new ObjectMap<>();
    private int memoizedClassId = -1;

    @Override // com.esotericsoftware.kryo.ClassResolver
    public Registration getRegistration(Class cls) {
        if (cls == this.memoizedClass) {
            return this.memoizedClassValue;
        }
        Registration registration = this.classToRegistration.get(cls);
        if (registration != null) {
            this.memoizedClass = cls;
            this.memoizedClassValue = registration;
        }
        return registration;
    }

    public Class<?> getTypeByName(String str) {
        ObjectMap<String, Class> objectMap = this.nameToClass;
        if (objectMap != null) {
            return objectMap.get(str);
        }
        return null;
    }

    @Override // com.esotericsoftware.kryo.ClassResolver
    public Registration readClass(Input input) {
        int readVarInt = input.readVarInt(true);
        if (readVarInt == 0) {
            a.C0063a aVar = a.a;
            return null;
        } else if (readVarInt == 1) {
            return readName(input);
        } else {
            if (readVarInt == this.memoizedClassId) {
                return this.memoizedClassIdValue;
            }
            int i = readVarInt - 2;
            Registration registration = this.idToRegistration.get(i);
            if (registration != null) {
                a.C0063a aVar2 = a.a;
                this.memoizedClassId = readVarInt;
                this.memoizedClassIdValue = registration;
                return registration;
            }
            throw new KryoException(b.d.b.a.a.p("Encountered unregistered class ID: ", i));
        }
    }

    public Registration readName(Input input) {
        int readVarInt = input.readVarInt(true);
        if (this.nameIdToClass == null) {
            this.nameIdToClass = new IntMap<>();
        }
        Class cls = this.nameIdToClass.get(readVarInt);
        if (cls == null) {
            String readString = input.readString();
            cls = getTypeByName(readString);
            if (cls == null) {
                try {
                    cls = Class.forName(readString, false, this.kryo.getClassLoader());
                } catch (ClassNotFoundException e) {
                    a.C0063a aVar = a.a;
                    a.a.a(4, "kryo", b.d.b.a.a.w("Unable to load class ", readString, " with kryo's ClassLoader. Retrying with current.."), null);
                    try {
                        cls = Class.forName(readString);
                    } catch (ClassNotFoundException unused) {
                        throw new KryoException(b.d.b.a.a.v("Unable to find class: ", readString), e);
                    }
                }
                if (this.nameToClass == null) {
                    this.nameToClass = new ObjectMap<>();
                }
                this.nameToClass.put(readString, cls);
            }
            this.nameIdToClass.put(readVarInt, cls);
            a.C0063a aVar2 = a.a;
        } else {
            a.C0063a aVar3 = a.a;
        }
        return this.kryo.getRegistration(cls);
    }

    @Override // com.esotericsoftware.kryo.ClassResolver
    public Registration register(Registration registration) {
        if (registration != null) {
            if (registration.getId() != -1) {
                a.C0063a aVar = a.a;
                this.idToRegistration.put(registration.getId(), registration);
            } else {
                a.C0063a aVar2 = a.a;
            }
            this.classToRegistration.put(registration.getType(), registration);
            if (registration.getType().isPrimitive()) {
                this.classToRegistration.put(Util.getWrapperClass(registration.getType()), registration);
            }
            return registration;
        }
        throw new IllegalArgumentException("registration cannot be null.");
    }

    @Override // com.esotericsoftware.kryo.ClassResolver
    public Registration registerImplicit(Class cls) {
        return register(new Registration(cls, this.kryo.getDefaultSerializer(cls), -1));
    }

    @Override // com.esotericsoftware.kryo.ClassResolver
    public void reset() {
        if (!this.kryo.isRegistrationRequired()) {
            IdentityObjectIntMap<Class> identityObjectIntMap = this.classToNameId;
            if (identityObjectIntMap != null) {
                identityObjectIntMap.clear(2048);
            }
            IntMap<Class> intMap = this.nameIdToClass;
            if (intMap != null) {
                intMap.clear();
            }
            this.nextNameId = 0;
        }
    }

    @Override // com.esotericsoftware.kryo.ClassResolver
    public void setKryo(Kryo kryo) {
        this.kryo = kryo;
    }

    @Override // com.esotericsoftware.kryo.ClassResolver
    public Registration writeClass(Output output, Class cls) {
        if (cls == null) {
            a.C0063a aVar = a.a;
            output.writeVarInt(0, true);
            return null;
        }
        Registration registration = this.kryo.getRegistration(cls);
        if (registration.getId() == -1) {
            writeName(output, cls, registration);
        } else {
            a.C0063a aVar2 = a.a;
            output.writeVarInt(registration.getId() + 2, true);
        }
        return registration;
    }

    public void writeName(Output output, Class cls, Registration registration) {
        int i;
        output.writeVarInt(1, true);
        IdentityObjectIntMap<Class> identityObjectIntMap = this.classToNameId;
        if (identityObjectIntMap == null || (i = identityObjectIntMap.get(cls, -1)) == -1) {
            a.C0063a aVar = a.a;
            int i2 = this.nextNameId;
            this.nextNameId = i2 + 1;
            if (this.classToNameId == null) {
                this.classToNameId = new IdentityObjectIntMap<>();
            }
            this.classToNameId.put(cls, i2);
            output.writeVarInt(i2, true);
            output.writeString(cls.getName());
            return;
        }
        a.C0063a aVar2 = a.a;
        output.writeVarInt(i, true);
    }

    @Override // com.esotericsoftware.kryo.ClassResolver
    public Registration getRegistration(int i) {
        return this.idToRegistration.get(i);
    }
}
