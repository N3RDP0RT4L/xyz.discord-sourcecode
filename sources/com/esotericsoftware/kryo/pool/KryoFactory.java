package com.esotericsoftware.kryo.pool;

import com.esotericsoftware.kryo.Kryo;
/* loaded from: classes2.dex */
public interface KryoFactory {
    Kryo create();
}
