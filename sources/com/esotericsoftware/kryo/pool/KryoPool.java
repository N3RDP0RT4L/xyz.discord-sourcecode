package com.esotericsoftware.kryo.pool;

import b.d.b.a.a;
import com.esotericsoftware.kryo.Kryo;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
/* loaded from: classes2.dex */
public interface KryoPool {

    /* loaded from: classes2.dex */
    public static class Builder {
        private final KryoFactory factory;
        private Queue<Kryo> queue = new ConcurrentLinkedQueue();
        private boolean softReferences;

        public Builder(KryoFactory kryoFactory) {
            if (kryoFactory != null) {
                this.factory = kryoFactory;
                return;
            }
            throw new IllegalArgumentException("factory must not be null");
        }

        public KryoPool build() {
            return new KryoPoolQueueImpl(this.factory, this.softReferences ? new SoftReferenceQueue(this.queue) : this.queue);
        }

        public Builder queue(Queue<Kryo> queue) {
            if (queue != null) {
                this.queue = queue;
                return this;
            }
            throw new IllegalArgumentException("queue must not be null");
        }

        public Builder softReferences() {
            this.softReferences = true;
            return this;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(getClass().getName());
            sb.append("[queue.class=");
            sb.append(this.queue.getClass());
            sb.append(", softReferences=");
            return a.M(sb, this.softReferences, "]");
        }
    }

    Kryo borrow();

    void release(Kryo kryo);

    <T> T run(KryoCallback<T> kryoCallback);
}
