package com.esotericsoftware.kryo.pool;

import com.esotericsoftware.kryo.Kryo;
/* loaded from: classes2.dex */
public interface KryoCallback<T> {
    T execute(Kryo kryo);
}
