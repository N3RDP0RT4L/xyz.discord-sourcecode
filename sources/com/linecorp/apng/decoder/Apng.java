package com.linecorp.apng.decoder;

import andhook.lib.HookHelper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Trace;
import androidx.annotation.IntRange;
import com.discord.models.domain.ModelAuditLogEntry;
import com.linecorp.apng.decoder.ApngException;
import d0.t.k;
import d0.z.d.m;
import java.io.InputStream;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: Apng.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0015\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u000f\b\u0000\u0018\u0000 A2\u00020\u0001:\u0002ABBE\u0012\u0006\u0010;\u001a\u00020\b\u0012\u0006\u0010>\u001a\u00020\b\u0012\u0006\u0010!\u001a\u00020\b\u0012\b\b\u0001\u0010&\u001a\u00020\b\u0012\u0006\u0010\u0018\u001a\u00020\u0013\u0012\b\b\u0001\u0010/\u001a\u00020\b\u0012\b\b\u0001\u00109\u001a\u000204¢\u0006\u0004\b?\u0010@J\r\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0003\u0010\u0004J\r\u0010\u0005\u001a\u00020\u0000¢\u0006\u0004\b\u0005\u0010\u0006J\r\u0010\u0007\u001a\u00020\u0002¢\u0006\u0004\b\u0007\u0010\u0004J7\u0010\u0011\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\n2\b\u0010\r\u001a\u0004\u0018\u00010\f2\u0006\u0010\u000e\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u000f¢\u0006\u0004\b\u0011\u0010\u0012R\u0019\u0010\u0018\u001a\u00020\u00138\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0016\u0010\u001c\u001a\u00020\u00198\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b\u001a\u0010\u001bR\u0019\u0010!\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u0013\u0010#\u001a\u00020\b8F@\u0006¢\u0006\u0006\u001a\u0004\b\"\u0010 R\u0019\u0010&\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b$\u0010\u001e\u001a\u0004\b%\u0010 R\u0013\u0010(\u001a\u00020'8F@\u0006¢\u0006\u0006\u001a\u0004\b(\u0010)R\u001c\u0010,\u001a\u00020\b8\u0006@\u0007X\u0087\u0004¢\u0006\f\n\u0004\b*\u0010\u001e\u001a\u0004\b+\u0010 R\u0019\u0010/\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b-\u0010\u001e\u001a\u0004\b.\u0010 R\u0013\u00103\u001a\u0002008F@\u0006¢\u0006\u0006\u001a\u0004\b1\u00102R\u0019\u00109\u001a\u0002048\u0006@\u0006¢\u0006\f\n\u0004\b5\u00106\u001a\u0004\b7\u00108R\u0016\u0010;\u001a\u00020\b8\u0002@\u0002X\u0082\u0004¢\u0006\u0006\n\u0004\b:\u0010\u001eR\u0019\u0010>\u001a\u00020\b8\u0006@\u0006¢\u0006\f\n\u0004\b<\u0010\u001e\u001a\u0004\b=\u0010 ¨\u0006C"}, d2 = {"Lcom/linecorp/apng/decoder/Apng;", "", "", "recycle", "()V", "copy", "()Lcom/linecorp/apng/decoder/Apng;", "finalize", "", "frameIndex", "Landroid/graphics/Canvas;", "canvas", "Landroid/graphics/Rect;", "src", "dst", "Landroid/graphics/Paint;", "paint", "drawWithIndex", "(ILandroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V", "", "g", "[I", "getFrameDurations", "()[I", "frameDurations", "Landroid/graphics/Bitmap;", "a", "Landroid/graphics/Bitmap;", "bitmap", "e", "I", "getHeight", "()I", "height", "getByteCount", "byteCount", "f", "getFrameCount", "frameCount", "", "isRecycled", "()Z", "b", "getDuration", "duration", "h", "getLoopCount", "loopCount", "Landroid/graphics/Bitmap$Config;", "getConfig", "()Landroid/graphics/Bitmap$Config;", "config", "", "i", "J", "getAllFrameByteCount", "()J", "allFrameByteCount", "c", ModelAuditLogEntry.CHANGE_KEY_ID, "d", "getWidth", "width", HookHelper.constructorName, "(IIII[IIJ)V", "Companion", "DecodeResult", "apng-drawable_release"}, k = 1, mv = {1, 4, 2})
/* loaded from: classes3.dex */
public final class Apng {
    public static final Companion Companion = new Companion(null);
    public final Bitmap a;
    @IntRange(from = 0, to = 2147483647L)

    /* renamed from: b  reason: collision with root package name */
    public final int f3130b;
    public final int c;
    public final int d;
    public final int e;
    public final int f;
    public final int[] g;
    public final int h;
    public final long i;

    /* compiled from: Apng.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\r\u0010\u000eJ\u0015\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\u0005\u0010\u0006J\u0015\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0002¢\u0006\u0004\b\b\u0010\tJ\u0015\u0010\u000b\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u0004¢\u0006\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lcom/linecorp/apng/decoder/Apng$Companion;", "", "Ljava/io/InputStream;", "stream", "Lcom/linecorp/apng/decoder/Apng;", "decode", "(Ljava/io/InputStream;)Lcom/linecorp/apng/decoder/Apng;", "", "isApng", "(Ljava/io/InputStream;)Z", "apng", "copy", "(Lcom/linecorp/apng/decoder/Apng;)Lcom/linecorp/apng/decoder/Apng;", HookHelper.constructorName, "()V", "apng-drawable_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes3.dex */
    public static final class Companion {
        public Companion() {
        }

        public final Apng copy(Apng apng) throws ApngException {
            m.checkNotNullParameter(apng, "apng");
            DecodeResult decodeResult = new DecodeResult();
            Trace.beginSection("Apng#copy");
            try {
                int copy = ApngDecoderJni.copy(apng.c, decodeResult);
                if (copy >= 0) {
                    try {
                        return new Apng(copy, decodeResult.getWidth(), decodeResult.getHeight(), decodeResult.getFrameCount(), decodeResult.getFrameDurations(), decodeResult.getLoopCount(), decodeResult.getAllFrameByteCount());
                    } finally {
                        ApngException apngException = new ApngException(th);
                    }
                } else {
                    throw new ApngException(ApngException.ErrorCode.Companion.fromErrorCode$apng_drawable_release(copy), null, 2, null);
                }
            } finally {
                try {
                    throw new ApngException(th);
                } finally {
                }
            }
        }

        public final Apng decode(InputStream inputStream) throws ApngException {
            m.checkNotNullParameter(inputStream, "stream");
            DecodeResult decodeResult = new DecodeResult();
            Trace.beginSection("Apng#decode");
            try {
                int decode = ApngDecoderJni.decode(inputStream, decodeResult);
                if (decode >= 0) {
                    try {
                        return new Apng(decode, decodeResult.getWidth(), decodeResult.getHeight(), decodeResult.getFrameCount(), decodeResult.getFrameDurations(), decodeResult.getLoopCount(), decodeResult.getAllFrameByteCount());
                    } finally {
                        ApngException apngException = new ApngException(th);
                    }
                } else {
                    throw new ApngException(ApngException.ErrorCode.Companion.fromErrorCode$apng_drawable_release(decode), null, 2, null);
                }
            } finally {
                try {
                    throw new ApngException(th);
                } finally {
                }
            }
        }

        public final boolean isApng(InputStream inputStream) throws ApngException {
            m.checkNotNullParameter(inputStream, "stream");
            try {
                return ApngDecoderJni.isApng(inputStream);
            } catch (Throwable th) {
                throw new ApngException(th);
            }
        }

        public Companion(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: Apng.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0015\n\u0002\b\t\n\u0002\u0010\t\n\u0002\b\u000f\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b \u0010!R\"\u0010\u0003\u001a\u00020\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\"\u0010\n\u001a\u00020\t8\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\"\u0010\u0010\u001a\u00020\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0010\u0010\u0004\u001a\u0004\b\u0011\u0010\u0006\"\u0004\b\u0012\u0010\bR\"\u0010\u0014\u001a\u00020\u00138\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\"\u0010\u001a\u001a\u00020\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001a\u0010\u0004\u001a\u0004\b\u001b\u0010\u0006\"\u0004\b\u001c\u0010\bR\"\u0010\u001d\u001a\u00020\u00028\u0006@\u0006X\u0086\u000e¢\u0006\u0012\n\u0004\b\u001d\u0010\u0004\u001a\u0004\b\u001e\u0010\u0006\"\u0004\b\u001f\u0010\b¨\u0006\""}, d2 = {"Lcom/linecorp/apng/decoder/Apng$DecodeResult;", "", "", "loopCount", "I", "getLoopCount", "()I", "setLoopCount", "(I)V", "", "frameDurations", "[I", "getFrameDurations", "()[I", "setFrameDurations", "([I)V", "frameCount", "getFrameCount", "setFrameCount", "", "allFrameByteCount", "J", "getAllFrameByteCount", "()J", "setAllFrameByteCount", "(J)V", "width", "getWidth", "setWidth", "height", "getHeight", "setHeight", HookHelper.constructorName, "()V", "apng-drawable_release"}, k = 1, mv = {1, 4, 2})
    /* loaded from: classes3.dex */
    public static final class DecodeResult {
        private long allFrameByteCount;
        private int frameCount;
        private int[] frameDurations = new int[0];
        private int height;
        private int loopCount;
        private int width;

        public final long getAllFrameByteCount() {
            return this.allFrameByteCount;
        }

        public final int getFrameCount() {
            return this.frameCount;
        }

        public final int[] getFrameDurations() {
            return this.frameDurations;
        }

        public final int getHeight() {
            return this.height;
        }

        public final int getLoopCount() {
            return this.loopCount;
        }

        public final int getWidth() {
            return this.width;
        }

        public final void setAllFrameByteCount(long j) {
            this.allFrameByteCount = j;
        }

        public final void setFrameCount(int i) {
            this.frameCount = i;
        }

        public final void setFrameDurations(int[] iArr) {
            m.checkNotNullParameter(iArr, "<set-?>");
            this.frameDurations = iArr;
        }

        public final void setHeight(int i) {
            this.height = i;
        }

        public final void setLoopCount(int i) {
            this.loopCount = i;
        }

        public final void setWidth(int i) {
            this.width = i;
        }
    }

    public Apng(int i, int i2, int i3, @IntRange(from = 1, to = 2147483647L) int i4, int[] iArr, @IntRange(from = 0, to = 2147483647L) int i5, @IntRange(from = 0, to = 2147483647L) long j) {
        m.checkNotNullParameter(iArr, "frameDurations");
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.f = i4;
        this.g = iArr;
        this.h = i5;
        this.i = j;
        Bitmap createBitmap = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
        m.checkNotNullExpressionValue(createBitmap, "Bitmap.createBitmap(widt… Bitmap.Config.ARGB_8888)");
        this.a = createBitmap;
        Trace.beginSection("Apng#draw");
        ApngDecoderJni.draw(i, 0, createBitmap);
        Trace.endSection();
        this.f3130b = k.sum(iArr);
    }

    public final Apng copy() {
        return Companion.copy(this);
    }

    public final void drawWithIndex(int i, Canvas canvas, Rect rect, Rect rect2, Paint paint) {
        m.checkNotNullParameter(canvas, "canvas");
        m.checkNotNullParameter(rect2, "dst");
        m.checkNotNullParameter(paint, "paint");
        Trace.beginSection("Apng#draw");
        ApngDecoderJni.draw(this.c, i, this.a);
        Trace.endSection();
        canvas.drawBitmap(this.a, rect, rect2, paint);
    }

    public final void finalize() {
        recycle();
    }

    public final long getAllFrameByteCount() {
        return this.i;
    }

    public final int getByteCount() {
        return this.a.getAllocationByteCount();
    }

    public final Bitmap.Config getConfig() {
        Bitmap.Config config = this.a.getConfig();
        m.checkNotNullExpressionValue(config, "bitmap.config");
        return config;
    }

    public final int getDuration() {
        return this.f3130b;
    }

    public final int getFrameCount() {
        return this.f;
    }

    public final int[] getFrameDurations() {
        return this.g;
    }

    public final int getHeight() {
        return this.e;
    }

    public final int getLoopCount() {
        return this.h;
    }

    public final int getWidth() {
        return this.d;
    }

    public final boolean isRecycled() {
        return this.a.isRecycled();
    }

    public final void recycle() {
        ApngDecoderJni.recycle(this.c);
    }
}
