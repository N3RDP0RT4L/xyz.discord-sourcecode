package s.a;

import b.i.a.f.e.o.f;
import d0.w.h.b;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
/* compiled from: Builders.common.kt */
/* loaded from: classes3.dex */
public final class j1<T> extends g0<T> {
    public final Continuation<Unit> m;

    public j1(CoroutineContext coroutineContext, Function2<? super CoroutineScope, ? super Continuation<? super T>, ? extends Object> function2) {
        super(coroutineContext, false);
        this.m = b.createCoroutineUnintercepted(function2, this, this);
    }

    @Override // s.a.b
    public void i0() {
        f.e1(this.m, this);
    }
}
