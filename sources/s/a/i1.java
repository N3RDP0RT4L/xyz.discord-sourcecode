package s.a;

import s.a.a.t;
/* compiled from: JobSupport.kt */
/* loaded from: classes3.dex */
public final class i1 {
    public static final t a = new t("COMPLETING_ALREADY");

    /* renamed from: b  reason: collision with root package name */
    public static final t f3813b = new t("COMPLETING_WAITING_CHILDREN");
    public static final t c = new t("COMPLETING_RETRY");
    public static final t d = new t("TOO_LATE_TO_CANCEL");
    public static final t e = new t("SEALED");
    public static final p0 f = new p0(false);
    public static final p0 g = new p0(true);

    public static final Object a(Object obj) {
        z0 z0Var;
        a1 a1Var = (a1) (!(obj instanceof a1) ? null : obj);
        return (a1Var == null || (z0Var = a1Var.a) == null) ? obj : z0Var;
    }
}
