package s.a;

import b.d.b.a.a;
import d0.z.d.m;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.Job;
import s.a.a.g;
import s.a.a.h;
import s.a.a.t;
/* compiled from: JobSupport.kt */
/* loaded from: classes3.dex */
public final class o extends e1<Job> {
    public final l<?> n;

    public o(Job job, l<?> lVar) {
        super(job);
        this.n = lVar;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        q(th);
        return Unit.a;
    }

    @Override // s.a.y
    public void q(Throwable th) {
        l<?> lVar = this.n;
        J j = this.m;
        Objects.requireNonNull(lVar);
        CancellationException q = j.q();
        boolean z2 = true;
        boolean z3 = false;
        if (lVar.l == 2) {
            Continuation<?> continuation = lVar.p;
            if (!(continuation instanceof g)) {
                continuation = null;
            }
            g gVar = (g) continuation;
            if (gVar != null) {
                while (true) {
                    Object obj = gVar._reusableCancellableContinuation;
                    t tVar = h.f3799b;
                    if (!m.areEqual(obj, tVar)) {
                        if (obj instanceof Throwable) {
                            break;
                        } else if (g.m.compareAndSet(gVar, obj, null)) {
                            z2 = false;
                            break;
                        }
                    } else if (g.m.compareAndSet(gVar, tVar, q)) {
                        break;
                    }
                }
                z3 = z2;
            }
        }
        if (!z3) {
            lVar.k(q);
            lVar.s();
        }
    }

    @Override // s.a.a.k
    public String toString() {
        StringBuilder R = a.R("ChildContinuation[");
        R.append(this.n);
        R.append(']');
        return R.toString();
    }
}
