package s.a;

import b.d.b.a.a;
import kotlin.Unit;
import s.a.a.k;
/* compiled from: CancellableContinuation.kt */
/* loaded from: classes3.dex */
public final class q1 extends e {
    public final k j;

    public q1(k kVar) {
        this.j = kVar;
    }

    @Override // s.a.k
    public void a(Throwable th) {
        this.j.n();
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Throwable th) {
        this.j.n();
        return Unit.a;
    }

    public String toString() {
        StringBuilder R = a.R("RemoveOnCancel[");
        R.append(this.j);
        R.append(']');
        return R.toString();
    }
}
