package s.a;

import b.d.b.a.a;
import kotlin.Unit;
/* compiled from: CancellableContinuation.kt */
/* loaded from: classes3.dex */
public final class n0 extends j {
    public final m0 j;

    public n0(m0 m0Var) {
        this.j = m0Var;
    }

    @Override // s.a.k
    public void a(Throwable th) {
        this.j.dispose();
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Throwable th) {
        this.j.dispose();
        return Unit.a;
    }

    public String toString() {
        StringBuilder R = a.R("DisposeOnCancel[");
        R.append(this.j);
        R.append(']');
        return R.toString();
    }
}
