package s.a;

import androidx.recyclerview.widget.RecyclerView;
import s.a.a.t;
/* compiled from: EventLoop.common.kt */
/* loaded from: classes3.dex */
public final class t0 {
    public static final t a = new t("REMOVED_TASK");

    /* renamed from: b  reason: collision with root package name */
    public static final t f3816b = new t("CLOSED_EMPTY");

    public static final long a(long j) {
        if (j <= 0) {
            return 0L;
        }
        return j >= 9223372036854L ? RecyclerView.FOREVER_NS : 1000000 * j;
    }
}
