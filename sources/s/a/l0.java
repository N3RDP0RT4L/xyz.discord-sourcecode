package s.a;

import b.d.b.a.a;
import java.util.concurrent.Future;
/* compiled from: Executors.kt */
/* loaded from: classes3.dex */
public final class l0 implements m0 {
    public final Future<?> j;

    public l0(Future<?> future) {
        this.j = future;
    }

    @Override // s.a.m0
    public void dispose() {
        this.j.cancel(false);
    }

    public String toString() {
        StringBuilder R = a.R("DisposableFutureHandle[");
        R.append(this.j);
        R.append(']');
        return R.toString();
    }
}
