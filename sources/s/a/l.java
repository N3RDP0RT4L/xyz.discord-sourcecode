package s.a;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import d0.k;
import d0.w.h.c;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CompletionHandlerException;
import kotlinx.coroutines.CoroutineDispatcher;
import kotlinx.coroutines.Job;
import s.a.a.g;
import s.a.a.h;
import s.a.a.t;
/* compiled from: CancellableContinuationImpl.kt */
/* loaded from: classes3.dex */
public class l<T> extends j0<T> implements CancellableContinuation<T>, CoroutineStackFrame {
    public static final AtomicIntegerFieldUpdater m = AtomicIntegerFieldUpdater.newUpdater(l.class, "_decision");
    public static final AtomicReferenceFieldUpdater n = AtomicReferenceFieldUpdater.newUpdater(l.class, Object.class, "_state");
    public final CoroutineContext o;
    public final Continuation<T> p;
    public volatile int _decision = 0;
    public volatile Object _state = c.j;
    public volatile Object _parentHandle = null;

    /* JADX WARN: Multi-variable type inference failed */
    public l(Continuation<? super T> continuation, int i) {
        super(i);
        this.p = continuation;
        this.o = continuation.getContext();
    }

    public final void A() {
        Job job;
        boolean v = v();
        if (this.l == 2) {
            Continuation<T> continuation = this.p;
            Throwable th = null;
            if (!(continuation instanceof g)) {
                continuation = null;
            }
            g gVar = (g) continuation;
            if (gVar != null) {
                while (true) {
                    Object obj = gVar._reusableCancellableContinuation;
                    t tVar = h.f3799b;
                    if (obj == tVar) {
                        if (g.m.compareAndSet(gVar, tVar, this)) {
                            break;
                        }
                    } else if (obj != null) {
                        if (!(obj instanceof Throwable)) {
                            throw new IllegalStateException(a.u("Inconsistent state ", obj).toString());
                        } else if (g.m.compareAndSet(gVar, obj, null)) {
                            th = (Throwable) obj;
                        } else {
                            throw new IllegalArgumentException("Failed requirement.".toString());
                        }
                    }
                }
                if (th != null) {
                    if (!v) {
                        k(th);
                    }
                    v = true;
                }
            }
        }
        if (!v && ((m0) this._parentHandle) == null && (job = (Job) this.p.getContext().get(Job.h)) != null) {
            m0 w0 = f.w0(job, true, false, new o(job, this), 2, null);
            this._parentHandle = w0;
            if (v() && !w()) {
                w0.dispose();
                this._parentHandle = n1.j;
            }
        }
    }

    public final t B(Object obj, Object obj2, Function1<? super Throwable, Unit> function1) {
        Object obj3;
        do {
            obj3 = this._state;
            if (obj3 instanceof o1) {
            } else if (!(obj3 instanceof v) || obj2 == null || ((v) obj3).d != obj2) {
                return null;
            } else {
                return m.a;
            }
        } while (!n.compareAndSet(this, obj3, z((o1) obj3, obj, this.l, function1, obj2)));
        s();
        return m.a;
    }

    @Override // kotlinx.coroutines.CancellableContinuation
    public boolean a() {
        return this._state instanceof o1;
    }

    @Override // s.a.j0
    public void b(Object obj, Throwable th) {
        while (true) {
            Object obj2 = this._state;
            if (obj2 instanceof o1) {
                throw new IllegalStateException("Not completed".toString());
            } else if (!(obj2 instanceof w)) {
                if (obj2 instanceof v) {
                    v vVar = (v) obj2;
                    if (!(vVar.e != null)) {
                        if (n.compareAndSet(this, obj2, v.a(vVar, null, null, null, null, th, 15))) {
                            j jVar = vVar.f3817b;
                            if (jVar != null) {
                                o(jVar, th);
                            }
                            Function1<Throwable, Unit> function1 = vVar.c;
                            if (function1 != null) {
                                p(function1, th);
                                return;
                            }
                            return;
                        }
                    } else {
                        throw new IllegalStateException("Must be called at most once".toString());
                    }
                } else if (n.compareAndSet(this, obj2, new v(obj2, null, null, null, th, 14))) {
                    return;
                }
            } else {
                return;
            }
        }
    }

    @Override // kotlinx.coroutines.CancellableContinuation
    public Object c(T t, Object obj) {
        return B(t, obj, null);
    }

    @Override // s.a.j0
    public final Continuation<T> d() {
        return this.p;
    }

    @Override // s.a.j0
    public Throwable e(Object obj) {
        Throwable e = super.e(obj);
        if (e != null) {
            return e;
        }
        return null;
    }

    @Override // kotlinx.coroutines.CancellableContinuation
    public void f(Function1<? super Throwable, Unit> function1) {
        j b1Var = function1 instanceof j ? (j) function1 : new b1(function1);
        while (true) {
            Object obj = this._state;
            if (!(obj instanceof c)) {
                Throwable th = null;
                if (!(obj instanceof j)) {
                    boolean z2 = obj instanceof w;
                    boolean z3 = true;
                    if (z2) {
                        w wVar = (w) obj;
                        Objects.requireNonNull(wVar);
                        if (!w.a.compareAndSet(wVar, 0, 1)) {
                            x(function1, obj);
                            throw null;
                        } else if (obj instanceof n) {
                            if (!z2) {
                                obj = null;
                            }
                            w wVar2 = (w) obj;
                            if (wVar2 != null) {
                                th = wVar2.f3819b;
                            }
                            n(function1, th);
                            return;
                        } else {
                            return;
                        }
                    } else if (obj instanceof v) {
                        v vVar = (v) obj;
                        if (vVar.f3817b != null) {
                            x(function1, obj);
                            throw null;
                        } else if (!(b1Var instanceof e)) {
                            Throwable th2 = vVar.e;
                            if (th2 == null) {
                                z3 = false;
                            }
                            if (z3) {
                                n(function1, th2);
                                return;
                            } else {
                                if (n.compareAndSet(this, obj, v.a(vVar, null, b1Var, null, null, null, 29))) {
                                    return;
                                }
                            }
                        } else {
                            return;
                        }
                    } else if (!(b1Var instanceof e)) {
                        if (n.compareAndSet(this, obj, new v(obj, b1Var, null, null, null, 28))) {
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    x(function1, obj);
                    throw null;
                }
            } else if (n.compareAndSet(this, obj, b1Var)) {
                return;
            }
        }
    }

    @Override // kotlinx.coroutines.CancellableContinuation
    public Object g(Throwable th) {
        return B(new w(th, false, 2), null, null);
    }

    @Override // kotlin.coroutines.Continuation
    public CoroutineContext getContext() {
        return this.o;
    }

    @Override // kotlinx.coroutines.CancellableContinuation
    public Object h(T t, Object obj, Function1<? super Throwable, Unit> function1) {
        return B(t, null, function1);
    }

    @Override // kotlinx.coroutines.CancellableContinuation
    public void i(CoroutineDispatcher coroutineDispatcher, T t) {
        Continuation<T> continuation = this.p;
        if (!(continuation instanceof g)) {
            continuation = null;
        }
        g gVar = (g) continuation;
        y(t, (gVar != null ? gVar.q : null) == coroutineDispatcher ? 4 : this.l, null);
    }

    /* JADX WARN: Multi-variable type inference failed */
    @Override // s.a.j0
    public <T> T j(Object obj) {
        return obj instanceof v ? (T) ((v) obj).a : obj;
    }

    @Override // kotlinx.coroutines.CancellableContinuation
    public boolean k(Throwable th) {
        Object obj;
        boolean z2;
        do {
            obj = this._state;
            if (!(obj instanceof o1)) {
                return false;
            }
            z2 = obj instanceof j;
        } while (!n.compareAndSet(this, obj, new n(this, th, z2)));
        if (!z2) {
            obj = null;
        }
        j jVar = (j) obj;
        if (jVar != null) {
            o(jVar, th);
        }
        s();
        t(this.l);
        return true;
    }

    @Override // s.a.j0
    public Object m() {
        return this._state;
    }

    public final void n(Function1<? super Throwable, Unit> function1, Throwable th) {
        try {
            function1.invoke(th);
        } catch (Throwable th2) {
            CoroutineContext coroutineContext = this.o;
            f.u0(coroutineContext, new CompletionHandlerException("Exception in invokeOnCancellation handler for " + this, th2));
        }
    }

    public final void o(j jVar, Throwable th) {
        try {
            jVar.a(th);
        } catch (Throwable th2) {
            CoroutineContext coroutineContext = this.o;
            f.u0(coroutineContext, new CompletionHandlerException("Exception in invokeOnCancellation handler for " + this, th2));
        }
    }

    public final void p(Function1<? super Throwable, Unit> function1, Throwable th) {
        try {
            function1.invoke(th);
        } catch (Throwable th2) {
            CoroutineContext coroutineContext = this.o;
            f.u0(coroutineContext, new CompletionHandlerException("Exception in resume onCancellation handler for " + this, th2));
        }
    }

    public final void q() {
        m0 m0Var = (m0) this._parentHandle;
        if (m0Var != null) {
            m0Var.dispose();
        }
        this._parentHandle = n1.j;
    }

    @Override // kotlinx.coroutines.CancellableContinuation
    public void r(Object obj) {
        t(this.l);
    }

    @Override // kotlin.coroutines.Continuation
    public void resumeWith(Object obj) {
        Throwable th = k.m75exceptionOrNullimpl(obj);
        if (th != null) {
            obj = new w(th, false, 2);
        }
        y(obj, this.l, null);
    }

    public final void s() {
        if (!w()) {
            q();
        }
    }

    /* JADX WARN: Finally extract failed */
    public final void t(int i) {
        boolean z2;
        boolean z3;
        while (true) {
            int i2 = this._decision;
            z2 = false;
            if (i2 == 0) {
                if (m.compareAndSet(this, 0, 2)) {
                    z3 = true;
                    break;
                }
            } else if (i2 == 1) {
                z3 = false;
            } else {
                throw new IllegalStateException("Already resumed".toString());
            }
        }
        if (!z3) {
            Continuation<T> d = d();
            if (i == 4) {
                z2 = true;
            }
            if (z2 || !(d instanceof g) || f.B0(i) != f.B0(this.l)) {
                f.Y0(this, d, z2);
                return;
            }
            CoroutineDispatcher coroutineDispatcher = ((g) d).q;
            CoroutineContext context = d.getContext();
            if (coroutineDispatcher.isDispatchNeeded(context)) {
                coroutineDispatcher.dispatch(context, this);
                return;
            }
            v1 v1Var = v1.f3818b;
            q0 a = v1.a();
            if (a.N()) {
                a.J(this);
                return;
            }
            a.L(true);
            try {
                f.Y0(this, d(), true);
                do {
                } while (a.R());
            } catch (Throwable th) {
                try {
                    l(th, null);
                } finally {
                    a.H(true);
                }
            }
        }
    }

    public String toString() {
        return "CancellableContinuation(" + f.s1(this.p) + "){" + this._state + "}@" + f.l0(this);
    }

    public final Object u() {
        boolean z2;
        Job job;
        A();
        while (true) {
            int i = this._decision;
            z2 = false;
            if (i == 0) {
                if (m.compareAndSet(this, 0, 1)) {
                    z2 = true;
                    break;
                }
            } else if (i != 2) {
                throw new IllegalStateException("Already suspended".toString());
            }
        }
        if (z2) {
            return c.getCOROUTINE_SUSPENDED();
        }
        Object obj = this._state;
        if (obj instanceof w) {
            throw ((w) obj).f3819b;
        } else if (!f.B0(this.l) || (job = (Job) this.o.get(Job.h)) == null || job.a()) {
            return j(obj);
        } else {
            CancellationException q = job.q();
            b(obj, q);
            throw q;
        }
    }

    public boolean v() {
        return !(this._state instanceof o1);
    }

    public final boolean w() {
        Continuation<T> continuation = this.p;
        if (!(continuation instanceof g)) {
            return false;
        }
        Object obj = ((g) continuation)._reusableCancellableContinuation;
        return obj != null && (!(obj instanceof l) || obj == this);
    }

    public final void x(Function1<? super Throwable, Unit> function1, Object obj) {
        throw new IllegalStateException(("It's prohibited to register multiple handlers, tried to register " + function1 + ", already has " + obj).toString());
    }

    public final void y(Object obj, int i, Function1<? super Throwable, Unit> function1) {
        Object obj2;
        do {
            obj2 = this._state;
            if (obj2 instanceof o1) {
            } else {
                if (obj2 instanceof n) {
                    n nVar = (n) obj2;
                    Objects.requireNonNull(nVar);
                    if (n.c.compareAndSet(nVar, 0, 1)) {
                        if (function1 != null) {
                            p(function1, nVar.f3819b);
                            return;
                        }
                        return;
                    }
                }
                throw new IllegalStateException(a.u("Already resumed, but proposed with update ", obj).toString());
            }
        } while (!n.compareAndSet(this, obj2, z((o1) obj2, obj, i, function1, null)));
        s();
        t(i);
    }

    public final Object z(o1 o1Var, Object obj, int i, Function1<? super Throwable, Unit> function1, Object obj2) {
        if (obj instanceof w) {
            return obj;
        }
        if (!f.B0(i) && obj2 == null) {
            return obj;
        }
        if (function1 == null && ((!(o1Var instanceof j) || (o1Var instanceof e)) && obj2 == null)) {
            return obj;
        }
        if (!(o1Var instanceof j)) {
            o1Var = null;
        }
        return new v(obj, (j) o1Var, function1, obj2, null, 16);
    }
}
