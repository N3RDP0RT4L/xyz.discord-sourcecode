package s.a;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.widgets.chat.input.MentionUtilsKt;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.Job;
/* compiled from: JobSupport.kt */
/* loaded from: classes3.dex */
public final class c1 extends e1<Job> {
    public static final AtomicIntegerFieldUpdater n = AtomicIntegerFieldUpdater.newUpdater(c1.class, "_invoked");
    public volatile int _invoked = 0;
    public final Function1<Throwable, Unit> o;

    /* JADX WARN: Multi-variable type inference failed */
    public c1(Job job, Function1<? super Throwable, Unit> function1) {
        super(job);
        this.o = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        q(th);
        return Unit.a;
    }

    @Override // s.a.y
    public void q(Throwable th) {
        if (n.compareAndSet(this, 0, 1)) {
            this.o.invoke(th);
        }
    }

    @Override // s.a.a.k
    public String toString() {
        StringBuilder R = a.R("InvokeOnCancelling[");
        R.append(c1.class.getSimpleName());
        R.append(MentionUtilsKt.MENTIONS_CHAR);
        R.append(f.l0(this));
        R.append(']');
        return R.toString();
    }
}
