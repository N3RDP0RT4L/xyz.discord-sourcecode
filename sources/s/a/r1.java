package s.a;

import kotlin.Unit;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.CoroutineDispatcher;
/* compiled from: Executors.kt */
/* loaded from: classes3.dex */
public final class r1 implements Runnable {
    public final CoroutineDispatcher j;
    public final CancellableContinuation<Unit> k;

    /* JADX WARN: Multi-variable type inference failed */
    public r1(CoroutineDispatcher coroutineDispatcher, CancellableContinuation<? super Unit> cancellableContinuation) {
        this.j = coroutineDispatcher;
        this.k = cancellableContinuation;
    }

    @Override // java.lang.Runnable
    public void run() {
        this.k.i(this.j, Unit.a);
    }
}
