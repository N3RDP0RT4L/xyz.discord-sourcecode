package s.a;

import kotlinx.coroutines.Job;
/* compiled from: JobSupport.kt */
/* loaded from: classes3.dex */
public class f1 extends h1 implements u {
    public final boolean k;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public f1(Job job) {
        super(true);
        h1 h1Var;
        boolean z2 = true;
        P(job);
        p pVar = (p) this._parentHandle;
        q qVar = (q) (!(pVar instanceof q) ? null : pVar);
        if (qVar != null && (h1Var = (h1) qVar.m) != null) {
            while (!h1Var.J()) {
                p pVar2 = (p) h1Var._parentHandle;
                q qVar2 = (q) (!(pVar2 instanceof q) ? null : pVar2);
                if (qVar2 != null) {
                    h1Var = (h1) qVar2.m;
                    if (h1Var == null) {
                    }
                }
            }
            this.k = z2;
        }
        z2 = false;
        this.k = z2;
    }

    @Override // s.a.h1
    public boolean J() {
        return this.k;
    }

    @Override // s.a.h1
    public boolean K() {
        return true;
    }
}
