package s.a;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import java.util.concurrent.locks.LockSupport;
import kotlin.Unit;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CancellableContinuation;
import s.a.a.m;
import s.a.a.t;
import s.a.a.v;
import s.a.a.w;
/* compiled from: EventLoop.common.kt */
/* loaded from: classes3.dex */
public abstract class r0 extends s0 implements h0 {
    public static final AtomicReferenceFieldUpdater n = AtomicReferenceFieldUpdater.newUpdater(r0.class, Object.class, "_queue");
    public static final AtomicReferenceFieldUpdater o = AtomicReferenceFieldUpdater.newUpdater(r0.class, Object.class, "_delayed");
    public volatile Object _queue = null;
    public volatile Object _delayed = null;
    public volatile int _isCompleted = 0;

    /* compiled from: EventLoop.common.kt */
    /* loaded from: classes3.dex */
    public final class a extends c {
        public final CancellableContinuation<Unit> m;

        /* JADX WARN: Multi-variable type inference failed */
        public a(long j, CancellableContinuation<? super Unit> cancellableContinuation) {
            super(j);
            this.m = cancellableContinuation;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.m.i(r0.this, Unit.a);
        }

        @Override // s.a.r0.c
        public String toString() {
            return super.toString() + this.m.toString();
        }
    }

    /* compiled from: EventLoop.common.kt */
    /* loaded from: classes3.dex */
    public static final class b extends c {
        public final Runnable m;

        public b(long j, Runnable runnable) {
            super(j);
            this.m = runnable;
        }

        @Override // java.lang.Runnable
        public void run() {
            this.m.run();
        }

        @Override // s.a.r0.c
        public String toString() {
            return super.toString() + this.m.toString();
        }
    }

    /* compiled from: EventLoop.common.kt */
    /* loaded from: classes3.dex */
    public static abstract class c implements Runnable, Comparable<c>, m0, w {
        public Object j;
        public int k = -1;
        public long l;

        public c(long j) {
            this.l = j;
        }

        @Override // java.lang.Comparable
        public int compareTo(c cVar) {
            int i = ((this.l - cVar.l) > 0L ? 1 : ((this.l - cVar.l) == 0L ? 0 : -1));
            if (i > 0) {
                return 1;
            }
            return i < 0 ? -1 : 0;
        }

        @Override // s.a.m0
        public final synchronized void dispose() {
            Object obj = this.j;
            t tVar = t0.a;
            if (obj != tVar) {
                if (!(obj instanceof d)) {
                    obj = null;
                }
                d dVar = (d) obj;
                if (dVar != null) {
                    synchronized (dVar) {
                        if (i() != null) {
                            dVar.c(getIndex());
                        }
                    }
                }
                this.j = tVar;
            }
        }

        @Override // s.a.a.w
        public void f(int i) {
            this.k = i;
        }

        @Override // s.a.a.w
        public void g(v<?> vVar) {
            if (this.j != t0.a) {
                this.j = vVar;
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }

        @Override // s.a.a.w
        public int getIndex() {
            return this.k;
        }

        @Override // s.a.a.w
        public v<?> i() {
            Object obj = this.j;
            if (!(obj instanceof v)) {
                obj = null;
            }
            return (v) obj;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Delayed[nanos=");
            R.append(this.l);
            R.append(']');
            return R.toString();
        }
    }

    /* compiled from: EventLoop.common.kt */
    /* loaded from: classes3.dex */
    public static final class d extends v<c> {

        /* renamed from: b  reason: collision with root package name */
        public long f3815b;

        public d(long j) {
            this.f3815b = j;
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:59:0x00a3  */
    /* JADX WARN: Removed duplicated region for block: B:96:? A[RETURN, SYNTHETIC] */
    @Override // s.a.q0
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public long O() {
        /*
            Method dump skipped, instructions count: 218
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.r0.O():long");
    }

    public final void T(Runnable runnable) {
        if (U(runnable)) {
            Thread S = S();
            if (Thread.currentThread() != S) {
                LockSupport.unpark(S);
                return;
            }
            return;
        }
        d0.q.T(runnable);
    }

    public final boolean U(Runnable runnable) {
        while (true) {
            Object obj = this._queue;
            if (this._isCompleted != 0) {
                return false;
            }
            if (obj == null) {
                if (n.compareAndSet(this, null, runnable)) {
                    return true;
                }
            } else if (obj instanceof m) {
                m mVar = (m) obj;
                int a2 = mVar.a(runnable);
                if (a2 == 0) {
                    return true;
                }
                if (a2 == 1) {
                    n.compareAndSet(this, obj, mVar.d());
                } else if (a2 == 2) {
                    return false;
                }
            } else if (obj == t0.f3816b) {
                return false;
            } else {
                m mVar2 = new m(8, true);
                mVar2.a((Runnable) obj);
                mVar2.a(runnable);
                if (n.compareAndSet(this, obj, mVar2)) {
                    return true;
                }
            }
        }
    }

    public boolean V() {
        s.a.a.b<j0<?>> bVar = this.m;
        if (!(bVar == null || bVar.f3798b == bVar.c)) {
            return false;
        }
        d dVar = (d) this._delayed;
        if (dVar != null) {
            if (!(dVar._size == 0)) {
                return false;
            }
        }
        Object obj = this._queue;
        if (obj == null) {
            return true;
        }
        return obj instanceof m ? ((m) obj).c() : obj == t0.f3816b;
    }

    /* JADX WARN: Removed duplicated region for block: B:35:0x0067  */
    /* JADX WARN: Removed duplicated region for block: B:40:0x007e  */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final void W(long r13, s.a.r0.c r15) {
        /*
            r12 = this;
            int r0 = r12._isCompleted
            r1 = 2
            r2 = 0
            r3 = 1
            r4 = 0
            if (r0 == 0) goto L9
            goto L38
        L9:
            java.lang.Object r0 = r12._delayed
            s.a.r0$d r0 = (s.a.r0.d) r0
            if (r0 == 0) goto L10
            goto L21
        L10:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r0 = s.a.r0.o
            s.a.r0$d r5 = new s.a.r0$d
            r5.<init>(r13)
            r0.compareAndSet(r12, r4, r5)
            java.lang.Object r0 = r12._delayed
            d0.z.d.m.checkNotNull(r0)
            s.a.r0$d r0 = (s.a.r0.d) r0
        L21:
            monitor-enter(r15)
            java.lang.Object r5 = r15.j     // Catch: java.lang.Throwable -> La7
            s.a.a.t r6 = s.a.t0.a     // Catch: java.lang.Throwable -> La7
            if (r5 != r6) goto L2b
            monitor-exit(r15)
            r0 = 2
            goto L65
        L2b:
            monitor-enter(r0)     // Catch: java.lang.Throwable -> La7
            s.a.a.w r5 = r0.b()     // Catch: java.lang.Throwable -> La4
            s.a.r0$c r5 = (s.a.r0.c) r5     // Catch: java.lang.Throwable -> La4
            int r6 = r12._isCompleted     // Catch: java.lang.Throwable -> La4
            if (r6 == 0) goto L3a
            monitor-exit(r0)     // Catch: java.lang.Throwable -> La7
            monitor-exit(r15)
        L38:
            r0 = 1
            goto L65
        L3a:
            r6 = 0
            if (r5 != 0) goto L41
            r0.f3815b = r13     // Catch: java.lang.Throwable -> La4
            goto L54
        L41:
            long r8 = r5.l     // Catch: java.lang.Throwable -> La4
            long r10 = r8 - r13
            int r5 = (r10 > r6 ? 1 : (r10 == r6 ? 0 : -1))
            if (r5 < 0) goto L4a
            r8 = r13
        L4a:
            long r10 = r0.f3815b     // Catch: java.lang.Throwable -> La4
            long r10 = r8 - r10
            int r5 = (r10 > r6 ? 1 : (r10 == r6 ? 0 : -1))
            if (r5 <= 0) goto L54
            r0.f3815b = r8     // Catch: java.lang.Throwable -> La4
        L54:
            long r8 = r15.l     // Catch: java.lang.Throwable -> La4
            long r10 = r0.f3815b     // Catch: java.lang.Throwable -> La4
            long r8 = r8 - r10
            int r5 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r5 >= 0) goto L5f
            r15.l = r10     // Catch: java.lang.Throwable -> La4
        L5f:
            r0.a(r15)     // Catch: java.lang.Throwable -> La4
            monitor-exit(r0)     // Catch: java.lang.Throwable -> La7
            monitor-exit(r15)
            r0 = 0
        L65:
            if (r0 == 0) goto L7e
            if (r0 == r3) goto L78
            if (r0 != r1) goto L6c
            goto La3
        L6c:
            java.lang.String r13 = "unexpected result"
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r13 = r13.toString()
            r14.<init>(r13)
            throw r14
        L78:
            s.a.d0 r0 = s.a.d0.q
            r0.W(r13, r15)
            goto La3
        L7e:
            java.lang.Object r13 = r12._delayed
            s.a.r0$d r13 = (s.a.r0.d) r13
            if (r13 == 0) goto L91
            monitor-enter(r13)
            s.a.a.w r14 = r13.b()     // Catch: java.lang.Throwable -> L8e
            monitor-exit(r13)
            r4 = r14
            s.a.r0$c r4 = (s.a.r0.c) r4
            goto L91
        L8e:
            r14 = move-exception
            monitor-exit(r13)
            throw r14
        L91:
            if (r4 != r15) goto L94
            r2 = 1
        L94:
            if (r2 == 0) goto La3
            java.lang.Thread r13 = r12.S()
            java.lang.Thread r14 = java.lang.Thread.currentThread()
            if (r14 == r13) goto La3
            java.util.concurrent.locks.LockSupport.unpark(r13)
        La3:
            return
        La4:
            r13 = move-exception
            monitor-exit(r0)     // Catch: java.lang.Throwable -> La7
            throw r13     // Catch: java.lang.Throwable -> La7
        La7:
            r13 = move-exception
            monitor-exit(r15)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.r0.W(long, s.a.r0$c):void");
    }

    @Override // s.a.h0
    public void c(long j, CancellableContinuation<? super Unit> cancellableContinuation) {
        long a2 = t0.a(j);
        if (a2 < 4611686018427387903L) {
            long nanoTime = System.nanoTime();
            a aVar = new a(a2 + nanoTime, cancellableContinuation);
            cancellableContinuation.f(new n0(aVar));
            W(nanoTime, aVar);
        }
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public final void dispatch(CoroutineContext coroutineContext, Runnable runnable) {
        T(runnable);
    }

    @Override // s.a.q0
    public void shutdown() {
        c c2;
        v1 v1Var = v1.f3818b;
        v1.a.set(null);
        this._isCompleted = 1;
        while (true) {
            Object obj = this._queue;
            if (obj == null) {
                if (n.compareAndSet(this, null, t0.f3816b)) {
                    break;
                }
            } else if (obj instanceof m) {
                ((m) obj).b();
                break;
            } else if (obj == t0.f3816b) {
                break;
            } else {
                m mVar = new m(8, true);
                mVar.a((Runnable) obj);
                if (n.compareAndSet(this, obj, mVar)) {
                    break;
                }
            }
        }
        do {
        } while (O() <= 0);
        long nanoTime = System.nanoTime();
        while (true) {
            d dVar = (d) this._delayed;
            if (dVar != null) {
                synchronized (dVar) {
                    c2 = dVar._size > 0 ? dVar.c(0) : null;
                }
                c cVar = c2;
                if (cVar != null) {
                    d0.q.W(nanoTime, cVar);
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    public m0 x(long j, Runnable runnable, CoroutineContext coroutineContext) {
        return e0.a.x(j, runnable, coroutineContext);
    }
}
