package s.a;

import b.i.a.f.e.o.f;
import d0.b;
import d0.k;
import d0.l;
import d0.z.d.m;
import java.util.concurrent.CancellationException;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.Job;
import s.a.a.a;
import s.a.a.g;
import s.a.d2.h;
import s.a.d2.i;
/* compiled from: DispatchedTask.kt */
/* loaded from: classes3.dex */
public abstract class j0<T> extends h {
    public int l;

    public j0(int i) {
        this.l = i;
    }

    public void b(Object obj, Throwable th) {
    }

    public abstract Continuation<T> d();

    public Throwable e(Object obj) {
        if (!(obj instanceof w)) {
            obj = null;
        }
        w wVar = (w) obj;
        if (wVar != null) {
            return wVar.f3819b;
        }
        return null;
    }

    /* JADX WARN: Multi-variable type inference failed */
    public <T> T j(Object obj) {
        return obj;
    }

    public final void l(Throwable th, Throwable th2) {
        if (th != null || th2 != null) {
            if (!(th == null || th2 == null)) {
                b.addSuppressed(th, th2);
            }
            if (th == null) {
                th = th2;
            }
            m.checkNotNull(th);
            f.u0(d().getContext(), new c0("Fatal exception in coroutines machinery for " + this + ". Please read KDoc to 'handleFatalException' method and report this incident to maintainers", th));
        }
    }

    public abstract Object m();

    @Override // java.lang.Runnable
    public final void run() {
        Object obj;
        Object obj2;
        i iVar = this.k;
        try {
            Continuation<T> d = d();
            if (d != null) {
                g gVar = (g) d;
                Continuation<T> continuation = gVar.r;
                CoroutineContext context = continuation.getContext();
                Object m = m();
                Object b2 = a.b(context, gVar.p);
                Throwable e = e(m);
                Job job = (e != null || !f.B0(this.l)) ? null : (Job) context.get(Job.h);
                if (job != null && !job.a()) {
                    CancellationException q = job.q();
                    b(m, q);
                    k.a aVar = k.j;
                    continuation.resumeWith(k.m73constructorimpl(l.createFailure(q)));
                } else if (e != null) {
                    k.a aVar2 = k.j;
                    continuation.resumeWith(k.m73constructorimpl(l.createFailure(e)));
                } else {
                    T j = j(m);
                    k.a aVar3 = k.j;
                    continuation.resumeWith(k.m73constructorimpl(j));
                }
                Unit unit = Unit.a;
                a.a(context, b2);
                try {
                    k.a aVar4 = k.j;
                    iVar.f();
                    obj2 = k.m73constructorimpl(unit);
                } catch (Throwable th) {
                    k.a aVar5 = k.j;
                    obj2 = k.m73constructorimpl(l.createFailure(th));
                }
                l(null, k.m75exceptionOrNullimpl(obj2));
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type kotlinx.coroutines.internal.DispatchedContinuation<T>");
        } catch (Throwable th2) {
            try {
                k.a aVar6 = k.j;
                iVar.f();
                obj = k.m73constructorimpl(Unit.a);
            } catch (Throwable th3) {
                k.a aVar7 = k.j;
                obj = k.m73constructorimpl(l.createFailure(th3));
            }
            l(th2, k.m75exceptionOrNullimpl(obj));
        }
    }
}
