package s.a.a2;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.VisibleForTesting;
import d0.k;
import d0.l;
import java.util.Objects;
/* compiled from: HandlerDispatcher.kt */
/* loaded from: classes3.dex */
public final class c {
    static {
        Object obj;
        b bVar = null;
        try {
            k.a aVar = k.j;
            obj = k.m73constructorimpl(new a(a(Looper.getMainLooper(), true), null, false));
        } catch (Throwable th) {
            k.a aVar2 = k.j;
            obj = k.m73constructorimpl(l.createFailure(th));
        }
        if (!k.m77isFailureimpl(obj)) {
            bVar = obj;
        }
    }

    @VisibleForTesting
    public static final Handler a(Looper looper, boolean z2) {
        if (!z2) {
            return new Handler(looper);
        }
        if (Build.VERSION.SDK_INT >= 28) {
            Object invoke = Handler.class.getDeclaredMethod("createAsync", Looper.class).invoke(null, looper);
            Objects.requireNonNull(invoke, "null cannot be cast to non-null type android.os.Handler");
            return (Handler) invoke;
        }
        try {
            return (Handler) Handler.class.getDeclaredConstructor(Looper.class, Handler.Callback.class, Boolean.TYPE).newInstance(looper, null, Boolean.TRUE);
        } catch (NoSuchMethodException unused) {
            return new Handler(looper);
        }
    }
}
