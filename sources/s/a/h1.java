package s.a;

import b.i.a.f.e.o.f;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.f0.l;
import d0.w.i.a.e;
import d0.z.d.m;
import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.sequences.Sequence;
import kotlinx.coroutines.CompletionHandlerException;
import kotlinx.coroutines.Job;
import kotlinx.coroutines.JobCancellationException;
import kotlinx.coroutines.TimeoutCancellationException;
import s.a.a.j;
import s.a.a.k;
import s.a.a.p;
/* compiled from: JobSupport.kt */
/* loaded from: classes3.dex */
public class h1 implements Job, r, p1 {
    public static final AtomicReferenceFieldUpdater j = AtomicReferenceFieldUpdater.newUpdater(h1.class, Object.class, "_state");
    public volatile Object _parentHandle;
    public volatile Object _state;

    /* compiled from: JobSupport.kt */
    /* loaded from: classes3.dex */
    public static final class a extends g1<Job> {
        public final h1 n;
        public final b o;
        public final q p;
        public final Object q;

        public a(h1 h1Var, b bVar, q qVar, Object obj) {
            super(qVar.n);
            this.n = h1Var;
            this.o = bVar;
            this.p = qVar;
            this.q = obj;
        }

        @Override // kotlin.jvm.functions.Function1
        public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
            q(th);
            return Unit.a;
        }

        @Override // s.a.y
        public void q(Throwable th) {
            h1 h1Var = this.n;
            b bVar = this.o;
            q qVar = this.p;
            Object obj = this.q;
            q U = h1Var.U(qVar);
            if (U == null || !h1Var.d0(bVar, U, obj)) {
                h1Var.v(h1Var.H(bVar, obj));
            }
        }

        @Override // s.a.a.k
        public String toString() {
            StringBuilder R = b.d.b.a.a.R("ChildCompletion[");
            R.append(this.p);
            R.append(", ");
            R.append(this.q);
            R.append(']');
            return R.toString();
        }
    }

    /* compiled from: JobSupport.kt */
    /* loaded from: classes3.dex */
    public static final class b implements z0 {
        public volatile Object _exceptionsHolder = null;
        public volatile int _isCompleting;
        public volatile Object _rootCause;
        public final m1 j;

        public b(m1 m1Var, boolean z2, Throwable th) {
            this.j = m1Var;
            this._isCompleting = z2 ? 1 : 0;
            this._rootCause = th;
        }

        @Override // s.a.z0
        public boolean a() {
            return ((Throwable) this._rootCause) == null;
        }

        /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Object] */
        public final void b(Throwable th) {
            Throwable th2 = (Throwable) this._rootCause;
            if (th2 == null) {
                this._rootCause = th;
            } else if (th != th2) {
                ?? r0 = this._exceptionsHolder;
                if (r0 == 0) {
                    this._exceptionsHolder = th;
                } else if (r0 instanceof Throwable) {
                    if (th != r0) {
                        ArrayList<Throwable> c = c();
                        c.add(r0);
                        c.add(th);
                        this._exceptionsHolder = c;
                    }
                } else if (r0 instanceof ArrayList) {
                    ((ArrayList) r0).add(th);
                } else {
                    throw new IllegalStateException(b.d.b.a.a.u("State is ", r0).toString());
                }
            }
        }

        public final ArrayList<Throwable> c() {
            return new ArrayList<>(4);
        }

        public final boolean d() {
            return ((Throwable) this._rootCause) != null;
        }

        public final boolean e() {
            return this._exceptionsHolder == i1.e;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [java.lang.Object] */
        public final List<Throwable> f(Throwable th) {
            ArrayList<Throwable> arrayList;
            ?? r0 = this._exceptionsHolder;
            if (r0 == 0) {
                arrayList = c();
            } else if (r0 instanceof Throwable) {
                ArrayList<Throwable> c = c();
                c.add(r0);
                arrayList = c;
            } else if (r0 instanceof ArrayList) {
                arrayList = (ArrayList) r0;
            } else {
                throw new IllegalStateException(b.d.b.a.a.u("State is ", r0).toString());
            }
            Throwable th2 = (Throwable) this._rootCause;
            if (th2 != null) {
                arrayList.add(0, th2);
            }
            if (th != null && (!m.areEqual(th, th2))) {
                arrayList.add(th);
            }
            this._exceptionsHolder = i1.e;
            return arrayList;
        }

        @Override // s.a.z0
        public m1 getList() {
            return this.j;
        }

        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r1v2, types: [boolean, int] */
        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Finishing[cancelling=");
            R.append(d());
            R.append(", completing=");
            R.append((boolean) this._isCompleting);
            R.append(", rootCause=");
            R.append((Throwable) this._rootCause);
            R.append(", exceptions=");
            R.append(this._exceptionsHolder);
            R.append(", list=");
            R.append(this.j);
            R.append(']');
            return R.toString();
        }
    }

    /* compiled from: LockFreeLinkedList.kt */
    /* loaded from: classes3.dex */
    public static final class c extends k.a {
        public final /* synthetic */ h1 d;
        public final /* synthetic */ Object e;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public c(k kVar, k kVar2, h1 h1Var, Object obj) {
            super(kVar2);
            this.d = h1Var;
            this.e = obj;
        }

        @Override // s.a.a.d
        public Object c(k kVar) {
            if (this.d.M() == this.e) {
                return null;
            }
            return j.a;
        }
    }

    /* compiled from: JobSupport.kt */
    @e(c = "kotlinx.coroutines.JobSupport$children$1", f = "JobSupport.kt", l = {949, 951}, m = "invokeSuspend")
    /* loaded from: classes3.dex */
    public static final class d extends d0.w.i.a.j implements Function2<d0.f0.k<? super r>, Continuation<? super Unit>, Object> {
        public Object L$0;
        public Object L$1;
        public Object L$2;
        public Object L$3;
        public Object L$4;
        public Object L$5;
        public int label;
        private d0.f0.k p$;

        public d(Continuation continuation) {
            super(2, continuation);
        }

        @Override // d0.w.i.a.a
        public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
            d dVar = new d(continuation);
            dVar.p$ = (d0.f0.k) obj;
            return dVar;
        }

        @Override // kotlin.jvm.functions.Function2
        public final Object invoke(d0.f0.k<? super r> kVar, Continuation<? super Unit> continuation) {
            d dVar = new d(continuation);
            dVar.p$ = kVar;
            return dVar.invokeSuspend(Unit.a);
        }

        /* JADX WARN: Removed duplicated region for block: B:22:0x007f  */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:23:0x0081 -> B:27:0x009d). Please submit an issue!!! */
        /* JADX WARN: Unsupported multi-entry loop pattern (BACK_EDGE: B:25:0x009a -> B:27:0x009d). Please submit an issue!!! */
        @Override // d0.w.i.a.a
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
                r10 = this;
                java.lang.Object r0 = d0.w.h.c.getCOROUTINE_SUSPENDED()
                int r1 = r10.label
                r2 = 2
                r3 = 1
                if (r1 == 0) goto L3a
                if (r1 == r3) goto L32
                if (r1 != r2) goto L2a
                java.lang.Object r1 = r10.L$5
                s.a.q r1 = (s.a.q) r1
                java.lang.Object r1 = r10.L$4
                s.a.a.k r1 = (s.a.a.k) r1
                java.lang.Object r4 = r10.L$3
                s.a.a.i r4 = (s.a.a.i) r4
                java.lang.Object r5 = r10.L$2
                s.a.m1 r5 = (s.a.m1) r5
                java.lang.Object r6 = r10.L$1
                java.lang.Object r7 = r10.L$0
                d0.f0.k r7 = (d0.f0.k) r7
                d0.l.throwOnFailure(r11)
                r11 = r10
                goto L9d
            L2a:
                java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r11.<init>(r0)
                throw r11
            L32:
                java.lang.Object r0 = r10.L$0
                d0.f0.k r0 = (d0.f0.k) r0
                d0.l.throwOnFailure(r11)
                goto La2
            L3a:
                d0.l.throwOnFailure(r11)
                d0.f0.k r11 = r10.p$
                s.a.h1 r1 = s.a.h1.this
                java.lang.Object r1 = r1.M()
                boolean r4 = r1 instanceof s.a.q
                if (r4 == 0) goto L5b
                r2 = r1
                s.a.q r2 = (s.a.q) r2
                s.a.r r2 = r2.n
                r10.L$0 = r11
                r10.L$1 = r1
                r10.label = r3
                java.lang.Object r11 = r11.yield(r2, r10)
                if (r11 != r0) goto La2
                return r0
            L5b:
                boolean r4 = r1 instanceof s.a.z0
                if (r4 == 0) goto La2
                r4 = r1
                s.a.z0 r4 = (s.a.z0) r4
                s.a.m1 r4 = r4.getList()
                if (r4 == 0) goto La2
                java.lang.Object r5 = r4.i()
            */
            //  java.lang.String r6 = "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"
            /*
                java.util.Objects.requireNonNull(r5, r6)
                s.a.a.k r5 = (s.a.a.k) r5
                r7 = r11
                r6 = r1
                r1 = r5
                r11 = r10
                r5 = r4
            L78:
                boolean r8 = d0.z.d.m.areEqual(r1, r4)
                r8 = r8 ^ r3
                if (r8 == 0) goto La2
                boolean r8 = r1 instanceof s.a.q
                if (r8 == 0) goto L9d
                r8 = r1
                s.a.q r8 = (s.a.q) r8
                s.a.r r9 = r8.n
                r11.L$0 = r7
                r11.L$1 = r6
                r11.L$2 = r5
                r11.L$3 = r4
                r11.L$4 = r1
                r11.L$5 = r8
                r11.label = r2
                java.lang.Object r8 = r7.yield(r9, r11)
                if (r8 != r0) goto L9d
                return r0
            L9d:
                s.a.a.k r1 = r1.j()
                goto L78
            La2:
                kotlin.Unit r11 = kotlin.Unit.a
                return r11
            */
            throw new UnsupportedOperationException("Method not decompiled: s.a.h1.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    public h1(boolean z2) {
        this._state = z2 ? i1.g : i1.f;
        this._parentHandle = null;
    }

    public static /* synthetic */ CancellationException b0(h1 h1Var, Throwable th, String str, int i, Object obj) {
        int i2 = i & 1;
        return h1Var.a0(th, null);
    }

    @Override // s.a.p1
    public CancellationException A() {
        Throwable th;
        Object M = M();
        CancellationException cancellationException = null;
        if (M instanceof b) {
            th = (Throwable) ((b) M)._rootCause;
        } else if (M instanceof w) {
            th = ((w) M).f3819b;
        } else if (!(M instanceof z0)) {
            th = null;
        } else {
            throw new IllegalStateException(b.d.b.a.a.u("Cannot be cancelling child in this state: ", M).toString());
        }
        if (th instanceof CancellationException) {
            cancellationException = th;
        }
        CancellationException cancellationException2 = cancellationException;
        if (cancellationException2 != null) {
            return cancellationException2;
        }
        StringBuilder R = b.d.b.a.a.R("Parent job is ");
        R.append(Z(M));
        return new JobCancellationException(R.toString(), th, this);
    }

    public String B() {
        return "Job was cancelled";
    }

    @Override // kotlinx.coroutines.Job
    public final p D(r rVar) {
        m0 w0 = f.w0(this, true, false, new q(this, rVar), 2, null);
        Objects.requireNonNull(w0, "null cannot be cast to non-null type kotlinx.coroutines.ChildHandle");
        return (p) w0;
    }

    public boolean E(Throwable th) {
        if (th instanceof CancellationException) {
            return true;
        }
        return w(th) && J();
    }

    public final void F(z0 z0Var, Object obj) {
        p pVar = (p) this._parentHandle;
        if (pVar != null) {
            pVar.dispose();
            this._parentHandle = n1.j;
        }
        CompletionHandlerException completionHandlerException = null;
        if (!(obj instanceof w)) {
            obj = null;
        }
        w wVar = (w) obj;
        Throwable th = wVar != null ? wVar.f3819b : null;
        if (z0Var instanceof g1) {
            try {
                ((g1) z0Var).q(th);
            } catch (Throwable th2) {
                O(new CompletionHandlerException("Exception in completion handler " + z0Var + " for " + this, th2));
            }
        } else {
            m1 list = z0Var.getList();
            if (list != null) {
                Object i = list.i();
                Objects.requireNonNull(i, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                for (k kVar = (k) i; !m.areEqual(kVar, list); kVar = kVar.j()) {
                    if (kVar instanceof g1) {
                        g1 g1Var = (g1) kVar;
                        try {
                            g1Var.q(th);
                        } catch (Throwable th3) {
                            if (completionHandlerException != null) {
                                d0.b.addSuppressed(completionHandlerException, th3);
                            } else {
                                completionHandlerException = new CompletionHandlerException("Exception in completion handler " + g1Var + " for " + this, th3);
                            }
                        }
                    }
                }
                if (completionHandlerException != null) {
                    O(completionHandlerException);
                }
            }
        }
    }

    public final Throwable G(Object obj) {
        if (obj != null ? obj instanceof Throwable : true) {
            return obj != null ? (Throwable) obj : new JobCancellationException(B(), null, this);
        }
        Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlinx.coroutines.ParentJob");
        return ((p1) obj).A();
    }

    public final Object H(b bVar, Object obj) {
        Throwable I;
        Throwable th = null;
        w wVar = (w) (!(obj instanceof w) ? null : obj);
        if (wVar != null) {
            th = wVar.f3819b;
        }
        synchronized (bVar) {
            bVar.d();
            List<Throwable> f = bVar.f(th);
            I = I(bVar, f);
            if (I != null && f.size() > 1) {
                Set newSetFromMap = Collections.newSetFromMap(new IdentityHashMap(f.size()));
                for (Throwable th2 : f) {
                    if (th2 != I && th2 != I && !(th2 instanceof CancellationException) && newSetFromMap.add(th2)) {
                        d0.b.addSuppressed(I, th2);
                    }
                }
            }
        }
        if (!(I == null || I == th)) {
            obj = new w(I, false, 2);
        }
        if (I != null) {
            if (z(I) || N(I)) {
                Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlinx.coroutines.CompletedExceptionally");
                w.a.compareAndSet((w) obj, 0, 1);
            }
        }
        W(obj);
        j.compareAndSet(this, bVar, obj instanceof z0 ? new a1((z0) obj) : obj);
        F(bVar, obj);
        return obj;
    }

    public final Throwable I(b bVar, List<? extends Throwable> list) {
        Object obj;
        boolean z2;
        Object obj2 = null;
        if (!list.isEmpty()) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (!(((Throwable) obj) instanceof CancellationException)) {
                    break;
                }
            }
            Throwable th = (Throwable) obj;
            if (th != null) {
                return th;
            }
            Throwable th2 = list.get(0);
            if (th2 instanceof TimeoutCancellationException) {
                Iterator<T> it2 = list.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    Object next = it2.next();
                    Throwable th3 = (Throwable) next;
                    if (th3 == th2 || !(th3 instanceof TimeoutCancellationException)) {
                        z2 = false;
                        continue;
                    } else {
                        z2 = true;
                        continue;
                    }
                    if (z2) {
                        obj2 = next;
                        break;
                    }
                }
                Throwable th4 = (Throwable) obj2;
                if (th4 != null) {
                    return th4;
                }
            }
            return th2;
        } else if (bVar.d()) {
            return new JobCancellationException(B(), null, this);
        } else {
            return null;
        }
    }

    public boolean J() {
        return true;
    }

    public boolean K() {
        return false;
    }

    public final m1 L(z0 z0Var) {
        m1 list = z0Var.getList();
        if (list != null) {
            return list;
        }
        if (z0Var instanceof p0) {
            return new m1();
        }
        if (z0Var instanceof g1) {
            Y((g1) z0Var);
            return null;
        }
        throw new IllegalStateException(("State should have list: " + z0Var).toString());
    }

    public final Object M() {
        while (true) {
            Object obj = this._state;
            if (!(obj instanceof p)) {
                return obj;
            }
            ((p) obj).a(this);
        }
    }

    public boolean N(Throwable th) {
        return false;
    }

    public void O(Throwable th) {
        throw th;
    }

    public final void P(Job job) {
        if (job == null) {
            this._parentHandle = n1.j;
            return;
        }
        job.start();
        p D = job.D(this);
        this._parentHandle = D;
        if (!(M() instanceof z0)) {
            D.dispose();
            this._parentHandle = n1.j;
        }
    }

    public boolean Q() {
        return this instanceof f;
    }

    public final Object R(Object obj) {
        Object c02;
        do {
            c02 = c0(M(), obj);
            if (c02 == i1.a) {
                String str = "Job " + this + " is already complete or completing, but is being completed with " + obj;
                Throwable th = null;
                if (!(obj instanceof w)) {
                    obj = null;
                }
                w wVar = (w) obj;
                if (wVar != null) {
                    th = wVar.f3819b;
                }
                throw new IllegalStateException(str, th);
            }
        } while (c02 == i1.c);
        return c02;
    }

    public final g1<?> S(Function1<? super Throwable, Unit> function1, boolean z2) {
        e1 e1Var = null;
        if (z2) {
            if (function1 instanceof e1) {
                e1Var = function1;
            }
            e1 e1Var2 = e1Var;
            return e1Var2 != null ? e1Var2 : new c1(this, function1);
        }
        if (function1 instanceof g1) {
            e1Var = function1;
        }
        g1<?> g1Var = e1Var;
        return g1Var != null ? g1Var : new d1(this, function1);
    }

    public String T() {
        return getClass().getSimpleName();
    }

    public final q U(k kVar) {
        while (kVar.m()) {
            kVar = kVar.k();
        }
        while (true) {
            kVar = kVar.j();
            if (!kVar.m()) {
                if (kVar instanceof q) {
                    return (q) kVar;
                }
                if (kVar instanceof m1) {
                    return null;
                }
            }
        }
    }

    public final void V(m1 m1Var, Throwable th) {
        CompletionHandlerException completionHandlerException = null;
        Object i = m1Var.i();
        Objects.requireNonNull(i, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
        for (k kVar = (k) i; !m.areEqual(kVar, m1Var); kVar = kVar.j()) {
            if (kVar instanceof e1) {
                g1 g1Var = (g1) kVar;
                try {
                    g1Var.q(th);
                } catch (Throwable th2) {
                    if (completionHandlerException != null) {
                        d0.b.addSuppressed(completionHandlerException, th2);
                    } else {
                        completionHandlerException = new CompletionHandlerException("Exception in completion handler " + g1Var + " for " + this, th2);
                    }
                }
            }
        }
        if (completionHandlerException != null) {
            O(completionHandlerException);
        }
        z(th);
    }

    public void W(Object obj) {
    }

    public void X() {
    }

    public final void Y(g1<?> g1Var) {
        m1 m1Var = new m1();
        k.k.lazySet(m1Var, g1Var);
        k.j.lazySet(m1Var, g1Var);
        while (true) {
            if (g1Var.i() == g1Var) {
                if (k.j.compareAndSet(g1Var, g1Var, m1Var)) {
                    m1Var.g(g1Var);
                    break;
                }
            } else {
                break;
            }
        }
        j.compareAndSet(this, g1Var, g1Var.j());
    }

    public final String Z(Object obj) {
        if (!(obj instanceof b)) {
            return obj instanceof z0 ? ((z0) obj).a() ? "Active" : "New" : obj instanceof w ? "Cancelled" : "Completed";
        }
        b bVar = (b) obj;
        return bVar.d() ? "Cancelling" : bVar._isCompleting != 0 ? "Completing" : "Active";
    }

    @Override // kotlinx.coroutines.Job
    public boolean a() {
        Object M = M();
        return (M instanceof z0) && ((z0) M).a();
    }

    public final CancellationException a0(Throwable th, String str) {
        CancellationException cancellationException = (CancellationException) (!(th instanceof CancellationException) ? null : th);
        if (cancellationException == null) {
            if (str == null) {
                str = B();
            }
            cancellationException = new JobCancellationException(str, th, this);
        }
        return cancellationException;
    }

    @Override // kotlinx.coroutines.Job
    public void b(CancellationException cancellationException) {
        if (cancellationException == null) {
            cancellationException = new JobCancellationException(B(), null, this);
        }
        x(cancellationException);
    }

    public final Object c0(Object obj, Object obj2) {
        if (!(obj instanceof z0)) {
            return i1.a;
        }
        boolean z2 = true;
        if (((obj instanceof p0) || (obj instanceof g1)) && !(obj instanceof q) && !(obj2 instanceof w)) {
            z0 z0Var = (z0) obj;
            if (!j.compareAndSet(this, z0Var, obj2 instanceof z0 ? new a1((z0) obj2) : obj2)) {
                z2 = false;
            } else {
                W(obj2);
                F(z0Var, obj2);
            }
            return z2 ? obj2 : i1.c;
        }
        z0 z0Var2 = (z0) obj;
        m1 L = L(z0Var2);
        if (L == null) {
            return i1.c;
        }
        q qVar = null;
        b bVar = (b) (!(z0Var2 instanceof b) ? null : z0Var2);
        if (bVar == null) {
            bVar = new b(L, false, null);
        }
        synchronized (bVar) {
            if (bVar._isCompleting != 0) {
                return i1.a;
            }
            bVar._isCompleting = 1;
            if (bVar == z0Var2 || j.compareAndSet(this, z0Var2, bVar)) {
                boolean d2 = bVar.d();
                w wVar = (w) (!(obj2 instanceof w) ? null : obj2);
                if (wVar != null) {
                    bVar.b(wVar.f3819b);
                }
                Throwable th = (Throwable) bVar._rootCause;
                if (!(true ^ d2)) {
                    th = null;
                }
                if (th != null) {
                    V(L, th);
                }
                q qVar2 = (q) (!(z0Var2 instanceof q) ? null : z0Var2);
                if (qVar2 != null) {
                    qVar = qVar2;
                } else {
                    m1 list = z0Var2.getList();
                    if (list != null) {
                        qVar = U(list);
                    }
                }
                if (qVar == null || !d0(bVar, qVar, obj2)) {
                    return H(bVar, obj2);
                }
                return i1.f3813b;
            }
            return i1.c;
        }
    }

    public final boolean d0(b bVar, q qVar, Object obj) {
        while (f.w0(qVar.n, false, false, new a(this, bVar, qVar, obj), 1, null) == n1.j) {
            qVar = U(qVar);
            if (qVar == null) {
                return false;
            }
        }
        return true;
    }

    @Override // kotlinx.coroutines.Job
    public final Sequence<Job> e() {
        return l.sequence(new d(null));
    }

    @Override // kotlin.coroutines.CoroutineContext
    public <R> R fold(R r, Function2<? super R, ? super CoroutineContext.Element, ? extends R> function2) {
        return (R) CoroutineContext.Element.a.fold(this, r, function2);
    }

    @Override // kotlin.coroutines.CoroutineContext.Element, kotlin.coroutines.CoroutineContext
    public <E extends CoroutineContext.Element> E get(CoroutineContext.Key<E> key) {
        return (E) CoroutineContext.Element.a.get(this, key);
    }

    @Override // kotlin.coroutines.CoroutineContext.Element
    public final CoroutineContext.Key<?> getKey() {
        return Job.h;
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext minusKey(CoroutineContext.Key<?> key) {
        return CoroutineContext.Element.a.minusKey(this, key);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r4v3, types: [s.a.y0] */
    @Override // kotlinx.coroutines.Job
    public final m0 n(boolean z2, boolean z3, Function1<? super Throwable, Unit> function1) {
        Throwable th;
        Throwable th2 = null;
        g1<?> g1Var = null;
        while (true) {
            Object M = M();
            if (M instanceof p0) {
                p0 p0Var = (p0) M;
                if (p0Var.j) {
                    if (g1Var == null) {
                        g1Var = S(function1, z2);
                    }
                    if (j.compareAndSet(this, M, g1Var)) {
                        return g1Var;
                    }
                } else {
                    m1 m1Var = new m1();
                    if (!p0Var.j) {
                        m1Var = new y0(m1Var);
                    }
                    j.compareAndSet(this, p0Var, m1Var);
                }
            } else if (M instanceof z0) {
                m1 list = ((z0) M).getList();
                if (list == null) {
                    Objects.requireNonNull(M, "null cannot be cast to non-null type kotlinx.coroutines.JobNode<*>");
                    Y((g1) M);
                } else {
                    m0 m0Var = n1.j;
                    if (!z2 || !(M instanceof b)) {
                        th = null;
                    } else {
                        synchronized (M) {
                            th = (Throwable) ((b) M)._rootCause;
                            if (th == null || ((function1 instanceof q) && ((b) M)._isCompleting == 0)) {
                                if (g1Var == null) {
                                    g1Var = S(function1, z2);
                                }
                                if (t(M, list, g1Var)) {
                                    if (th == null) {
                                        return g1Var;
                                    }
                                    m0Var = g1Var;
                                }
                            }
                        }
                    }
                    if (th != null) {
                        if (z3) {
                            function1.invoke(th);
                        }
                        return m0Var;
                    }
                    if (g1Var == null) {
                        g1Var = S(function1, z2);
                    }
                    if (t(M, list, g1Var)) {
                        return g1Var;
                    }
                }
            } else {
                if (z3) {
                    if (!(M instanceof w)) {
                        M = null;
                    }
                    w wVar = (w) M;
                    if (wVar != null) {
                        th2 = wVar.f3819b;
                    }
                    function1.invoke(th2);
                }
                return n1.j;
            }
        }
    }

    @Override // kotlin.coroutines.CoroutineContext
    public CoroutineContext plus(CoroutineContext coroutineContext) {
        return CoroutineContext.Element.a.plus(this, coroutineContext);
    }

    @Override // kotlinx.coroutines.Job
    public final CancellationException q() {
        Object M = M();
        if (M instanceof b) {
            Throwable th = (Throwable) ((b) M)._rootCause;
            if (th != null) {
                return a0(th, getClass().getSimpleName() + " is cancelling");
            }
            throw new IllegalStateException(("Job is still new or active: " + this).toString());
        } else if (M instanceof z0) {
            throw new IllegalStateException(("Job is still new or active: " + this).toString());
        } else if (M instanceof w) {
            return b0(this, ((w) M).f3819b, null, 1, null);
        } else {
            return new JobCancellationException(getClass().getSimpleName() + " has completed normally", null, this);
        }
    }

    @Override // s.a.r
    public final void s(p1 p1Var) {
        w(p1Var);
    }

    /* JADX WARN: Removed duplicated region for block: B:20:0x0040 A[SYNTHETIC] */
    @Override // kotlinx.coroutines.Job
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean start() {
        /*
            r6 = this;
        L0:
            java.lang.Object r0 = r6.M()
            boolean r1 = r0 instanceof s.a.p0
            r2 = -1
            r3 = 0
            r4 = 1
            if (r1 == 0) goto L22
            r1 = r0
            s.a.p0 r1 = (s.a.p0) r1
            boolean r1 = r1.j
            if (r1 == 0) goto L13
            goto L39
        L13:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = s.a.h1.j
            s.a.p0 r5 = s.a.i1.g
            boolean r0 = r1.compareAndSet(r6, r0, r5)
            if (r0 != 0) goto L1e
            goto L3a
        L1e:
            r6.X()
            goto L37
        L22:
            boolean r1 = r0 instanceof s.a.y0
            if (r1 == 0) goto L39
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = s.a.h1.j
            r5 = r0
            s.a.y0 r5 = (s.a.y0) r5
            s.a.m1 r5 = r5.j
            boolean r0 = r1.compareAndSet(r6, r0, r5)
            if (r0 != 0) goto L34
            goto L3a
        L34:
            r6.X()
        L37:
            r2 = 1
            goto L3a
        L39:
            r2 = 0
        L3a:
            if (r2 == 0) goto L40
            if (r2 == r4) goto L3f
            goto L0
        L3f:
            return r4
        L40:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.h1.start():boolean");
    }

    public final boolean t(Object obj, m1 m1Var, g1<?> g1Var) {
        int p;
        c cVar = new c(g1Var, g1Var, this, obj);
        do {
            p = m1Var.k().p(g1Var, m1Var, cVar);
            if (p == 1) {
                return true;
            }
        } while (p != 2);
        return false;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(T() + '{' + Z(M()) + '}');
        sb.append(MentionUtilsKt.MENTIONS_CHAR);
        sb.append(f.l0(this));
        return sb.toString();
    }

    @Override // kotlinx.coroutines.Job
    public final m0 u(Function1<? super Throwable, Unit> function1) {
        return n(false, true, function1);
    }

    public void v(Object obj) {
    }

    /* JADX WARN: Removed duplicated region for block: B:80:0x00b9 A[SYNTHETIC] */
    /* JADX WARN: Removed duplicated region for block: B:86:0x003e A[SYNTHETIC] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean w(java.lang.Object r9) {
        /*
            Method dump skipped, instructions count: 248
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.h1.w(java.lang.Object):boolean");
    }

    public void x(Throwable th) {
        w(th);
    }

    public final boolean z(Throwable th) {
        if (Q()) {
            return true;
        }
        boolean z2 = th instanceof CancellationException;
        p pVar = (p) this._parentHandle;
        return (pVar == null || pVar == n1.j) ? z2 : pVar.h(th) || z2;
    }
}
