package s.a;

import b.i.a.f.e.o.f;
import d0.w.h.b;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import s.a.a.h;
import s.a.a.r;
/* compiled from: Builders.common.kt */
/* loaded from: classes3.dex */
public final class i0<T> extends r<T> {
    public static final AtomicIntegerFieldUpdater n = AtomicIntegerFieldUpdater.newUpdater(i0.class, "_decision");
    public volatile int _decision = 0;

    public i0(CoroutineContext coroutineContext, Continuation<? super T> continuation) {
        super(coroutineContext, continuation);
    }

    @Override // s.a.a.r, s.a.b
    public void e0(Object obj) {
        boolean z2;
        while (true) {
            int i = this._decision;
            z2 = false;
            if (i == 0) {
                if (n.compareAndSet(this, 0, 2)) {
                    z2 = true;
                    break;
                }
            } else if (i != 1) {
                throw new IllegalStateException("Already resumed".toString());
            }
        }
        if (!z2) {
            h.b(b.intercepted(this.m), f.X0(obj, this.m), null, 2);
        }
    }

    @Override // s.a.a.r, s.a.h1
    public void v(Object obj) {
        e0(obj);
    }
}
