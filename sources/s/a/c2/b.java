package s.a.c2;

import d0.w.f;
import d0.w.i.a.d;
import d0.w.i.a.e;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.channels.ProducerScope;
/* compiled from: Builders.kt */
/* loaded from: classes3.dex */
public final class b<T> extends c<T> {
    public final Function2<ProducerScope<? super T>, Continuation<? super Unit>, Object> e;

    /* compiled from: Builders.kt */
    @e(c = "kotlinx.coroutines.flow.CallbackFlowBuilder", f = "Builders.kt", l = {358}, m = "collectTo")
    /* loaded from: classes3.dex */
    public static final class a extends d {
        public Object L$0;
        public Object L$1;
        public int label;
        public /* synthetic */ Object result;

        public a(Continuation continuation) {
            super(continuation);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return b.this.b(null, this);
        }
    }

    public b(Function2 function2, CoroutineContext coroutineContext, int i, s.a.b2.e eVar, int i2) {
        super(function2, (i2 & 2) != 0 ? f.j : null, (i2 & 4) != 0 ? -2 : i, (i2 & 8) != 0 ? s.a.b2.e.SUSPEND : null);
        this.e = function2;
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0039  */
    /* JADX WARN: Removed duplicated region for block: B:21:0x005a  */
    /* JADX WARN: Removed duplicated region for block: B:23:0x005d  */
    @Override // s.a.c2.g.a
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.lang.Object b(kotlinx.coroutines.channels.ProducerScope<? super T> r5, kotlin.coroutines.Continuation<? super kotlin.Unit> r6) {
        /*
            r4 = this;
            boolean r0 = r6 instanceof s.a.c2.b.a
            if (r0 == 0) goto L13
            r0 = r6
            s.a.c2.b$a r0 = (s.a.c2.b.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            s.a.c2.b$a r0 = new s.a.c2.b$a
            r0.<init>(r6)
        L18:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L39
            if (r2 != r3) goto L31
            java.lang.Object r5 = r0.L$1
            kotlinx.coroutines.channels.ProducerScope r5 = (kotlinx.coroutines.channels.ProducerScope) r5
            java.lang.Object r0 = r0.L$0
            s.a.c2.b r0 = (s.a.c2.b) r0
            d0.l.throwOnFailure(r6)
            goto L54
        L31:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L39:
            d0.l.throwOnFailure(r6)
            r0.L$0 = r4
            r0.L$1 = r5
            r0.label = r3
            kotlin.jvm.functions.Function2<kotlinx.coroutines.channels.ProducerScope<? super T>, kotlin.coroutines.Continuation<? super kotlin.Unit>, java.lang.Object> r6 = r4.d
            java.lang.Object r6 = r6.invoke(r5, r0)
            java.lang.Object r0 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            if (r6 != r0) goto L4f
            goto L51
        L4f:
            kotlin.Unit r6 = kotlin.Unit.a
        L51:
            if (r6 != r1) goto L54
            return r1
        L54:
            boolean r5 = r5.p()
            if (r5 == 0) goto L5d
            kotlin.Unit r5 = kotlin.Unit.a
            return r5
        L5d:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "'awaitClose { yourCallbackOrListener.cancel() }' should be used in the end of callbackFlow block.\nOtherwise, a callback/listener may leak in case of external cancellation.\nSee callbackFlow API documentation for the details."
            r5.<init>(r6)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.c2.b.b(kotlinx.coroutines.channels.ProducerScope, kotlin.coroutines.Continuation):java.lang.Object");
    }
}
