package s.a.c2;

import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.channels.ProducerScope;
import s.a.b2.e;
import s.a.c2.g.a;
/* compiled from: Builders.kt */
/* loaded from: classes3.dex */
public class c<T> extends a<T> {
    public final Function2<ProducerScope<? super T>, Continuation<? super Unit>, Object> d;

    /* JADX WARN: Multi-variable type inference failed */
    public c(Function2<? super ProducerScope<? super T>, ? super Continuation<? super Unit>, ? extends Object> function2, CoroutineContext coroutineContext, int i, e eVar) {
        super(coroutineContext, i, eVar);
        this.d = function2;
    }

    @Override // s.a.c2.g.a
    public String toString() {
        StringBuilder R = b.d.b.a.a.R("block[");
        R.append(this.d);
        R.append("] -> ");
        R.append(super.toString());
        return R.toString();
    }
}
