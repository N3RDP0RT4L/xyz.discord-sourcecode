package s.a.c2;

import kotlin.Unit;
import kotlin.coroutines.Continuation;
/* compiled from: FlowCollector.kt */
/* loaded from: classes3.dex */
public interface e<T> {
    Object emit(T t, Continuation<? super Unit> continuation);
}
