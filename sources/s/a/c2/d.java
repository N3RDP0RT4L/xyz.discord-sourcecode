package s.a.c2;

import kotlin.Unit;
import kotlin.coroutines.Continuation;
/* compiled from: Flow.kt */
/* loaded from: classes3.dex */
public interface d<T> {
    Object a(e<? super T> eVar, Continuation<? super Unit> continuation);
}
