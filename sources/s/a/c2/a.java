package s.a.c2;

import d0.w.i.a.d;
import d0.w.i.a.e;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
/* compiled from: Flow.kt */
/* loaded from: classes3.dex */
public abstract class a<T> implements d<T> {

    /* compiled from: Flow.kt */
    @e(c = "kotlinx.coroutines.flow.AbstractFlow", f = "Flow.kt", l = {212}, m = "collect")
    /* renamed from: s.a.c2.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0426a extends d {
        public Object L$0;
        public Object L$1;
        public Object L$2;
        public int label;
        public /* synthetic */ Object result;

        public C0426a(Continuation continuation) {
            super(continuation);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return a.this.a(null, this);
        }
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x003f  */
    @Override // s.a.c2.d
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Object a(s.a.c2.e<? super T> r6, kotlin.coroutines.Continuation<? super kotlin.Unit> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof s.a.c2.a.C0426a
            if (r0 == 0) goto L13
            r0 = r7
            s.a.c2.a$a r0 = (s.a.c2.a.C0426a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            s.a.c2.a$a r0 = new s.a.c2.a$a
            r0.<init>(r7)
        L18:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L3f
            if (r2 != r3) goto L37
            java.lang.Object r6 = r0.L$2
            s.a.c2.g.e r6 = (s.a.c2.g.e) r6
            java.lang.Object r1 = r0.L$1
            s.a.c2.e r1 = (s.a.c2.e) r1
            java.lang.Object r0 = r0.L$0
            s.a.c2.a r0 = (s.a.c2.a) r0
            d0.l.throwOnFailure(r7)     // Catch: java.lang.Throwable -> L35
            goto L5b
        L35:
            r7 = move-exception
            goto L65
        L37:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L3f:
            d0.l.throwOnFailure(r7)
            s.a.c2.g.e r7 = new s.a.c2.g.e
            kotlin.coroutines.CoroutineContext r2 = r0.getContext()
            r7.<init>(r6, r2)
            r0.L$0 = r5     // Catch: java.lang.Throwable -> L61
            r0.L$1 = r6     // Catch: java.lang.Throwable -> L61
            r0.L$2 = r7     // Catch: java.lang.Throwable -> L61
            r0.label = r3     // Catch: java.lang.Throwable -> L61
            java.lang.Object r6 = r5.b(r7, r0)     // Catch: java.lang.Throwable -> L61
            if (r6 != r1) goto L5a
            return r1
        L5a:
            r6 = r7
        L5b:
            r6.releaseIntercepted()
            kotlin.Unit r6 = kotlin.Unit.a
            return r6
        L61:
            r6 = move-exception
            r4 = r7
            r7 = r6
            r6 = r4
        L65:
            r6.releaseIntercepted()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.c2.a.a(s.a.c2.e, kotlin.coroutines.Continuation):java.lang.Object");
    }

    public abstract Object b(e<? super T> eVar, Continuation<? super Unit> continuation);
}
