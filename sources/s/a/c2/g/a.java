package s.a.c2.g;

import b.i.a.f.e.o.f;
import d0.l;
import d0.t.u;
import d0.w.h.c;
import d0.w.i.a.k;
import java.util.ArrayList;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.channels.ProducerScope;
import org.objectweb.asm.Opcodes;
import s.a.b2.e;
import s.a.b2.m;
import s.a.c2.d;
import s.a.z;
/* compiled from: ChannelFlow.kt */
/* loaded from: classes3.dex */
public abstract class a<T> implements d {
    public final CoroutineContext a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3808b;
    public final e c;

    /* compiled from: ChannelFlow.kt */
    @d0.w.i.a.e(c = "kotlinx.coroutines.flow.internal.ChannelFlow$collect$2", f = "ChannelFlow.kt", l = {Opcodes.I2D}, m = "invokeSuspend")
    /* renamed from: s.a.c2.g.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0427a extends k implements Function2<CoroutineScope, Continuation<? super Unit>, Object> {
        public final /* synthetic */ s.a.c2.e $collector;
        public Object L$0;
        public int label;
        private CoroutineScope p$;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0427a(s.a.c2.e eVar, Continuation continuation) {
            super(2, continuation);
            this.$collector = eVar;
        }

        @Override // d0.w.i.a.a
        public final Continuation<Unit> create(Object obj, Continuation<?> continuation) {
            C0427a aVar = new C0427a(this.$collector, continuation);
            aVar.p$ = (CoroutineScope) obj;
            return aVar;
        }

        @Override // kotlin.jvm.functions.Function2
        public final Object invoke(CoroutineScope coroutineScope, Continuation<? super Unit> continuation) {
            C0427a aVar = new C0427a(this.$collector, continuation);
            aVar.p$ = coroutineScope;
            return aVar.invokeSuspend(Unit.a);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            Object coroutine_suspended = c.getCOROUTINE_SUSPENDED();
            int i = this.label;
            if (i == 0) {
                l.throwOnFailure(obj);
                CoroutineScope coroutineScope = this.p$;
                s.a.c2.e eVar = this.$collector;
                a aVar = a.this;
                CoroutineContext coroutineContext = aVar.a;
                int i2 = aVar.f3808b;
                if (i2 == -3) {
                    i2 = -2;
                }
                e eVar2 = aVar.c;
                CoroutineStart coroutineStart = CoroutineStart.ATOMIC;
                b bVar = new b(aVar, null);
                m mVar = new m(z.a(coroutineScope, coroutineContext), f.b(i2, eVar2, null, 4));
                mVar.j0(coroutineStart, mVar, bVar);
                this.L$0 = coroutineScope;
                this.label = 1;
                Object U = f.U(eVar, mVar, true, this);
                if (U != c.getCOROUTINE_SUSPENDED()) {
                    U = Unit.a;
                }
                if (U == coroutine_suspended) {
                    return coroutine_suspended;
                }
            } else if (i == 1) {
                CoroutineScope coroutineScope2 = (CoroutineScope) this.L$0;
                l.throwOnFailure(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Unit.a;
        }
    }

    public a(CoroutineContext coroutineContext, int i, e eVar) {
        this.a = coroutineContext;
        this.f3808b = i;
        this.c = eVar;
    }

    @Override // s.a.c2.d
    public Object a(s.a.c2.e<? super T> eVar, Continuation<? super Unit> continuation) {
        Object M = f.M(new C0427a(eVar, null), continuation);
        return M == c.getCOROUTINE_SUSPENDED() ? M : Unit.a;
    }

    public abstract Object b(ProducerScope<? super T> producerScope, Continuation<? super Unit> continuation);

    public String toString() {
        ArrayList arrayList = new ArrayList(4);
        if (this.a != d0.w.f.j) {
            StringBuilder R = b.d.b.a.a.R("context=");
            R.append(this.a);
            arrayList.add(R.toString());
        }
        if (this.f3808b != -3) {
            StringBuilder R2 = b.d.b.a.a.R("capacity=");
            R2.append(this.f3808b);
            arrayList.add(R2.toString());
        }
        if (this.c != e.SUSPEND) {
            StringBuilder R3 = b.d.b.a.a.R("onBufferOverflow=");
            R3.append(this.c);
            arrayList.add(R3.toString());
        }
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append('[');
        return b.d.b.a.a.G(sb, u.joinToString$default(arrayList, ", ", null, null, 0, null, null, 62, null), ']');
    }
}
