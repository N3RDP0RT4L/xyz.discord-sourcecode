package s.a.c2.g;

import d0.g0.m;
import d0.k;
import d0.w.f;
import d0.w.h.c;
import d0.w.i.a.d;
import d0.w.i.a.g;
import d0.z.d.o;
import java.util.Objects;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlinx.coroutines.Job;
/* compiled from: SafeCollector.kt */
/* loaded from: classes3.dex */
public final class e<T> extends d implements s.a.c2.e<T>, CoroutineStackFrame {
    public final CoroutineContext collectContext;
    public final int collectContextSize;
    public final s.a.c2.e<T> collector;
    private Continuation<? super Unit> completion;
    private CoroutineContext lastEmissionContext;

    /* compiled from: SafeCollector.kt */
    /* loaded from: classes3.dex */
    public static final class a extends o implements Function2<Integer, CoroutineContext.Element, Integer> {
        public static final a j = new a();

        public a() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        public Integer invoke(Integer num, CoroutineContext.Element element) {
            return Integer.valueOf(num.intValue() + 1);
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public e(s.a.c2.e<? super T> eVar, CoroutineContext coroutineContext) {
        super(d.j, f.j);
        this.collector = eVar;
        this.collectContext = coroutineContext;
        this.collectContextSize = ((Number) coroutineContext.fold(0, a.j)).intValue();
    }

    public final Object b(Continuation<? super Unit> continuation, T t) {
        CoroutineContext context = continuation.getContext();
        Job job = (Job) context.get(Job.h);
        if (job == null || job.a()) {
            CoroutineContext coroutineContext = this.lastEmissionContext;
            if (coroutineContext != context) {
                if (coroutineContext instanceof c) {
                    StringBuilder R = b.d.b.a.a.R("\n            Flow exception transparency is violated:\n                Previous 'emit' call has thrown exception ");
                    R.append(((c) coroutineContext).k);
                    R.append(", but then emission attempt of value '");
                    R.append(t);
                    R.append("' has been detected.\n                Emissions from 'catch' blocks are prohibited in order to avoid unspecified behaviour, 'Flow.catch' operator can be used instead.\n                For a more detailed explanation, please refer to Flow documentation.\n            ");
                    throw new IllegalStateException(m.trimIndent(R.toString()).toString());
                } else if (((Number) context.fold(0, new g(this))).intValue() == this.collectContextSize) {
                    this.lastEmissionContext = context;
                } else {
                    StringBuilder V = b.d.b.a.a.V("Flow invariant is violated:\n", "\t\tFlow was collected in ");
                    V.append(this.collectContext);
                    V.append(",\n");
                    V.append("\t\tbut emission happened in ");
                    V.append(context);
                    throw new IllegalStateException(b.d.b.a.a.H(V, ".\n", "\t\tPlease refer to 'flow' documentation or use 'flowOn' instead").toString());
                }
            }
            this.completion = continuation;
            Function3<s.a.c2.e<Object>, Object, Continuation<? super Unit>, Object> function3 = f.a;
            s.a.c2.e<T> eVar = this.collector;
            Objects.requireNonNull(eVar, "null cannot be cast to non-null type kotlinx.coroutines.flow.FlowCollector<kotlin.Any?>");
            return function3.invoke(eVar, t, this);
        }
        throw job.q();
    }

    @Override // s.a.c2.e
    public Object emit(T t, Continuation<? super Unit> continuation) {
        try {
            Object b2 = b(continuation, t);
            if (b2 == c.getCOROUTINE_SUSPENDED()) {
                g.probeCoroutineSuspended(continuation);
            }
            return b2 == c.getCOROUTINE_SUSPENDED() ? b2 : Unit.a;
        } catch (Throwable th) {
            this.lastEmissionContext = new c(th);
            throw th;
        }
    }

    @Override // d0.w.i.a.a
    public CoroutineStackFrame getCallerFrame() {
        Continuation<? super Unit> continuation = this.completion;
        if (!(continuation instanceof CoroutineStackFrame)) {
            continuation = null;
        }
        return (CoroutineStackFrame) continuation;
    }

    @Override // d0.w.i.a.d, kotlin.coroutines.Continuation
    public CoroutineContext getContext() {
        CoroutineContext context;
        Continuation<? super Unit> continuation = this.completion;
        return (continuation == null || (context = continuation.getContext()) == null) ? f.j : context;
    }

    @Override // d0.w.i.a.a
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @Override // d0.w.i.a.a
    public Object invokeSuspend(Object obj) {
        Throwable th = k.m75exceptionOrNullimpl(obj);
        if (th != null) {
            this.lastEmissionContext = new c(th);
        }
        Continuation<? super Unit> continuation = this.completion;
        if (continuation != null) {
            continuation.resumeWith(obj);
        }
        return c.getCOROUTINE_SUSPENDED();
    }

    @Override // d0.w.i.a.d, d0.w.i.a.a
    public void releaseIntercepted() {
        super.releaseIntercepted();
    }
}
