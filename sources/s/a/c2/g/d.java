package s.a.c2.g;

import d0.w.f;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
/* compiled from: SafeCollector.kt */
/* loaded from: classes3.dex */
public final class d implements Continuation<Object> {
    public static final d j = new d();

    @Override // kotlin.coroutines.Continuation
    public CoroutineContext getContext() {
        return f.j;
    }

    @Override // kotlin.coroutines.Continuation
    public void resumeWith(Object obj) {
    }
}
