package s.a.c2;

import d0.w.h.c;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
/* compiled from: Builders.kt */
/* loaded from: classes3.dex */
public final class f<T> extends a<T> {
    public final Function2<e<? super T>, Continuation<? super Unit>, Object> a;

    /* JADX WARN: Multi-variable type inference failed */
    public f(Function2<? super e<? super T>, ? super Continuation<? super Unit>, ? extends Object> function2) {
        this.a = function2;
    }

    @Override // s.a.c2.a
    public Object b(e<? super T> eVar, Continuation<? super Unit> continuation) {
        Object invoke = this.a.invoke(eVar, continuation);
        return invoke == c.getCOROUTINE_SUSPENDED() ? invoke : Unit.a;
    }
}
