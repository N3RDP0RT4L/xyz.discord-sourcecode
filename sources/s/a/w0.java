package s.a;

import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import s.a.a.e;
/* compiled from: Executors.kt */
/* loaded from: classes3.dex */
public final class w0 extends v0 {
    public final Executor k;

    public w0(Executor executor) {
        Method method;
        this.k = executor;
        Method method2 = e.a;
        boolean z2 = false;
        try {
            ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = (ScheduledThreadPoolExecutor) (!(executor instanceof ScheduledThreadPoolExecutor) ? null : executor);
            if (!(scheduledThreadPoolExecutor == null || (method = e.a) == null)) {
                method.invoke(scheduledThreadPoolExecutor, Boolean.TRUE);
                z2 = true;
            }
        } catch (Throwable unused) {
        }
        this.j = z2;
    }

    @Override // kotlinx.coroutines.ExecutorCoroutineDispatcher
    public Executor H() {
        return this.k;
    }
}
