package s.a;

import b.i.a.f.e.o.f;
import d0.k;
import d0.l;
import d0.w.e;
import d0.w.h.c;
import d0.w.i.a.g;
import d0.z.d.e0;
import kotlin.NoWhenBranchMatchedException;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.CoroutineStart;
import kotlinx.coroutines.Job;
import s.a.a.a;
/* compiled from: AbstractCoroutine.kt */
/* loaded from: classes3.dex */
public abstract class b<T> extends h1 implements Job, Continuation<T>, CoroutineScope {
    public final CoroutineContext k;
    public final CoroutineContext l;

    public b(CoroutineContext coroutineContext, boolean z2) {
        super(z2);
        this.l = coroutineContext;
        this.k = coroutineContext.plus(this);
    }

    @Override // s.a.h1
    public String B() {
        return getClass().getSimpleName() + " was cancelled";
    }

    @Override // s.a.h1
    public final void O(Throwable th) {
        f.u0(this.k, th);
    }

    @Override // s.a.h1
    public String T() {
        boolean z2 = z.a;
        return super.T();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v2, types: [boolean, int] */
    @Override // s.a.h1
    public final void W(Object obj) {
        if (obj instanceof w) {
            w wVar = (w) obj;
            g0(wVar.f3819b, wVar._handled);
            return;
        }
        h0(obj);
    }

    @Override // s.a.h1
    public final void X() {
        i0();
    }

    @Override // s.a.h1, kotlinx.coroutines.Job
    public boolean a() {
        return super.a();
    }

    public void e0(Object obj) {
        v(obj);
    }

    public final void f0() {
        P((Job) this.l.get(Job.h));
    }

    public void g0(Throwable th, boolean z2) {
    }

    @Override // kotlin.coroutines.Continuation
    public final CoroutineContext getContext() {
        return this.k;
    }

    @Override // kotlinx.coroutines.CoroutineScope
    public CoroutineContext getCoroutineContext() {
        return this.k;
    }

    public void h0(T t) {
    }

    public void i0() {
    }

    public final <R> void j0(CoroutineStart coroutineStart, R r, Function2<? super R, ? super Continuation<? super T>, ? extends Object> function2) {
        f0();
        int ordinal = coroutineStart.ordinal();
        if (ordinal == 0) {
            f.f1(function2, r, this, null, 4);
        } else if (ordinal == 1) {
        } else {
            if (ordinal == 2) {
                e.startCoroutine(function2, r, this);
            } else if (ordinal == 3) {
                Continuation probeCoroutineCreated = g.probeCoroutineCreated(this);
                try {
                    CoroutineContext coroutineContext = this.k;
                    Object b2 = a.b(coroutineContext, null);
                    if (function2 != null) {
                        Object invoke = ((Function2) e0.beforeCheckcastToFunctionOfArity(function2, 2)).invoke(r, probeCoroutineCreated);
                        a.a(coroutineContext, b2);
                        if (invoke != c.getCOROUTINE_SUSPENDED()) {
                            k.a aVar = k.j;
                            probeCoroutineCreated.resumeWith(k.m73constructorimpl(invoke));
                            return;
                        }
                        return;
                    }
                    throw new NullPointerException("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
                } catch (Throwable th) {
                    k.a aVar2 = k.j;
                    probeCoroutineCreated.resumeWith(k.m73constructorimpl(l.createFailure(th)));
                }
            } else {
                throw new NoWhenBranchMatchedException();
            }
        }
    }

    @Override // kotlin.coroutines.Continuation
    public final void resumeWith(Object obj) {
        Object R = R(f.v1(obj, null));
        if (R != i1.f3813b) {
            e0(R);
        }
    }
}
