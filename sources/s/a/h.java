package s.a;

import b.i.a.f.e.o.f;
import d0.w.h.c;
import d0.w.i.a.d;
import d0.w.i.a.e;
import d0.z.d.e0;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function2;
import kotlinx.coroutines.CoroutineScope;
import kotlinx.coroutines.TimeoutCancellationException;
/* compiled from: Builders.kt */
/* loaded from: classes3.dex */
public final /* synthetic */ class h {

    /* compiled from: Timeout.kt */
    @e(c = "kotlinx.coroutines.TimeoutKt", f = "Timeout.kt", l = {101}, m = "withTimeoutOrNull")
    /* loaded from: classes3.dex */
    public static final class a extends d {
        public long J$0;
        public Object L$0;
        public Object L$1;
        public int label;
        public /* synthetic */ Object result;

        public a(Continuation continuation) {
            super(continuation);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return h.b(0L, null, this);
        }
    }

    public static final <U, T extends U> Object a(w1<U, ? super T> w1Var, Function2<? super CoroutineScope, ? super Continuation<? super T>, ? extends Object> function2) {
        Object obj;
        Object R;
        boolean z2 = false;
        w1Var.n(false, true, new o0(w1Var, f.i0(w1Var.m.getContext()).x(w1Var.n, w1Var, w1Var.k)));
        w1Var.f0();
        try {
        } catch (Throwable th) {
            obj = new w(th, false, 2);
        }
        if (function2 != null) {
            obj = ((Function2) e0.beforeCheckcastToFunctionOfArity(function2, 2)).invoke(w1Var, w1Var);
            if (!(obj == c.getCOROUTINE_SUSPENDED() || (R = w1Var.R(obj)) == i1.f3813b)) {
                if (!(R instanceof w)) {
                    return i1.a(R);
                }
                Throwable th2 = ((w) R).f3819b;
                if (!(th2 instanceof TimeoutCancellationException) || ((TimeoutCancellationException) th2).coroutine != w1Var) {
                    z2 = true;
                }
                if (z2) {
                    throw th2;
                } else if (!(obj instanceof w)) {
                    return obj;
                } else {
                    throw ((w) obj).f3819b;
                }
            }
            return c.getCOROUTINE_SUSPENDED();
        }
        throw new NullPointerException("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
    }

    /* JADX WARN: Removed duplicated region for block: B:10:0x0024  */
    /* JADX WARN: Removed duplicated region for block: B:16:0x003c  */
    /* JADX WARN: Removed duplicated region for block: B:31:0x0079 A[RETURN] */
    /* JADX WARN: Removed duplicated region for block: B:32:0x007a  */
    /* JADX WARN: Type inference failed for: r2v1, types: [s.a.w1, T] */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final <T> java.lang.Object b(long r7, kotlin.jvm.functions.Function2<? super kotlinx.coroutines.CoroutineScope, ? super kotlin.coroutines.Continuation<? super T>, ? extends java.lang.Object> r9, kotlin.coroutines.Continuation<? super T> r10) {
        /*
            boolean r0 = r10 instanceof s.a.h.a
            if (r0 == 0) goto L13
            r0 = r10
            s.a.h$a r0 = (s.a.h.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            s.a.h$a r0 = new s.a.h$a
            r0.<init>(r10)
        L18:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L3c
            if (r2 != r3) goto L34
            java.lang.Object r7 = r0.L$1
            kotlin.jvm.internal.Ref$ObjectRef r7 = (kotlin.jvm.internal.Ref$ObjectRef) r7
            java.lang.Object r8 = r0.L$0
            kotlin.jvm.functions.Function2 r8 = (kotlin.jvm.functions.Function2) r8
            d0.l.throwOnFailure(r10)     // Catch: kotlinx.coroutines.TimeoutCancellationException -> L32
            goto L6d
        L32:
            r8 = move-exception
            goto L71
        L34:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L3c:
            d0.l.throwOnFailure(r10)
            r5 = 0
            int r10 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r10 > 0) goto L46
            return r4
        L46:
            kotlin.jvm.internal.Ref$ObjectRef r10 = new kotlin.jvm.internal.Ref$ObjectRef
            r10.<init>()
            r10.element = r4
            r0.J$0 = r7     // Catch: kotlinx.coroutines.TimeoutCancellationException -> L6e
            r0.L$0 = r9     // Catch: kotlinx.coroutines.TimeoutCancellationException -> L6e
            r0.L$1 = r10     // Catch: kotlinx.coroutines.TimeoutCancellationException -> L6e
            r0.label = r3     // Catch: kotlinx.coroutines.TimeoutCancellationException -> L6e
            s.a.w1 r2 = new s.a.w1     // Catch: kotlinx.coroutines.TimeoutCancellationException -> L6e
            r2.<init>(r7, r0)     // Catch: kotlinx.coroutines.TimeoutCancellationException -> L6e
            r10.element = r2     // Catch: kotlinx.coroutines.TimeoutCancellationException -> L6e
            java.lang.Object r7 = a(r2, r9)     // Catch: kotlinx.coroutines.TimeoutCancellationException -> L6e
            java.lang.Object r8 = d0.w.h.c.getCOROUTINE_SUSPENDED()     // Catch: kotlinx.coroutines.TimeoutCancellationException -> L6e
            if (r7 != r8) goto L69
            d0.w.i.a.g.probeCoroutineSuspended(r0)     // Catch: kotlinx.coroutines.TimeoutCancellationException -> L6e
        L69:
            if (r7 != r1) goto L6c
            return r1
        L6c:
            r10 = r7
        L6d:
            return r10
        L6e:
            r7 = move-exception
            r8 = r7
            r7 = r10
        L71:
            kotlinx.coroutines.Job r9 = r8.coroutine
            T r7 = r7.element
            s.a.w1 r7 = (s.a.w1) r7
            if (r9 != r7) goto L7a
            return r4
        L7a:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.h.b(long, kotlin.jvm.functions.Function2, kotlin.coroutines.Continuation):java.lang.Object");
    }
}
