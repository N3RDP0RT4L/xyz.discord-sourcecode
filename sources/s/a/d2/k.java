package s.a.d2;

import b.i.a.f.e.o.f;
import java.util.concurrent.TimeUnit;
import s.a.a.u;
/* compiled from: Tasks.kt */
/* loaded from: classes3.dex */
public final class k {

    /* renamed from: b  reason: collision with root package name */
    public static final int f3811b;
    public static final int c;
    public static final long a = f.m1("kotlinx.coroutines.scheduler.resolution.ns", 100000, 0, 0, 12, null);
    public static final long d = TimeUnit.SECONDS.toNanos(f.m1("kotlinx.coroutines.scheduler.keep.alive.sec", 60, 0, 0, 12, null));
    public static l e = f.a;

    static {
        f.l1("kotlinx.coroutines.scheduler.blocking.parallelism", 16, 0, 0, 12, null);
        int i = u.a;
        int l1 = f.l1("kotlinx.coroutines.scheduler.core.pool.size", d0.d0.f.coerceAtLeast(i, 2), 1, 0, 8, null);
        f3811b = l1;
        c = f.l1("kotlinx.coroutines.scheduler.max.pool.size", d0.d0.f.coerceIn(i * 128, l1, 2097150), 0, 2097150, 4, null);
    }
}
