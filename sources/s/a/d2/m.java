package s.a.d2;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import org.objectweb.asm.Opcodes;
/* compiled from: WorkQueue.kt */
/* loaded from: classes3.dex */
public final class m {
    public static final AtomicReferenceFieldUpdater a = AtomicReferenceFieldUpdater.newUpdater(m.class, Object.class, "lastScheduledTask");

    /* renamed from: b  reason: collision with root package name */
    public static final AtomicIntegerFieldUpdater f3812b = AtomicIntegerFieldUpdater.newUpdater(m.class, "producerIndex");
    public static final AtomicIntegerFieldUpdater c = AtomicIntegerFieldUpdater.newUpdater(m.class, "consumerIndex");
    public static final AtomicIntegerFieldUpdater d = AtomicIntegerFieldUpdater.newUpdater(m.class, "blockingTasksInBuffer");
    public final AtomicReferenceArray<h> e = new AtomicReferenceArray<>(128);
    public volatile Object lastScheduledTask = null;
    public volatile int producerIndex = 0;
    public volatile int consumerIndex = 0;
    public volatile int blockingTasksInBuffer = 0;

    public final h a(h hVar, boolean z2) {
        if (z2) {
            return b(hVar);
        }
        h hVar2 = (h) a.getAndSet(this, hVar);
        if (hVar2 != null) {
            return b(hVar2);
        }
        return null;
    }

    public final h b(h hVar) {
        boolean z2 = true;
        if (hVar.k.t() != 1) {
            z2 = false;
        }
        if (z2) {
            d.incrementAndGet(this);
        }
        if (c() == 127) {
            return hVar;
        }
        int i = this.producerIndex & Opcodes.LAND;
        while (this.e.get(i) != null) {
            Thread.yield();
        }
        this.e.lazySet(i, hVar);
        f3812b.incrementAndGet(this);
        return null;
    }

    public final int c() {
        return this.producerIndex - this.consumerIndex;
    }

    public final int d() {
        return this.lastScheduledTask != null ? c() + 1 : c();
    }

    public final h e() {
        h hVar = (h) a.getAndSet(this, null);
        return hVar != null ? hVar : f();
    }

    public final h f() {
        h andSet;
        while (true) {
            int i = this.consumerIndex;
            if (i - this.producerIndex == 0) {
                return null;
            }
            int i2 = i & Opcodes.LAND;
            if (c.compareAndSet(this, i, i + 1) && (andSet = this.e.getAndSet(i2, null)) != null) {
                boolean z2 = true;
                if (andSet.k.t() != 1) {
                    z2 = false;
                }
                if (z2) {
                    d.decrementAndGet(this);
                }
                return andSet;
            }
        }
    }

    public final long g(m mVar, boolean z2) {
        h hVar;
        do {
            hVar = (h) mVar.lastScheduledTask;
            if (hVar == null) {
                return -2L;
            }
            if (z2) {
                boolean z3 = true;
                if (hVar.k.t() != 1) {
                    z3 = false;
                }
                if (!z3) {
                    return -2L;
                }
            }
            long a2 = k.e.a() - hVar.j;
            long j = k.a;
            if (a2 < j) {
                return j - a2;
            }
        } while (!a.compareAndSet(mVar, hVar, null));
        a(hVar, false);
        return -1L;
    }
}
