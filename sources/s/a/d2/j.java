package s.a.d2;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.widgets.chat.input.MentionUtilsKt;
/* compiled from: Tasks.kt */
/* loaded from: classes3.dex */
public final class j extends h {
    public final Runnable l;

    public j(Runnable runnable, long j, i iVar) {
        super(j, iVar);
        this.l = runnable;
    }

    @Override // java.lang.Runnable
    public void run() {
        try {
            this.l.run();
        } finally {
            this.k.f();
        }
    }

    public String toString() {
        StringBuilder R = a.R("Task[");
        R.append(f.e0(this.l));
        R.append(MentionUtilsKt.MENTIONS_CHAR);
        R.append(f.l0(this.l));
        R.append(", ");
        R.append(this.j);
        R.append(", ");
        R.append(this.k);
        R.append(']');
        return R.toString();
    }
}
