package s.a.d2;

import androidx.recyclerview.widget.RecyclerView;
import b.c.a.y.b;
import com.discord.api.permission.Permission;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.c0.c;
import d0.d0.f;
import d0.z.d.m;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.LockSupport;
import org.objectweb.asm.Opcodes;
import s.a.a.t;
/* compiled from: CoroutineScheduler.kt */
/* loaded from: classes3.dex */
public final class a implements Executor, Closeable {
    public volatile int _isTerminated;
    public volatile long controlState;
    public final d n;
    public final d o;
    public final AtomicReferenceArray<C0428a> p;
    public volatile long parkedWorkersStack;
    public final int q;
    public final int r;

    /* renamed from: s */
    public final long f3810s;
    public final String t;
    public static final t m = new t("NOT_IN_STACK");
    public static final AtomicLongFieldUpdater j = AtomicLongFieldUpdater.newUpdater(a.class, "parkedWorkersStack");
    public static final AtomicLongFieldUpdater k = AtomicLongFieldUpdater.newUpdater(a.class, "controlState");
    public static final AtomicIntegerFieldUpdater l = AtomicIntegerFieldUpdater.newUpdater(a.class, "_isTerminated");

    /* compiled from: CoroutineScheduler.kt */
    /* renamed from: s.a.d2.a$a */
    /* loaded from: classes3.dex */
    public final class C0428a extends Thread {
        public static final AtomicIntegerFieldUpdater j = AtomicIntegerFieldUpdater.newUpdater(C0428a.class, "workerCtl");
        public volatile int indexInArray;
        public long m;
        public long n;
        public boolean p;
        public final m k = new m();
        public int l = 4;
        public volatile int workerCtl = 0;
        public volatile Object nextParkedWorker = a.m;
        public int o = c.k.nextInt();

        public C0428a(int i) {
            a.this = r1;
            setDaemon(true);
            d(i);
        }

        /* JADX WARN: Removed duplicated region for block: B:16:0x0033  */
        /* JADX WARN: Removed duplicated region for block: B:32:0x006a  */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final s.a.d2.h a(boolean r10) {
            /*
                r9 = this;
                int r0 = r9.l
                r1 = 0
                r2 = 1
                if (r0 != r2) goto L7
                goto L2e
            L7:
                s.a.d2.a r0 = s.a.d2.a.this
            L9:
                long r5 = r0.controlState
                r3 = 9223367638808264704(0x7ffffc0000000000, double:NaN)
                long r3 = r3 & r5
                r7 = 42
                long r3 = r3 >> r7
                int r4 = (int) r3
                if (r4 != 0) goto L19
                r0 = 0
                goto L2a
            L19:
                r3 = 4398046511104(0x40000000000, double:2.1729236899484E-311)
                long r7 = r5 - r3
                java.util.concurrent.atomic.AtomicLongFieldUpdater r3 = s.a.d2.a.k
                r4 = r0
                boolean r3 = r3.compareAndSet(r4, r5, r7)
                if (r3 == 0) goto L9
                r0 = 1
            L2a:
                if (r0 == 0) goto L30
                r9.l = r2
            L2e:
                r0 = 1
                goto L31
            L30:
                r0 = 0
            L31:
                if (r0 == 0) goto L6a
                if (r10 == 0) goto L5e
                s.a.d2.a r10 = s.a.d2.a.this
                int r10 = r10.q
                int r10 = r10 * 2
                int r10 = r9.b(r10)
                if (r10 != 0) goto L42
                goto L43
            L42:
                r2 = 0
            L43:
                if (r2 == 0) goto L4c
                s.a.d2.h r10 = r9.c()
                if (r10 == 0) goto L4c
                goto L69
            L4c:
                s.a.d2.m r10 = r9.k
                s.a.d2.h r10 = r10.e()
                if (r10 == 0) goto L55
                goto L69
            L55:
                if (r2 != 0) goto L65
                s.a.d2.h r10 = r9.c()
                if (r10 == 0) goto L65
                goto L69
            L5e:
                s.a.d2.h r10 = r9.c()
                if (r10 == 0) goto L65
                goto L69
            L65:
                s.a.d2.h r10 = r9.f(r1)
            L69:
                return r10
            L6a:
                if (r10 == 0) goto L80
                s.a.d2.m r10 = r9.k
                s.a.d2.h r10 = r10.e()
                if (r10 == 0) goto L75
                goto L8a
            L75:
                s.a.d2.a r10 = s.a.d2.a.this
                s.a.d2.d r10 = r10.o
                java.lang.Object r10 = r10.d()
                s.a.d2.h r10 = (s.a.d2.h) r10
                goto L8a
            L80:
                s.a.d2.a r10 = s.a.d2.a.this
                s.a.d2.d r10 = r10.o
                java.lang.Object r10 = r10.d()
                s.a.d2.h r10 = (s.a.d2.h) r10
            L8a:
                if (r10 == 0) goto L8d
                goto L91
            L8d:
                s.a.d2.h r10 = r9.f(r2)
            L91:
                return r10
            */
            throw new UnsupportedOperationException("Method not decompiled: s.a.d2.a.C0428a.a(boolean):s.a.d2.h");
        }

        public final int b(int i) {
            int i2 = this.o;
            int i3 = i2 ^ (i2 << 13);
            int i4 = i3 ^ (i3 >> 17);
            int i5 = i4 ^ (i4 << 5);
            this.o = i5;
            int i6 = i - 1;
            return (i6 & i) == 0 ? i5 & i6 : (i5 & Integer.MAX_VALUE) % i;
        }

        public final h c() {
            if (b(2) == 0) {
                h d = a.this.n.d();
                return d != null ? d : a.this.o.d();
            }
            h d2 = a.this.o.d();
            return d2 != null ? d2 : a.this.n.d();
        }

        public final void d(int i) {
            StringBuilder sb = new StringBuilder();
            sb.append(a.this.t);
            sb.append("-worker-");
            sb.append(i == 0 ? "TERMINATED" : String.valueOf(i));
            setName(sb.toString());
            this.indexInArray = i;
        }

        public final boolean e(int i) {
            int i2 = this.l;
            boolean z2 = true;
            if (i2 != 1) {
                z2 = false;
            }
            if (z2) {
                a.k.addAndGet(a.this, 4398046511104L);
            }
            if (i2 != i) {
                this.l = i;
            }
            return z2;
        }

        public final h f(boolean z2) {
            long j2;
            int i = (int) (a.this.controlState & 2097151);
            if (i < 2) {
                return null;
            }
            int b2 = b(i);
            long j3 = RecyclerView.FOREVER_NS;
            for (int i2 = 0; i2 < i; i2++) {
                b2++;
                if (b2 > i) {
                    b2 = 1;
                }
                C0428a aVar = a.this.p.get(b2);
                if (aVar != null && aVar != this) {
                    if (z2) {
                        m mVar = this.k;
                        m mVar2 = aVar.k;
                        Objects.requireNonNull(mVar);
                        int i3 = mVar2.producerIndex;
                        AtomicReferenceArray<h> atomicReferenceArray = mVar2.e;
                        for (int i4 = mVar2.consumerIndex; i4 != i3; i4++) {
                            int i5 = i4 & Opcodes.LAND;
                            if (mVar2.blockingTasksInBuffer == 0) {
                                break;
                            }
                            h hVar = atomicReferenceArray.get(i5);
                            if (hVar != null) {
                                if ((hVar.k.t() == 1) && atomicReferenceArray.compareAndSet(i5, hVar, null)) {
                                    m.d.decrementAndGet(mVar2);
                                    mVar.a(hVar, false);
                                    j2 = -1;
                                    break;
                                }
                            }
                        }
                        j2 = mVar.g(mVar2, true);
                    } else {
                        m mVar3 = this.k;
                        m mVar4 = aVar.k;
                        Objects.requireNonNull(mVar3);
                        h f = mVar4.f();
                        if (f != null) {
                            mVar3.a(f, false);
                            j2 = -1;
                        } else {
                            j2 = mVar3.g(mVar4, false);
                        }
                    }
                    if (j2 == -1) {
                        return this.k.e();
                    }
                    if (j2 > 0) {
                        j3 = Math.min(j3, j2);
                    }
                }
            }
            j3 = 0;
            if (j3 == RecyclerView.FOREVER_NS) {
            }
            this.n = j3;
            return null;
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            long j2;
            int i;
            loop0: while (true) {
                boolean z2 = false;
                while (a.this._isTerminated == 0 && this.l != 5) {
                    h a = a(this.p);
                    if (a != null) {
                        this.n = 0L;
                        int t = a.k.t();
                        this.m = 0L;
                        if (this.l == 3) {
                            this.l = 2;
                        }
                        if (t != 0 && e(2)) {
                            a.this.s();
                        }
                        a.this.q(a);
                        if (t != 0) {
                            a.k.addAndGet(a.this, -2097152L);
                            if (this.l != 5) {
                                this.l = 4;
                            }
                        }
                    } else {
                        this.p = false;
                        if (this.n == 0) {
                            Object obj = this.nextParkedWorker;
                            t tVar = a.m;
                            if (!(obj != tVar)) {
                                a aVar = a.this;
                                Objects.requireNonNull(aVar);
                                if (this.nextParkedWorker == tVar) {
                                    do {
                                        j2 = aVar.parkedWorkersStack;
                                        i = this.indexInArray;
                                        this.nextParkedWorker = aVar.p.get((int) (j2 & 2097151));
                                    } while (!a.j.compareAndSet(aVar, j2, i | ((Permission.SPEAK + j2) & (-2097152))));
                                }
                            } else {
                                this.workerCtl = -1;
                                while (true) {
                                    if ((this.nextParkedWorker != a.m) && a.this._isTerminated == 0 && this.l != 5) {
                                        e(3);
                                        Thread.interrupted();
                                        if (this.m == 0) {
                                            this.m = System.nanoTime() + a.this.f3810s;
                                        }
                                        LockSupport.parkNanos(a.this.f3810s);
                                        if (System.nanoTime() - this.m >= 0) {
                                            this.m = 0L;
                                            synchronized (a.this.p) {
                                                if (a.this._isTerminated == 0) {
                                                    if (((int) (a.this.controlState & 2097151)) > a.this.q) {
                                                        if (j.compareAndSet(this, -1, 1)) {
                                                            int i2 = this.indexInArray;
                                                            d(0);
                                                            a.this.n(this, i2, 0);
                                                            int andDecrement = (int) (a.k.getAndDecrement(a.this) & 2097151);
                                                            if (andDecrement != i2) {
                                                                C0428a aVar2 = a.this.p.get(andDecrement);
                                                                m.checkNotNull(aVar2);
                                                                C0428a aVar3 = aVar2;
                                                                a.this.p.set(i2, aVar3);
                                                                aVar3.d(i2);
                                                                a.this.n(aVar3, andDecrement, i2);
                                                            }
                                                            a.this.p.set(andDecrement, null);
                                                            this.l = 5;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else if (!z2) {
                            z2 = true;
                        } else {
                            e(3);
                            Thread.interrupted();
                            LockSupport.parkNanos(this.n);
                            this.n = 0L;
                        }
                    }
                }
            }
            e(5);
        }
    }

    public a(int i, int i2, long j2, String str) {
        this.q = i;
        this.r = i2;
        this.f3810s = j2;
        this.t = str;
        if (i >= 1) {
            if (i2 >= i) {
                if (i2 <= 2097150) {
                    if (j2 > 0) {
                        this.n = new d();
                        this.o = new d();
                        this.parkedWorkersStack = 0L;
                        this.p = new AtomicReferenceArray<>(i2 + 1);
                        this.controlState = i << 42;
                        this._isTerminated = 0;
                        return;
                    }
                    throw new IllegalArgumentException(("Idle worker keep alive time " + j2 + " must be positive").toString());
                }
                throw new IllegalArgumentException(b.d.b.a.a.q("Max pool size ", i2, " should not exceed maximal supported number of threads 2097150").toString());
            }
            throw new IllegalArgumentException(b.d.b.a.a.r("Max pool size ", i2, " should be greater than or equals to core pool size ", i).toString());
        }
        throw new IllegalArgumentException(b.d.b.a.a.q("Core pool size ", i, " should be at least 1").toString());
    }

    public static /* synthetic */ void e(a aVar, Runnable runnable, i iVar, boolean z2, int i) {
        g gVar = (i & 2) != 0 ? g.j : null;
        if ((i & 4) != 0) {
            z2 = false;
        }
        aVar.d(runnable, gVar, z2);
    }

    public final int a() {
        synchronized (this.p) {
            if (this._isTerminated != 0) {
                return -1;
            }
            long j2 = this.controlState;
            int i = (int) (j2 & 2097151);
            boolean z2 = false;
            int coerceAtLeast = f.coerceAtLeast(i - ((int) ((j2 & 4398044413952L) >> 21)), 0);
            if (coerceAtLeast >= this.q) {
                return 0;
            }
            if (i >= this.r) {
                return 0;
            }
            int i2 = ((int) (this.controlState & 2097151)) + 1;
            if (i2 > 0 && this.p.get(i2) == null) {
                C0428a aVar = new C0428a(i2);
                this.p.set(i2, aVar);
                if (i2 == ((int) (2097151 & k.incrementAndGet(this)))) {
                    z2 = true;
                }
                if (z2) {
                    aVar.start();
                    return coerceAtLeast + 1;
                }
                throw new IllegalArgumentException("Failed requirement.".toString());
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }

    public final h b(Runnable runnable, i iVar) {
        long a = k.e.a();
        if (!(runnable instanceof h)) {
            return new j(runnable, a, iVar);
        }
        h hVar = (h) runnable;
        hVar.j = a;
        hVar.k = iVar;
        return hVar;
    }

    public final C0428a c() {
        Thread currentThread = Thread.currentThread();
        if (!(currentThread instanceof C0428a)) {
            currentThread = null;
        }
        C0428a aVar = (C0428a) currentThread;
        if (aVar == null || !m.areEqual(a.this, this)) {
            return null;
        }
        return aVar;
    }

    /* JADX WARN: Code restructure failed: missing block: B:29:0x0073, code lost:
        if (r1 != null) goto L31;
     */
    /* JADX WARN: Removed duplicated region for block: B:25:0x0060 A[LOOP:0: B:11:0x001e->B:25:0x0060, LOOP_END] */
    /* JADX WARN: Removed duplicated region for block: B:43:0x0063 A[EDGE_INSN: B:43:0x0063->B:26:0x0063 ?: BREAK  , SYNTHETIC] */
    @Override // java.io.Closeable, java.lang.AutoCloseable
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public void close() {
        /*
            r9 = this;
            java.util.concurrent.atomic.AtomicIntegerFieldUpdater r0 = s.a.d2.a.l
            r1 = 0
            r2 = 1
            boolean r0 = r0.compareAndSet(r9, r1, r2)
            if (r0 != 0) goto Lc
            goto L9b
        Lc:
            s.a.d2.a$a r0 = r9.c()
            java.util.concurrent.atomic.AtomicReferenceArray<s.a.d2.a$a> r3 = r9.p
            monitor-enter(r3)
            long r4 = r9.controlState     // Catch: java.lang.Throwable -> L9c
            r6 = 2097151(0x1fffff, double:1.0361303E-317)
            long r4 = r4 & r6
            int r5 = (int) r4
            monitor-exit(r3)
            if (r2 > r5) goto L63
            r3 = 1
        L1e:
            java.util.concurrent.atomic.AtomicReferenceArray<s.a.d2.a$a> r4 = r9.p
            java.lang.Object r4 = r4.get(r3)
            d0.z.d.m.checkNotNull(r4)
            s.a.d2.a$a r4 = (s.a.d2.a.C0428a) r4
            if (r4 == r0) goto L5e
        L2b:
            boolean r6 = r4.isAlive()
            if (r6 == 0) goto L3a
            java.util.concurrent.locks.LockSupport.unpark(r4)
            r6 = 10000(0x2710, double:4.9407E-320)
            r4.join(r6)
            goto L2b
        L3a:
            s.a.d2.m r4 = r4.k
            s.a.d2.d r6 = r9.o
            java.util.Objects.requireNonNull(r4)
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r7 = s.a.d2.m.a
            r8 = 0
            java.lang.Object r7 = r7.getAndSet(r4, r8)
            s.a.d2.h r7 = (s.a.d2.h) r7
            if (r7 == 0) goto L4f
            r6.a(r7)
        L4f:
            s.a.d2.h r7 = r4.f()
            if (r7 == 0) goto L5a
            r6.a(r7)
            r7 = 1
            goto L5b
        L5a:
            r7 = 0
        L5b:
            if (r7 == 0) goto L5e
            goto L4f
        L5e:
            if (r3 == r5) goto L63
            int r3 = r3 + 1
            goto L1e
        L63:
            s.a.d2.d r1 = r9.o
            r1.b()
            s.a.d2.d r1 = r9.n
            r1.b()
        L6d:
            if (r0 == 0) goto L76
            s.a.d2.h r1 = r0.a(r2)
            if (r1 == 0) goto L76
            goto L7e
        L76:
            s.a.d2.d r1 = r9.n
            java.lang.Object r1 = r1.d()
            s.a.d2.h r1 = (s.a.d2.h) r1
        L7e:
            if (r1 == 0) goto L81
            goto L89
        L81:
            s.a.d2.d r1 = r9.o
            java.lang.Object r1 = r1.d()
            s.a.d2.h r1 = (s.a.d2.h) r1
        L89:
            if (r1 == 0) goto L8f
            r9.q(r1)
            goto L6d
        L8f:
            if (r0 == 0) goto L95
            r1 = 5
            r0.e(r1)
        L95:
            r0 = 0
            r9.parkedWorkersStack = r0
            r9.controlState = r0
        L9b:
            return
        L9c:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.d2.a.close():void");
    }

    public final void d(Runnable runnable, i iVar, boolean z2) {
        h hVar;
        boolean z3;
        h b2 = b(runnable, iVar);
        C0428a c = c();
        boolean z4 = true;
        if (c == null || c.l == 5 || (b2.k.t() == 0 && c.l == 2)) {
            hVar = b2;
        } else {
            c.p = true;
            hVar = c.k.a(b2, z2);
        }
        if (hVar != null) {
            if (hVar.k.t() == 1) {
                z3 = this.o.a(hVar);
            } else {
                z3 = this.n.a(hVar);
            }
            if (!z3) {
                throw new RejectedExecutionException(b.d.b.a.a.H(new StringBuilder(), this.t, " was terminated"));
            }
        }
        if (!z2 || c == null) {
            z4 = false;
        }
        if (b2.k.t() != 0) {
            long addAndGet = k.addAndGet(this, Permission.SPEAK);
            if (!z4 && !u() && !t(addAndGet)) {
                u();
            }
        } else if (!z4) {
            s();
        }
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        e(this, runnable, null, false, 6);
    }

    public final int f(C0428a aVar) {
        Object obj = aVar.nextParkedWorker;
        while (obj != m) {
            if (obj == null) {
                return 0;
            }
            C0428a aVar2 = (C0428a) obj;
            int i = aVar2.indexInArray;
            if (i != 0) {
                return i;
            }
            obj = aVar2.nextParkedWorker;
        }
        return -1;
    }

    public final void n(C0428a aVar, int i, int i2) {
        while (true) {
            long j2 = this.parkedWorkersStack;
            int i3 = (int) (2097151 & j2);
            long j3 = (Permission.SPEAK + j2) & (-2097152);
            if (i3 == i) {
                i3 = i2 == 0 ? f(aVar) : i2;
            }
            if (i3 >= 0 && j.compareAndSet(this, j2, j3 | i3)) {
                return;
            }
        }
    }

    public final void q(h hVar) {
        try {
            hVar.run();
        } finally {
        }
    }

    public final void s() {
        if (!u() && !t(this.controlState)) {
            u();
        }
    }

    public final boolean t(long j2) {
        if (f.coerceAtLeast(((int) (2097151 & j2)) - ((int) ((j2 & 4398044413952L) >> 21)), 0) < this.q) {
            int a = a();
            if (a == 1 && this.q > 1) {
                a();
            }
            if (a > 0) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        ArrayList arrayList = new ArrayList();
        int length = this.p.length();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        for (int i6 = 1; i6 < length; i6++) {
            C0428a aVar = this.p.get(i6);
            if (aVar != null) {
                int d = aVar.k.d();
                int h = b.h(aVar.l);
                if (h == 0) {
                    i++;
                    arrayList.add(String.valueOf(d) + "c");
                } else if (h == 1) {
                    i2++;
                    arrayList.add(String.valueOf(d) + "b");
                } else if (h == 2) {
                    i3++;
                } else if (h == 3) {
                    i4++;
                    if (d > 0) {
                        arrayList.add(String.valueOf(d) + "d");
                    }
                } else if (h == 4) {
                    i5++;
                }
            }
        }
        long j2 = this.controlState;
        return this.t + MentionUtilsKt.MENTIONS_CHAR + b.i.a.f.e.o.f.l0(this) + "[Pool Size {core = " + this.q + ", max = " + this.r + "}, Worker States {CPU = " + i + ", blocking = " + i2 + ", parked = " + i3 + ", dormant = " + i4 + ", terminated = " + i5 + "}, running workers queues = " + arrayList + ", global CPU queue size = " + this.n.c() + ", global blocking queue size = " + this.o.c() + ", Control State {created workers= " + ((int) (2097151 & j2)) + ", blocking tasks = " + ((int) ((4398044413952L & j2) >> 21)) + ", CPUs acquired = " + (this.q - ((int) ((9223367638808264704L & j2) >> 42))) + "}]";
    }

    public final boolean u() {
        while (true) {
            long j2 = this.parkedWorkersStack;
            C0428a aVar = this.p.get((int) (2097151 & j2));
            if (aVar != null) {
                long j3 = (Permission.SPEAK + j2) & (-2097152);
                int f = f(aVar);
                if (f >= 0 && j.compareAndSet(this, j2, f | j3)) {
                    aVar.nextParkedWorker = m;
                }
            } else {
                aVar = null;
            }
            if (aVar == null) {
                return false;
            }
            if (C0428a.j.compareAndSet(aVar, -1, 0)) {
                LockSupport.unpark(aVar);
                return true;
            }
        }
    }
}
