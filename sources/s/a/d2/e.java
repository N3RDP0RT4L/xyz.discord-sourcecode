package s.a.d2;

import java.util.Objects;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.ExecutorCoroutineDispatcher;
import s.a.d0;
/* compiled from: Dispatcher.kt */
/* loaded from: classes3.dex */
public final class e extends ExecutorCoroutineDispatcher implements i, Executor {
    public static final AtomicIntegerFieldUpdater j = AtomicIntegerFieldUpdater.newUpdater(e.class, "inFlightTasks");
    public final c l;
    public final int m;
    public final String n;
    public final int o;
    public final ConcurrentLinkedQueue<Runnable> k = new ConcurrentLinkedQueue<>();
    public volatile int inFlightTasks = 0;

    public e(c cVar, int i, String str, int i2) {
        this.l = cVar;
        this.m = i;
        this.n = str;
        this.o = i2;
    }

    public final void I(Runnable runnable, boolean z2) {
        do {
            AtomicIntegerFieldUpdater atomicIntegerFieldUpdater = j;
            if (atomicIntegerFieldUpdater.incrementAndGet(this) <= this.m) {
                c cVar = this.l;
                Objects.requireNonNull(cVar);
                try {
                    cVar.j.d(runnable, this, z2);
                    return;
                } catch (RejectedExecutionException unused) {
                    d0.q.T(cVar.j.b(runnable, this));
                    return;
                }
            } else {
                this.k.add(runnable);
                if (atomicIntegerFieldUpdater.decrementAndGet(this) < this.m) {
                    runnable = this.k.poll();
                } else {
                    return;
                }
            }
        } while (runnable != null);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        throw new IllegalStateException("Close cannot be invoked on LimitingBlockingDispatcher".toString());
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void dispatch(CoroutineContext coroutineContext, Runnable runnable) {
        I(runnable, false);
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public void dispatchYield(CoroutineContext coroutineContext, Runnable runnable) {
        I(runnable, true);
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        I(runnable, false);
    }

    @Override // s.a.d2.i
    public void f() {
        Runnable poll = this.k.poll();
        if (poll != null) {
            c cVar = this.l;
            Objects.requireNonNull(cVar);
            try {
                cVar.j.d(poll, this, true);
            } catch (RejectedExecutionException unused) {
                d0.q.T(cVar.j.b(poll, this));
            }
        } else {
            j.decrementAndGet(this);
            Runnable poll2 = this.k.poll();
            if (poll2 != null) {
                I(poll2, true);
            }
        }
    }

    @Override // s.a.d2.i
    public int t() {
        return this.o;
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public String toString() {
        String str = this.n;
        if (str != null) {
            return str;
        }
        return super.toString() + "[dispatcher = " + this.l + ']';
    }
}
