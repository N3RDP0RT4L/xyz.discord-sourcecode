package s.a;

import b.d.b.a.a;
import java.util.concurrent.Future;
import kotlin.Unit;
/* compiled from: Future.kt */
/* loaded from: classes3.dex */
public final class i extends j {
    public final Future<?> j;

    public i(Future<?> future) {
        this.j = future;
    }

    @Override // s.a.k
    public void a(Throwable th) {
        this.j.cancel(false);
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Throwable th) {
        this.j.cancel(false);
        return Unit.a;
    }

    public String toString() {
        StringBuilder R = a.R("CancelFutureOnCancel[");
        R.append(this.j);
        R.append(']');
        return R.toString();
    }
}
