package s.a;

import androidx.recyclerview.widget.RecyclerView;
import d0.d0.f;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import kotlin.coroutines.CoroutineContext;
import s.a.r0;
/* compiled from: DefaultExecutor.kt */
/* loaded from: classes3.dex */
public final class d0 extends r0 implements Runnable {
    public static volatile Thread _thread;
    public static volatile int debugStatus;
    public static final long p;
    public static final d0 q;

    static {
        Long l;
        d0 d0Var = new d0();
        q = d0Var;
        d0Var.L(false);
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        try {
            l = Long.getLong("kotlinx.coroutines.DefaultExecutor.keepAlive", 1000L);
        } catch (SecurityException unused) {
            l = 1000L;
        }
        p = timeUnit.toNanos(l.longValue());
    }

    @Override // s.a.s0
    public Thread S() {
        Thread thread = _thread;
        if (thread == null) {
            synchronized (this) {
                thread = _thread;
                if (thread == null) {
                    thread = new Thread(this, "kotlinx.coroutines.DefaultExecutor");
                    _thread = thread;
                    thread.setDaemon(true);
                    thread.start();
                }
            }
        }
        return thread;
    }

    public final synchronized void X() {
        if (b0()) {
            debugStatus = 3;
            this._queue = null;
            this._delayed = null;
            notifyAll();
        }
    }

    public final boolean b0() {
        int i = debugStatus;
        return i == 2 || i == 3;
    }

    @Override // java.lang.Runnable
    public void run() {
        boolean z2;
        v1 v1Var = v1.f3818b;
        v1.a.set(this);
        try {
            synchronized (this) {
                if (b0()) {
                    z2 = false;
                } else {
                    z2 = true;
                    debugStatus = 1;
                    notifyAll();
                }
            }
            if (z2) {
                long j = Long.MAX_VALUE;
                while (true) {
                    Thread.interrupted();
                    long O = O();
                    if (O == RecyclerView.FOREVER_NS) {
                        long nanoTime = System.nanoTime();
                        if (j == RecyclerView.FOREVER_NS) {
                            j = p + nanoTime;
                        }
                        long j2 = j - nanoTime;
                        if (j2 <= 0) {
                            _thread = null;
                            X();
                            if (!V()) {
                                S();
                                return;
                            }
                            return;
                        }
                        O = f.coerceAtMost(O, j2);
                    } else {
                        j = Long.MAX_VALUE;
                    }
                    if (O > 0) {
                        if (b0()) {
                            _thread = null;
                            X();
                            if (!V()) {
                                S();
                                return;
                            }
                            return;
                        }
                        LockSupport.parkNanos(this, O);
                    }
                }
            }
        } finally {
            _thread = null;
            X();
            if (!V()) {
                S();
            }
        }
    }

    @Override // s.a.r0, s.a.h0
    public m0 x(long j, Runnable runnable, CoroutineContext coroutineContext) {
        long a = t0.a(j);
        if (a >= 4611686018427387903L) {
            return n1.j;
        }
        long nanoTime = System.nanoTime();
        r0.b bVar = new r0.b(a + nanoTime, runnable);
        W(nanoTime, bVar);
        return bVar;
    }
}
