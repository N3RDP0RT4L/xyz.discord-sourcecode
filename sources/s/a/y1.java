package s.a;

import b.i.a.f.e.o.f;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import s.a.a.a;
import s.a.a.r;
/* compiled from: Builders.common.kt */
/* loaded from: classes3.dex */
public final class y1<T> extends r<T> {
    public y1(CoroutineContext coroutineContext, Continuation<? super T> continuation) {
        super(coroutineContext, continuation);
    }

    @Override // s.a.a.r, s.a.b
    public void e0(Object obj) {
        Object X0 = f.X0(obj, this.m);
        CoroutineContext context = this.m.getContext();
        Object b2 = a.b(context, null);
        try {
            this.m.resumeWith(X0);
        } finally {
            a.a(context, b2);
        }
    }
}
