package s.a.b2;

import d0.k;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.CancellationException;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.CancellableContinuation;
import s.a.a.i;
import s.a.a.j;
import s.a.a.k;
import s.a.a.o;
import s.a.a.q;
import s.a.a.s;
import s.a.a.t;
import s.a.b2.v;
import s.a.l;
import s.a.m;
/* compiled from: AbstractChannel.kt */
/* loaded from: classes3.dex */
public abstract class a<E> extends s.a.b2.c<E> implements s.a.b2.f<E> {

    /* compiled from: AbstractChannel.kt */
    /* renamed from: s.a.b2.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0425a<E> implements s.a.b2.g<E> {
        public Object a = s.a.b2.b.d;

        /* renamed from: b  reason: collision with root package name */
        public final a<E> f3805b;

        public C0425a(a<E> aVar) {
            this.f3805b = aVar;
        }

        @Override // s.a.b2.g
        public Object a(Continuation<? super Boolean> continuation) {
            Object obj = this.a;
            t tVar = s.a.b2.b.d;
            if (obj != tVar) {
                return d0.w.i.a.b.boxBoolean(b(obj));
            }
            Object v = this.f3805b.v();
            this.a = v;
            if (v != tVar) {
                return d0.w.i.a.b.boxBoolean(b(v));
            }
            l o0 = b.i.a.f.e.o.f.o0(d0.w.h.b.intercepted(continuation));
            d dVar = new d(this, o0);
            while (true) {
                if (this.f3805b.r(dVar)) {
                    a<E> aVar = this.f3805b;
                    Objects.requireNonNull(aVar);
                    o0.f(new e(dVar));
                    break;
                }
                Object v2 = this.f3805b.v();
                this.a = v2;
                if (v2 instanceof i) {
                    i iVar = (i) v2;
                    if (iVar.m == null) {
                        Boolean boxBoolean = d0.w.i.a.b.boxBoolean(false);
                        k.a aVar2 = k.j;
                        o0.resumeWith(k.m73constructorimpl(boxBoolean));
                    } else {
                        Throwable v3 = iVar.v();
                        k.a aVar3 = k.j;
                        o0.resumeWith(k.m73constructorimpl(d0.l.createFailure(v3)));
                    }
                } else if (v2 != s.a.b2.b.d) {
                    Boolean boxBoolean2 = d0.w.i.a.b.boxBoolean(true);
                    Function1<E, Unit> function1 = this.f3805b.l;
                    o0.y(boxBoolean2, o0.l, function1 != null ? new o(function1, v2, o0.o) : null);
                }
            }
            Object u = o0.u();
            if (u == d0.w.h.c.getCOROUTINE_SUSPENDED()) {
                d0.w.i.a.g.probeCoroutineSuspended(continuation);
            }
            return u;
        }

        public final boolean b(Object obj) {
            if (!(obj instanceof i)) {
                return true;
            }
            i iVar = (i) obj;
            if (iVar.m == null) {
                return false;
            }
            Throwable v = iVar.v();
            String str = s.a;
            throw v;
        }

        @Override // s.a.b2.g
        public E next() {
            E e = (E) this.a;
            if (!(e instanceof i)) {
                t tVar = s.a.b2.b.d;
                if (e != tVar) {
                    this.a = tVar;
                    return e;
                }
                throw new IllegalStateException("'hasNext' should be called prior to 'next' invocation");
            }
            Throwable v = ((i) e).v();
            String str = s.a;
            throw v;
        }
    }

    /* compiled from: AbstractChannel.kt */
    /* loaded from: classes3.dex */
    public static class b<E> extends n<E> {
        public final CancellableContinuation<Object> m;
        public final int n;

        public b(CancellableContinuation<Object> cancellableContinuation, int i) {
            this.m = cancellableContinuation;
            this.n = i;
        }

        @Override // s.a.b2.p
        public void c(E e) {
            this.m.r(m.a);
        }

        @Override // s.a.b2.p
        public t d(E e, k.b bVar) {
            if (this.m.h(this.n != 2 ? e : new v(e), null, q(e)) != null) {
                return m.a;
            }
            return null;
        }

        @Override // s.a.b2.n
        public void r(i<?> iVar) {
            int i = this.n;
            if (i == 1 && iVar.m == null) {
                CancellableContinuation<Object> cancellableContinuation = this.m;
                k.a aVar = d0.k.j;
                cancellableContinuation.resumeWith(d0.k.m73constructorimpl(null));
            } else if (i == 2) {
                CancellableContinuation<Object> cancellableContinuation2 = this.m;
                v vVar = new v(new v.a(iVar.m));
                k.a aVar2 = d0.k.j;
                cancellableContinuation2.resumeWith(d0.k.m73constructorimpl(vVar));
            } else {
                CancellableContinuation<Object> cancellableContinuation3 = this.m;
                Throwable v = iVar.v();
                k.a aVar3 = d0.k.j;
                cancellableContinuation3.resumeWith(d0.k.m73constructorimpl(d0.l.createFailure(v)));
            }
        }

        @Override // s.a.a.k
        public String toString() {
            StringBuilder R = b.d.b.a.a.R("ReceiveElement@");
            R.append(b.i.a.f.e.o.f.l0(this));
            R.append("[receiveMode=");
            return b.d.b.a.a.z(R, this.n, ']');
        }
    }

    /* compiled from: AbstractChannel.kt */
    /* loaded from: classes3.dex */
    public static final class c<E> extends b<E> {
        public final Function1<E, Unit> o;

        /* JADX WARN: Multi-variable type inference failed */
        public c(CancellableContinuation<Object> cancellableContinuation, int i, Function1<? super E, Unit> function1) {
            super(cancellableContinuation, i);
            this.o = function1;
        }

        @Override // s.a.b2.n
        public Function1<Throwable, Unit> q(E e) {
            return new o(this.o, e, this.m.getContext());
        }
    }

    /* compiled from: AbstractChannel.kt */
    /* loaded from: classes3.dex */
    public static class d<E> extends n<E> {
        public final C0425a<E> m;
        public final CancellableContinuation<Boolean> n;

        /* JADX WARN: Multi-variable type inference failed */
        public d(C0425a<E> aVar, CancellableContinuation<? super Boolean> cancellableContinuation) {
            this.m = aVar;
            this.n = cancellableContinuation;
        }

        @Override // s.a.b2.p
        public void c(E e) {
            this.m.a = e;
            this.n.r(m.a);
        }

        @Override // s.a.b2.p
        public t d(E e, k.b bVar) {
            if (this.n.h(Boolean.TRUE, null, q(e)) != null) {
                return m.a;
            }
            return null;
        }

        @Override // s.a.b2.n
        public Function1<Throwable, Unit> q(E e) {
            Function1<E, Unit> function1 = this.m.f3805b.l;
            if (function1 != null) {
                return new o(function1, e, this.n.getContext());
            }
            return null;
        }

        @Override // s.a.b2.n
        public void r(i<?> iVar) {
            Object obj;
            if (iVar.m == null) {
                obj = this.n.c(Boolean.FALSE, null);
            } else {
                obj = this.n.g(iVar.v());
            }
            if (obj != null) {
                this.m.a = iVar;
                this.n.r(obj);
            }
        }

        @Override // s.a.a.k
        public String toString() {
            StringBuilder R = b.d.b.a.a.R("ReceiveHasNext@");
            R.append(b.i.a.f.e.o.f.l0(this));
            return R.toString();
        }
    }

    /* compiled from: AbstractChannel.kt */
    /* loaded from: classes3.dex */
    public final class e extends s.a.e {
        public final n<?> j;

        public e(n<?> nVar) {
            this.j = nVar;
        }

        @Override // s.a.k
        public void a(Throwable th) {
            if (this.j.n()) {
                Objects.requireNonNull(a.this);
            }
        }

        @Override // kotlin.jvm.functions.Function1
        public Unit invoke(Throwable th) {
            if (this.j.n()) {
                Objects.requireNonNull(a.this);
            }
            return Unit.a;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("RemoveReceiveOnCancel[");
            R.append(this.j);
            R.append(']');
            return R.toString();
        }
    }

    /* compiled from: LockFreeLinkedList.kt */
    /* loaded from: classes3.dex */
    public static final class f extends k.a {
        public final /* synthetic */ a d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public f(s.a.a.k kVar, s.a.a.k kVar2, a aVar) {
            super(kVar2);
            this.d = aVar;
        }

        @Override // s.a.a.d
        public Object c(s.a.a.k kVar) {
            if (this.d.t()) {
                return null;
            }
            return j.a;
        }
    }

    /* compiled from: AbstractChannel.kt */
    @d0.w.i.a.e(c = "kotlinx.coroutines.channels.AbstractChannel", f = "AbstractChannel.kt", l = {624}, m = "receiveOrClosed-ZYPwvRU")
    /* loaded from: classes3.dex */
    public static final class g extends d0.w.i.a.d {
        public Object L$0;
        public Object L$1;
        public int label;
        public /* synthetic */ Object result;

        public g(Continuation continuation) {
            super(continuation);
        }

        @Override // d0.w.i.a.a
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return a.this.m(this);
        }
    }

    public a(Function1<? super E, Unit> function1) {
        super(function1);
    }

    @Override // s.a.b2.o
    public final void b(CancellationException cancellationException) {
        if (cancellationException == null) {
            cancellationException = new CancellationException(getClass().getSimpleName() + " was cancelled");
        }
        u(j(cancellationException));
    }

    @Override // s.a.b2.o
    public final s.a.b2.g<E> iterator() {
        return new C0425a(this);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARN: Removed duplicated region for block: B:14:0x0036  */
    /* JADX WARN: Type inference failed for: r2v7, types: [s.a.b2.a$b] */
    /* JADX WARN: Type inference failed for: r6v0, types: [s.a.b2.a<E>, s.a.b2.a, java.lang.Object, s.a.b2.c] */
    @Override // s.a.b2.o
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final java.lang.Object m(kotlin.coroutines.Continuation<? super s.a.b2.v<? extends E>> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof s.a.b2.a.g
            if (r0 == 0) goto L13
            r0 = r7
            s.a.b2.a$g r0 = (s.a.b2.a.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L13
            int r1 = r1 - r2
            r0.label = r1
            goto L18
        L13:
            s.a.b2.a$g r0 = new s.a.b2.a$g
            r0.<init>(r7)
        L18:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L36
            if (r2 != r3) goto L2e
            java.lang.Object r0 = r0.L$0
            s.a.b2.a r0 = (s.a.b2.a) r0
            d0.l.throwOnFailure(r7)
            goto Lb5
        L2e:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L36:
            d0.l.throwOnFailure(r7)
            java.lang.Object r7 = r6.v()
            s.a.a.t r2 = s.a.b2.b.d
            if (r7 == r2) goto L50
            boolean r0 = r7 instanceof s.a.b2.i
            if (r0 == 0) goto L4f
            s.a.b2.i r7 = (s.a.b2.i) r7
            java.lang.Throwable r7 = r7.m
            s.a.b2.v$a r0 = new s.a.b2.v$a
            r0.<init>(r7)
            r7 = r0
        L4f:
            return r7
        L50:
            r0.L$0 = r6
            r0.L$1 = r7
            r0.label = r3
            kotlin.coroutines.Continuation r7 = d0.w.h.b.intercepted(r0)
            s.a.l r7 = b.i.a.f.e.o.f.o0(r7)
            kotlin.jvm.functions.Function1<E, kotlin.Unit> r2 = r6.l
            r3 = 2
            if (r2 != 0) goto L69
            s.a.b2.a$b r2 = new s.a.b2.a$b
            r2.<init>(r7, r3)
            goto L70
        L69:
            s.a.b2.a$c r2 = new s.a.b2.a$c
            kotlin.jvm.functions.Function1<E, kotlin.Unit> r4 = r6.l
            r2.<init>(r7, r3, r4)
        L70:
            boolean r4 = r6.r(r2)
            if (r4 == 0) goto L7f
            s.a.b2.a$e r3 = new s.a.b2.a$e
            r3.<init>(r2)
            r7.f(r3)
            goto La5
        L7f:
            java.lang.Object r4 = r6.v()
            boolean r5 = r4 instanceof s.a.b2.i
            if (r5 == 0) goto L8d
            s.a.b2.i r4 = (s.a.b2.i) r4
            r2.r(r4)
            goto La5
        L8d:
            s.a.a.t r5 = s.a.b2.b.d
            if (r4 == r5) goto L70
            int r5 = r2.n
            if (r5 == r3) goto L97
            r3 = r4
            goto L9c
        L97:
            s.a.b2.v r3 = new s.a.b2.v
            r3.<init>(r4)
        L9c:
            kotlin.jvm.functions.Function1 r2 = r2.q(r4)
            int r4 = r7.l
            r7.y(r3, r4, r2)
        La5:
            java.lang.Object r7 = r7.u()
            java.lang.Object r2 = d0.w.h.c.getCOROUTINE_SUSPENDED()
            if (r7 != r2) goto Lb2
            d0.w.i.a.g.probeCoroutineSuspended(r0)
        Lb2:
            if (r7 != r1) goto Lb5
            return r1
        Lb5:
            s.a.b2.v r7 = (s.a.b2.v) r7
            java.lang.Object r7 = r7.a
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.b2.a.m(kotlin.coroutines.Continuation):java.lang.Object");
    }

    @Override // s.a.b2.c
    public p<E> n() {
        p<E> n = super.n();
        if (n != null) {
            boolean z2 = n instanceof i;
        }
        return n;
    }

    public boolean r(n<? super E> nVar) {
        int p;
        s.a.a.k k;
        if (!s()) {
            s.a.a.k kVar = this.k;
            f fVar = new f(nVar, nVar, this);
            do {
                s.a.a.k k2 = kVar.k();
                if (!(!(k2 instanceof r))) {
                    break;
                }
                p = k2.p(nVar, kVar, fVar);
                if (p == 1) {
                    return true;
                }
            } while (p != 2);
        } else {
            s.a.a.k kVar2 = this.k;
            do {
                k = kVar2.k();
                if (!(!(k instanceof r))) {
                }
            } while (!k.e(nVar, kVar2));
            return true;
        }
        return false;
    }

    public abstract boolean s();

    public abstract boolean t();

    public void u(boolean z2) {
        i<?> e2 = e();
        if (e2 != null) {
            Object obj = null;
            while (true) {
                s.a.a.k k = e2.k();
                if (k instanceof i) {
                    break;
                } else if (!k.n()) {
                    Object i = k.i();
                    Objects.requireNonNull(i, "null cannot be cast to non-null type kotlinx.coroutines.internal.Removed");
                    ((q) i).a.f(null);
                } else {
                    obj = b.i.a.f.e.o.f.T0(obj, (r) k);
                }
            }
            if (obj != null) {
                if (!(obj instanceof ArrayList)) {
                    ((r) obj).s(e2);
                    return;
                }
                ArrayList arrayList = (ArrayList) obj;
                int size = arrayList.size();
                while (true) {
                    size--;
                    if (size >= 0) {
                        ((r) arrayList.get(size)).s(e2);
                    } else {
                        return;
                    }
                }
            }
        } else {
            throw new IllegalStateException("Cannot happen".toString());
        }
    }

    public Object v() {
        while (true) {
            r q = q();
            if (q == null) {
                return s.a.b2.b.d;
            }
            if (q.t(null) != null) {
                q.q();
                return q.r();
            }
            q.u();
        }
    }
}
