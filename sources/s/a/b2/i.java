package s.a.b2;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import kotlinx.coroutines.channels.ClosedReceiveChannelException;
import kotlinx.coroutines.channels.ClosedSendChannelException;
import s.a.a.k;
import s.a.a.t;
import s.a.m;
/* compiled from: AbstractChannel.kt */
/* loaded from: classes3.dex */
public final class i<E> extends r implements p<E> {
    public final Throwable m;

    public i(Throwable th) {
        this.m = th;
    }

    @Override // s.a.b2.p
    public Object b() {
        return this;
    }

    @Override // s.a.b2.p
    public void c(E e) {
    }

    @Override // s.a.b2.p
    public t d(E e, k.b bVar) {
        return m.a;
    }

    @Override // s.a.b2.r
    public void q() {
    }

    @Override // s.a.b2.r
    public Object r() {
        return this;
    }

    @Override // s.a.b2.r
    public void s(i<?> iVar) {
    }

    @Override // s.a.b2.r
    public t t(k.b bVar) {
        return m.a;
    }

    @Override // s.a.a.k
    public String toString() {
        StringBuilder R = a.R("Closed@");
        R.append(f.l0(this));
        R.append('[');
        R.append(this.m);
        R.append(']');
        return R.toString();
    }

    public final Throwable v() {
        Throwable th = this.m;
        return th != null ? th : new ClosedReceiveChannelException("Channel was closed");
    }

    public final Throwable w() {
        Throwable th = this.m;
        return th != null ? th : new ClosedSendChannelException("Channel was closed");
    }
}
