package s.a.b2;
/* compiled from: Channel.kt */
/* loaded from: classes3.dex */
public interface f<E> extends s<E>, o<E> {
    public static final a i = a.f3807b;

    /* compiled from: Channel.kt */
    /* loaded from: classes3.dex */
    public static final class a {

        /* renamed from: b  reason: collision with root package name */
        public static final /* synthetic */ a f3807b = new a();
        public static final int a = (int) b.i.a.f.e.o.f.j1("kotlinx.coroutines.channels.defaultBuffer", 64, 1, 2147483646);
    }
}
