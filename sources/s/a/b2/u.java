package s.a.b2;

import b.i.a.f.e.o.f;
import kotlin.Unit;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.CancellableContinuation;
import kotlinx.coroutines.internal.UndeliveredElementException;
/* compiled from: AbstractChannel.kt */
/* loaded from: classes3.dex */
public final class u<E> extends t<E> {
    public final Function1<E, Unit> o;

    /* JADX WARN: Multi-variable type inference failed */
    public u(E e, CancellableContinuation<? super Unit> cancellableContinuation, Function1<? super E, Unit> function1) {
        super(e, cancellableContinuation);
        this.o = function1;
    }

    @Override // s.a.a.k
    public boolean n() {
        if (!super.n()) {
            return false;
        }
        u();
        return true;
    }

    @Override // s.a.b2.r
    public void u() {
        Function1<E, Unit> function1 = this.o;
        E e = this.m;
        CoroutineContext context = this.n.getContext();
        UndeliveredElementException p = f.p(function1, e, null);
        if (p != null) {
            f.u0(context, p);
        }
    }
}
