package s.a.b2;

import d0.z.d.m;
/* compiled from: Channel.kt */
/* loaded from: classes3.dex */
public final class v<T> {
    public final Object a;

    /* compiled from: Channel.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public final Throwable a;

        public a(Throwable th) {
            this.a = th;
        }

        public boolean equals(Object obj) {
            return (obj instanceof a) && m.areEqual(this.a, ((a) obj).a);
        }

        public int hashCode() {
            Throwable th = this.a;
            if (th != null) {
                return th.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder R = b.d.b.a.a.R("Closed(");
            R.append(this.a);
            R.append(')');
            return R.toString();
        }
    }

    public boolean equals(Object obj) {
        return (obj instanceof v) && m.areEqual(this.a, ((v) obj).a);
    }

    public int hashCode() {
        Object obj = this.a;
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    public String toString() {
        Object obj = this.a;
        if (obj instanceof a) {
            return obj.toString();
        }
        return "Value(" + obj + ')';
    }
}
