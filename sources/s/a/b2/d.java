package s.a.b2;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import d0.t.j;
import d0.z.d.m;
import java.util.concurrent.locks.ReentrantLock;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.internal.UndeliveredElementException;
import s.a.a.t;
/* compiled from: ArrayChannel.kt */
/* loaded from: classes3.dex */
public class d<E> extends a<E> {
    public final ReentrantLock m;
    public Object[] n;
    public int o;
    public final int p;
    public final e q;
    public volatile int size;

    public d(int i, e eVar, Function1<? super E, Unit> function1) {
        super(function1);
        this.p = i;
        this.q = eVar;
        if (i < 1 ? false : true) {
            this.m = new ReentrantLock();
            Object[] objArr = new Object[Math.min(i, 8)];
            j.fill$default(objArr, b.a, 0, 0, 6, null);
            this.n = objArr;
            this.size = 0;
            return;
        }
        throw new IllegalArgumentException(a.q("ArrayChannel capacity must be at least 1, but ", i, " was specified").toString());
    }

    @Override // s.a.b2.c
    public Object c(r rVar) {
        ReentrantLock reentrantLock = this.m;
        reentrantLock.lock();
        try {
            return super.c(rVar);
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // s.a.b2.c
    public String d() {
        StringBuilder R = a.R("(buffer:capacity=");
        R.append(this.p);
        R.append(",size=");
        return a.z(R, this.size, ')');
    }

    @Override // s.a.b2.c
    public final boolean h() {
        return false;
    }

    @Override // s.a.b2.c
    public final boolean i() {
        return this.size == this.p && this.q == e.SUSPEND;
    }

    /* JADX WARN: Removed duplicated region for block: B:21:0x0038 A[DONT_GENERATE] */
    /* JADX WARN: Removed duplicated region for block: B:23:0x003c  */
    @Override // s.a.b2.c
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public java.lang.Object k(E r6) {
        /*
            r5 = this;
            java.util.concurrent.locks.ReentrantLock r0 = r5.m
            r0.lock()
            int r1 = r5.size     // Catch: java.lang.Throwable -> L76
            s.a.b2.i r2 = r5.e()     // Catch: java.lang.Throwable -> L76
            if (r2 == 0) goto L11
            r0.unlock()
            return r2
        L11:
            int r2 = r5.p     // Catch: java.lang.Throwable -> L76
            r3 = 1
            r4 = 0
            if (r1 >= r2) goto L1c
            int r2 = r1 + 1
            r5.size = r2     // Catch: java.lang.Throwable -> L76
            goto L32
        L1c:
            s.a.b2.e r2 = r5.q     // Catch: java.lang.Throwable -> L76
            int r2 = r2.ordinal()     // Catch: java.lang.Throwable -> L76
            if (r2 == 0) goto L34
            if (r2 == r3) goto L32
            r3 = 2
            if (r2 != r3) goto L2c
            s.a.a.t r2 = s.a.b2.b.f3806b     // Catch: java.lang.Throwable -> L76
            goto L36
        L2c:
            kotlin.NoWhenBranchMatchedException r6 = new kotlin.NoWhenBranchMatchedException     // Catch: java.lang.Throwable -> L76
            r6.<init>()     // Catch: java.lang.Throwable -> L76
            throw r6     // Catch: java.lang.Throwable -> L76
        L32:
            r2 = r4
            goto L36
        L34:
            s.a.a.t r2 = s.a.b2.b.c     // Catch: java.lang.Throwable -> L76
        L36:
            if (r2 == 0) goto L3c
            r0.unlock()
            return r2
        L3c:
            if (r1 != 0) goto L6d
        L3e:
            s.a.b2.p r2 = r5.n()     // Catch: java.lang.Throwable -> L76
            if (r2 == 0) goto L6d
            boolean r3 = r2 instanceof s.a.b2.i     // Catch: java.lang.Throwable -> L76
            if (r3 == 0) goto L51
            r5.size = r1     // Catch: java.lang.Throwable -> L76
            d0.z.d.m.checkNotNull(r2)     // Catch: java.lang.Throwable -> L76
            r0.unlock()
            return r2
        L51:
            d0.z.d.m.checkNotNull(r2)     // Catch: java.lang.Throwable -> L76
            s.a.a.t r3 = r2.d(r6, r4)     // Catch: java.lang.Throwable -> L76
            if (r3 == 0) goto L3e
            r5.size = r1     // Catch: java.lang.Throwable -> L76
            r0.unlock()
            d0.z.d.m.checkNotNull(r2)
            r2.c(r6)
            d0.z.d.m.checkNotNull(r2)
            java.lang.Object r6 = r2.b()
            return r6
        L6d:
            r5.w(r1, r6)     // Catch: java.lang.Throwable -> L76
            s.a.a.t r6 = s.a.b2.b.f3806b     // Catch: java.lang.Throwable -> L76
            r0.unlock()
            return r6
        L76:
            r6 = move-exception
            r0.unlock()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.b2.d.k(java.lang.Object):java.lang.Object");
    }

    @Override // s.a.b2.a
    public boolean r(n<? super E> nVar) {
        ReentrantLock reentrantLock = this.m;
        reentrantLock.lock();
        try {
            return super.r(nVar);
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // s.a.b2.a
    public final boolean s() {
        return false;
    }

    @Override // s.a.b2.a
    public final boolean t() {
        return this.size == 0;
    }

    /* JADX WARN: Finally extract failed */
    @Override // s.a.b2.a
    public void u(boolean z2) {
        Function1<E, Unit> function1 = this.l;
        ReentrantLock reentrantLock = this.m;
        reentrantLock.lock();
        try {
            int i = this.size;
            UndeliveredElementException undeliveredElementException = null;
            for (int i2 = 0; i2 < i; i2++) {
                Object obj = this.n[this.o];
                if (!(function1 == null || obj == b.a)) {
                    undeliveredElementException = f.p(function1, obj, undeliveredElementException);
                }
                Object[] objArr = this.n;
                int i3 = this.o;
                objArr[i3] = b.a;
                this.o = (i3 + 1) % objArr.length;
            }
            this.size = 0;
            reentrantLock.unlock();
            super.u(z2);
            if (undeliveredElementException != null) {
                throw undeliveredElementException;
            }
        } catch (Throwable th) {
            reentrantLock.unlock();
            throw th;
        }
    }

    @Override // s.a.b2.a
    public Object v() {
        ReentrantLock reentrantLock = this.m;
        reentrantLock.lock();
        try {
            int i = this.size;
            if (i == 0) {
                Object e = e();
                if (e == null) {
                    e = b.d;
                }
                return e;
            }
            Object[] objArr = this.n;
            int i2 = this.o;
            Object obj = objArr[i2];
            r rVar = null;
            objArr[i2] = null;
            this.size = i - 1;
            Object obj2 = b.d;
            boolean z2 = false;
            if (i == this.p) {
                r rVar2 = null;
                while (true) {
                    r q = q();
                    if (q == null) {
                        rVar = rVar2;
                        break;
                    }
                    m.checkNotNull(q);
                    if (q.t(null) != null) {
                        m.checkNotNull(q);
                        obj2 = q.r();
                        rVar = q;
                        z2 = true;
                        break;
                    }
                    m.checkNotNull(q);
                    q.u();
                    rVar2 = q;
                }
            }
            if (obj2 != b.d && !(obj2 instanceof i)) {
                this.size = i;
                Object[] objArr2 = this.n;
                objArr2[(this.o + i) % objArr2.length] = obj2;
            }
            this.o = (this.o + 1) % this.n.length;
            if (z2) {
                m.checkNotNull(rVar);
                rVar.q();
            }
            return obj;
        } finally {
            reentrantLock.unlock();
        }
    }

    public final void w(int i, E e) {
        int i2 = this.p;
        if (i < i2) {
            Object[] objArr = this.n;
            if (i >= objArr.length) {
                int min = Math.min(objArr.length * 2, i2);
                Object[] objArr2 = new Object[min];
                for (int i3 = 0; i3 < i; i3++) {
                    Object[] objArr3 = this.n;
                    objArr2[i3] = objArr3[(this.o + i3) % objArr3.length];
                }
                j.fill((t[]) objArr2, b.a, i, min);
                this.n = objArr2;
                this.o = 0;
            }
            Object[] objArr4 = this.n;
            objArr4[(this.o + i) % objArr4.length] = e;
            return;
        }
        Object[] objArr5 = this.n;
        int i4 = this.o;
        objArr5[i4 % objArr5.length] = null;
        objArr5[(i + i4) % objArr5.length] = e;
        this.o = (i4 + 1) % objArr5.length;
    }
}
