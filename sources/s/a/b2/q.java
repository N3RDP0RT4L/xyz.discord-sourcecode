package s.a.b2;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: RendezvousChannel.kt */
/* loaded from: classes3.dex */
public class q<E> extends a<E> {
    public q(Function1<? super E, Unit> function1) {
        super(function1);
    }

    @Override // s.a.b2.c
    public final boolean h() {
        return true;
    }

    @Override // s.a.b2.c
    public final boolean i() {
        return true;
    }

    @Override // s.a.b2.a
    public final boolean s() {
        return true;
    }

    @Override // s.a.b2.a
    public final boolean t() {
        return true;
    }
}
