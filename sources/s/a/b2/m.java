package s.a.b2;

import b.i.a.f.e.o.f;
import java.util.concurrent.CancellationException;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.JobCancellationException;
import kotlinx.coroutines.channels.ProducerScope;
import s.a.h1;
/* compiled from: Produce.kt */
/* loaded from: classes3.dex */
public class m<E> implements ProducerScope<E>, f {
    public final f<E> m;

    public m(CoroutineContext coroutineContext, f<E> fVar) {
        super(coroutineContext, true);
        this.m = fVar;
    }

    public boolean a() {
        return super.a();
    }

    @Override // s.a.b2.o
    public final void b(CancellationException cancellationException) {
        if (cancellationException == null) {
            cancellationException = new JobCancellationException(B(), null, this);
        }
        x(cancellationException);
    }

    public void g0(Throwable th, boolean z2) {
        if (!this.m.j(th) && !z2) {
            f.u0(this.k, th);
        }
    }

    public void h0(Object obj) {
        Unit unit = (Unit) obj;
        f.I(this.m, null, 1, null);
    }

    @Override // s.a.b2.o
    public g iterator() {
        return this.m.iterator();
    }

    @Override // s.a.b2.s
    public boolean j(Throwable th) {
        return this.m.j(th);
    }

    @Override // s.a.b2.s
    public void l(Function1 function1) {
        this.m.l(function1);
    }

    @Override // s.a.b2.o
    public Object m(Continuation continuation) {
        return this.m.m(continuation);
    }

    @Override // s.a.b2.s
    public Object o(Object obj, Continuation continuation) {
        return this.m.o(obj, continuation);
    }

    @Override // s.a.b2.s
    public boolean offer(Object obj) {
        return this.m.offer(obj);
    }

    @Override // s.a.b2.s
    public boolean p() {
        return this.m.p();
    }

    public void x(Throwable th) {
        CancellationException b02 = h1.b0(this, th, null, 1, null);
        this.m.b(b02);
        w(b02);
    }
}
