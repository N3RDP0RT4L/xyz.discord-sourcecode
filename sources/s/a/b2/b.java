package s.a.b2;

import s.a.a.t;
/* compiled from: AbstractChannel.kt */
/* loaded from: classes3.dex */
public final class b {
    public static final t a = new t("EMPTY");

    /* renamed from: b  reason: collision with root package name */
    public static final t f3806b = new t("OFFER_SUCCESS");
    public static final t c = new t("OFFER_FAILED");
    public static final t d = new t("POLL_FAILED");
    public static final t e = new t("ENQUEUE_FAILED");
    public static final t f = new t("ON_CLOSE_HANDLER_INVOKED");
}
