package s.a.b2;

import b.d.b.a.a;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import s.a.a.t;
import s.a.b2.c;
/* compiled from: LinkedListChannel.kt */
/* loaded from: classes3.dex */
public class k<E> extends a<E> {
    public k(Function1<? super E, Unit> function1) {
        super(function1);
    }

    @Override // s.a.b2.c
    public final boolean h() {
        return false;
    }

    @Override // s.a.b2.c
    public final boolean i() {
        return false;
    }

    @Override // s.a.b2.c
    public Object k(E e) {
        p pVar;
        do {
            Object k = super.k(e);
            t tVar = b.f3806b;
            if (k == tVar) {
                return tVar;
            }
            if (k == b.c) {
                s.a.a.k kVar = this.k;
                c.a aVar = new c.a(e);
                while (true) {
                    s.a.a.k k2 = kVar.k();
                    if (!(k2 instanceof p)) {
                        if (k2.e(aVar, kVar)) {
                            pVar = null;
                            break;
                        }
                    } else {
                        pVar = (p) k2;
                        break;
                    }
                }
                if (pVar == null) {
                    return b.f3806b;
                }
            } else if (k instanceof i) {
                return k;
            } else {
                throw new IllegalStateException(a.u("Invalid offerInternal result ", k).toString());
            }
        } while (!(pVar instanceof i));
        return pVar;
    }

    @Override // s.a.b2.a
    public final boolean s() {
        return true;
    }

    @Override // s.a.b2.a
    public final boolean t() {
        return true;
    }
}
