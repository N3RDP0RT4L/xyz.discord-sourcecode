package s.a.b2;

import b.i.a.f.e.o.f;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.k;
import d0.w.i.a.g;
import d0.z.d.e0;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.internal.UndeliveredElementException;
import s.a.a.i;
import s.a.a.j;
import s.a.a.k;
import s.a.a.q;
import s.a.a.s;
import s.a.a.t;
import s.a.l;
import s.a.m;
import s.a.q1;
/* compiled from: AbstractChannel.kt */
/* loaded from: classes3.dex */
public abstract class c<E> implements s<E> {
    public static final AtomicReferenceFieldUpdater j = AtomicReferenceFieldUpdater.newUpdater(c.class, Object.class, "onCloseHandler");
    public final Function1<E, Unit> l;
    public final i k = new i();
    public volatile Object onCloseHandler = null;

    /* compiled from: AbstractChannel.kt */
    /* loaded from: classes3.dex */
    public static final class a<E> extends r {
        public final E m;

        public a(E e) {
            this.m = e;
        }

        @Override // s.a.b2.r
        public void q() {
        }

        @Override // s.a.b2.r
        public Object r() {
            return this.m;
        }

        @Override // s.a.b2.r
        public void s(i<?> iVar) {
        }

        @Override // s.a.b2.r
        public t t(k.b bVar) {
            return m.a;
        }

        @Override // s.a.a.k
        public String toString() {
            StringBuilder R = b.d.b.a.a.R("SendBuffered@");
            R.append(f.l0(this));
            R.append('(');
            R.append(this.m);
            R.append(')');
            return R.toString();
        }
    }

    /* compiled from: LockFreeLinkedList.kt */
    /* loaded from: classes3.dex */
    public static final class b extends k.a {
        public final /* synthetic */ c d;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public b(k kVar, k kVar2, c cVar) {
            super(kVar2);
            this.d = cVar;
        }

        @Override // s.a.a.d
        public Object c(k kVar) {
            if (this.d.i()) {
                return null;
            }
            return j.a;
        }
    }

    /* JADX WARN: Multi-variable type inference failed */
    public c(Function1<? super E, Unit> function1) {
        this.l = function1;
    }

    public static final void a(c cVar, Continuation continuation, Object obj, i iVar) {
        UndeliveredElementException q;
        cVar.f(iVar);
        Throwable w = iVar.w();
        Function1<E, Unit> function1 = cVar.l;
        if (function1 == null || (q = f.q(function1, obj, null, 2)) == null) {
            k.a aVar = d0.k.j;
            ((l) continuation).resumeWith(d0.k.m73constructorimpl(d0.l.createFailure(w)));
            return;
        }
        d0.b.addSuppressed(q, w);
        k.a aVar2 = d0.k.j;
        ((l) continuation).resumeWith(d0.k.m73constructorimpl(d0.l.createFailure(q)));
    }

    public Object c(r rVar) {
        boolean z2;
        s.a.a.k k;
        if (h()) {
            s.a.a.k kVar = this.k;
            do {
                k = kVar.k();
                if (k instanceof p) {
                    return k;
                }
            } while (!k.e(rVar, kVar));
            return null;
        }
        s.a.a.k kVar2 = this.k;
        b bVar = new b(rVar, rVar, this);
        while (true) {
            s.a.a.k k2 = kVar2.k();
            if (!(k2 instanceof p)) {
                int p = k2.p(rVar, kVar2, bVar);
                z2 = true;
                if (p != 1) {
                    if (p == 2) {
                        z2 = false;
                        break;
                    }
                } else {
                    break;
                }
            } else {
                return k2;
            }
        }
        if (!z2) {
            return s.a.b2.b.e;
        }
        return null;
    }

    public String d() {
        return "";
    }

    public final i<?> e() {
        s.a.a.k k = this.k.k();
        if (!(k instanceof i)) {
            k = null;
        }
        i<?> iVar = (i) k;
        if (iVar == null) {
            return null;
        }
        f(iVar);
        return iVar;
    }

    public final void f(i<?> iVar) {
        Object obj = null;
        while (true) {
            s.a.a.k k = iVar.k();
            if (!(k instanceof n)) {
                k = null;
            }
            n nVar = (n) k;
            if (nVar == null) {
                break;
            } else if (!nVar.n()) {
                Object i = nVar.i();
                Objects.requireNonNull(i, "null cannot be cast to non-null type kotlinx.coroutines.internal.Removed");
                ((q) i).a.f(null);
            } else {
                obj = f.T0(obj, nVar);
            }
        }
        if (obj != null) {
            if (!(obj instanceof ArrayList)) {
                ((n) obj).r(iVar);
                return;
            }
            ArrayList arrayList = (ArrayList) obj;
            int size = arrayList.size();
            while (true) {
                size--;
                if (size >= 0) {
                    ((n) arrayList.get(size)).r(iVar);
                } else {
                    return;
                }
            }
        }
    }

    public final Throwable g(E e, i<?> iVar) {
        UndeliveredElementException q;
        f(iVar);
        Function1<E, Unit> function1 = this.l;
        if (function1 == null || (q = f.q(function1, e, null, 2)) == null) {
            return iVar.w();
        }
        d0.b.addSuppressed(q, iVar.w());
        throw q;
    }

    public abstract boolean h();

    public abstract boolean i();

    @Override // s.a.b2.s
    public boolean j(Throwable th) {
        boolean z2;
        Object obj;
        t tVar;
        i<?> iVar = new i<>(th);
        s.a.a.k kVar = this.k;
        while (true) {
            s.a.a.k k = kVar.k();
            if (!(k instanceof i)) {
                if (k.e(iVar, kVar)) {
                    z2 = true;
                    break;
                }
            } else {
                z2 = false;
                break;
            }
        }
        if (!z2) {
            iVar = (i) this.k.k();
        }
        f(iVar);
        if (z2 && (obj = this.onCloseHandler) != null && obj != (tVar = s.a.b2.b.f) && j.compareAndSet(this, obj, tVar)) {
            ((Function1) e0.beforeCheckcastToFunctionOfArity(obj, 1)).invoke(th);
        }
        return z2;
    }

    public Object k(E e) {
        p<E> n;
        do {
            n = n();
            if (n == null) {
                return s.a.b2.b.c;
            }
        } while (n.d(e, null) == null);
        n.c(e);
        return n.b();
    }

    @Override // s.a.b2.s
    public void l(Function1<? super Throwable, Unit> function1) {
        AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = j;
        if (!atomicReferenceFieldUpdater.compareAndSet(this, null, function1)) {
            Object obj = this.onCloseHandler;
            if (obj == s.a.b2.b.f) {
                throw new IllegalStateException("Another handler was already registered and successfully invoked");
            }
            throw new IllegalStateException(b.d.b.a.a.u("Another handler was already registered: ", obj));
        }
        i<?> e = e();
        if (e != null && atomicReferenceFieldUpdater.compareAndSet(this, function1, s.a.b2.b.f)) {
            function1.invoke(e.m);
        }
    }

    public p<E> n() {
        s.a.a.k kVar;
        s.a.a.k o;
        i iVar = this.k;
        while (true) {
            Object i = iVar.i();
            Objects.requireNonNull(i, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            kVar = (s.a.a.k) i;
            kVar = null;
            if (kVar != iVar && (kVar instanceof p)) {
                if (((((p) kVar) instanceof i) && !kVar.m()) || (o = kVar.o()) == null) {
                    break;
                }
                o.l();
            }
        }
        return (p) kVar;
    }

    @Override // s.a.b2.s
    public final Object o(E e, Continuation<? super Unit> continuation) {
        r rVar;
        if (k(e) == s.a.b2.b.f3806b) {
            return Unit.a;
        }
        l o0 = f.o0(d0.w.h.b.intercepted(continuation));
        while (true) {
            if (!(this.k.j() instanceof p) && i()) {
                if (this.l == null) {
                    rVar = new t(e, o0);
                } else {
                    rVar = new u(e, o0, this.l);
                }
                Object c = c(rVar);
                if (c == null) {
                    o0.f(new q1(rVar));
                    break;
                } else if (c instanceof i) {
                    a(this, o0, e, (i) c);
                    break;
                } else if (c != s.a.b2.b.e && !(c instanceof n)) {
                    throw new IllegalStateException(b.d.b.a.a.u("enqueueSend returned ", c).toString());
                }
            }
            Object k = k(e);
            if (k == s.a.b2.b.f3806b) {
                Unit unit = Unit.a;
                k.a aVar = d0.k.j;
                o0.resumeWith(d0.k.m73constructorimpl(unit));
                break;
            } else if (k != s.a.b2.b.c) {
                if (k instanceof i) {
                    a(this, o0, e, (i) k);
                } else {
                    throw new IllegalStateException(b.d.b.a.a.u("offerInternal returned ", k).toString());
                }
            }
        }
        Object u = o0.u();
        if (u == d0.w.h.c.getCOROUTINE_SUSPENDED()) {
            g.probeCoroutineSuspended(continuation);
        }
        return u == d0.w.h.c.getCOROUTINE_SUSPENDED() ? u : Unit.a;
    }

    @Override // s.a.b2.s
    public final boolean offer(E e) {
        Object k = k(e);
        if (k == s.a.b2.b.f3806b) {
            return true;
        }
        if (k == s.a.b2.b.c) {
            i<?> e2 = e();
            if (e2 == null) {
                return false;
            }
            Throwable g = g(e, e2);
            String str = s.a;
            throw g;
        } else if (k instanceof i) {
            Throwable g2 = g(e, (i) k);
            String str2 = s.a;
            throw g2;
        } else {
            throw new IllegalStateException(b.d.b.a.a.u("offerInternal returned ", k).toString());
        }
    }

    @Override // s.a.b2.s
    public final boolean p() {
        return e() != null;
    }

    public final r q() {
        s.a.a.k kVar;
        s.a.a.k o;
        i iVar = this.k;
        while (true) {
            Object i = iVar.i();
            Objects.requireNonNull(i, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            kVar = (s.a.a.k) i;
            kVar = null;
            if (kVar != iVar && (kVar instanceof r)) {
                if (((((r) kVar) instanceof i) && !kVar.m()) || (o = kVar.o()) == null) {
                    break;
                }
                o.l();
            }
        }
        return (r) kVar;
    }

    public String toString() {
        String str;
        String str2;
        i iVar;
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(MentionUtilsKt.MENTIONS_CHAR);
        sb.append(f.l0(this));
        sb.append('{');
        s.a.a.k j2 = this.k.j();
        if (j2 == this.k) {
            str = "EmptyQueue";
        } else {
            if (j2 instanceof i) {
                str2 = j2.toString();
            } else if (j2 instanceof n) {
                str2 = "ReceiveQueued";
            } else if (j2 instanceof r) {
                str2 = "SendQueued";
            } else {
                str2 = "UNEXPECTED:" + j2;
            }
            s.a.a.k k = this.k.k();
            if (k != j2) {
                StringBuilder V = b.d.b.a.a.V(str2, ",queueSize=");
                Object i = this.k.i();
                Objects.requireNonNull(i, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
                int i2 = 0;
                for (s.a.a.k kVar = (s.a.a.k) i; !d0.z.d.m.areEqual(kVar, iVar); kVar = kVar.j()) {
                    i2++;
                }
                V.append(i2);
                str = V.toString();
                if (k instanceof i) {
                    str = str + ",closedForSend=" + k;
                }
            } else {
                str = str2;
            }
        }
        sb.append(str);
        sb.append('}');
        sb.append(d());
        return sb.toString();
    }
}
