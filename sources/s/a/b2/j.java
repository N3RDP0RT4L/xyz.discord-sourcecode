package s.a.b2;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import d0.z.d.m;
import java.util.concurrent.locks.ReentrantLock;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.internal.UndeliveredElementException;
import s.a.a.t;
/* compiled from: ConflatedChannel.kt */
/* loaded from: classes3.dex */
public class j<E> extends a<E> {
    public final ReentrantLock m = new ReentrantLock();
    public Object n = b.a;

    public j(Function1<? super E, Unit> function1) {
        super(function1);
    }

    @Override // s.a.b2.c
    public String d() {
        StringBuilder R = a.R("(value=");
        R.append(this.n);
        R.append(')');
        return R.toString();
    }

    @Override // s.a.b2.c
    public final boolean h() {
        return false;
    }

    @Override // s.a.b2.c
    public final boolean i() {
        return false;
    }

    @Override // s.a.b2.c
    public Object k(E e) {
        p<E> n;
        ReentrantLock reentrantLock = this.m;
        reentrantLock.lock();
        try {
            i<?> e2 = e();
            if (e2 != null) {
                return e2;
            }
            if (this.n == b.a) {
                do {
                    n = n();
                    if (n != null) {
                        if (n instanceof i) {
                            m.checkNotNull(n);
                            return n;
                        }
                        m.checkNotNull(n);
                    }
                } while (n.d(e, null) == null);
                reentrantLock.unlock();
                m.checkNotNull(n);
                n.c(e);
                m.checkNotNull(n);
                return n.b();
            }
            UndeliveredElementException w = w(e);
            if (w == null) {
                return b.f3806b;
            }
            throw w;
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // s.a.b2.a
    public boolean r(n<? super E> nVar) {
        ReentrantLock reentrantLock = this.m;
        reentrantLock.lock();
        try {
            return super.r(nVar);
        } finally {
            reentrantLock.unlock();
        }
    }

    @Override // s.a.b2.a
    public final boolean s() {
        return false;
    }

    @Override // s.a.b2.a
    public final boolean t() {
        return this.n == b.a;
    }

    /* JADX WARN: Finally extract failed */
    @Override // s.a.b2.a
    public void u(boolean z2) {
        ReentrantLock reentrantLock = this.m;
        reentrantLock.lock();
        try {
            UndeliveredElementException w = w(b.a);
            reentrantLock.unlock();
            super.u(z2);
            if (w != null) {
                throw w;
            }
        } catch (Throwable th) {
            reentrantLock.unlock();
            throw th;
        }
    }

    @Override // s.a.b2.a
    public Object v() {
        ReentrantLock reentrantLock = this.m;
        reentrantLock.lock();
        try {
            Object obj = this.n;
            t tVar = b.a;
            if (obj == tVar) {
                Object e = e();
                if (e == null) {
                    e = b.d;
                }
                return e;
            }
            this.n = tVar;
            return obj;
        } finally {
            reentrantLock.unlock();
        }
    }

    public final UndeliveredElementException w(Object obj) {
        Function1<E, Unit> function1;
        Object obj2 = this.n;
        UndeliveredElementException undeliveredElementException = null;
        if (!(obj2 == b.a || (function1 = this.l) == null)) {
            undeliveredElementException = f.q(function1, obj2, null, 2);
        }
        this.n = obj;
        return undeliveredElementException;
    }
}
