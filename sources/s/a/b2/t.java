package s.a.b2;

import b.i.a.f.e.o.f;
import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.k;
import d0.l;
import kotlin.Unit;
import kotlinx.coroutines.CancellableContinuation;
import s.a.a.k;
import s.a.m;
/* compiled from: AbstractChannel.kt */
/* loaded from: classes3.dex */
public class t<E> extends r {
    public final E m;
    public final CancellableContinuation<Unit> n;

    /* JADX WARN: Multi-variable type inference failed */
    public t(E e, CancellableContinuation<? super Unit> cancellableContinuation) {
        this.m = e;
        this.n = cancellableContinuation;
    }

    @Override // s.a.b2.r
    public void q() {
        this.n.r(m.a);
    }

    @Override // s.a.b2.r
    public E r() {
        return this.m;
    }

    @Override // s.a.b2.r
    public void s(i<?> iVar) {
        CancellableContinuation<Unit> cancellableContinuation = this.n;
        Throwable w = iVar.w();
        k.a aVar = k.j;
        cancellableContinuation.resumeWith(k.m73constructorimpl(l.createFailure(w)));
    }

    @Override // s.a.b2.r
    public s.a.a.t t(k.b bVar) {
        if (this.n.c(Unit.a, null) != null) {
            return m.a;
        }
        return null;
    }

    @Override // s.a.a.k
    public String toString() {
        return getClass().getSimpleName() + MentionUtilsKt.MENTIONS_CHAR + f.l0(this) + '(' + this.m + ')';
    }
}
