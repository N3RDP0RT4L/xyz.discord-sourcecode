package s.a;

import b.d.b.a.a;
import d0.z.d.m;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: CompletionState.kt */
/* loaded from: classes3.dex */
public final class x {
    public final Object a;

    /* renamed from: b  reason: collision with root package name */
    public final Function1<Throwable, Unit> f3820b;

    /* JADX WARN: Multi-variable type inference failed */
    public x(Object obj, Function1<? super Throwable, Unit> function1) {
        this.a = obj;
        this.f3820b = function1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof x)) {
            return false;
        }
        x xVar = (x) obj;
        return m.areEqual(this.a, xVar.a) && m.areEqual(this.f3820b, xVar.f3820b);
    }

    public int hashCode() {
        Object obj = this.a;
        int i = 0;
        int hashCode = (obj != null ? obj.hashCode() : 0) * 31;
        Function1<Throwable, Unit> function1 = this.f3820b;
        if (function1 != null) {
            i = function1.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("CompletedWithCancellation(result=");
        R.append(this.a);
        R.append(", onCancellation=");
        R.append(this.f3820b);
        R.append(")");
        return R.toString();
    }
}
