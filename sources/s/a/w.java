package s.a;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
/* compiled from: CompletionState.kt */
/* loaded from: classes3.dex */
public class w {
    public static final AtomicIntegerFieldUpdater a = AtomicIntegerFieldUpdater.newUpdater(w.class, "_handled");
    public volatile int _handled;

    /* renamed from: b  reason: collision with root package name */
    public final Throwable f3819b;

    public w(Throwable th, boolean z2) {
        this.f3819b = th;
        this._handled = z2 ? 1 : 0;
    }

    public String toString() {
        return getClass().getSimpleName() + '[' + this.f3819b + ']';
    }

    public w(Throwable th, boolean z2, int i) {
        z2 = (i & 2) != 0 ? false : z2;
        this.f3819b = th;
        int i2 = z2 ? 1 : 0;
        int i3 = z2 ? 1 : 0;
        this._handled = i2;
    }
}
