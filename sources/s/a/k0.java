package s.a;

import java.util.Objects;
import kotlinx.coroutines.CoroutineDispatcher;
import s.a.d2.b;
/* compiled from: Dispatchers.kt */
/* loaded from: classes3.dex */
public final class k0 {
    public static final CoroutineDispatcher a;

    /* renamed from: b  reason: collision with root package name */
    public static final CoroutineDispatcher f3814b;

    static {
        a = z.a ? b.p : s.k;
        x1 x1Var = x1.j;
        Objects.requireNonNull(b.p);
        f3814b = b.o;
    }
}
