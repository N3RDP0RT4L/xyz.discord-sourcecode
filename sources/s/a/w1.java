package s.a;

import d0.w.i.a.d;
import kotlin.coroutines.Continuation;
import kotlinx.coroutines.TimeoutCancellationException;
import s.a.a.r;
/* compiled from: Timeout.kt */
/* loaded from: classes3.dex */
public final class w1<U, T extends U> extends r<T> implements Runnable {
    public final long n;

    public w1(long j, Continuation<? super U> continuation) {
        super(((d) continuation).getContext(), continuation);
        this.n = j;
    }

    @Override // s.a.b, s.a.h1
    public String T() {
        return super.T() + "(timeMillis=" + this.n + ')';
    }

    @Override // java.lang.Runnable
    public void run() {
        long j = this.n;
        w(new TimeoutCancellationException("Timed out waiting for " + j + " ms", this));
    }
}
