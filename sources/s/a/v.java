package s.a;

import b.d.b.a.a;
import d0.z.d.m;
import java.util.Objects;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: CancellableContinuationImpl.kt */
/* loaded from: classes3.dex */
public final class v {
    public final Object a;

    /* renamed from: b  reason: collision with root package name */
    public final j f3817b;
    public final Function1<Throwable, Unit> c;
    public final Object d;
    public final Throwable e;

    /* JADX WARN: Multi-variable type inference failed */
    public v(Object obj, j jVar, Function1<? super Throwable, Unit> function1, Object obj2, Throwable th) {
        this.a = obj;
        this.f3817b = jVar;
        this.c = function1;
        this.d = obj2;
        this.e = th;
    }

    public static v a(v vVar, Object obj, j jVar, Function1 function1, Object obj2, Throwable th, int i) {
        Object obj3 = null;
        Object obj4 = (i & 1) != 0 ? vVar.a : null;
        if ((i & 2) != 0) {
            jVar = vVar.f3817b;
        }
        j jVar2 = jVar;
        Function1<Throwable, Unit> function12 = (i & 4) != 0 ? vVar.c : null;
        if ((i & 8) != 0) {
            obj3 = vVar.d;
        }
        Object obj5 = obj3;
        if ((i & 16) != 0) {
            th = vVar.e;
        }
        Objects.requireNonNull(vVar);
        return new v(obj4, jVar2, function12, obj5, th);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof v)) {
            return false;
        }
        v vVar = (v) obj;
        return m.areEqual(this.a, vVar.a) && m.areEqual(this.f3817b, vVar.f3817b) && m.areEqual(this.c, vVar.c) && m.areEqual(this.d, vVar.d) && m.areEqual(this.e, vVar.e);
    }

    public int hashCode() {
        Object obj = this.a;
        int i = 0;
        int hashCode = (obj != null ? obj.hashCode() : 0) * 31;
        j jVar = this.f3817b;
        int hashCode2 = (hashCode + (jVar != null ? jVar.hashCode() : 0)) * 31;
        Function1<Throwable, Unit> function1 = this.c;
        int hashCode3 = (hashCode2 + (function1 != null ? function1.hashCode() : 0)) * 31;
        Object obj2 = this.d;
        int hashCode4 = (hashCode3 + (obj2 != null ? obj2.hashCode() : 0)) * 31;
        Throwable th = this.e;
        if (th != null) {
            i = th.hashCode();
        }
        return hashCode4 + i;
    }

    public String toString() {
        StringBuilder R = a.R("CompletedContinuation(result=");
        R.append(this.a);
        R.append(", cancelHandler=");
        R.append(this.f3817b);
        R.append(", onCancellation=");
        R.append(this.c);
        R.append(", idempotentResume=");
        R.append(this.d);
        R.append(", cancelCause=");
        R.append(this.e);
        R.append(")");
        return R.toString();
    }

    public v(Object obj, j jVar, Function1 function1, Object obj2, Throwable th, int i) {
        jVar = (i & 2) != 0 ? null : jVar;
        function1 = (i & 4) != 0 ? null : function1;
        obj2 = (i & 8) != 0 ? null : obj2;
        th = (i & 16) != 0 ? null : th;
        this.a = obj;
        this.f3817b = jVar;
        this.c = function1;
        this.d = obj2;
        this.e = th;
    }
}
