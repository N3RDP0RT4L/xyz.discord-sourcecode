package s.a;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import com.discord.widgets.chat.input.MentionUtilsKt;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
/* compiled from: CancellableContinuationImpl.kt */
/* loaded from: classes3.dex */
public final class b1 extends j {
    public final Function1<Throwable, Unit> j;

    /* JADX WARN: Multi-variable type inference failed */
    public b1(Function1<? super Throwable, Unit> function1) {
        this.j = function1;
    }

    @Override // s.a.k
    public void a(Throwable th) {
        this.j.invoke(th);
    }

    @Override // kotlin.jvm.functions.Function1
    public Unit invoke(Throwable th) {
        this.j.invoke(th);
        return Unit.a;
    }

    public String toString() {
        StringBuilder R = a.R("InvokeOnCancel[");
        R.append(f.e0(this.j));
        R.append(MentionUtilsKt.MENTIONS_CHAR);
        R.append(f.l0(this));
        R.append(']');
        return R.toString();
    }
}
