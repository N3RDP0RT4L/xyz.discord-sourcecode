package s.a.a;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import d0.z.d.m;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import kotlinx.coroutines.CoroutineDispatcher;
import s.a.j0;
import s.a.q0;
import s.a.v1;
import s.a.x;
/* compiled from: DispatchedContinuation.kt */
/* loaded from: classes3.dex */
public final class g<T> extends j0<T> implements CoroutineStackFrame, Continuation<T> {
    public static final AtomicReferenceFieldUpdater m = AtomicReferenceFieldUpdater.newUpdater(g.class, Object.class, "_reusableCancellableContinuation");
    public volatile Object _reusableCancellableContinuation;
    public Object n = h.a;
    public final CoroutineStackFrame o;
    public final Object p;
    public final CoroutineDispatcher q;
    public final Continuation<T> r;

    /* JADX WARN: Multi-variable type inference failed */
    public g(CoroutineDispatcher coroutineDispatcher, Continuation<? super T> continuation) {
        super(-1);
        this.q = coroutineDispatcher;
        this.r = continuation;
        this.o = (CoroutineStackFrame) (!(continuation instanceof CoroutineStackFrame) ? null : continuation);
        Object fold = getContext().fold(0, a.f3797b);
        m.checkNotNull(fold);
        this.p = fold;
        this._reusableCancellableContinuation = null;
    }

    @Override // s.a.j0
    public void b(Object obj, Throwable th) {
        if (obj instanceof x) {
            ((x) obj).f3820b.invoke(th);
        }
    }

    @Override // s.a.j0
    public Continuation<T> d() {
        return this;
    }

    @Override // kotlin.coroutines.Continuation
    public CoroutineContext getContext() {
        return this.r.getContext();
    }

    @Override // s.a.j0
    public Object m() {
        Object obj = this.n;
        this.n = h.a;
        return obj;
    }

    @Override // kotlin.coroutines.Continuation
    public void resumeWith(Object obj) {
        CoroutineContext context = this.r.getContext();
        Object v1 = f.v1(obj, null);
        if (this.q.isDispatchNeeded(context)) {
            this.n = v1;
            this.l = 0;
            this.q.dispatch(context, this);
            return;
        }
        v1 v1Var = v1.f3818b;
        q0 a = v1.a();
        if (a.N()) {
            this.n = v1;
            this.l = 0;
            a.J(this);
            return;
        }
        a.L(true);
        try {
            CoroutineContext context2 = getContext();
            Object b2 = a.b(context2, this.p);
            this.r.resumeWith(obj);
            a.a(context2, b2);
            do {
            } while (a.R());
        } finally {
            try {
            } finally {
            }
        }
    }

    public String toString() {
        StringBuilder R = a.R("DispatchedContinuation[");
        R.append(this.q);
        R.append(", ");
        R.append(f.s1(this.r));
        R.append(']');
        return R.toString();
    }
}
