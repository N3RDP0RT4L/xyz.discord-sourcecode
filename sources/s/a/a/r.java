package s.a.a;

import b.i.a.f.e.o.f;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.CoroutineStackFrame;
import s.a.b;
/* compiled from: Scopes.kt */
/* loaded from: classes3.dex */
public class r<T> extends b<T> implements CoroutineStackFrame {
    public final Continuation<T> m;

    /* JADX WARN: Multi-variable type inference failed */
    public r(CoroutineContext coroutineContext, Continuation<? super T> continuation) {
        super(coroutineContext, true);
        this.m = continuation;
    }

    @Override // s.a.h1
    public final boolean Q() {
        return true;
    }

    @Override // s.a.b
    public void e0(Object obj) {
        Continuation<T> continuation = this.m;
        continuation.resumeWith(f.X0(obj, continuation));
    }

    @Override // s.a.h1
    public void v(Object obj) {
        h.b(d0.w.h.b.intercepted(this.m), f.X0(obj, this.m), null, 2);
    }
}
