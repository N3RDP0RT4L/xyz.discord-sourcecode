package s.a.a;

import b.d.b.a.a;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScope;
/* compiled from: Scopes.kt */
/* loaded from: classes3.dex */
public final class f implements CoroutineScope {
    public final CoroutineContext j;

    public f(CoroutineContext coroutineContext) {
        this.j = coroutineContext;
    }

    @Override // kotlinx.coroutines.CoroutineScope
    public CoroutineContext getCoroutineContext() {
        return this.j;
    }

    public String toString() {
        StringBuilder R = a.R("CoroutineScope(coroutineContext=");
        R.append(this.j);
        R.append(')');
        return R.toString();
    }
}
