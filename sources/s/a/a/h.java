package s.a.a;

import b.i.a.f.e.o.f;
import d0.k;
import d0.l;
import java.util.concurrent.CancellationException;
import kotlin.Unit;
import kotlin.coroutines.Continuation;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function1;
import kotlinx.coroutines.Job;
import s.a.q0;
import s.a.v1;
import s.a.x;
/* compiled from: DispatchedContinuation.kt */
/* loaded from: classes3.dex */
public final class h {
    public static final t a = new t("UNDEFINED");

    /* renamed from: b */
    public static final t f3799b = new t("REUSABLE_CLAIMED");

    public static final <T> void a(Continuation<? super T> continuation, Object obj, Function1<? super Throwable, Unit> function1) {
        boolean z2;
        if (continuation instanceof g) {
            g gVar = (g) continuation;
            Object v1 = f.v1(obj, function1);
            if (gVar.q.isDispatchNeeded(gVar.getContext())) {
                gVar.n = v1;
                gVar.l = 1;
                gVar.q.dispatch(gVar.getContext(), gVar);
                return;
            }
            v1 v1Var = v1.f3818b;
            q0 a2 = v1.a();
            if (a2.N()) {
                gVar.n = v1;
                gVar.l = 1;
                a2.J(gVar);
                return;
            }
            a2.L(true);
            try {
                Job job = (Job) gVar.getContext().get(Job.h);
                if (job == null || job.a()) {
                    z2 = false;
                } else {
                    CancellationException q = job.q();
                    if (v1 instanceof x) {
                        ((x) v1).f3820b.invoke(q);
                    }
                    k.a aVar = k.j;
                    gVar.resumeWith(k.m73constructorimpl(l.createFailure(q)));
                    z2 = true;
                }
                if (!z2) {
                    CoroutineContext context = gVar.getContext();
                    Object b2 = a.b(context, gVar.p);
                    gVar.r.resumeWith(obj);
                    a.a(context, b2);
                }
                do {
                } while (a2.R());
            } finally {
                try {
                    return;
                } finally {
                }
            }
            return;
        }
        continuation.resumeWith(obj);
    }

    public static /* synthetic */ void b(Continuation continuation, Object obj, Function1 function1, int i) {
        int i2 = i & 2;
        a(continuation, obj, null);
    }
}
