package s.a.a;

import b.i.a.f.e.o.f;
import com.discord.widgets.chat.input.MentionUtilsKt;
/* compiled from: Atomic.kt */
/* loaded from: classes3.dex */
public abstract class p {
    public abstract Object a(Object obj);

    public String toString() {
        return getClass().getSimpleName() + MentionUtilsKt.MENTIONS_CHAR + f.l0(this);
    }
}
