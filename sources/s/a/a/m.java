package s.a.a;

import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: LockFreeTaskQueue.kt */
/* loaded from: classes3.dex */
public final class m<E> {
    public volatile Object _next = null;
    public volatile long _state = 0;
    public final int e;
    public AtomicReferenceArray f;
    public final int g;
    public final boolean h;
    public static final a d = new a(null);
    public static final t c = new t("REMOVE_FROZEN");
    public static final AtomicReferenceFieldUpdater a = AtomicReferenceFieldUpdater.newUpdater(m.class, Object.class, "_next");

    /* renamed from: b  reason: collision with root package name */
    public static final AtomicLongFieldUpdater f3801b = AtomicLongFieldUpdater.newUpdater(m.class, "_state");

    /* compiled from: LockFreeTaskQueue.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* compiled from: LockFreeTaskQueue.kt */
    /* loaded from: classes3.dex */
    public static final class b {
        public final int a;

        public b(int i) {
            this.a = i;
        }
    }

    public m(int i, boolean z2) {
        this.g = i;
        this.h = z2;
        int i2 = i - 1;
        this.e = i2;
        this.f = new AtomicReferenceArray(i);
        boolean z3 = false;
        if (i2 <= 1073741823) {
            if (!((i & i2) == 0 ? true : z3)) {
                throw new IllegalStateException("Check failed.".toString());
            }
            return;
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    /* JADX WARN: Code restructure failed: missing block: B:19:0x0051, code lost:
        return 1;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final int a(E r16) {
        /*
            r15 = this;
            r6 = r15
            r7 = r16
        L3:
            long r2 = r6._state
            r0 = 3458764513820540928(0x3000000000000000, double:1.727233711018889E-77)
            long r0 = r0 & r2
            r8 = 0
            r4 = 1
            int r5 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r5 == 0) goto L18
            r0 = 2305843009213693952(0x2000000000000000, double:1.4916681462400413E-154)
            long r0 = r0 & r2
            int r2 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r2 == 0) goto L17
            r4 = 2
        L17:
            return r4
        L18:
            r0 = 1073741823(0x3fffffff, double:5.304989472E-315)
            long r0 = r0 & r2
            r10 = 0
            long r0 = r0 >> r10
            int r1 = (int) r0
            r11 = 1152921503533105152(0xfffffffc0000000, double:1.2882296003504729E-231)
            long r11 = r11 & r2
            r0 = 30
            long r11 = r11 >> r0
            int r12 = (int) r11
            int r11 = r6.e
            int r5 = r12 + 2
            r5 = r5 & r11
            r13 = r1 & r11
            if (r5 != r13) goto L33
            return r4
        L33:
            boolean r5 = r6.h
            r13 = 1073741823(0x3fffffff, float:1.9999999)
            if (r5 != 0) goto L52
            java.util.concurrent.atomic.AtomicReferenceArray r5 = r6.f
            r14 = r12 & r11
            java.lang.Object r5 = r5.get(r14)
            if (r5 == 0) goto L52
            int r0 = r6.g
            r2 = 1024(0x400, float:1.435E-42)
            if (r0 < r2) goto L51
            int r12 = r12 - r1
            r1 = r12 & r13
            int r0 = r0 >> 1
            if (r1 <= r0) goto L3
        L51:
            return r4
        L52:
            int r1 = r12 + 1
            r1 = r1 & r13
            java.util.concurrent.atomic.AtomicLongFieldUpdater r4 = s.a.a.m.f3801b
            r13 = -1152921503533105153(0xf00000003fffffff, double:-3.1050369248997324E231)
            long r13 = r13 & r2
            long r8 = (long) r1
            long r0 = r8 << r0
            long r8 = r13 | r0
            r0 = r4
            r1 = r15
            r4 = r8
            boolean r0 = r0.compareAndSet(r1, r2, r4)
            if (r0 == 0) goto L3
            java.util.concurrent.atomic.AtomicReferenceArray r0 = r6.f
            r1 = r12 & r11
            r0.set(r1, r7)
            r0 = r6
        L73:
            long r1 = r0._state
            r3 = 1152921504606846976(0x1000000000000000, double:1.2882297539194267E-231)
            long r1 = r1 & r3
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 != 0) goto L7f
            goto La3
        L7f:
            s.a.a.m r0 = r0.d()
            java.util.concurrent.atomic.AtomicReferenceArray r1 = r0.f
            int r2 = r0.e
            r2 = r2 & r12
            java.lang.Object r1 = r1.get(r2)
            boolean r2 = r1 instanceof s.a.a.m.b
            if (r2 == 0) goto L9f
            s.a.a.m$b r1 = (s.a.a.m.b) r1
            int r1 = r1.a
            if (r1 != r12) goto L9f
            java.util.concurrent.atomic.AtomicReferenceArray r1 = r0.f
            int r2 = r0.e
            r2 = r2 & r12
            r1.set(r2, r7)
            goto La0
        L9f:
            r0 = 0
        La0:
            if (r0 == 0) goto La3
            goto L73
        La3:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.a.m.a(java.lang.Object):int");
    }

    public final boolean b() {
        long j;
        do {
            j = this._state;
            if ((j & 2305843009213693952L) != 0) {
                return true;
            }
            if ((1152921504606846976L & j) != 0) {
                return false;
            }
        } while (!f3801b.compareAndSet(this, j, j | 2305843009213693952L));
        return true;
    }

    public final boolean c() {
        long j = this._state;
        return ((int) ((1073741823 & j) >> 0)) == ((int) ((j & 1152921503533105152L) >> 30));
    }

    /* JADX WARN: Multi-variable type inference failed */
    public final m<E> d() {
        long j;
        while (true) {
            j = this._state;
            if ((j & 1152921504606846976L) == 0) {
                long j2 = j | 1152921504606846976L;
                if (f3801b.compareAndSet(this, j, j2)) {
                    j = j2;
                    break;
                }
            } else {
                break;
            }
        }
        while (true) {
            m<E> mVar = (m) this._next;
            if (mVar != null) {
                return mVar;
            }
            AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = a;
            m mVar2 = new m(this.g * 2, this.h);
            int i = (int) ((1073741823 & j) >> 0);
            int i2 = (int) ((1152921503533105152L & j) >> 30);
            while (true) {
                int i3 = this.e;
                int i4 = i & i3;
                if (i4 != (i3 & i2)) {
                    Object obj = this.f.get(i4);
                    if (obj == null) {
                        obj = new b(i);
                    }
                    mVar2.f.set(mVar2.e & i, obj);
                    i++;
                }
            }
            mVar2._state = (-1152921504606846977L) & j;
            atomicReferenceFieldUpdater.compareAndSet(this, null, mVar2);
        }
    }

    public final Object e() {
        while (true) {
            long j = this._state;
            if ((j & 1152921504606846976L) != 0) {
                return c;
            }
            int i = (int) ((j & 1073741823) >> 0);
            int i2 = this.e;
            int i3 = ((int) ((1152921503533105152L & j) >> 30)) & i2;
            int i4 = i2 & i;
            if (i3 == i4) {
                return null;
            }
            Object obj = this.f.get(i4);
            if (obj == null) {
                if (this.h) {
                    return null;
                }
            } else if (obj instanceof b) {
                return null;
            } else {
                long j2 = ((i + 1) & 1073741823) << 0;
                if (f3801b.compareAndSet(this, j, (j & (-1073741824)) | j2)) {
                    this.f.set(this.e & i, null);
                    return obj;
                } else if (this.h) {
                    m<E> mVar = this;
                    while (true) {
                        long j3 = mVar._state;
                        int i5 = (int) ((j3 & 1073741823) >> 0);
                        if ((j3 & 1152921504606846976L) != 0) {
                            mVar = mVar.d();
                        } else {
                            if (f3801b.compareAndSet(mVar, j3, (j3 & (-1073741824)) | j2)) {
                                mVar.f.set(mVar.e & i5, null);
                                mVar = null;
                            } else {
                                continue;
                            }
                        }
                        if (mVar == null) {
                            return obj;
                        }
                    }
                }
            }
        }
    }
}
