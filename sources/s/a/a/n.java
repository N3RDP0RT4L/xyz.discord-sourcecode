package s.a.a;

import b.i.a.f.e.o.f;
import d0.f0.q;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.internal.MainDispatcherFactory;
import s.a.l1;
/* compiled from: MainDispatchers.kt */
/* loaded from: classes3.dex */
public final class n {
    public static final boolean a;

    /* renamed from: b  reason: collision with root package name */
    public static final l1 f3802b;

    static {
        n nVar = new n();
        String k1 = f.k1("kotlinx.coroutines.fast.service.loader");
        a = k1 != null ? Boolean.parseBoolean(k1) : true;
        f3802b = nVar.a();
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v5 */
    public final l1 a() {
        MainDispatcherFactory mainDispatcherFactory;
        l1 createDispatcher;
        List<? extends MainDispatcherFactory> list = q.toList(d0.f0.n.asSequence(a.b()));
        Iterator it = list.iterator();
        if (!it.hasNext()) {
            mainDispatcherFactory = null;
        } else {
            Object next = it.next();
            if (it.hasNext()) {
                int loadPriority = ((MainDispatcherFactory) next).getLoadPriority();
                do {
                    Object next2 = it.next();
                    int loadPriority2 = ((MainDispatcherFactory) next2).getLoadPriority();
                    if (loadPriority < loadPriority2) {
                        next = next2;
                        loadPriority = loadPriority2;
                    }
                } while (it.hasNext());
            }
            mainDispatcherFactory = next;
        }
        MainDispatcherFactory mainDispatcherFactory2 = mainDispatcherFactory;
        if (mainDispatcherFactory2 != null && (createDispatcher = mainDispatcherFactory2.createDispatcher(list)) != null) {
            return createDispatcher;
        }
        throw new IllegalStateException("Module with the Main dispatcher is missing. Add dependency providing the Main dispatcher, e.g. 'kotlinx-coroutines-android' and ensure it has the same version as 'kotlinx-coroutines-core'");
    }
}
