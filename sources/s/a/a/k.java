package s.a.a;

import com.discord.widgets.chat.input.MentionUtilsKt;
import d0.z.d.m;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;
/* compiled from: LockFreeLinkedList.kt */
/* loaded from: classes3.dex */
public class k {
    public static final AtomicReferenceFieldUpdater j = AtomicReferenceFieldUpdater.newUpdater(k.class, Object.class, "_next");
    public static final AtomicReferenceFieldUpdater k = AtomicReferenceFieldUpdater.newUpdater(k.class, Object.class, "_prev");
    public static final AtomicReferenceFieldUpdater l = AtomicReferenceFieldUpdater.newUpdater(k.class, Object.class, "_removedRef");
    public volatile Object _next = this;
    public volatile Object _prev = this;
    public volatile Object _removedRef = null;

    /* compiled from: LockFreeLinkedList.kt */
    /* loaded from: classes3.dex */
    public static abstract class a extends d<k> {

        /* renamed from: b  reason: collision with root package name */
        public k f3800b;
        public final k c;

        public a(k kVar) {
            this.c = kVar;
        }

        @Override // s.a.a.d
        public void b(k kVar, Object obj) {
            k kVar2 = kVar;
            boolean z2 = obj == null;
            k kVar3 = z2 ? this.c : this.f3800b;
            if (kVar3 != null && k.j.compareAndSet(kVar2, this, kVar3) && z2) {
                k kVar4 = this.c;
                k kVar5 = this.f3800b;
                m.checkNotNull(kVar5);
                kVar4.g(kVar5);
            }
        }
    }

    /* compiled from: LockFreeLinkedList.kt */
    /* loaded from: classes3.dex */
    public static final class b extends p {
    }

    public final boolean e(k kVar, k kVar2) {
        k.lazySet(kVar, this);
        AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = j;
        atomicReferenceFieldUpdater.lazySet(kVar, kVar2);
        if (!atomicReferenceFieldUpdater.compareAndSet(this, kVar2, kVar)) {
            return false;
        }
        kVar.g(kVar2);
        return true;
    }

    /* JADX WARN: Code restructure failed: missing block: B:23:0x003c, code lost:
        if (s.a.a.k.j.compareAndSet(r2, r1, ((s.a.a.q) r3).a) != false) goto L24;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final s.a.a.k f(s.a.a.p r7) {
        /*
            r6 = this;
        L0:
            java.lang.Object r7 = r6._prev
            s.a.a.k r7 = (s.a.a.k) r7
            r0 = 0
            r1 = r7
        L6:
            r2 = r0
        L7:
            java.lang.Object r3 = r1._next
            if (r3 != r6) goto L18
            if (r7 != r1) goto Le
            return r1
        Le:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r0 = s.a.a.k.k
            boolean r7 = r0.compareAndSet(r6, r7, r1)
            if (r7 != 0) goto L17
            goto L0
        L17:
            return r1
        L18:
            boolean r4 = r6.m()
            if (r4 == 0) goto L1f
            return r0
        L1f:
            if (r3 != 0) goto L22
            return r1
        L22:
            boolean r4 = r3 instanceof s.a.a.p
            if (r4 == 0) goto L2c
            s.a.a.p r3 = (s.a.a.p) r3
            r3.a(r1)
            goto L0
        L2c:
            boolean r4 = r3 instanceof s.a.a.q
            if (r4 == 0) goto L46
            if (r2 == 0) goto L41
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r4 = s.a.a.k.j
            s.a.a.q r3 = (s.a.a.q) r3
            s.a.a.k r3 = r3.a
            boolean r1 = r4.compareAndSet(r2, r1, r3)
            if (r1 != 0) goto L3f
            goto L0
        L3f:
            r1 = r2
            goto L6
        L41:
            java.lang.Object r1 = r1._prev
            s.a.a.k r1 = (s.a.a.k) r1
            goto L7
        L46:
        */
        //  java.lang.String r2 = "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */"
        /*
            java.util.Objects.requireNonNull(r3, r2)
            r2 = r3
            s.a.a.k r2 = (s.a.a.k) r2
            r5 = r2
            r2 = r1
            r1 = r5
            goto L7
        */
        throw new UnsupportedOperationException("Method not decompiled: s.a.a.k.f(s.a.a.p):s.a.a.k");
    }

    public final void g(k kVar) {
        k kVar2;
        do {
            kVar2 = (k) kVar._prev;
            if (i() != kVar) {
                return;
            }
        } while (!k.compareAndSet(kVar, kVar2, this));
        if (m()) {
            kVar.f(null);
        }
    }

    public final Object i() {
        while (true) {
            Object obj = this._next;
            if (!(obj instanceof p)) {
                return obj;
            }
            ((p) obj).a(this);
        }
    }

    public final k j() {
        k kVar;
        Object i = i();
        q qVar = (q) (!(i instanceof q) ? null : i);
        if (qVar != null && (kVar = qVar.a) != null) {
            return kVar;
        }
        Objects.requireNonNull(i, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
        return (k) i;
    }

    public final k k() {
        k f = f(null);
        if (f == null) {
            Object obj = this._prev;
            while (true) {
                f = (k) obj;
                if (!f.m()) {
                    break;
                }
                obj = f._prev;
            }
        }
        return f;
    }

    public final void l() {
        k kVar = this;
        while (true) {
            Object i = kVar.i();
            if (!(i instanceof q)) {
                kVar.f(null);
                return;
            }
            kVar = ((q) i).a;
        }
    }

    public boolean m() {
        return i() instanceof q;
    }

    public boolean n() {
        return o() == null;
    }

    public final k o() {
        Object i;
        k kVar;
        q qVar;
        do {
            i = i();
            if (i instanceof q) {
                return ((q) i).a;
            }
            if (i == this) {
                return (k) i;
            }
            Objects.requireNonNull(i, "null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
            kVar = (k) i;
            qVar = (q) kVar._removedRef;
            if (qVar == null) {
                qVar = new q(kVar);
                l.lazySet(kVar, qVar);
            }
        } while (!j.compareAndSet(this, i, qVar));
        kVar.f(null);
        return null;
    }

    public final int p(k kVar, k kVar2, a aVar) {
        k.lazySet(kVar, this);
        AtomicReferenceFieldUpdater atomicReferenceFieldUpdater = j;
        atomicReferenceFieldUpdater.lazySet(kVar, kVar2);
        aVar.f3800b = kVar2;
        if (!atomicReferenceFieldUpdater.compareAndSet(this, kVar2, aVar)) {
            return 0;
        }
        return aVar.a(this) == null ? 1 : 2;
    }

    public String toString() {
        return getClass().getSimpleName() + MentionUtilsKt.MENTIONS_CHAR + Integer.toHexString(System.identityHashCode(this));
    }
}
