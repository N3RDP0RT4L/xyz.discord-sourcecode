package s.a.a;

import d0.z.d.m;
import d0.z.d.o;
import java.util.Objects;
import kotlin.coroutines.CoroutineContext;
import kotlin.jvm.functions.Function2;
import s.a.u1;
/* compiled from: ThreadContext.kt */
/* loaded from: classes3.dex */
public final class a {
    public static final t a = new t("ZERO");

    /* renamed from: b  reason: collision with root package name */
    public static final Function2<Object, CoroutineContext.Element, Object> f3797b = b.j;
    public static final Function2<u1<?>, CoroutineContext.Element, u1<?>> c = c.j;
    public static final Function2<x, CoroutineContext.Element, x> d = C0423a.k;
    public static final Function2<x, CoroutineContext.Element, x> e = C0423a.j;

    /* compiled from: kotlin-style lambda group */
    /* renamed from: s.a.a.a$a  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0423a extends o implements Function2<x, CoroutineContext.Element, x> {
        public static final C0423a j = new C0423a(0);
        public static final C0423a k = new C0423a(1);
        public final /* synthetic */ int l;

        /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
        public C0423a(int i) {
            super(2);
            this.l = i;
        }

        @Override // kotlin.jvm.functions.Function2
        public final x invoke(x xVar, CoroutineContext.Element element) {
            int i = this.l;
            if (i == 0) {
                x xVar2 = xVar;
                CoroutineContext.Element element2 = element;
                if (element2 instanceof u1) {
                    CoroutineContext coroutineContext = xVar2.c;
                    Object[] objArr = xVar2.a;
                    int i2 = xVar2.f3804b;
                    xVar2.f3804b = i2 + 1;
                    ((u1) element2).y(coroutineContext, objArr[i2]);
                }
                return xVar2;
            } else if (i == 1) {
                x xVar3 = xVar;
                CoroutineContext.Element element3 = element;
                if (element3 instanceof u1) {
                    Object C = ((u1) element3).C(xVar3.c);
                    Object[] objArr2 = xVar3.a;
                    int i3 = xVar3.f3804b;
                    xVar3.f3804b = i3 + 1;
                    objArr2[i3] = C;
                }
                return xVar3;
            } else {
                throw null;
            }
        }
    }

    /* compiled from: ThreadContext.kt */
    /* loaded from: classes3.dex */
    public static final class b extends o implements Function2<Object, CoroutineContext.Element, Object> {
        public static final b j = new b();

        public b() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        public Object invoke(Object obj, CoroutineContext.Element element) {
            CoroutineContext.Element element2 = element;
            if (!(element2 instanceof u1)) {
                return obj;
            }
            if (!(obj instanceof Integer)) {
                obj = null;
            }
            Integer num = (Integer) obj;
            int intValue = num != null ? num.intValue() : 1;
            return intValue == 0 ? element2 : Integer.valueOf(intValue + 1);
        }
    }

    /* compiled from: ThreadContext.kt */
    /* loaded from: classes3.dex */
    public static final class c extends o implements Function2<u1<?>, CoroutineContext.Element, u1<?>> {
        public static final c j = new c();

        public c() {
            super(2);
        }

        @Override // kotlin.jvm.functions.Function2
        public u1<?> invoke(u1<?> u1Var, CoroutineContext.Element element) {
            u1<?> u1Var2 = u1Var;
            CoroutineContext.Element element2 = element;
            if (u1Var2 != null) {
                return u1Var2;
            }
            if (!(element2 instanceof u1)) {
                element2 = null;
            }
            return (u1) element2;
        }
    }

    public static final void a(CoroutineContext coroutineContext, Object obj) {
        if (obj != a) {
            if (obj instanceof x) {
                ((x) obj).f3804b = 0;
                coroutineContext.fold(obj, e);
                return;
            }
            Object fold = coroutineContext.fold(null, c);
            Objects.requireNonNull(fold, "null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
            ((u1) fold).y(coroutineContext, obj);
        }
    }

    public static final Object b(CoroutineContext coroutineContext, Object obj) {
        if (obj == null) {
            obj = coroutineContext.fold(0, f3797b);
            m.checkNotNull(obj);
        }
        if (obj == 0) {
            return a;
        }
        if (obj instanceof Integer) {
            return coroutineContext.fold(new x(coroutineContext, ((Number) obj).intValue()), d);
        }
        Objects.requireNonNull(obj, "null cannot be cast to non-null type kotlinx.coroutines.ThreadContextElement<kotlin.Any?>");
        return ((u1) obj).C(coroutineContext);
    }
}
