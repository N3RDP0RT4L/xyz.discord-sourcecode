package s.a.a;

import kotlin.coroutines.CoroutineContext;
/* compiled from: ThreadContext.kt */
/* loaded from: classes3.dex */
public final class x {
    public Object[] a;

    /* renamed from: b  reason: collision with root package name */
    public int f3804b;
    public final CoroutineContext c;

    public x(CoroutineContext coroutineContext, int i) {
        this.c = coroutineContext;
        this.a = new Object[i];
    }
}
