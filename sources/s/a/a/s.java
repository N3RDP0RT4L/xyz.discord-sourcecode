package s.a.a;

import d0.k;
import d0.l;
/* compiled from: StackTraceRecovery.kt */
/* loaded from: classes3.dex */
public final class s {
    public static final String a;

    /* renamed from: b  reason: collision with root package name */
    public static final String f3803b;

    static {
        Object obj;
        Object obj2;
        try {
            k.a aVar = k.j;
            obj = k.m73constructorimpl(Class.forName("d0.w.i.a.a").getCanonicalName());
        } catch (Throwable th) {
            k.a aVar2 = k.j;
            obj = k.m73constructorimpl(l.createFailure(th));
        }
        if (k.m75exceptionOrNullimpl(obj) != null) {
            obj = "kotlin.coroutines.jvm.internal.BaseContinuationImpl";
        }
        a = (String) obj;
        try {
            k.a aVar3 = k.j;
            obj2 = k.m73constructorimpl(Class.forName("s.a.a.s").getCanonicalName());
        } catch (Throwable th2) {
            k.a aVar4 = k.j;
            obj2 = k.m73constructorimpl(l.createFailure(th2));
        }
        if (k.m75exceptionOrNullimpl(obj2) != null) {
            obj2 = "kotlinx.coroutines.internal.StackTraceRecoveryKt";
        }
        f3803b = (String) obj2;
    }
}
