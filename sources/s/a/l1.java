package s.a;

import b.i.a.f.e.o.f;
import com.discord.widgets.chat.input.MentionUtilsKt;
import kotlinx.coroutines.CoroutineDispatcher;
import s.a.a.n;
/* compiled from: MainCoroutineDispatcher.kt */
/* loaded from: classes3.dex */
public abstract class l1 extends CoroutineDispatcher {
    public abstract l1 H();

    public final String I() {
        l1 l1Var;
        CoroutineDispatcher coroutineDispatcher = k0.a;
        l1 l1Var2 = n.f3802b;
        if (this == l1Var2) {
            return "Dispatchers.Main";
        }
        try {
            l1Var = l1Var2.H();
        } catch (UnsupportedOperationException unused) {
            l1Var = null;
        }
        if (this == l1Var) {
            return "Dispatchers.Main.immediate";
        }
        return null;
    }

    @Override // kotlinx.coroutines.CoroutineDispatcher
    public String toString() {
        String I = I();
        if (I != null) {
            return I;
        }
        return getClass().getSimpleName() + MentionUtilsKt.MENTIONS_CHAR + f.l0(this);
    }
}
