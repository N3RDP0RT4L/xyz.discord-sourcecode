package s.a;

import kotlin.coroutines.CoroutineContext;
/* compiled from: ThreadContextElement.kt */
/* loaded from: classes3.dex */
public interface u1<S> extends CoroutineContext.Element {
    S C(CoroutineContext coroutineContext);

    void y(CoroutineContext coroutineContext, S s2);
}
