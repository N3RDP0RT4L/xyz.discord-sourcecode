package s.a;

import b.i.a.f.e.o.f;
import kotlin.Unit;
import kotlin.coroutines.CoroutineContext;
/* compiled from: Builders.common.kt */
/* loaded from: classes3.dex */
public class s1 extends b<Unit> {
    public s1(CoroutineContext coroutineContext, boolean z2) {
        super(coroutineContext, z2);
    }

    @Override // s.a.h1
    public boolean N(Throwable th) {
        f.u0(this.k, th);
        return true;
    }
}
