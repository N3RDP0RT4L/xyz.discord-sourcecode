package s.a;

import d0.z.d.m;
import java.util.concurrent.locks.LockSupport;
import kotlin.coroutines.CoroutineContext;
/* compiled from: Builders.kt */
/* loaded from: classes3.dex */
public final class f<T> extends b<T> {
    public final Thread m;
    public final q0 n;

    public f(CoroutineContext coroutineContext, Thread thread, q0 q0Var) {
        super(coroutineContext, true);
        this.m = thread;
        this.n = q0Var;
    }

    @Override // s.a.h1
    public void v(Object obj) {
        if (!m.areEqual(Thread.currentThread(), this.m)) {
            LockSupport.unpark(this.m);
        }
    }
}
