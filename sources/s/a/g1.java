package s.a;

import java.util.Objects;
import kotlinx.coroutines.Job;
/* compiled from: JobSupport.kt */
/* loaded from: classes3.dex */
public abstract class g1<J extends Job> extends y implements m0, z0 {
    public final J m;

    public g1(J j) {
        this.m = j;
    }

    @Override // s.a.z0
    public boolean a() {
        return true;
    }

    @Override // s.a.m0
    public void dispose() {
        Object M;
        J j = this.m;
        Objects.requireNonNull(j, "null cannot be cast to non-null type kotlinx.coroutines.JobSupport");
        h1 h1Var = (h1) j;
        do {
            M = h1Var.M();
            if (M instanceof g1) {
                if (M != this) {
                    return;
                }
            } else if ((M instanceof z0) && ((z0) M).getList() != null) {
                n();
                return;
            } else {
                return;
            }
        } while (!h1.j.compareAndSet(h1Var, M, i1.g));
    }

    @Override // s.a.z0
    public m1 getList() {
        return null;
    }
}
