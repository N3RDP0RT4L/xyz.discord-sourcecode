package s.a;

import d0.w.f;
import kotlin.coroutines.CoroutineContext;
import kotlinx.coroutines.CoroutineScope;
/* compiled from: CoroutineScope.kt */
/* loaded from: classes3.dex */
public final class x0 implements CoroutineScope {
    public static final x0 j = new x0();

    @Override // kotlinx.coroutines.CoroutineScope
    public CoroutineContext getCoroutineContext() {
        return f.j;
    }
}
