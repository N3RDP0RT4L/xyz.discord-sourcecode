package s.a;

import kotlin.coroutines.CoroutineContext;
/* compiled from: Builders.common.kt */
/* loaded from: classes3.dex */
public class g0<T> extends b<T> implements f0<T> {
    public g0(CoroutineContext coroutineContext, boolean z2) {
        super(coroutineContext, z2);
    }

    @Override // s.a.f0
    public T d() {
        Object M = M();
        if (!(!(M instanceof z0))) {
            throw new IllegalStateException("This job has not completed yet".toString());
        } else if (!(M instanceof w)) {
            return (T) i1.a(M);
        } else {
            throw ((w) M).f3819b;
        }
    }
}
