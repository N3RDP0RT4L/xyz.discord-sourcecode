package org.objectweb.asm.tree.analysis;
/* loaded from: xyz.discord_v112014.apk:org/objectweb/asm/tree/analysis/Value.SCL.lombok */
public interface Value {
    int getSize();
}
