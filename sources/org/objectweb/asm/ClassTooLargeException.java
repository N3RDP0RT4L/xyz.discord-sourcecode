package org.objectweb.asm;
/* JADX WARN: Classes with same name are omitted:
  classes3.dex
 */
/* loaded from: xyz.discord_v112014.apk:org/objectweb/asm/ClassTooLargeException.SCL.lombok */
public final class ClassTooLargeException extends IndexOutOfBoundsException {
    private static final long serialVersionUID = 160715609518896765L;
    private final String className;
    private final int constantPoolCount;

    public ClassTooLargeException(String className, int constantPoolCount) {
        super("Class too large: " + className);
        this.className = className;
        this.constantPoolCount = constantPoolCount;
    }

    public String getClassName() {
        return this.className;
    }

    public int getConstantPoolCount() {
        return this.constantPoolCount;
    }
}
