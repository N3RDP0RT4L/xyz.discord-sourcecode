package org.webrtc;

import b.d.b.a.a;
/* loaded from: classes3.dex */
public class StatsReport {

    /* renamed from: id  reason: collision with root package name */
    public final String f3794id;
    public final double timestamp;
    public final String type;
    public final Value[] values;

    /* loaded from: classes3.dex */
    public static class Value {
        public final String name;
        public final String value;

        @CalledByNative("Value")
        public Value(String str, String str2) {
            this.name = str;
            this.value = str2;
        }

        public String toString() {
            StringBuilder R = a.R("[");
            R.append(this.name);
            R.append(": ");
            return a.H(R, this.value, "]");
        }
    }

    @CalledByNative
    public StatsReport(String str, String str2, double d, Value[] valueArr) {
        this.f3794id = str;
        this.type = str2;
        this.timestamp = d;
        this.values = valueArr;
    }

    public String toString() {
        StringBuilder R = a.R("id: ");
        R.append(this.f3794id);
        R.append(", type: ");
        R.append(this.type);
        R.append(", timestamp: ");
        R.append(this.timestamp);
        R.append(", values: ");
        int i = 0;
        while (true) {
            Value[] valueArr = this.values;
            if (i >= valueArr.length) {
                return R.toString();
            }
            R.append(valueArr[i].toString());
            R.append(", ");
            i++;
        }
    }
}
