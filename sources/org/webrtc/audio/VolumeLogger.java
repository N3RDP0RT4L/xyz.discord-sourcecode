package org.webrtc.audio;

import android.media.AudioManager;
import androidx.annotation.Nullable;
import b.d.b.a.a;
import java.util.Timer;
import java.util.TimerTask;
import org.webrtc.Logging;
/* loaded from: classes3.dex */
public class VolumeLogger {
    private static final String TAG = "VolumeLogger";
    private static final String THREAD_NAME = "WebRtcVolumeLevelLoggerThread";
    private static final int TIMER_PERIOD_IN_SECONDS = 30;
    private final AudioManager audioManager;
    @Nullable
    private Timer timer;

    /* loaded from: classes3.dex */
    public class LogVolumeTask extends TimerTask {
        private final int maxRingVolume;
        private final int maxVoiceCallVolume;

        public LogVolumeTask(int i, int i2) {
            this.maxRingVolume = i;
            this.maxVoiceCallVolume = i2;
        }

        @Override // java.util.TimerTask, java.lang.Runnable
        public void run() {
            int mode = VolumeLogger.this.audioManager.getMode();
            if (mode == 1) {
                StringBuilder R = a.R("STREAM_RING stream volume: ");
                R.append(VolumeLogger.this.audioManager.getStreamVolume(2));
                R.append(" (max=");
                R.append(this.maxRingVolume);
                R.append(")");
                Logging.d(VolumeLogger.TAG, R.toString());
            } else if (mode == 3) {
                StringBuilder R2 = a.R("VOICE_CALL stream volume: ");
                R2.append(VolumeLogger.this.audioManager.getStreamVolume(0));
                R2.append(" (max=");
                R2.append(this.maxVoiceCallVolume);
                R2.append(")");
                Logging.d(VolumeLogger.TAG, R2.toString());
            }
        }
    }

    public VolumeLogger(AudioManager audioManager) {
        this.audioManager = audioManager;
    }

    public void start() {
        StringBuilder R = a.R("start");
        R.append(WebRtcAudioUtils.getThreadInfo());
        Logging.d(TAG, R.toString());
        if (this.timer == null) {
            StringBuilder R2 = a.R("audio mode is: ");
            R2.append(WebRtcAudioUtils.modeToString(this.audioManager.getMode()));
            Logging.d(TAG, R2.toString());
            Timer timer = new Timer(THREAD_NAME);
            this.timer = timer;
            timer.schedule(new LogVolumeTask(this.audioManager.getStreamMaxVolume(2), this.audioManager.getStreamMaxVolume(0)), 0L, 30000L);
        }
    }

    public void stop() {
        StringBuilder R = a.R("stop");
        R.append(WebRtcAudioUtils.getThreadInfo());
        Logging.d(TAG, R.toString());
        Timer timer = this.timer;
        if (timer != null) {
            timer.cancel();
            this.timer = null;
        }
    }
}
