package org.webrtc;

import b.d.b.a.a;
import java.util.Map;
/* loaded from: classes3.dex */
public class RTCStats {

    /* renamed from: id  reason: collision with root package name */
    private final String f3792id;
    private final Map<String, Object> members;
    private final long timestampUs;
    private final String type;

    public RTCStats(long j, String str, String str2, Map<String, Object> map) {
        this.timestampUs = j;
        this.type = str;
        this.f3792id = str2;
        this.members = map;
    }

    private static void appendValue(StringBuilder sb, Object obj) {
        if (obj instanceof Object[]) {
            Object[] objArr = (Object[]) obj;
            sb.append('[');
            for (int i = 0; i < objArr.length; i++) {
                if (i != 0) {
                    sb.append(", ");
                }
                appendValue(sb, objArr[i]);
            }
            sb.append(']');
        } else if (obj instanceof String) {
            sb.append('\"');
            sb.append(obj);
            sb.append('\"');
        } else {
            sb.append(obj);
        }
    }

    @CalledByNative
    public static RTCStats create(long j, String str, String str2, Map map) {
        return new RTCStats(j, str, str2, map);
    }

    public String getId() {
        return this.f3792id;
    }

    public Map<String, Object> getMembers() {
        return this.members;
    }

    public double getTimestampUs() {
        return this.timestampUs;
    }

    public String getType() {
        return this.type;
    }

    public String toString() {
        StringBuilder R = a.R("{ timestampUs: ");
        R.append(this.timestampUs);
        R.append(", type: ");
        R.append(this.type);
        R.append(", id: ");
        R.append(this.f3792id);
        for (Map.Entry<String, Object> entry : this.members.entrySet()) {
            R.append(", ");
            R.append(entry.getKey());
            R.append(": ");
            appendValue(R, entry.getValue());
        }
        R.append(" }");
        return R.toString();
    }
}
