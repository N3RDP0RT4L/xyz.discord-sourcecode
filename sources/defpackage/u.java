package defpackage;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import d0.z.d.m;
import java.util.Objects;
/* compiled from: GifStaggeredGridItemDecoration.kt */
/* renamed from: u  reason: default package */
/* loaded from: classes3.dex */
public final class u extends RecyclerView.ItemDecoration {
    public final int a;

    /* renamed from: b  reason: collision with root package name */
    public final int f3821b;

    public u(int i, int i2) {
        this.a = i;
        this.f3821b = i2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        m.checkNotNullParameter(rect, "outRect");
        m.checkNotNullParameter(view, "view");
        m.checkNotNullParameter(recyclerView, "parent");
        m.checkNotNullParameter(state, "state");
        super.getItemOffsets(rect, view, recyclerView, state);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        Objects.requireNonNull(layoutParams, "null cannot be cast to non-null type androidx.recyclerview.widget.StaggeredGridLayoutManager.LayoutParams");
        boolean z2 = true;
        int i = 0;
        if (((StaggeredGridLayoutManager.LayoutParams) layoutParams).getSpanIndex() != this.f3821b - 1) {
            z2 = false;
        }
        if (!z2) {
            i = this.a;
        }
        rect.set(rect.left, rect.top, i, rect.bottom);
    }
}
