package defpackage;

import android.view.View;
import com.discord.views.TernaryCheckBox;
/* compiled from: java-style lambda group */
/* renamed from: c  reason: default package */
/* loaded from: classes.dex */
public final class c implements View.OnClickListener {
    public final /* synthetic */ int j;
    public final /* synthetic */ Object k;

    public c(int i, Object obj) {
        this.j = i;
        this.k = obj;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        int i = this.j;
        if (i == 0) {
            TernaryCheckBox.a aVar = TernaryCheckBox.j;
            ((TernaryCheckBox) this.k).setSwitchStatus(1);
        } else if (i == 1) {
            TernaryCheckBox.a aVar2 = TernaryCheckBox.j;
            ((TernaryCheckBox) this.k).setSwitchStatus(-1);
        } else if (i == 2) {
            TernaryCheckBox.a aVar3 = TernaryCheckBox.j;
            ((TernaryCheckBox) this.k).setSwitchStatus(0);
        } else {
            throw null;
        }
    }
}
