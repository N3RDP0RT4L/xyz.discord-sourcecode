package defpackage;

import kotlin.jvm.functions.Function2;
import rx.functions.Func2;
/* compiled from: WidgetFriendsAddUserRequestsModel.kt */
/* renamed from: w  reason: default package */
/* loaded from: classes3.dex */
public final class w implements Func2 {
    public final /* synthetic */ Function2 j;

    public w(Function2 function2) {
        this.j = function2;
    }

    @Override // rx.functions.Func2
    public final /* synthetic */ Object call(Object obj, Object obj2) {
        return this.j.invoke(obj, obj2);
    }
}
