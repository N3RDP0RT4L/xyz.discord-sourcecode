package defpackage;

import android.view.View;
import b.a.d.m;
import b.a.y.s;
import b.a.y.t;
import com.discord.app.AppActivity;
import com.discord.stores.StoreInviteSettings;
import com.discord.stores.StoreStream;
import com.discord.utilities.intent.IntentUtils;
import com.discord.utilities.rx.ObservableExtensionsKt;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$1;
import com.discord.utilities.rx.ObservableExtensionsKt$appSubscribe$2;
import com.discord.views.OverlayMenuView;
import java.util.Objects;
import xyz.discord.R;
/* compiled from: java-style lambda group */
/* renamed from: f  reason: default package */
/* loaded from: classes.dex */
public final class f implements View.OnClickListener {
    public final /* synthetic */ int j;
    public final /* synthetic */ Object k;
    public final /* synthetic */ Object l;

    public f(int i, Object obj, Object obj2) {
        this.j = i;
        this.k = obj;
        this.l = obj2;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        int i = this.j;
        if (i == 0) {
            OverlayMenuView overlayMenuView = (OverlayMenuView) this.k;
            Long valueOf = Long.valueOf(((OverlayMenuView.a) this.l).c.getChannel().h());
            int i2 = OverlayMenuView.j;
            Objects.requireNonNull(overlayMenuView);
            if (valueOf != null) {
                ObservableExtensionsKt.appSubscribe(ObservableExtensionsKt.ui$default(StoreInviteSettings.generateInvite$default(StoreStream.Companion.getInviteSettings(), valueOf.longValue(), null, 2, null), overlayMenuView, null, 2, null), OverlayMenuView.class, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : new s(overlayMenuView), (r18 & 16) != 0 ? ObservableExtensionsKt$appSubscribe$1.INSTANCE : null, (r18 & 32) != 0 ? ObservableExtensionsKt$appSubscribe$2.INSTANCE : null, new t(overlayMenuView));
            } else {
                m.g(overlayMenuView.getContext(), R.string.instant_invite_failed_to_generate, 0, null, 12);
            }
        } else if (i == 1) {
            OverlayMenuView overlayMenuView2 = (OverlayMenuView) this.k;
            Long valueOf2 = Long.valueOf(((OverlayMenuView.a) this.l).c.getChannel().h());
            int i3 = OverlayMenuView.j;
            Objects.requireNonNull(overlayMenuView2);
            overlayMenuView2.getContext().startActivity(IntentUtils.RouteBuilders.INSTANCE.connectVoice(valueOf2 != null ? valueOf2.longValue() : 0L).setClass(overlayMenuView2.getContext(), AppActivity.Main.class).addFlags(268435456));
            overlayMenuView2.m.invoke();
        } else {
            throw null;
        }
    }
}
