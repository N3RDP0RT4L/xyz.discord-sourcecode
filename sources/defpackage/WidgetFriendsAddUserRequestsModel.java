package defpackage;

import andhook.lib.HookHelper;
import b.d.b.a.a;
import d0.z.d.m;
import java.util.Set;
import kotlin.Metadata;
/* compiled from: WidgetFriendsAddUserRequestsModel.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\b\u000b\b\u0086\b\u0018\u00002\u00020\u0001B+\u0012\u0010\u0010\u0013\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f\u0012\u0010\u0010\u0016\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f¢\u0006\u0004\b\u0017\u0010\u0018J\u0010\u0010\u0003\u001a\u00020\u0002HÖ\u0001¢\u0006\u0004\b\u0003\u0010\u0004J\u0010\u0010\u0006\u001a\u00020\u0005HÖ\u0001¢\u0006\u0004\b\u0006\u0010\u0007J\u001a\u0010\n\u001a\u00020\t2\b\u0010\b\u001a\u0004\u0018\u00010\u0001HÖ\u0003¢\u0006\u0004\b\n\u0010\u000bR#\u0010\u0013\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\u0011\u0010\u0012R#\u0010\u0016\u001a\f\u0012\b\u0012\u00060\rj\u0002`\u000e0\f8\u0006@\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0010\u001a\u0004\b\u0015\u0010\u0012¨\u0006\u0019"}, d2 = {"LWidgetFriendsAddUserRequestsModel;", "", "", "toString", "()Ljava/lang/String;", "", "hashCode", "()I", "other", "", "equals", "(Ljava/lang/Object;)Z", "", "", "Lcom/discord/primitives/UserId;", "a", "Ljava/util/Set;", "getOutgoingIds", "()Ljava/util/Set;", "outgoingIds", "b", "getIncomingIds", "incomingIds", HookHelper.constructorName, "(Ljava/util/Set;Ljava/util/Set;)V", "app_productionGoogleRelease"}, k = 1, mv = {1, 4, 2})
/* renamed from: WidgetFriendsAddUserRequestsModel  reason: default package */
/* loaded from: classes3.dex */
public final class WidgetFriendsAddUserRequestsModel {
    public final Set<Long> a;

    /* renamed from: b  reason: collision with root package name */
    public final Set<Long> f13b;

    public WidgetFriendsAddUserRequestsModel(Set<Long> set, Set<Long> set2) {
        m.checkNotNullParameter(set, "outgoingIds");
        m.checkNotNullParameter(set2, "incomingIds");
        this.a = set;
        this.f13b = set2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WidgetFriendsAddUserRequestsModel)) {
            return false;
        }
        WidgetFriendsAddUserRequestsModel widgetFriendsAddUserRequestsModel = (WidgetFriendsAddUserRequestsModel) obj;
        return m.areEqual(this.a, widgetFriendsAddUserRequestsModel.a) && m.areEqual(this.f13b, widgetFriendsAddUserRequestsModel.f13b);
    }

    public int hashCode() {
        Set<Long> set = this.a;
        int i = 0;
        int hashCode = (set != null ? set.hashCode() : 0) * 31;
        Set<Long> set2 = this.f13b;
        if (set2 != null) {
            i = set2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder R = a.R("WidgetFriendsAddUserRequestsModel(outgoingIds=");
        R.append(this.a);
        R.append(", incomingIds=");
        R.append(this.f13b);
        R.append(")");
        return R.toString();
    }
}
