package defpackage;

import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import d0.z.d.m;
import d0.z.d.o;
import kotlin.jvm.functions.Function0;
/* compiled from: kotlin-style lambda group */
/* renamed from: k  reason: default package */
/* loaded from: classes2.dex */
public final class k extends o implements Function0<ViewModelStore> {
    public final /* synthetic */ int j;
    public final /* synthetic */ Object k;

    /* JADX WARN: 'super' call moved to the top of the method (can break code semantics) */
    public k(int i, Object obj) {
        super(0);
        this.j = i;
        this.k = obj;
    }

    @Override // kotlin.jvm.functions.Function0
    public final ViewModelStore invoke() {
        int i = this.j;
        if (i == 0) {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) ((Function0) this.k).invoke()).getViewModelStore();
            m.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        } else if (i == 1) {
            ViewModelStore viewModelStore2 = ((ViewModelStoreOwner) ((Function0) this.k).invoke()).getViewModelStore();
            m.checkNotNullExpressionValue(viewModelStore2, "ownerProducer().viewModelStore");
            return viewModelStore2;
        } else if (i == 2) {
            ViewModelStore viewModelStore3 = ((ViewModelStoreOwner) ((Function0) this.k).invoke()).getViewModelStore();
            m.checkNotNullExpressionValue(viewModelStore3, "ownerProducer().viewModelStore");
            return viewModelStore3;
        } else if (i == 3) {
            ViewModelStore viewModelStore4 = ((ViewModelStoreOwner) ((Function0) this.k).invoke()).getViewModelStore();
            m.checkNotNullExpressionValue(viewModelStore4, "ownerProducer().viewModelStore");
            return viewModelStore4;
        } else if (i == 4) {
            ViewModelStore viewModelStore5 = ((ViewModelStoreOwner) ((Function0) this.k).invoke()).getViewModelStore();
            m.checkNotNullExpressionValue(viewModelStore5, "ownerProducer().viewModelStore");
            return viewModelStore5;
        } else if (i == 5) {
            ViewModelStore viewModelStore6 = ((ViewModelStoreOwner) ((Function0) this.k).invoke()).getViewModelStore();
            m.checkNotNullExpressionValue(viewModelStore6, "ownerProducer().viewModelStore");
            return viewModelStore6;
        } else {
            throw null;
        }
    }
}
