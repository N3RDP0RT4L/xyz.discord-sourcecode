package defpackage;

import android.view.View;
import b.a.a.z.c;
import com.discord.stores.StoreGifting;
import com.discord.stores.StoreStream;
/* compiled from: java-style lambda group */
/* renamed from: g  reason: default package */
/* loaded from: classes2.dex */
public final class g implements View.OnClickListener {
    public final /* synthetic */ int j;
    public final /* synthetic */ Object k;

    public g(int i, Object obj) {
        this.j = i;
        this.k = obj;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        int i = this.j;
        if (i == 0) {
            StoreStream.Companion.getGifting().acceptGift(((StoreGifting.GiftState.RedeemedFailed) this.k).getGift());
        } else if (i == 1) {
            ((c) this.k).dismiss();
        } else {
            throw null;
        }
    }
}
