package defpackage;

import android.view.View;
import com.lytefast.flexinput.fragment.FlexInputFragment;
import com.lytefast.flexinput.viewmodel.FlexInputViewModel;
/* compiled from: java-style lambda group */
/* renamed from: h  reason: default package */
/* loaded from: classes2.dex */
public final class h implements View.OnClickListener {
    public final /* synthetic */ int j;
    public final /* synthetic */ Object k;

    public h(int i, Object obj) {
        this.j = i;
        this.k = obj;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        int i = this.j;
        if (i == 0) {
            FlexInputViewModel flexInputViewModel = ((FlexInputFragment) this.k).f3137s;
            if (flexInputViewModel != null) {
                flexInputViewModel.onExpressionTrayButtonClicked();
            }
        } else if (i == 1) {
            FlexInputFragment flexInputFragment = (FlexInputFragment) this.k;
            FlexInputViewModel flexInputViewModel2 = flexInputFragment.f3137s;
            if (flexInputViewModel2 != null) {
                flexInputViewModel2.onSendButtonClicked(flexInputFragment.o);
            }
        } else if (i == 2) {
            FlexInputViewModel flexInputViewModel3 = ((FlexInputFragment) this.k).f3137s;
            if (flexInputViewModel3 != null) {
                flexInputViewModel3.onGalleryButtonClicked();
            }
        } else if (i == 3) {
            FlexInputViewModel flexInputViewModel4 = ((FlexInputFragment) this.k).f3137s;
            if (flexInputViewModel4 != null) {
                flexInputViewModel4.onGiftButtonClicked();
            }
        } else if (i == 4) {
            FlexInputViewModel flexInputViewModel5 = ((FlexInputFragment) this.k).f3137s;
            if (flexInputViewModel5 != null) {
                flexInputViewModel5.onExpandButtonClicked();
            }
        } else {
            throw null;
        }
    }
}
