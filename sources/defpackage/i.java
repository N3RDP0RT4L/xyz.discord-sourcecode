package defpackage;

import android.content.Context;
import android.view.View;
import com.discord.rtcconnection.audio.DiscordAudioManager;
import com.discord.stores.StoreMediaSettings;
import com.discord.stores.StoreStream;
import com.discord.utilities.voice.DiscordOverlayService;
import com.discord.views.OverlayMenuView;
import d0.t.n;
import d0.z.d.m;
import java.util.Collection;
import java.util.List;
import xyz.discord.R;
/* compiled from: java-style lambda group */
/* renamed from: i  reason: default package */
/* loaded from: classes2.dex */
public final class i implements View.OnClickListener {
    public final /* synthetic */ int j;
    public final /* synthetic */ Object k;

    public i(int i, Object obj) {
        this.j = i;
        this.k = obj;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        int i;
        int i2 = this.j;
        if (i2 != 0) {
            boolean z2 = true;
            if (i2 == 1) {
                List<DiscordAudioManager.AudioDevice> audioDevices = ((OverlayMenuView.a) this.k).c.getAudioManagerState().getAudioDevices();
                if (!(audioDevices instanceof Collection) || !audioDevices.isEmpty()) {
                    i = 0;
                    for (DiscordAudioManager.AudioDevice audioDevice : audioDevices) {
                        if (audioDevice.f2761b && (i = i + 1) < 0) {
                            n.throwCountOverflow();
                        }
                    }
                } else {
                    i = 0;
                }
                if (i <= 1) {
                    z2 = false;
                }
                if (z2) {
                    StoreStream.Companion.getAudioManagerV2().toggleSpeakerOutput();
                    return;
                }
                m.checkNotNullExpressionValue(view, "it");
                b.a.d.m.g(view.getContext(), R.string.audio_devices_toggle_unavailable, 0, null, 12);
            } else if (i2 != 2) {
                if (i2 == 3) {
                    ((OverlayMenuView) this.k).getOnDismissRequested$app_productionGoogleRelease().invoke();
                    StoreStream.Companion.getVoiceChannelSelected().clear();
                    return;
                }
                throw null;
            } else if (((OverlayMenuView.a) this.k).c.isSuppressed()) {
                m.checkNotNullExpressionValue(view, "view");
                b.a.d.m.g(view.getContext(), R.string.suppressed_permission_body, 0, null, 12);
            } else if (((OverlayMenuView.a) this.k).c.isMuted()) {
                m.checkNotNullExpressionValue(view, "view");
                b.a.d.m.g(view.getContext(), R.string.server_muted_dialog_body, 0, null, 12);
            } else {
                StoreMediaSettings.SelfMuteFailure selfMuteFailure = StoreStream.Companion.getMediaSettings().toggleSelfMuted();
                if (selfMuteFailure != null && selfMuteFailure.ordinal() == 0) {
                    m.checkNotNullExpressionValue(view, "view");
                    b.a.d.m.g(view.getContext(), R.string.vad_permission_small, 0, null, 12);
                }
            }
        } else {
            DiscordOverlayService.Companion companion = DiscordOverlayService.Companion;
            Context context = ((OverlayMenuView) this.k).getContext();
            m.checkNotNullExpressionValue(context, "context");
            companion.launchForVoiceChannelSelect(context);
            ((OverlayMenuView) this.k).getOnDismissRequested$app_productionGoogleRelease().invoke();
        }
    }
}
