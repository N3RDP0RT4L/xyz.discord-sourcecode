package g0;

import b.d.b.a.a;
import d0.z.d.m;
import java.io.IOException;
/* compiled from: AsyncTimeout.kt */
/* loaded from: classes3.dex */
public final class d implements x {
    public final /* synthetic */ b j;
    public final /* synthetic */ x k;

    public d(b bVar, x xVar) {
        this.j = bVar;
        this.k = xVar;
    }

    @Override // g0.x, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        b bVar = this.j;
        bVar.i();
        try {
            this.k.close();
            if (bVar.j()) {
                throw bVar.k(null);
            }
        } catch (IOException e) {
            if (bVar.j()) {
                throw bVar.k(e);
            }
        } finally {
            bVar.j();
        }
    }

    @Override // g0.x
    public long i0(e eVar, long j) {
        m.checkParameterIsNotNull(eVar, "sink");
        b bVar = this.j;
        bVar.i();
        try {
            long i02 = this.k.i0(eVar, j);
            if (!bVar.j()) {
                return i02;
            }
            throw bVar.k(null);
        } catch (IOException e) {
            if (!bVar.j()) {
                throw e;
            }
            throw bVar.k(e);
        } finally {
            bVar.j();
        }
    }

    @Override // g0.x
    public y timeout() {
        return this.j;
    }

    public String toString() {
        StringBuilder R = a.R("AsyncTimeout.source(");
        R.append(this.k);
        R.append(')');
        return R.toString();
    }
}
