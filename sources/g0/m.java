package g0;

import b.d.b.a.a;
import java.io.EOFException;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;
/* compiled from: InflaterSource.kt */
/* loaded from: classes3.dex */
public final class m implements x {
    public int j;
    public boolean k;
    public final g l;
    public final Inflater m;

    public m(x xVar, Inflater inflater) {
        d0.z.d.m.checkParameterIsNotNull(xVar, "source");
        d0.z.d.m.checkParameterIsNotNull(inflater, "inflater");
        d0.z.d.m.checkParameterIsNotNull(xVar, "$this$buffer");
        r rVar = new r(xVar);
        d0.z.d.m.checkParameterIsNotNull(rVar, "source");
        d0.z.d.m.checkParameterIsNotNull(inflater, "inflater");
        this.l = rVar;
        this.m = inflater;
    }

    public final long a(e eVar, long j) throws IOException {
        d0.z.d.m.checkParameterIsNotNull(eVar, "sink");
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (!(i >= 0)) {
            throw new IllegalArgumentException(a.s("byteCount < 0: ", j).toString());
        } else if (!(!this.k)) {
            throw new IllegalStateException("closed".toString());
        } else if (i == 0) {
            return 0L;
        } else {
            try {
                s N = eVar.N(1);
                int min = (int) Math.min(j, 8192 - N.c);
                if (this.m.needsInput() && !this.l.w()) {
                    s sVar = this.l.g().j;
                    if (sVar == null) {
                        d0.z.d.m.throwNpe();
                    }
                    int i2 = sVar.c;
                    int i3 = sVar.f3665b;
                    int i4 = i2 - i3;
                    this.j = i4;
                    this.m.setInput(sVar.a, i3, i4);
                }
                int inflate = this.m.inflate(N.a, N.c, min);
                int i5 = this.j;
                if (i5 != 0) {
                    int remaining = i5 - this.m.getRemaining();
                    this.j -= remaining;
                    this.l.skip(remaining);
                }
                if (inflate > 0) {
                    N.c += inflate;
                    long j2 = inflate;
                    eVar.k += j2;
                    return j2;
                }
                if (N.f3665b == N.c) {
                    eVar.j = N.a();
                    t.a(N);
                }
                return 0L;
            } catch (DataFormatException e) {
                throw new IOException(e);
            }
        }
    }

    @Override // g0.x, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (!this.k) {
            this.m.end();
            this.k = true;
            this.l.close();
        }
    }

    @Override // g0.x
    public long i0(e eVar, long j) throws IOException {
        d0.z.d.m.checkParameterIsNotNull(eVar, "sink");
        do {
            long a = a(eVar, j);
            if (a > 0) {
                return a;
            }
            if (this.m.finished() || this.m.needsDictionary()) {
                return -1L;
            }
        } while (!this.l.w());
        throw new EOFException("source exhausted prematurely");
    }

    @Override // g0.x
    public y timeout() {
        return this.l.timeout();
    }

    public m(g gVar, Inflater inflater) {
        d0.z.d.m.checkParameterIsNotNull(gVar, "source");
        d0.z.d.m.checkParameterIsNotNull(inflater, "inflater");
        this.l = gVar;
        this.m = inflater;
    }
}
