package g0;

import d0.z.d.m;
import java.io.OutputStream;
/* compiled from: Buffer.kt */
/* loaded from: classes3.dex */
public final class f extends OutputStream {
    public final /* synthetic */ e j;

    public f(e eVar) {
        this.j = eVar;
    }

    @Override // java.io.OutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    @Override // java.io.OutputStream, java.io.Flushable
    public void flush() {
    }

    public String toString() {
        return this.j + ".outputStream()";
    }

    @Override // java.io.OutputStream
    public void write(int i) {
        this.j.T(i);
    }

    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        m.checkParameterIsNotNull(bArr, "data");
        this.j.S(bArr, i, i2);
    }
}
