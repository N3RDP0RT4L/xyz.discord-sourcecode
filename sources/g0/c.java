package g0;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import d0.z.d.m;
import java.io.IOException;
/* compiled from: AsyncTimeout.kt */
/* loaded from: classes3.dex */
public final class c implements v {
    public final /* synthetic */ b j;
    public final /* synthetic */ v k;

    public c(b bVar, v vVar) {
        this.j = bVar;
        this.k = vVar;
    }

    @Override // g0.v, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        b bVar = this.j;
        bVar.i();
        try {
            this.k.close();
            if (bVar.j()) {
                throw bVar.k(null);
            }
        } catch (IOException e) {
            if (bVar.j()) {
                throw bVar.k(e);
            }
        } finally {
            bVar.j();
        }
    }

    @Override // g0.v, java.io.Flushable
    public void flush() {
        b bVar = this.j;
        bVar.i();
        try {
            this.k.flush();
            if (bVar.j()) {
                throw bVar.k(null);
            }
        } catch (IOException e) {
            if (bVar.j()) {
                throw bVar.k(e);
            }
        } finally {
            bVar.j();
        }
    }

    @Override // g0.v
    public y timeout() {
        return this.j;
    }

    public String toString() {
        StringBuilder R = a.R("AsyncTimeout.sink(");
        R.append(this.k);
        R.append(')');
        return R.toString();
    }

    @Override // g0.v
    public void write(e eVar, long j) {
        m.checkParameterIsNotNull(eVar, "source");
        f.B(eVar.k, 0L, j);
        while (true) {
            long j2 = 0;
            if (j > 0) {
                s sVar = eVar.j;
                if (sVar == null) {
                    m.throwNpe();
                }
                while (true) {
                    if (j2 >= 65536) {
                        break;
                    }
                    j2 += sVar.c - sVar.f3665b;
                    if (j2 >= j) {
                        j2 = j;
                        break;
                    }
                    sVar = sVar.f;
                    if (sVar == null) {
                        m.throwNpe();
                    }
                }
                b bVar = this.j;
                bVar.i();
                try {
                    this.k.write(eVar, j2);
                    if (!bVar.j()) {
                        j -= j2;
                    } else {
                        throw bVar.k(null);
                    }
                } catch (IOException e) {
                    if (bVar.j()) {
                        throw bVar.k(e);
                    }
                    throw e;
                } finally {
                    bVar.j();
                }
            } else {
                return;
            }
        }
    }
}
