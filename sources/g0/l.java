package g0;

import androidx.recyclerview.widget.RecyclerView;
import b.d.b.a.a;
import d0.z.d.m;
import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.CRC32;
import java.util.zip.Inflater;
/* compiled from: GzipSource.kt */
/* loaded from: classes3.dex */
public final class l implements x {
    public byte j;
    public final r k;
    public final Inflater l;
    public final m m;
    public final CRC32 n = new CRC32();

    public l(x xVar) {
        m.checkParameterIsNotNull(xVar, "source");
        r rVar = new r(xVar);
        this.k = rVar;
        Inflater inflater = new Inflater(true);
        this.l = inflater;
        this.m = new m((g) rVar, inflater);
    }

    public final void a(String str, int i, int i2) {
        if (i2 != i) {
            String format = String.format("%s: actual 0x%08x != expected 0x%08x", Arrays.copyOf(new Object[]{str, Integer.valueOf(i2), Integer.valueOf(i)}, 3));
            m.checkExpressionValueIsNotNull(format, "java.lang.String.format(this, *args)");
            throw new IOException(format);
        }
    }

    public final void b(e eVar, long j, long j2) {
        int i;
        s sVar = eVar.j;
        if (sVar == null) {
            m.throwNpe();
        }
        while (true) {
            int i2 = sVar.c;
            int i3 = sVar.f3665b;
            if (j >= i2 - i3) {
                j -= i2 - i3;
                sVar = sVar.f;
                if (sVar == null) {
                    m.throwNpe();
                }
            }
        }
        while (j2 > 0) {
            int min = (int) Math.min(sVar.c - i, j2);
            this.n.update(sVar.a, (int) (sVar.f3665b + j), min);
            j2 -= min;
            sVar = sVar.f;
            if (sVar == null) {
                m.throwNpe();
            }
            j = 0;
        }
    }

    @Override // g0.x, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.m.close();
    }

    @Override // g0.x
    public long i0(e eVar, long j) throws IOException {
        long j2;
        m.checkParameterIsNotNull(eVar, "sink");
        boolean z2 = false;
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (!(i >= 0)) {
            throw new IllegalArgumentException(a.s("byteCount < 0: ", j).toString());
        } else if (i == 0) {
            return 0L;
        } else {
            if (this.j == 0) {
                this.k.p0(10L);
                byte q = this.k.j.q(3L);
                boolean z3 = ((q >> 1) & 1) == 1;
                if (z3) {
                    b(this.k.j, 0L, 10L);
                }
                a("ID1ID2", 8075, this.k.readShort());
                this.k.skip(8L);
                if (((q >> 2) & 1) == 1) {
                    this.k.p0(2L);
                    if (z3) {
                        b(this.k.j, 0L, 2L);
                    }
                    long y2 = this.k.j.y();
                    this.k.p0(y2);
                    if (z3) {
                        j2 = y2;
                        b(this.k.j, 0L, y2);
                    } else {
                        j2 = y2;
                    }
                    this.k.skip(j2);
                }
                if (((q >> 3) & 1) == 1) {
                    long a = this.k.a((byte) 0, 0L, RecyclerView.FOREVER_NS);
                    if (a != -1) {
                        if (z3) {
                            b(this.k.j, 0L, a + 1);
                        }
                        this.k.skip(a + 1);
                    } else {
                        throw new EOFException();
                    }
                }
                if (((q >> 4) & 1) == 1) {
                    z2 = true;
                }
                if (z2) {
                    long a2 = this.k.a((byte) 0, 0L, RecyclerView.FOREVER_NS);
                    if (a2 != -1) {
                        if (z3) {
                            b(this.k.j, 0L, a2 + 1);
                        }
                        this.k.skip(a2 + 1);
                    } else {
                        throw new EOFException();
                    }
                }
                if (z3) {
                    r rVar = this.k;
                    rVar.p0(2L);
                    a("FHCRC", rVar.j.y(), (short) this.n.getValue());
                    this.n.reset();
                }
                this.j = (byte) 1;
            }
            if (this.j == 1) {
                long j3 = eVar.k;
                long i02 = this.m.i0(eVar, j);
                if (i02 != -1) {
                    b(eVar, j3, i02);
                    return i02;
                }
                this.j = (byte) 2;
            }
            if (this.j == 2) {
                a("CRC", this.k.b(), (int) this.n.getValue());
                a("ISIZE", this.k.b(), (int) this.l.getBytesWritten());
                this.j = (byte) 3;
                if (!this.k.w()) {
                    throw new IOException("gzip finished without exhausting source");
                }
            }
            return -1L;
        }
    }

    @Override // g0.x
    public y timeout() {
        return this.k.timeout();
    }
}
