package g0;

import d0.z.d.m;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.internal.DefaultConstructorMarker;
/* compiled from: AsyncTimeout.kt */
/* loaded from: classes3.dex */
public class b extends y {
    public static final long e;
    public static final long f;
    public static b g;
    public static final a h = new a(null);
    public boolean i;
    public b j;
    public long k;

    /* compiled from: AsyncTimeout.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final b a() throws InterruptedException {
            b bVar = b.g;
            if (bVar == null) {
                m.throwNpe();
            }
            b bVar2 = bVar.j;
            if (bVar2 == null) {
                long nanoTime = System.nanoTime();
                b.class.wait(b.e);
                b bVar3 = b.g;
                if (bVar3 == null) {
                    m.throwNpe();
                }
                if (bVar3.j != null || System.nanoTime() - nanoTime < b.f) {
                    return null;
                }
                return b.g;
            }
            long nanoTime2 = bVar2.k - System.nanoTime();
            if (nanoTime2 > 0) {
                long j = nanoTime2 / 1000000;
                b.class.wait(j, (int) (nanoTime2 - (1000000 * j)));
                return null;
            }
            b bVar4 = b.g;
            if (bVar4 == null) {
                m.throwNpe();
            }
            bVar4.j = bVar2.j;
            bVar2.j = null;
            return bVar2;
        }
    }

    /* compiled from: AsyncTimeout.kt */
    /* renamed from: g0.b$b  reason: collision with other inner class name */
    /* loaded from: classes3.dex */
    public static final class C0392b extends Thread {
        public C0392b() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            b a;
            while (true) {
                try {
                    synchronized (b.class) {
                        a = b.h.a();
                        if (a == b.g) {
                            b.g = null;
                            return;
                        }
                    }
                    if (a != null) {
                        a.l();
                    }
                } catch (InterruptedException unused) {
                }
            }
        }
    }

    static {
        long millis = TimeUnit.SECONDS.toMillis(60L);
        e = millis;
        f = TimeUnit.MILLISECONDS.toNanos(millis);
    }

    public final void i() {
        b bVar;
        if (!this.i) {
            long j = this.d;
            boolean z2 = this.f3667b;
            int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
            if (i != 0 || z2) {
                this.i = true;
                synchronized (b.class) {
                    if (g == null) {
                        g = new b();
                        new C0392b().start();
                    }
                    long nanoTime = System.nanoTime();
                    if (i != 0 && z2) {
                        this.k = Math.min(j, c() - nanoTime) + nanoTime;
                    } else if (i != 0) {
                        this.k = j + nanoTime;
                    } else if (z2) {
                        this.k = c();
                    } else {
                        throw new AssertionError();
                    }
                    long j2 = this.k - nanoTime;
                    b bVar2 = g;
                    if (bVar2 == null) {
                        m.throwNpe();
                    }
                    while (true) {
                        bVar = bVar2.j;
                        if (bVar == null || j2 < bVar.k - nanoTime) {
                            break;
                        }
                        bVar2 = bVar;
                    }
                    this.j = bVar;
                    bVar2.j = this;
                    if (bVar2 == g) {
                        b.class.notify();
                    }
                }
                return;
            }
            return;
        }
        throw new IllegalStateException("Unbalanced enter/exit".toString());
    }

    /* JADX WARN: Code restructure failed: missing block: B:11:0x0013, code lost:
        r2.j = r4.j;
        r4.j = null;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public final boolean j() {
        /*
            r4 = this;
            boolean r0 = r4.i
            r1 = 0
            if (r0 != 0) goto L6
            return r1
        L6:
            r4.i = r1
            java.lang.Class<g0.b> r0 = g0.b.class
            monitor-enter(r0)
            g0.b r2 = g0.b.g     // Catch: java.lang.Throwable -> L21
        Ld:
            if (r2 == 0) goto L1e
            g0.b r3 = r2.j     // Catch: java.lang.Throwable -> L21
            if (r3 != r4) goto L1c
            g0.b r3 = r4.j     // Catch: java.lang.Throwable -> L21
            r2.j = r3     // Catch: java.lang.Throwable -> L21
            r2 = 0
            r4.j = r2     // Catch: java.lang.Throwable -> L21
            monitor-exit(r0)
            goto L20
        L1c:
            r2 = r3
            goto Ld
        L1e:
            r1 = 1
            monitor-exit(r0)
        L20:
            return r1
        L21:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: g0.b.j():boolean");
    }

    public IOException k(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    public void l() {
    }
}
