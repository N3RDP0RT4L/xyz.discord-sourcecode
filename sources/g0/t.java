package g0;

import d0.z.d.m;
import java.util.concurrent.atomic.AtomicReference;
/* compiled from: SegmentPool.kt */
/* loaded from: classes3.dex */
public final class t {

    /* renamed from: b  reason: collision with root package name */
    public static final int f3666b;
    public static final AtomicReference<s>[] c;
    public static final t d = new t();
    public static final s a = new s(new byte[0], 0, 0, false, false);

    static {
        int highestOneBit = Integer.highestOneBit((Runtime.getRuntime().availableProcessors() * 2) - 1);
        f3666b = highestOneBit;
        AtomicReference<s>[] atomicReferenceArr = new AtomicReference[highestOneBit];
        for (int i = 0; i < highestOneBit; i++) {
            atomicReferenceArr[i] = new AtomicReference<>();
        }
        c = atomicReferenceArr;
    }

    public static final void a(s sVar) {
        m.checkParameterIsNotNull(sVar, "segment");
        if (!(sVar.f == null && sVar.g == null)) {
            throw new IllegalArgumentException("Failed requirement.".toString());
        } else if (!sVar.d) {
            Thread currentThread = Thread.currentThread();
            m.checkExpressionValueIsNotNull(currentThread, "Thread.currentThread()");
            AtomicReference<s> atomicReference = c[(int) (currentThread.getId() & (f3666b - 1))];
            s sVar2 = atomicReference.get();
            if (sVar2 != a) {
                int i = sVar2 != null ? sVar2.c : 0;
                if (i < 65536) {
                    sVar.f = sVar2;
                    sVar.f3665b = 0;
                    sVar.c = i + 8192;
                    if (!atomicReference.compareAndSet(sVar2, sVar)) {
                        sVar.f = null;
                    }
                }
            }
        }
    }

    public static final s b() {
        Thread currentThread = Thread.currentThread();
        m.checkExpressionValueIsNotNull(currentThread, "Thread.currentThread()");
        AtomicReference<s> atomicReference = c[(int) (currentThread.getId() & (f3666b - 1))];
        s sVar = a;
        s andSet = atomicReference.getAndSet(sVar);
        if (andSet == sVar) {
            return new s();
        }
        if (andSet == null) {
            atomicReference.set(null);
            return new s();
        }
        atomicReference.set(andSet.f);
        andSet.f = null;
        andSet.c = 0;
        return andSet;
    }
}
