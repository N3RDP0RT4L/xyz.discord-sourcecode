package g0;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import d0.z.d.m;
import java.io.OutputStream;
/* compiled from: JvmOkio.kt */
/* loaded from: classes3.dex */
public final class p implements v {
    public final OutputStream j;
    public final y k;

    public p(OutputStream outputStream, y yVar) {
        m.checkParameterIsNotNull(outputStream, "out");
        m.checkParameterIsNotNull(yVar, "timeout");
        this.j = outputStream;
        this.k = yVar;
    }

    @Override // g0.v, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.j.close();
    }

    @Override // g0.v, java.io.Flushable
    public void flush() {
        this.j.flush();
    }

    @Override // g0.v
    public y timeout() {
        return this.k;
    }

    public String toString() {
        StringBuilder R = a.R("sink(");
        R.append(this.j);
        R.append(')');
        return R.toString();
    }

    @Override // g0.v
    public void write(e eVar, long j) {
        m.checkParameterIsNotNull(eVar, "source");
        f.B(eVar.k, 0L, j);
        while (j > 0) {
            this.k.f();
            s sVar = eVar.j;
            if (sVar == null) {
                m.throwNpe();
            }
            int min = (int) Math.min(j, sVar.c - sVar.f3665b);
            this.j.write(sVar.a, sVar.f3665b, min);
            int i = sVar.f3665b + min;
            sVar.f3665b = i;
            long j2 = min;
            j -= j2;
            eVar.k -= j2;
            if (i == sVar.c) {
                eVar.j = sVar.a();
                t.a(sVar);
            }
        }
    }
}
