package g0;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import d0.z.d.m;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
/* compiled from: JvmOkio.kt */
/* loaded from: classes3.dex */
public final class w extends b {
    public final Logger l = Logger.getLogger("okio.Okio");
    public final Socket m;

    public w(Socket socket) {
        m.checkParameterIsNotNull(socket, "socket");
        this.m = socket;
    }

    @Override // g0.b
    public IOException k(IOException iOException) {
        SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
        if (iOException != null) {
            socketTimeoutException.initCause(iOException);
        }
        return socketTimeoutException;
    }

    @Override // g0.b
    public void l() {
        try {
            this.m.close();
        } catch (AssertionError e) {
            if (f.z0(e)) {
                Logger logger = this.l;
                Level level = Level.WARNING;
                StringBuilder R = a.R("Failed to close timed out socket ");
                R.append(this.m);
                logger.log(level, R.toString(), (Throwable) e);
                return;
            }
            throw e;
        } catch (Exception e2) {
            Logger logger2 = this.l;
            Level level2 = Level.WARNING;
            StringBuilder R2 = a.R("Failed to close timed out socket ");
            R2.append(this.m);
            logger2.log(level2, R2.toString(), (Throwable) e2);
        }
    }
}
