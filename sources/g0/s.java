package g0;

import d0.t.j;
import d0.z.d.m;
/* compiled from: Segment.kt */
/* loaded from: classes3.dex */
public final class s {
    public final byte[] a;

    /* renamed from: b  reason: collision with root package name */
    public int f3665b;
    public int c;
    public boolean d;
    public boolean e;
    public s f;
    public s g;

    public s() {
        this.a = new byte[8192];
        this.e = true;
        this.d = false;
    }

    public final s a() {
        s sVar = this.f;
        if (sVar == this) {
            sVar = null;
        }
        s sVar2 = this.g;
        if (sVar2 == null) {
            m.throwNpe();
        }
        sVar2.f = this.f;
        s sVar3 = this.f;
        if (sVar3 == null) {
            m.throwNpe();
        }
        sVar3.g = this.g;
        this.f = null;
        this.g = null;
        return sVar;
    }

    public final s b(s sVar) {
        m.checkParameterIsNotNull(sVar, "segment");
        sVar.g = this;
        sVar.f = this.f;
        s sVar2 = this.f;
        if (sVar2 == null) {
            m.throwNpe();
        }
        sVar2.g = sVar;
        this.f = sVar;
        return sVar;
    }

    public final s c() {
        this.d = true;
        return new s(this.a, this.f3665b, this.c, true, false);
    }

    public final void d(s sVar, int i) {
        m.checkParameterIsNotNull(sVar, "sink");
        if (sVar.e) {
            int i2 = sVar.c;
            if (i2 + i > 8192) {
                if (!sVar.d) {
                    int i3 = sVar.f3665b;
                    if ((i2 + i) - i3 <= 8192) {
                        byte[] bArr = sVar.a;
                        j.copyInto$default(bArr, bArr, 0, i3, i2, 2, (Object) null);
                        sVar.c -= sVar.f3665b;
                        sVar.f3665b = 0;
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else {
                    throw new IllegalArgumentException();
                }
            }
            byte[] bArr2 = this.a;
            byte[] bArr3 = sVar.a;
            int i4 = sVar.c;
            int i5 = this.f3665b;
            j.copyInto(bArr2, bArr3, i4, i5, i5 + i);
            sVar.c += i;
            this.f3665b += i;
            return;
        }
        throw new IllegalStateException("only owner can write".toString());
    }

    public s(byte[] bArr, int i, int i2, boolean z2, boolean z3) {
        m.checkParameterIsNotNull(bArr, "data");
        this.a = bArr;
        this.f3665b = i;
        this.c = i2;
        this.d = z2;
        this.e = z3;
    }
}
