package g0;

import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import d0.g0.c;
import d0.t.j;
import d0.z.d.m;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Objects;
import okio.BufferedSink;
import okio.ByteString;
import org.objectweb.asm.Opcodes;
/* compiled from: Buffer.kt */
/* loaded from: classes3.dex */
public final class e implements g, BufferedSink, Cloneable, ByteChannel {
    public s j;
    public long k;

    /* compiled from: Buffer.kt */
    /* loaded from: classes3.dex */
    public static final class a implements Closeable {
        public e j;
        public boolean k;
        public s l;
        public byte[] n;
        public long m = -1;
        public int o = -1;
        public int p = -1;

        public final long a(long j) {
            e eVar = this.j;
            if (eVar == null) {
                throw new IllegalStateException("not attached to a buffer".toString());
            } else if (this.k) {
                long j2 = eVar.k;
                int i = 1;
                int i2 = (j > j2 ? 1 : (j == j2 ? 0 : -1));
                if (i2 <= 0) {
                    if (j >= 0) {
                        long j3 = j2 - j;
                        while (true) {
                            if (j3 > 0) {
                                s sVar = eVar.j;
                                if (sVar == null) {
                                    m.throwNpe();
                                }
                                s sVar2 = sVar.g;
                                if (sVar2 == null) {
                                    m.throwNpe();
                                }
                                int i3 = sVar2.c;
                                long j4 = i3 - sVar2.f3665b;
                                if (j4 > j3) {
                                    sVar2.c = i3 - ((int) j3);
                                    break;
                                }
                                eVar.j = sVar2.a();
                                t.a(sVar2);
                                j3 -= j4;
                            } else {
                                break;
                            }
                        }
                        this.l = null;
                        this.m = j;
                        this.n = null;
                        this.o = -1;
                        this.p = -1;
                    } else {
                        throw new IllegalArgumentException(b.d.b.a.a.s("newSize < 0: ", j).toString());
                    }
                } else if (i2 > 0) {
                    long j5 = j - j2;
                    boolean z2 = true;
                    while (j5 > 0) {
                        s N = eVar.N(i);
                        int min = (int) Math.min(j5, 8192 - N.c);
                        int i4 = N.c + min;
                        N.c = i4;
                        j5 -= min;
                        if (z2) {
                            this.l = N;
                            this.m = j2;
                            this.n = N.a;
                            this.o = i4 - min;
                            this.p = i4;
                            z2 = false;
                        }
                        i = 1;
                    }
                }
                eVar.k = j;
                return j2;
            } else {
                throw new IllegalStateException("resizeBuffer() only permitted for read/write buffers".toString());
            }
        }

        public final int b(long j) {
            long j2;
            s sVar;
            e eVar = this.j;
            if (eVar != null) {
                if (j >= -1) {
                    long j3 = eVar.k;
                    if (j <= j3) {
                        if (j == -1 || j == j3) {
                            this.l = null;
                            this.m = j;
                            this.n = null;
                            this.o = -1;
                            this.p = -1;
                            return -1;
                        }
                        s sVar2 = eVar.j;
                        s sVar3 = this.l;
                        if (sVar3 != null) {
                            long j4 = this.m;
                            int i = this.o;
                            if (sVar3 == null) {
                                m.throwNpe();
                            }
                            j2 = j4 - (i - sVar3.f3665b);
                            if (j2 > j) {
                                sVar = sVar2;
                                sVar2 = this.l;
                                j3 = j2;
                                j2 = 0;
                            } else {
                                sVar = this.l;
                            }
                        } else {
                            j2 = 0;
                            sVar = sVar2;
                        }
                        if (j3 - j > j - j2) {
                            while (true) {
                                if (sVar == null) {
                                    m.throwNpe();
                                }
                                int i2 = sVar.c;
                                int i3 = sVar.f3665b;
                                if (j < (i2 - i3) + j2) {
                                    break;
                                }
                                j2 += i2 - i3;
                                sVar = sVar.f;
                            }
                        } else {
                            while (j3 > j) {
                                if (sVar2 == null) {
                                    m.throwNpe();
                                }
                                sVar2 = sVar2.g;
                                if (sVar2 == null) {
                                    m.throwNpe();
                                }
                                j3 -= sVar2.c - sVar2.f3665b;
                            }
                            sVar = sVar2;
                            j2 = j3;
                        }
                        if (this.k) {
                            if (sVar == null) {
                                m.throwNpe();
                            }
                            if (sVar.d) {
                                byte[] bArr = sVar.a;
                                byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
                                m.checkExpressionValueIsNotNull(copyOf, "java.util.Arrays.copyOf(this, size)");
                                s sVar4 = new s(copyOf, sVar.f3665b, sVar.c, false, true);
                                if (eVar.j == sVar) {
                                    eVar.j = sVar4;
                                }
                                sVar.b(sVar4);
                                s sVar5 = sVar4.g;
                                if (sVar5 == null) {
                                    m.throwNpe();
                                }
                                sVar5.a();
                                sVar = sVar4;
                            }
                        }
                        this.l = sVar;
                        this.m = j;
                        if (sVar == null) {
                            m.throwNpe();
                        }
                        this.n = sVar.a;
                        int i4 = sVar.f3665b + ((int) (j - j2));
                        this.o = i4;
                        int i5 = sVar.c;
                        this.p = i5;
                        return i5 - i4;
                    }
                }
                String format = String.format("offset=%s > size=%s", Arrays.copyOf(new Object[]{Long.valueOf(j), Long.valueOf(eVar.k)}, 2));
                m.checkExpressionValueIsNotNull(format, "java.lang.String.format(format, *args)");
                throw new ArrayIndexOutOfBoundsException(format);
            }
            throw new IllegalStateException("not attached to a buffer".toString());
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            if (this.j != null) {
                this.j = null;
                this.l = null;
                this.m = -1L;
                this.n = null;
                this.o = -1;
                this.p = -1;
                return;
            }
            throw new IllegalStateException("not attached to a buffer".toString());
        }
    }

    public String A(long j, Charset charset) throws EOFException {
        m.checkParameterIsNotNull(charset, "charset");
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (!(i >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(b.d.b.a.a.s("byteCount: ", j).toString());
        } else if (this.k < j) {
            throw new EOFException();
        } else if (i == 0) {
            return "";
        } else {
            s sVar = this.j;
            if (sVar == null) {
                m.throwNpe();
            }
            int i2 = sVar.f3665b;
            if (i2 + j > sVar.c) {
                return new String(Z(j), charset);
            }
            int i3 = (int) j;
            String str = new String(sVar.a, i2, i3, charset);
            int i4 = sVar.f3665b + i3;
            sVar.f3665b = i4;
            this.k -= j;
            if (i4 == sVar.c) {
                this.j = sVar.a();
                t.a(sVar);
            }
            return str;
        }
    }

    @Override // g0.g
    public void B(e eVar, long j) throws EOFException {
        m.checkParameterIsNotNull(eVar, "sink");
        long j2 = this.k;
        if (j2 >= j) {
            eVar.write(this, j);
        } else {
            eVar.write(this, j2);
            throw new EOFException();
        }
    }

    public String D() {
        return A(this.k, c.a);
    }

    @Override // g0.g
    public long E(ByteString byteString) {
        m.checkParameterIsNotNull(byteString, "targetBytes");
        return t(byteString, 0L);
    }

    @Override // okio.BufferedSink
    public BufferedSink F() {
        return this;
    }

    @Override // g0.g
    public String G(long j) throws EOFException {
        if (j >= 0) {
            long j2 = RecyclerView.FOREVER_NS;
            if (j != RecyclerView.FOREVER_NS) {
                j2 = j + 1;
            }
            byte b2 = (byte) 10;
            long s2 = s(b2, 0L, j2);
            if (s2 != -1) {
                return g0.z.a.a(this, s2);
            }
            if (j2 < this.k && q(j2 - 1) == ((byte) 13) && q(j2) == b2) {
                return g0.z.a.a(this, j2);
            }
            e eVar = new e();
            n(eVar, 0L, Math.min(32, this.k));
            throw new EOFException("\\n not found: limit=" + Math.min(this.k, j) + " content=" + eVar.x().k() + (char) 8230);
        }
        throw new IllegalArgumentException(b.d.b.a.a.s("limit < 0: ", j).toString());
    }

    public String H(long j) throws EOFException {
        return A(j, c.a);
    }

    public int I() throws EOFException {
        int i;
        int i2;
        int i3;
        if (this.k != 0) {
            byte q = q(0L);
            if ((q & 128) == 0) {
                i3 = q & Byte.MAX_VALUE;
                i2 = 1;
                i = 0;
            } else if ((q & 224) == 192) {
                i3 = q & 31;
                i2 = 2;
                i = 128;
            } else if ((q & 240) == 224) {
                i3 = q & 15;
                i2 = 3;
                i = 2048;
            } else if ((q & 248) == 240) {
                i3 = q & 7;
                i2 = 4;
                i = 65536;
            } else {
                skip(1L);
                return 65533;
            }
            long j = i2;
            if (this.k >= j) {
                for (int i4 = 1; i4 < i2; i4++) {
                    long j2 = i4;
                    byte q2 = q(j2);
                    if ((q2 & 192) == 128) {
                        i3 = (i3 << 6) | (q2 & 63);
                    } else {
                        skip(j2);
                        return 65533;
                    }
                }
                skip(j);
                if (i3 > 1114111) {
                    return 65533;
                }
                if ((55296 <= i3 && 57343 >= i3) || i3 < i) {
                    return 65533;
                }
                return i3;
            }
            StringBuilder S = b.d.b.a.a.S("size < ", i2, ": ");
            S.append(this.k);
            S.append(" (to read code point prefixed 0x");
            S.append(f.t1(q));
            S.append(')');
            throw new EOFException(S.toString());
        }
        throw new EOFException();
    }

    public final ByteString J(int i) {
        if (i == 0) {
            return ByteString.j;
        }
        f.B(this.k, 0L, i);
        s sVar = this.j;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i3 < i) {
            if (sVar == null) {
                m.throwNpe();
            }
            int i5 = sVar.c;
            int i6 = sVar.f3665b;
            if (i5 != i6) {
                i3 += i5 - i6;
                i4++;
                sVar = sVar.f;
            } else {
                throw new AssertionError("s.limit == s.pos");
            }
        }
        byte[][] bArr = new byte[i4];
        int[] iArr = new int[i4 * 2];
        s sVar2 = this.j;
        int i7 = 0;
        while (i2 < i) {
            if (sVar2 == null) {
                m.throwNpe();
            }
            bArr[i7] = sVar2.a;
            i2 += sVar2.c - sVar2.f3665b;
            iArr[i7] = Math.min(i2, i);
            iArr[i7 + i4] = sVar2.f3665b;
            sVar2.d = true;
            i7++;
            sVar2 = sVar2.f;
        }
        return new u(bArr, iArr);
    }

    @Override // okio.BufferedSink
    public /* bridge */ /* synthetic */ BufferedSink K(String str) {
        b0(str);
        return this;
    }

    @Override // g0.g
    public String M(Charset charset) {
        m.checkParameterIsNotNull(charset, "charset");
        return A(this.k, charset);
    }

    public final s N(int i) {
        boolean z2 = true;
        if (i < 1 || i > 8192) {
            z2 = false;
        }
        if (z2) {
            s sVar = this.j;
            if (sVar == null) {
                s b2 = t.b();
                this.j = b2;
                b2.g = b2;
                b2.f = b2;
                return b2;
            }
            if (sVar == null) {
                m.throwNpe();
            }
            s sVar2 = sVar.g;
            if (sVar2 == null) {
                m.throwNpe();
            }
            if (sVar2.c + i <= 8192 && sVar2.e) {
                return sVar2;
            }
            s b3 = t.b();
            sVar2.b(b3);
            return b3;
        }
        throw new IllegalArgumentException("unexpected capacity".toString());
    }

    public e O(ByteString byteString) {
        m.checkParameterIsNotNull(byteString, "byteString");
        byteString.s(this, 0, byteString.j());
        return this;
    }

    @Override // okio.BufferedSink
    public long P(x xVar) throws IOException {
        m.checkParameterIsNotNull(xVar, "source");
        long j = 0;
        while (true) {
            long i02 = xVar.i0(this, 8192);
            if (i02 == -1) {
                return j;
            }
            j += i02;
        }
    }

    public e R(byte[] bArr) {
        m.checkParameterIsNotNull(bArr, "source");
        S(bArr, 0, bArr.length);
        return this;
    }

    public e S(byte[] bArr, int i, int i2) {
        m.checkParameterIsNotNull(bArr, "source");
        long j = i2;
        f.B(bArr.length, i, j);
        int i3 = i2 + i;
        while (i < i3) {
            s N = N(1);
            int min = Math.min(i3 - i, 8192 - N.c);
            int i4 = i + min;
            j.copyInto(bArr, N.a, N.c, i, i4);
            N.c += min;
            i = i4;
        }
        this.k += j;
        return this;
    }

    public e T(int i) {
        s N = N(1);
        byte[] bArr = N.a;
        int i2 = N.c;
        N.c = i2 + 1;
        bArr[i2] = (byte) i;
        this.k++;
        return this;
    }

    /* renamed from: U */
    public e q0(long j) {
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (i == 0) {
            T(48);
        } else {
            boolean z2 = false;
            int i2 = 1;
            if (i < 0) {
                j = -j;
                if (j < 0) {
                    b0("-9223372036854775808");
                } else {
                    z2 = true;
                }
            }
            if (j >= 100000000) {
                i2 = j < 1000000000000L ? j < 10000000000L ? j < 1000000000 ? 9 : 10 : j < 100000000000L ? 11 : 12 : j < 1000000000000000L ? j < 10000000000000L ? 13 : j < 100000000000000L ? 14 : 15 : j < 100000000000000000L ? j < 10000000000000000L ? 16 : 17 : j < 1000000000000000000L ? 18 : 19;
            } else if (j >= 10000) {
                i2 = j < 1000000 ? j < 100000 ? 5 : 6 : j < 10000000 ? 7 : 8;
            } else if (j >= 100) {
                i2 = j < 1000 ? 3 : 4;
            } else if (j >= 10) {
                i2 = 2;
            }
            if (z2) {
                i2++;
            }
            s N = N(i2);
            byte[] bArr = N.a;
            int i3 = N.c + i2;
            while (j != 0) {
                long j2 = 10;
                i3--;
                bArr[i3] = g0.z.a.a[(int) (j % j2)];
                j /= j2;
            }
            if (z2) {
                bArr[i3 - 1] = (byte) 45;
            }
            N.c += i2;
            this.k += i2;
        }
        return this;
    }

    /* renamed from: V */
    public e Q(long j) {
        if (j == 0) {
            T(48);
        } else {
            long j2 = (j >>> 1) | j;
            long j3 = j2 | (j2 >>> 2);
            long j4 = j3 | (j3 >>> 4);
            long j5 = j4 | (j4 >>> 8);
            long j6 = j5 | (j5 >>> 16);
            long j7 = j6 | (j6 >>> 32);
            long j8 = j7 - ((j7 >>> 1) & 6148914691236517205L);
            long j9 = ((j8 >>> 2) & 3689348814741910323L) + (j8 & 3689348814741910323L);
            long j10 = ((j9 >>> 4) + j9) & 1085102592571150095L;
            long j11 = j10 + (j10 >>> 8);
            long j12 = j11 + (j11 >>> 16);
            int i = (int) ((((j12 & 63) + ((j12 >>> 32) & 63)) + 3) / 4);
            s N = N(i);
            byte[] bArr = N.a;
            int i2 = N.c;
            for (int i3 = (i2 + i) - 1; i3 >= i2; i3--) {
                bArr[i3] = g0.z.a.a[(int) (15 & j)];
                j >>>= 4;
            }
            N.c += i;
            this.k += i;
        }
        return this;
    }

    public e W(int i) {
        s N = N(4);
        byte[] bArr = N.a;
        int i2 = N.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 24) & 255);
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((i >>> 16) & 255);
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((i >>> 8) & 255);
        bArr[i5] = (byte) (i & 255);
        N.c = i5 + 1;
        this.k += 4;
        return this;
    }

    public e X(int i) {
        s N = N(2);
        byte[] bArr = N.a;
        int i2 = N.c;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((i >>> 8) & 255);
        bArr[i3] = (byte) (i & 255);
        N.c = i3 + 1;
        this.k += 2;
        return this;
    }

    @Override // g0.g
    public String Y() throws EOFException {
        return G(RecyclerView.FOREVER_NS);
    }

    @Override // g0.g
    public byte[] Z(long j) throws EOFException {
        if (!(j >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(b.d.b.a.a.s("byteCount: ", j).toString());
        } else if (this.k >= j) {
            byte[] bArr = new byte[(int) j];
            readFully(bArr);
            return bArr;
        } else {
            throw new EOFException();
        }
    }

    /* renamed from: b */
    public e clone() {
        e eVar = new e();
        if (this.k != 0) {
            s sVar = this.j;
            if (sVar == null) {
                m.throwNpe();
            }
            s c = sVar.c();
            eVar.j = c;
            c.g = c;
            c.f = c;
            for (s sVar2 = sVar.f; sVar2 != sVar; sVar2 = sVar2.f) {
                s sVar3 = c.g;
                if (sVar3 == null) {
                    m.throwNpe();
                }
                if (sVar2 == null) {
                    m.throwNpe();
                }
                sVar3.b(sVar2.c());
            }
            eVar.k = this.k;
        }
        return eVar;
    }

    public e b0(String str) {
        m.checkParameterIsNotNull(str, "string");
        c0(str, 0, str.length());
        return this;
    }

    public e c0(String str, int i, int i2) {
        char charAt;
        m.checkParameterIsNotNull(str, "string");
        if (i >= 0) {
            if (i2 >= i) {
                if (i2 <= str.length()) {
                    while (i < i2) {
                        char charAt2 = str.charAt(i);
                        if (charAt2 < 128) {
                            s N = N(1);
                            byte[] bArr = N.a;
                            int i3 = N.c - i;
                            int min = Math.min(i2, 8192 - i3);
                            int i4 = i + 1;
                            bArr[i + i3] = (byte) charAt2;
                            while (true) {
                                i = i4;
                                if (i >= min || (charAt = str.charAt(i)) >= 128) {
                                    break;
                                }
                                i4 = i + 1;
                                bArr[i + i3] = (byte) charAt;
                            }
                            int i5 = N.c;
                            int i6 = (i3 + i) - i5;
                            N.c = i5 + i6;
                            this.k += i6;
                        } else {
                            if (charAt2 < 2048) {
                                s N2 = N(2);
                                byte[] bArr2 = N2.a;
                                int i7 = N2.c;
                                bArr2[i7] = (byte) ((charAt2 >> 6) | Opcodes.CHECKCAST);
                                bArr2[i7 + 1] = (byte) ((charAt2 & '?') | 128);
                                N2.c = i7 + 2;
                                this.k += 2;
                            } else if (charAt2 < 55296 || charAt2 > 57343) {
                                s N3 = N(3);
                                byte[] bArr3 = N3.a;
                                int i8 = N3.c;
                                bArr3[i8] = (byte) ((charAt2 >> '\f') | 224);
                                bArr3[i8 + 1] = (byte) ((63 & (charAt2 >> 6)) | 128);
                                bArr3[i8 + 2] = (byte) ((charAt2 & '?') | 128);
                                N3.c = i8 + 3;
                                this.k += 3;
                            } else {
                                int i9 = i + 1;
                                char charAt3 = i9 < i2 ? str.charAt(i9) : (char) 0;
                                if (charAt2 > 56319 || 56320 > charAt3 || 57343 < charAt3) {
                                    T(63);
                                    i = i9;
                                } else {
                                    int i10 = (((charAt2 & 1023) << 10) | (charAt3 & 1023)) + 65536;
                                    s N4 = N(4);
                                    byte[] bArr4 = N4.a;
                                    int i11 = N4.c;
                                    bArr4[i11] = (byte) ((i10 >> 18) | 240);
                                    bArr4[i11 + 1] = (byte) (((i10 >> 12) & 63) | 128);
                                    bArr4[i11 + 2] = (byte) (((i10 >> 6) & 63) | 128);
                                    bArr4[i11 + 3] = (byte) ((i10 & 63) | 128);
                                    N4.c = i11 + 4;
                                    this.k += 4;
                                    i += 2;
                                }
                            }
                            i++;
                        }
                    }
                    return this;
                }
                StringBuilder S = b.d.b.a.a.S("endIndex > string.length: ", i2, " > ");
                S.append(str.length());
                throw new IllegalArgumentException(S.toString().toString());
            }
            throw new IllegalArgumentException(b.d.b.a.a.r("endIndex < beginIndex: ", i2, " < ", i).toString());
        }
        throw new IllegalArgumentException(b.d.b.a.a.p("beginIndex < 0: ", i).toString());
    }

    @Override // g0.x, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    public e d0(int i) {
        String str;
        if (i < 128) {
            T(i);
        } else if (i < 2048) {
            s N = N(2);
            byte[] bArr = N.a;
            int i2 = N.c;
            bArr[i2] = (byte) ((i >> 6) | Opcodes.CHECKCAST);
            bArr[i2 + 1] = (byte) ((i & 63) | 128);
            N.c = i2 + 2;
            this.k += 2;
        } else if (55296 <= i && 57343 >= i) {
            T(63);
        } else if (i < 65536) {
            s N2 = N(3);
            byte[] bArr2 = N2.a;
            int i3 = N2.c;
            bArr2[i3] = (byte) ((i >> 12) | 224);
            bArr2[i3 + 1] = (byte) (((i >> 6) & 63) | 128);
            bArr2[i3 + 2] = (byte) ((i & 63) | 128);
            N2.c = i3 + 3;
            this.k += 3;
        } else if (i <= 1114111) {
            s N3 = N(4);
            byte[] bArr3 = N3.a;
            int i4 = N3.c;
            bArr3[i4] = (byte) ((i >> 18) | 240);
            bArr3[i4 + 1] = (byte) (((i >> 12) & 63) | 128);
            bArr3[i4 + 2] = (byte) (((i >> 6) & 63) | 128);
            bArr3[i4 + 3] = (byte) ((i & 63) | 128);
            N3.c = i4 + 4;
            this.k += 4;
        } else {
            StringBuilder R = b.d.b.a.a.R("Unexpected code point: 0x");
            if (i != 0) {
                char[] cArr = g0.z.b.a;
                int i5 = 0;
                char[] cArr2 = {cArr[(i >> 28) & 15], cArr[(i >> 24) & 15], cArr[(i >> 20) & 15], cArr[(i >> 16) & 15], cArr[(i >> 12) & 15], cArr[(i >> 8) & 15], cArr[(i >> 4) & 15], cArr[i & 15]};
                while (i5 < 8 && cArr2[i5] == '0') {
                    i5++;
                }
                str = new String(cArr2, i5, 8 - i5);
            } else {
                str = "0";
            }
            R.append(str);
            throw new IllegalArgumentException(R.toString());
        }
        return this;
    }

    @Override // okio.BufferedSink
    public /* bridge */ /* synthetic */ BufferedSink e0(ByteString byteString) {
        O(byteString);
        return this;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (!(obj instanceof e)) {
                return false;
            }
            long j = this.k;
            e eVar = (e) obj;
            if (j != eVar.k) {
                return false;
            }
            if (j != 0) {
                s sVar = this.j;
                if (sVar == null) {
                    m.throwNpe();
                }
                s sVar2 = eVar.j;
                if (sVar2 == null) {
                    m.throwNpe();
                }
                int i = sVar.f3665b;
                int i2 = sVar2.f3665b;
                long j2 = 0;
                while (j2 < this.k) {
                    long min = Math.min(sVar.c - i, sVar2.c - i2);
                    for (long j3 = 0; j3 < min; j3++) {
                        i++;
                        i2++;
                        if (sVar.a[i] != sVar2.a[i2]) {
                            return false;
                        }
                    }
                    if (i == sVar.c) {
                        sVar = sVar.f;
                        if (sVar == null) {
                            m.throwNpe();
                        }
                        i = sVar.f3665b;
                    }
                    if (i2 == sVar2.c) {
                        sVar2 = sVar2.f;
                        if (sVar2 == null) {
                            m.throwNpe();
                        }
                        i2 = sVar2.f3665b;
                    }
                    j2 += min;
                }
            }
        }
        return true;
    }

    public final long f() {
        long j = this.k;
        if (j == 0) {
            return 0L;
        }
        s sVar = this.j;
        if (sVar == null) {
            m.throwNpe();
        }
        s sVar2 = sVar.g;
        if (sVar2 == null) {
            m.throwNpe();
        }
        int i = sVar2.c;
        if (i < 8192 && sVar2.e) {
            j -= i - sVar2.f3665b;
        }
        return j;
    }

    @Override // okio.BufferedSink, g0.v, java.io.Flushable
    public void flush() {
    }

    @Override // g0.g, okio.BufferedSink
    public e g() {
        return this;
    }

    public int hashCode() {
        s sVar = this.j;
        if (sVar == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = sVar.c;
            for (int i3 = sVar.f3665b; i3 < i2; i3++) {
                i = (i * 31) + sVar.a[i3];
            }
            sVar = sVar.f;
            if (sVar == null) {
                m.throwNpe();
            }
        } while (sVar != this.j);
        return i;
    }

    @Override // g0.x
    public long i0(e eVar, long j) {
        m.checkParameterIsNotNull(eVar, "sink");
        if (j >= 0) {
            long j2 = this.k;
            if (j2 == 0) {
                return -1L;
            }
            if (j > j2) {
                j = j2;
            }
            eVar.write(this, j);
            return j;
        }
        throw new IllegalArgumentException(b.d.b.a.a.s("byteCount < 0: ", j).toString());
    }

    @Override // java.nio.channels.Channel
    public boolean isOpen() {
        return true;
    }

    @Override // g0.g
    public boolean j(long j) {
        return this.k >= j;
    }

    @Override // g0.g
    public long k0(v vVar) throws IOException {
        m.checkParameterIsNotNull(vVar, "sink");
        long j = this.k;
        if (j > 0) {
            vVar.write(this, j);
        }
        return j;
    }

    public final e n(e eVar, long j, long j2) {
        m.checkParameterIsNotNull(eVar, "out");
        f.B(this.k, j, j2);
        if (j2 != 0) {
            eVar.k += j2;
            s sVar = this.j;
            while (true) {
                if (sVar == null) {
                    m.throwNpe();
                }
                int i = sVar.c;
                int i2 = sVar.f3665b;
                if (j >= i - i2) {
                    j -= i - i2;
                    sVar = sVar.f;
                }
            }
            while (j2 > 0) {
                if (sVar == null) {
                    m.throwNpe();
                }
                s c = sVar.c();
                int i3 = c.f3665b + ((int) j);
                c.f3665b = i3;
                c.c = Math.min(i3 + ((int) j2), c.c);
                s sVar2 = eVar.j;
                if (sVar2 == null) {
                    c.g = c;
                    c.f = c;
                    eVar.j = c;
                } else {
                    if (sVar2 == null) {
                        m.throwNpe();
                    }
                    s sVar3 = sVar2.g;
                    if (sVar3 == null) {
                        m.throwNpe();
                    }
                    sVar3.b(c);
                }
                j2 -= c.c - c.f3665b;
                sVar = sVar.f;
                j = 0;
            }
        }
        return this;
    }

    @Override // g0.g
    public ByteString o(long j) throws EOFException {
        if (!(j >= 0 && j <= ((long) Integer.MAX_VALUE))) {
            throw new IllegalArgumentException(b.d.b.a.a.s("byteCount: ", j).toString());
        } else if (this.k < j) {
            throw new EOFException();
        } else if (j < 4096) {
            return new ByteString(Z(j));
        } else {
            ByteString J = J((int) j);
            skip(j);
            return J;
        }
    }

    @Override // okio.BufferedSink
    public BufferedSink p() {
        return this;
    }

    @Override // g0.g
    public void p0(long j) throws EOFException {
        if (this.k < j) {
            throw new EOFException();
        }
    }

    public final byte q(long j) {
        f.B(this.k, j, 1L);
        s sVar = this.j;
        if (sVar != null) {
            long j2 = this.k;
            if (j2 - j < j) {
                while (j2 > j) {
                    sVar = sVar.g;
                    if (sVar == null) {
                        m.throwNpe();
                    }
                    j2 -= sVar.c - sVar.f3665b;
                }
                return sVar.a[(int) ((sVar.f3665b + j) - j2)];
            }
            long j3 = 0;
            while (true) {
                int i = sVar.c;
                int i2 = sVar.f3665b;
                long j4 = (i - i2) + j3;
                if (j4 > j) {
                    return sVar.a[(int) ((i2 + j) - j3)];
                }
                sVar = sVar.f;
                if (sVar == null) {
                    m.throwNpe();
                }
                j3 = j4;
            }
        } else {
            m.throwNpe();
            throw null;
        }
    }

    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer byteBuffer) throws IOException {
        m.checkParameterIsNotNull(byteBuffer, "sink");
        s sVar = this.j;
        if (sVar == null) {
            return -1;
        }
        int min = Math.min(byteBuffer.remaining(), sVar.c - sVar.f3665b);
        byteBuffer.put(sVar.a, sVar.f3665b, min);
        int i = sVar.f3665b + min;
        sVar.f3665b = i;
        this.k -= min;
        if (i == sVar.c) {
            this.j = sVar.a();
            t.a(sVar);
        }
        return min;
    }

    @Override // g0.g
    public byte readByte() throws EOFException {
        if (this.k != 0) {
            s sVar = this.j;
            if (sVar == null) {
                m.throwNpe();
            }
            int i = sVar.f3665b;
            int i2 = sVar.c;
            int i3 = i + 1;
            byte b2 = sVar.a[i];
            this.k--;
            if (i3 == i2) {
                this.j = sVar.a();
                t.a(sVar);
            } else {
                sVar.f3665b = i3;
            }
            return b2;
        }
        throw new EOFException();
    }

    @Override // g0.g
    public void readFully(byte[] bArr) throws EOFException {
        m.checkParameterIsNotNull(bArr, "sink");
        int i = 0;
        while (i < bArr.length) {
            int read = read(bArr, i, bArr.length - i);
            if (read != -1) {
                i += read;
            } else {
                throw new EOFException();
            }
        }
    }

    @Override // g0.g
    public int readInt() throws EOFException {
        if (this.k >= 4) {
            s sVar = this.j;
            if (sVar == null) {
                m.throwNpe();
            }
            int i = sVar.f3665b;
            int i2 = sVar.c;
            if (i2 - i < 4) {
                return ((readByte() & 255) << 24) | ((readByte() & 255) << 16) | ((readByte() & 255) << 8) | (readByte() & 255);
            }
            byte[] bArr = sVar.a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            int i5 = ((bArr[i] & 255) << 24) | ((bArr[i3] & 255) << 16);
            int i6 = i4 + 1;
            int i7 = i5 | ((bArr[i4] & 255) << 8);
            int i8 = i6 + 1;
            int i9 = i7 | (bArr[i6] & 255);
            this.k -= 4;
            if (i8 == i2) {
                this.j = sVar.a();
                t.a(sVar);
            } else {
                sVar.f3665b = i8;
            }
            return i9;
        }
        throw new EOFException();
    }

    @Override // g0.g
    public long readLong() throws EOFException {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        if (this.k >= 8) {
            s sVar = this.j;
            if (sVar == null) {
                m.throwNpe();
            }
            int i7 = sVar.f3665b;
            int i8 = sVar.c;
            if (i8 - i7 < 8) {
                return ((readInt() & 4294967295L) << 32) | (4294967295L & readInt());
            }
            byte[] bArr = sVar.a;
            long j = ((bArr[i7] & 255) << 56) | ((bArr[i] & 255) << 48) | ((bArr[i2] & 255) << 40);
            int i9 = i7 + 1 + 1 + 1 + 1;
            long j2 = ((bArr[i3] & 255) << 32) | j;
            long j3 = j2 | ((bArr[i9] & 255) << 24) | ((bArr[i4] & 255) << 16);
            int i10 = i9 + 1 + 1 + 1 + 1;
            long j4 = j3 | ((bArr[i5] & 255) << 8) | (bArr[i6] & 255);
            this.k -= 8;
            if (i10 == i8) {
                this.j = sVar.a();
                t.a(sVar);
            } else {
                sVar.f3665b = i10;
            }
            return j4;
        }
        throw new EOFException();
    }

    @Override // g0.g
    public short readShort() throws EOFException {
        if (this.k >= 2) {
            s sVar = this.j;
            if (sVar == null) {
                m.throwNpe();
            }
            int i = sVar.f3665b;
            int i2 = sVar.c;
            if (i2 - i < 2) {
                return (short) (((readByte() & 255) << 8) | (readByte() & 255));
            }
            byte[] bArr = sVar.a;
            int i3 = i + 1;
            int i4 = i3 + 1;
            int i5 = ((bArr[i] & 255) << 8) | (bArr[i3] & 255);
            this.k -= 2;
            if (i4 == i2) {
                this.j = sVar.a();
                t.a(sVar);
            } else {
                sVar.f3665b = i4;
            }
            return (short) i5;
        }
        throw new EOFException();
    }

    public long s(byte b2, long j, long j2) {
        s sVar;
        long j3 = 0;
        if (0 <= j && j2 >= j) {
            long j4 = this.k;
            if (j2 > j4) {
                j2 = j4;
            }
            if (!(j == j2 || (sVar = this.j) == null)) {
                if (j4 - j < j) {
                    while (j4 > j) {
                        sVar = sVar.g;
                        if (sVar == null) {
                            m.throwNpe();
                        }
                        j4 -= sVar.c - sVar.f3665b;
                    }
                    while (j4 < j2) {
                        byte[] bArr = sVar.a;
                        int min = (int) Math.min(sVar.c, (sVar.f3665b + j2) - j4);
                        for (int i = (int) ((sVar.f3665b + j) - j4); i < min; i++) {
                            if (bArr[i] == b2) {
                                return (i - sVar.f3665b) + j4;
                            }
                        }
                        j4 += sVar.c - sVar.f3665b;
                        sVar = sVar.f;
                        if (sVar == null) {
                            m.throwNpe();
                        }
                        j = j4;
                    }
                } else {
                    while (true) {
                        long j5 = (sVar.c - sVar.f3665b) + j3;
                        if (j5 > j) {
                            break;
                        }
                        sVar = sVar.f;
                        if (sVar == null) {
                            m.throwNpe();
                        }
                        j3 = j5;
                    }
                    while (j3 < j2) {
                        byte[] bArr2 = sVar.a;
                        int min2 = (int) Math.min(sVar.c, (sVar.f3665b + j2) - j3);
                        for (int i2 = (int) ((sVar.f3665b + j) - j3); i2 < min2; i2++) {
                            if (bArr2[i2] == b2) {
                                return (i2 - sVar.f3665b) + j3;
                            }
                        }
                        j3 += sVar.c - sVar.f3665b;
                        sVar = sVar.f;
                        if (sVar == null) {
                            m.throwNpe();
                        }
                        j = j3;
                    }
                }
            }
            return -1L;
        }
        StringBuilder R = b.d.b.a.a.R("size=");
        R.append(this.k);
        R.append(" fromIndex=");
        R.append(j);
        R.append(" toIndex=");
        R.append(j2);
        throw new IllegalArgumentException(R.toString().toString());
    }

    /* JADX WARN: Removed duplicated region for block: B:34:0x0091  */
    /* JADX WARN: Removed duplicated region for block: B:35:0x009b  */
    /* JADX WARN: Removed duplicated region for block: B:37:0x009f  */
    /* JADX WARN: Removed duplicated region for block: B:44:0x00a3 A[EDGE_INSN: B:44:0x00a3->B:39:0x00a3 ?: BREAK  , SYNTHETIC] */
    @Override // g0.g
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public long s0() throws java.io.EOFException {
        /*
            r15 = this;
            long r0 = r15.k
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 == 0) goto Laa
            r0 = 0
            r1 = 0
            r4 = r2
        Lb:
            g0.s r6 = r15.j
            if (r6 != 0) goto L12
            d0.z.d.m.throwNpe()
        L12:
            byte[] r7 = r6.a
            int r8 = r6.f3665b
            int r9 = r6.c
        L18:
            if (r8 >= r9) goto L8f
            r10 = r7[r8]
            r11 = 48
            byte r11 = (byte) r11
            if (r10 < r11) goto L29
            r12 = 57
            byte r12 = (byte) r12
            if (r10 > r12) goto L29
            int r11 = r10 - r11
            goto L42
        L29:
            r11 = 97
            byte r11 = (byte) r11
            if (r10 < r11) goto L34
            r12 = 102(0x66, float:1.43E-43)
            byte r12 = (byte) r12
            if (r10 > r12) goto L34
            goto L3e
        L34:
            r11 = 65
            byte r11 = (byte) r11
            if (r10 < r11) goto L74
            r12 = 70
            byte r12 = (byte) r12
            if (r10 > r12) goto L74
        L3e:
            int r11 = r10 - r11
            int r11 = r11 + 10
        L42:
            r12 = -1152921504606846976(0xf000000000000000, double:-3.105036184601418E231)
            long r12 = r12 & r4
            int r14 = (r12 > r2 ? 1 : (r12 == r2 ? 0 : -1))
            if (r14 != 0) goto L52
            r10 = 4
            long r4 = r4 << r10
            long r10 = (long) r11
            long r4 = r4 | r10
            int r8 = r8 + 1
            int r0 = r0 + 1
            goto L18
        L52:
            g0.e r0 = new g0.e
            r0.<init>()
            r0.Q(r4)
            r0.T(r10)
            java.lang.NumberFormatException r1 = new java.lang.NumberFormatException
            java.lang.String r2 = "Number too large: "
            java.lang.StringBuilder r2 = b.d.b.a.a.R(r2)
            java.lang.String r0 = r0.D()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            throw r1
        L74:
            if (r0 == 0) goto L78
            r1 = 1
            goto L8f
        L78:
            java.lang.NumberFormatException r0 = new java.lang.NumberFormatException
            java.lang.String r1 = "Expected leading [0-9a-fA-F] character but was 0x"
            java.lang.StringBuilder r1 = b.d.b.a.a.R(r1)
            java.lang.String r2 = b.i.a.f.e.o.f.t1(r10)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L8f:
            if (r8 != r9) goto L9b
            g0.s r7 = r6.a()
            r15.j = r7
            g0.t.a(r6)
            goto L9d
        L9b:
            r6.f3665b = r8
        L9d:
            if (r1 != 0) goto La3
            g0.s r6 = r15.j
            if (r6 != 0) goto Lb
        La3:
            long r1 = r15.k
            long r6 = (long) r0
            long r1 = r1 - r6
            r15.k = r1
            return r4
        Laa:
            java.io.EOFException r0 = new java.io.EOFException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: g0.e.s0():long");
    }

    @Override // g0.g
    public void skip(long j) throws EOFException {
        while (j > 0) {
            s sVar = this.j;
            if (sVar != null) {
                int min = (int) Math.min(j, sVar.c - sVar.f3665b);
                long j2 = min;
                this.k -= j2;
                j -= j2;
                int i = sVar.f3665b + min;
                sVar.f3665b = i;
                if (i == sVar.c) {
                    this.j = sVar.a();
                    t.a(sVar);
                }
            } else {
                throw new EOFException();
            }
        }
    }

    public long t(ByteString byteString, long j) {
        int i;
        int i2;
        int i3;
        int i4;
        long j2 = j;
        m.checkParameterIsNotNull(byteString, "targetBytes");
        long j3 = 0;
        if (j2 >= 0) {
            s sVar = this.j;
            if (sVar == null) {
                return -1L;
            }
            long j4 = this.k;
            if (j4 - j2 < j2) {
                while (j4 > j2) {
                    sVar = sVar.g;
                    if (sVar == null) {
                        m.throwNpe();
                    }
                    j4 -= sVar.c - sVar.f3665b;
                }
                if (byteString.j() == 2) {
                    byte m = byteString.m(0);
                    byte m2 = byteString.m(1);
                    while (j4 < this.k) {
                        byte[] bArr = sVar.a;
                        i3 = (int) ((sVar.f3665b + j2) - j4);
                        int i5 = sVar.c;
                        while (i3 < i5) {
                            byte b2 = bArr[i3];
                            if (b2 == m || b2 == m2) {
                                i4 = sVar.f3665b;
                            } else {
                                i3++;
                            }
                        }
                        j4 += sVar.c - sVar.f3665b;
                        sVar = sVar.f;
                        if (sVar == null) {
                            m.throwNpe();
                        }
                        j2 = j4;
                    }
                    return -1L;
                }
                byte[] l = byteString.l();
                while (j4 < this.k) {
                    byte[] bArr2 = sVar.a;
                    i3 = (int) ((sVar.f3665b + j2) - j4);
                    int i6 = sVar.c;
                    while (i3 < i6) {
                        byte b3 = bArr2[i3];
                        for (byte b4 : l) {
                            if (b3 == b4) {
                                i4 = sVar.f3665b;
                            }
                        }
                        i3++;
                    }
                    j4 += sVar.c - sVar.f3665b;
                    sVar = sVar.f;
                    if (sVar == null) {
                        m.throwNpe();
                    }
                    j2 = j4;
                }
                return -1L;
                return (i3 - i4) + j4;
            }
            while (true) {
                long j5 = (sVar.c - sVar.f3665b) + j3;
                if (j5 > j2) {
                    break;
                }
                sVar = sVar.f;
                if (sVar == null) {
                    m.throwNpe();
                }
                j3 = j5;
            }
            if (byteString.j() == 2) {
                byte m3 = byteString.m(0);
                byte m4 = byteString.m(1);
                while (j3 < this.k) {
                    byte[] bArr3 = sVar.a;
                    i = (int) ((sVar.f3665b + j2) - j3);
                    int i7 = sVar.c;
                    while (i < i7) {
                        byte b5 = bArr3[i];
                        if (b5 == m3 || b5 == m4) {
                            i2 = sVar.f3665b;
                        } else {
                            i++;
                        }
                    }
                    j3 += sVar.c - sVar.f3665b;
                    sVar = sVar.f;
                    if (sVar == null) {
                        m.throwNpe();
                    }
                    j2 = j3;
                }
                return -1L;
            }
            byte[] l2 = byteString.l();
            while (j3 < this.k) {
                byte[] bArr4 = sVar.a;
                i = (int) ((sVar.f3665b + j2) - j3);
                int i8 = sVar.c;
                while (i < i8) {
                    byte b6 = bArr4[i];
                    for (byte b7 : l2) {
                        if (b6 == b7) {
                            i2 = sVar.f3665b;
                        }
                    }
                    i++;
                }
                j3 += sVar.c - sVar.f3665b;
                sVar = sVar.f;
                if (sVar == null) {
                    m.throwNpe();
                }
                j2 = j3;
            }
            return -1L;
            return (i - i2) + j3;
        }
        throw new IllegalArgumentException(b.d.b.a.a.s("fromIndex < 0: ", j2).toString());
    }

    @Override // g0.x
    public y timeout() {
        return y.a;
    }

    public String toString() {
        long j = this.k;
        if (j <= ((long) Integer.MAX_VALUE)) {
            return J((int) j).toString();
        }
        StringBuilder R = b.d.b.a.a.R("size > Int.MAX_VALUE: ");
        R.append(this.k);
        throw new IllegalStateException(R.toString().toString());
    }

    public final a u(a aVar) {
        m.checkParameterIsNotNull(aVar, "unsafeCursor");
        if (aVar.j == null) {
            aVar.j = this;
            aVar.k = true;
            return aVar;
        }
        throw new IllegalStateException("already attached to a buffer".toString());
    }

    @Override // g0.g
    public InputStream u0() {
        return new b();
    }

    @Override // g0.g
    public int v0(o oVar) {
        m.checkParameterIsNotNull(oVar, "options");
        int b2 = g0.z.a.b(this, oVar, false);
        if (b2 == -1) {
            return -1;
        }
        skip(oVar.l[b2].j());
        return b2;
    }

    @Override // g0.g
    public boolean w() {
        return this.k == 0;
    }

    @Override // okio.BufferedSink
    public /* bridge */ /* synthetic */ BufferedSink write(byte[] bArr) {
        R(bArr);
        return this;
    }

    @Override // okio.BufferedSink
    public /* bridge */ /* synthetic */ BufferedSink writeByte(int i) {
        T(i);
        return this;
    }

    @Override // okio.BufferedSink
    public /* bridge */ /* synthetic */ BufferedSink writeInt(int i) {
        W(i);
        return this;
    }

    @Override // okio.BufferedSink
    public /* bridge */ /* synthetic */ BufferedSink writeShort(int i) {
        X(i);
        return this;
    }

    public ByteString x() {
        return o(this.k);
    }

    public short y() throws EOFException {
        int readShort = readShort() & 65535;
        return (short) (((readShort & 255) << 8) | ((65280 & readShort) >>> 8));
    }

    @Override // okio.BufferedSink
    public /* bridge */ /* synthetic */ BufferedSink write(byte[] bArr, int i, int i2) {
        S(bArr, i, i2);
        return this;
    }

    /* compiled from: Buffer.kt */
    /* loaded from: classes3.dex */
    public static final class b extends InputStream {
        public b() {
        }

        @Override // java.io.InputStream
        public int available() {
            return (int) Math.min(e.this.k, Integer.MAX_VALUE);
        }

        @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
        }

        @Override // java.io.InputStream
        public int read() {
            e eVar = e.this;
            if (eVar.k > 0) {
                return eVar.readByte() & 255;
            }
            return -1;
        }

        public String toString() {
            return e.this + ".inputStream()";
        }

        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) {
            m.checkParameterIsNotNull(bArr, "sink");
            return e.this.read(bArr, i, i2);
        }
    }

    @Override // g0.v
    public void write(e eVar, long j) {
        s sVar;
        int i;
        s sVar2;
        s sVar3;
        m.checkParameterIsNotNull(eVar, "source");
        if (eVar != this) {
            f.B(eVar.k, 0L, j);
            long j2 = j;
            while (j2 > 0) {
                s sVar4 = eVar.j;
                if (sVar4 == null) {
                    m.throwNpe();
                }
                int i2 = sVar4.c;
                if (eVar.j == null) {
                    m.throwNpe();
                }
                if (j2 < i2 - sVar.f3665b) {
                    s sVar5 = this.j;
                    if (sVar5 != null) {
                        if (sVar5 == null) {
                            m.throwNpe();
                        }
                        sVar2 = sVar5.g;
                    } else {
                        sVar2 = null;
                    }
                    if (sVar2 != null && sVar2.e) {
                        if ((sVar2.c + j2) - (sVar2.d ? 0 : sVar2.f3665b) <= 8192) {
                            s sVar6 = eVar.j;
                            if (sVar6 == null) {
                                m.throwNpe();
                            }
                            sVar6.d(sVar2, (int) j2);
                            eVar.k -= j2;
                            this.k += j2;
                            return;
                        }
                    }
                    s sVar7 = eVar.j;
                    if (sVar7 == null) {
                        m.throwNpe();
                    }
                    int i3 = (int) j2;
                    Objects.requireNonNull(sVar7);
                    if (i3 > 0 && i3 <= sVar7.c - sVar7.f3665b) {
                        if (i3 >= 1024) {
                            sVar3 = sVar7.c();
                        } else {
                            sVar3 = t.b();
                            byte[] bArr = sVar7.a;
                            byte[] bArr2 = sVar3.a;
                            int i4 = sVar7.f3665b;
                            j.copyInto$default(bArr, bArr2, 0, i4, i4 + i3, 2, (Object) null);
                        }
                        sVar3.c = sVar3.f3665b + i3;
                        sVar7.f3665b += i3;
                        s sVar8 = sVar7.g;
                        if (sVar8 == null) {
                            m.throwNpe();
                        }
                        sVar8.b(sVar3);
                        eVar.j = sVar3;
                    } else {
                        throw new IllegalArgumentException("byteCount out of range".toString());
                    }
                }
                s sVar9 = eVar.j;
                if (sVar9 == null) {
                    m.throwNpe();
                }
                long j3 = sVar9.c - sVar9.f3665b;
                eVar.j = sVar9.a();
                s sVar10 = this.j;
                if (sVar10 == null) {
                    this.j = sVar9;
                    sVar9.g = sVar9;
                    sVar9.f = sVar9;
                } else {
                    if (sVar10 == null) {
                        m.throwNpe();
                    }
                    s sVar11 = sVar10.g;
                    if (sVar11 == null) {
                        m.throwNpe();
                    }
                    sVar11.b(sVar9);
                    s sVar12 = sVar9.g;
                    if (sVar12 != sVar9) {
                        if (sVar12 == null) {
                            m.throwNpe();
                        }
                        if (sVar12.e) {
                            int i5 = sVar9.c - sVar9.f3665b;
                            s sVar13 = sVar9.g;
                            if (sVar13 == null) {
                                m.throwNpe();
                            }
                            int i6 = 8192 - sVar13.c;
                            s sVar14 = sVar9.g;
                            if (sVar14 == null) {
                                m.throwNpe();
                            }
                            if (sVar14.d) {
                                i = 0;
                            } else {
                                s sVar15 = sVar9.g;
                                if (sVar15 == null) {
                                    m.throwNpe();
                                }
                                i = sVar15.f3665b;
                            }
                            if (i5 <= i6 + i) {
                                s sVar16 = sVar9.g;
                                if (sVar16 == null) {
                                    m.throwNpe();
                                }
                                sVar9.d(sVar16, i5);
                                sVar9.a();
                                t.a(sVar9);
                            }
                        }
                    } else {
                        throw new IllegalStateException("cannot compact".toString());
                    }
                }
                eVar.k -= j3;
                this.k += j3;
                j2 -= j3;
            }
            return;
        }
        throw new IllegalArgumentException("source == this".toString());
    }

    public int read(byte[] bArr, int i, int i2) {
        m.checkParameterIsNotNull(bArr, "sink");
        f.B(bArr.length, i, i2);
        s sVar = this.j;
        if (sVar == null) {
            return -1;
        }
        int min = Math.min(i2, sVar.c - sVar.f3665b);
        byte[] bArr2 = sVar.a;
        int i3 = sVar.f3665b;
        j.copyInto(bArr2, bArr, i, i3, i3 + min);
        int i4 = sVar.f3665b + min;
        sVar.f3665b = i4;
        this.k -= min;
        if (i4 != sVar.c) {
            return min;
        }
        this.j = sVar.a();
        t.a(sVar);
        return min;
    }

    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer byteBuffer) throws IOException {
        m.checkParameterIsNotNull(byteBuffer, "source");
        int remaining = byteBuffer.remaining();
        int i = remaining;
        while (i > 0) {
            s N = N(1);
            int min = Math.min(i, 8192 - N.c);
            byteBuffer.get(N.a, N.c, min);
            i -= min;
            N.c += min;
        }
        this.k += remaining;
        return remaining;
    }
}
