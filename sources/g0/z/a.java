package g0.z;

import d0.g0.c;
import d0.z.d.m;
import g0.e;
/* compiled from: Buffer.kt */
/* loaded from: classes3.dex */
public final class a {
    public static final byte[] a;

    static {
        m.checkParameterIsNotNull("0123456789abcdef", "$this$asUtf8ToByteArray");
        byte[] bytes = "0123456789abcdef".getBytes(c.a);
        m.checkExpressionValueIsNotNull(bytes, "(this as java.lang.String).getBytes(charset)");
        a = bytes;
    }

    public static final String a(e eVar, long j) {
        m.checkParameterIsNotNull(eVar, "$this$readUtf8Line");
        if (j > 0) {
            long j2 = j - 1;
            if (eVar.q(j2) == ((byte) 13)) {
                String H = eVar.H(j2);
                eVar.skip(2L);
                return H;
            }
        }
        String H2 = eVar.H(j);
        eVar.skip(1L);
        return H2;
    }

    /* JADX WARN: Code restructure failed: missing block: B:27:0x005f, code lost:
        if (r19 == false) goto L29;
     */
    /* JADX WARN: Code restructure failed: missing block: B:28:0x0061, code lost:
        return -2;
     */
    /* JADX WARN: Code restructure failed: missing block: B:29:0x0062, code lost:
        return r10;
     */
    /*
        Code decompiled incorrectly, please refer to instructions dump.
        To view partially-correct add '--show-bad-code' argument
    */
    public static final int b(g0.e r17, g0.o r18, boolean r19) {
        /*
            Method dump skipped, instructions count: 181
            To view this dump add '--comments-level debug' option
        */
        throw new UnsupportedOperationException("Method not decompiled: g0.z.a.b(g0.e, g0.o, boolean):int");
    }
}
