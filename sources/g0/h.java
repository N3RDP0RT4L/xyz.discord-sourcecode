package g0;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import d0.z.d.m;
import java.io.IOException;
import java.util.zip.Deflater;
import okio.BufferedSink;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
/* compiled from: DeflaterSink.kt */
/* loaded from: classes3.dex */
public final class h implements v {
    public boolean j;
    public final BufferedSink k;
    public final Deflater l;

    public h(v vVar, Deflater deflater) {
        m.checkParameterIsNotNull(vVar, "sink");
        m.checkParameterIsNotNull(deflater, "deflater");
        m.checkParameterIsNotNull(vVar, "$this$buffer");
        q qVar = new q(vVar);
        m.checkParameterIsNotNull(qVar, "sink");
        m.checkParameterIsNotNull(deflater, "deflater");
        this.k = qVar;
        this.l = deflater;
    }

    @IgnoreJRERequirement
    public final void a(boolean z2) {
        s N;
        int i;
        e g = this.k.g();
        while (true) {
            N = g.N(1);
            if (z2) {
                Deflater deflater = this.l;
                byte[] bArr = N.a;
                int i2 = N.c;
                i = deflater.deflate(bArr, i2, 8192 - i2, 2);
            } else {
                Deflater deflater2 = this.l;
                byte[] bArr2 = N.a;
                int i3 = N.c;
                i = deflater2.deflate(bArr2, i3, 8192 - i3);
            }
            if (i > 0) {
                N.c += i;
                g.k += i;
                this.k.F();
            } else if (this.l.needsInput()) {
                break;
            }
        }
        if (N.f3665b == N.c) {
            g.j = N.a();
            t.a(N);
        }
    }

    @Override // g0.v, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        if (!this.j) {
            Throwable th = null;
            try {
                this.l.finish();
                a(false);
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.l.end();
            } catch (Throwable th3) {
                th = th3;
                if (th == null) {
                }
            }
            try {
                this.k.close();
            } catch (Throwable th4) {
                th = th4;
                if (th == null) {
                }
            }
            this.j = true;
            if (th != null) {
                throw th;
            }
        }
    }

    @Override // g0.v, java.io.Flushable
    public void flush() throws IOException {
        a(true);
        this.k.flush();
    }

    @Override // g0.v
    public y timeout() {
        return this.k.timeout();
    }

    public String toString() {
        StringBuilder R = a.R("DeflaterSink(");
        R.append(this.k);
        R.append(')');
        return R.toString();
    }

    @Override // g0.v
    public void write(e eVar, long j) throws IOException {
        m.checkParameterIsNotNull(eVar, "source");
        f.B(eVar.k, 0L, j);
        while (j > 0) {
            s sVar = eVar.j;
            if (sVar == null) {
                m.throwNpe();
            }
            int min = (int) Math.min(j, sVar.c - sVar.f3665b);
            this.l.setInput(sVar.a, sVar.f3665b, min);
            a(false);
            long j2 = min;
            eVar.k -= j2;
            int i = sVar.f3665b + min;
            sVar.f3665b = i;
            if (i == sVar.c) {
                eVar.j = sVar.a();
                t.a(sVar);
            }
            j -= j2;
        }
    }
}
