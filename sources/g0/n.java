package g0;

import b.d.b.a.a;
import b.i.a.f.e.o.f;
import d0.z.d.m;
import java.io.IOException;
import java.io.InputStream;
/* compiled from: JvmOkio.kt */
/* loaded from: classes3.dex */
public final class n implements x {
    public final InputStream j;
    public final y k;

    public n(InputStream inputStream, y yVar) {
        m.checkParameterIsNotNull(inputStream, "input");
        m.checkParameterIsNotNull(yVar, "timeout");
        this.j = inputStream;
        this.k = yVar;
    }

    @Override // g0.x, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.j.close();
    }

    @Override // g0.x
    public long i0(e eVar, long j) {
        m.checkParameterIsNotNull(eVar, "sink");
        int i = (j > 0L ? 1 : (j == 0L ? 0 : -1));
        if (i == 0) {
            return 0L;
        }
        if (i >= 0) {
            try {
                this.k.f();
                s N = eVar.N(1);
                int read = this.j.read(N.a, N.c, (int) Math.min(j, 8192 - N.c));
                if (read != -1) {
                    N.c += read;
                    long j2 = read;
                    eVar.k += j2;
                    return j2;
                } else if (N.f3665b != N.c) {
                    return -1L;
                } else {
                    eVar.j = N.a();
                    t.a(N);
                    return -1L;
                }
            } catch (AssertionError e) {
                if (f.z0(e)) {
                    throw new IOException(e);
                }
                throw e;
            }
        } else {
            throw new IllegalArgumentException(a.s("byteCount < 0: ", j).toString());
        }
    }

    @Override // g0.x
    public y timeout() {
        return this.k;
    }

    public String toString() {
        StringBuilder R = a.R("source(");
        R.append(this.j);
        R.append(')');
        return R.toString();
    }
}
