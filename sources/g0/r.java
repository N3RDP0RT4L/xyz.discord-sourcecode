package g0;

import androidx.recyclerview.widget.RecyclerView;
import b.i.a.f.e.o.f;
import d0.z.d.m;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import okio.ByteString;
/* compiled from: RealBufferedSource.kt */
/* loaded from: classes3.dex */
public final class r implements g {
    public final e j = new e();
    public boolean k;
    public final x l;

    public r(x xVar) {
        m.checkParameterIsNotNull(xVar, "source");
        this.l = xVar;
    }

    @Override // g0.g
    public void B(e eVar, long j) {
        m.checkParameterIsNotNull(eVar, "sink");
        try {
            if (j(j)) {
                this.j.B(eVar, j);
                return;
            }
            throw new EOFException();
        } catch (EOFException e) {
            eVar.P(this.j);
            throw e;
        }
    }

    @Override // g0.g
    public long E(ByteString byteString) {
        m.checkParameterIsNotNull(byteString, "targetBytes");
        m.checkParameterIsNotNull(byteString, "targetBytes");
        if (!this.k) {
            long j = 0;
            while (true) {
                long t = this.j.t(byteString, j);
                if (t != -1) {
                    return t;
                }
                e eVar = this.j;
                long j2 = eVar.k;
                if (this.l.i0(eVar, 8192) == -1) {
                    return -1L;
                }
                j = Math.max(j, j2);
            }
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    @Override // g0.g
    public String G(long j) {
        if (j >= 0) {
            long j2 = j == RecyclerView.FOREVER_NS ? Long.MAX_VALUE : j + 1;
            byte b2 = (byte) 10;
            long a2 = a(b2, 0L, j2);
            if (a2 != -1) {
                return g0.z.a.a(this.j, a2);
            }
            if (j2 < RecyclerView.FOREVER_NS && j(j2) && this.j.q(j2 - 1) == ((byte) 13) && j(1 + j2) && this.j.q(j2) == b2) {
                return g0.z.a.a(this.j, j2);
            }
            e eVar = new e();
            e eVar2 = this.j;
            eVar2.n(eVar, 0L, Math.min(32, eVar2.k));
            throw new EOFException("\\n not found: limit=" + Math.min(this.j.k, j) + " content=" + eVar.x().k() + "…");
        }
        throw new IllegalArgumentException(b.d.b.a.a.s("limit < 0: ", j).toString());
    }

    @Override // g0.g
    public String M(Charset charset) {
        m.checkParameterIsNotNull(charset, "charset");
        this.j.P(this.l);
        return this.j.M(charset);
    }

    @Override // g0.g
    public String Y() {
        return G(RecyclerView.FOREVER_NS);
    }

    @Override // g0.g
    public byte[] Z(long j) {
        if (j(j)) {
            return this.j.Z(j);
        }
        throw new EOFException();
    }

    public long a(byte b2, long j, long j2) {
        boolean z2 = true;
        if (!this.k) {
            if (0 > j || j2 < j) {
                z2 = false;
            }
            if (z2) {
                while (j < j2) {
                    long s2 = this.j.s(b2, j, j2);
                    if (s2 != -1) {
                        return s2;
                    }
                    e eVar = this.j;
                    long j3 = eVar.k;
                    if (j3 >= j2 || this.l.i0(eVar, 8192) == -1) {
                        return -1L;
                    }
                    j = Math.max(j, j3);
                }
                return -1L;
            }
            throw new IllegalArgumentException(("fromIndex=" + j + " toIndex=" + j2).toString());
        }
        throw new IllegalStateException("closed".toString());
    }

    public int b() {
        p0(4L);
        int readInt = this.j.readInt();
        return ((readInt & 255) << 24) | (((-16777216) & readInt) >>> 24) | ((16711680 & readInt) >>> 8) | ((65280 & readInt) << 8);
    }

    @Override // g0.x, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        if (!this.k) {
            this.k = true;
            this.l.close();
            e eVar = this.j;
            eVar.skip(eVar.k);
        }
    }

    @Override // g0.g, okio.BufferedSink
    public e g() {
        return this.j;
    }

    @Override // g0.x
    public long i0(e eVar, long j) {
        m.checkParameterIsNotNull(eVar, "sink");
        if (!(j >= 0)) {
            throw new IllegalArgumentException(b.d.b.a.a.s("byteCount < 0: ", j).toString());
        } else if (!this.k) {
            e eVar2 = this.j;
            if (eVar2.k == 0 && this.l.i0(eVar2, 8192) == -1) {
                return -1L;
            }
            return this.j.i0(eVar, Math.min(j, this.j.k));
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    @Override // java.nio.channels.Channel
    public boolean isOpen() {
        return !this.k;
    }

    @Override // g0.g
    public boolean j(long j) {
        e eVar;
        if (!(j >= 0)) {
            throw new IllegalArgumentException(b.d.b.a.a.s("byteCount < 0: ", j).toString());
        } else if (!this.k) {
            do {
                eVar = this.j;
                if (eVar.k >= j) {
                    return true;
                }
            } while (this.l.i0(eVar, 8192) != -1);
            return false;
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    @Override // g0.g
    public long k0(v vVar) {
        m.checkParameterIsNotNull(vVar, "sink");
        long j = 0;
        while (this.l.i0(this.j, 8192) != -1) {
            long f = this.j.f();
            if (f > 0) {
                j += f;
                vVar.write(this.j, f);
            }
        }
        e eVar = this.j;
        long j2 = eVar.k;
        if (j2 <= 0) {
            return j;
        }
        long j3 = j + j2;
        vVar.write(eVar, j2);
        return j3;
    }

    @Override // g0.g
    public ByteString o(long j) {
        if (j(j)) {
            return this.j.o(j);
        }
        throw new EOFException();
    }

    @Override // g0.g
    public void p0(long j) {
        if (!j(j)) {
            throw new EOFException();
        }
    }

    @Override // java.nio.channels.ReadableByteChannel
    public int read(ByteBuffer byteBuffer) {
        m.checkParameterIsNotNull(byteBuffer, "sink");
        e eVar = this.j;
        if (eVar.k == 0 && this.l.i0(eVar, 8192) == -1) {
            return -1;
        }
        return this.j.read(byteBuffer);
    }

    @Override // g0.g
    public byte readByte() {
        p0(1L);
        return this.j.readByte();
    }

    @Override // g0.g
    public void readFully(byte[] bArr) {
        m.checkParameterIsNotNull(bArr, "sink");
        try {
            p0(bArr.length);
            this.j.readFully(bArr);
        } catch (EOFException e) {
            int i = 0;
            while (true) {
                e eVar = this.j;
                long j = eVar.k;
                if (j > 0) {
                    int read = eVar.read(bArr, i, (int) j);
                    if (read != -1) {
                        i += read;
                    } else {
                        throw new AssertionError();
                    }
                } else {
                    throw e;
                }
            }
        }
    }

    @Override // g0.g
    public int readInt() {
        p0(4L);
        return this.j.readInt();
    }

    @Override // g0.g
    public long readLong() {
        p0(8L);
        return this.j.readLong();
    }

    @Override // g0.g
    public short readShort() {
        p0(2L);
        return this.j.readShort();
    }

    @Override // g0.g
    public long s0() {
        byte q;
        p0(1L);
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (!j(i2)) {
                break;
            }
            q = this.j.q(i);
            if ((q < ((byte) 48) || q > ((byte) 57)) && ((q < ((byte) 97) || q > ((byte) 102)) && (q < ((byte) 65) || q > ((byte) 70)))) {
                break;
            }
            i = i2;
        }
        if (i == 0) {
            StringBuilder R = b.d.b.a.a.R("Expected leading [0-9a-fA-F] character but was 0x");
            String num = Integer.toString(q, d0.g0.a.checkRadix(d0.g0.a.checkRadix(16)));
            m.checkExpressionValueIsNotNull(num, "java.lang.Integer.toStri…(this, checkRadix(radix))");
            R.append(num);
            throw new NumberFormatException(R.toString());
        }
        return this.j.s0();
    }

    @Override // g0.g
    public void skip(long j) {
        if (!this.k) {
            while (j > 0) {
                e eVar = this.j;
                if (eVar.k == 0 && this.l.i0(eVar, 8192) == -1) {
                    throw new EOFException();
                }
                long min = Math.min(j, this.j.k);
                this.j.skip(min);
                j -= min;
            }
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // g0.x
    public y timeout() {
        return this.l.timeout();
    }

    public String toString() {
        StringBuilder R = b.d.b.a.a.R("buffer(");
        R.append(this.l);
        R.append(')');
        return R.toString();
    }

    @Override // g0.g
    public InputStream u0() {
        return new a();
    }

    @Override // g0.g
    public int v0(o oVar) {
        m.checkParameterIsNotNull(oVar, "options");
        if (!this.k) {
            while (true) {
                int b2 = g0.z.a.b(this.j, oVar, true);
                if (b2 == -2) {
                    if (this.l.i0(this.j, 8192) == -1) {
                        break;
                    }
                } else if (b2 != -1) {
                    this.j.skip(oVar.l[b2].j());
                    return b2;
                }
            }
            return -1;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // g0.g
    public boolean w() {
        if (!this.k) {
            return this.j.w() && this.l.i0(this.j, (long) 8192) == -1;
        }
        throw new IllegalStateException("closed".toString());
    }

    /* compiled from: RealBufferedSource.kt */
    /* loaded from: classes3.dex */
    public static final class a extends InputStream {
        public a() {
        }

        @Override // java.io.InputStream
        public int available() {
            r rVar = r.this;
            if (!rVar.k) {
                return (int) Math.min(rVar.j.k, Integer.MAX_VALUE);
            }
            throw new IOException("closed");
        }

        @Override // java.io.InputStream, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            r.this.close();
        }

        @Override // java.io.InputStream
        public int read() {
            r rVar = r.this;
            if (!rVar.k) {
                e eVar = rVar.j;
                if (eVar.k == 0 && rVar.l.i0(eVar, 8192) == -1) {
                    return -1;
                }
                return r.this.j.readByte() & 255;
            }
            throw new IOException("closed");
        }

        public String toString() {
            return r.this + ".inputStream()";
        }

        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) {
            m.checkParameterIsNotNull(bArr, "data");
            if (!r.this.k) {
                f.B(bArr.length, i, i2);
                r rVar = r.this;
                e eVar = rVar.j;
                if (eVar.k == 0 && rVar.l.i0(eVar, 8192) == -1) {
                    return -1;
                }
                return r.this.j.read(bArr, i, i2);
            }
            throw new IOException("closed");
        }
    }
}
