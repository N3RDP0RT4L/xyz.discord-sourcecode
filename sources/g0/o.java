package g0;

import d0.t.c;
import java.util.List;
import java.util.RandomAccess;
import kotlin.jvm.internal.DefaultConstructorMarker;
import okio.ByteString;
/* compiled from: Options.kt */
/* loaded from: classes3.dex */
public final class o extends c<ByteString> implements RandomAccess {
    public static final a k = new a(null);
    public final ByteString[] l;
    public final int[] m;

    /* compiled from: Options.kt */
    /* loaded from: classes3.dex */
    public static final class a {
        public a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final void a(long j, e eVar, int i, List<? extends ByteString> list, int i2, int i3, List<Integer> list2) {
            int i4;
            int i5;
            int i6;
            int i7;
            e eVar2;
            int i8 = i;
            if (i2 < i3) {
                for (int i9 = i2; i9 < i3; i9++) {
                    if (!(list.get(i9).j() >= i8)) {
                        throw new IllegalArgumentException("Failed requirement.".toString());
                    }
                }
                ByteString byteString = list.get(i2);
                ByteString byteString2 = list.get(i3 - 1);
                if (i8 == byteString.j()) {
                    i5 = list2.get(i2).intValue();
                    int i10 = i2 + 1;
                    byteString = list.get(i10);
                    i4 = i10;
                } else {
                    i4 = i2;
                    i5 = -1;
                }
                if (byteString.m(i8) != byteString2.m(i8)) {
                    int i11 = 1;
                    for (int i12 = i4 + 1; i12 < i3; i12++) {
                        if (list.get(i12 - 1).m(i8) != list.get(i12).m(i8)) {
                            i11++;
                        }
                    }
                    long b2 = b(eVar) + j + 2 + (i11 * 2);
                    eVar.W(i11);
                    eVar.W(i5);
                    for (int i13 = i4; i13 < i3; i13++) {
                        byte m = list.get(i13).m(i8);
                        if (i13 == i4 || m != list.get(i13 - 1).m(i8)) {
                            eVar.W(m & 255);
                        }
                    }
                    e eVar3 = new e();
                    while (i4 < i3) {
                        byte m2 = list.get(i4).m(i8);
                        int i14 = i4 + 1;
                        int i15 = i14;
                        while (true) {
                            if (i15 >= i3) {
                                i6 = i3;
                                break;
                            } else if (m2 != list.get(i15).m(i8)) {
                                i6 = i15;
                                break;
                            } else {
                                i15++;
                            }
                        }
                        if (i14 == i6 && i8 + 1 == list.get(i4).j()) {
                            eVar.W(list2.get(i4).intValue());
                            i7 = i6;
                            eVar2 = eVar3;
                        } else {
                            eVar.W(((int) (b(eVar3) + b2)) * (-1));
                            i7 = i6;
                            eVar2 = eVar3;
                            a(b2, eVar3, i8 + 1, list, i4, i6, list2);
                        }
                        eVar3 = eVar2;
                        i4 = i7;
                    }
                    eVar.P(eVar3);
                    return;
                }
                int min = Math.min(byteString.j(), byteString2.j());
                int i16 = 0;
                for (int i17 = i8; i17 < min && byteString.m(i17) == byteString2.m(i17); i17++) {
                    i16++;
                }
                long b3 = b(eVar) + j + 2 + i16 + 1;
                eVar.W(-i16);
                eVar.W(i5);
                int i18 = i8 + i16;
                while (i8 < i18) {
                    eVar.W(byteString.m(i8) & 255);
                    i8++;
                }
                if (i4 + 1 == i3) {
                    if (i18 == list.get(i4).j()) {
                        eVar.W(list2.get(i4).intValue());
                        return;
                    }
                    throw new IllegalStateException("Check failed.".toString());
                }
                e eVar4 = new e();
                eVar.W(((int) (b(eVar4) + b3)) * (-1));
                a(b3, eVar4, i18, list, i4, i3, list2);
                eVar.P(eVar4);
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }

        public final long b(e eVar) {
            return eVar.k / 4;
        }

        /* JADX WARN: Code restructure failed: missing block: B:58:0x00fa, code lost:
            continue;
         */
        /*
            Code decompiled incorrectly, please refer to instructions dump.
            To view partially-correct add '--show-bad-code' argument
        */
        public final g0.o c(okio.ByteString... r17) {
            /*
                Method dump skipped, instructions count: 348
                To view this dump add '--comments-level debug' option
            */
            throw new UnsupportedOperationException("Method not decompiled: g0.o.a.c(okio.ByteString[]):g0.o");
        }
    }

    public o(ByteString[] byteStringArr, int[] iArr, DefaultConstructorMarker defaultConstructorMarker) {
        this.l = byteStringArr;
        this.m = iArr;
    }

    @Override // d0.t.a, java.util.Collection, java.util.List
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof ByteString) {
            return super.contains((ByteString) obj);
        }
        return false;
    }

    @Override // d0.t.c, java.util.List
    public Object get(int i) {
        return this.l[i];
    }

    @Override // d0.t.a
    public int getSize() {
        return this.l.length;
    }

    @Override // d0.t.c, java.util.List
    public final /* bridge */ int indexOf(Object obj) {
        if (obj instanceof ByteString) {
            return super.indexOf((ByteString) obj);
        }
        return -1;
    }

    @Override // d0.t.c, java.util.List
    public final /* bridge */ int lastIndexOf(Object obj) {
        if (obj instanceof ByteString) {
            return super.lastIndexOf((ByteString) obj);
        }
        return -1;
    }
}
