package g0;

import d0.z.d.m;
import java.io.IOException;
/* compiled from: ForwardingSource.kt */
/* loaded from: classes3.dex */
public abstract class j implements x {
    public final x j;

    public j(x xVar) {
        m.checkParameterIsNotNull(xVar, "delegate");
        this.j = xVar;
    }

    @Override // g0.x, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.j.close();
    }

    @Override // g0.x
    public y timeout() {
        return this.j.timeout();
    }

    public String toString() {
        return getClass().getSimpleName() + '(' + this.j + ')';
    }
}
