package g0;

import b.d.b.a.a;
import d0.z.d.m;
import java.nio.ByteBuffer;
import okio.BufferedSink;
import okio.ByteString;
/* compiled from: RealBufferedSink.kt */
/* loaded from: classes3.dex */
public final class q implements BufferedSink {
    public final e j = new e();
    public boolean k;
    public final v l;

    public q(v vVar) {
        m.checkParameterIsNotNull(vVar, "sink");
        this.l = vVar;
    }

    @Override // okio.BufferedSink
    public BufferedSink F() {
        if (!this.k) {
            long f = this.j.f();
            if (f > 0) {
                this.l.write(this.j, f);
            }
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.BufferedSink
    public BufferedSink K(String str) {
        m.checkParameterIsNotNull(str, "string");
        if (!this.k) {
            this.j.b0(str);
            F();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.BufferedSink
    public long P(x xVar) {
        m.checkParameterIsNotNull(xVar, "source");
        long j = 0;
        while (true) {
            long i02 = ((n) xVar).i0(this.j, 8192);
            if (i02 == -1) {
                return j;
            }
            j += i02;
            F();
        }
    }

    @Override // okio.BufferedSink
    public BufferedSink Q(long j) {
        if (!this.k) {
            this.j.Q(j);
            return F();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // g0.v, java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        if (!this.k) {
            Throwable th = null;
            try {
                e eVar = this.j;
                long j = eVar.k;
                if (j > 0) {
                    this.l.write(eVar, j);
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.l.close();
            } catch (Throwable th3) {
                th = th3;
                if (th == null) {
                }
            }
            this.k = true;
            if (th != null) {
                throw th;
            }
        }
    }

    @Override // okio.BufferedSink
    public BufferedSink e0(ByteString byteString) {
        m.checkParameterIsNotNull(byteString, "byteString");
        if (!this.k) {
            this.j.O(byteString);
            F();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.BufferedSink, g0.v, java.io.Flushable
    public void flush() {
        if (!this.k) {
            e eVar = this.j;
            long j = eVar.k;
            if (j > 0) {
                this.l.write(eVar, j);
            }
            this.l.flush();
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.BufferedSink
    public e g() {
        return this.j;
    }

    @Override // java.nio.channels.Channel
    public boolean isOpen() {
        return !this.k;
    }

    @Override // okio.BufferedSink
    public BufferedSink p() {
        if (!this.k) {
            e eVar = this.j;
            long j = eVar.k;
            if (j > 0) {
                this.l.write(eVar, j);
            }
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.BufferedSink
    public BufferedSink q0(long j) {
        if (!this.k) {
            this.j.q0(j);
            F();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // g0.v
    public y timeout() {
        return this.l.timeout();
    }

    public String toString() {
        StringBuilder R = a.R("buffer(");
        R.append(this.l);
        R.append(')');
        return R.toString();
    }

    @Override // java.nio.channels.WritableByteChannel
    public int write(ByteBuffer byteBuffer) {
        m.checkParameterIsNotNull(byteBuffer, "source");
        if (!this.k) {
            int write = this.j.write(byteBuffer);
            F();
            return write;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.BufferedSink
    public BufferedSink writeByte(int i) {
        if (!this.k) {
            this.j.T(i);
            return F();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.BufferedSink
    public BufferedSink writeInt(int i) {
        if (!this.k) {
            this.j.W(i);
            return F();
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.BufferedSink
    public BufferedSink writeShort(int i) {
        if (!this.k) {
            this.j.X(i);
            F();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // g0.v
    public void write(e eVar, long j) {
        m.checkParameterIsNotNull(eVar, "source");
        if (!this.k) {
            this.j.write(eVar, j);
            F();
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.BufferedSink
    public BufferedSink write(byte[] bArr) {
        m.checkParameterIsNotNull(bArr, "source");
        if (!this.k) {
            this.j.R(bArr);
            F();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    @Override // okio.BufferedSink
    public BufferedSink write(byte[] bArr, int i, int i2) {
        m.checkParameterIsNotNull(bArr, "source");
        if (!this.k) {
            this.j.S(bArr, i, i2);
            F();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }
}
