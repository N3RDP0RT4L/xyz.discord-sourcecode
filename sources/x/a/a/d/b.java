package x.a.a.d;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
/* compiled from: ITrustedWebActivityService.java */
/* loaded from: classes.dex */
public interface b extends IInterface {

    /* compiled from: ITrustedWebActivityService.java */
    /* loaded from: classes.dex */
    public static abstract class a extends Binder implements b {
        private static final String DESCRIPTOR = "android.support.customtabs.trusted.ITrustedWebActivityService";
        public static final int TRANSACTION_areNotificationsEnabled = 6;
        public static final int TRANSACTION_cancelNotification = 3;
        public static final int TRANSACTION_extraCommand = 9;
        public static final int TRANSACTION_getActiveNotifications = 5;
        public static final int TRANSACTION_getSmallIconBitmap = 7;
        public static final int TRANSACTION_getSmallIconId = 4;
        public static final int TRANSACTION_notifyNotificationWithChannel = 2;

        /* compiled from: ITrustedWebActivityService.java */
        /* renamed from: x.a.a.d.b$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0435a implements b {
            public static b a;

            /* renamed from: b  reason: collision with root package name */
            public IBinder f3826b;

            public C0435a(IBinder iBinder) {
                this.f3826b = iBinder;
            }

            @Override // x.a.a.d.b
            public Bundle areNotificationsEnabled(Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!this.f3826b.transact(6, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().areNotificationsEnabled(bundle);
                    }
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // android.os.IInterface
            public IBinder asBinder() {
                return this.f3826b;
            }

            @Override // x.a.a.d.b
            public void cancelNotification(Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (this.f3826b.transact(3, obtain, obtain2, 0) || a.getDefaultImpl() == null) {
                        obtain2.readException();
                    } else {
                        a.getDefaultImpl().cancelNotification(bundle);
                    }
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.d.b
            public Bundle extraCommand(String str, Bundle bundle, IBinder iBinder) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(iBinder);
                    if (!this.f3826b.transact(9, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().extraCommand(str, bundle, iBinder);
                    }
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.d.b
            public Bundle getActiveNotifications() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    if (!this.f3826b.transact(5, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().getActiveNotifications();
                    }
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.d.b
            public Bundle getSmallIconBitmap() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    if (!this.f3826b.transact(7, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().getSmallIconBitmap();
                    }
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.d.b
            public int getSmallIconId() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    if (!this.f3826b.transact(4, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().getSmallIconId();
                    }
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.d.b
            public Bundle notifyNotificationWithChannel(Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!this.f3826b.transact(2, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().notifyNotificationWithChannel(bundle);
                    }
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public a() {
            attachInterface(this, DESCRIPTOR);
        }

        public static b asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof b)) {
                return new C0435a(iBinder);
            }
            return (b) queryLocalInterface;
        }

        public static b getDefaultImpl() {
            return C0435a.a;
        }

        public static boolean setDefaultImpl(b bVar) {
            if (C0435a.a != null) {
                throw new IllegalStateException("setDefaultImpl() called twice");
            } else if (bVar == null) {
                return false;
            } else {
                C0435a.a = bVar;
                return true;
            }
        }

        @Override // android.os.IInterface
        public IBinder asBinder() {
            return this;
        }

        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            Bundle bundle = null;
            if (i == 9) {
                parcel.enforceInterface(DESCRIPTOR);
                String readString = parcel.readString();
                if (parcel.readInt() != 0) {
                    bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                }
                Bundle extraCommand = extraCommand(readString, bundle, parcel.readStrongBinder());
                parcel2.writeNoException();
                if (extraCommand != null) {
                    parcel2.writeInt(1);
                    extraCommand.writeToParcel(parcel2, 1);
                } else {
                    parcel2.writeInt(0);
                }
                return true;
            } else if (i != 1598968902) {
                switch (i) {
                    case 2:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        Bundle notifyNotificationWithChannel = notifyNotificationWithChannel(bundle);
                        parcel2.writeNoException();
                        if (notifyNotificationWithChannel != null) {
                            parcel2.writeInt(1);
                            notifyNotificationWithChannel.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    case 3:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        cancelNotification(bundle);
                        parcel2.writeNoException();
                        return true;
                    case 4:
                        parcel.enforceInterface(DESCRIPTOR);
                        int smallIconId = getSmallIconId();
                        parcel2.writeNoException();
                        parcel2.writeInt(smallIconId);
                        return true;
                    case 5:
                        parcel.enforceInterface(DESCRIPTOR);
                        Bundle activeNotifications = getActiveNotifications();
                        parcel2.writeNoException();
                        if (activeNotifications != null) {
                            parcel2.writeInt(1);
                            activeNotifications.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    case 6:
                        parcel.enforceInterface(DESCRIPTOR);
                        if (parcel.readInt() != 0) {
                            bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        Bundle areNotificationsEnabled = areNotificationsEnabled(bundle);
                        parcel2.writeNoException();
                        if (areNotificationsEnabled != null) {
                            parcel2.writeInt(1);
                            areNotificationsEnabled.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    case 7:
                        parcel.enforceInterface(DESCRIPTOR);
                        Bundle smallIconBitmap = getSmallIconBitmap();
                        parcel2.writeNoException();
                        if (smallIconBitmap != null) {
                            parcel2.writeInt(1);
                            smallIconBitmap.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString(DESCRIPTOR);
                return true;
            }
        }
    }

    Bundle areNotificationsEnabled(Bundle bundle) throws RemoteException;

    void cancelNotification(Bundle bundle) throws RemoteException;

    Bundle extraCommand(String str, Bundle bundle, IBinder iBinder) throws RemoteException;

    Bundle getActiveNotifications() throws RemoteException;

    Bundle getSmallIconBitmap() throws RemoteException;

    int getSmallIconId() throws RemoteException;

    Bundle notifyNotificationWithChannel(Bundle bundle) throws RemoteException;
}
