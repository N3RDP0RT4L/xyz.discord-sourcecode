package x.a.a.d;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
/* compiled from: ITrustedWebActivityCallback.java */
/* loaded from: classes.dex */
public interface a extends IInterface {

    /* compiled from: ITrustedWebActivityCallback.java */
    /* renamed from: x.a.a.d.a$a  reason: collision with other inner class name */
    /* loaded from: classes.dex */
    public static abstract class AbstractBinderC0433a extends Binder implements a {
        private static final String DESCRIPTOR = "android.support.customtabs.trusted.ITrustedWebActivityCallback";
        public static final int TRANSACTION_onExtraCallback = 2;

        /* compiled from: ITrustedWebActivityCallback.java */
        /* renamed from: x.a.a.d.a$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0434a implements a {
            public static a a;

            /* renamed from: b  reason: collision with root package name */
            public IBinder f3825b;

            public C0434a(IBinder iBinder) {
                this.f3825b = iBinder;
            }

            @Override // android.os.IInterface
            public IBinder asBinder() {
                return this.f3825b;
            }

            @Override // x.a.a.d.a
            public void onExtraCallback(String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(AbstractBinderC0433a.DESCRIPTOR);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (this.f3825b.transact(2, obtain, obtain2, 0) || AbstractBinderC0433a.getDefaultImpl() == null) {
                        obtain2.readException();
                    } else {
                        AbstractBinderC0433a.getDefaultImpl().onExtraCallback(str, bundle);
                    }
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public AbstractBinderC0433a() {
            attachInterface(this, DESCRIPTOR);
        }

        public static a asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof a)) {
                return new C0434a(iBinder);
            }
            return (a) queryLocalInterface;
        }

        public static a getDefaultImpl() {
            return C0434a.a;
        }

        public static boolean setDefaultImpl(a aVar) {
            if (C0434a.a != null) {
                throw new IllegalStateException("setDefaultImpl() called twice");
            } else if (aVar == null) {
                return false;
            } else {
                C0434a.a = aVar;
                return true;
            }
        }

        @Override // android.os.IInterface
        public IBinder asBinder() {
            return this;
        }

        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 2) {
                parcel.enforceInterface(DESCRIPTOR);
                onExtraCallback(parcel.readString(), parcel.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(parcel) : null);
                parcel2.writeNoException();
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString(DESCRIPTOR);
                return true;
            }
        }
    }

    void onExtraCallback(String str, Bundle bundle) throws RemoteException;
}
