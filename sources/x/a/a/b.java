package x.a.a;

import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;
import x.a.a.a;
/* compiled from: ICustomTabsService.java */
/* loaded from: classes.dex */
public interface b extends IInterface {

    /* compiled from: ICustomTabsService.java */
    /* loaded from: classes.dex */
    public static abstract class a extends Binder implements b {
        private static final String DESCRIPTOR = "android.support.customtabs.ICustomTabsService";
        public static final int TRANSACTION_extraCommand = 5;
        public static final int TRANSACTION_mayLaunchUrl = 4;
        public static final int TRANSACTION_newSession = 3;
        public static final int TRANSACTION_newSessionWithExtras = 10;
        public static final int TRANSACTION_postMessage = 8;
        public static final int TRANSACTION_receiveFile = 12;
        public static final int TRANSACTION_requestPostMessageChannel = 7;
        public static final int TRANSACTION_requestPostMessageChannelWithExtras = 11;
        public static final int TRANSACTION_updateVisuals = 6;
        public static final int TRANSACTION_validateRelationship = 9;
        public static final int TRANSACTION_warmup = 2;

        /* compiled from: ICustomTabsService.java */
        /* renamed from: x.a.a.b$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0431a implements b {
            public static b a;

            /* renamed from: b  reason: collision with root package name */
            public IBinder f3823b;

            public C0431a(IBinder iBinder) {
                this.f3823b = iBinder;
            }

            @Override // android.os.IInterface
            public IBinder asBinder() {
                return this.f3823b;
            }

            @Override // x.a.a.b
            public Bundle extraCommand(String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!this.f3823b.transact(5, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().extraCommand(str, bundle);
                    }
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.b
            public boolean mayLaunchUrl(x.a.a.a aVar, Uri uri, Bundle bundle, List<Bundle> list) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    boolean z2 = true;
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeTypedList(list);
                    if (!this.f3823b.transact(4, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().mayLaunchUrl(aVar, uri, bundle, list);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z2 = false;
                    }
                    return z2;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.b
            public boolean newSession(x.a.a.a aVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    boolean z2 = false;
                    if (!this.f3823b.transact(3, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().newSession(aVar);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z2 = true;
                    }
                    return z2;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.b
            public boolean newSessionWithExtras(x.a.a.a aVar, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    boolean z2 = true;
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!this.f3823b.transact(10, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().newSessionWithExtras(aVar, bundle);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z2 = false;
                    }
                    return z2;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.b
            public int postMessage(x.a.a.a aVar, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!this.f3823b.transact(8, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().postMessage(aVar, str, bundle);
                    }
                    obtain2.readException();
                    return obtain2.readInt();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.b
            public boolean receiveFile(x.a.a.a aVar, Uri uri, int i, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    boolean z2 = true;
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeInt(i);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!this.f3823b.transact(12, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().receiveFile(aVar, uri, i, bundle);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z2 = false;
                    }
                    return z2;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.b
            public boolean requestPostMessageChannel(x.a.a.a aVar, Uri uri) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    boolean z2 = true;
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!this.f3823b.transact(7, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().requestPostMessageChannel(aVar, uri);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z2 = false;
                    }
                    return z2;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.b
            public boolean requestPostMessageChannelWithExtras(x.a.a.a aVar, Uri uri, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    boolean z2 = true;
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!this.f3823b.transact(11, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().requestPostMessageChannelWithExtras(aVar, uri, bundle);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z2 = false;
                    }
                    return z2;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.b
            public boolean updateVisuals(x.a.a.a aVar, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    boolean z2 = true;
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!this.f3823b.transact(6, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().updateVisuals(aVar, bundle);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z2 = false;
                    }
                    return z2;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.b
            public boolean validateRelationship(x.a.a.a aVar, int i, Uri uri, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    obtain.writeInt(i);
                    boolean z2 = true;
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (!this.f3823b.transact(9, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().validateRelationship(aVar, i, uri, bundle);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() == 0) {
                        z2 = false;
                    }
                    return z2;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.b
            public boolean warmup(long j) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeLong(j);
                    boolean z2 = false;
                    if (!this.f3823b.transact(2, obtain, obtain2, 0) && a.getDefaultImpl() != null) {
                        return a.getDefaultImpl().warmup(j);
                    }
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z2 = true;
                    }
                    return z2;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public a() {
            attachInterface(this, DESCRIPTOR);
        }

        public static b asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof b)) {
                return new C0431a(iBinder);
            }
            return (b) queryLocalInterface;
        }

        public static b getDefaultImpl() {
            return C0431a.a;
        }

        public static boolean setDefaultImpl(b bVar) {
            if (C0431a.a != null) {
                throw new IllegalStateException("setDefaultImpl() called twice");
            } else if (bVar == null) {
                return false;
            } else {
                C0431a.a = bVar;
                return true;
            }
        }

        @Override // android.os.IInterface
        public IBinder asBinder() {
            return this;
        }

        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i != 1598968902) {
                Bundle bundle = null;
                Bundle bundle2 = null;
                Bundle bundle3 = null;
                Bundle bundle4 = null;
                Bundle bundle5 = null;
                Bundle bundle6 = null;
                Uri uri = null;
                Bundle bundle7 = null;
                Bundle bundle8 = null;
                switch (i) {
                    case 2:
                        parcel.enforceInterface(DESCRIPTOR);
                        boolean warmup = warmup(parcel.readLong());
                        parcel2.writeNoException();
                        parcel2.writeInt(warmup ? 1 : 0);
                        return true;
                    case 3:
                        parcel.enforceInterface(DESCRIPTOR);
                        boolean newSession = newSession(a.AbstractBinderC0429a.asInterface(parcel.readStrongBinder()));
                        parcel2.writeNoException();
                        parcel2.writeInt(newSession ? 1 : 0);
                        return true;
                    case 4:
                        parcel.enforceInterface(DESCRIPTOR);
                        x.a.a.a asInterface = a.AbstractBinderC0429a.asInterface(parcel.readStrongBinder());
                        Uri uri2 = parcel.readInt() != 0 ? (Uri) Uri.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean mayLaunchUrl = mayLaunchUrl(asInterface, uri2, bundle, parcel.createTypedArrayList(Bundle.CREATOR));
                        parcel2.writeNoException();
                        parcel2.writeInt(mayLaunchUrl ? 1 : 0);
                        return true;
                    case 5:
                        parcel.enforceInterface(DESCRIPTOR);
                        String readString = parcel.readString();
                        if (parcel.readInt() != 0) {
                            bundle8 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        Bundle extraCommand = extraCommand(readString, bundle8);
                        parcel2.writeNoException();
                        if (extraCommand != null) {
                            parcel2.writeInt(1);
                            extraCommand.writeToParcel(parcel2, 1);
                        } else {
                            parcel2.writeInt(0);
                        }
                        return true;
                    case 6:
                        parcel.enforceInterface(DESCRIPTOR);
                        x.a.a.a asInterface2 = a.AbstractBinderC0429a.asInterface(parcel.readStrongBinder());
                        if (parcel.readInt() != 0) {
                            bundle7 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean updateVisuals = updateVisuals(asInterface2, bundle7);
                        parcel2.writeNoException();
                        parcel2.writeInt(updateVisuals ? 1 : 0);
                        return true;
                    case 7:
                        parcel.enforceInterface(DESCRIPTOR);
                        x.a.a.a asInterface3 = a.AbstractBinderC0429a.asInterface(parcel.readStrongBinder());
                        if (parcel.readInt() != 0) {
                            uri = (Uri) Uri.CREATOR.createFromParcel(parcel);
                        }
                        boolean requestPostMessageChannel = requestPostMessageChannel(asInterface3, uri);
                        parcel2.writeNoException();
                        parcel2.writeInt(requestPostMessageChannel ? 1 : 0);
                        return true;
                    case 8:
                        parcel.enforceInterface(DESCRIPTOR);
                        x.a.a.a asInterface4 = a.AbstractBinderC0429a.asInterface(parcel.readStrongBinder());
                        String readString2 = parcel.readString();
                        if (parcel.readInt() != 0) {
                            bundle6 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        int postMessage = postMessage(asInterface4, readString2, bundle6);
                        parcel2.writeNoException();
                        parcel2.writeInt(postMessage);
                        return true;
                    case 9:
                        parcel.enforceInterface(DESCRIPTOR);
                        x.a.a.a asInterface5 = a.AbstractBinderC0429a.asInterface(parcel.readStrongBinder());
                        int readInt = parcel.readInt();
                        Uri uri3 = parcel.readInt() != 0 ? (Uri) Uri.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            bundle5 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean validateRelationship = validateRelationship(asInterface5, readInt, uri3, bundle5);
                        parcel2.writeNoException();
                        parcel2.writeInt(validateRelationship ? 1 : 0);
                        return true;
                    case 10:
                        parcel.enforceInterface(DESCRIPTOR);
                        x.a.a.a asInterface6 = a.AbstractBinderC0429a.asInterface(parcel.readStrongBinder());
                        if (parcel.readInt() != 0) {
                            bundle4 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean newSessionWithExtras = newSessionWithExtras(asInterface6, bundle4);
                        parcel2.writeNoException();
                        parcel2.writeInt(newSessionWithExtras ? 1 : 0);
                        return true;
                    case 11:
                        parcel.enforceInterface(DESCRIPTOR);
                        x.a.a.a asInterface7 = a.AbstractBinderC0429a.asInterface(parcel.readStrongBinder());
                        Uri uri4 = parcel.readInt() != 0 ? (Uri) Uri.CREATOR.createFromParcel(parcel) : null;
                        if (parcel.readInt() != 0) {
                            bundle3 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean requestPostMessageChannelWithExtras = requestPostMessageChannelWithExtras(asInterface7, uri4, bundle3);
                        parcel2.writeNoException();
                        parcel2.writeInt(requestPostMessageChannelWithExtras ? 1 : 0);
                        return true;
                    case 12:
                        parcel.enforceInterface(DESCRIPTOR);
                        x.a.a.a asInterface8 = a.AbstractBinderC0429a.asInterface(parcel.readStrongBinder());
                        Uri uri5 = parcel.readInt() != 0 ? (Uri) Uri.CREATOR.createFromParcel(parcel) : null;
                        int readInt2 = parcel.readInt();
                        if (parcel.readInt() != 0) {
                            bundle2 = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                        }
                        boolean receiveFile = receiveFile(asInterface8, uri5, readInt2, bundle2);
                        parcel2.writeNoException();
                        parcel2.writeInt(receiveFile ? 1 : 0);
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            } else {
                parcel2.writeString(DESCRIPTOR);
                return true;
            }
        }
    }

    Bundle extraCommand(String str, Bundle bundle) throws RemoteException;

    boolean mayLaunchUrl(x.a.a.a aVar, Uri uri, Bundle bundle, List<Bundle> list) throws RemoteException;

    boolean newSession(x.a.a.a aVar) throws RemoteException;

    boolean newSessionWithExtras(x.a.a.a aVar, Bundle bundle) throws RemoteException;

    int postMessage(x.a.a.a aVar, String str, Bundle bundle) throws RemoteException;

    boolean receiveFile(x.a.a.a aVar, Uri uri, int i, Bundle bundle) throws RemoteException;

    boolean requestPostMessageChannel(x.a.a.a aVar, Uri uri) throws RemoteException;

    boolean requestPostMessageChannelWithExtras(x.a.a.a aVar, Uri uri, Bundle bundle) throws RemoteException;

    boolean updateVisuals(x.a.a.a aVar, Bundle bundle) throws RemoteException;

    boolean validateRelationship(x.a.a.a aVar, int i, Uri uri, Bundle bundle) throws RemoteException;

    boolean warmup(long j) throws RemoteException;
}
