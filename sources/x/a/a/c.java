package x.a.a;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import x.a.a.a;
/* compiled from: IPostMessageService.java */
/* loaded from: classes.dex */
public interface c extends IInterface {

    /* compiled from: IPostMessageService.java */
    /* loaded from: classes.dex */
    public static abstract class a extends Binder implements c {
        private static final String DESCRIPTOR = "android.support.customtabs.IPostMessageService";
        public static final int TRANSACTION_onMessageChannelReady = 2;
        public static final int TRANSACTION_onPostMessage = 3;

        /* compiled from: IPostMessageService.java */
        /* renamed from: x.a.a.c$a$a  reason: collision with other inner class name */
        /* loaded from: classes.dex */
        public static class C0432a implements c {
            public static c a;

            /* renamed from: b  reason: collision with root package name */
            public IBinder f3824b;

            public C0432a(IBinder iBinder) {
                this.f3824b = iBinder;
            }

            @Override // android.os.IInterface
            public IBinder asBinder() {
                return this.f3824b;
            }

            @Override // x.a.a.c
            public void onMessageChannelReady(x.a.a.a aVar, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (this.f3824b.transact(2, obtain, obtain2, 0) || a.getDefaultImpl() == null) {
                        obtain2.readException();
                    } else {
                        a.getDefaultImpl().onMessageChannelReady(aVar, bundle);
                    }
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @Override // x.a.a.c
            public void onPostMessage(x.a.a.a aVar, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(a.DESCRIPTOR);
                    obtain.writeStrongBinder(aVar != null ? aVar.asBinder() : null);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (this.f3824b.transact(3, obtain, obtain2, 0) || a.getDefaultImpl() == null) {
                        obtain2.readException();
                    } else {
                        a.getDefaultImpl().onPostMessage(aVar, str, bundle);
                    }
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public a() {
            attachInterface(this, DESCRIPTOR);
        }

        public static c asInterface(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface == null || !(queryLocalInterface instanceof c)) {
                return new C0432a(iBinder);
            }
            return (c) queryLocalInterface;
        }

        public static c getDefaultImpl() {
            return C0432a.a;
        }

        public static boolean setDefaultImpl(c cVar) {
            if (C0432a.a != null) {
                throw new IllegalStateException("setDefaultImpl() called twice");
            } else if (cVar == null) {
                return false;
            } else {
                C0432a.a = cVar;
                return true;
            }
        }

        @Override // android.os.IInterface
        public IBinder asBinder() {
            return this;
        }

        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            Bundle bundle = null;
            if (i == 2) {
                parcel.enforceInterface(DESCRIPTOR);
                x.a.a.a asInterface = a.AbstractBinderC0429a.asInterface(parcel.readStrongBinder());
                if (parcel.readInt() != 0) {
                    bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                }
                onMessageChannelReady(asInterface, bundle);
                parcel2.writeNoException();
                return true;
            } else if (i == 3) {
                parcel.enforceInterface(DESCRIPTOR);
                x.a.a.a asInterface2 = a.AbstractBinderC0429a.asInterface(parcel.readStrongBinder());
                String readString = parcel.readString();
                if (parcel.readInt() != 0) {
                    bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel);
                }
                onPostMessage(asInterface2, readString, bundle);
                parcel2.writeNoException();
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString(DESCRIPTOR);
                return true;
            }
        }
    }

    void onMessageChannelReady(x.a.a.a aVar, Bundle bundle) throws RemoteException;

    void onPostMessage(x.a.a.a aVar, String str, Bundle bundle) throws RemoteException;
}
