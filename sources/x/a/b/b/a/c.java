package x.a.b.b.a;

import android.media.MediaMetadata;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.ParcelableVolumeInfo;
import android.support.v4.media.session.PlaybackStateCompat;
import androidx.annotation.RequiresApi;
import androidx.collection.ArrayMap;
import androidx.media.AudioAttributesCompat;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import x.a.b.b.a.a;
/* compiled from: MediaControllerCompat.java */
/* loaded from: classes.dex */
public abstract class c implements IBinder.DeathRecipient {
    public x.a.b.b.a.a a;

    /* compiled from: MediaControllerCompat.java */
    @RequiresApi(21)
    /* loaded from: classes.dex */
    public static class a extends MediaController.Callback {
        public final WeakReference<c> a;

        public a(c cVar) {
            this.a = new WeakReference<>(cVar);
        }

        @Override // android.media.session.MediaController.Callback
        public void onAudioInfoChanged(MediaController.PlaybackInfo playbackInfo) {
            if (this.a.get() != null) {
                playbackInfo.getPlaybackType();
                AudioAttributesCompat.wrap(playbackInfo.getAudioAttributes());
                playbackInfo.getVolumeControl();
                playbackInfo.getMaxVolume();
                playbackInfo.getCurrentVolume();
            }
        }

        @Override // android.media.session.MediaController.Callback
        public void onExtrasChanged(Bundle bundle) {
            MediaSessionCompat.a(bundle);
            this.a.get();
        }

        @Override // android.media.session.MediaController.Callback
        public void onMetadataChanged(MediaMetadata mediaMetadata) {
            if (this.a.get() != null) {
                ArrayMap<String, Integer> arrayMap = MediaMetadataCompat.j;
                if (mediaMetadata != null) {
                    Parcel obtain = Parcel.obtain();
                    mediaMetadata.writeToParcel(obtain, 0);
                    obtain.setDataPosition(0);
                    obtain.recycle();
                    Objects.requireNonNull(MediaMetadataCompat.CREATOR.createFromParcel(obtain));
                }
            }
        }

        @Override // android.media.session.MediaController.Callback
        public void onPlaybackStateChanged(PlaybackState playbackState) {
            ArrayList arrayList;
            PlaybackStateCompat.CustomAction customAction;
            c cVar = this.a.get();
            if (cVar != null && cVar.a == null) {
                Bundle bundle = null;
                if (playbackState != null) {
                    List<PlaybackState.CustomAction> j = PlaybackStateCompat.b.j(playbackState);
                    if (j != null) {
                        ArrayList arrayList2 = new ArrayList(j.size());
                        for (PlaybackState.CustomAction customAction2 : j) {
                            if (customAction2 != null) {
                                PlaybackState.CustomAction customAction3 = customAction2;
                                Bundle l = PlaybackStateCompat.b.l(customAction3);
                                MediaSessionCompat.a(l);
                                customAction = new PlaybackStateCompat.CustomAction(PlaybackStateCompat.b.f(customAction3), PlaybackStateCompat.b.o(customAction3), PlaybackStateCompat.b.m(customAction3), l);
                            } else {
                                customAction = null;
                            }
                            arrayList2.add(customAction);
                        }
                        arrayList = arrayList2;
                    } else {
                        arrayList = null;
                    }
                    if (Build.VERSION.SDK_INT >= 22) {
                        bundle = PlaybackStateCompat.c.a(playbackState);
                        MediaSessionCompat.a(bundle);
                    }
                    new PlaybackStateCompat(PlaybackStateCompat.b.r(playbackState), PlaybackStateCompat.b.q(playbackState), PlaybackStateCompat.b.i(playbackState), PlaybackStateCompat.b.p(playbackState), PlaybackStateCompat.b.g(playbackState), 0, PlaybackStateCompat.b.k(playbackState), PlaybackStateCompat.b.n(playbackState), arrayList, PlaybackStateCompat.b.h(playbackState), bundle);
                }
            }
        }

        @Override // android.media.session.MediaController.Callback
        public void onQueueChanged(List<MediaSession.QueueItem> list) {
            if (this.a.get() != null) {
                MediaSessionCompat.QueueItem queueItem = null;
                if (list != null) {
                    ArrayList arrayList = new ArrayList();
                    for (MediaSession.QueueItem queueItem2 : list) {
                        if (queueItem2 != null) {
                            MediaSession.QueueItem queueItem3 = queueItem2;
                            queueItem = new MediaSessionCompat.QueueItem(queueItem3, MediaDescriptionCompat.a(MediaSessionCompat.QueueItem.b.b(queueItem3)), MediaSessionCompat.QueueItem.b.c(queueItem3));
                        }
                        arrayList.add(queueItem);
                    }
                }
            }
        }

        @Override // android.media.session.MediaController.Callback
        public void onQueueTitleChanged(CharSequence charSequence) {
            this.a.get();
        }

        @Override // android.media.session.MediaController.Callback
        public void onSessionDestroyed() {
            this.a.get();
        }

        @Override // android.media.session.MediaController.Callback
        public void onSessionEvent(String str, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            c cVar = this.a.get();
            if (cVar != null && cVar.a != null) {
                int i = Build.VERSION.SDK_INT;
            }
        }
    }

    /* compiled from: MediaControllerCompat.java */
    /* loaded from: classes.dex */
    public static class b extends a.AbstractBinderC0438a {
        public final WeakReference<c> a;

        public b(c cVar) {
            this.a = new WeakReference<>(cVar);
        }

        public void U(CharSequence charSequence) throws RemoteException {
            this.a.get();
        }

        public void W() throws RemoteException {
            this.a.get();
        }

        public void X(MediaMetadataCompat mediaMetadataCompat) throws RemoteException {
            this.a.get();
        }

        public void s0(ParcelableVolumeInfo parcelableVolumeInfo) throws RemoteException {
            if (this.a.get() != null && parcelableVolumeInfo != null) {
                new AudioAttributesCompat.Builder().setLegacyStreamType(parcelableVolumeInfo.k).build();
            }
        }

        public void y(Bundle bundle) throws RemoteException {
            this.a.get();
        }

        public void z(List<MediaSessionCompat.QueueItem> list) throws RemoteException {
            this.a.get();
        }
    }

    public c() {
        new a(this);
    }

    @Override // android.os.IBinder.DeathRecipient
    public void binderDied() {
    }
}
